/*
 * Created on Feb 12, 2005
 */
package org.jocelyn_chaumel.tools;

import java.util.ArrayList;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class StringManipulatorHelperTest extends JCTestCase
{

	/**
	 * @param aName
	 */
	public StringManipulatorHelperTest(final String aName)
	{
		super(aName);
	}

	public final void testBuildString_invalidLength()
	{
		try
		{
			StringManipulatorHelper.buildString(' ', 0);
			fail("Invalid length has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void testBuildString_valid()
	{
		try
		{
			assertEquals(" ", StringManipulatorHelper.buildString(' ', 1));
			assertEquals("   ", StringManipulatorHelper.buildString(' ', 3));
			assertEquals("-----", StringManipulatorHelper.buildString('-', 5));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_leftPadding_nullParam()
	{
		try
		{
			StringManipulatorHelper.leftPadding(null, 1);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_leftPadding_invalidFinalStringLength()
	{
		try
		{
			StringManipulatorHelper.leftPadding("", -1);
			fail("Invalid Final String Length has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_leftPadding_valid()
	{
		try
		{
			assertEquals("", StringManipulatorHelper.leftPadding("", 0));
			assertEquals("", StringManipulatorHelper.leftPadding("1", 0));
			assertEquals("3", StringManipulatorHelper.leftPadding("123", 1));
			assertEquals("23", StringManipulatorHelper.leftPadding("123", 2));
			assertEquals("123", StringManipulatorHelper.leftPadding("123", 3));
			assertEquals(" 123", StringManipulatorHelper.leftPadding("123", 4));
			assertEquals("  123", StringManipulatorHelper.leftPadding("123", 5));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_rightPadding_valid()
	{
		try
		{
			assertEquals("", StringManipulatorHelper.rightPadding("", 0, '0'));
			assertEquals("", StringManipulatorHelper.rightPadding("1", 0, '0'));
			assertEquals("1", StringManipulatorHelper.rightPadding("123", 1, '0'));
			assertEquals("12", StringManipulatorHelper.rightPadding("123", 2, '0'));
			assertEquals("123", StringManipulatorHelper.rightPadding("123", 3, '0'));
			assertEquals("1230", StringManipulatorHelper.rightPadding("123", 4, '0'));
			assertEquals("12300", StringManipulatorHelper.rightPadding("123", 5, '0'));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_replace_nullParam_1()
	{
		try
		{
			StringManipulatorHelper.replace(null, "1", "2");
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_replace_nullParam_2()
	{
		try
		{
			StringManipulatorHelper.replace("1", null, "2");
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_replace_nullParam_3()
	{
		try
		{
			StringManipulatorHelper.replace("1", "2", null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_replace_valid()
	{
		try
		{
			assertEquals("1", StringManipulatorHelper.replace("1", "2", ""));
			assertEquals("1", StringManipulatorHelper.replace("1", "1", "1"));
			assertEquals("11", StringManipulatorHelper.replace("1", "1", "11"));
			assertEquals("110011", StringManipulatorHelper.replace("1001", "1", "11"));
			assertEquals("Jocelyn", StringManipulatorHelper.replace("jocelyn", "jo", "Jo"));
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_leftPaddingStringBuffer_all()
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			StringManipulatorHelper.leftPaddind(sb, 1);
			sb.append(1);
			assertEquals(" 1", sb.toString());

			sb = new StringBuffer("1");
			StringManipulatorHelper.leftPaddind(sb, 3);
			sb.append("2");
			assertEquals("1  2", sb.toString());

			sb = new StringBuffer("1");
			StringManipulatorHelper.leftPaddind(sb, 0);
			sb.append("2");
			assertEquals("12", sb.toString());

			sb = new StringBuffer("123");
			StringManipulatorHelper.leftPaddind(sb, 2);
			assertEquals("12", sb.toString());

			sb = new StringBuffer("123456");
			StringManipulatorHelper.leftPaddind(sb, 4);
			assertEquals("1...", sb.toString());

			try
			{
				StringManipulatorHelper.leftPaddind(null, 5);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				StringManipulatorHelper.leftPaddind(sb, -1);
				fail("Invalid 'final length' param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_rightPaddingStringBuffer_all()
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			StringManipulatorHelper.rightPadding(sb, "123", 4);
			assertEquals(" 123", sb.toString());

			sb = new StringBuffer();
			StringManipulatorHelper.rightPadding(sb, "123", 3);
			assertEquals("123", sb.toString());

			sb = new StringBuffer();
			StringManipulatorHelper.rightPadding(sb, "123456", 5);
			assertEquals("12...", sb.toString());

			sb = new StringBuffer();
			try
			{
				StringManipulatorHelper.rightPadding(null, "123", 4);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				StringManipulatorHelper.rightPadding(sb, null, 4);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				StringManipulatorHelper.rightPadding(sb, "123", 0);
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			sb.append("1234567");
			try
			{
				StringManipulatorHelper.rightPadding(sb, "123", 4);
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_formatNumber_all()
	{
		try
		{
			assertEquals("1", StringManipulatorHelper.formatNumber(1));
			assertEquals("12", StringManipulatorHelper.formatNumber(12));
			assertEquals("123", StringManipulatorHelper.formatNumber(123));
			assertEquals("1 234", StringManipulatorHelper.formatNumber(1234));
			assertEquals("12 345", StringManipulatorHelper.formatNumber(12345));
			assertEquals("123 456", StringManipulatorHelper.formatNumber(123456));
			assertEquals("1 234 567", StringManipulatorHelper.formatNumber(1234567));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_tokenize_all()
	{
		try
		{
			ArrayList<String> tokenList = null;
			tokenList = StringManipulatorHelper.tokenize("joss", ",");
			assertEquals(1, tokenList.size());
			assertEquals("joss", tokenList.get(0));

			tokenList = StringManipulatorHelper.tokenize("j,k", ",");
			assertEquals(2, tokenList.size());
			assertEquals("j", tokenList.get(0));
			assertEquals("k", tokenList.get(1));

			tokenList = StringManipulatorHelper.tokenize("j,,k", ",");
			assertEquals(2, tokenList.size());
			assertEquals("j", tokenList.get(0));
			assertEquals("k", tokenList.get(1));

			tokenList = StringManipulatorHelper.tokenize("j, ,k", ",");
			assertEquals(3, tokenList.size());
			assertEquals("j", tokenList.get(0));
			assertEquals(" ", tokenList.get(1));
			assertEquals("k", tokenList.get(2));
			
			tokenList = StringManipulatorHelper.tokenize("", ",");
			assertEquals(0, tokenList.size());
			try
			{
				StringManipulatorHelper.tokenize(null, ",");
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			
			try
			{
				StringManipulatorHelper.tokenize("sdf", null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			
			try
			{
				StringManipulatorHelper.tokenize("empty", "");
				fail("Empty Delimiter has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_buildCSVString_all()
	{
		try
		{
			ArrayList<String> tokenList = new ArrayList<String>();
			String csvString = StringManipulatorHelper.buildCSVString(tokenList, ",");
			assertEquals("", csvString);
			
			tokenList.add("joss");
			csvString = StringManipulatorHelper.buildCSVString(tokenList, ",");
			assertEquals("joss", csvString);
			
			tokenList.add("toto");
			csvString = StringManipulatorHelper.buildCSVString(tokenList, ",");
			assertEquals("joss,toto", csvString);
			
			tokenList.add("");
			csvString = StringManipulatorHelper.buildCSVString(tokenList, ",");
			assertEquals("joss,toto", csvString);
			
			try
			{
				StringManipulatorHelper.buildCSVString(null, ",");
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			
			try
			{
				StringManipulatorHelper.buildCSVString(tokenList, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			
			try
			{
				StringManipulatorHelper.buildCSVString(tokenList, "");
				fail("Empty Delimiter has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
