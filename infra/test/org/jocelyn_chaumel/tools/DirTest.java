/*
 * DirTest.java
 * JUnit based test
 *
 * Created on October 25, 2003, 4:17 PM
 */

package org.jocelyn_chaumel.tools;

import java.io.File;
import java.io.FileOutputStream;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;


/**
 * 
 * @author Jocelyn
 */
public class DirTest extends JCTestCase
{
    public DirTest(final String aName)
    {
        super(aName);
    }

    public final void testCntr_nullParam()
    {
        try
        {
            new Dir(null);
            fail("Invalid param has been accepted.");
        } catch (final IllegalArgumentException ex)
        {
            // NOP
        } catch (final Exception ex)
        {
            fail(ex);
        }
    }

    public final void testCntr_invalidDirParam()
    {
        try
        {
            new Dir(new File("c:\\windows\\regedit.exe"));

            fail("Invalid param has been accepted.");
        } catch (final IllegalArgumentException ex)
        {
            // NOP
        } catch (final Exception ex)
        {
            fail(ex);
        }
    }

    //------------------------------------
    public final void testgetDirList_valid()
    {
        final File srcDir = new File("c:\\temp_test");
        try
        {
            assertTrue(!srcDir.exists());
            assertTrue(srcDir.mkdir());
            (new FileOutputStream(new File(srcDir, "5.txt"))).close();
            (new FileOutputStream(new File(srcDir, "4.txt"))).close();
            (new FileOutputStream(new File(srcDir, "3.txt"))).close();
            (new FileOutputStream(new File(srcDir, "2.txt"))).close();
            (new FileOutputStream(new File(srcDir, "1.txt"))).close();
            assertTrue((new File(srcDir, "dir5")).mkdir());
            assertTrue((new File(srcDir, "dir4")).mkdir());
            assertTrue((new File(srcDir, "dir3")).mkdir());
            assertTrue((new File(srcDir, "dir2")).mkdir());
            assertTrue((new File(srcDir, "dir1")).mkdir());

            final Dir dir = new Dir(srcDir);

            final File[] dirList = dir.getDirList();
            final File[] fileList = dir.getFileList();

            assertTrue(dirList[0].getAbsolutePath()
                .equals("c:\\temp_test\\dir1"));
            assertTrue(dirList[1].getAbsolutePath()
                .equals("c:\\temp_test\\dir2"));
            assertTrue(dirList[2].getAbsolutePath()
                .equals("c:\\temp_test\\dir3"));
            assertTrue(dirList[3].getAbsolutePath()
                .equals("c:\\temp_test\\dir4"));
            assertTrue(dirList[4].getAbsolutePath()
                .equals("c:\\temp_test\\dir5"));
            assertTrue(fileList[0].getAbsolutePath()
                .equals("c:\\temp_test\\1.txt"));
            assertTrue(fileList[1].getAbsolutePath()
                .equals("c:\\temp_test\\2.txt"));
            assertTrue(fileList[2].getAbsolutePath()
                .equals("c:\\temp_test\\3.txt"));
            assertTrue(fileList[3].getAbsolutePath()
                .equals("c:\\temp_test\\4.txt"));
            assertTrue(fileList[4].getAbsolutePath()
                .equals("c:\\temp_test\\5.txt"));

        } catch (final Exception ex)
        {
            fail(ex);
        } finally
        {
            assertTrue((new File(srcDir, "1.txt")).delete());
            assertTrue((new File(srcDir, "2.txt")).delete());
            assertTrue((new File(srcDir, "3.txt")).delete());
            assertTrue((new File(srcDir, "4.txt")).delete());
            assertTrue((new File(srcDir, "5.txt")).delete());
            assertTrue((new File(srcDir, "dir1")).delete());
            assertTrue((new File(srcDir, "dir2")).delete());
            assertTrue((new File(srcDir, "dir3")).delete());
            assertTrue((new File(srcDir, "dir4")).delete());
            assertTrue((new File(srcDir, "dir5")).delete());
            assertTrue(srcDir.delete());
        }
    }
}