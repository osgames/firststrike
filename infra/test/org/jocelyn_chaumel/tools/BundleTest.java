/*
 * Created on Mar 21, 2005
 */
package org.jocelyn_chaumel.tools;

import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.ImageIcon;

/**
 * @author Jocelyn Chaumel
 */
public class BundleTest
{

	public static void main(String[] args)
	{

		String path = "org/jocelyn_chaumel/tools/city.gif";
		ImageIcon icon = new ImageIcon(path);
		System.out.println("Path : " + path);
		System.out.println("image status : " + icon.getImageLoadStatus() + "\n");

		path = ClassLoader.getSystemResource("org/jocelyn_chaumel/tools/city.gif").getPath();
		icon = new ImageIcon(path);
		System.out.println("Path : " + path);
		System.out.println("image status : " + icon.getImageLoadStatus() + "\n");

		URL url = ClassLoader.getSystemResource("org/jocelyn_chaumel/tools/city.gif");
		icon = new ImageIcon(url);
		System.out.println("Url : " + url);
		System.out.println("image status : " + icon.getImageLoadStatus() + "\n");

		path = "D:/My Documents/dev_java/projet infra/classes/org/jocelyn_chaumel/tools/city.gif";
		icon = new ImageIcon(path);
		System.out.println("Path : " + path);
		System.out.println("image status : " + icon.getImageLoadStatus() + "\n");

		url = ClassLoader.getSystemClassLoader().getResource("org/jocelyn_chaumel/tools/bundle.properties");
		String filename = url.getFile().replaceAll("%20", " ");
		File file = new File(filename);

		System.out.println("filename:" + filename);
		System.out.println("exists:" + file.exists());

		if (file.isFile())
		{
			System.out.println("est in fichier");
			Properties properties = new Properties();

			try
			{
				properties.load(new FileInputStream(file));
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			Enumeration keysEnum = properties.keys();
			String key;
			String value;
			while (keysEnum.hasMoreElements())
			{
				key = (String) keysEnum.nextElement();
				value = properties.getProperty(key);

				System.out.println(key + "=" + value);
			}
		}

		try
		{
			url = new File("D:/My Documents/dev_java/projet infra/classes/org/jocelyn_chaumel/tools/bundle.properties")
					.toURL();
			System.out.println("url.getFile : " + url.getFile().replaceAll(" ", "%20"));
			url = ClassLoader.getSystemClassLoader().getResource("org/jocelyn_chaumel/tools/bundle.properties");
			System.out.println("url.getPath : " + url.getPath());
		} catch (MalformedURLException ex)
		{
			ex.printStackTrace();
		}

	}
}
