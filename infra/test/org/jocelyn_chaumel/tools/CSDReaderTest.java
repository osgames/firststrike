/*
 * Created on Oct 23, 2004
 */
package org.jocelyn_chaumel.tools;

import java.io.File;
import java.text.ParseException;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class CSDReaderTest extends JCTestCase
{
    public CSDReaderTest(final String aName)
    {
        super(aName);
    }

    public void test_NextTokenAsInt_valid()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "1,2,000000000000000003");
            CSDReader reader = new CSDReader(file);
            reader.readFile();
            assertEquals(true, reader.hasToken());
            assertEquals(1, reader.nextTokenAsInt());
            assertEquals(true, reader.hasToken());
            assertEquals(2, reader.nextTokenAsInt());
            assertEquals(true, reader.hasToken());
            assertEquals(3, reader.nextTokenAsInt());
            assertEquals(false, reader.hasToken());
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch(Exception ex){
                fail(ex);
            }
        }
    }

    public void test_NextTokenAsInt_invalideInt()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "1,2,1234567890123456");
            CSDReader reader = new CSDReader(file);
            reader.readFile();
            assertEquals(true, reader.hasToken());
            assertEquals(1, reader.nextTokenAsInt());
            assertEquals(true, reader.hasToken());
            assertEquals(2, reader.nextTokenAsInt());
            assertEquals(true, reader.hasToken());
            reader.nextTokenAsInt();
            fail();
        } catch (ParseException ex){
            // nop
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch(Exception ex){
                fail(ex);
            }
        }
    }

    public void test_NextTokenAsLong_valid()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "1,2,00000001234567890123");
            CSDReader reader = new CSDReader(file);
            reader.readFile();
            assertEquals(true, reader.hasToken());
            assertEquals(1, reader.nextTokenAsLong());
            assertEquals(true, reader.hasToken());
            assertEquals(2, reader.nextTokenAsLong());
            assertEquals(true, reader.hasToken());
            assertEquals(1234567890123l, reader.nextTokenAsLong());
            assertEquals(false, reader.hasToken());
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch(Exception ex){
                fail(ex);
            }
        }
    }

    public void test_NextTokenAsLong_invalideLong()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "1,2,123456789012345601234l");
            CSDReader reader = new CSDReader(file);
            reader.readFile();
            assertEquals(true, reader.hasToken());
            assertEquals(1, reader.nextTokenAsInt());
            assertEquals(true, reader.hasToken());
            assertEquals(2, reader.nextTokenAsInt());
            assertEquals(true, reader.hasToken());
            reader.nextTokenAsLong();
            fail();
        } catch (ParseException ex){
            // nop
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch(Exception ex){
                fail(ex);
            }
        }
    }

    public void test_NextTokenAsFloat_valid()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "1.0,2.2,00000001.2345678900000000");
            CSDReader reader = new CSDReader(file);
            reader.readFile();
            assertEquals(true, reader.hasToken());
            assertEquals(1.0, reader.nextTokenAsFloat(), 0.000001);
            assertEquals(true, reader.hasToken());
            assertEquals(2.2, reader.nextTokenAsFloat(), 0.000001);
            assertEquals(true, reader.hasToken());
            assertEquals(1.23456789, reader.nextTokenAsFloat(), 0.00000001);
            assertEquals(false, reader.hasToken());
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch(Exception ex){
                fail(ex);
            }
        }
    }

    public void test_NextTokenAsFloat_invalideFloat()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "1,2.2,1.234567890123456");
            CSDReader reader = new CSDReader(file);
            reader.readFile();
            assertEquals(true, reader.hasToken());
            assertEquals(1.0, reader.nextTokenAsFloat(), 0.000001);
            assertEquals(true, reader.hasToken());
            assertEquals(2.2, reader.nextTokenAsFloat(), 0.000001);
            assertEquals(true, reader.hasToken());
            reader.nextTokenAsFloat();
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch(Exception ex){
                fail(ex);
            }
        }
    }

    public void test_NextTokenAsDouble_valid()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "1.0,2.2,00000000001.0002345678900000000");
            CSDReader reader = new CSDReader(file);
            reader.readFile();
            assertEquals(true, reader.hasToken());
            assertEquals(1.0, reader.nextTokenAsDouble(), 0.000000001);
            assertEquals(true, reader.hasToken());
            assertEquals(2.2, reader.nextTokenAsDouble(), 0.000000001);
            assertEquals(true, reader.hasToken());
            assertEquals(1.0002345678900000000, reader.nextTokenAsDouble(), 0.00000000001);
            assertEquals(false, reader.hasToken());
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch(Exception ex){
                fail(ex);
            }
        }
    }

    public void test_NextTokenAsFloat_invalideDouble()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "1,2.2,1.0000002000030004000567890123456");
            CSDReader reader = new CSDReader(file);
            reader.readFile();
            assertEquals(true, reader.hasToken());
            assertEquals(1.0, reader.nextTokenAsDouble(), 0.000000001);
            assertEquals(true, reader.hasToken());
            assertEquals(2.2, reader.nextTokenAsDouble(), 0.000000001);
            assertEquals(true, reader.hasToken());
            reader.nextTokenAsDouble();
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch(Exception ex){
                fail(ex);
            }
        }
    }

    public void test_NextTokenAsJavaDate_valid()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "1,2,00000001234567890123");
            CSDReader reader = new CSDReader(file);
            reader.readFile();
            assertEquals(true, reader.hasToken());
            assertEquals(1, reader.nextTokenAsJavaDate().getTime());
            assertEquals(true, reader.hasToken());
            assertEquals(2, reader.nextTokenAsJavaDate().getTime());
            assertEquals(true, reader.hasToken());
            assertEquals(1234567890123l, reader.nextTokenAsJavaDate().getTime());
            assertEquals(false, reader.hasToken());
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch(Exception ex){
                fail(ex);
            }
        }
    }

    public void test_NextTokenAsJavaDate_invalideJavaDate()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "1,2,1234567890123456");
            CSDReader reader = new CSDReader(file);
            reader.readFile();
            assertEquals(true, reader.hasToken());
            assertEquals(1, reader.nextTokenAsJavaDate().getTime());
            assertEquals(true, reader.hasToken());
            assertEquals(2, reader.nextTokenAsJavaDate().getTime());
            assertEquals(true, reader.hasToken());
            reader.nextTokenAsJavaDate();
        } catch (ParseException ex){
            // nop
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch(Exception ex){
                fail(ex);
            }
        }
    }
}