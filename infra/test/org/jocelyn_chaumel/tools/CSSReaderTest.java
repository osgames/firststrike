/*
 * Created on Oct 23, 2004
 */
package org.jocelyn_chaumel.tools;

import java.io.File;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class CSSReaderTest extends JCTestCase
{

    public CSSReaderTest(final String aName)
    {
        super(aName);
    }

    public final void test_contructor_NullParam()
    {
        try
        {
            new CSSReader(null);
            fail("Null Ref has been accepted");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_contructor_InvalidParam()
    {
        try
        {
            new CSSReader(new File("toto"));
            fail("Null Ref has been accepted");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_hasToken_invalideState()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "mot1, mot2, mot3");

            CSSReader reader = new CSSReader(file);
            reader.hasToken();
            fail("readFile method must be executed before hasToken method");
        } catch (IllegalStateException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch (Exception ex){
                fail (ex);
            }
        }
    }

    public final void test_nextToken_invalideState()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "mot1, mot2, mot3");

            CSSReader reader = new CSSReader(file);
            reader.nextToken();
            fail("readFile method must be executed before nextToken method");
        } catch (IllegalStateException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch (Exception ex){
                fail (ex);
            }
        }
    }

    public final void test_readFile_invalideState()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "mot1, mot2, mot3");

            CSSReader reader = new CSSReader(file);
            reader.readFile();
            reader.readFile();
            fail("readFile method must be executed before nextToken method");
        } catch (IllegalStateException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch (Exception ex){
                fail (ex);
            }
        }
    }

    public final void test_nextToken_()
    {
        File file = new File("c:\\Temp\\fichier_a_effacer.txt");
        try
        {
            FileMgr.createFile(file, "mot1, mot2, mot3\nmot4");
            CSSReader reader = new CSSReader(file);
            reader.readFile();
            assertEquals(true, reader.hasToken());
            assertEquals("mot1", reader.nextToken());
            assertEquals(true, reader.hasToken());
            assertEquals(" mot2", reader.nextToken());
            assertEquals(true, reader.hasToken());
            assertEquals(" mot3\nmot4", reader.nextToken());
            assertEquals(false, reader.hasToken());
        } catch (IllegalStateException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        } finally {
            try {
                FileMgr.forceDelete(file);
            } catch (Exception ex){
                fail (ex);
            }
        }
    }

}