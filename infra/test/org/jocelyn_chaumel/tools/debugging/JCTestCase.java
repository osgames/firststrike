/*
 * Created on Oct 23, 2004
 */
package org.jocelyn_chaumel.tools.debugging;

import junit.framework.TestCase;

/**
 * @author Jocelyn Chaumel
 */
public class JCTestCase extends TestCase
{

	public JCTestCase(final String aName)
	{
		super(aName);
	}

	public static void fail(Exception ex) throws RuntimeException
	{
		ex.printStackTrace();
		throw new RuntimeException(ex);
	}

}