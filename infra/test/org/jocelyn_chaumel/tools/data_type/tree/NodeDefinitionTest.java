/*
 * Created on Jul 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class NodeDefinitionTest extends JCTestCase
{

    /**
     * @param aName
     */
    public NodeDefinitionTest(final String aName)
    {
        super(aName);
    }

    public final void test_Cntr_nullParam()
    {
        try
        {
            new NodeDefinition(null);
            fail("Null param has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    public final void test_addAccessibleNode_nullParam()
    {
        try
        {
            NodeDefinition nodeDef = new NodeDefinition(new NodeId(1));
            nodeDef.addAccessibleNode(null);
            fail("Null param has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    public final void test_addAccessibleNode_nodeAlreadyAdded()
    {
        try
        {
            NodeDefinition nodeDef = new NodeDefinition(new NodeId(1));
            AccessibleNode accessibleNode = new AccessibleNode(new NodeDefinition(new NodeId(2)), 0);
            nodeDef.addAccessibleNode( accessibleNode);
            try {
                nodeDef.addAccessibleNode(accessibleNode);
	            fail("Invalid node id has been accepted.");
	        } catch (IllegalArgumentException ex)
	        {
	            // NOP
	        }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    public final void test_getDistanceForANode_nullParam()
    {
        try
        {
            NodeDefinition nodeDef = new NodeDefinition(new NodeId(1));
            nodeDef.addAccessibleNode(new AccessibleNode (new NodeDefinition(new NodeId(2)), 3));
            try {
	            nodeDef.getDistanceForANode(null);
	            fail("Null param has been accepted.");
	        } catch (IllegalArgumentException ex)
	        {
	            // NOP
	        }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    public final void test_getDistanceForANode_invalidNode()
    {
        try
        {
            NodeDefinition nodeDef = new NodeDefinition(new NodeId(1));
            nodeDef.addAccessibleNode(new AccessibleNode (new NodeDefinition(new NodeId(2)), 3));
            try {
	            nodeDef.getDistanceForANode(new NodeDefinition(new NodeId(3)));
	            fail("Invalid node id has been accepted.");
	        } catch (IllegalStateException ex)
	        {
	            // NOP
	        }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    public final void test_valid()
    {
        try
        {
            final NodeId rootNode = new NodeId(10);
            final NodeDefinition node_1 = new NodeDefinition(new NodeId(1));
            final NodeDefinition node_2 = new NodeDefinition(new NodeId(2));
            final NodeDefinition node_3 = new NodeDefinition(new NodeId(3));
            NodeDefinition nodeDef = new NodeDefinition(rootNode);
            
            assertEquals(0, nodeDef.getAccessibleNodes().length);
            nodeDef.addAccessibleNode(new AccessibleNode(node_1, 1));
            assertEquals(1, nodeDef.getAccessibleNodes().length);
            
            nodeDef.addAccessibleNode(new AccessibleNode(node_2, 2));
            nodeDef.addAccessibleNode(new AccessibleNode(node_3, 3));
            assertEquals(3, nodeDef.getAccessibleNodes().length);
            assertEquals(1, nodeDef.getAccessibleNodes()[0]._nodeDefinition.getNodeId().getId());
            assertEquals(2, nodeDef.getAccessibleNodes()[1]._nodeDefinition.getNodeId().getId());
            assertEquals(3, nodeDef.getAccessibleNodes()[2]._nodeDefinition.getNodeId().getId());
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
}
