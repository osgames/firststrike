/*
 * Created on Jul 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class TreeNodeTest extends JCTestCase
{

    /**
     * @param aName
     */
    public TreeNodeTest(final String aName)
    {
        super(aName);
    }

    public final void test_cntr1_nullParam()
    {
        try
        {
            new TreeNode(null);
            fail("Null param has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_cntr2_nullParam_1()
    {
        try
        {
            final TreeNode parent = new TreeNode(new NodeDefinition(new NodeId(1)));
            new TreeNode(null, 0, parent);
            fail("Null param has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_cntr2_nullParam_2()
    {
        try
        {
            final NodeDefinition start = new NodeDefinition(new NodeId(1));
            new TreeNode(start, 0, null);
            fail("Null param has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_isAParentNode_nullParam()
    {
        try
        {
            final NodeDefinition start = new NodeDefinition(new NodeId(1));
            final TreeNode parent = new TreeNode(new NodeDefinition(new NodeId(1)));
            final TreeNode node = new TreeNode(start, 1, parent);
            node.isAParentNode(null);
            fail("Null param has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_isAParentNode_valid()
    {
        try
        {
            final NodeDefinition start = new NodeDefinition(new NodeId(1));
            final TreeNode parent = new TreeNode(new NodeDefinition(new NodeId(1)));
            final TreeNode node = new TreeNode(start, 1, parent);
            assertEquals(true, node.isAParentNode(start));
            assertEquals(false, node.isAParentNode(new NodeDefinition(new NodeId(2))));
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_addChildNode_nullParam()
    {
        try
        {
            final NodeDefinition start = new NodeDefinition(new NodeId(1));
            final TreeNode parent = new TreeNode(new NodeDefinition(new NodeId(1)));
            final TreeNode node = new TreeNode(start, 1, parent);
            node.addChildNode(null);
            fail("Null param has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_addChildNode_invalidNode_1()
    {
        try
        {
            final NodeDefinition start = new NodeDefinition(new NodeId(1));
            final TreeNode level_1 = new TreeNode(start);
            level_1.addChildNode(level_1);
            fail("Invalid child node has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_addChildNode_invalidNode_2()
    {
        try
        {

            final NodeDefinition def_level_1 = new NodeDefinition(new NodeId(1));
            final NodeDefinition def_level_2 = new NodeDefinition(new NodeId(2));
            final TreeNode level_1 = new TreeNode(def_level_1);
            final TreeNode level_2 = new TreeNode(def_level_2, 1, level_1);
            level_1.addChildNode(level_2);
            try {
                level_2.addChildNode(level_1);
                fail("Invalid child node has been accepted.");
            } catch (IllegalArgumentException ex)
            {
                // NOP
            }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_addChildNode_invalidNode_3()
    {
        try
        {

            final NodeDefinition def_level_1 = new NodeDefinition(new NodeId(1));
            final NodeDefinition def_level_2 = new NodeDefinition(new NodeId(2));
            final NodeDefinition def_level_3 = new NodeDefinition(new NodeId(3));
            final TreeNode level_1 = new TreeNode(def_level_1);
            final TreeNode level_2 = new TreeNode(def_level_2);            
            final TreeNode level_3 = new TreeNode(def_level_3, 1, level_2);
            try {
                level_1.addChildNode(level_3);
                fail("Invalid child node has been accepted.");
            } catch (IllegalStateException ex)
            {
                // NOP
            }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_addChildNode_valid()
    {
        try
        {
            final NodeDefinition def_level_1 = new NodeDefinition(new NodeId(1));
            final NodeDefinition def_level_2 = new NodeDefinition(new NodeId(2));
            final NodeDefinition def_level_3 = new NodeDefinition(new NodeId(3));
            final TreeNode level_1 = new TreeNode(def_level_1);
            final TreeNode level_2 = new TreeNode(def_level_2, 1, level_1);
            final TreeNode level_3 = new TreeNode(def_level_3, 1, level_2);
            level_1.addChildNode(level_2);
            level_2.addChildNode(level_3);
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    public final void test_getPath_valid()
    {
        try
        {
            final NodeDefinition def_level_1 = new NodeDefinition(new NodeId(1));
            final NodeDefinition def_level_2 = new NodeDefinition(new NodeId(2));
            final NodeDefinition def_level_3 = new NodeDefinition(new NodeId(3));
            final TreeNode level_1 = new TreeNode(def_level_1);
            final TreeNode level_2 = new TreeNode(def_level_2, 1, level_1);
            final TreeNode level_3 = new TreeNode(def_level_3, 1, level_2);
            level_1.addChildNode(level_2);
            level_2.addChildNode(level_3);
            
            TreeNodeList path = level_3.getPath();
            assertEquals(3, path.size());
            assertTrue(level_1 == path.get(0));
            assertTrue(level_2 == path.get(1));
            assertTrue(level_3 == path.get(2));
            
            
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
}
