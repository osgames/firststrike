/*
 * Created on Jul 24, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class TreeTest extends JCTestCase
{

    /**
     * @param aName
     */
    public TreeTest(final String aName)
    {
        super(aName);
    }

    public final void test_cntr_nullParam()
    {
        try
        {
            new Tree(null);
            fail("Null param has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_cntr_invalidNodeDefList()
    {
        try
        {
            new Tree(new NodeDefinitionList());
            fail("Invalid node definition list has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_getPath_nullParam1()
    {
        try
        {
            final NodeId node1 = new NodeId(1);
            final NodeId node2 = new NodeId(2);
            final NodeDefinition nodeDef1 = new NodeDefinition(node1);
            final NodeDefinition nodeDef2 = new NodeDefinition(node2);
            final NodeDefinitionList nodeDefList = new NodeDefinitionList();
            nodeDefList.add(nodeDef1);
            nodeDefList.add(nodeDef2);
            Tree tree = new Tree(new NodeDefinitionList());
            
            tree.getPath(null, nodeDef2);
            fail("Invalid node definition list has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_getPath_nullParam2()
    {
        try
        {
            final NodeId node1 = new NodeId(1);
            final NodeId node2 = new NodeId(2);
            final NodeDefinition nodeDef1 = new NodeDefinition(node1);
            final NodeDefinition nodeDef2 = new NodeDefinition(node2);
            final NodeDefinitionList nodeDefList = new NodeDefinitionList();
            nodeDefList.add(nodeDef1);
            nodeDefList.add(nodeDef2);
            Tree tree = new Tree(new NodeDefinitionList());
            
            tree.getPath(nodeDef1, null);
            fail("Invalid node definition list has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    // Avec 2 noeuds seulement r�unis entre eux
    public final void test_getPath_valid_1()
    {
        try
        {
            final NodeId node1 = new NodeId(1);
            final NodeId node2 = new NodeId(2);
            final NodeDefinition nodeDef1 = new NodeDefinition(node1);
            final NodeDefinition nodeDef2 = new NodeDefinition(node2);
            nodeDef1.addAccessibleNode(new AccessibleNode(nodeDef2, 2));
            nodeDef2.addAccessibleNode(new AccessibleNode(nodeDef1, 2));
            final NodeDefinitionList nodeDefList = new NodeDefinitionList();
            nodeDefList.add(nodeDef1);
            nodeDefList.add(nodeDef2);
            Tree tree = new Tree(nodeDefList);
            
            TreeNodeList path = tree.getPath(nodeDef1, nodeDef2);
            assertEquals(2, path.size());
            assertEquals(0, ((TreeNode)path.get(0)).getDistance());
            assertEquals(2, ((TreeNode)path.get(1)).getDistance());
            assertEquals(node1, ((TreeNode)path.get(0)).getNodeId());
            assertEquals(node2, ((TreeNode)path.get(1)).getNodeId());
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    // Avec 4 noeuds
    public final void test_getPath_valid_2()
    {
        try
        {
            final NodeId node1 = new NodeId(1);
            final NodeId node2 = new NodeId(2);
            final NodeId node3 = new NodeId(3);
            final NodeId node4 = new NodeId(4);
            final NodeDefinition nodeDef1 = new NodeDefinition(node1);
            final NodeDefinition nodeDef2 = new NodeDefinition(node2);
            final NodeDefinition nodeDef3 = new NodeDefinition(node3);
            final NodeDefinition nodeDef4 = new NodeDefinition(node4);
            nodeDef1.addAccessibleNode(new AccessibleNode(nodeDef2, 2));
            nodeDef2.addAccessibleNode(new AccessibleNode(nodeDef1, 2));
            nodeDef2.addAccessibleNode(new AccessibleNode(nodeDef3, 2));
            nodeDef3.addAccessibleNode(new AccessibleNode(nodeDef2, 2));
            nodeDef3.addAccessibleNode(new AccessibleNode(nodeDef4, 2));
            nodeDef4.addAccessibleNode(new AccessibleNode(nodeDef3, 2));
            final NodeDefinitionList nodeDefList = new NodeDefinitionList();
            nodeDefList.add(nodeDef1);
            nodeDefList.add(nodeDef2);
            nodeDefList.add(nodeDef3);
            nodeDefList.add(nodeDef4);
            Tree tree = new Tree(nodeDefList);
            
            TreeNodeList path = tree.getPath(nodeDef1, nodeDef4);
            assertEquals(4, path.size());
            assertEquals(6, ((TreeNode)path.get(3)).getDistance());
            assertEquals(node1, ((TreeNode)path.get(0)).getNodeId());
            assertEquals(node2, ((TreeNode)path.get(1)).getNodeId());
            assertEquals(node3, ((TreeNode)path.get(2)).getNodeId());
            assertEquals(node4, ((TreeNode)path.get(3)).getNodeId());
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    // Cas complexe
    public final void test_getPath_valid_3()
    {
        try
        {
            final NodeId depart = new NodeId(1);
            final NodeId node2 = new NodeId(2);
            final NodeId node3 = new NodeId(3);
            final NodeId node4 = new NodeId(4);
            final NodeId node5 = new NodeId(5);
            final NodeId node6 = new NodeId(6);
            final NodeId node7 = new NodeId(7);
            final NodeId node8 = new NodeId(8);
            final NodeId node9 = new NodeId(9);
            final NodeId destination = new NodeId(10);
            final NodeDefinition nodeDef_depart = new NodeDefinition(depart);
            final NodeDefinition nodeDef_2 = new NodeDefinition(node2);
            final NodeDefinition nodeDef_3 = new NodeDefinition(node3);
            final NodeDefinition nodeDef_4 = new NodeDefinition(node4);
            final NodeDefinition nodeDef_5 = new NodeDefinition(node5);
            final NodeDefinition nodeDef_6 = new NodeDefinition(node6);
            final NodeDefinition nodeDef_7 = new NodeDefinition(node7);
            final NodeDefinition nodeDef_8 = new NodeDefinition(node8);
            final NodeDefinition nodeDef_9 = new NodeDefinition(node9);
            final NodeDefinition nodeDef_dest = new NodeDefinition(destination);
            // Depart
            nodeDef_depart.addAccessibleNode(new AccessibleNode(nodeDef_2, 2));
            nodeDef_depart.addAccessibleNode(new AccessibleNode(nodeDef_3, 2));
            nodeDef_depart.addAccessibleNode(new AccessibleNode(nodeDef_4, 4));
            
            nodeDef_2.addAccessibleNode(new AccessibleNode(nodeDef_depart, 2));
            nodeDef_2.addAccessibleNode(new AccessibleNode(nodeDef_5, 2));
            nodeDef_2.addAccessibleNode(new AccessibleNode(nodeDef_3, 2));
            
            nodeDef_3.addAccessibleNode(new AccessibleNode(nodeDef_depart, 2));
            nodeDef_3.addAccessibleNode(new AccessibleNode(nodeDef_2, 2));
            nodeDef_3.addAccessibleNode(new AccessibleNode(nodeDef_8, 10));
            nodeDef_3.addAccessibleNode(new AccessibleNode(nodeDef_9, 2));
            
            nodeDef_4.addAccessibleNode(new AccessibleNode(nodeDef_depart, 4));
            nodeDef_4.addAccessibleNode(new AccessibleNode(nodeDef_7, 4));
            
            nodeDef_5.addAccessibleNode(new AccessibleNode(nodeDef_2, 2));
            nodeDef_5.addAccessibleNode(new AccessibleNode(nodeDef_6, 10));

            nodeDef_6.addAccessibleNode(new AccessibleNode(nodeDef_5, 10));
            nodeDef_6.addAccessibleNode(new AccessibleNode(nodeDef_dest, 3));

            nodeDef_7.addAccessibleNode(new AccessibleNode(nodeDef_4, 4));
            
            nodeDef_8.addAccessibleNode(new AccessibleNode(nodeDef_3, 10));
            nodeDef_8.addAccessibleNode(new AccessibleNode(nodeDef_dest, 4));

            nodeDef_9.addAccessibleNode(new AccessibleNode(nodeDef_3, 2));
            nodeDef_9.addAccessibleNode(new AccessibleNode(nodeDef_dest, 6));
            
            nodeDef_dest.addAccessibleNode(new AccessibleNode(nodeDef_6, 3));
            nodeDef_dest.addAccessibleNode(new AccessibleNode(nodeDef_8, 4));
            nodeDef_dest.addAccessibleNode(new AccessibleNode(nodeDef_9, 6));

            final NodeDefinitionList nodeDefList = new NodeDefinitionList();
            nodeDefList.add(nodeDef_depart);
            nodeDefList.add(nodeDef_2);
            nodeDefList.add(nodeDef_3);
            nodeDefList.add(nodeDef_4);
            nodeDefList.add(nodeDef_5);
            nodeDefList.add(nodeDef_6);
            nodeDefList.add(nodeDef_7);
            nodeDefList.add(nodeDef_8);
            nodeDefList.add(nodeDef_9);
            nodeDefList.add(nodeDef_dest);
            Tree tree = new Tree(nodeDefList);
            
            TreeNodeList path = tree.getPath(nodeDef_depart, nodeDef_dest);
            assertEquals(4, path.size());
            assertEquals(10, ((TreeNode)path.get(3)).getDistance());
            assertEquals(depart, ((TreeNode)path.get(0)).getNodeId());
            assertEquals(node3, ((TreeNode)path.get(1)).getNodeId());
            assertEquals(node9, ((TreeNode)path.get(2)).getNodeId());
            assertEquals(destination, ((TreeNode)path.get(3)).getNodeId());
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
}
