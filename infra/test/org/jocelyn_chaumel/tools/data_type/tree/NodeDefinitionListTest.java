/*
 * Created on Jul 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class NodeDefinitionListTest extends JCTestCase
{

    /**
     * @param aName
     */
    public NodeDefinitionListTest(final String aName)
    {
        super(aName);
    }

    public final void test_getNodeById_nullParam()
    {
        try
        {
            NodeDefinitionList list = new NodeDefinitionList();
            list.getNodeById(null);
            fail("Null param has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_getNodeById_invalidId()
    {
        try
        {
            NodeDefinitionList list = new NodeDefinitionList();
            list.getNodeById(new NodeId(1));
            fail("Invalid node id has been accepted.");
        } catch (IllegalStateException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_isAValidNode_nullParam()
    {
        try
        {
            NodeDefinitionList list = new NodeDefinitionList();
            list.isAValidNode(null);
            fail("Null param has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_isAValidNode_invalidId()
    {
        try
        {
            NodeDefinitionList list = new NodeDefinitionList();
            final NodeId node1 = new NodeId(1);
            final NodeDefinition nodeDef1 = new NodeDefinition(node1);
            assertEquals(false, list.isAValidNode(node1));
            
            list.add(nodeDef1);
            assertEquals(true, list.isAValidNode(node1));

        } catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    public final void test_getNodeById_valid()
    {
        try
        {
            final NodeDefinitionList list = new NodeDefinitionList();
            final NodeId node1 = new NodeId(1);
            final NodeId node2 = new NodeId(2);
            final NodeDefinition nodeDef1 = new NodeDefinition(node1);
            final NodeDefinition nodeDef2 = new NodeDefinition(node2);
            
            list.add(nodeDef1);
            assertEquals(nodeDef1, list.getNodeById(node1));
            
            list.add(nodeDef2);
            assertEquals(nodeDef2, list.getNodeById(node2));
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    public final void test_getNodeByRef_valid()
    {
        try
        {
            final NodeDefinitionList list = new NodeDefinitionList();
            final NodeId node1 = new NodeId(1);
            final NodeId node2 = new NodeId(2);
            final Integer objectRef1 = new Integer(1);
            final Integer objectRef2 = new Integer(2);
            final NodeDefinition nodeDef1 = new NodeDefinition(node1, objectRef1);
            final NodeDefinition nodeDef2 = new NodeDefinition(node2, objectRef2);
            
            list.add(nodeDef1);
            assertEquals(nodeDef1, list.getNodeByRef(objectRef1));
            
            list.add(nodeDef2);
            assertEquals(nodeDef2, list.getNodeByRef(objectRef2));
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
}
