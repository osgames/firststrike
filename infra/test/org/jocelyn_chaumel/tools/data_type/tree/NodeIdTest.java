/*
 * Created on Aug 6, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

import org.jocelyn_chaumel.tools.data_type.Id;
import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class NodeIdTest extends JCTestCase
{

    /**
     * @param aName
     */
    public NodeIdTest(final String aName)
    {
        super(aName);
    }
    
    public final void test_equals_valid()
    {
        try {
            final NodeId id = new NodeId(1);
            
            assertEquals(false, id.equals(null));
            assertEquals(false, id.equals(new Id(1)));
            assertEquals(false, id.equals(new NodeId(2)));
            assertEquals(true, id.equals(new NodeId(1)));
            
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

}
