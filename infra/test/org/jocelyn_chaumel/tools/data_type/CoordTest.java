/*
 * Created on Jan 25, 2005
 */
package org.jocelyn_chaumel.tools.data_type;

import java.util.ArrayList;
import java.util.Collections;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class CoordTest extends JCTestCase
{

    /**
     * @param aName
     */
    public CoordTest(final String aName)
    {
        super(aName);
    }

    public void testCalculateDistance_nullParam()
    {
        try
        {
            Coord coord = new Coord();
            coord.calculateDistanceDouble(null);
            fail("Null param has been accepted");
        }
        catch (IllegalArgumentException ex)
        {
            // NOP
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public void test_calculateDelta_nullParam()
    {
        try
        {
            final Coord coord = new Coord();
            coord.calculateDelta(null);
            fail("Null param has been accepted");
        }
        catch (IllegalArgumentException ex)
        {
            // NOP
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public void test_calculateDelta_valid()
    {
        try
        {
            Coord coord = new Coord(0, 0);
            assertEquals(1, coord.calculateDelta(new Coord(1, 1)));
            assertEquals(0, coord.calculateDelta(new Coord(2, 0)));
            assertEquals(1, coord.calculateDelta(new Coord(4, 1)));
            assertEquals(2, coord.calculateDelta(new Coord(3, 2)));
            assertEquals(1, coord.calculateDelta(new Coord(3, 1)));
            assertEquals(0, coord.calculateDelta(new Coord(3, 0)));
            assertEquals(0, coord.calculateDelta(new Coord(0, 2)));
            assertEquals(0, coord.calculateDelta(new Coord(0, -2)));
            assertEquals(0, coord.calculateDelta(new Coord(-2, 0)));
            assertEquals(0, coord.calculateDelta(new Coord(-1, 0)));

            coord.setX(-2);
            assertEquals(1, coord.calculateDelta(new Coord(1, 1)));
            assertEquals(0, coord.calculateDelta(new Coord(2, 0)));
            assertEquals(2, coord.calculateDelta(new Coord(0, 2)));
            assertEquals(0, coord.calculateDelta(new Coord(-1, 0)));
            assertEquals(0, coord.calculateDelta(new Coord(-2, 0)));

            coord.setY(-2);
            assertEquals(3, coord.calculateDelta(new Coord(1, 1)));
            assertEquals(2, coord.calculateDelta(new Coord(2, 0)));
            assertEquals(2, coord.calculateDelta(new Coord(0, 2)));
            assertEquals(1, coord.calculateDelta(new Coord(-1, 0)));
            assertEquals(0, coord.calculateDelta(new Coord(-2, 0)));
            assertEquals(0, coord.calculateDelta(new Coord(-2, -1)));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public void testCalculateIntDistance_valid()
    {
        try
        {
            Coord coord = new Coord(2, 2);
            assertEquals(0, coord.calculateIntDistance(new Coord(2, 2)));
            assertEquals(2, coord.calculateIntDistance(new Coord(2, 0)));
            assertEquals(2, coord.calculateIntDistance(new Coord(0, 2)));
            assertEquals(2, coord.calculateIntDistance(new Coord(0, 0)));
            assertEquals(4, coord.calculateIntDistance(new Coord(0, -2)));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public void testCalculateDistance_valid()
    {
        try
        {
            Coord coord = new Coord(2, 2);
            assertEquals(0, coord.calculateDistanceDouble(new Coord(2, 2)), 0.00001d);
            assertEquals(2, coord.calculateDistanceDouble(new Coord(2, 0)), 0.00001d);
            assertEquals(2, coord.calculateDistanceDouble(new Coord(0, 2)), 0.00001d);
            assertEquals(2.66, coord.calculateDistanceDouble(new Coord(0, 0)), 0.02d);
            assertEquals(2.33, coord.calculateDistanceDouble(new Coord(3, 4)), 0.02d);
            assertEquals(8.88, coord.calculateDistanceDouble(new Coord(10, 10)), 0.02d);
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_isInRange_nullParam()
    {
        try
        {
            Coord coord = new Coord(1, 1);
            coord.isInRange(null, 2);
            fail("Null param has been accepted.");
        }
        catch (IllegalArgumentException ex)
        {
            // NOP
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_isInRange_invalidRange()
    {
        try
        {
            Coord coord = new Coord(1, 1);
            coord.isInRange(new Coord(1, 1), -1);
            fail("Invalid Range has been accepted.");
        }
        catch (IllegalArgumentException ex)
        {
            // NOP
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_isInRange_valid()
    {
        try
        {
            Coord coord = new Coord(1, 1);
            assertEquals(true, coord.isInRange(new Coord(1, 1), 1));
            assertEquals(true, coord.isInRange(new Coord(0, 0), 1));
            assertEquals(true, coord.isInRange(new Coord(1, 2), 1));
            assertEquals(true, coord.isInRange(new Coord(2, 1), 1));
            assertEquals(true, coord.isInRange(new Coord(2, 2), 1));
            assertEquals(false, coord.isInRange(new Coord(1, 3), 1));
            assertEquals(false, coord.isInRange(new Coord(3, 1), 1));
            assertEquals(false, coord.isInRange(new Coord(3, 3), 1));
            assertEquals(true, coord.isInRange(new Coord(3, 3), 2));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_compareTo_invalidInstance_valid()
    {
        try
        {
            final Coord coord = new Coord(1, 1);
            assertEquals(1, coord.compareTo(new Integer(1)));

        }
        catch (IllegalArgumentException ex)
        {
            // NOP;
        }
    }

    public final void test_compareTo_valid()
    {
        final Coord coord_01_01 = new Coord(1, 1);
        final Coord coord_01_02 = new Coord(1, 2);
        final Coord coord_01_03 = new Coord(1, 3);
        final Coord coord_02_01 = new Coord(2, 1);
        final Coord coord_02_02 = new Coord(2, 2);
        final Coord coord_02_03 = new Coord(2, 3);
        final Coord coord_03_01 = new Coord(3, 1);
        final Coord coord_03_02 = new Coord(3, 2);
        final Coord coord_03_03 = new Coord(3, 3);

        assertTrue(coord_01_01.compareTo(coord_01_01) == 0);
        assertTrue(coord_01_01.compareTo(coord_02_01) < 0);
        assertTrue(coord_02_01.compareTo(coord_01_01) > 0);

        assertTrue(coord_01_01.compareTo(coord_01_01) == 0);
        assertTrue(coord_01_01.compareTo(coord_01_02) < 0);
        assertTrue(coord_01_02.compareTo(coord_01_01) > 0);

        ArrayList<Coord> list = new ArrayList<Coord>();
        list.add(coord_02_01);
        list.add(coord_03_01);
        list.add(coord_01_03);
        list.add(coord_01_02);
        list.add(coord_02_02);
        list.add(coord_03_02);
        list.add(coord_02_03);
        list.add(coord_01_01);
        list.add(coord_03_03);
        Collections.sort(list);
        assertEquals(list.get(0), coord_01_01);
        assertEquals(list.get(1), coord_02_01);
        assertEquals(list.get(2), coord_03_01);
        assertEquals(list.get(3), coord_01_02);
        assertEquals(list.get(4), coord_02_02);
        assertEquals(list.get(5), coord_03_02);
        assertEquals(list.get(6), coord_01_03);
        assertEquals(list.get(7), coord_02_03);
        assertEquals(list.get(8), coord_03_03);
    }

    public final void getCoord_valid()
    {
        try
        {
            assertNull(Coord.getCoord(null));
            assertEquals(new Coord(1,2), Coord.getCoord("1,2"));
            assertEquals(new Coord(-1,2), Coord.getCoord("-1,2"));
            assertEquals(new Coord(1,-2), Coord.getCoord("1,-2"));
            assertEquals(new Coord(1,2), Coord.getCoord("(1,2)"));
            assertEquals(new Coord(1,2), Coord.getCoord("[1,2]"));
            assertEquals(new Coord(1,2), Coord.getCoord("{1,2}"));
            assertEquals(new Coord(1,2), Coord.getCoord(new Coord(1,2).toString()));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }
    
    public final void test_getOrientation_all(){
        try
        {
            final Coord coord = new Coord(1,1);
            
            assertEquals(Coord.NORTH, coord.getOrientation(new Coord(1,0)));
            assertEquals(Coord.NORTH_EAST, coord.getOrientation(new Coord(2,0)));
            assertEquals(Coord.EAST, coord.getOrientation(new Coord(2,1)));
            assertEquals(Coord.SOUHT_EAST, coord.getOrientation(new Coord(2,2)));
            assertEquals(Coord.SOUHT, coord.getOrientation(new Coord(1,2)));
            assertEquals(Coord.SOUHT_WEST, coord.getOrientation(new Coord(0,2)));
            assertEquals(Coord.WEST, coord.getOrientation(new Coord(0,1)));
            assertEquals(Coord.NORTH_WEST, coord.getOrientation(new Coord(0,0)));
            
            try {
                coord.getOrientation(null);
                fail("Null param has been accepted");
            } catch (IllegalArgumentException ex){
                // NOP
            }
            
            try {
                coord.getOrientation(new Coord(3,0));
                fail("Invalid param has been accepted");
            } catch (IllegalArgumentException ex){
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }
}
