/*
 * Created on Jun 4, 2005
 */
package org.jocelyn_chaumel.tools.data_type;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class FloatListTest extends JCTestCase
{

    /**
     * @param aName
     */
    public FloatListTest(final String aName)
    {
        super(aName);
    }
    
    public final void test_Cntr_nullParam(){
        try
        {
            new FloatList((float []) null);
            fail("Null param has been accepted.");
        } catch (IllegalArgumentException ex)
        {
            // NOP
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

}
