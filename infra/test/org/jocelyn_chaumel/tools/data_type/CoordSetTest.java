package org.jocelyn_chaumel.tools.data_type;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

public class CoordSetTest extends JCTestCase
{

	public CoordSetTest(final String p_name)
	{
		super(p_name);
	}
	
	public void test_getNearestCoord_Coord_all(){
		try {
			final CoordSet coordSet = new CoordSet();
			coordSet.add(new Coord(1,1));
			
			try {
				coordSet.getDirectNearestCoord((Coord) null);
				fail("Null param has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}
			coordSet.clear();
			try {
				coordSet.getDirectNearestCoord(new Coord(0,0));
				fail("Empty coordset has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}
			
			coordSet.add(new Coord (5,0));
			assertEquals( new Coord(5,0),  coordSet.getDirectNearestCoord(new Coord(0,0)));
			
			
			coordSet.add(new Coord (5,1));
			assertEquals( new Coord(5,0),  coordSet.getDirectNearestCoord(new Coord(0,0)));
			assertEquals( new Coord(5,1),  coordSet.getDirectNearestCoord(new Coord(0,1)));
			assertEquals( new Coord(5,1),  coordSet.getDirectNearestCoord(new Coord(3,2)));
			
			coordSet.add(new Coord (5,2));
			assertEquals( new Coord(5,0),  coordSet.getDirectNearestCoord(new Coord(0,0)));
			assertEquals( new Coord(5,1),  coordSet.getDirectNearestCoord(new Coord(0,1)));
			assertEquals( new Coord(5,2),  coordSet.getDirectNearestCoord(new Coord(0,2)));
		} catch (Exception ex){
			fail(ex);
		}
	}

	
	public void test_getNearestCoord_CoordSet_all(){
		try {
			final CoordSet coordSet = new CoordSet();
			coordSet.add(new Coord(1,1));
			try {
				coordSet.getDirectNearestCoord((CoordSet) null);
				fail("Null param has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}
			
			try {
				coordSet.getDirectNearestCoord(new CoordSet());
				fail("Empty coordset param has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}

			try {
				new CoordSet().getDirectNearestCoord(coordSet);
				fail("Empty coordset has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}
			coordSet.clear();
			

			coordSet.add(new Coord (5,0));
			final CoordSet coordSetParam = new CoordSet();
			coordSetParam.add(new Coord(0,0));
			CoupleCoord couple = coordSet.getDirectNearestCoord(coordSetParam);
			assertEquals( new Coord(5,0),  couple.getFirstCoord());
			assertEquals( new Coord(0,0),  couple.getSecondCoord());
			
			
			coordSet.add(new Coord (6,1));
			couple = coordSet.getDirectNearestCoord(coordSetParam);
			assertEquals( new Coord(5,0),  couple.getFirstCoord());
			assertEquals( new Coord(0,0),  couple.getSecondCoord());
			
		} catch (Exception ex){
			fail(ex);
		}
	}
	
	public void test_clone_all(){
		try {
			final CoordSet coordSet = new CoordSet();
			coordSet.add(new Coord(1,1));
			
			CoordSet clonedCoordSet = (CoordSet) coordSet.clone();
			assertEquals(1, clonedCoordSet.size());
			assertEquals(1, coordSet.size());
			assertTrue(clonedCoordSet.contains(new Coord(1,1)));
			assertFalse(clonedCoordSet.contains(new Coord(0,0)));
			
			clonedCoordSet.clear();
			assertEquals(0, clonedCoordSet.size());
			assertEquals(1, coordSet.size());
			
		} catch (Exception ex){
			fail(ex);
		}
	}
	
	public void test_getExtendedCoordSet_1param_all() {
		try {
			
			try {
				new CoordSet().getExtendedCoordSet(10);
				fail();
			} catch (IllegalStateException ex) {
				// NOP
			}
			
			final CoordSet coordSet = new CoordSet();
			CoordSet coordSetResult = null;
			coordSet.add(new Coord(3,3));
			try {
				coordSet.getExtendedCoordSet(0);
				fail();
			} catch (IllegalArgumentException ex) {
				// NOP
			}
			
			coordSetResult = coordSet.getExtendedCoordSet(1);
			assertEquals(9, coordSetResult.size());

			coordSet.add(new Coord(4,4));
			coordSetResult = coordSet.getExtendedCoordSet(1);
			assertEquals(14, coordSetResult.size());

			coordSet.add(new Coord(4,3));
			coordSetResult = coordSet.getExtendedCoordSet(1);
			assertEquals(15, coordSetResult.size());

			coordSetResult = coordSet.getExtendedCoordSet(2);
			assertEquals(35, coordSetResult.size());

		} catch(Exception ex) {
			fail(ex);
		}
	}
	
	public void test_getExtendedCoordSet_3params_all(){
		try {
			final CoordSet coordSet = new CoordSet();
			coordSet.add(new Coord(3,3));
			
			CoordSet result = coordSet.getExtendedCoordSet(1, 10, 10);
			assertEquals(9, result.size());
			assertTrue(result.contains(new Coord(2,2)));
			assertTrue(result.contains(new Coord(4,4)));
			assertTrue(result.contains(new Coord(2,4)));
			assertTrue(result.contains(new Coord(4,2)));
			
			coordSet.add(new Coord(5,3));
			
			result = coordSet.getExtendedCoordSet(1, 10, 10);
			assertEquals(15, result.size());
			assertTrue(result.contains(new Coord(2,2)));
			assertTrue(result.contains(new Coord(6,4)));
			assertTrue(result.contains(new Coord(4,3)));
			assertTrue(result.contains(new Coord(2,4)));
			assertTrue(result.contains(new Coord(6,2)));
			
			coordSet.add(new Coord(0,0));
			
			result = coordSet.getExtendedCoordSet(1, 10, 10);
			assertEquals(19, result.size());
			assertTrue(result.contains(new Coord(0,0)));
			assertTrue(result.contains(new Coord(1,0)));
			assertTrue(result.contains(new Coord(0,1)));
			assertTrue(result.contains(new Coord(1,1)));
			
			assertTrue(result.contains(new Coord(2,2)));
			assertTrue(result.contains(new Coord(6,4)));
			assertTrue(result.contains(new Coord(4,3)));
			assertTrue(result.contains(new Coord(2,4)));
			assertTrue(result.contains(new Coord(6,2)));

			try {
				coordSet.getExtendedCoordSet(-1, 10, 10);
				fail("Invalid range has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}

			try {
				coordSet.getExtendedCoordSet(1, -10, 10);
				fail("Invalid max X has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}
			

			try {
				coordSet.getExtendedCoordSet(1, 10, -10);
				fail("Invalid max Y has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}
			
			coordSet.clear();

			try {
				coordSet.getExtendedCoordSet(1, 10, 10);
				fail("Empty coord set has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}
		} catch (Exception ex){
			fail(ex);
		}
	}

	public void test_createInitiazedCoordSet_all(){
		try {
			final CoordSet coordSet = new CoordSet();
			coordSet.add(new Coord(3,3));
			
			CoordSet result = CoordSet.createInitiazedCoordSet(new Coord(3,3), 1, 5, 5);
			assertEquals(9, result.size());
			assertTrue(result.contains(new Coord(2,2)));
			assertTrue(result.contains(new Coord(4,4)));
			assertTrue(result.contains(new Coord(2,4)));
			assertTrue(result.contains(new Coord(4,2)));
			
			result = CoordSet.createInitiazedCoordSet(new Coord(3,3), 1, 4,4);
			assertEquals(4, result.size());
			assertTrue(result.contains(new Coord(2,2)));
			assertTrue(result.contains(new Coord(3,3)));
			assertTrue(result.contains(new Coord(2,3)));
			assertTrue(result.contains(new Coord(3,2)));

			result = CoordSet.createInitiazedCoordSet(new Coord(3,3), 2, 4,4);
			assertEquals(9, result.size());
			assertTrue(result.contains(new Coord(1,1)));
			assertTrue(result.contains(new Coord(3,3)));
			assertTrue(result.contains(new Coord(1,3)));
			assertTrue(result.contains(new Coord(3,1)));
			

			result = CoordSet.createInitiazedCoordSet(new Coord(3,3), 7, 4,4);
			assertEquals(16, result.size());
			assertTrue(result.contains(new Coord(0,0)));
			assertTrue(result.contains(new Coord(3,3)));
			assertTrue(result.contains(new Coord(0,3)));
			assertTrue(result.contains(new Coord(3,0)));
			
			try {
				CoordSet.createInitiazedCoordSet(null, 1, 10, 10);
				fail("Null coord has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}

			try {
				CoordSet.createInitiazedCoordSet(new Coord(1,1), -1, 10, 10);
				fail("Invalid range has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}

			try {
				CoordSet.createInitiazedCoordSet(new Coord(1,1), 1, -10, 10);
				fail("Invalid width has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}

			try {
				CoordSet.createInitiazedCoordSet(new Coord(1,1), 1, 10, -10);
				fail("Invalid height has been accepted");
			} catch(IllegalArgumentException ex){
				// NOP
			}
		} catch (Exception ex){
			fail(ex);
		}
	}

	public void test_getMinX_all(){
		try {
			final CoordSet coordSet = new CoordSet();
			assertEquals(Integer.MAX_VALUE,  coordSet.getMinX());
			coordSet.add(new Coord(3,3));
			assertEquals(3,  coordSet.getMinX());
			
			coordSet.add(new Coord(3,2));
			assertEquals(3,  coordSet.getMinX());
			
			coordSet.add(new Coord(4,2));
			assertEquals(3,  coordSet.getMinX());
			
			coordSet.add(new Coord(1,2));
			assertEquals(1,  coordSet.getMinX());
			
		} catch (Exception ex){
			fail(ex);
		}
	}

	public void test_getMinY_all(){
		try {
			final CoordSet coordSet = new CoordSet();
			assertEquals(Integer.MAX_VALUE,  coordSet.getMinY());
			coordSet.add(new Coord(3, 3));
			assertEquals(3,  coordSet.getMinY());
			
			coordSet.add(new Coord(2, 3));
			assertEquals(3,  coordSet.getMinY());
			
			coordSet.add(new Coord(2, 4));
			assertEquals(3,  coordSet.getMinY());
			
			coordSet.add(new Coord(2, 1));
			assertEquals(1,  coordSet.getMinY());
			
		} catch (Exception ex){
			fail(ex);
		}
	}

	public void test_getMaxX_all(){
		try {
			final CoordSet coordSet = new CoordSet();
			assertEquals(Integer.MIN_VALUE,  coordSet.getMaxX());
			coordSet.add(new Coord(3,3));
			assertEquals(3,  coordSet.getMaxX());
			
			coordSet.add(new Coord(3,4));
			assertEquals(3,  coordSet.getMaxX());
			
			coordSet.add(new Coord(2,2));
			assertEquals(3,  coordSet.getMaxX());
			
			coordSet.add(new Coord(4,2));
			assertEquals(4,  coordSet.getMaxX());
			
		} catch (Exception ex){
			fail(ex);
		}
	}

	public void test_getMaxY_all(){
		try {
			final CoordSet coordSet = new CoordSet();
			assertEquals(Integer.MIN_VALUE,  coordSet.getMaxY());
			coordSet.add(new Coord(3, 3));
			assertEquals(3,  coordSet.getMaxY());
			
			coordSet.add(new Coord(2, 3));
			assertEquals(3,  coordSet.getMaxY());
			
			coordSet.add(new Coord(2, 2));
			assertEquals(3,  coordSet.getMaxY());
			
			coordSet.add(new Coord(2, 4));
			assertEquals(4,  coordSet.getMaxY());
			
		} catch (Exception ex){
			fail(ex);
		}
	}
	
	public void test_getLimitedCoordSet_all(){
		try {
			final CoordSet coordSet = new CoordSet();
			assertEquals(0,  coordSet.getLimitedCoordSet(new Coord(), 1).size());
			
			coordSet.add(new Coord(3, 3));
			assertEquals(0,  coordSet.getLimitedCoordSet(new Coord(1,1), 1).size());
			assertEquals(1,  coordSet.getLimitedCoordSet(new Coord(1,1), 2).size());

			coordSet.add(new Coord(4, 4));
			assertEquals(0,  coordSet.getLimitedCoordSet(new Coord(1,1), 1).size());
			assertEquals(1,  coordSet.getLimitedCoordSet(new Coord(1,1), 2).size());
			assertEquals(2,  coordSet.getLimitedCoordSet(new Coord(1,1), 3).size());
			
		} catch (Exception ex){
			fail(ex);
		}
	}
	
	public void test_getMinMaxXYCoords_all()
	{
		try {
			final CoordSet coordSet = new CoordSet();
			assertEquals(0,  coordSet.getMinXCoords().size());
			assertEquals(0,  coordSet.getMaxXCoords().size());
			assertEquals(0,  coordSet.getMinYCoords().size());
			assertEquals(0,  coordSet.getMaxYCoords().size());
			
			
			
			coordSet.add(new Coord(3, 3));
			assertEquals(1,  coordSet.getMinXCoords().size());
			assertTrue(coordSet.getMinXCoords().contains(new Coord(3, 3)));
			assertEquals(1,  coordSet.getMaxXCoords().size());
			assertTrue(coordSet.getMaxXCoords().contains(new Coord(3, 3)));
			
			assertEquals(1,  coordSet.getMinYCoords().size());
			assertTrue(coordSet.getMinYCoords().contains(new Coord(3, 3)));
			assertEquals(1,  coordSet.getMaxYCoords().size());
			assertTrue(coordSet.getMaxYCoords().contains(new Coord(3, 3)));

			
			
			coordSet.add(new Coord(4, 4));
			assertEquals(1,  coordSet.getMinXCoords().size());
			assertTrue(coordSet.getMinXCoords().contains(new Coord(3, 3)));
			assertEquals(1,  coordSet.getMaxXCoords().size());
			assertTrue(coordSet.getMaxXCoords().contains(new Coord(4, 4)));

			assertEquals(1,  coordSet.getMinYCoords().size());
			assertTrue(coordSet.getMinYCoords().contains(new Coord(3, 3)));
			assertEquals(1,  coordSet.getMaxYCoords().size());
			assertTrue(coordSet.getMaxYCoords().contains(new Coord(4, 4)));
			
			
			coordSet.add(new Coord(3, 4));
			assertEquals(2,  coordSet.getMinXCoords().size());
			assertTrue(coordSet.getMinXCoords().contains(new Coord(3, 3)));
			assertTrue(coordSet.getMinXCoords().contains(new Coord(3, 4)));

			assertEquals(1,  coordSet.getMinYCoords().size());
			assertTrue(coordSet.getMinYCoords().contains(new Coord(3, 3)));
			assertEquals(2,  coordSet.getMaxYCoords().size());
			assertTrue(coordSet.getMaxYCoords().contains(new Coord(4, 4)));
			assertTrue(coordSet.getMaxYCoords().contains(new Coord(3, 4)));
			
			
			coordSet.add(new Coord(4, 3));
			assertEquals(2,  coordSet.getMaxXCoords().size());
			assertTrue(coordSet.getMaxXCoords().contains(new Coord(4, 4)));
			assertTrue(coordSet.getMaxXCoords().contains(new Coord(4, 3)));

			assertEquals(2,  coordSet.getMinYCoords().size());
			assertTrue(coordSet.getMinYCoords().contains(new Coord(3, 3)));
			assertTrue(coordSet.getMinYCoords().contains(new Coord(4, 3)));
			assertEquals(2,  coordSet.getMaxYCoords().size());
			assertTrue(coordSet.getMaxYCoords().contains(new Coord(4, 4)));
			assertTrue(coordSet.getMaxYCoords().contains(new Coord(3, 4)));
			
		} catch (Exception ex){
			fail(ex);
		}
	}
}
