package org.jocelyn_chaumel.tools.data_type;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

public class AbstractInstanceArrayListTest extends JCTestCase
{

    public AbstractInstanceArrayListTest(final String p_name)
    {
        super(p_name);
    }
    
    public final void test_getRandomItem_valid()
    {
        InstanceArrayList list = new InstanceArrayList();
        list.add(new Integer(1));
        list.add(new Integer(2));
        list.add(new Integer(3));
        list.add(new Integer(4));
        Integer item = (Integer) list.getRandomItem();
        assertTrue(item.intValue() >= 1 && item.intValue() <= 4);
        assertEquals(4, list.size());

        item = (Integer) list.getRandomItem();
        assertTrue(item.intValue() >= 1 && item.intValue() <= 4);
        assertEquals(4, list.size());

        item = (Integer) list.getRandomItem();
        assertTrue(item.intValue() >= 1 && item.intValue() <= 4);
        assertEquals(4, list.size());

        item = (Integer) list.getRandomItem();
        assertTrue(item.intValue() >= 1 && item.intValue() <= 4);
        assertEquals(4, list.size());
    }

    
    public final void test_removeRandomItem_valid()
    {
        InstanceArrayList list = new InstanceArrayList();
        list.add(new Integer(1));
        list.add(new Integer(2));
        list.add(new Integer(3));
        list.add(new Integer(4));
        assertEquals(4, list.size());
        Integer item = (Integer) list.removeRandomItem();
        assertTrue(item.intValue() >= 1 && item.intValue() <= 4);
        assertEquals(3, list.size());

        item = (Integer) list.removeRandomItem();
        assertTrue(item.intValue() >= 1 && item.intValue() <= 4);
        assertEquals(2, list.size());
        
        item = (Integer) list.removeRandomItem();
        assertTrue(item.intValue() >= 1 && item.intValue() <= 4);
        assertEquals(1, list.size());
        
        item = (Integer) list.removeRandomItem();
        assertTrue(item.intValue() >= 1 && item.intValue() <= 4);
        assertEquals(0, list.size());
    }
    
    private class InstanceArrayList extends AbstractInstanceArrayList{

        public InstanceArrayList()
        {
            super(Integer.class, "Private Integer List");
        }
        
    }
}
