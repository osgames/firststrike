/*
 * Created on Jan 25, 2005
 */
package org.jocelyn_chaumel.tools.data_type.cst;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class IntCstTest extends JCTestCase
{

    /**
     * @param aName
     */
    public IntCstTest(final String aName)
    {
        super(aName);
    }
    
    public final void test_equals_all(){
        try {
            IntTestCst cst_1 = new IntTestCst(new Integer(1));
            IntTestCst cst_2 = new IntTestCst(new Integer(1));
            assertEquals(cst_1, cst_2);
        } catch (Exception ex) {
            fail(ex);
        }
    }
    
    class IntTestCst extends IntCst {

        protected IntTestCst(int p_value)
        {
            super(p_value);
        } 
        
        public IntTestCst (Integer p_int){
            super(p_int.intValue());
        }
        
    };
}
