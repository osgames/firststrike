package org.jocelyn_chaumel.tools.data_type;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

public class CoordHashtableTest extends JCTestCase
{
    public CoordHashtableTest(final String aName)
    {
        super(aName);
    }

    public void test_removeAll_nullParam()
    {
        try
        {
            final CoordHashtable coordHashtable = new CoordHashtable();
            try
            {
                coordHashtable.removeAll(null);
                fail("Null param has been accepted");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public void test_removeAll_valid()
    {
        try
        {
            final CoordHashtable coordHashtable = new CoordHashtable();
            final CoordSet coordSet = new CoordSet();
            coordSet.add(new Coord(1,1));
            
            coordHashtable.removeAll(coordSet);
            assertTrue(coordHashtable.size() == 0);
            
            coordHashtable.put(new Coord(1,1), "");
            assertTrue(coordHashtable.size() == 1);
            coordHashtable.removeAll(coordSet);
            assertTrue(coordHashtable.size() == 0);
            
            coordHashtable.put(new Coord(1,1), "cell1");
            coordHashtable.put(new Coord(1,1), "cell1");
            coordHashtable.put(new Coord(1,2), "cell2");
            coordHashtable.put(new Coord(1,3), "cell3");
            assertTrue(coordHashtable.size() == 3);
            coordHashtable.removeAll(coordSet);
            assertTrue(coordHashtable.size() == 2);
            coordHashtable.removeAll(coordSet);
            assertTrue(coordHashtable.size() == 2);
            
            coordSet.add(new Coord(1,2));
            coordSet.add(new Coord(1,3));
            coordHashtable.removeAll(coordSet);
            assertTrue(coordHashtable.size() == 0);
            
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }
}
