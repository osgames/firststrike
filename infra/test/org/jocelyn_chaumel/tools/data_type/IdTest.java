/*
 * Created on Jan 25, 2005
 */
package org.jocelyn_chaumel.tools.data_type;

import java.util.ArrayList;
import java.util.Collections;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class IdTest extends JCTestCase
{

    /**
     * @param aName
     */
    public IdTest(final String aName)
    {
        super(aName);
    }

    public void test_equals_valid()
    {
        final Id id = new Id(1);
        try
        {
            assertFalse(id.equals(null));
            assertFalse(id.equals(new Integer(1)));
            assertFalse(id.equals(new Id(2)));
            assertTrue(id.equals(new Id(1)));
            assertTrue(id.equals(id));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public void test_comparable_invalidInstance()
    {
        final Id id = new Id(1);
        try
        {
            id.compareTo(new Integer(1));
            fail("Invalid object has been accepted");
        }
        catch (IllegalArgumentException ex)
        {
            // NOP
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public void test_comparable_valid()
    {
        try
        {
            final Id id_1 = new Id(1);
            final Id id_2 = new Id(2);
            final Id id_3 = new Id(3);
            final Id id_4 = new Id(4);
            
            assertEquals(1, id_2.compareTo(null));
            assertEquals(1, id_2.compareTo(new Id(1)));
            assertEquals(0, id_2.compareTo(new Id(2)));
            assertEquals(-1, id_2.compareTo(new Id(3)));
            
            final ArrayList<Id> list = new ArrayList<Id>();
            list.add(id_2);
            list.add(id_1);
            list.add(id_4);
            list.add(id_3);
            Collections.sort(list);
            
            assertEquals(id_1, list.get(0));
            assertEquals(id_2, list.get(1));
            assertEquals(id_3, list.get(2));
            assertEquals(id_4, list.get(3));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }
}
