package org.jocelyn_chaumel.tools.zip;

import java.io.File;
import java.io.IOException;

import org.jocelyn_chaumel.tools.FileMgr;

import junit.framework.TestCase;

public class ZipMgrTest extends TestCase
{

	public void testExtractZip()
	{
		final File zipSourceFile = new File("bin/zip-with-sub-folder.zip");
		final File zipDestinationFile = new File("c:/temp/extractZipTest");
		assertTrue(zipSourceFile.isFile());
		try
		{
			FileMgr.forceDeleteCascadeIfPresent(zipDestinationFile);
			ZipMgr.extractZip(zipSourceFile, zipDestinationFile, false);
		} catch (IOException ex)
		{
			fail(ex.getMessage());
		}
		
	}

}
