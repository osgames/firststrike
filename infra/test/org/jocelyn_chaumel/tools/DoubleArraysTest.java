/*
 * DoubleArraysTest.java
 * JUnit based test
 *
 * Created on 17 mars 2003, 22:32
 */

package org.jocelyn_chaumel.tools;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * 
 * @author Serveur_Showmel
 */
public class DoubleArraysTest extends JCTestCase
{
    public DoubleArraysTest(final String aName)
    {
        super(aName);
    }

    public void testEquals_IntInt_NullParm_valid()
    {
        try
        {
            int[][] array1 = new int[0][0];
            assertTrue("Equals array hasn't been accepted.", DoubleArrays
                .equals(null, null));
            assertTrue("Non-equals array has been accepted.", !DoubleArrays
                .equals(array1, null));
            assertTrue("Non-equals array has been accepted.", !DoubleArrays
                .equals(null, array1));
        } catch (Exception ex)
        {
            ex.printStackTrace();
            fail();
        }
    }

    public void testEquals_IntInt_InvalidArray_valid()
    {
        try
        {
            int[][] array1 = { { 1, 2 }, { 3, 4 } };
            int[][] array2 = { { 1, 2 }, { 3, 5 } };
            assertTrue("Non-equals array has been accepted.", !DoubleArrays
                .equals(array1, array2));
        } catch (Exception ex)
        {
            ex.printStackTrace();
            fail();
        }
    }
}