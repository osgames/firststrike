package org.jocelyn_chaumel.tools.filling;

import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.debugging.JCTestCase;

public class FillingToolTest extends JCTestCase
{

	public FillingToolTest(final String p_name)
	{
		super(p_name);
	}

	public final void test_fillFromCoord_all()
	{

		final byte[][] ARRAY_01 = new byte[][] { { 1, 1 }, { 1, 1 } };
		Coord startCoord = new Coord(0, 0);
		CoordList bag = createBagFromByteArray(ARRAY_01);
		CoordList filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(4, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(1, 0)));
		assertTrue(filled.contains(new Coord(1, 1)));

		startCoord = new Coord(0, 1);
		bag = createBagFromByteArray(ARRAY_01);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(4, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(1, 0)));
		assertTrue(filled.contains(new Coord(1, 1)));

		startCoord = new Coord(1, 0);
		bag = createBagFromByteArray(ARRAY_01);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(4, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(1, 0)));
		assertTrue(filled.contains(new Coord(1, 1)));

		startCoord = new Coord(1, 1);
		bag = createBagFromByteArray(ARRAY_01);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(4, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(1, 0)));
		assertTrue(filled.contains(new Coord(1, 1)));

		
		// -----------------------------------------------------------
		final byte[][] ARRAY_13 = new byte[][] { 
				{ 1, 0, 0, 0, 1, 0}, 
				{ 1, 1, 1, 1, 1, 1}
				};
		startCoord = new Coord(0,0);
		bag = createBagFromByteArray(ARRAY_13);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(8, filled.size());
		assertEquals(0, bag.size());

		// -----------------------------------------------------------
		final byte[][] ARRAY_02 = new byte[][] { { 1, 1 }, { 1, 0 } };
		startCoord = new Coord(0, 0);
		bag = createBagFromByteArray(ARRAY_02);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(3, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(1, 0)));
		
		startCoord = new Coord(1, 0);
		bag = createBagFromByteArray(ARRAY_02);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(3, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(1, 0)));
		
		// -----------------------------------------------------------
		final byte[][] ARRAY_03 = new byte[][] { { 1, 1 }, { 0, 1 } };
		startCoord = new Coord(0, 0);
		bag = createBagFromByteArray(ARRAY_03);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(3, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(1, 0)));
		assertTrue(filled.contains(new Coord(1, 1)));
		
		startCoord = new Coord(1, 0);
		bag = createBagFromByteArray(ARRAY_03);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(3, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(1, 0)));
		assertTrue(filled.contains(new Coord(1, 1)));
		
		// -----------------------------------------------------------
		final byte[][] ARRAY_04 = new byte[][] { { 1, 1, 1}, { 0, 1, 0 }, { 1, 1, 1} };
		startCoord = new Coord(1, 0);
		bag = createBagFromByteArray(ARRAY_04);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(7, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(1, 0)));
		assertTrue(filled.contains(new Coord(2, 0)));
		assertTrue(filled.contains(new Coord(1, 1)));
		assertTrue(filled.contains(new Coord(0, 2)));
		assertTrue(filled.contains(new Coord(1, 2)));
		assertTrue(filled.contains(new Coord(2, 2)));

		
		// -----------------------------------------------------------
		final byte[][] ARRAY_05 = new byte[][] { 
				{ 1, 0, 1}, 
				{ 1, 0, 1 }, 
				{ 1, 1, 1} };
		startCoord = new Coord(0, 0);
		bag = createBagFromByteArray(ARRAY_05);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(7, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(2, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(2, 1)));
		assertTrue(filled.contains(new Coord(0, 2)));
		assertTrue(filled.contains(new Coord(1, 2)));
		assertTrue(filled.contains(new Coord(2, 2)));

	
		startCoord = new Coord(2, 0);
		bag = createBagFromByteArray(ARRAY_05);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(7, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(2, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(2, 1)));
		assertTrue(filled.contains(new Coord(0, 2)));
		assertTrue(filled.contains(new Coord(1, 2)));
		assertTrue(filled.contains(new Coord(2, 2)));

		
		// -----------------------------------------------------------
		final byte[][] ARRAY_06 = new byte[][] { 
				{ 1, 1, 1}, 
				{ 1, 0, 1 }, 
				{ 1, 0, 1} };
		startCoord = new Coord(0, 2);
		bag = createBagFromByteArray(ARRAY_06);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(7, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(1, 0)));
		assertTrue(filled.contains(new Coord(2, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(2, 1)));
		assertTrue(filled.contains(new Coord(0, 2)));
		assertTrue(filled.contains(new Coord(2, 2)));

	
		startCoord = new Coord(2, 2);
		bag = createBagFromByteArray(ARRAY_06);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(7, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(1, 0)));
		assertTrue(filled.contains(new Coord(2, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(2, 1)));
		assertTrue(filled.contains(new Coord(0, 2)));
		assertTrue(filled.contains(new Coord(2, 2)));

	
		
		// -----------------------------------------------------------
		final byte[][] ARRAY_07 = new byte[][] { 
				{ 1, 1, 1, 0, 0, 1}, 
				{ 1, 0, 1, 0, 1, 1}, 
				{ 1, 0, 1, 1, 0, 1} };
		startCoord = new Coord(0, 2);
		bag = createBagFromByteArray(ARRAY_07);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(12, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(1, 0)));
		assertTrue(filled.contains(new Coord(2, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(2, 1)));
		assertTrue(filled.contains(new Coord(0, 2)));
		assertTrue(filled.contains(new Coord(2, 2)));
		
		assertTrue(filled.contains(new Coord(5, 0)));
		assertTrue(filled.contains(new Coord(4, 1)));
		assertTrue(filled.contains(new Coord(5, 1)));
		assertTrue(filled.contains(new Coord(5, 2)));

		startCoord = new Coord(2, 2);
		bag = createBagFromByteArray(ARRAY_07);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(12, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(1, 0)));
		assertTrue(filled.contains(new Coord(2, 0)));
		assertTrue(filled.contains(new Coord(0, 1)));
		assertTrue(filled.contains(new Coord(2, 1)));
		assertTrue(filled.contains(new Coord(0, 2)));
		assertTrue(filled.contains(new Coord(2, 2)));
		
		assertTrue(filled.contains(new Coord(5, 0)));
		assertTrue(filled.contains(new Coord(4, 1)));
		assertTrue(filled.contains(new Coord(5, 1)));
		assertTrue(filled.contains(new Coord(5, 2)));
		
	
		
		// -----------------------------------------------------------
		final byte[][] ARRAY_08 = new byte[][] { 
				{ 1, 0, 1, 0}, 
				{ 0, 1, 0, 1}, 
				{ 1, 0, 1, 0},
				{ 0, 1, 0, 1}
				};
		startCoord = new Coord(1, 1);
		bag = createBagFromByteArray(ARRAY_08);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(8, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(2, 0)));
		assertTrue(filled.contains(new Coord(1, 1)));
		assertTrue(filled.contains(new Coord(3, 1)));
		assertTrue(filled.contains(new Coord(0, 2)));
		assertTrue(filled.contains(new Coord(2, 2)));
		assertTrue(filled.contains(new Coord(1, 3)));
		assertTrue(filled.contains(new Coord(3, 3)));
		
		startCoord = new Coord(0, 0);
		bag = createBagFromByteArray(ARRAY_08);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(8, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(2, 0)));
		assertTrue(filled.contains(new Coord(1, 1)));
		assertTrue(filled.contains(new Coord(3, 1)));
		assertTrue(filled.contains(new Coord(0, 2)));
		assertTrue(filled.contains(new Coord(2, 2)));
		assertTrue(filled.contains(new Coord(1, 3)));
		assertTrue(filled.contains(new Coord(3, 3)));
		
		startCoord = new Coord(3, 3);
		bag = createBagFromByteArray(ARRAY_08);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(8, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(2, 0)));
		assertTrue(filled.contains(new Coord(1, 1)));
		assertTrue(filled.contains(new Coord(3, 1)));
		assertTrue(filled.contains(new Coord(0, 2)));
		assertTrue(filled.contains(new Coord(2, 2)));
		assertTrue(filled.contains(new Coord(1, 3)));
		assertTrue(filled.contains(new Coord(3, 3)));
		
		// -----------------------------------------------------------
		final byte[][] ARRAY_09 = new byte[][] { 
				{ 1, 0, 0, 1}, 
				{ 0, 1, 0, 1}, 
				{ 1, 0, 0, 1},
				{ 0, 1, 0, 1}
				};
		startCoord = new Coord(1, 1);
		bag = createBagFromByteArray(ARRAY_09);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(4, filled.size());
		assertEquals(4, bag.size());
		assertTrue(filled.contains(new Coord(0, 0)));
		assertTrue(filled.contains(new Coord(1, 1)));
		assertTrue(filled.contains(new Coord(0, 2)));
		assertTrue(filled.contains(new Coord(1, 3)));
		
		startCoord = new Coord(3, 0);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(4, filled.size());
		assertEquals(0, bag.size());
		assertTrue(filled.contains(new Coord(3, 0)));
		assertTrue(filled.contains(new Coord(3, 1)));
		assertTrue(filled.contains(new Coord(3, 2)));
		assertTrue(filled.contains(new Coord(3, 3)));

	
		
		// -----------------------------------------------------------
		final byte[][] ARRAY_10 = new byte[][] { 
				{ 1, 1, 0, 1, 0, 1}, 
				{ 0, 1, 1, 1, 0, 1}, 
				{ 1, 0, 0, 1, 1, 1},
				{ 0, 1, 0, 1, 0, 1}
				};
		startCoord = new Coord(1, 1);
		bag = createBagFromByteArray(ARRAY_10);
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(15, filled.size());
		assertEquals(0, bag.size());
		
		// -----------------------------------------------------------
		final byte[][] ARRAY_11 = new byte[][] { 
				{ 1, 1, 0, 1, 0}, 
				{ 0, 1, 1, 1, 0}, 
				{ 1, 0, 0, 1, 1},
				{ 0, 1, 0, 1, 0}
				};
		startCoord = new Coord(1, 1);
		bag = createBagFromByteArray(ARRAY_11);
		assertEquals(11, bag.size());
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(11, filled.size());
		assertEquals(0, bag.size());
	
		
		// -----------------------------------------------------------
		final byte[][] ARRAY_12 = new byte[][] { 
				{ 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0}, 
				{ 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0}, 
				{ 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0},
				{ 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0},
				{ 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0},				
				{ 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0},
				{ 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0},
				{ 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0},
				{ 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0},
				{ 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0},
				{ 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0},
				{ 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0},
				{ 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0},
				{ 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0},
				{ 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0},
				{ 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0},
				{ 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0}, 
				{ 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0}, 
				{ 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0},
				{ 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0},
				{ 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0},				
				{ 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0},
				{ 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0},
				{ 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0},
				{ 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0},
				{ 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0},
				{ 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0},
				{ 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0},
				{ 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0},
				{ 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0},
				{ 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0},
				{ 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0}
				};
		startCoord = new Coord(1, 1);
		bag = createBagFromByteArray(ARRAY_12);
		assertEquals(993, bag.size());
		filled = FillingTool.fillFromCoord(bag, startCoord);

		assertEquals(993, filled.size());
		assertEquals(0, bag.size());
		
		try {
			try {
				FillingTool.fillFromCoord(null, startCoord);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex) {
				// NOP
			}
			
			try {
				FillingTool.fillFromCoord(filled, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex) {
				// NOP
			}
			
			try {
				FillingTool.fillFromCoord(bag, null);
				fail("An empty bag param has been accepted");
			} catch (IllegalArgumentException ex) {
				// NOP
			}
			
			try {
				FillingTool.fillFromCoord(filled, new Coord(-1, -1));
				fail("An invalid start coord param has been accepted");
			} catch (IllegalArgumentException ex) {
				// NOP
			}
		} catch(Exception ex){
			fail(ex);
		}
}

	private CoordList createBagFromByteArray(final byte[][] p_byteArray)
	{
		final CoordList bag = new CoordList();
		for (int y = 0; y < p_byteArray.length; y++)
		{
			for (int x = 0; x < p_byteArray[y].length; x++)
			{
				if (p_byteArray[y][x] != 0)
				{
					bag.add(new Coord(x, y));
				}
			}
		}

		return bag;
	}
}
