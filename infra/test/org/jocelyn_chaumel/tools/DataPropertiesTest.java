/*
 * Created on Sep 7, 2005
 */
package org.jocelyn_chaumel.tools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class DataPropertiesTest extends JCTestCase
{

    /**
     * @param aName
     */
    public DataPropertiesTest(String aName)
    {
        super(aName);
    }

    public final void test_setIntProperty_nullParam()
    {
        try
        {
            final DataProperties data = new DataProperties();
            try
            {
                data.setIntProperty(null, 0);
                fail("Null param has been accepted.");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_getIntProperty_nullParam()
    {
        try
        {
            final DataProperties data = new DataProperties();
            try
            {
                data.getIntProperty(null);
                fail("Null param has been accepted.");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_getIntProperty_invalid()
    {
        try
        {
            final DataProperties data = new DataProperties();
            try
            {
                data.getIntProperty("invalid");
                fail("Invalid param has been accepted.");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_get_and_setIntProperty_valid()
    {
        try
        {
            final DataProperties data = new DataProperties();

            data.setIntProperty("toto", 1);
            assertEquals("1", data.getProperty("toto"));
            assertEquals(1, data.getIntProperty("toto"));

            data.setIntProperty("toto2", 2);
            assertEquals("2", data.getProperty("toto2"));
            assertEquals(2, data.getIntProperty("toto2"));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_setBooleanProperty_nullParam()
    {
        try
        {
            final DataProperties data = new DataProperties();
            try
            {
                data.setBooleanProperty(null, true);
                fail("Null param has been accepted.");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_getBooleanProperty_nullParam()
    {
        try
        {
            final DataProperties data = new DataProperties();
            try
            {
                data.getBooleanProperty(null);
                fail("Null param has been accepted.");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_getBooleanProperty_invalid()
    {
        try
        {
            final DataProperties data = new DataProperties();
            try
            {
                data.getBooleanProperty("invalid");
                fail("Invalid param has been accepted.");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_get_and_setBooleanProperty_valid()
    {
        try
        {
            final DataProperties data = new DataProperties();

            data.setBooleanProperty("toto", true);
            assertEquals("true", data.getProperty("toto"));
            assertEquals(true, data.getBooleanProperty("toto"));

            data.setBooleanProperty("toto", false);
            assertEquals("false", data.getProperty("toto"));
            assertEquals(false, data.getBooleanProperty("toto"));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_get_and_setSequentialListProperty_valid()
    {
        try
        {
            final DataProperties data = new DataProperties();

            data.setSequentialListForProperty("toto", new ArrayList<String>());
            assertEquals(0, data.getIntProperty("toto.size"));
            assertEquals(0, data.getSequentialListForProperty("toto").size());

            data.clear();
            final ArrayList<String> srcPropList = new ArrayList<String>();
            srcPropList.add("bibi");
            data.setSequentialListForProperty("toto", srcPropList);
            assertEquals(1, data.getIntProperty("toto.size"));
            assertEquals(1, data.getSequentialListForProperty("toto").size());
            assertEquals("bibi", (String) data.getSequentialListForProperty("toto").get(0));

            data.clear();
            srcPropList.clear();
            srcPropList.add("bibi");
            srcPropList.add("bobo");
            data.setSequentialListForProperty("toto", srcPropList);
            assertEquals(2, data.getIntProperty("toto.size"));
            assertEquals(2, data.getSequentialListForProperty("toto").size());
            assertEquals("bibi", (String) data.getSequentialListForProperty("toto").get(0));
            assertEquals("bobo", (String) data.getSequentialListForProperty("toto").get(1));

        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_getSequentialListProperty_nullParam()
    {
        try
        {
            final DataProperties data = new DataProperties();

            data.setSequentialListForProperty("toto", new ArrayList<String>());
            assertEquals(0, data.getIntProperty("toto.size"));
            try
            {
                data.getSequentialListForProperty(null);
                fail("Null param has been accepted");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_getSequentialListProperty_invalidProperty()
    {
        try
        {
            final DataProperties data = new DataProperties();

            try
            {
                data.getSequentialListForProperty("bibi");
                fail("Invalid param has been accepted");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_setSequentialListProperty_nullParam_1()
    {
        try
        {
            final DataProperties data = new DataProperties();

            try
            {
                data.setSequentialListForProperty(null, new ArrayList<String>());
                fail("Null param has been accepted");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_setSequentialListProperty_nullParam_2()
    {
        try
        {
            final DataProperties data = new DataProperties();

            try
            {
                data.setSequentialListForProperty("toto", null);
                fail("Null param has been accepted");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_store_nullParam_1()
    {
        try
        {
            final DataProperties data = new DataProperties();

            try
            {
                data.store((File) null, "");
                fail("Null param has been accepted");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_store_invalidFile()
    {
        final File file = new File("c:\\temp\\data_properties_test.properties");
        try
        {
            final DataProperties data = new DataProperties();
            FileMgr.forceDeleteIfPresent(file);
            FileMgr.createFile(file, "data=properties");
            assertTrue(file.isFile());
            try
            {
                data.store(file, "hey mon gars");
                fail("Invalid file param has been accepted");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
        finally
        {
            try
            {
                FileMgr.forceDeleteIfPresent(file);
            }
            catch (IOException ex)
            {
                fail(ex);
            }
        }
    }

    public final void test_load_nullParam_1()
    {
        try
        {
            final DataProperties data = new DataProperties();

            try
            {
                data.load((File) null);
                fail("Null param has been accepted");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_load_invalidFile()
    {
        final File file = new File("c:\\temp\\");
        try
        {
            final DataProperties data = new DataProperties();
            assertTrue(file.isDirectory());
            try
            {
                data.load(file);
                fail("Invalid file param has been accepted");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_store_and_load_valid()
    {
        final File file = new File("c:\\temp\\data_properties_test.properties");
        try
        {
            assertFalse(file.isFile());

            final DataProperties data = new DataProperties();
            data.setBooleanProperty("boolean", true);
            data.setIntProperty("int", 12);
            ArrayList<String> list = new ArrayList<String>();
            list.add("1");
            list.add("2");
            data.setSequentialListForProperty("sequence", list);

            data.store(file, "Yes");
            assertTrue(file.isFile());

            final DataProperties newData = new DataProperties();
            newData.load(file);
            assertEquals(true, newData.getBooleanProperty("boolean"));
            assertEquals(12, newData.getIntProperty("int"));
            assertEquals(2, newData.getSequentialListForProperty("sequence").size());
            assertEquals("1", newData.getSequentialListForProperty("sequence").get(0));
            assertEquals("2", newData.getSequentialListForProperty("sequence").get(1));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
        finally
        {
            try
            {
                FileMgr.forceDeleteIfPresent(file);
            }
            catch (IOException ex)
            {
                fail(ex);
            }
        }

    }

    public final void test_removeSequence_valid()
    {
        try
        {
            final DataProperties data = new DataProperties();
            ArrayList<String> list = new ArrayList<String>();
            list.add("1");
            list.add("2");
            data.setSequentialListForProperty("sequence", list);
            assertEquals("1", data.getProperty("sequence.1"));
            assertEquals("2", data.getProperty("sequence.2"));
            assertEquals("2", data.getProperty("sequence.size"));
            data.removeSequentialListForProperty("sequence");
            assertFalse(data.containsKey("sequence.1"));
            assertFalse(data.containsKey("sequence.2"));
            assertFalse(data.containsKey("sequence.size"));

            data.setProperty("papa", "toto");
            data.removeSequentialListForProperty("sequence");
            assertFalse(data.containsKey("sequence.1"));
            assertFalse(data.containsKey("sequence.2"));
            assertFalse(data.containsKey("sequence.size"));
            assertEquals("toto", data.getProperty("papa"));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_removeSequence_nullParam()
    {
        try
        {
            final DataProperties data = new DataProperties();
            try
            {
                data.removeSequentialListForProperty(null);
                fail("Null param has been accepted");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_updateSequence_valid()
    {
        try
        {
            final DataProperties data = new DataProperties();
            ArrayList<String> list = new ArrayList<String>();
            list.add("1");
            list.add("2");
            data.updateSequentialListForProperty("sequence", list);
            assertEquals("1", data.getProperty("sequence.1"));
            assertEquals("2", data.getProperty("sequence.2"));
            assertEquals("2", data.getProperty("sequence.size"));

            list.remove(1);
            list.add("3");
            list.add("4");
            data.updateSequentialListForProperty("sequence", list);
            assertEquals("1", data.getProperty("sequence.1"));
            assertEquals("3", data.getProperty("sequence.2"));
            assertEquals("4", data.getProperty("sequence.3"));
            assertEquals("3", data.getProperty("sequence.size"));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_isSequentialListExist_valid()
    {
        try
        {
            final DataProperties data = new DataProperties();
            assertEquals(false, data.isSequentialListExist("sequence"));
            ArrayList<String> list = new ArrayList<String>();
            list.add("1");
            list.add("2");
            data.updateSequentialListForProperty("sequence", list);
            assertEquals(false, data.isSequentialListExist("toto"));
            assertEquals(true, data.isSequentialListExist("sequence"));

            data.updateSequentialListForProperty("toto", new ArrayList<String>());
            assertEquals(true, data.isSequentialListExist("toto"));
            assertEquals(true, data.isSequentialListExist("sequence"));
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_isSequentialListExist_nullParam()
    {
        try
        {
            final DataProperties data = new DataProperties();
            try
            {
                data.isSequentialListExist(null);
                fail("Null param has been accepted");
            }
            catch (IllegalArgumentException ex)
            {
                // NOP
            }
        }
        catch (Exception ex)
        {
            fail(ex);
        }
    }
}