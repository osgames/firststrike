package org.jocelyn_chaumel.tools.params;
import java.util.HashMap;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class ParameterAnalyserTest extends JCTestCase
{

    /**
     * @param aName
     */
    public ParameterAnalyserTest(final String aName)
    {
        super(aName);
    }

    public final void test_cntr_nullParam_1()
    {
        try
        {
            
            try
            {
                new ParameterAnalyser(null);
                fail("Null param has been accepted.");
            } catch (IllegalArgumentException ex)
            {
                // NOP
            }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_cntr_nullParam_2()
    {
        try
        {
            final String [] array = { "1", null, "2" };
            try
            {
                new ParameterAnalyser(array);
                fail("Null param has been accepted.");
            } catch (IllegalArgumentException ex)
            {
                // NOP
            }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_cntr_emptyParam()
    {
        try
        {
            final String [] array = { "1", "", "2" };
            
            try
            {
                new ParameterAnalyser(array);
                fail("Empty param has been accepted.");
            } catch (IllegalArgumentException ex)
            {
                // NOP
            }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_cntr_duplicatedParam_1()
    {
        try
        {
            final String [] array = { "1", "2", "2" };
            try
            {
                new ParameterAnalyser(array);
                fail("Duplicated param has been accepted.");
            } catch (IllegalArgumentException ex)
            {
                // NOP
            }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_cntr_duplicatedParam_2()
    {
        try
        {
            final String [] array = { "1", "2", " 2" };
            try
            {
                new ParameterAnalyser(array);
                fail("Duplicated param has been accepted.");
            } catch (IllegalArgumentException ex)
            {
                // NOP
            }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_parseParam_nullParam_1()
    {
        try
        {
            final String [] array = { "1", "2", "22" };
            final ParameterAnalyser pa = new ParameterAnalyser(array);
            try
            {
                pa.parseParams(null);
                fail("Null param has been accepted.");
            } catch (IllegalArgumentException ex)
            {
                // NOP
            }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_parseParam_nullParam_2()
    {
        try
        {
            final String [] paramNameArray = { "1", "2", "22" };
            final ParameterAnalyser pa = new ParameterAnalyser(paramNameArray);
            final String [] paramArray = { "1to", null, "22" };
            try
            {
                pa.parseParams(paramArray);
                fail("Null param has been accepted.");
            } catch (IllegalArgumentException ex)
            {
                // NOP
            }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_parseParam_emptyParam()
    {
        try
        {
            final String [] paramNameArray = { "1", "2", "22" };
            final ParameterAnalyser pa = new ParameterAnalyser(paramNameArray);
            final String [] paramArray = { "1to", "", "22" };
            try
            {
                pa.parseParams(paramArray);
                fail("Empty param has been accepted.");
            } catch (IllegalParameterException ex)
            {
                // NOP
            }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_parseParam_invalidParamName()
    {
        try
        {
            final String [] paramNameArray = { "1", "2", "22" };
            final ParameterAnalyser pa = new ParameterAnalyser(paramNameArray);
            final String [] paramArray = { "1to", "3", "22bib" };
            try
            {
                pa.parseParams(paramArray);
                fail("Invalid param has been accepted.");
            } catch (IllegalParameterException ex)
            {
                // NOP
            }
        } catch (Exception ex)
        {
            fail(ex);
        }
    }

    public final void test_parseParam_valid()
    {
        try
        {
            final String [] paramNameArray = { "1:", "2:", "22:" };
            final ParameterAnalyser pa = new ParameterAnalyser(paramNameArray);
            final String [] paramArray = { "1:to", "22:bib" };
            final HashMap map = pa.parseParams(paramArray);
            assertEquals(2, map.size());
            assertEquals("to", map.get("1:"));
            assertEquals("bib", map.get("22:"));
        } catch (Exception ex)
        {
            fail(ex);
        }
    }
}
