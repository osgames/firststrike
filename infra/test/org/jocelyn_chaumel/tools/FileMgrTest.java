/*
 * FileMgrTest.java JUnit based test Created on October 25, 2003, 10:49 AM
 */

package org.jocelyn_chaumel.tools;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * Classe de test pour le gestionnaire de fichier.
 */
public final class FileMgrTest extends JCTestCase
{
	public FileMgrTest(final String aName)
	{
		super(aName);
	}

	private final static File SOURCE = new File("C:\\temp\\source.txt");
	private final static File DESTINATION = new File("C:\\temp\\destination.txt");

	private void createSourceFile() throws FileNotFoundException, IOException
	{
		new File("C:\\temp").mkdir();
		FileOutputStream fo = new FileOutputStream(SOURCE);
		BufferedOutputStream bo = new BufferedOutputStream(fo);

		bo.write("hey mon gars".getBytes());
		bo.close();
		fo.close();
	}

	// ----------------------------------------
	public final void test_copyFile_nullSource()
	{
		try
		{
			FileMgr.copyFile(null, DESTINATION, null);
			fail("Invalid param has been accepted.");
		} catch (final IllegalArgumentException ex)
		{
			// NOP
		} catch (final Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_copyFile_nullDestination()
	{
		try
		{
			FileMgr.copyFile(SOURCE, null, null);
			fail("Invalid param has been accepted.");
		} catch (final IllegalArgumentException ex)
		{
			// NOP
		} catch (final Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_copyFile_sourceNonExist()
	{
		try
		{
			FileMgr.copyFile(new File(""), DESTINATION, null);
			fail("Invalid param has been accepted.");
		} catch (final IllegalArgumentException ex)
		{
			// NOP
		} catch (final Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_copyFile_destinationExist()
	{
		try
		{
			createSourceFile();
			FileMgr.copyFile(SOURCE, SOURCE, null);
			fail("Invalid param has been accepted.");
		} catch (final IllegalArgumentException ex)
		{
			// NOP
		} catch (final Exception ex)
		{
			fail(ex);
		} finally
		{
			try
			{
				FileMgr.forceDelete(SOURCE);
			} catch (final Exception ex)
			{
				fail(ex);
			}
		}
	}

	// ----------------------------------------
	public final void test_copyFile_valid()
	{
		try
		{
			createSourceFile();
			FileMgr.copyFile(SOURCE, DESTINATION, null);
		} catch (final Exception ex)
		{
			fail(ex);
		} finally
		{
			try
			{
				FileMgr.forceDelete(SOURCE);
				FileMgr.forceDelete(DESTINATION);
			} catch (final Exception ex)
			{
				fail(ex);
			}
		}
	}

	// ----------------------------------------
	public final void test_copyFile_valid_readOnly()
	{
		try
		{
			createSourceFile();
			assertTrue(SOURCE.setReadOnly());
			FileMgr.copyFile(SOURCE, DESTINATION, null);
			assertTrue(!DESTINATION.canWrite());
		} catch (final Exception ex)
		{
			fail(ex);
		} finally
		{
			try
			{
				FileMgr.forceDelete(SOURCE);
				FileMgr.forceDelete(DESTINATION);
			} catch (final Exception ex)
			{
				fail(ex);
			}
		}
	}

	// ----------------------------------------
	public final void test_forceDelete_nullParam()
	{
		try
		{
			FileMgr.forceDelete(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_forceDelete_invalidFile()
	{
		try
		{
			FileMgr.forceDelete(new File("dfg"));
			fail("Invalid file has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_forceDelete_valid_readOnlyFile()
	{
		final File file = new File("C:\\Temp\\fichier.txt");
		try
		{
			final File tempDir = new File("c:\\Temp");
			if (tempDir.exists())
			{
				assertTrue(tempDir.isDirectory());
			} else
			{
				assertTrue(tempDir.mkdir());
			}

			assertTrue(file.createNewFile());
			assertTrue(file.setReadOnly());
			FileMgr.forceDelete(file);
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			assertTrue(!file.exists());
		}
	}

	// ----------------------------------------
	public final void test_forceDeleteCascade_nullParam()
	{
		try
		{
			FileMgr.forceDeleteCascade(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_forceDeleteCascade_invalidDir()
	{
		try
		{
			FileMgr.forceDeleteCascade(new File("sdfsd"));
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_forceDeleteCascade_file()
	{
		final File file = new File("C:\\Temp\\fichier.txt");
		try
		{
			final File tempDir = new File("c:\\Temp");
			if (tempDir.exists())
			{
				assertTrue(tempDir.isDirectory());
			} else
			{
				assertTrue(tempDir.mkdir());
			}

			assertTrue(file.createNewFile());
			FileMgr.forceDeleteCascade(file);
			fail("A file has been accepted has a valid directory");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			assertTrue(file.delete());
		}
	}

	// ----------------------------------------
	public final void test_forceDeleteCascade_valid()
	{
		final File dir = new File("C:\\tempFS");
		try
		{
			assertTrue(new File("c:\\tempFS\\papa\\jocelyn").mkdirs());
			assertTrue(new File("c:\\tempFS\\papa\\frederic").mkdirs());
			assertTrue(new File("c:\\tempFS\\papa\\david").mkdirs());
			assertTrue(new File("c:\\tempFS\\maman\\genevieve").mkdirs());
			assertTrue(new File("c:\\tempFS\\sam").mkdirs());
			assertTrue(new File("c:\\tempFS\\f1.txt").createNewFile());
			assertTrue(new File("c:\\tempFS\\f2.txt").createNewFile());
			assertTrue(new File("c:\\tempFS\\papa\\f1.txt").createNewFile());
			assertTrue(new File("c:\\tempFS\\papa\\f2.txt").createNewFile());
			assertTrue(new File("c:\\tempFS\\maman\\f1.txt").createNewFile());
			assertTrue(new File("c:\\tempFS\\maman\\f2.txt").createNewFile());
			assertTrue(new File("c:\\tempFS\\papa\\jocelyn\\f1.txt").createNewFile());
			assertTrue(new File("c:\\tempFS\\papa\\jocelyn\\f2.txt").createNewFile());

			FileMgr.forceDeleteCascade(dir);
			assertTrue(!dir.exists());
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{

			try
			{
				FileMgr.forceCreateDirIfNotPresent(dir);
			} catch (IOException ex)
			{
				fail(ex);
			}
		}
	}

	// ----------------------------------------
	public final void test_forceDeleteCascadeIfPresent_nullParam()
	{
		try
		{
			FileMgr.forceDeleteCascadeIfPresent(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_forceDeleteCascadeIfPresent_valid()
	{
		final File dir = new File("C:\\tempFS");
		try
		{
			FileMgr.forceCreateDirIfNotPresent(new File("c:\\tempFS\\papa\\jocelyn"));

			assertEquals(true, dir.isDirectory());
			FileMgr.forceDeleteCascadeIfPresent(dir);
			assertEquals(false, dir.isDirectory());
			FileMgr.forceDeleteCascadeIfPresent(dir);
			assertEquals(false, dir.isDirectory());

		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{

			try
			{
				FileMgr.forceCreateDirIfNotPresent(dir);
			} catch (IOException ex)
			{
				fail(ex);
			}
		}
	}

	public final void test_createFile_nullFileParam()
	{
		try
		{
			FileMgr.createFile(null, "");
			fail();
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_createFile_nullDataParam()
	{
		try
		{
			FileMgr.createFile(new File("toto"), null);
			fail();
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_createFile_invalideFile()
	{
		File testFile = new File("c:\\Temp\\toto.txt");
		try
		{
			FileMgr.createFile(testFile, "des donn�es");
			try
			{
				FileMgr.createFile(testFile, "des donn�es");
				fail();
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			try
			{
				FileMgr.forceDeleteIfPresent(testFile);
			} catch (IOException ex)
			{
				fail(ex);
			}
		}
	}

	public final void test_createFileIfNotExist_nullParam_1()
	{
		try
		{
			FileMgr.createFile(null, "des donn�es");
			fail();
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_createFileIfNotExist_nullParam_2()
	{
		try
		{
			FileMgr.createFile(new File("toto"), null);
			fail();
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_createFileIfNotPresent_valid()
	{
		File testFile = new File("c:\\Temp\\toto.txt");
		try
		{
			FileMgr.forceDeleteIfPresent(testFile);
			assertEquals(false, testFile.isFile());
			FileMgr.createFileIfNotExist(testFile, "des donn�es");
			assertEquals(true, testFile.isFile());
			FileMgr.createFileIfNotExist(testFile, "des donn�es");
			assertEquals(true, testFile.isFile());
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			try
			{
				FileMgr.forceDeleteIfPresent(testFile);
			} catch (IOException ex)
			{
				fail(ex);
			}
		}
	}

	public final void test_createFile_valid()
	{
		File fichier = new File("c:\\Temp\\fichier");
		try
		{
			FileMgr.createFile(new File("c:\\Temp\\fichier"), "des donn�es");
			assertTrue(fichier.isFile());
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			try
			{
				FileMgr.forceDelete(fichier);
			} catch (final Exception ex)
			{
				fail(ex);
			}

		}
	}

	public final void test_forceCreateDirIfNotPresent_nullParam()
	{
		try
		{
			FileMgr.forceCreateDirIfNotPresent(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_forceCreateDirIfNotPresent_valid()
	{
		try
		{
			final File jossDir = new File("c:\\joss");
			FileMgr.forceDeleteCascadeIfPresent(jossDir);
			assertEquals(false, jossDir.isDirectory());

			FileMgr.forceCreateDirIfNotPresent(jossDir);
			assertEquals(true, jossDir.isDirectory());

			FileMgr.forceCreateDirIfNotPresent(jossDir);
			assertEquals(true, jossDir.isDirectory());

			FileMgr.forceDeleteCascadeIfPresent(jossDir);
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_forceDeleteIfPresent_nullParam()
	{
		try
		{
			FileMgr.forceDeleteIfPresent(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_forceDeleteIfPresent_valid()
	{
		try
		{
			final File jossFile = new File("c:\\Temp\\jossFile.tmp");
			FileMgr.forceDeleteIfPresent(jossFile);
			assertEquals(false, jossFile.isFile());

			FileMgr.createFile(jossFile, "des donn�es");
			assertEquals(true, jossFile.isFile());

			FileMgr.forceDeleteIfPresent(jossFile);
			assertEquals(false, jossFile.isFile());

			FileMgr.forceDeleteIfPresent(jossFile);
			assertEquals(false, jossFile.isFile());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_isDirectory_nullParam()
	{
		try
		{
			FileMgr.isDirectory(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_isDirectory_valid()
	{
		try
		{
			assertEquals(true, FileMgr.isDirectory("c:\\program files"));
			assertEquals(false, FileMgr.isDirectory("c:\\bob"));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_isFile_nullParam()
	{
		try
		{
			FileMgr.isFile(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_isFile_valid()
	{
		try
		{
			File roots = new File("c:\\");
			File[] fileArray = roots.listFiles();
			int i = 0;
			for (; i < fileArray.length; i++)
			{
				if (fileArray[i].isFile())
				{
					break;
				}
			}
			assertEquals(true, FileMgr.isFile(fileArray[i].getAbsolutePath()));
			assertEquals(false, FileMgr.isFile("c:\\bob.txt"));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getRelativePath_valid()
	{
		final File fileFormCurrentDir = new File(FileMgr.getCurrentDir(), "toto");
		final File simpleFile = new File("c:\\bibi");
		try
		{
			assertEquals("~\\toto", FileMgr.getRelativePath(fileFormCurrentDir));
			assertEquals(simpleFile.getAbsolutePath(), FileMgr.getRelativePath(simpleFile));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getRelativePath_nullParam()
	{
		try
		{

			FileMgr.getRelativePath(null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_setCurrentDir_all()
	{
		File setCurrentDirTempFile = null;
		try
		{

			final File newCurrentDir = new File("c:\\temp");
			FileMgr.setCurrentDir(newCurrentDir);

			try
			{
				FileMgr.setCurrentDir(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			setCurrentDirTempFile = File.createTempFile("setCurrentDirTempFile", "tmp", new File("c:\\temp"));
			assertTrue(setCurrentDirTempFile.isFile());
			try
			{
				FileMgr.setCurrentDir(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{

			try
			{
				FileMgr.forceDeleteIfPresent(setCurrentDirTempFile);
			} catch (IOException ex)
			{
				fail(ex);
			}
		}

	}

	public final void test_getFileExtention_all()
	{
		try
		{
			assertEquals("jpg", FileMgr.getFileExtention(new File("c:\\toto.jpg")));
			assertEquals("j", FileMgr.getFileExtention(new File("c:\\toto.j")));
			assertEquals("j", FileMgr.getFileExtention(new File("c:\\toto.jpg.j")));
			assertEquals("", FileMgr.getFileExtention(new File("c:\\toto.")));
			assertEquals("", FileMgr.getFileExtention(new File("c:\\toto")));

			try
			{
				FileMgr.getFileExtention(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getFileNameWithoutExtention_all()
	{
		try
		{
			assertEquals("toto", FileMgr.getFileNameWithoutExtention(new File("c:\\toto.jpg")));
			assertEquals("toto", FileMgr.getFileNameWithoutExtention(new File("c:\\toto.j")));
			assertEquals("toto.jpg", FileMgr.getFileNameWithoutExtention(new File("c:\\toto.jpg.j")));
			assertEquals("toto", FileMgr.getFileNameWithoutExtention(new File("c:\\toto.")));
			assertEquals("toto", FileMgr.getFileNameWithoutExtention(new File("c:\\toto")));

			try
			{
				FileMgr.getFileNameWithoutExtention(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_addExtentionIfNecessairy_all()
	{
		try
		{
			assertEquals("toto.jpg", FileMgr.addExtentionIfNecessairy(new File("c:\\toto.jpg"), "jpG").getName());
			assertEquals("toto.j.JPG", FileMgr.addExtentionIfNecessairy(new File("c:\\toto.j"), "JPG").getName());
			assertEquals("toto.jpg.j.JPG", FileMgr.addExtentionIfNecessairy(new File("c:\\toto.jpg.j"), "JPG")
					.getName());
			assertEquals("toto.JPG", FileMgr.addExtentionIfNecessairy(new File("c:\\toto."), "JPG").getName());
			assertEquals("toto.JPG", FileMgr.addExtentionIfNecessairy(new File("c:\\toto"), "JPG").getName());

			try
			{
				FileMgr.addExtentionIfNecessairy(null, "f");
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				FileMgr.addExtentionIfNecessairy(new File("c:\\toto"), null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				FileMgr.addExtentionIfNecessairy(new File("c:\\toto"), ".jpg");
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				FileMgr.addExtentionIfNecessairy(new File("c:\\toto"), "");
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_convertPathToString_valid()
	{
		try
		{

			File file = new File("c:\\temp");
			assertEquals("c:\\temp", FileMgr.convertPathToString(file, 11));
			
			file = new File("c:\\temp8901");
			assertEquals("c:\\temp8901", FileMgr.convertPathToString(file, 11));
			
			file = new File("c:\\temp89012");
			assertEquals("... mp89012", FileMgr.convertPathToString(file, 11));
			
			file = new File("c:\\temp89012\\12345");
			assertEquals("c:\\ ... 12345", FileMgr.convertPathToString(file, 13));
			try
			{
				FileMgr.convertPathToString(null, 11);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			} catch (Exception ex)
			{
				fail(ex);
			}
			
			try
			{
				FileMgr.convertPathToString(file, 10);
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			} catch (Exception ex)
			{
				fail(ex);
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}