package org.jocelyn_chaumel.prototype.resource_loading;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;

import org.jocelyn_chaumel.tools.DataProperties;

public class ResourceLoading
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        try
        {
            URL url = ClassLoader.getSystemResource("org/jocelyn_chaumel/prototype/resource_loading/test.properties");
            InputStream inputStream = url.openStream();
            DataProperties prop = new DataProperties();
            prop.load(inputStream);
            Set keySet = prop.keySet();
            String value;
            String entry;
            final Iterator iter = keySet.iterator();
            while (iter.hasNext())
            {
                entry = (String) iter.next();
                value = prop.getProperty(entry);
                System.out.println(entry + "=" + value);
            }
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }

    }

}
