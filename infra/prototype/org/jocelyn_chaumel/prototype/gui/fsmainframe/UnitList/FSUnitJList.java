package org.jocelyn_chaumel.prototype.gui.fsmainframe.UnitList;

import javax.swing.DefaultListModel;
import javax.swing.JList;

public class FSUnitJList extends JList
{
    private DefaultListModel _model = new DefaultListModel();
    public FSUnitJList(){
        setModel(_model);
    }
}
