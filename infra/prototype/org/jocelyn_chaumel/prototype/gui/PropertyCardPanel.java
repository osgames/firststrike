package org.jocelyn_chaumel.prototype.gui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jocelyn_chaumel.tools.data_type.Property;
import org.jocelyn_chaumel.tools.data_type.PropertyArrayList;
import org.jocelyn_chaumel.tools.gui.jtable.JTableProperties;
import org.jocelyn_chaumel.tools.gui.jtable.PropertiesTableModel;

public class PropertyCardPanel
{
	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Prototype - JTable With Row Header");

		final JLabel jossLbl = new JLabel("Joss");
		final JPanel jossPnl = new JPanel(new FlowLayout());
		jossPnl.add(jossLbl);

		final Property propertyFamillyName = new Property("Familly Name", "CHAUMEL");
		final Property propertyFirstName = new Property("First Name", "Jocelyn");
		final PropertyArrayList list = new PropertyArrayList();
		list.add(propertyFamillyName);
		list.add(propertyFirstName);
		final JTableProperties sTable = new JTableProperties(new PropertiesTableModel(list));

		final CardLayout cardLayout = new CardLayout();
		final JPanel centerPanel = new JPanel(cardLayout);
		centerPanel.add("joss", jossPnl);
		centerPanel.add("table", sTable);
		frame.getContentPane().add(centerPanel, BorderLayout.CENTER);
		cardLayout.show(centerPanel, "joss");

		final JPanel topPanel = new JPanel();

		final JButton button = new JButton(new AbstractAction()
		{
			private boolean _toggle = false;

			public void actionPerformed(final ActionEvent anActionEvent)
			{
				if (_toggle)
				{
					System.out.println("joss");
					jossPnl.add(new JLabel("" + new Date()));
					cardLayout.show(centerPanel, "joss");
				} else
				{
					System.out.println("table");
					cardLayout.show(centerPanel, "table");
				}
				_toggle = !_toggle;
			}

		});
		button.setText("Switch Current Card");
		topPanel.add(button);
		frame.getContentPane().add(topPanel, BorderLayout.NORTH);

		frame.pack();
		frame.setVisible(true);
	}
}
