/*
 * Created on 12 nov. 2006
 */
package org.jocelyn_chaumel.prototype.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.jocelyn_chaumel.tools.gui.jtable.ScrollableJTable;

/**
 * @author Jocelyn Chaumel
 */
public class JTableWithRowHeader
{
	public static String[][] _data = { { "11", "12", "13" }, { "21", "22", "23" }, { "31", "32", "33" },
			{ "41", "42", "43" }, };

	public static String[] _columnName = { "Colonne #1", "Colonne #2", "Colonne #3" };

	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Prototype - JTable With Row Header");

		DefaultTableModel tableModel = new DefaultTableModel(_data, _columnName);
		JTable table = new JTable(tableModel);
		ScrollableJTable sTable = new ScrollableJTable(table);
		frame.getContentPane().add(sTable, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

}
