package org.jocelyn_chaumel.prototype.gui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ScrollableButtonList
{

	ScrollableButtonList()
	{
		final JPanel labelPnl = new JPanel(new FlowLayout());
		final JScrollPane scrollPnl = new CostumScrollPnl(labelPnl);
		final CardLayout cardLayout = new CardLayout();
		final JPanel cardPnl = new JPanel(cardLayout);
		final JPanel emptyPnl = new JPanel(new FlowLayout());
		final JPanel southPnl = new JPanel(new FlowLayout());
		emptyPnl.add(new JLabel("empty"));
		southPnl.add(new JButton("south"));
		cardPnl.add("scroll", scrollPnl);
		cardPnl.add("empty", emptyPnl);
		cardLayout.show(cardPnl, "empty");
		JPanel nothPnl = new JPanel(new BorderLayout());
		JButton button = new JButton(new AbstractAction()
		{

			public void actionPerformed(final ActionEvent arg0)
			{
				String text = "TT";
				System.out.println(text);
				JButton label = new JButton(text);
				labelPnl.add(label);
				if ((++_counter) % 2 == 0)
				{
					System.out.println("show Scroll");
					cardLayout.show(cardPnl, "scroll");
				} else
				{
					System.out.println("show Empty");
					cardLayout.show(cardPnl, "empty");
				}
			}
		});

		button.setText("Go!");
		nothPnl.add(button);

		final JFrame frame = new JFrame("Prototype - JTable With Row Header");
		frame.getContentPane().add(nothPnl, BorderLayout.NORTH);
		frame.getContentPane().add(cardPnl, BorderLayout.CENTER);
		frame.getContentPane().add(southPnl, BorderLayout.SOUTH);

		frame.addWindowListener(new WindowListener()
		{
			public void windowActivated(WindowEvent arg0)
			{
				System.out.println("activated");
			}

			public void windowClosed(WindowEvent arg0)
			{
				System.out.println("closed");
				System.exit(0);
			}

			public void windowClosing(WindowEvent arg0)
			{
				System.out.println("closing");
				System.exit(0);
			}

			public void windowDeactivated(WindowEvent arg0)
			{
				System.out.println("desactivated");
			}

			public void windowDeiconified(WindowEvent arg0)
			{
			}

			public void windowIconified(WindowEvent arg0)
			{
			}

			public void windowOpened(WindowEvent arg0)
			{
				System.out.println("opened");
			}

		});
		frame.pack();
		frame.setVisible(true);

	}

	public static int _counter = 0;

	public static void main(final String[] anArray)
	{

		new ScrollableButtonList();
	}

	class CostumScrollPnl extends JScrollPane
	{
		private final Dimension _dim = new Dimension(200, 100);

		public CostumScrollPnl(final Component aComp)
		{
			super(aComp);
			getViewport().setPreferredSize(_dim);
			getViewport().setMaximumSize(_dim);
		}

		public Dimension getPreferredSize()
		{
			return _dim;
		}

		public Dimension getMaxDimension()
		{
			return _dim;
		}
	}
}
