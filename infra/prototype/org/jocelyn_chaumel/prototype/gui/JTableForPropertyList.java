/*
 * Created on 12 nov. 2006
 */
package org.jocelyn_chaumel.prototype.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import org.jocelyn_chaumel.tools.data_type.Property;
import org.jocelyn_chaumel.tools.data_type.PropertyArrayList;
import org.jocelyn_chaumel.tools.gui.jtable.JTableProperties;
import org.jocelyn_chaumel.tools.gui.jtable.PropertiesTableModel;

/**
 * @author Jocelyn Chaumel
 */
public class JTableForPropertyList
{
	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Prototype - JTable With Row Header");

		final Property propertyFamillyName = new Property("Familly Name", "CHAUMEL");
		final Property propertyFirstName = new Property("First Name", "Jocelyn");
		final PropertyArrayList list = new PropertyArrayList();
		list.add(propertyFamillyName);
		list.add(propertyFirstName);

		final JTableProperties sTable = new JTableProperties(new PropertiesTableModel(list));
		frame.getContentPane().add(sTable, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}
}
