package org.jocelyn_chaumel.prototype.serialization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.jocelyn_chaumel.tools.FileMgr;

public class SeriealizeTest implements Serializable
{
	public void run()
	{
		System.out.println("Starting...");
		ClassA classA = new ClassA();
		ClassB classB = new ClassB();
		MainClass mainClass = new MainClass();

		classA.setChild(classB);
		classB.setChild(classA);

		mainClass.setChildA(classA);
		mainClass.setChildB(classB);
		File serieFile = new File("c:\\temp\\serie.bin");
		try
		{
			FileMgr.forceDeleteIfPresent(serieFile);

			FileOutputStream fos = new FileOutputStream(serieFile);
			ObjectOutputStream os = new ObjectOutputStream(fos);

			os.writeObject(mainClass);
			os.flush();
			os.close();

			System.out.println(" File writed :" + serieFile.getAbsolutePath());
			if (!serieFile.isFile())
			{
				throw new IllegalStateException("Unable to find expected file " + serieFile.getAbsolutePath());
			}

			FileInputStream fis = new FileInputStream(serieFile);
			ObjectInputStream is = new ObjectInputStream(fis);
			Object obj = is.readObject();
			System.out.println(" File readed :" + serieFile.getAbsolutePath());
			if (((MainClass) obj).getChildA().getChild() == ((MainClass) obj).getChildB())
			{
				System.out.println("  Test 1 OK");
			} else
			{
				System.out.println("  Test 1 KO");
			}
			if (((MainClass) obj).getChildB().getChild() == ((MainClass) obj).getChildA())
			{
				System.out.println("  Test 2 OK");
			} else
			{
				System.out.println("  Test 2 KO");
			}

			is.close();

		} catch (Exception ex)
		{
			ex.printStackTrace();
		} finally
		{
		}

		System.out.println("Finished");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new SeriealizeTest().run();

	}

	class MainClass implements Serializable
	{
		private ClassA _childA = null;
		private ClassB _childB = null;

		public ClassB getChildB()
		{
			return _childB;
		}

		public void setChildB(ClassB p_childB)
		{
			_childB = p_childB;
		}

		public ClassA getChildA()
		{
			return _childA;
		}

		public void setChildA(ClassA p_childA)
		{
			_childA = p_childA;
		}
	}

	class ClassA implements Serializable
	{
		private ClassB _child = null;

		public ClassB getChild()
		{
			return _child;
		}

		public void setChild(ClassB p_child)
		{
			_child = p_child;
		}

	}

	class ClassB implements Serializable
	{
		private ClassA _child = null;

		public ClassA getChild()
		{
			return _child;
		}

		public void setChild(ClassA p_child)
		{
			_child = p_child;
		}

	}
}
