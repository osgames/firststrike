package org.jocelyn_chaumel.prototype.zip;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import org.jocelyn_chaumel.tools.FileMgr;

public class ZipProto
{
	static final int BUFFER = 2048;

	public static void main(String[] p_param)
	{

		final File z1 = new File("c:\\temp\\z1.zip");
		final File z2 = new File("c:\\temp\\z2.zip");
		try
		{
			File tempDir = new File("c:\\temp");
			tempDir.mkdir();

			FileMgr.forceDeleteIfPresent(z1);
			createZip(z1, "joss", "joss.txt");
			Object[] stringArray = getZipContent(z1);
			System.out.println("z1 content [" + stringArray.length + "]");
			System.out.println("   z1[0]:" + (String) stringArray[0]);

			FileMgr.forceDeleteIfPresent(z2);
			String[] stringArraySrc = { "joss.txt", "julie.txt", "william.txt" };
			createZip(z2, stringArraySrc);

			stringArray = getZipContent(z2);
			System.out.println("z2 content [" + stringArray.length + "]");
			System.out.println("   z2[0]:" + (String) stringArray[0]);
			System.out.println("   z2[1]:" + (String) stringArray[1]);
			System.out.println("   z2[2]:" + (String) stringArray[2]);

		} catch (Exception ex)
		{
			ex.printStackTrace();
		} finally
		{
			try
			{
				FileMgr.forceDeleteIfPresent(z1);
				FileMgr.forceDeleteIfPresent(z2);
			} catch (Exception ex2)
			{
			}
		}
	}

	public static void createZip(File zipFile, Object p_object, String entryName) throws IOException
	{
		FileOutputStream fileOutput = new FileOutputStream(zipFile);
		ZipOutputStream zipOutput = new ZipOutputStream(fileOutput);
		ZipEntry zipEntry = new ZipEntry(entryName);
		zipOutput.putNextEntry(zipEntry);
		ObjectOutputStream oos = new ObjectOutputStream(zipOutput);
		oos.writeObject(p_object);
		zipOutput.flush();
		zipOutput.close();
	}

	public static void createZip(File p_zipFile, final String[] p_stringArray) throws Exception
	{
		FileOutputStream fileOutput = new FileOutputStream(p_zipFile);
		ZipOutputStream zipOutput = new ZipOutputStream(fileOutput);
		ZipEntry zipEntry = null;
		ObjectOutputStream oos = null;
		ArrayList list = new ArrayList();
		for (int i = 0; i < p_stringArray.length; i++)
		{
			zipEntry = new ZipEntry(p_stringArray[i]);
			zipOutput.putNextEntry(zipEntry);
			oos = new ObjectOutputStream(zipOutput);
			oos.writeObject(p_stringArray[i]);
			list.add(oos);
			zipOutput.closeEntry();
		}
		zipOutput.flush();
		zipOutput.close();
		final Iterator oosIter = list.iterator();
		while(oosIter.hasNext()){
			((ObjectOutputStream)oosIter.next()).close();
		}
	}

	public static Object[] getZipContent(File p_zipFile) throws ZipException, IOException, ClassNotFoundException
	{
		final ZipFile zipFileMgr = new ZipFile(p_zipFile);
		ZipEntry currentEntry = null;
		InputStream currentInputS = null;
		ObjectInputStream currentObjectInputS = null;
		Object[] objectArray = new Object[zipFileMgr.size()];
		int idx = 0;
		Enumeration entryEnum = zipFileMgr.entries();
		ArrayList inputStreamList = new ArrayList();
		while (entryEnum.hasMoreElements())
		{
			currentEntry = (ZipEntry) entryEnum.nextElement();
			currentInputS = zipFileMgr.getInputStream(currentEntry);
			if (currentObjectInputS != null)
			{
				currentObjectInputS.close();
			}
			currentObjectInputS = new ObjectInputStream(currentInputS);
			inputStreamList.add(currentObjectInputS);
			objectArray[idx++] = currentObjectInputS.readObject();
		}
		zipFileMgr.close();
		return objectArray;
	}
}
