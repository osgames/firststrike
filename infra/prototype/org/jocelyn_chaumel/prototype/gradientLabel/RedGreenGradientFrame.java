package org.jocelyn_chaumel.prototype.gradientLabel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jocelyn_chaumel.RedGreenGradientJLabel;

public class RedGreenGradientFrame extends JFrame
{
	public RedGreenGradientFrame(){
		super();
		
		final JPanel mainPanel = new JPanel(new BorderLayout());
		getContentPane().add(mainPanel);
		setPreferredSize(new Dimension(200, 200));
		RedGreenGradientJLabel lable = new RedGreenGradientJLabel(0.5f);
		lable.setBackground(Color.WHITE);
		mainPanel.add(lable, BorderLayout.CENTER);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String [] p_paramArray){
		RedGreenGradientFrame frame = new RedGreenGradientFrame();
		frame.pack();
		frame.setVisible(true);
	}

}
