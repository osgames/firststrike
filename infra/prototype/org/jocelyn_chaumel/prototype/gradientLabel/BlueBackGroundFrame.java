package org.jocelyn_chaumel.prototype.gradientLabel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class BlueBackGroundFrame extends JFrame
{
	public BlueBackGroundFrame(){
		super();
		
		final JPanel mainPanel = new JPanel(new BorderLayout());
		getContentPane().add(mainPanel);
		
		mainPanel.add(new BlueLable(), BorderLayout.CENTER);
	}
	
	private class BlueLable extends JLabel {
		
		public void paint(final Graphics p_graph){
			final Dimension dim = this.getSize();
			
			float step = 100f / dim.height;
			float currentStep = 100;
			for (int i = 0 ; i < dim.height; i++){
				p_graph.setColor(new Color((int) currentStep, (int) currentStep, 254));
				currentStep += step;
				p_graph.drawLine(0, i, dim.width, i);
			}
		}
	}
	
	public static void main(String [] p_paramArray){
		BlueBackGroundFrame frame = new BlueBackGroundFrame();
		frame.pack();
		frame.setVisible(true);
	}
}
