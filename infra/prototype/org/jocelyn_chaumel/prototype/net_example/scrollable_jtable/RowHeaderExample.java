package org.jocelyn_chaumel.prototype.net_example.scrollable_jtable;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import org.jocelyn_chaumel.tools.debugging.Assert;

public class RowHeaderExample extends JFrame
{

	private static final long serialVersionUID = -3399914244647524616L;

	public RowHeaderExample()
	{
		super("Row Header Example");
		setSize(300, 150);

		ListModel lm = new AbstractListModel()
		{
			private static final long serialVersionUID = 7872622782869933336L;

			String headers[] = { "a", "b", "c", "d", "e", "f", "g", "h", "i" };

			public int getSize()
			{
				return headers.length;
			}

			public Object getElementAt(int index)
			{
				return headers[index];
			}
		};

		DefaultTableModel dm = new DefaultTableModel(lm.getSize(), 10);
		JTable table = new JTable(dm);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		JList rowHeader = new JList(lm);
		rowHeader.setFixedCellWidth(50);

		rowHeader.setFixedCellHeight(table.getRowHeight()
		// + table.getRowMargin()
				// + table.getIntercellSpacing().height
				);
		rowHeader.setCellRenderer(new RowHeaderRenderer(table));

		JScrollPane scroll = new JScrollPane(table);
		scroll.setRowHeaderView(rowHeader);
		getContentPane().add(scroll, BorderLayout.CENTER);
	}

	public static void main(String[] args)
	{
		RowHeaderExample frame = new RowHeaderExample();
		frame.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(final WindowEvent anEvent)
			{
				Assert.preconditionNotNull(anEvent, "Window Event");

				System.exit(0);
			}
		});
		frame.setVisible(true);
	}
}

/**
 * @version 1.0 11/09/98
 */

class RowHeaderRenderer extends JLabel implements ListCellRenderer
{

	private static final long serialVersionUID = 3832512938553620962L;

	RowHeaderRenderer(JTable table)
	{
		JTableHeader header = table.getTableHeader();
		setOpaque(true);
		setBorder(UIManager.getBorder("TableHeader.cellBorder"));
		setHorizontalAlignment(CENTER);
		setForeground(header.getForeground());
		setBackground(header.getBackground());
		setFont(header.getFont());
	}

	public Component getListCellRendererComponent(	JList aList,
													Object p_value,
													int p_index,
													boolean anIsSelectedFlag,
													boolean p_cellHasFocusFlag)
	{
		Assert.preconditionNotNull(aList, "List");
		Assert.preconditionNotNull(p_value, "Value");
		Assert.preconditionIsPositive(p_index, "Invalid index value");
		Assert.precondition(anIsSelectedFlag == p_cellHasFocusFlag || anIsSelectedFlag != p_cellHasFocusFlag, "");

		setText((p_value == null) ? "" : p_value.toString());
		return this;
	}
}