package org.jocelyn_chaumel.prototype;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

public class JLabelHtmlText extends JFrame
{
	/**
	 * DevDaily.com A sample program showing how to use an animated gif image in
	 * a Java Swing application.
	 */

	JPanel contentPane;
	JLabel headerLabel = new JLabel();
	JLabel _htmlLabel = new JLabel();
	JTextArea _textArea = new JTextArea();
	JTextArea _codeArea = new JTextArea();
	JScrollPane _scrollpane = new JScrollPane();
	JButton _translationMode = new JButton("???");

	public JLabelHtmlText()
	{
		try
		{
			setDefaultCloseOperation(EXIT_ON_CLOSE);
			contentPane = (JPanel) getContentPane();
			contentPane.setLayout(new BorderLayout());
			setSize(new Dimension(1000, 300));
			setTitle("Test your formated html text");

			// add the header label
			headerLabel.setFont(new java.awt.Font("Comic Sans MS", Font.BOLD, 16));
			headerLabel.setText("   Test your formated html text!");
			contentPane.add(headerLabel, BorderLayout.NORTH);
			_codeArea.setColumns(30);
			_codeArea.setLineWrap(true);
			_codeArea.setWrapStyleWord(true);
			_codeArea.setBorder(new LineBorder(Color.BLACK));
			_codeArea.addKeyListener(new KeyListener()
			{

				@Override
				public void keyTyped(KeyEvent arg0)
				{
					// N/A
				}

				@Override
				public void keyReleased(KeyEvent arg0)
				{
					displayAndTranslateToText();
				}

				@Override
				public void keyPressed(KeyEvent arg0)
				{
					// N/A

				}
			});
			contentPane.add(_codeArea, BorderLayout.EAST);

			_textArea.setColumns(40);
			_textArea.setLineWrap(true);
			_textArea.setWrapStyleWord(true);
			_textArea.setBorder(new LineBorder(Color.BLACK));
			_textArea.setRows(5);
			_textArea.setText("<html><h1>Description</h1>�crire du text ici</html>");
			_textArea.addKeyListener(new KeyListener()
			{

				@Override
				public void keyTyped(KeyEvent arg0)
				{
				}

				@Override
				public void keyReleased(KeyEvent arg0)
				{
					displayAndTranslateToCode();
				}

				@Override
				public void keyPressed(KeyEvent arg0)
				{
				}
			});
			contentPane.add(_textArea, BorderLayout.WEST);
			_translationMode.setHorizontalAlignment(JLabel.CENTER);
			_translationMode.setAction(new AbstractAction()
			{

				@Override
				public void actionPerformed(ActionEvent arg0)
				{
					if (">>".equals(_translationMode.getText()))
					{
						displayAndTranslateToText();
					} else {
						displayAndTranslateToCode();
					}

				}
			});
			contentPane.add(_translationMode, BorderLayout.SOUTH);

			contentPane.add(_scrollpane, BorderLayout.CENTER);

			displayAndTranslateToCode();
			_scrollpane.getViewport().add(_htmlLabel);
			_htmlLabel.setText(_textArea.getText());

			// show it
		} catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}

	public final void displayAndTranslateToCode()
	{
		// String text = _textArea.getText().replaceAll("\n", "<br/>");
		String text = _textArea.getText();
		_htmlLabel.setText(text);
		_translationMode.setText(">>");
		text = text.replaceAll("<", "&lt;");
		text = text.replaceAll(">", "&gt;");
		text = text.replaceAll("\n", "");
		int currentLenght = 0;
		do
		{
			currentLenght = text.length();
			text = text.replace("  ", "&amp;nbsp;&amp;nbsp;");
		} while (text.length() != currentLenght);
		do
		{
			currentLenght = text.length();
			text = text.replace("&amp;nbsp; ", "&amp;nbsp;&amp;nbsp;");
		} while (text.length() != currentLenght);

		_codeArea.setText(text);
	}

	public final void displayAndTranslateToText()
	{
		_translationMode.setText("<<");
		String text = _codeArea.getText();
		text = text.replaceAll("&lt;", "<");
		text = text.replaceAll("&gt;", ">");
		text = text.replaceAll("<p/>", "\n<p/>");
		text = text.replaceAll("&amp;nbsp;", " ");

		_htmlLabel.setText(text);

		text = text.replaceAll("<br/>", "\n");
		_textArea.setText(text);
	}

	public static void main(String[] args)
	{
		new JLabelHtmlText().setVisible(true);
	}

}
