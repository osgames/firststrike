package org.jocelyn_chaumel.prototype;

import java.util.ArrayList;

import org.jocelyn_chaumel.tools.debugging.GCResult;

public class MemoryManager {

	public static final int NB_TEST = 5;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final ArrayList<ArrayList<Double>> list = new ArrayList<ArrayList<Double>>();
		ArrayList<Double> currentList = null;
		System.out.println(getTotalMemory());
		for (int i =  0; i <= 1000; i++){
			currentList = new ArrayList<Double>();
			list.add(currentList);
			for (int j = 0; j <= 1000; j++){
				if ((j + 5) % 100 == 0){
					currentList.remove(0);
					currentList.remove(0);
					currentList.remove(0);
					currentList.remove(0);
					currentList.remove(0);
				}
				
				currentList.add(new Double(j));
			}
			System.out.println(getTotalMemory());
		}

	}
	
	public final static String getTotalMemory(){
		GCResult result = GCResult.getMemoryUsed();
		
		if (result.isGCSuccesfull()){
			return "OK " + result.getMemoryUsedBeforeGC();
		} else {
			return "KO " + result.getMemoryUsedBeforeGC();
		}
	}

}
