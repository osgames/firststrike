package org.jocelyn_chaumel.prototype.class_loaded;

import java.io.File;

public class ClassLoaderProto
{

	public static void main(String[] args)
	{
		try {
			//File fileClass = new File("D:\\Utilisateurs\\Jocelyn\\OneDrive\\Documents\\Prog\\Java\\workspace 3.4\\infra\\bin\\org\\jocelyn_chaumel\\prototype\\class_loaded\\LoadedClass.class");
			Class classToLoad = Class.forName("org.jocelyn_chaumel.prototype.class_loaded.LoadedClass");
			ILoadedClass loadedClass = (ILoadedClass) classToLoad.newInstance();
			loadedClass.start();
		
		} catch (Exception ex)
		{
			ex.printStackTrace();
			
		}
	}

}
