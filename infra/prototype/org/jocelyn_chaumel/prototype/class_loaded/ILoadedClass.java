package org.jocelyn_chaumel.prototype.class_loaded;

public interface ILoadedClass
{
	public void start();
}
