package org.jocelyn_chaumel.prototype.gif_animated;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class AnimatedGifFrame extends JFrame 
{
	/**
	  * DevDaily.com
	  * A sample program showing how to use an animated gif image
	  * in a Java Swing application.
	  */


	    JPanel contentPane;
	    JLabel imageLabel = new JLabel();
	    JLabel headerLabel = new JLabel();

	    public AnimatedGifFrame() {
	        try {
	            setDefaultCloseOperation(EXIT_ON_CLOSE);
	            contentPane = (JPanel) getContentPane();
	            contentPane.setLayout(new BorderLayout());
	            setSize(new Dimension(400, 300));
	            setTitle("Your Job Crashed!");
	            // add the header label
	            headerLabel.setFont(new java.awt.Font("Comic Sans MS", Font.BOLD, 16));
	            headerLabel.setText("   Your job crashed during the save process!");
	            contentPane.add(headerLabel, java.awt.BorderLayout.NORTH);
	            // add the image label
	            ImageIcon ii = new ImageIcon(ClassLoader.getSystemResource(
	                    "./cube.gif"));
	            imageLabel.setIcon(ii);
	            contentPane.add(imageLabel, java.awt.BorderLayout.CENTER);
	            // show it
	            this.setLocationRelativeTo(null);
	            this.setVisible(true);
	        } catch (Exception exception) {
	            exception.printStackTrace();
	        }
	    }

	    public static void main(String[] args) {
	        new AnimatedGifFrame();
	    }

	}

