package org.jocelyn_chaumel.prototype.imaging.memory;

import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.MemoryImageSource;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MemoryImagePrototype extends JFrame
{
	private MemoryImageSource _imageSource = null;
	private int _index = 0;
	final int _pix[] = new int[100 * 100];

	public MemoryImagePrototype()
	{
		final int w = 100;
		final int h = 100;
		int index = 0;
		for (int y = 0; y < h; y++)
		{
			int red = (y * 255) / (h - 1);
			for (int x = 0; x < w; x++)
			{
				int blue = (x * 255) / (w - 1);
				_pix[index++] = (255 << 24) | (red << 16) | blue;
			}
		}
		_imageSource = new MemoryImageSource(w, h, _pix, 0, w);
		_imageSource.setAnimated(true);
		_imageSource.setFullBufferUpdates(true);
		final Image image = createImage(_imageSource);
		final ImageIcon icon = new ImageIcon(image);
		BufferedImage bi = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		final Graphics graphics = bi.getGraphics();
		graphics.drawImage(icon.getImage(), 0, 0, null);
		graphics.dispose();
		final Image scaledImage = bi.getScaledInstance(20, 20, BufferedImage.SCALE_SMOOTH);

		final JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayout(1, 3));

		final JButton actionBtn = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_event)
			{
				
				int green, red, blue = 0;
				int newPix[] = new int[100 * 10];
				for (int x = 0; x < 100; x++)
				{
					for (int y = 0; y < 10; y++)
					{
						green = (_index * 20) << 24;
						blue = 255;
						red = (x * 20) << 16;
						newPix[x + (y * 100)] = green | red | blue;
					}
				}
				
				_imageSource.newPixels(	newPix,
										ColorModel.getRGBdefault(),
										_index * 100,
										100);
										/*
				for (int i = 0; i < 100; i++){
					_pix[i + _index * 100] = (200 << 24) | (100<<16)| 150;
				}
				_imageSource.newPixels(	0, 100, 100, 100);*/

				_index++;

			}
		});
		actionBtn.setText("Update Image");
		mainPanel.add(actionBtn);

		final JLabel imageLabel = new JLabel(new ImageIcon(image));
		mainPanel.add(imageLabel);
		
		final JLabel imageScaledLabel = new JLabel(new ImageIcon(scaledImage));
		mainPanel.add(imageScaledLabel);
		getContentPane().add(mainPanel);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		MemoryImagePrototype frame = new MemoryImagePrototype();

		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

}
