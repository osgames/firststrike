package org.jocelyn_chaumel.prototype.imaging;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

public class ColorTester extends JDialog
{

	private final JPanel _contentPanel = new JPanel();
	private JTextField _redStartTxt;
	private JTextField _redEndTxt;
	private JTextField _blueStartTxt;
	private JTextField _blueEndTxt;
	private JTextField _greenStartTxt;
	private JTextField _greenEndTxt;
	private JLabel _startColorResultLbl;
	private JLabel _endColorResultLbl;
	private static final String _defaultStartValue = "150";
	private static final String _defaultEndValue = "255";
	private JLabel _gradientColorResult;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		try
		{
			ColorTester dialog = new ColorTester();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ColorTester()
	{
		setBounds(100, 100, 322, 300);
		getContentPane().setLayout(new BorderLayout());
		_contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(_contentPanel, BorderLayout.CENTER);
		_contentPanel.setLayout(new MigLayout("", "[][131.00,grow][][][grow][]", "[][][][][]"));
		JLabel lblDpart = new JLabel("D\u00E9part");
		_contentPanel.add(lblDpart, "cell 1 0,alignx center");
		JLabel lblFin = new JLabel("Fin");
		_contentPanel.add(lblFin, "cell 4 0,alignx center");
		JButton minusRedStartBtn = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_redStartTxt.setBackground(new Color(minusValue(_redStartTxt), 0, 0));
			}
		});
		minusRedStartBtn.setText("-");
		minusRedStartBtn.setMargin(new Insets(1, 5, 1, 5));
		_contentPanel.add(minusRedStartBtn, "cell 0 1");
		_redStartTxt = new JTextField();
		_redStartTxt.setText(_defaultStartValue);
		_contentPanel.add(_redStartTxt, "cell 1 1,growx");
		_redStartTxt.setColumns(10);
		JButton redPlusStartBtn = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_redStartTxt.setBackground(new Color(plusValue(_redStartTxt), 0, 0));
			}
		});
		redPlusStartBtn.setText("+");
		redPlusStartBtn.setMargin(new Insets(1, 3, 1, 3));
		_contentPanel.add(redPlusStartBtn, "cell 2 1");
		JButton redMinusEndBtn = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_redEndTxt.setBackground(new Color(minusValue(_redEndTxt), 0, 0));
			}
		});
		redMinusEndBtn.setText("-");
		redMinusEndBtn.setMargin(new Insets(1, 5, 1, 5));
		_contentPanel.add(redMinusEndBtn, "cell 3 1");
		_redEndTxt = new JTextField();
		_redEndTxt.setText("255");
		_contentPanel.add(_redEndTxt, "cell 4 1,growx");
		_redEndTxt.setColumns(10);

		JButton redPlusEndBtn = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_redEndTxt.setBackground(new Color(plusValue(_redEndTxt), 0, 0));
			}
		});
		redPlusEndBtn.setText("+");
		redPlusEndBtn.setMargin(new Insets(1, 3, 1, 3));
		_contentPanel.add(redPlusEndBtn, "cell 5 1");

		JButton blueMinusStartBtn = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_blueStartTxt.setBackground(new Color(0, 0, minusValue(_blueStartTxt)));
			}
		});
		blueMinusStartBtn.setText("-");
		blueMinusStartBtn.setMargin(new Insets(1, 5, 1, 5));
		_contentPanel.add(blueMinusStartBtn, "cell 0 2");
		_blueStartTxt = new JTextField();
		_blueStartTxt.setText("125");
		_contentPanel.add(_blueStartTxt, "cell 1 2,growx");
		_blueStartTxt.setColumns(10);
		
		JButton bluePlusStartBtn = new JButton(new AbstractAction()
		{
			
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_blueStartTxt.setBackground(new Color(0, 0, plusValue(_blueStartTxt)));
			}
		});
		
		bluePlusStartBtn.setText("+");
		bluePlusStartBtn.setMargin(new Insets(1, 3, 1, 3));
		_contentPanel.add(bluePlusStartBtn, "cell 2 2,alignx left");
		
		JButton blueMinusEndBtn = new JButton(new AbstractAction()
		{
			
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_blueEndTxt.setBackground(new Color(0, 0, minusValue(_blueEndTxt)));
				
			}
		});
		blueMinusEndBtn.setText("-");
		blueMinusEndBtn.setMargin(new Insets(1, 5, 1, 5));
		_contentPanel.add(blueMinusEndBtn, "cell 3 2");
		_blueEndTxt = new JTextField();
		_blueEndTxt.setText("255");
		_contentPanel.add(_blueEndTxt, "cell 4 2,growx");
		_blueEndTxt.setColumns(10);
		
		
		JButton bluePlusEndBtn = new JButton(new AbstractAction()
		{
			
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_blueEndTxt.setBackground(new Color(0, 0, plusValue(_blueEndTxt)));
			}
		});
		bluePlusEndBtn.setText("+");
		bluePlusEndBtn.setMargin(new Insets(1, 3, 1, 3));
		_contentPanel.add(bluePlusEndBtn, "cell 5 2");
		
		
		JButton greenMinusStartBtn = new JButton(new AbstractAction()
		{
			
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_greenStartTxt.setBackground(new Color(0, minusValue(_greenStartTxt), 0));
			}
		});
		greenMinusStartBtn.setText("-");
		greenMinusStartBtn.setMargin(new Insets(1, 5, 1, 5));
		_contentPanel.add(greenMinusStartBtn, "cell 0 3");
		_greenStartTxt = new JTextField();
		_greenStartTxt.setText("125");
		_contentPanel.add(_greenStartTxt, "cell 1 3,growx");
		_greenStartTxt.setColumns(10);
		
		JButton plusGreenStartBtn = new JButton(new AbstractAction()
		{
			
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_greenStartTxt.setBackground(new Color(0, plusValue(_greenStartTxt), 0));
			}
		});
		plusGreenStartBtn.setText("+");
		plusGreenStartBtn.setMargin(new Insets(1, 3, 1, 3));
		_contentPanel.add(plusGreenStartBtn, "cell 2 3");
		
		
		JButton greenMinusEndBtn = new JButton(new AbstractAction()
		{
			
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_greenEndTxt.setBackground(new Color(0, minusValue(_greenEndTxt), 0));
			}
		});
		greenMinusEndBtn.setText("-");
		greenMinusEndBtn.setMargin(new Insets(1, 5, 1, 5));
		_contentPanel.add(greenMinusEndBtn, "cell 3 3");
		_greenEndTxt = new JTextField();
		_greenEndTxt.setText("255");
		_contentPanel.add(_greenEndTxt, "cell 4 3,growx");
		_greenEndTxt.setColumns(10);
		
		JButton greenPlusEndBtn = new JButton(new AbstractAction()
		{
			
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_greenEndTxt.setBackground(new Color(0, plusValue(_greenEndTxt), 0));
			}
		});
		greenPlusEndBtn.setText("+");
		greenPlusEndBtn.setMargin(new Insets(1, 3, 1, 3));
		_contentPanel.add(greenPlusEndBtn, "cell 5 3");
		
		_startColorResultLbl = new JLabel("Color Result");
		_startColorResultLbl.setOpaque(true);
		_contentPanel.add(_startColorResultLbl, "cell 1 4,growx");
		_endColorResultLbl = new JLabel("Color Result");
		_endColorResultLbl.setOpaque(true);
		_contentPanel.add(_endColorResultLbl, "flowx,cell 4 4");
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		_gradientColorResult = new JLabelGradient();
		_gradientColorResult.setOpaque(true);
		_gradientColorResult.setText("hey hey");

		buttonPane.add(_gradientColorResult);
		JButton okButton = new JButton("OK");
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		JButton cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
	}

	private int plusValue(final JTextField p_label)
	{
		if (!isValidNumber(p_label.getText()))
		{
			p_label.setText(_defaultEndValue);
		}
		int value = Integer.parseInt(p_label.getText());
		if (value < 255)
		{
			value++;
			p_label.setText("" + value);
		}
		refreshAllColor();
		return value;
	}

	private int minusValue(final JTextField p_label)
	{
		if (!isValidNumber(p_label.getText()))
		{
			p_label.setText(_defaultEndValue);
		}
		int value = Integer.parseInt(p_label.getText());
		if (value > 0)
		{
			value--;
			p_label.setText("" + value);
		}
		refreshAllColor();
		return value;
	}

	private class JLabelGradient extends JLabel
	{
		@Override
		public final void paint(final Graphics p_graphics)
		{
			super.paint(p_graphics);
			final Dimension dimension = this.getSize();

			int width = (int) dimension.getWidth();
			int height = (int) dimension.getHeight();

			float startRed = Integer.parseInt(_redStartTxt.getText()) / 255f;
			float startBlue = Integer.parseInt(_blueStartTxt.getText()) / 255f;
			float startGreen = Integer.parseInt(_greenStartTxt.getText()) / 255f;
			float endRed = Integer.parseInt(_redEndTxt.getText()) / 255f;
			float endBlue = Integer.parseInt(_blueEndTxt.getText()) / 255f;
			float endGreen = Integer.parseInt(_greenEndTxt.getText()) / 255f;
			float redStep = (endRed - startRed) / width;
			float blueStep = (endBlue - startBlue) / width;
			float greenStep = (endGreen - startGreen) / width;

			for (int i = 0; i < width; i++)
			{
				p_graphics.setColor(new Color(startRed, startGreen, startBlue));
				p_graphics.drawLine(i, 0, i, height);
				startRed += redStep;
				startGreen += greenStep;
				startBlue += blueStep;
			}
			p_graphics.drawString("hey hey", 1, height - 2);
		}
	}

	private boolean isValidNumber(final String p_str)
	{
		try
		{
			Integer.parseInt(p_str);
		} catch (NumberFormatException ex)
		{
			JOptionPane.showMessageDialog(this, "Invalid Number", "Error", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		return true;
	}

	private void refreshAllColor()
	{
		int startRed = Integer.parseInt(_redStartTxt.getText());
		int startBlue = Integer.parseInt(_blueStartTxt.getText());
		int startGreen = Integer.parseInt(_greenStartTxt.getText());
		_startColorResultLbl.setBackground(new Color(startRed, startGreen, startBlue));

		int endRed = Integer.parseInt(_redEndTxt.getText());
		int endBlue = Integer.parseInt(_blueEndTxt.getText());
		int endGreen = Integer.parseInt(_greenEndTxt.getText());
		_endColorResultLbl.setBackground(new Color(endRed, endGreen, endBlue));

		_gradientColorResult.setText(" hey hey ");
		int height = _gradientColorResult.getHeight();
		int width = _gradientColorResult.getWidth();
		_gradientColorResult.paintImmediately(0, 0, width, height);
	}
}
