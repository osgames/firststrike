package org.jocelyn_chaumel.prototype.imaging.doubleImage;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DoubleImageFrame extends JFrame
{
	private boolean _status = true;
	private final JLabel _label;
	private final JPanel _mainPanel = new JPanel(new GridLayout(1, 2));
	
	public DoubleImageFrame()
	{
		super();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getContentPane().add(_mainPanel);
		_label = new JLabel()
		{
			public void paint(final Graphics p_graphs)
			{
				super.paint(p_graphs);
				ImageIcon tank = new ImageIcon(ClassLoader.getSystemResource("./tank.gif"));
				Image tankImage = tank.getImage();
				p_graphs.drawImage(tankImage, 0, 0, null, null);

				if (_status)
				{
					ImageIcon explosion = new ImageIcon(ClassLoader.getSystemResource("./explosion.gif"));
					Image explosionImage = explosion.getImage();
					p_graphs.drawImage(explosionImage, 10, 0, null, null);
				}
			}
			
		};
		_label.setPreferredSize(new Dimension(30, 30));
		this.setMinimumSize(new Dimension(30, 30));
		this.setMaximumSize(new Dimension(30, 30));
		this.setSize(30, 30);

		_mainPanel.add(_label);
		final JButton button = new JButton(new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent p_e)
			{
				_status = !_status;
				//_label.paint(_mainPanel.getGraphics());
				_mainPanel.paint(_mainPanel.getGraphics());
			}
		});
		button.setText("Toggle");
		_mainPanel.add(button);
	}

	public static void main(String[] p_paramArray)
	{
		DoubleImageFrame frame = new DoubleImageFrame();
		frame.pack();
		frame.setVisible(true);
	}
}
