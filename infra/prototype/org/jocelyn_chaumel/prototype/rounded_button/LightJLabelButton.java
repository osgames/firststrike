package org.jocelyn_chaumel.prototype.rounded_button;

import java.awt.AWTEvent;
import java.awt.AWTEventMulticaster;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.jocelyn_chaumel.tools.ImageTool;

class LightJLabelButton extends JLabel
{

	ActionListener actionListener; // Post action events to listeners
	protected boolean pressed = false; // true if the button is detented.
	private ImageIcon _pressedIcon = null;
	private Icon _normalIcon = null;

	/**
	 * Constructs a RoundedButton with no label.
	 */
	public LightJLabelButton()
	{
		super();
	}

	/**
	 * Constructs a RoundedButton with the specified label.
	 * 
	 * @param label the label of the button
	 */
	public LightJLabelButton(String label)
	{
		super(label);
		enableEvents(AWTEvent.MOUSE_EVENT_MASK);
	}

	@Override
	public void setIcon(Icon icon)
	{
		if (icon == null)
		{
			_pressedIcon = null;
			_normalIcon = null;
		} else
		{

			_normalIcon = icon;
			_pressedIcon = ImageTool.darkerImage((ImageIcon) icon);
		}

		super.setIcon(_pressedIcon);
	}

	public void addActionListener(ActionListener listener)
	{
		actionListener = AWTEventMulticaster.add(actionListener, listener);
		enableEvents(AWTEvent.MOUSE_EVENT_MASK);
	}

	public void removeActionListener(ActionListener listener)
	{
		actionListener = AWTEventMulticaster.remove(actionListener, listener);
	}

	@Override
	public void processMouseEvent(MouseEvent e)
	{
		switch (e.getID())
		{
		case MouseEvent.MOUSE_PRESSED:
			// render myself inverted....
			pressed = true;
			setIcon(_pressedIcon);

			// Repaint might flicker a bit. To avoid this, you can use
			// double buffering (see the Gauge example).
			repaint();
			break;
		case MouseEvent.MOUSE_RELEASED:
			if (actionListener != null)
			{
				actionListener.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "release"));
			}
			// render myself normal again
			if (pressed == true)
			{
				pressed = false;
				setIcon(_normalIcon);

				// Repaint might flicker a bit. To avoid this, you can use
				// double buffering (see the Gauge example).
				repaint();
			}
			break;
		case MouseEvent.MOUSE_ENTERED:

			break;
		case MouseEvent.MOUSE_EXITED:
			if (pressed == true)
			{
				// Cancel! Don't send action event.
				pressed = false;

				// Repaint might flicker a bit. To avoid this, you can use
				// double buffering (see the Gauge example).
				repaint();

				// Note: for a more complete button implementation,
				// you wouldn't want to cancel at this point, but
				// rather detect when the mouse re-entered, and
				// re-highlight the button. There are a few state
				// issues that that you need to handle, which we leave
				// this an an excercise for the reader (I always
				// wanted to say that!)
			}
			break;
		}
		super.processMouseEvent(e);
	}

	private static void initComponents()
	{
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final JTextField tf = new JTextField("");

		LightJLabelButton rb = new LightJLabelButton();
		
		rb.setIcon(new ImageIcon(LightJLabelButton.class.getResource("/tank.gif")));

		rb.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent ae)
			{
				JOptionPane.showMessageDialog(frame, "You said: " + tf.getText());
			}
		});


		frame.add(tf, BorderLayout.NORTH);
		frame.add(rb);
		//rb.getInsets().set(0, 0, 0, 0);

		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				initComponents();
			}
		});
	}
}
