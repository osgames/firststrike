package org.jocelyn_chaumel.prototype.swing_worker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;
import javax.swing.SwingWorker;

import org.jocelyn_chaumel.tools.Dir;

/**
 * Searches the text files under the given directory and counts the number of
 * instances a given word is found in these file.
 * 
 * @author Albert Attard
 */
public class SearchForWordWorker extends SwingWorker<Integer, String>
{

	private static void failIfInterrupted() throws InterruptedException
	{
		if (Thread.currentThread().isInterrupted())
		{
			throw new InterruptedException("Interrupted while searching files");
		}
	}

	/** The word that is searched */
	private final String word;
	private int matches = 0;

	/**
	 * The directory under which the search occurs. All text files found under
	 * the given directory are searched.
	 */
	private final File directory;

	/** The text area where messages are written. */
	private final JTextArea messagesTextArea;

	/**
	 * Creates an instance of the worker
	 * 
	 * @param word The word to search
	 * @param directory the directory under which the search will occur. All
	 *        text files found under the given directory are searched
	 * @param messagesTextArea The text area where messages are written
	 */
	public SearchForWordWorker(final String word, final File directory, final JTextArea messagesTextArea)
	{
		this.word = word;
		this.directory = directory;
		this.messagesTextArea = messagesTextArea;
	}

	@Override
	protected Integer doInBackground() throws Exception
	{
		// The number of instances the word is found

		/*
		 * List all text files under the given directory using the Apache IO library. This process cannot be interrupted
		 * (stopped through cancellation). That is why we are checking right after the process whether it was interrupted or
		 * not.
		 */
		recursiveSearch(new Dir(directory));
		matches += Math.random() * 100;
		// Return the number of matches found
		return matches;
	}
	
	protected void recursiveSearch(final Dir p_dir) throws InterruptedException
	{
		publish("Listing all text files under the directory: " + p_dir.getDir().getAbsolutePath());
		final List<File> textFiles = p_dir.getFileArrayList();
		SearchForWordWorker.failIfInterrupted();
		publish("Found " + textFiles.size() + " file(s)");

		for (int i = 0, size = textFiles.size(); i < size; i++)
		{
			/*
			 * In order to respond to the cancellations, we need to check whether this thread (the worker thread) was
			 * interrupted or not. If the thread was interrupted, then we simply throw an InterruptedException to indicate
			 * that the worker thread was cancelled.
			 */
			SearchForWordWorker.failIfInterrupted();
			Thread.sleep(100);
			// Update the status and indicate which file is being searched.
			final File file = textFiles.get(i);
			publish("Searching file: " + file);

			/*
			 * Read the file content into a string, and count the matches using the Apache common IO and Lang libraries
			 * respectively.
			 */
			;

			// Update the progress
			setProgress((i + 1) * 100 / size);
		}
		
		for (File currentDir : p_dir.getDirList())
		{
			recursiveSearch(new Dir(currentDir));
		}
		
	}

	@Override
	protected void process(final List<String> chunks)
	{
		// Updates the messages text area
		for (final String string : chunks)
		{
			messagesTextArea.append(string);
			messagesTextArea.append("\n");
		}
	}
}