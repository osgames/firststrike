package org.jocelyn_chaumel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;

public class TestCurrentDir
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		Properties prop = System.getProperties();
		Enumeration enumKey = prop.keys();
		ArrayList<String> list = new ArrayList<String>();
		while (enumKey.hasMoreElements())
		{
			String currentKey = (String) enumKey.nextElement();
			String currentValue = prop.getProperty(currentKey);
			list.add(currentKey + " : " + currentValue);
		}

		Collections.sort(list);
		Iterator iter = list.iterator();
		while (iter.hasNext())
		{
			System.out.println(iter.next());
		}

	}

}
