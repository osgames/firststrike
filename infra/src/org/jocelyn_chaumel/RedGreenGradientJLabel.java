package org.jocelyn_chaumel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JLabel;

import org.jocelyn_chaumel.tools.debugging.Assert;

public class RedGreenGradientJLabel extends JLabel
{
	private float _percent = 0;
	
	public RedGreenGradientJLabel(){
		this(1.0f);
	}
	
	public RedGreenGradientJLabel(float p_percent){
		_percent = p_percent;
	}
	
	public void updateRedGreenGradient(final float p_percent){
		Assert.precondition(p_percent <= 1.0000, "Percent Greater than 1.0");
		Assert.precondition(p_percent >= 0.0000, "Percent Lower than 0.0");
	
		_percent = p_percent;
		paint(getGraphics());
	}
	
	public void paint(Graphics p_graphics){
		super.paint(p_graphics);
		
		final Dimension dimension = this.getSize();
		final int maxX = (int) dimension.getWidth();
		final int maxY = (int) dimension.getHeight();
		float currentPercent = 0.0f;
		int red, green;
		for (int x = 0; x <= maxX; x++){
			currentPercent = (float) x/maxX;
			if (currentPercent > _percent){
				break;
			}
			red = Math.max(0, (255 - (int) (currentPercent * 300)));
			green = (int)(255.0f * currentPercent);
			p_graphics.setColor(new Color(red, green, 0));
			p_graphics.drawLine(x, 0, x, maxY);
		}
	}
}
