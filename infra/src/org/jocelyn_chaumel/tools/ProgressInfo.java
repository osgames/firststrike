/*
 * ProgressInfo.java
 *
 * Created on 1 avril 2004, 18:13
 */

package org.jocelyn_chaumel.tools;

import java.util.ArrayList;
import java.util.Iterator;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe permet de suivre la progression d'une t�che en fonction d'une
 * valeur totale et d'une valeur courante. Il est �galement possible d'utiliser
 * un t�moin pour interrompre la t�che en cours.
 */
public final class ProgressInfo
{
	private long m_total = 0;
	private long m_current = 0;
	private boolean m_enable = true;
	private final ArrayList m_listenerList = new ArrayList();

	/**
	 * Ajuste le t�moin annoncant � la t�che d'interrompre son traitement le
	 * plus rapidement possible.
	 * 
	 * @param anEnableFlag
	 *            Valeur du t�moin.
	 */
	public final void setEnable(final boolean p_enableFlag)
	{
		m_enable = p_enableFlag;
	}

	/**
	 * Indique si la t�che doit s'interrompre le plus rapidement possible.
	 */
	public final boolean isEnabled()
	{
		return m_enable;
	}

	/**
	 * Ajuste la valeur finale de la t�che � faire.
	 */
	public final void setTotal(final long p_total)
	{
		m_total = p_total;
		fireReset();
	}

	public final void setCurrent(final long p_current)
	{
		m_current = p_current;
		fireUpdate();
	}

	public final long getTotal()
	{
		return m_total;
	}

	public final long getCurrent()
	{
		return m_current;
	}

	public final void addListener(final IProgressEvent p_listener)
	{
		Assert.precondition(p_listener != null, "Null Ref");

		m_listenerList.add(p_listener);
	}

	public final void removeListener(final IProgressEvent p_listener)
	{
		Assert.precondition(p_listener != null, "Null Ref");

		m_listenerList.remove(p_listener);
	}

	private void fireReset()
	{
		final Iterator iter = m_listenerList.iterator();
		while (iter.hasNext())
		{
			((IProgressEvent) iter.next()).resetProgress(m_total);
		}
	}

	private void fireUpdate()
	{
		final Iterator iter = m_listenerList.iterator();
		while (iter.hasNext())
		{
			((IProgressEvent) iter.next()).updateProgress(m_current);
		}
	}
}
