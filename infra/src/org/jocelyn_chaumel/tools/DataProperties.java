/*
 * Created on Sep 7, 2005
 */
package org.jocelyn_chaumel.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class DataProperties extends Properties
{
	private final static String DELIMITER = ",";
	/**
	 * Contructeur param�tris�.
	 */
	public DataProperties()
	{
		super();
	}

	/**
	 * @param p_properties
	 */
	public DataProperties(final Properties p_properties)
	{
		super(p_properties);
	}

	/**
	 * Ajoute une propri�t� en s'assurant qu'il ne s'agit pas d'une s�quence.
	 */
	public Object setProperty(final String p_property, final String p_value)
	{
		Assert.preconditionNotNull(p_property, "Property");

		return super.setProperty(p_property, p_value);
	}

	public String getProperty(final String p_property)
	{
		Assert.preconditionNotNull(p_property, "Property");
		Assert.precondition(containsKey(p_property), "Property unknown : " + p_property);

		return super.getProperty(p_property);
	}

	public final Object setIntProperty(final String p_propertyName, final int p_propertyValue)
	{
		Assert.preconditionNotNull(p_propertyName, "PropertyName");

		return setProperty(p_propertyName, "" + p_propertyValue);
	}

	public final int getIntProperty(final String p_propertyName) throws NumberFormatException
	{
		Assert.preconditionNotNull(p_propertyName, "PropertyName");

		final String value = getProperty(p_propertyName);

		return Integer.parseInt(value);
	}

	public final void setBooleanProperty(final String p_propertyName, final boolean p_propertyValue)
	{
		Assert.preconditionNotNull(p_propertyName, "Property Name");

		setProperty(p_propertyName, Boolean.toString(p_propertyValue));
	}

	public final boolean getBooleanProperty(final String p_propertyName)
	{
		Assert.preconditionNotNull(p_propertyName, "Property Name");

		final String value = getProperty(p_propertyName);
		return Boolean.valueOf(value).booleanValue();
	}

	public void store(final File p_destinationFile, final String p_comment) throws IOException
	{
		Assert.preconditionNotNull(p_destinationFile, "Destination File");
		Assert.precondition(!p_destinationFile.isFile(), "File already exist :" + p_destinationFile.getAbsolutePath());

		FileOutputStream fOutputStream = null;

		try
		{
			fOutputStream = new FileOutputStream(p_destinationFile);
			super.store(fOutputStream, p_comment);
		} finally
		{
			if (fOutputStream != null)
			{
				fOutputStream.close();
			}
		}
	}

	public void load(final File p_filename) throws IOException
	{
		Assert.preconditionNotNull(p_filename, "File");
		Assert.preconditionIsFile(p_filename);

		FileInputStream fInputStream = null;
		try
		{
			fInputStream = new FileInputStream(p_filename);
			load(fInputStream);
		} finally
		{
			if (fInputStream != null)
			{
				fInputStream.close();
			}
		}

	}

	public ArrayList<String> getSequentialListForProperty(final String p_property)
	{
		Assert.preconditionNotNull(p_property, "Property");
		Assert.precondition(containsKey(p_property + ".size"), "Property unknown : " + p_property);

		final ArrayList<String> propertyList = new ArrayList<String>();

		final int size = getIntProperty(p_property + ".size");
		String value;
		for (int i = 1; i <= size; i++)
		{
			value = getProperty(p_property + "." + i);
			propertyList.add(value);
		}

		return propertyList;
	}

	public void setSequentialListForProperty(final String p_property, final ArrayList<String> p_propertyList)
	{
		Assert.preconditionNotNull(p_property, "Property");
		Assert.preconditionNotNull(p_propertyList, "Property List");

		final int size = p_propertyList.size();
		String property = p_property + ".size";
		Assert.check(!containsKey(property), "This property is already used : " + property);
		setProperty(p_property + ".size", "" + size);
		String value;
		for (int i = 1; i <= size; i++)
		{
			value = p_propertyList.get(i - 1);
			property = p_property + "." + i;
			Assert.precondition(!containsKey(property), "This property is already used : " + property);
			setProperty(property, value);
		}
	}

	public void removeSequentialListForProperty(final String p_property)
	{
		Assert.preconditionNotNull(p_property, "Property");

		String property = p_property + ".size";
		if (containsKey(property))
		{
			final int size = getIntProperty(property);
			remove(property);
			for (int i = 1; i <= size; i++)
			{
				remove(p_property + "." + i);
			}
		}
	}

	public final void updateSequentialListForProperty(final String p_property, final ArrayList<String> p_propertyList)
	{
		removeSequentialListForProperty(p_property);
		setSequentialListForProperty(p_property, p_propertyList);
	}

	public final boolean isSequentialListExist(final String p_property)
	{
		Assert.preconditionNotNull(p_property, "Property Name");

		String property = p_property + ".size";
		return containsKey(property);
	}

	public final ArrayList<String> getPropertyAsStringList(final String p_property)
	{
		final String value = super.getProperty(p_property);
		if (value == null)
		{
			return new ArrayList<String>();
		}

		final ArrayList<String> valueList = StringManipulatorHelper.tokenize(value, DELIMITER);
		return valueList;
	}

	public final void appendStringList(final String p_property, final ArrayList<String> p_stringValueList)
	{
		Assert.preconditionNotNull(p_property, "Property Name");
		Assert.preconditionNotNull(p_stringValueList, "String Value List");
		
		final ArrayList<String> valueList = getPropertyAsStringList(p_property);
		final ArrayList<String> valueListToAppend = (ArrayList<String>) p_stringValueList.clone();
		valueListToAppend.removeAll(valueList);
		valueList.addAll(valueListToAppend);
		final String valueStringList = StringManipulatorHelper.buildCSVString(valueList, DELIMITER);
		setProperty(p_property, valueStringList);
	}
}