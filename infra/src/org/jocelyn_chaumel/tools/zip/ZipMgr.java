package org.jocelyn_chaumel.tools.zip;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.jocelyn_chaumel.tools.FileMgr;

public class ZipMgr
{
	static final int BUFFER = 2048;

	public final static void extractZip(final File zipFile, final File targetDir, final boolean overRide) throws IOException
	{
		if (!zipFile.isFile())
		{
			throw new IllegalArgumentException("Invalid zip file source");
		}

		if (targetDir.isFile())
		{
			throw new IllegalArgumentException("Invalid tardet dir");
		}

		if (!targetDir.isDirectory())
		{
			if (!targetDir.mkdirs())
			{
				throw new IllegalArgumentException("Unable to create target directory");
			}
		}

		// Zip source file
		final FileInputStream fis = new FileInputStream(zipFile);
		final BufferedInputStream bis = new BufferedInputStream(fis, BUFFER);
		ZipInputStream zis = new ZipInputStream(bis);
		ZipEntry currentZipEntry;

		// target file
		FileOutputStream fos;
		BufferedOutputStream bos;
		String currentZipEntryName;
		File currentOutputFile;
		int count;

		byte data[] = new byte[BUFFER];
		while ((currentZipEntry = zis.getNextEntry()) != null)
		{
			currentZipEntryName = currentZipEntry.getName();
			currentOutputFile = new File(targetDir, currentZipEntryName);
			if (currentZipEntry.isDirectory())
			{
				if (currentOutputFile.isDirectory())
				{
					continue;
				}
				if (!currentOutputFile.mkdirs())
				{
					throw new IOException("Unable to create following folder: " + currentOutputFile.getAbsolutePath());
				}
			} else
			{
				if (currentOutputFile.isFile())
				{
					if (!overRide)
					{
						throw new IllegalArgumentException("Target file already exists: "
								+ currentOutputFile.getAbsolutePath());
					}
					FileMgr.forceDelete(currentOutputFile);
				}
				fos = new FileOutputStream(currentOutputFile);
				bos = new BufferedOutputStream(fos, BUFFER);

				while ((count = zis.read(data, 0, BUFFER)) != -1)
				{
					bos.write(data, 0, count);
				}
				bos.flush();
				bos.close();
			}
		}
		zis.close();
	}
}
