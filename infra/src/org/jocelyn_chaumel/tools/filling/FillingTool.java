package org.jocelyn_chaumel.tools.filling;

import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FillingTool
{
	public final static CoordList fillFromCoord(final CoordList p_bag, final Coord p_startCoord)
	{
		Assert.preconditionNotNull(p_bag, "Bag");
		Assert.preconditionNotNull(p_startCoord, "Start Coord");
		Assert.precondition(p_bag.contains(p_startCoord), "Bag param doesn't contains the expected start coord");

		final CoordList filledCoord = new CoordList();

		fillFromCoord(p_bag, filledCoord, p_startCoord);
		return filledCoord;
	}

	private static final void fillFromCoord(final CoordList p_bag,
											final CoordList p_filledCoordBag,
											final Coord p_startCoord)
	{		
		if (!p_bag.contains(p_startCoord)){
			return;
		}
		final int minX = propagateToLeft(p_bag, p_filledCoordBag, p_startCoord);
		final int maxX = propagateToRight(p_bag, p_filledCoordBag, p_startCoord);

		final int y = p_startCoord.getY();
		Coord currentCoord = null;
		for (int x = minX; x <= maxX; x++)
		{
			currentCoord = new Coord(x, y);
			p_filledCoordBag.add(currentCoord);
			p_bag.remove(currentCoord);
		}

		propagateToDown(p_bag, p_filledCoordBag, minX, maxX, y + 1);
		propagateToTop(p_bag, p_filledCoordBag, minX, maxX, y - 1);

	}

	private static final void propagateToDown(	final CoordList p_bag,
												final CoordList p_filledCoordBag,
												final int p_minX,
												final int p_maxX,
												final int p_y)
	{
		int newMinX = p_minX;
		int newMaxX = p_maxX;
		if (isLeftLargest(p_bag, p_minX, p_y))
		{
			newMinX = propagateToLeft(p_bag, p_filledCoordBag, new Coord(p_minX - 1, p_y));
		}

		if (isRightLargest(p_bag, p_maxX, p_y))
		{
			newMaxX = propagateToRight(p_bag, p_filledCoordBag, new Coord(p_maxX + 1, p_y));
		}

		int firstX = -1;
		Coord currentCoord = null;
		for (int x = newMinX; x <= newMaxX; x++)
		{
			currentCoord = new Coord(x, p_y);
			if (p_bag.remove(currentCoord))
			{
				p_filledCoordBag.add(currentCoord);
				if (firstX == -1)
				{
					firstX = x;
				}
			} else if (firstX != -1)
			{
				propagateToDown(p_bag, p_filledCoordBag, firstX, x - 1, p_y + 1);
				firstX = -1;
			}
		}

		if (firstX != -1)
		{
			propagateToDown(p_bag, p_filledCoordBag, firstX, newMaxX, p_y + 1);
		}

		if (newMinX < p_minX)
		{
			propagateToTop(p_bag, p_filledCoordBag, Math.min(newMinX, p_minX - 1), p_minX - 1, p_y - 1);
			propagateToDown(p_bag, p_filledCoordBag, Math.min(newMinX, p_minX - 1), p_minX - 1, p_y);
			// fillFromCoord(p_bag, p_filledCoordBag, new Coord(p_minX - 2, p_y - 1));
		}

		if (newMaxX > p_maxX)
		{
			propagateToTop(p_bag, p_filledCoordBag, p_maxX + 1, Math.max(newMaxX, p_maxX + 1), p_y - 1);
			propagateToDown(p_bag, p_filledCoordBag, p_maxX + 1, Math.max(newMaxX, p_maxX + 1), p_y);
			// fillFromCoord(p_bag, p_filledCoordBag, new Coord(p_maxX + 2, p_y - 1));
		}
	}

	private static final void propagateToTop(	final CoordList p_bag,
												final CoordList p_filledCoordBag,
												final int p_minX,
												final int p_maxX,
												final int p_y)
	{
		int newMinX = p_minX;
		int newMaxX = p_maxX;
		if (isLeftLargest(p_bag, p_minX, p_y))
		{
			newMinX = propagateToLeft(p_bag, p_filledCoordBag, new Coord(p_minX - 1, p_y));
		}

		if (isRightLargest(p_bag, p_maxX, p_y))
		{
			newMaxX = propagateToRight(p_bag, p_filledCoordBag, new Coord(p_maxX + 1, p_y));
		}

		int firstX = -1;
		Coord currentCoord = null;
		for (int x = newMinX; x <= newMaxX; x++)
		{
			currentCoord = new Coord(x, p_y);
			if (p_bag.remove(currentCoord))
			{
				p_filledCoordBag.add(currentCoord);
				if (firstX == -1)
				{
					firstX = x;
				}
			} else if (firstX != -1)
			{
				propagateToTop(p_bag, p_filledCoordBag, firstX, x - 1, p_y - 1);
				firstX = -1;
			}
		}

		if (firstX != -1)
		{
			propagateToTop(p_bag, p_filledCoordBag, firstX, newMaxX, p_y - 1);
		}

		if (newMinX < p_minX)
		{
			propagateToDown(p_bag, p_filledCoordBag, Math.min(newMinX, p_minX - 2), p_minX - 1, p_y + 1);
			propagateToTop(p_bag, p_filledCoordBag, Math.min(newMinX, p_minX - 2), p_minX - 1, p_y);
			//fillFromCoord(p_bag, p_filledCoordBag, new Coord(p_minX - 2, p_y + 1));
		}

		if (newMaxX > p_maxX)
		{
			propagateToDown(p_bag, p_filledCoordBag, p_maxX + 1, Math.max(newMaxX, p_maxX + 1), p_y + 1);
			propagateToTop(p_bag, p_filledCoordBag, p_maxX + 1, Math.max(newMaxX, p_maxX + 1), p_y);
			//fillFromCoord(p_bag, p_filledCoordBag, new Coord(p_maxX + 2, p_y + 1));
		}
	}

	private static final boolean isLeftLargest(final CoordList p_bag, final int p_x, final int p_y)
	{
		final Coord coord = new Coord(p_x - 1, p_y);
		return p_bag.contains(coord);
	}

	private static final boolean isRightLargest(final CoordList p_bag, final int p_x, final int p_y)
	{
		final Coord coord = new Coord(p_x + 1, p_y);
		return p_bag.contains(coord);
	}

	private static final int propagateToLeft(	final CoordList p_bag,
												final CoordList p_filledCoordBag,
												final Coord p_startCoord)
	{
		if (!p_bag.contains(p_startCoord))
		{
			return -1;
		}
		final int y = p_startCoord.getY();
		int x = p_startCoord.getX();
		Coord currentCoord = null;
		while (true)
		{
			x--;
			currentCoord = new Coord(x, y);
			if (!p_bag.contains(currentCoord))
			{
				x++;
				break;
			}
		}

		return x;
	}

	private static final int propagateToRight(	final CoordList p_bag,
												final CoordList p_filledCoordBag,
												final Coord p_startCoord)
	{
		if (!p_bag.contains(p_startCoord))
		{
			return -1;
		}
		final int y = p_startCoord.getY();
		int x = p_startCoord.getX();
		Coord currentCoord = null;
		while (true)
		{
			x++;
			currentCoord = new Coord(x, y);
			if (!p_bag.contains(currentCoord))
			{
				x--;
				break;
			}
		}

		return x;
	}
}
