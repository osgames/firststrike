/*
 * Dir.java
 *
 * Created on October 25, 2003, 4:04 PM
 */

package org.jocelyn_chaumel.tools;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe permet de connaître le contenu d'un répertoire.
 * 
 * @author Jocelyn
 */
public final class Dir
{
	private final File _dir;

	private File[] _fileList = null;
	private File[] _dirList = null;

	/**
	 * Contructeur paramétrisé.
	 * 
	 * @param aDir
	 *            Répertoire de travail.
	 */
	public Dir(final File aDir)
	{
		Assert.precondition(aDir != null, "Null Ref");
		Assert.precondition(aDir.isDirectory(), "Invalid Directory");

		_dir = aDir;
	}

	public final File[] getDirList()
	{
		if (_dirList == null)
		{
			parseCurrentDir();
		}

		return _dirList;
	}

	public final File[] getFileList()
	{
		if (_fileList == null)
		{
			parseCurrentDir();
		}

		return _fileList;
	}
	
	public final ArrayList<File> getFileArrayList()
	{
		if (_fileList == null)
		{
			parseCurrentDir();
		}

		ArrayList<File> fileArrayList = new ArrayList<File>();
		for (int i = 0;i < _fileList.length;i++){
			fileArrayList.add(_fileList[i]);
		}
		return fileArrayList;
	}

	public final File getDir()
	{
		return _dir;
	}

	/**
	 * Recherche et classifie tous les fichiers et les répertoires du répertoire
	 * courant.
	 */
	private void parseCurrentDir()
	{
		final String[] itemList = _dir.list();
		final List dirList = new ArrayList();
		final List fileList = new ArrayList();
		File currentItem;

		for (int i = 0; i < itemList.length; i++)
		{
			currentItem = new File(_dir, itemList[i]);
			if (currentItem.isDirectory())
			{
				dirList.add(currentItem);
			} else
			{
				fileList.add(currentItem);
			}
		}
		_dirList = (File[]) dirList.toArray(new File[0]);
		_fileList = (File[]) fileList.toArray(new File[0]);
		Arrays.sort(_dirList);
		Arrays.sort(_fileList);
	}
	
	public File[] getFileEndingWith(final String p_endsWith){
		final File[] fileArray = _dir.listFiles(new FilenameFilter()
		{
			
			@Override
			public boolean accept(File p_dir, String p_filename)
			{
				
				return p_filename.endsWith(p_endsWith);
			}
		});
		
		return fileArray;
	}

}
