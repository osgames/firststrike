/*
 * CSDReader.java
 *
 * Created on 12 juin 2004, 09:53
 */

package org.jocelyn_chaumel.tools;

import java.io.File;
import java.text.ParseException;
import java.util.Date;

/**
 * Comma Seperated Data Reader permet de lire un fichier text dont les valeurs
 * sont s�par�es par une valeur.
 * 
 * @author Jocelyn Chaumel
 */
public class CSDReader extends CSSReader
{
	/** Creates a new instance of CSDReader */
	public CSDReader(final File aCSDFile)
	{
		super(aCSDFile);
	}

	public final int nextTokenAsInt() throws ParseException
	{
		int ret;
		try
		{
			ret = Integer.parseInt(nextToken());
		} catch (NumberFormatException ex)
		{
			throw new ParseException(ex.getMessage(), 0);
		}
		return ret;
	}

	public final long nextTokenAsLong() throws ParseException
	{
		long ret;
		try
		{
			ret = Long.parseLong(nextToken());
		} catch (NumberFormatException ex)
		{
			throw new ParseException(ex.getMessage(), 0);
		}
		return ret;
	}

	public final float nextTokenAsFloat() throws ParseException
	{
		float ret;
		try
		{
			ret = Float.parseFloat(nextToken());
		} catch (NumberFormatException ex)
		{
			throw new ParseException(ex.getMessage(), 0);
		}
		return ret;
	}

	public final double nextTokenAsDouble() throws ParseException
	{
		double ret;
		try
		{
			ret = Double.parseDouble(nextToken());
		} catch (NumberFormatException ex)
		{
			throw new ParseException(ex.getMessage(), 0);
		}
		return ret;
	}

	public final Date nextTokenAsJavaDate() throws ParseException
	{
		return new Date(nextTokenAsLong());
	}
}