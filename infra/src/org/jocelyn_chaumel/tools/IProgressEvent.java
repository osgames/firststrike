/*
 * IProgressEvent.java
 *
 * Created on 1 avril 2004, 19:06
 */

package org.jocelyn_chaumel.tools;

/**
 * Interface pour capter les �v�nements pouvant survenir lorsqu'une t�che
 * "progresssive" s'ex�cute.
 */
public interface IProgressEvent
{
	/**
	 * Survient lorsque le compteur total est remis � 0.
	 */
	public void resetProgress(final long aNewTotal);

	/**
	 * Survient lorsque la position courante est modifi�e.
	 */
	public void updateProgress(final long aNewCurrent);
}
