/*
 * Created on Mar 26, 2005
 */
package org.jocelyn_chaumel.tools.data_type;

import java.util.ArrayList;
import java.util.Random;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public abstract class AbstractInstanceArrayList extends ArrayList
{

	private final Class _instanceKey;
	private final String _shortName;

	public AbstractInstanceArrayList(final Class anInstanceKey, final String aShortName)
	{
		Assert.preconditionNotNull(anInstanceKey, "Instance Key");
		Assert.preconditionNotNull(aShortName, "Short Name");

		_instanceKey = anInstanceKey;
		_shortName = aShortName;
	}

	/**
	 * Constructeur param�tris� permettant d'initialiser la liste avec les
	 * valeurs contenues dans le tableau re�u en param�tre.
	 * 
	 * @param anInstanceKey
	 *            Type de la classe contenu dans cette liste.
	 * @param aShortName
	 *            Nom court de la liste. (Utilis� dans la m�thode toString).
	 * @param p_objectArray
	 *            Valeurs initiales.
	 */
	public AbstractInstanceArrayList(final Class anInstanceKey, final String aShortName, final Object[] p_objectArray)
	{
		this(anInstanceKey, aShortName);

		Assert.preconditionNotNull(p_objectArray, "Object Array");

		for (int i = 0; i < p_objectArray.length; i++)
		{
			Assert.preconditionNotNull(p_objectArray[i], "Object item[" + i + "]");
			add(p_objectArray[i]);
		}
	}

	public final boolean add(final Object p_value)
	{
		Assert.preconditionNotNull(p_value, "Value");
		Assert.precondition(_instanceKey.isInstance(p_value), "Invalid Instance Value");

		return super.add(p_value);
	}

	public final void add(final int index, Object p_value)
	{

		Assert.preconditionNotNull(p_value, "Value");
		Assert.precondition(_instanceKey.isInstance(p_value), "Invalid Value Instance");
		super.add(index, p_value);
	}

	public boolean contains(final Object p_value)
	{

		Assert.preconditionNotNull(p_value, "Value");
		Assert.precondition(_instanceKey.isInstance(p_value), "Invalid Value Instance");

		return super.contains(p_value);
	}

	public int indexOf(final Object p_value)
	{

		Assert.preconditionNotNull(p_value, "Value");
		Assert.precondition(_instanceKey.isInstance(p_value), "Invalid Value Instance");

		return super.indexOf(p_value);
	}

	public int lastIndexOf(final Object p_value)
	{
		Assert.preconditionNotNull(p_value, "Value");
		Assert.precondition(_instanceKey.isInstance(p_value), "Invalid Value Instance");

		return super.lastIndexOf(p_value);
	}

	public Object set(int index, Object p_value)
	{
		Assert.preconditionNotNull(p_value, "Value");
		Assert.precondition(_instanceKey.isInstance(p_value), "Invalid Value Instance");

		return super.set(index, p_value);
	}

	public final String toString()
	{

		final StringBuffer sb = new StringBuffer();

		sb.append("{").append(_shortName.toUpperCase());
		final int size = size();
		sb.append("[").append(size).append("]= ");
		for (int i = 0; i < size; i++)
		{
			if (i > 0)
			{
				sb.append(", ");
			}
			sb.append(i).append(":");
			sb.append(get(i));
		}
		sb.append("}");

		return sb.toString();
	}

	public final Object getRandomItem()
	{
		if (isEmpty())
		{
			return null;
		}
		final int size = size();
		Random rnd = new Random();
		final int index = rnd.nextInt(size);

		final Object object = get(index);

		return object;
	}

	public final Object removeRandomItem()
	{
		final Object object = getRandomItem();
		remove(object);

		return object;
	}
}
