/*
 * Coord.java Created on 23 f�vrier 2003, 12:03
 */

package org.jocelyn_chaumel.tools.data_type;

import java.io.Serializable;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe d�finit une coordonn�e X Y.
 */
public final class Coord implements Comparable, Serializable
{

	public static final int NORTH_EAST = 1;
	public static final int NORTH = 2;
	public static final int NORTH_WEST = 3;
	public static final int EAST = 4;
	public static final int NOT_APPLICABLE = 5;
	public static final int WEST = 6;
	public static final int SOUHT_EAST = 7;
	public static final int SOUHT = 8;
	public static final int SOUHT_WEST = 9;

	private int _x;
	private int _y;

	/**
	 * Constructeur par d�faut.
	 */
	public Coord()
	{
		_x = -1;
		_y = -1;
	}

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param x
	 *            Position x.
	 * @param y
	 *            Position y.
	 */
	public Coord(final int x, final int y)
	{
		_x = x;
		_y = y;
	}

	public Coord(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		_x = p_coord._x;
		_y = p_coord._y;
	}

	public final int getX()
	{
		return _x;
	}

	public final int getY()
	{
		return _y;
	}

	public final void setX(int x)
	{
		_x = x;
	}

	public final void setY(int y)
	{
		_y = y;
	}

	public final int hashCode()
	{
		return toString().hashCode();
	}

	public final Coord duplicateAddXY(final int p_x, final int p_y)
	{
		return new Coord(p_x + _x, p_y + _y);
	}

	/**
	 * Retourne le contenu d'une instance de type <code>Coord</code> sous la
	 * forme d'une <code>String</code>.
	 * 
	 * @return Contenue de la classe sous forme d'une <code>String</code>.
	 */
	public final String toString()
	{
		return "{COORD= " + _x + ", " + _y + "}";
	}

	public final String toStringLight()
	{
		return _x + ", " + _y;

	}

	/**
	 * Permet de faire la comparaison entre 2 objet de type Coord.
	 * 
	 * @param p_object
	 *            Objet � comparer.
	 * @return Vrai ou faux.
	 */
	public final boolean equals(Object p_object)
	{
		if (!(p_object instanceof Coord))
		{
			return false;
		}

		if (_x != ((Coord) p_object)._x || _y != ((Coord) p_object)._y)
		{
			return false;
		}
		return true;
	}

	/**
	 * @param p_coord
	 * @return
	 */
	public final double calculateDistanceDouble(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord);

		int distanceX = Math.abs(p_coord.getX() - _x);
		int distanceY = Math.abs(p_coord.getY() - _y);
		int distance = Math.max(distanceX, distanceY);
		if (distance == 0)
		{
			return 0d;
		}
		int delta = calculateDelta(p_coord);
		if (delta == 0)
		{
			return distance;
		}
		double deltaLog = delta / ((double) distance + 1d);
		Assert.check(delta >= 1.0d, "Invalid result (more than 1)");

		double distanceAndDelta = distance + deltaLog;

		return distanceAndDelta;
	}

	public final int calculateIntDistance(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord);
		int distanceX = Math.abs(p_coord.getX() - _x);
		int distanceY = Math.abs(p_coord.getY() - _y);
		int distance = Math.max(distanceX, distanceY);

		return distance;
	}

	public final int calculateDelta(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord);

		int distanceX = Math.abs(p_coord.getX() - _x);
		int distanceY = Math.abs(p_coord.getY() - _y);

		return Math.min(distanceX, distanceY);
	}

	public final boolean isInRange(final Coord p_coord, final int p_range)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		Assert.precondition(p_range >= 0, "Invalid Range");

		if (p_coord._x >= _x - p_range && p_coord._x <= _x + p_range)
		{
			if (p_coord._y >= _y - p_range && p_coord._y <= _y + p_range)
			{
				return true;
			}
		}

		return false;
	}

	public final Object clone()
	{
		final Coord coord = new Coord(_x, _y);

		return coord;
	}

	public int compareTo(final Object p_coord)
	{
		if (p_coord == null || !(p_coord instanceof Coord))
		{
			return 1;
		}

		final Coord coord = (Coord) p_coord;
		if (_y > coord._y)
		{
			return 1;
		} else if (_y < coord._y)
		{
			return -1;
		}

		if (_x > coord._x)
		{
			return 1;
		} else if (_x < coord._x)
		{
			return -1;
		}

		return 0;
	}

	public final static Coord getCoord(final String p_coordStr)
	{
		if (p_coordStr == null)
		{
			return null;
		}

		String coordStr;
		if (p_coordStr.startsWith("Coord:"))
		{
			coordStr = p_coordStr.substring("Coord:".length());
		} else
		{
			coordStr = p_coordStr;
		}

		if (coordStr.startsWith("(") && coordStr.endsWith(")"))
		{
			coordStr = coordStr.substring(1, coordStr.length() - 2);
		}

		if (coordStr.startsWith("[") && coordStr.endsWith("]"))
		{
			coordStr = coordStr.substring(1, coordStr.length() - 2);
		}

		if (coordStr.startsWith("{") && coordStr.endsWith("}"))
		{
			coordStr = coordStr.substring(1, coordStr.length() - 2);
		}

		int position = p_coordStr.indexOf(",");
		if (position == -1)
		{
			return null;
		}

		final String xStr = coordStr.substring(0, position).trim();
		final String yStr = coordStr.substring(position + 1).trim();
		final int x;
		final int y;
		try
		{
			x = Integer.parseInt(xStr);
			y = Integer.parseInt(yStr);
		} catch (NumberFormatException ex)
		{
			return null;
		}
		final Coord coord = new Coord(x, y);

		return coord;
	}

	public final int getOrientation(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		Assert.precondition(p_coord.calculateIntDistance(this) <= 1, "Invalid distance between these coord");

		if (_x > p_coord.getX() && _y > p_coord.getY())
		{
			return NORTH_EAST;
		} else if (_x == p_coord.getX() && _y > p_coord.getY())
		{
			return NORTH;
		} else if (_x < p_coord.getX() && _y > p_coord.getY())
		{
			return NORTH_WEST;
		} else if (_x > p_coord.getX() && _y == p_coord.getY())
		{
			return EAST;
		} else if (_x == p_coord.getX() && _y == p_coord.getY())
		{
			return NOT_APPLICABLE;
		} else if (_x < p_coord.getX() && _y == p_coord.getY())
		{
			return WEST;
		} else if (_x > p_coord.getX() && _y < p_coord.getY())
		{
			return SOUHT_EAST;
		} else if (_x == p_coord.getX() && _y < p_coord.getY())
		{
			return SOUHT;
		} else if (_x < p_coord.getX() && _y < p_coord.getY())
		{
			return SOUHT_WEST;
		} else
		{
			throw new IllegalArgumentException("Unable to calculate the orientation :" + p_coord);
		}
	}
}
