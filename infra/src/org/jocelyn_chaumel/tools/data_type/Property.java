/*
 * Created on 18 nov. 2006
 */
package org.jocelyn_chaumel.tools.data_type;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class Property
{
	private final String _name;
	private String _value = null;

	public Property(final String aName, final String p_value)
	{
		Assert.preconditionNotNull(aName, "Property Name");
		Assert.preconditionNotNull(p_value, "Property Value");

		_name = aName;
		_value = p_value;
	}

	/**
	 * Retourne la valeur de la propri�t�.
	 * 
	 * @return Valeur de la propri�t�.
	 */
	public final String getValue()
	{
		return _value;
	}

	/**
	 * Ajuste la valeur de la propri�t�.
	 * 
	 * @param p_value
	 *            Valeur.
	 */
	public final void setValue(final String p_value)
	{
		_value = p_value;
	}

	/**
	 * Retourne le nom de la propri�t�.
	 * 
	 * @return Nom.
	 */
	public final String getName()
	{
		return _name;
	}
}
