/*
 * Created on Mar 26, 2005
 */
package org.jocelyn_chaumel.tools.data_type;

import java.util.HashSet;
import java.util.Iterator;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public abstract class AbstractInstanceHashSet extends HashSet
{

	private final Class _instanceKey;
	private final String _shortName;

	public AbstractInstanceHashSet(final Class anInstanceKey, final String aShortName)
	{
		Assert.preconditionNotNull(anInstanceKey, "Instance Key");
		Assert.preconditionNotNull(aShortName, "Short Name");

		_instanceKey = anInstanceKey;
		_shortName = aShortName;
	}

	/**
	 * Constructeur param�tris� permettant d'initialiser la liste avec les
	 * valeurs contenues dans le tableau re�u en param�tre.
	 * 
	 * @param anInstanceKey
	 *            Type de la classe contenu dans cette liste.
	 * @param aShortName
	 *            Nom court de la liste. (Utilis� dans la m�thode toString).
	 * @param p_objectArray
	 *            Valeurs initiales.
	 */
	public AbstractInstanceHashSet(final Class anInstanceKey, final String aShortName, final Object[] p_objectArray)
	{
		this(anInstanceKey, aShortName);

		Assert.preconditionNotNull(p_objectArray, "Object Array");

		for (int i = 0; i < p_objectArray.length; i++)
		{
			Assert.preconditionNotNull(p_objectArray[i], "Object item[" + i + "]");
			add(p_objectArray[i]);
		}
	}

	public final boolean add(final Object p_value)
	{
		Assert.preconditionNotNull(p_value, "Value");
		Assert.precondition(_instanceKey.isInstance(p_value), "Invalid Instance Value");

		return super.add(p_value);
	}

	public boolean contains(final Object p_value)
	{

		Assert.preconditionNotNull(p_value, "Value");
		Assert.precondition(_instanceKey.isInstance(p_value), "Invalid Value Instance. Expected:" + _instanceKey.getName() + "  Observed:" + p_value.getClass().getName());

		return super.contains(p_value);
	}

	public final String toString()
	{

		final StringBuffer sb = new StringBuffer();
		sb.append("{").append(_shortName.toUpperCase());
		final int size = size();
		sb.append("[").append(size).append("]= ");
		final Iterator iter = iterator();
		boolean isFirst = true;
		while (iter.hasNext())
		{
			if (isFirst)
			{
				sb.append(", ");
			}
			sb.append("item:");
			sb.append(iter.next());
		}
		sb.append("}");

		return sb.toString();

	}
}
