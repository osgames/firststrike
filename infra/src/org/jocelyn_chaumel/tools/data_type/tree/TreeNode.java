/*
 * Created on Jul 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public final class TreeNode implements INodeId
{
	private final NodeDefinition _nodeDefinition;
	private final int _distance;
	private final TreeNodeList _childTreeNodeList = new TreeNodeList();
	private final TreeNode _parentNode;

	/**
	 * Constructeur paramétrisé.
	 */
	public TreeNode(final NodeDefinition aNodeDefinition)
	{
		Assert.preconditionNotNull(aNodeDefinition, "Node Id");
		_nodeDefinition = aNodeDefinition;
		_distance = 0;
		_parentNode = null;
	}

	public TreeNode(final NodeDefinition aNodeDefinition, final int aDistance, final TreeNode aParentNode)
	{
		Assert.preconditionNotNull(aNodeDefinition, "Node Id");
		Assert.preconditionNotNull(aParentNode, "Parent Node");

		_nodeDefinition = aNodeDefinition;
		_distance = aDistance;
		_parentNode = aParentNode;
	}

	public final Object getObjectRef()
	{
		return _nodeDefinition.getObjectRef();
	}

	public final NodeId getNodeId()
	{
		return _nodeDefinition.getNodeId();
	}

	public final int getDistance()
	{
		return _distance;
	}

	public final boolean isAParentNode(final NodeDefinition aNodeDefinition)
	{
		Assert.preconditionNotNull(aNodeDefinition, "Node Definition");

		if (_parentNode == null)
		{
			return false;
		}
		if (aNodeDefinition.equals(_parentNode.getNodeDefinition()))
		{
			return true;
		}

		return _parentNode.isAParentNode(aNodeDefinition);

	}

	public final void addChildNode(final TreeNode aTreeNode)
	{
		Assert.preconditionNotNull(aTreeNode, "Tree Node");
		Assert.precondition(!isAParentNode(aTreeNode.getNodeDefinition()), "This node is a parent node");
		Assert.precondition(aTreeNode._parentNode != null, "Invalid Child Node");
		Assert.check(aTreeNode._parentNode.equals(this), "Invalid Parent Node");

		_childTreeNodeList.add(aTreeNode);
	}

	public final NodeDefinition getNodeDefinition()
	{
		return _nodeDefinition;
	}

	public final TreeNodeList getPath()
	{
		TreeNodeList path;
		if (_parentNode != null)
		{
			path = _parentNode.getPath();
			path.add(this);
		} else
		{
			path = new TreeNodeList();
			path.add(this);
		}

		return path;
	}

	public final String toString()
	{
		final StringBuffer sb = new StringBuffer();

		sb.append("{TREE NODE= ");
		sb.append("dist.:").append(_distance);
		sb.append(", nodeDef:").append(_nodeDefinition);
		if (_parentNode == null)
		{
			sb.append(", parent:null");
		} else
		{
			sb.append(", parent:").append(_parentNode.getNodeId());
		}
		sb.append(", child:").append(_childTreeNodeList);
		sb.append("}");

		return sb.toString();
	}
}
