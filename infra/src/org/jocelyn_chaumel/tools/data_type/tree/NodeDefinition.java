/*
 * Created on Jul 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

import java.util.ArrayList;
import java.util.Iterator;

import org.jocelyn_chaumel.tools.data_type.FSUtil;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class NodeDefinition implements INodeId
{

	private final NodeId _nodeDefintionId;

	/**
	 * Contient la liste de tous les autres noeuds accessibles depuis ce noeud.
	 */
	private final AccessibleNodeList _nodeList = new AccessibleNodeList();

	/**
	 * Contient une référence vers l'objet "utilisateur".
	 */
	private final Object _ref;

	/**
	 * Constructeur paramétrisé.
	 */
	public NodeDefinition(final NodeId aNodeId)
	{
		this(aNodeId, null);
	}

	public NodeDefinition(final NodeId aNodeId, final Object p_objectRef)
	{
		Assert.preconditionNotNull(aNodeId, "Tree Node Id");
		_nodeDefintionId = aNodeId;
		_ref = p_objectRef;
	}

	public final NodeId getNodeId()
	{
		return _nodeDefintionId;
	}

	public final Object getObjectRef()
	{
		return _ref;
	}
	
	public final void addAccessibleNode(final AccessibleNode p_accessibleNode){
		Assert.preconditionNotNull(p_accessibleNode, "Node Definition");
		Assert.precondition(!this.equals(p_accessibleNode._nodeDefinition), "Invalid Node Definition Id");
		Assert.precondition(!_nodeList.contains(p_accessibleNode), "Already in the list");

		_nodeList.add(p_accessibleNode);
	}

	public final int getDistanceForANode(final NodeDefinition aNodeDefinition)
	{
		Assert.preconditionNotNull(aNodeDefinition, "Node Id");

		Iterator iter = _nodeList.iterator();
		AccessibleNode currentNode = null;
		boolean isFound = false;
		while (iter.hasNext())
		{
			currentNode = (AccessibleNode) iter.next();
			if (currentNode._nodeDefinition.equals(aNodeDefinition))
			{
				isFound = true;
				break;
			}
		}

		if (!isFound)
		{
			Assert.fail("Invalid Node Id");
		}

		return currentNode._distance;
	}

	public final AccessibleNode[] getAccessibleNodes()
	{
		return (AccessibleNode[]) _nodeList.toArray(new AccessibleNode[0]);
	}

	private class AccessibleNodeList extends ArrayList<AccessibleNode>
	{
		@Override
		public final String toString(){
			return FSUtil.toStringList(AccessibleNodeList.class.getName(), this);
		}
	}

	public final boolean equals(final Object p_object)
	{
		if (p_object == null)
		{
			return false;
		}

		if (!(p_object instanceof NodeDefinition))
		{
			return false;
		}

		NodeDefinition aNodeDef = (NodeDefinition) p_object;

		if (!aNodeDef.getNodeId().equals(_nodeDefintionId))
		{
			return false;
		}

		return true;
	}

	public String toString()
	{
		final StringBuffer sb = new StringBuffer();
		sb.append("{NODE DEF= ");
		sb.append("id:").append(_nodeDefintionId);
		sb.append(", ref:").append(_ref);
		sb.append(", child:").append(_nodeList);
		sb.append("}");
		return sb.toString();
	}
}