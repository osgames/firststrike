/*
 * Created on Jul 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

/**
 * @author Jocelyn Chaumel
 */
public class TreeNodeList extends AbstractNodeIdArrayList
{

	/**
	 * Constructeur par d�faut.
	 */
	public TreeNodeList()
	{
		super(TreeNode.class, "Tree Node List");
	}

}
