/*
 * Created on Jul 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class Tree
{

	private final NodeDefinitionList _nodeDefintionList;

	/**
	 * Constructeur paramétrisé.
	 */
	public Tree(final NodeDefinitionList aNodeDefinitionList)
	{
		Assert.preconditionNotNull(aNodeDefinitionList, "Node Definition List");
		Assert.precondition(aNodeDefinitionList.size() > 1, "Invalid Node Definition List");

		_nodeDefintionList = aNodeDefinitionList;
	}

	public TreeNodeList getPath(final NodeDefinition p_startNode, final NodeDefinition p_destinationNode) throws TreePathNotFound
	{
		Assert.preconditionNotNull(p_startNode, "Start Node");
		Assert.preconditionNotNull(p_destinationNode, "Destination Node");
		Assert.precondition(p_startNode != p_destinationNode, "Same start and destination node");
		Assert.precondition(_nodeDefintionList.contains(p_startNode), "Unknown Start Node");
		Assert.precondition(_nodeDefintionList.contains(p_destinationNode), "Unknown Destination Node");

		TreeNode rootNode = new TreeNode(p_startNode);

		final TreeNodeList path = findPath(rootNode, Integer.MAX_VALUE, p_destinationNode);
		if (path == null)
		{
			throw new TreePathNotFound();
		}
		return path;
	}

	private TreeNodeList findPath(	final TreeNode p_treeNode,
									int p_currentBestPath,
									final NodeDefinition p_destinationNode)
	{
		Assert.preconditionNotNull(p_treeNode, "Tree Node");

		final NodeDefinition nodeDef = p_treeNode.getNodeDefinition();
		final AccessibleNode[] accessibleNodeArray = nodeDef.getAccessibleNodes();
		AccessibleNode currentAccessibleNode;
		int currentTotalDist = p_treeNode.getDistance();
		int currentDist;
		TreeNode childNode;
		TreeNodeList bestPath = null;
		TreeNodeList currentPath;
		for (int i = 0; i < accessibleNodeArray.length; i++)
		{
			currentAccessibleNode = accessibleNodeArray[i];
			if (!currentAccessibleNode.equals(nodeDef)
					&& !p_treeNode.isAParentNode(currentAccessibleNode._nodeDefinition))
			{
				currentDist = currentAccessibleNode._distance;
				if (currentDist + currentTotalDist < p_currentBestPath)
				{
					childNode = new TreeNode(	currentAccessibleNode._nodeDefinition,
												currentTotalDist + currentDist,
												p_treeNode);
					p_treeNode.addChildNode(childNode);
					if (p_destinationNode.equals(currentAccessibleNode._nodeDefinition))
					{
						return childNode.getPath();
					}

					currentPath = findPath(childNode, p_currentBestPath, p_destinationNode);
					if (currentPath != null)
					{
						bestPath = currentPath;
						p_currentBestPath = ((TreeNode) bestPath.get(bestPath.size() - 1)).getDistance();
					}
				}
			}
		}

		return bestPath;
	}

	public String toString()
	{
		final StringBuffer sb = new StringBuffer();

		sb.append("{TREE= ");
		sb.append("nodeDefList:").append(_nodeDefintionList);
		sb.append("}");

		return sb.toString();
	}
}