/*
 * Created on Jul 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

import java.util.Iterator;

import org.jocelyn_chaumel.tools.data_type.AbstractInstanceArrayList;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class AbstractNodeIdArrayList extends AbstractInstanceArrayList
{

	/**
	 * @param anInstanceKey
	 * @param aShortName
	 */
	public AbstractNodeIdArrayList(Class anInstanceKey, String aShortName)
	{
		super(anInstanceKey, aShortName);

		final Class[] classArray = anInstanceKey.getInterfaces();
		boolean isValid = false;
		for (int i = 0; i < classArray.length; i++)
		{
			if (classArray[i] == INodeId.class)
			{
				isValid = true;
				break;
			}
		}

		if (!isValid)
		{
			Assert.fail("Invalid Instance");
		}
	}

	public boolean isAValidNode(final NodeId aNodeId)
	{
		Assert.preconditionNotNull(aNodeId, "Node Id");

		final Iterator iter = iterator();
		boolean isValid = false;
		while (iter.hasNext())
		{
			if (((INodeId) iter.next()).getNodeId().equals(aNodeId))
			{
				isValid = true;
				break;
			}
		}

		return isValid;
	}

	public final INodeId getNodeById(final NodeId aNodeId)
	{
		Assert.preconditionNotNull(aNodeId, "Node Id");

		final Iterator iter = iterator();
		boolean isValid = false;
		INodeId currentNode = null;
		while (iter.hasNext())
		{
			currentNode = (INodeId) iter.next();
			if (currentNode.getNodeId().equals(aNodeId))
			{
				isValid = true;
				break;
			}
		}

		if (!isValid)
		{
			Assert.fail("Unknown Node Id");
		}

		return currentNode;
	}

}