/*
 * Created on Jul 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

/**
 * @author Jocelyn Chaumel
 */
public interface INodeId
{
	public NodeId getNodeId();

	public Object getObjectRef();
}
