/*
 * Created on Jul 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

import org.jocelyn_chaumel.tools.data_type.Id;

/**
 * @author Jocelyn Chaumel
 */
public final class NodeId extends Id
{
	public NodeId(final int anId)
	{
		super(anId);
	}

	public NodeId(final Id anId)
	{
		super(anId);
	}
}
