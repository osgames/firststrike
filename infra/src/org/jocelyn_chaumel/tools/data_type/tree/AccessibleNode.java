/*
 * Created on Aug 6, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

/**
 * @author Jocelyn Chaumel
 */
public class AccessibleNode
{
	final NodeDefinition _nodeDefinition;

	final int _distance;

	public AccessibleNode(final NodeDefinition aNodeDefinition, final int aDistance)
	{
		_nodeDefinition = aNodeDefinition;
		_distance = aDistance;
	}

	public String toString()
	{
		final StringBuffer sb = new StringBuffer();
		sb.append("[ACCESSIBLE_NODE= ");
		sb.append("dist.:").append(_distance);
		sb.append(", nodeDefId.:").append(_nodeDefinition.getNodeId());
		sb.append(", obj.:").append(_nodeDefinition.getObjectRef());
		sb.append("]");

		return sb.toString();
	}
}
