/*
 * Created on Jul 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type.tree;

import java.util.Iterator;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 * 
 *         Liste contenant uniquement des classes de type TreeNode.
 */
public class NodeDefinitionList extends AbstractNodeIdArrayList
{

	/**
	 * Constructeur par d�faut.
	 */
	public NodeDefinitionList()
	{
		super(NodeDefinition.class, "NodeDefinitionList");
	}

	public final NodeDefinition getNodeByRef(final Object p_objectRef)
	{
		Assert.preconditionNotNull(p_objectRef, "Object Ref");

		final Iterator iter = iterator();
		NodeDefinition currentNode;
		Object currentRef;
		while (iter.hasNext())
		{
			currentNode = (NodeDefinition) iter.next();
			currentRef = currentNode.getObjectRef();
			if (currentRef.equals(p_objectRef))
			{
				return currentNode;
			}
		}

		throw new IllegalArgumentException("Unable to found a node with ObjectRef :" + p_objectRef);
	}
}
