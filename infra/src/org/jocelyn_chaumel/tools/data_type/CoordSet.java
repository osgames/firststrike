/*
 * Created on Mar 26, 2005
 */
package org.jocelyn_chaumel.tools.data_type;

import java.util.HashSet;
import java.util.Iterator;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * HashSet ne pouvant contenir que des objets de type Coord.
 * 
 * @author Jocelyn Chaumel
 */
public class CoordSet extends HashSet<Coord>
{
	private final static String INTERNAL_NAME = "CoordSet";
	
	/**
	 * Constructeur par d�faut.
	 */
	public CoordSet()
	{
	}

	public CoordSet(final Coord p_coord)
	{
		this();
		Assert.preconditionNotNull(p_coord, "Coord");
		add(p_coord);
	}

	public Coord getDirectNearestCoord(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		Assert.precondition(!isEmpty(), "Coord Set is empty");

		final Iterator iter = iterator();
		Coord currentCoord;
		double bestDistance = Double.MAX_VALUE;
		double distance;
		Coord bestCoord = null;
		while (iter.hasNext())
		{
			currentCoord = (Coord) iter.next();
			distance = p_coord.calculateDistanceDouble(currentCoord);
			if (distance < bestDistance)
			{
				bestDistance = distance;
				bestCoord = currentCoord;
			}
		}
		return bestCoord;
	}

	public CoupleCoord getDirectNearestCoord(final CoordSet p_coordSet)
	{
		Assert.preconditionNotNull(p_coordSet, "CoordSet");
		Assert.precondition(!p_coordSet.isEmpty(), "Invalid Coord Set (empty)");

		Coord currentDestCoord = null;
		Coord currentStartCoord = null;
		double currentDistance;
		double shortestDistance = Integer.MAX_VALUE;
		CoupleCoord bestCoupleCoord = null;
		final Iterator destCoordIter = p_coordSet.iterator();
		while (destCoordIter.hasNext())
		{
			currentDestCoord = (Coord) destCoordIter.next();
			currentStartCoord = getDirectNearestCoord(currentDestCoord);
			currentDistance = currentStartCoord.calculateDistanceDouble(currentDestCoord);
			if (shortestDistance > currentDistance)
			{
				shortestDistance = currentDistance;
				bestCoupleCoord = new CoupleCoord();
				bestCoupleCoord.setFirstCoord(currentStartCoord);
				bestCoupleCoord.setSecondCoord(currentDestCoord);
			}
		}

		return bestCoupleCoord;
	}

	public final CoordSet getCoordInRange(final Coord p_startPoint, final int p_range)
	{

		final CoordSet coordSet = new CoordSet();

		final Iterator iter = iterator();
		Coord currentCoord;
		while (iter.hasNext())
		{
			currentCoord = (Coord) iter.next();
			if (currentCoord.calculateIntDistance(p_startPoint) <= p_range)
			{
				coordSet.add(currentCoord);
			}
		}

		return coordSet;
	}

	public Object clone()
	{
		final CoordSet clonedCoordSet = new CoordSet();
		clonedCoordSet.addAll(this);

		return clonedCoordSet;
	}

	/**
	 * Returns an extended CoordSet based on this CoordSet's content plus all Coords included in extended range param.
	 * 
	 * @param p_extendedRange
	 *            extended range to append to current coord set.
	 * @param p_maxX
	 *            Width limit.
	 * @param p_maxY
	 *            Height limit.
	 * 
	 * @return Extended coord set.
	 */
	public final CoordSet getExtendedCoordSet(final int p_extendedRange, final int p_maxX, final int p_maxY)
	{
		Assert.precondition(!isEmpty(), "Empty Coord Set");
		Assert.precondition(p_extendedRange >= 0, "Invalid Extended Range");
		Assert.precondition(p_maxX >= 0, "Invalid Max X");
		Assert.precondition(p_maxY >= 0, "Invalid Max Y");

		final CoordSet extendedCoordSet = new CoordSet();
		final RangeStrut range = new RangeStrut(this, p_extendedRange, p_maxX, p_maxY);

		extendedCoordSet.addAll(this);

		final Coord currentCoord = new Coord(0, 0);
		for (int y = range._minY; y <= range._maxY; y++)
		{
			currentCoord.setY(y);
			for (int x = range._minX; x <= range._maxX; x++)
			{
				currentCoord.setX(x);
				if (!extendedCoordSet.contains(currentCoord)
						&& getDirectNearestCoord(currentCoord).calculateIntDistance(currentCoord) <= p_extendedRange)
				{
					extendedCoordSet.add(new Coord(x, y));
				}
			}
		}

		return extendedCoordSet;
	}

	/**
	 * Create a new & initialized CoordSet. The new CoordSet is filled with a
	 * range of coord.
	 * 
	 * @param p_startCoord
	 *            Starting point of the range.
	 * @param p_range
	 *            Range radius.
	 * @param p_width
	 *            Map max width.
	 * @param p_height
	 *            Map max height
	 * 
	 * @return New initialized CoordSet.
	 */
	public static final CoordSet createInitiazedCoordSet(	final Coord p_startCoord,
															final int p_range,
															final int p_width,
															final int p_height)
	{
		Assert.preconditionNotNull(p_startCoord, "Start Coord");
		Assert.precondition(p_range >= 0, "Invalid must be greater than equal to 0");
		Assert.precondition(p_width >= 1, "Width must be greater than equal to 1");
		Assert.precondition(p_height >= 1, "Width must be greater than equal to 1");

		final CoordSet newCoordSet = new CoordSet();
		final RangeStrut range = new RangeStrut(p_startCoord, p_range, p_width, p_height);

		Coord currentCoord = null;
		for (int y = range._minY; y <= range._maxY; y++)
		{
			for (int x = range._minX; x <= range._maxX; x++)
			{
				currentCoord = new Coord(x, y);
				newCoordSet.add(currentCoord);
			}
		}

		return newCoordSet;
	}

	public static final CoordSet createInitiazedCoordSet(	final Coord p_startCoord,
															final int p_width,
															final int p_height,
															final int p_maxWidth,
															final int p_maxHeight)
	{
		Assert.preconditionNotNull(p_startCoord, "Start Coord");
		Assert.precondition(p_maxWidth >= 1, "Invalid must be greater than equal to 1");
		Assert.precondition(p_maxHeight >= 1, "Invalid must be greater than equal to 1");
		Assert.precondition(p_width >= 0, "Width must be greater than equal to 0");
		Assert.precondition(p_height >= 0, "Width must be greater than equal to 0");
		
		final CoordSet newCoordSet = new CoordSet();
		
		final RangeStrut range = new RangeStrut(p_startCoord, p_width, p_height, p_maxWidth, p_maxHeight);

		Coord currentCoord = null;
		for (int y = range._minY; y <= range._maxY; y++)
		{
			for (int x = range._minX; x <= range._maxX; x++)
			{
				currentCoord = new Coord(x, y);
				newCoordSet.add(currentCoord);
			}
		}

		return newCoordSet;
	}
	
	public final int getMinX()
	{
		int minX = Integer.MAX_VALUE;
		Coord currentCoord = null;
		final Iterator coordIter = iterator();
		while (coordIter.hasNext())
		{
			currentCoord = (Coord) coordIter.next();
			minX = Math.min(minX, currentCoord.getX());
		}

		return minX;
	}

	public final int getMaxX()
	{
		int maxX = Integer.MIN_VALUE;
		Coord currentCoord = null;
		final Iterator coordIter = iterator();
		while (coordIter.hasNext())
		{
			currentCoord = (Coord) coordIter.next();
			maxX = Math.max(maxX, currentCoord.getX());
		}

		return maxX;
	}

	public final int getMinY()
	{
		int minY = Integer.MAX_VALUE;
		Coord currentCoord = null;
		final Iterator coordIter = iterator();
		while (coordIter.hasNext())
		{
			currentCoord = (Coord) coordIter.next();
			minY = Math.min(minY, currentCoord.getY());
		}

		return minY;
	}

	public final int getMaxY()
	{
		int maxY = Integer.MIN_VALUE;
		Coord currentCoord = null;
		final Iterator coordIter = iterator();
		while (coordIter.hasNext())
		{
			currentCoord = (Coord) coordIter.next();
			maxY = Math.max(maxY, currentCoord.getY());
		}

		return maxY;
	}
	
	public final CoordSet getMinXCoords()
	{
		final CoordSet coordSet = new CoordSet();
		int currentX, minX = Integer.MAX_VALUE;
		
		for (Coord currentCoord : this)
		{
			currentX = currentCoord.getX();
			if (currentX == minX){
				coordSet.add(currentCoord);
			} else if (currentX < minX){
				coordSet.clear();
				coordSet.add(currentCoord);
				minX = currentX;
			}
		}
		
		return coordSet;
	}
	
	public final CoordSet getMaxXCoords()
	{
		final CoordSet coordSet = new CoordSet();
		int currentX, maxX = Integer.MIN_VALUE;
		
		for (Coord currentCoord : this)
		{
			currentX = currentCoord.getX();
			if (currentX == maxX){
				coordSet.add(currentCoord);
			} else if (currentX > maxX){
				coordSet.clear();
				coordSet.add(currentCoord);
				maxX = currentX;
			}
		}
		
		return coordSet;
	}
	
	
	public final CoordSet getMinYCoords()
	{
		final CoordSet coordSet = new CoordSet();
		int currentY, minY = Integer.MAX_VALUE;
		
		for (Coord currentCoord : this)
		{
			currentY = currentCoord.getY();
			if (currentY == minY){
				coordSet.add(currentCoord);
			} else if (currentY < minY){
				coordSet.clear();
				coordSet.add(currentCoord);
				minY = currentY;
			}
		}
		
		return coordSet;
	}
	
	public final CoordSet getMaxYCoords()
	{
		final CoordSet coordSet = new CoordSet();
		int currentY, maxY = Integer.MIN_VALUE;
		
		for (Coord currentCoord : this)
		{
			currentY = currentCoord.getY();
			if (currentY == maxY){
				coordSet.add(currentCoord);
			} else if (currentY > maxY){
				coordSet.clear();
				coordSet.add(currentCoord);
				maxY = currentY;
			}
		}
		
		return coordSet;
	}

	
	/**
	 * Return a new CoordSet extending the original by a specific range.
	 *  
	 * @param p_range
	 * @return
	 */
	public final CoordSet getExtendedCoordSet(final int p_range)
	{
		Assert.precondition(p_range >= 1, "Invalid Range");
		Assert.check(!isEmpty(), "Empty city coord set");
		
		final CoordSet coveredCoordSet = new CoordSet();
		final Iterator cityIter = this.iterator();
		for (Coord currentCoord : this)
		{
			currentCoord = (Coord) cityIter.next();
			coveredCoordSet.addAll(CoordSet.createInitiazedCoordSet(currentCoord,
																	p_range,
																	Integer.MAX_VALUE,
																	Integer.MAX_VALUE));
		}
		return coveredCoordSet;
	}
	
	/**
	 * Returns a sub set of coord included into a specific range.
	 * @param p_position
	 * @param p_range
	 * @return
	 */
	public final CoordSet getLimitedCoordSet(final Coord p_position, final int p_range){
		Assert.preconditionNotNull(p_position, "Position");
		Assert.precondition(p_range > 0, "Invalid position");
		
		final CoordSet finalCoordSet = new CoordSet();
		for (Coord currentCoord : this){
			if (currentCoord.calculateDelta(p_position) <= p_range){
				finalCoordSet.add(currentCoord);
			}
		}
		return finalCoordSet;
	}
	
	@Override
	public final String toString(){
		return FSUtil.toStringHashSet(INTERNAL_NAME, this);
	}
}
