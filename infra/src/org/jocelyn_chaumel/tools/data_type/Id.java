/*
 * Created on Dec 18, 2004
 */
package org.jocelyn_chaumel.tools.data_type;

import java.io.Serializable;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class Id implements Comparable, Serializable
{
	private final int _id;

	public Id(final int anId)
	{
		Assert.precondition(anId >= 1, "Invalid Id");
		_id = anId;
	}

	public Id(final Id anId)
	{
		Assert.preconditionNotNull(anId, "Id");

		_id = anId.getId();
	}

	public final int getId()
	{
		return _id;
	}

	public boolean equals(final Object p_object)
	{
		if (p_object == null)
		{
			return false;
		}

		if (!this.getClass().isInstance(p_object))
		{
			return false;
		}

		if (_id != ((Id) p_object)._id)
		{
			return false;
		}
		return true;
	}

	public int hashCode()
	{
		return _id;
	}

	public String toString()
	{
		return "" + _id;
	}

	public int compareTo(final Object anId)
	{
		if (anId == null)
		{
			return 1;
		}

		if (!(anId instanceof Id))
		{
			throw new IllegalArgumentException("Invalid Id instance class");
		}

		final int id = ((Id) anId)._id;
		if (_id > id)
		{
			return 1;
		} else if (_id == id)
		{
			return 0;
		} else
		{
			return -1;
		}
	}
}