/*
 * Created on Apr 23, 2005
 */
package org.jocelyn_chaumel.tools.data_type;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe permet de calculer les limites d'une plage par rapport � une
 * position et un range dans un tableau.
 * 
 * @author Jocelyn Chaumel
 */
public class RangeStrut
{

	public final int _minX;

	public final int _minY;

	public final int _maxX;

	public final int _maxY;

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param p_position
	 *            Centre de la plage.
	 * @param p_range
	 *            Rayon de la plage.
	 * @param p_width
	 *            Largeur du tableau.
	 * @param p_height
	 *            Hauteur du tableau.
	 */
	public RangeStrut(final Coord p_position, final int p_range, final int p_width, final int p_height)
	{
		this(p_position.getX(), p_position.getY(), p_range, p_width, p_height);
	}

	/**
	 * Constructs an instance of this class but based on set of coord.
	 * 
	 * @param p_positionSet a set of coord 
	 * @param p_range 
	 * @param p_width
	 * @param p_height
	 */
	public RangeStrut(final CoordSet p_positionSet, final int p_range, final int p_width, final int p_height)
	{
		Assert.preconditionNotNull(p_positionSet, "Coord Set");
		Assert.precondition(!p_positionSet.isEmpty(), "Empty Coord Set");
		Assert.precondition(p_width > 0, "Invalid Width");
		Assert.precondition(p_height > 0, "Invalid Height");
		Assert.precondition(p_range >= 0, "Invalid Range");
		
		_minX = Math.max(p_positionSet.getMinX() - p_range, 0);
		_minY = Math.max(p_positionSet.getMinY() - p_range, 0);
		_maxX = Math.min(p_positionSet.getMaxX() + p_range, p_width);
		_maxY = Math.min(p_positionSet.getMaxY() + p_range, p_height);
		Assert.check(_minX <= _maxX, "Unable to calculate minX and maxX");
		Assert.check(_minY <= _maxY, "Unable to calculate minY and maxY");
	}

	public RangeStrut(final int p_posX, final int p_posY, final int p_range, final int p_maxWidth, final int p_maxHeight)
	{
		Assert.precondition(p_maxWidth > 0, "Invalid Width");
		Assert.precondition(p_maxHeight > 0, "Invalid Height");
		Assert.precondition(p_range >= 0, "Invalid Range");
		Assert.precondition_between(p_posX, 0, p_maxWidth - 1, "Invalid Pos X");
		Assert.precondition_between(p_posY, 0, p_maxHeight - 1, "Invalid Pos Y");

		_minX = Math.max(0, p_posX - p_range);
		_maxX = Math.min(p_maxWidth - 1, p_posX + p_range);
		_minY = Math.max(0, p_posY - p_range);
		_maxY = Math.min(p_maxHeight - 1, p_posY + p_range);

		Assert.check(_minX <= _maxX, "Unable to calculate minX and maxX");
		Assert.check(_minY <= _maxY, "Unable to calculate minY and maxY");
	}


	public RangeStrut(final Coord p_position, final int p_width, final int p_height, final int p_maxWidth, final int p_maxHeight)
	{
		Assert.precondition(p_maxWidth > 0, "Invalid Max Width");
		Assert.precondition(p_maxHeight > 0, "Invalid Max Height");
		Assert.precondition(p_width >= 0, "Invalid Width");
		Assert.precondition(p_height >= 0, "Invalid Height");
		Assert.precondition_between(p_position.getX(), 0, p_maxWidth - 1, "Invalid Pos X");
		Assert.precondition_between(p_position.getY(), 0, p_maxHeight - 1, "Invalid Pos Y");

		_minX = p_position.getX();
		_maxX = Math.min(p_maxWidth - 1, _minX + p_width);
		_minY = p_position.getY();
		_maxY = Math.min(p_maxHeight - 1, _minY + p_height);
	}
}