/*
 * HashTableCoord.java
 *
 * Created on 15 juin 2003, 01:13
 */

package org.jocelyn_chaumel.tools.data_type;

import java.util.Iterator;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Joss
 */
public final class CoordHashtable extends AbstractInstanceHashtable
{

	/** Creates a new instance of HashTableCoord */
	public CoordHashtable()
	{
		super(Coord.class);
	}

	public final void removeAll(final CoordSet p_coordSet)
	{
		Assert.preconditionNotNull(p_coordSet, "Coord Set");
		Coord currentCoord;
		final Iterator iter = p_coordSet.iterator();
		while (iter.hasNext())
		{
			currentCoord = (Coord) iter.next();
			remove(currentCoord);
		}
	}
}
