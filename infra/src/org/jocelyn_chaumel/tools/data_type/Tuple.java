package org.jocelyn_chaumel.tools.data_type;

import java.io.Serializable;

public class Tuple<O1> implements Serializable
{
	private O1 _value1;
	private O1 _value2;
	
	public Tuple(final O1 p_value1, final O1 p_value2)
	{
		setValue1(p_value1);
		setValue2(p_value2);
	}
	
	public void setValue1(O1 p_value1)
	{
		_value1 = p_value1;
	}
	
	public void setValue2(O1 p_value2)
	{
		_value2 = p_value2;
	}
	
	public O1 getValue1()
	{
		return _value1;
	}
	
	public O1 getValue2()
	{
		return _value2;
	}
}
