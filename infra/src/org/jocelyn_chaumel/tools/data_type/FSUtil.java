package org.jocelyn_chaumel.tools.data_type;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class FSUtil
{
	public static String toStringHashSet(final String p_hashSetName, final HashSet p_hashSet){

			final StringBuffer sb = new StringBuffer();
			sb.append("{").append(p_hashSetName.toUpperCase());
			final int size = p_hashSet.size();
			sb.append("[").append(size).append("]= ");
			final Iterator iter = p_hashSet.iterator();
			boolean isFirst = true;
			while (iter.hasNext())
			{
				if (isFirst)
				{
					sb.append(", ");
				}
				sb.append("item:");
				sb.append(iter.next());
			}
			sb.append("}");

			return sb.toString();
	}
	public static String toStringList(final String p_hashSetName, final List p_hashSet){

		final StringBuffer sb = new StringBuffer();
		sb.append("{").append(p_hashSetName.toUpperCase());
		final int size = p_hashSet.size();
		sb.append("[").append(size).append("]= ");
		final Iterator iter = p_hashSet.iterator();
		boolean isFirst = true;
		while (iter.hasNext())
		{
			if (isFirst)
			{
				isFirst = false;
			} else {
				sb.append(", ");
			}
			sb.append("item:");
			sb.append(iter.next());
		}
		sb.append("}");

		return sb.toString();
	}
}
