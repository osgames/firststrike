/*
 * Created on Jan 6, 2005
 */
package org.jocelyn_chaumel.tools.data_type;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public abstract class AbstractInstanceHashtable extends Hashtable
{
	private Class _instanceKey;

	protected AbstractInstanceHashtable(final Class anInstanceKey)
	{
		Assert.preconditionNotNull(anInstanceKey);

		_instanceKey = anInstanceKey;
	}

	public final Object put(final Object aKey, final Object p_value)
	{
		Assert.preconditionNotNull(aKey, "Key");
		Assert.preconditionNotNull(p_value, "Value");
		//Assert.precondition(_instanceKey.isInstance(aKey), "Invalid Key Instance");

		super.put(aKey, p_value);

		return null;
	}

	public final boolean containsKey(final Object aKey)
	{
		Assert.preconditionNotNull(aKey);
		Assert.precondition(_instanceKey.isInstance(aKey), "Invalid Instance");

		return super.containsKey(aKey);
	}

	public final Object get(final Object aKey)
	{
		Assert.preconditionNotNull(aKey, "Key");
		Assert.precondition(_instanceKey.isInstance(aKey), "Invalid Instance");

		return super.get(aKey);
	}

	public final Object remove(final Object aKey)
	{
		Assert.preconditionNotNull(aKey);
		Assert
				.precondition(_instanceKey.isInstance(aKey), "Invalid Instance : " + _instanceKey.getName()
						+ " expected");

		return super.remove(aKey);
	}

	public final void putAll(Map p_map)
	{
		Assert.preconditionNotNull(p_map);
		throw new IllegalArgumentException("This method is not implemented.");
	}

	public final void clear()
	{
		Iterator iteration = this.values().iterator();
		Object currentValue;
		while (iteration.hasNext())
		{
			currentValue = iteration.next();
			if (currentValue instanceof List)
			{
				((List) currentValue).clear();
			} else
			{
				break;
			}
		}

		super.clear();
	}
}