/*
 * Created on Mar 26, 2005
 */
package org.jocelyn_chaumel.tools.data_type;

import java.util.ArrayList;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Liste contenant exclusivement des instances de classe Float.
 * 
 * @author Jocelyn Chaumel
 */
public class FloatList extends ArrayList<Float>
{

	public final static String SHORT_NAME = "FloatList";
	
	/**
	 * Contructeur par d�faut.
	 */
	public FloatList()
	{
	}

	public FloatList(final float[] p_floatArray)
	{
		Assert.preconditionNotNull(p_floatArray, "Float Array");
		for (int i = 0; i < p_floatArray.length; i++)
		{
			add(new Float(p_floatArray[i]));
		}
	}
	
	@Override
	public final String toString(){
		return FSUtil.toStringList(SHORT_NAME, this);
	}
}
