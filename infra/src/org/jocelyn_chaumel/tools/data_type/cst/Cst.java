/*
 * Created on Oct 24, 2004
 */
package org.jocelyn_chaumel.tools.data_type.cst;

import java.io.Serializable;

/**
 * Classe de base pour toutes les constantes.
 * 
 * @author Jocelyn Chaumel
 */
public abstract class Cst implements Serializable
{
	private final Object _value;
	private final String _label;

	protected Cst(final Object p_value, final String p_label)
	{
		_value = p_value;
		_label = p_label;
	}

	protected Object getProtectedValue()
	{
		return _value;
	}

	public final String getLabel()
	{
		return _label;
	}
	
	/**
	 * Retourne la constante associ� � la valeur.
	 * 
	 * @param p_value
	 *            Valeur de la constante recherch�e.
	 * @return L'instance associ�e � la constante.
	 * @throws InvalidConstantValueException
	 */
	protected static Cst getInstance(final String p_value, final Cst[] p_constantList, final Class p_class) throws InvalidConstantValueException
	{
		for (int i = 0; i < p_constantList.length; i++)
		{
			if (p_constantList[i].getLabel().equalsIgnoreCase(p_value))
			{
				return p_constantList[i];
			}
		}

		throw new InvalidConstantValueException(p_class.getName(), new Integer(p_value));
	}


	public String toString()
	{

		if (_label != null)
		{
			return _label + "(" + _value.toString() + ")";
		}
		return _value.toString();
	}
	
	public boolean equals(final Object p_cst){
		if (p_cst instanceof Cst) {
			return getProtectedValue().equals(((Cst)p_cst).getProtectedValue());
		}
		
		return false;
	}
	
}
