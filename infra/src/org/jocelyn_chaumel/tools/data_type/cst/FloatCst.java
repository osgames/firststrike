/*
 * Created on May 8, 2005
 */
package org.jocelyn_chaumel.tools.data_type.cst;

/**
 * @author Jocelyn Chaumel
 */
public abstract class FloatCst extends Cst
{

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param p_value
	 *            Valeur de la constante.
	 */
	protected FloatCst(final float p_value)
	{
		this(p_value, null);
	}

	protected FloatCst(final float p_value, final String p_label)
	{
		super(new Float(p_value), p_label);
	}

	/**
	 * Retourne la valeur de la constante.
	 * 
	 * @return Valeur enti�re de la constante.
	 */
	public float getValue()
	{
		return ((Float) getProtectedValue()).floatValue();
	}

	/**
	 * Retourne la constante associ� � la valeur.
	 * 
	 * @param p_value
	 *            Valeur de la constante recherch�e.
	 * @return L'instance associ�e � la constante.
	 * @throws InvalidConstantValueException
	 */
	protected static FloatCst getInstance(final float p_value, final FloatCst[] aConstantList, final Class aClass) throws InvalidConstantValueException
	{
		for (int i = 0; i < aConstantList.length; i++)
		{
			if (aConstantList[i].getValue() == p_value)
			{
				return aConstantList[i];
			}
		}

		throw new InvalidConstantValueException(aClass.getName(), new Float(p_value));
	}
	
    
    public boolean equals(final Object p_object){
        if (p_object == null){
            return false;
        }
        
        if (!(p_object instanceof FloatCst)){
            return false;
        }
        
        final float id = ((FloatCst) p_object).getValue();
        final Float idObject = new Float(id);
        
        return idObject.equals((Float) getProtectedValue());
    }
	
}
