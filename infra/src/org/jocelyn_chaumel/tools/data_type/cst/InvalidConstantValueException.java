/*
 * Created on Oct 24, 2004
 */
package org.jocelyn_chaumel.tools.data_type.cst;

/**
 * @author Jocelyn Chaumel
 */
public class InvalidConstantValueException extends Exception
{
	public InvalidConstantValueException(final String aConstantClassName, final Object aConstantValue)
	{
		super("Invalue value (" + aConstantValue + ") for constant (" + aConstantClassName + ").");
	}
}