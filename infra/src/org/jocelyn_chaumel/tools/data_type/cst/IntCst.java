/*
 * Created on Oct 24, 2004
 */
package org.jocelyn_chaumel.tools.data_type.cst;

/**
 * Classe abstraite contenant des constantes de type enti�re. Elle permet de
 * trouver les constantes correspondant � une valeur.
 * 
 * @author Jocelyn Chaumel
 */
public abstract class IntCst extends Cst
{
	/**
	 * Constructeur param�tris�.
	 * 
	 * @param p_value
	 *            Valeur de la constante.
	 */
	protected IntCst(final int p_value)
	{
		this(p_value, null);
	}

	protected IntCst(final int p_value, final String p_label)
	{
		super(new Integer(p_value), p_label);
	}

	/**
	 * Retourne la valeur de la constante.
	 * 
	 * @return Valeur enti�re de la constante.
	 */
	public int getValue()
	{
		return ((Integer) getProtectedValue()).intValue();
	}

	/**
	 * Retourne la constante associ� � la valeur.
	 * 
	 * @param p_value
	 *            Valeur de la constante recherch�e.
	 * @return L'instance associ�e � la constante.
	 * @throws InvalidConstantValueException
	 */
	protected static IntCst getInstance(final int p_value, final IntCst[] p_constantList, final Class p_class) throws InvalidConstantValueException
	{
		for (int i = 0; i < p_constantList.length; i++)
		{
			if (p_constantList[i].getValue() == p_value)
			{
				return p_constantList[i];
			}
		}

		throw new InvalidConstantValueException(p_class.getName(), new Integer(p_value));
	}

	protected static IntCst getInstance(final String p_value, final IntCst[] p_constantList, final Class p_class) throws InvalidConstantValueException
	{
		for (int i = 0; i < p_constantList.length; i++)
		{
			if (p_constantList[i].getLabel().equals(p_value))
			{
				return p_constantList[i];
			}
		}

		throw new InvalidConstantValueException(p_class.getName(), new Integer(p_value));
	}
	
	public boolean equals(final Object p_object){
	    if (p_object == null){
	        return false;
	    }
	    
	    if (!(p_object instanceof IntCst)){
	        return false;
	    }
	    
	    int id = ((IntCst) p_object).getValue();
	    return id == this.getValue();
	}
}