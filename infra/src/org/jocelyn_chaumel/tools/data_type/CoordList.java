/*
 * Created on Mar 26, 2005
 */
package org.jocelyn_chaumel.tools.data_type;

import java.util.ArrayList;

/**
 * ArrayList pouvant contenir exclusivement des objets de type Coord.
 * 
 * @author Jocelyn Chaumel
 */
public class CoordList extends ArrayList<Coord>
{
	public final static String SHORT_NAME = "CoordList";

	/**
	 * Constructeur par d�faut.
	 */
	public CoordList()
	{
	}

	public CoordList(final Coord[] p_objectArray)
	{
		super(p_objectArray.length);
		
		for (int i = 0; i < p_objectArray.length; i++){
			add(p_objectArray[i]);
		}
	}

	public CoordList(final CoordSet p_coordSet)
	{
		this();
		this.addAll(p_coordSet);
	}
	
	public CoordList(final Coord p_coord){
		this();
		this.add(p_coord);
	}

	@Override
	public final String toString()
	{
		return FSUtil.toStringList(SHORT_NAME, this);
	}
}
