/*
 * Created on 18 nov. 2006
 */
package org.jocelyn_chaumel.tools.data_type;

/**
 * @author Jocelyn Chaumel
 */
public class PropertyArrayList extends AbstractInstanceArrayList
{
	private static final String PROPERTY_LIST_NAME = "PropertyList";
	private static final Class INSTANCE_CLASS = Property.class;

	/**
	 * @param anInstanceKey
	 * @param aShortName
	 */
	public PropertyArrayList()
	{
		super(INSTANCE_CLASS, PROPERTY_LIST_NAME);
	}

}
