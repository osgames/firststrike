package org.jocelyn_chaumel.tools.data_type;

public class CoupleCoord
{
	private Coord _firstCoord;
	private Coord _secondCoord;

	public Coord getFirstCoord()
	{
		return _firstCoord;
	}

	public void setFirstCoord(Coord p_firstCoord)
	{
		_firstCoord = p_firstCoord;
	}

	public Coord getSecondCoord()
	{
		return _secondCoord;
	}

	public void setSecondCoord(Coord p_secondCoord)
	{
		_secondCoord = p_secondCoord;
	}

	public double calculateDistance()
	{
		return _firstCoord.calculateDistanceDouble(_secondCoord);
	}
}
