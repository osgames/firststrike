package org.jocelyn_chaumel.tools;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;

import javax.swing.ImageIcon;

public class ImageTool
{
	private final static RescaleOp _darkerOp = new RescaleOp(0.8f, 0f, null);

	public static ImageIcon darkerImage(final ImageIcon p_imageIcon)
	{
		BufferedImage bi = new BufferedImage(	p_imageIcon.getIconWidth(),
		                                     	p_imageIcon.getIconHeight(),
												BufferedImage.TYPE_INT_RGB);
		final Graphics graphics = bi.getGraphics();
		graphics.drawImage(p_imageIcon.getImage(), 0, 0, null);
		graphics.dispose();
		BufferedImage biOut = _darkerOp.filter(bi, null);
		return new ImageIcon(biOut);
	}
}
