/*
 * Created on Jan 3, 2006
 */
package org.jocelyn_chaumel.tools.params;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class ParameterAnalyser
{
	private final ArrayList _parameterNameArray = new ArrayList();

	/**
	 * Constructeur param�tris�.
	 */
	public ParameterAnalyser(final String[] aParameterNameArray)
	{
		Assert.preconditionNotNull(aParameterNameArray, "Parameter Name Array");
		Assert.precondition(aParameterNameArray.length > 0, "Invalid Parameter Name Array Size");

		String currentParam;
		for (int i = 0; i < aParameterNameArray.length; i++)
		{
			currentParam = aParameterNameArray[i];
			Assert.preconditionNotNull(currentParam, "Parameter Name [" + i + "]");
			currentParam = currentParam.trim();

			Assert.precondition(currentParam.length() > 0, "Empty Param " + i);
			Assert.precondition(!_parameterNameArray.contains(currentParam), "Duplicate Parameter Name :"
					+ currentParam);
			_parameterNameArray.add(currentParam);
		}
	}

	/**
	 * Analyse et retourne les param�tres.
	 * 
	 * @param aParamsArray
	 *            Tableau de param�tre � analyser.
	 * @return Map de param�tre (cl�, valeur)
	 * @throws IllegalParameterException
	 */
	public final HashMap parseParams(final String[] aParamsArray) throws IllegalParameterException
	{
		Assert.preconditionNotNull(aParamsArray, "Params Array");

		final HashMap map = new HashMap();
		String currentParam;
		Iterator iter;
		String currentParamName;
		String currentParamValue;
		for (int i = 0; i < aParamsArray.length; i++)
		{
			currentParam = aParamsArray[i];
			Assert.preconditionNotNull(currentParam, "Param [" + i + "]");
			currentParam = currentParam.trim();
			if (currentParam.length() == 0)
			{
				throw new IllegalParameterException("Empty param " + i);
			}

			iter = _parameterNameArray.iterator();
			currentParamValue = null;
			while (iter.hasNext())
			{
				currentParamName = (String) iter.next();
				if (currentParam.startsWith(currentParamName))
				{
					currentParamValue = currentParam.substring(currentParamName.length());
					map.put(currentParamName, currentParamValue);
					break;
				}
			}

			if (currentParamValue == null)
			{
				throw new IllegalParameterException(currentParam);
			}
		}

		return map;
	}
}