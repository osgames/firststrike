/*
 * Created on Jan 3, 2006
 */
package org.jocelyn_chaumel.tools.params;

/**
 * @author Jocelyn Chaumel
 */
public class IllegalParameterException extends Exception
{
	/**
	 * @param anInvalidParam
	 */
	public IllegalParameterException(final String anInvalidParam)
	{
		super("Unsupported parameter :" + anInvalidParam);
	}

}
