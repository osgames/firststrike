/*
 * Created on Feb 11, 2006
 */
package org.jocelyn_chaumel.tools.gui;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * @author Jocelyn Chaumel
 */
public class JCTextField extends JPanel
{
	private final JLabel _label = new JLabel();
	private final JTextField _field = new JTextField();

	public JCTextField(final String p_label, final int p_labelWidth, final int aFieldWidth)
	{
		super(new FlowLayout());

		final Dimension labelDimension = new Dimension(p_labelWidth, 20);
		_label.setText(p_label + " : ");
		_label.setHorizontalAlignment(SwingConstants.RIGHT);
		_label.setPreferredSize(labelDimension);
		_label.setLabelFor(_field);
		this.add(_label);

		final Dimension fieldDimension = new Dimension(aFieldWidth, 20);
		_field.setPreferredSize(fieldDimension);
		this.add(_field);

	}

	public JCTextField(final String p_label, final int p_labelWidth, final int aFieldWidth, final String aDefaultValue)
	{
		this(p_label, p_labelWidth, aFieldWidth);
		_field.setText(aDefaultValue);
	}

	public final String getText()
	{
		return _field.getText();
	}
}
