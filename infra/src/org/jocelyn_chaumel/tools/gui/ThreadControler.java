/*
 * ThreadControler.java
 *
 * Created on 1 avril 2004, 12:19
 */

package org.jocelyn_chaumel.tools.gui;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe permet � un processus ainsi qu'une bo�te de dialogue de se
 * synchroniser pour permettre l'affichage de la progression d'une tache
 * quelconque.
 */
public class ThreadControler
{

	private static final byte THREAD_NOT_FINISH = 0;
	private static final byte THREAD_FINISH_NORMALLY = 1;

	private byte mThreadFinishState = THREAD_NOT_FINISH;
	private boolean mDlgCancel = false;
	private final ProgressDlg mDlg;

	/**
	 * Constructeur param�tris�.
	 */
	public ThreadControler(final ProgressDlg aDlg)
	{
		Assert.precondition(aDlg != null, "Null Ref");

		mDlg = aDlg;
	}

	protected final ProgressDlg getDlg()
	{
		return mDlg;
	}

	/**
	 * Est-ce que la bo�te de dialogue a �t� ferm�e?
	 */
	public final boolean isDlgCancel()
	{
		return mDlgCancel;
	}

	/**
	 * Indique que la bo�te de dialogue a �t� ferm�e.
	 */
	public final void setDlgCancel()
	{
		mDlgCancel = true;
	}

	public final boolean isThreadFinishNormally()
	{
		return mThreadFinishState == THREAD_FINISH_NORMALLY;
	}

	public final boolean isThreadFinish()
	{
		return mThreadFinishState != THREAD_NOT_FINISH;
	}

	/**
	 * Indique que le processus a termin�.
	 */
	public final void setThreadFinishNormally()
	{
		mThreadFinishState = THREAD_FINISH_NORMALLY;
		if (mDlgCancel == false)
		{
			mDlg.setVisible(false);
		}
	}

}
