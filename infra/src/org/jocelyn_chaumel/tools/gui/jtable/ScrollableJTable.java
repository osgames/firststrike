/*
 * Created on 17 nov. 2006
 */
package org.jocelyn_chaumel.tools.gui.jtable;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.table.JTableHeader;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 * 
 *         Cette classe permet d'encapsuler un table de type JTable dans un
 *         panneau fixe avec un ascenseur verticale et horizontale.
 */
public class ScrollableJTable extends JScrollPane
{

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param aTable
	 *            JTable
	 */
	public ScrollableJTable(final JTable p_table)
	{
		this(p_table, new DefaultRowHeaderRenderer(p_table));

	}

	public ScrollableJTable(final JTable p_table, final ListCellRenderer aRowHeaderRenderer)
	{
		super(p_table);

		p_table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		final int rowCount = p_table.getModel().getRowCount();
		final ScrollableListModel rowHeaderModel = new ScrollableListModel(rowCount);
		final JList rowHeaderList = new JList(rowHeaderModel);

		rowHeaderList.setFixedCellWidth(50);

		rowHeaderList.setFixedCellHeight(p_table.getRowHeight());
		rowHeaderList.setCellRenderer(aRowHeaderRenderer);

		setRowHeaderView(rowHeaderList);
	}
}

/**
 * Cette classe impl�mente le controle (Etiquette) par d�faut pour les ent�tes
 * de ligne dans la table (JTable).
 */

class DefaultRowHeaderRenderer extends JLabel implements ListCellRenderer
{

	DefaultRowHeaderRenderer(final JTable p_table)
	{
		JTableHeader header = p_table.getTableHeader();
		setOpaque(true);
		setBorder(UIManager.getBorder("TableHeader.cellBorder"));
		setHorizontalAlignment(CENTER);
		setForeground(header.getForeground());
		setBackground(header.getBackground());
		setFont(header.getFont());
	}

	public final Component getListCellRendererComponent(final JList aList,
														final Object p_value,
														final int p_index,
														final boolean anIsSelectedFlag,
														final boolean p_cellHasFocusFlag)
	{
		Assert.preconditionNotNull(aList, "List");
		Assert.preconditionNotNull(p_value, "Value");
		Assert.preconditionIsPositive(p_index, "Invalid index value");
		Assert.precondition(anIsSelectedFlag == p_cellHasFocusFlag || anIsSelectedFlag != p_cellHasFocusFlag, "");

		setText((p_value == null) ? "" : p_value.toString());
		return this;
	}
}
