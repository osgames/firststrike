/*
 * Created on 18 nov. 2006
 */
package org.jocelyn_chaumel.tools.gui.jtable;

import javax.swing.table.AbstractTableModel;

import org.jocelyn_chaumel.tools.data_type.Property;
import org.jocelyn_chaumel.tools.data_type.PropertyArrayList;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe impl�mente le mod�le de donn�es pour une classe JTable contenant
 * une liste de propri�t�s.
 * 
 * @author Jocelyn Chaumel
 */
public class PropertiesTableModel extends AbstractTableModel
{
	private final PropertyArrayList _data;

	public final static int PROPERTY = -1;

	public final static int PROPERTY_NAME = 0;

	public final static int PROPERTY_VALUE = 1;

	public final static int[] COL_INDEX_VALUE_LIST = { PROPERTY, PROPERTY_NAME, PROPERTY_VALUE };

	public PropertiesTableModel(final PropertyArrayList aPropertyArrayList)
	{
		Assert.preconditionNotNull(aPropertyArrayList, "Property Array List");

		_data = aPropertyArrayList;
	}

	/**
	 * Retourne le nombre de colonne.
	 * 
	 * @return nombre de colonne.
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount()
	{
		return 2;
	}

	/**
	 * Retourne le nombre de ligne
	 * 
	 * @return nombre de ligne.
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount()
	{
		return _data.size();
	}

	/**
	 * Retourne la valeur de l'objet � la position souhait�e.
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int aRowIndex, int aColIndex)
	{
		Assert.preconditionIsPositive(aRowIndex, "Invalid Row Index.  Must be a positive value");
		Assert.precondition(aRowIndex < _data.size(), "Row Index Greather than the Array.");
		Assert.preconditionIsIn(aColIndex, COL_INDEX_VALUE_LIST, "Invalid Column Index Value. Expected values : "
				+ COL_INDEX_VALUE_LIST.toString());

		final Property currentProperty = (Property) _data.get(aRowIndex);
		if (aColIndex == PROPERTY)
		{
			return currentProperty;
		} else if (aColIndex == PROPERTY_NAME)
		{
			return currentProperty.getName();
		} else
		{
			return currentProperty.getValue();
		}
	}

	public final void setValueAt(final Object aV, final int y, final int x)
	{
		if (x == PROPERTY_VALUE)
		{
			final Property property = (Property) _data.get(y);
			property.setValue((String) aV);
			this.fireTableCellUpdated(y, x);
		}
	}
}