/*
 * Created on 17 nov. 2006
 */
package org.jocelyn_chaumel.tools.gui.jtable;

import javax.swing.AbstractListModel;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe permet de d�finir la liste des valeurs affich�es comme ent�te de
 * ligne dans une table de type ScrollableJTable.
 * 
 * @author Jocelyn Chaumel
 */
public class ScrollableListModel extends AbstractListModel
{
	final int _rowCount;

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param aRowCount
	 *            Nombre de ligne.
	 */
	public ScrollableListModel(final int aRowCount)
	{
		Assert.preconditionIsPositive(aRowCount, "Invalid RowCount Parameter value.");
		_rowCount = aRowCount;
	}

	/**
	 * Retourne la taille de la liste.
	 * 
	 * @return Taille d'une liste.
	 * 
	 * @see javax.swing.ListModel#getSize()
	 */
	public int getSize()
	{
		return _rowCount;
	}

	/**
	 * Retourne l'�l�ment � la position d�sir�e.
	 * 
	 * @param p_position
	 *            Position de l'�l�ment d�sir�.
	 * 
	 * @see javax.swing.ListModel#getElementAt(int)
	 */
	public Object getElementAt(int p_position)
	{
		return "" + (1 + p_position);
	}

}