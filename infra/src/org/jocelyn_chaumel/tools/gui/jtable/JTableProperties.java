/*
 * Created on 18 nov. 2006
 */
package org.jocelyn_chaumel.tools.gui.jtable;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe d�finit une table affichant une liste de propri�t�s.
 * 
 * @author Jocelyn Chaumel
 */
public class JTableProperties extends JTable
{
	public JTableProperties(final PropertiesTableModel aTableModel)
	{

		super(aTableModel);

		final TableColumn column = columnModel.getColumn(0);
		column.setResizable(false);
		columnModel.setColumnSelectionAllowed(false);
		column.setCellRenderer(new DefaultPropertyNameRenderer(this));
		setCellSelectionEnabled(false);
		addMouseListener(new MouseListener()
		{

			@Override
			public void mouseReleased(MouseEvent p_e)
			{
				System.out.println("released");

			}

			@Override
			public void mousePressed(MouseEvent p_e)
			{
				System.out.println("pressed");
			}

			@Override
			public void mouseExited(MouseEvent p_e)
			{
				System.out.println("exit");

			}

			@Override
			public void mouseEntered(MouseEvent p_e)
			{
				System.out.println("entered");

			}

			@Override
			public void mouseClicked(MouseEvent p_e)
			{
				System.out.println("clicked");

			}
		});
		
		super.addMouseMotionListener(new MouseMotionListener() 
		{
			
			@Override
			public void mouseMoved(MouseEvent p_e)
			{
				System.out.println("move");
			}
			
			@Override
			public void mouseDragged(MouseEvent p_e)
			{
				System.out.println("drag");
				
			}
		});
	}
}

/**
 * Cette classe impl�mente le controle (Etiquette) par d�faut pour les ent�tes
 * de ligne dans la table affichant une liste de propri�t�.
 */

class DefaultPropertyNameRenderer extends JLabel implements TableCellRenderer
{

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param table
	 */
	DefaultPropertyNameRenderer(final JTable table)
	{
		JTableHeader header = table.getTableHeader();
		setOpaque(true);
		setBorder(UIManager.getBorder("TableHeader.cellBorder"));
		setHorizontalAlignment(CENTER);
		setForeground(header.getForeground());
		setBackground(header.getBackground());
		setFont(header.getFont());
	}

	/**
	 * Retourne le composant �tiquette.
	 * 
	 * @see javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable,
	 *      java.lang.Object, boolean, boolean, int, int)
	 */
	public Component getTableCellRendererComponent(	final JTable aTable,
													final Object p_value,
													final boolean anIsSelectedFlag,
													final boolean p_cellHasFocusFlag,
													final int aRowIndex,
													final int aColIndex)
	{
		Assert.preconditionNotNull(aTable, "Table");
		Assert.preconditionNotNull(p_value, "Value");
		Assert.precondition(anIsSelectedFlag == p_cellHasFocusFlag || anIsSelectedFlag != p_cellHasFocusFlag, "");
		Assert.preconditionIsPositive(aRowIndex, "Invalid Row Index Value");
		Assert.preconditionIsPositive(aColIndex, "Invalid Col Index Value");

		setText((String) p_value);

		return this;
	}
}
