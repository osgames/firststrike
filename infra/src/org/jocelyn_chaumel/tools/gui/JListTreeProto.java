package org.jocelyn_chaumel.tools.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;

public class JListTreeProto extends JFrame
{
	private JList _jList = new JList();
	private final DefaultListModel _model = new DefaultListModel();
	public JListTreeProto(){
		super("JList Tree Prototype");
		
		_model.addElement(new Group("A"));
		_model.addElement(new Group("B"));
		_model.addElement(new Group("C"));
		
		_jList.setModel(_model);
		_jList.setCellRenderer(new MyListCellRenderer());
		
		_jList.addMouseListener(new MyMouseListener());
 
//		JScrollPane jScrollPane1 = new JScrollPane();
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(100, 100));
		panel.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
		panel.setLayout(new FlowLayout());
		_jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//		jScrollPane1.setViewportView(_jList);
		panel.add(_jList);
		getContentPane().add(panel, BorderLayout.CENTER);
	}
	
	private class MyListCellRenderer implements ListCellRenderer {

		private final JLabel _label = new JLabel();
		
		@Override
		public Component getListCellRendererComponent(	JList p_list,
														Object p_value,
														int p_index,
														boolean p_isSelected,
														boolean p_cellHasFocus)
		{
			if (p_value instanceof Group) {
				final Group group = (Group) p_value;
				_label.setText(group.getGroupName());
				_label.setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
				_label.setBackground(Color.LIGHT_GRAY);
				
			} else {
				final CityItem cityItem = (CityItem) p_value;
				_label.setText(" " + cityItem.getCityName());
				_label.setBorder(javax.swing.BorderFactory.createEmptyBorder());
				_label.setBackground(Color.WHITE);
			}
			return _label;
		}
		
	}
	
	private class MyMouseListener implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent p_e)
		{
			JList myList = (JList) p_e.getSource();
			Object value = myList.getSelectedValue();
			int index = myList.getSelectedIndex();
			final DefaultListModel model = (DefaultListModel) myList.getModel();
			if (value instanceof Group) {
				final Group group = (Group) value;
				if (group.isDeployed()) {
					int size = group.size();
					while (size > 0) {
						model.remove(index + size);
						size--;
					}
					group.setDeployed(false);
				} else {
					int size = group.size();
					while (size > 0) {
						size--;
						model.add(index + 1, group.get(size));
					}
					group.setDeployed(true);
					
				}
			} else {
			}
		}

		@Override
		public void mouseEntered(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mouseExited(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mousePressed(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mouseReleased(MouseEvent p_e)
		{
			// NOP
		}
		
	}
	
	private class Group extends ArrayList<CityItem>{
		private final String _groupName;
		private boolean _isDeployed = false;
		
		public Group(final String p_groupName){
		    _groupName = p_groupName;	
		    this.add(new CityItem(_groupName + ".1"));
		    this.add(new CityItem(_groupName + ".2"));
		    this.add(new CityItem(_groupName + ".3"));
		}
		
		public boolean isDeployed() {
			return _isDeployed;
		}
		
		public final void setDeployed(final boolean p_isDeployed){
			_isDeployed = p_isDeployed;
		}
		
		public final String getGroupName() {
			return _groupName;
		}
		
	}
	
	private class CityItem {
		private final String _cityName;
		
		public CityItem(final String p_cityName){
			_cityName = p_cityName;
		}
		
		public final String getCityName() {
			return _cityName;
		}
	}
	
	public static void main(final String[] p_paramArray){
		final JListTreeProto frame = new JListTreeProto();
		frame.pack();
		frame.setVisible(true);
	}
}
