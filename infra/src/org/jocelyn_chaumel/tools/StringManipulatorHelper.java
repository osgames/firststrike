/*
 * Created on Feb 12, 2005
 */
package org.jocelyn_chaumel.tools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class StringManipulatorHelper
{
	public final static String FIRST_PARAM = "{0}";
	public final static String SECOND_PARAM = "{1}";

	protected final static String padString(final String aStringSrc,
											final int aFinalStringLength,
											final char aPaddingChar,
											final boolean aLeftPaddingFlag)
	{
		Assert.preconditionNotNull(aStringSrc, "aStringSrc");
		Assert.precondition(aFinalStringLength >= 0, "Invalid Final String Length Param");

		int length = aStringSrc.length();
		if (length > aFinalStringLength)
		{
			if (aFinalStringLength == 0)
			{
				return "";
			} else if (aLeftPaddingFlag)
			{
				return aStringSrc.substring(length - aFinalStringLength, length);
			} else
			{
				return aStringSrc.substring(0, aFinalStringLength);
			}
		} else if (length == aFinalStringLength)
		{
			return aStringSrc;
		} else if (aLeftPaddingFlag)
		{
			return buildString(aPaddingChar, aFinalStringLength - length) + aStringSrc;
		} else
		{
			return aStringSrc + buildString(aPaddingChar, aFinalStringLength - length);
		}
	}

	/**
	 * Appends with space character the string buffer. The final String buffer's
	 * length is specified by the p_maxLenght parameter.
	 * 
	 * @param p_sb String Buffer to manipulate.
	 * @param p_finalLength final String buffer's length expected.
	 */
	public final static void leftPaddind(final StringBuffer p_sb, final int p_finalLength)
	{
		Assert.preconditionNotNull(p_sb, "String Buffer");
		Assert.precondition(p_finalLength >= 0, "Invalid max length");
		if (p_finalLength == 0)
		{
			return;
		}
		int size = p_sb.length();
		if (size > p_finalLength && p_finalLength < 3)
		{
			p_sb.delete(p_finalLength, size);
		} else if (size > p_finalLength)
		{
			p_sb.delete(p_finalLength - 3, size);
			p_sb.append("...");
		} else
		{
			while (size < p_finalLength)
			{
				p_sb.append(" ");
				size++;
			}
		}
	}

	public final static void rightPadding(final StringBuffer p_sb, final String p_str, final int p_finalLength)
	{
		Assert.preconditionNotNull(p_sb, "String Buffer");
		Assert.preconditionNotNull(p_str, "String");
		Assert.precondition(p_finalLength > 0, "Invalid final length");
		Assert.precondition(p_sb.length() < p_finalLength, "The String Buffer's length is invalid");

		final int sbSize = p_sb.length();
		final int strSize = p_str.length();

		if (sbSize + strSize <= p_finalLength)
		{
			leftPaddind(p_sb, p_finalLength - strSize);
			p_sb.append(p_str);
		} else
		{
			p_sb.append(p_str);
			leftPaddind(p_sb, p_finalLength);
		}

	}

	public final static String leftPadding(final String aStringSrc, final int aFinalStringLength)
	{
		return padString(aStringSrc, aFinalStringLength, ' ', true);
	}

	public final static String leftPadding(	final String aStringSrc,
											final int aFinalStringLength,
											final char aPaddingChar)
	{
		return padString(aStringSrc, aFinalStringLength, aPaddingChar, true);
	}

	public final static String rightPadding(final String aStringSrc, final int aFinalStringLength)
	{
		return padString(aStringSrc, aFinalStringLength, ' ', false);
	}

	public final static String rightPadding(final String aStringSrc,
											final int aFinalStringLength,
											final char aPaddingChar)
	{
		return padString(aStringSrc, aFinalStringLength, aPaddingChar, false);
	}

	public final static String parenthesisString(final String p_str)
	{
		Assert.preconditionNotNull(p_str, "String");

		return "(" + p_str + ")";
	}

	public final static void parenthesisString(final StringBuffer p_sb, final String p_str)
	{
		Assert.preconditionNotNull(p_sb, "String Buffer");
		Assert.preconditionNotNull(p_str, "String");
		p_sb.append("(");
		p_sb.append(p_str);
		p_sb.append(")");
	}

	public final static String buildString(final char aFillCaract, final int aLength)
	{
		Assert.precondition(aLength >= 1, "Invalid Length Param");
		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < aLength; i++)
		{
			sb.append(aFillCaract);
		}

		return sb.toString();
	}

	public final static String replace(String aSource, final String aStrToReplace, final String aNewStr)
	{
		Assert.preconditionNotNull(aSource, "String Source");
		Assert.preconditionNotNull(aStrToReplace, "String to replace");
		Assert.preconditionNotNull(aNewStr, "New String");

		final StringBuffer sb = new StringBuffer();
		final int size = aStrToReplace.length();
		int index = aSource.indexOf(aStrToReplace);
		while (index != -1)
		{
			sb.append(aSource.substring(0, index));
			sb.append(aNewStr);

			aSource = aSource.substring(index + size, aSource.length());
			index = aSource.indexOf(aStrToReplace);
		}
		sb.append(aSource);

		return sb.toString();
	}

	public static String buildMessage(final String p_message, final Object p_obj)
	{
		return buildMessage(p_message, p_obj, 0);
	}

	public static String buildMessage(final String p_message, final Object p_obj, final int p_index)
	{
		if (p_message.indexOf("{" + p_index + "}") != -1)
		{
			return p_message.replaceAll("\\{" + p_index + "\\}", toStringObject(p_obj));
		}

		return p_message + toStringObject(p_obj);
	}

	public static String buildMessage(final String p_message, final Object p_obj1, final Object p_obj2)
	{
		String message;
		boolean isAppend = false;
		if (p_message.indexOf(FIRST_PARAM) != -1)
		{
			message = p_message.replaceAll("\\{0\\}", toStringObject(p_obj1));
		} else
		{
			message = p_message + toStringObject(p_obj1);
			isAppend = true;
		}

		if (p_message.indexOf(SECOND_PARAM) != -1)
		{
			message = p_message.replaceAll("\\{1\\}", toStringObject(p_obj2));
		} else
		{
			if (isAppend)
			{
				message = message + ", " + toStringObject(p_obj2);
			} else
			{
				message = message + toStringObject(p_obj2);
			}
		}

		return message;
	}

	private static String toStringObject(final Object p_obj)
	{
		if (p_obj == null)
		{
			return "null";
		}

		return p_obj.toString();
	}

	public static String formatProperty(final int p_current, final int p_max)
	{
		final StringBuffer sb = new StringBuffer();
		sb.append(p_current);
		sb.append("/");
		sb.append(p_max);

		return sb.toString();
	}

	public static String formatNumber(final long p_number)
	{
		String strCurrentNumber = "" + p_number;
		String strfinalNumber = "";

		while (strCurrentNumber.length() > 3)
		{
			strfinalNumber = strCurrentNumber.substring(strCurrentNumber.length() - 3) + strfinalNumber;
			strCurrentNumber = strCurrentNumber.substring(0, strCurrentNumber.length() - 3);
			if (strCurrentNumber.length() > 0)
			{
				strfinalNumber = " " + strfinalNumber;
			}
		}
		strfinalNumber = strCurrentNumber + strfinalNumber;

		return strfinalNumber;
	}

	/**
	 * Split into token a string value delimited by a specific delimiter.
	 * 
	 * @param p_value Value to split.
	 * @param p_delimiter Delimiter.
	 * 
	 * @return List of token.
	 */
	public static final ArrayList<String> tokenize(final String p_value, final String p_delimiter)
	{
		Assert.preconditionNotNull(p_value, "Value");
		Assert.preconditionNotNull(p_delimiter, "Delimiter");
		Assert.precondition(p_delimiter.length() > 0, "Empty Delimiter");

		final ArrayList<String> list = new ArrayList<String>();

		if (p_value.length() == 0)
		{
			return list;
		}

		String currentToken;
		final StringTokenizer tokenizer = new StringTokenizer(p_value, p_delimiter);
		while (tokenizer.hasMoreTokens())
		{
			currentToken = tokenizer.nextToken();
			list.add(currentToken);
		}
		return list;
	}

	public final static String buildCSVString(final ArrayList<String> p_list, final String p_delimiter)
	{
		Assert.preconditionNotNull(p_list, "List");
		Assert.preconditionNotNull(p_delimiter, "Delimiter");
		Assert.precondition(p_delimiter.length() > 0, "Empty Delimiter");

		boolean isFirst = true;
		String currentValue;
		final StringBuffer sb = new StringBuffer();
		final Iterator<String> iter = p_list.iterator();
		while (iter.hasNext())
		{
			currentValue = iter.next();
			if (currentValue.equals("")){
				continue;
			}
			if (isFirst)
			{
				isFirst = false;
			} else
			{
				sb.append(p_delimiter);
			}
			
			Assert.precondition(currentValue.indexOf(p_delimiter) == -1	, "Token containing the delimiter has been detected");
			sb.append(currentValue);
		}
		
		return sb.toString();
	}
}