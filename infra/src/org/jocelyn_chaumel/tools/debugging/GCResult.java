package org.jocelyn_chaumel.tools.debugging;

import java.util.Date;

/**
 * This class is a tool to make easy the follow-up of the JVM memory.
 * 
 * <hr>
 * </hr>
 * <li><b>Project</b>: NECTAR</li> <li><b>Creation date</b>: 3 d�c. 08</li> <li>
 * <b>Modification date</b>: N/A</li>
 * <hr>
 * </hr>
 * 
 * @version 1.0
 * @author THALES SERVICES<br>
 */
public class GCResult
{

	public static final int NB_TEST = 5;
	
	private final Date _date = new Date(System.currentTimeMillis());

	private final long _totalMemory;

	private final long _freeMemBefore;

	private final long _freeMemAfter;

	/**
	 * Constructor.
	 * 
	 * @param p_totalMem Total Memory available at this time for the JVM.
	 * @param p_freeMemBefore Free memory available before any explicit call to
	 *        method runtime.gc().
	 * @param p_freeMemAfter Free memory available after an explicit call to
	 *        method runtime.gc().
	 */
	public GCResult(final long p_totalMem, final long p_freeMemBefore, final long p_freeMemAfter)
	{
		_totalMemory = p_totalMem;
		_freeMemBefore = p_freeMemBefore;
		_freeMemAfter = p_freeMemAfter;
	}

	/**
	 * Return the memory size used by the JVM before any explicit call to
	 * runtime.gc().
	 * 
	 * @return Memory size.
	 */
	public long getMemoryUsedBeforeGC()
	{
		return _totalMemory - _freeMemBefore;
	}

	/**
	 * Return the memory size used by the JVM after an explicit call to
	 * runtime.gc(). If an error occurs during the garbage collection, the
	 * result of this method is -1.
	 * 
	 * @return Memory size.
	 */
	public final long getMemoryUsedAfterGC()
	{
		if (_freeMemAfter == Long.MAX_VALUE)
		{
			return -1;
		}
		return _totalMemory - _freeMemAfter;
	}

	public final long getFreeMemoryAfterGC()
	{
		return _freeMemAfter;
	}

	public final long getFreeMemoryBeforeGC()
	{
		return _freeMemBefore;
	}

	public final long getMemoryCapacity()
	{
		return _totalMemory;
	}

	/**
	 * Indicates if the runtime.gc() recycled memory.
	 * 
	 * @return True if runtime.gc() has been performed succesfully.
	 */
	public final boolean isGCSuccesfull()
	{
		return _freeMemAfter > _freeMemBefore;
	}

	/**
	 * Launch the data garbage collection for the JVM and return its results.
	 * 
	 * @return GC Result object.
	 */
	public final static GCResult getMemoryUsed()
	{
		final Runtime runtime = Runtime.getRuntime();

		final long totalMem = runtime.totalMemory();
		final long freeMemBefore = runtime.freeMemory();
		long freeMemAfter = 0;

		try
		{
			for (int i = 0; i < NB_TEST; i++)
			{
				runtime.gc();
				Thread.sleep(50);
				freeMemAfter = runtime.freeMemory();
				if (freeMemAfter > freeMemBefore)
				{
					break;
				}
			}
		} catch (InterruptedException e)
		{
			return new GCResult(totalMem, freeMemBefore, Long.MAX_VALUE);
		}

		return new GCResult(totalMem, freeMemBefore, freeMemAfter);
	}

	/**
	 * Returns the date when the garbage collection has been performed.
	 * 
	 * @return GC date.
	 */
	public final Date getDate()
	{
		return _date;
	}
}
