/*
 * Assert.java
 *
 * Created on 5 f�vrier 2003, 23:33
 */

package org.jocelyn_chaumel.tools.debugging;

import java.io.File;

import org.jocelyn_chaumel.tools.StringManipulatorHelper;
import org.jocelyn_chaumel.tools.exception.InvalidResultException;

/**
 * 
 * @author Jocelyn Chaumel
 */
public class Assert
{

	/****************************
	 * V�rifie que le r�sultat soit vrai.
	 * 
	 * @param p_result
	 *            R�sultat � v�rifier
	 * @param p_message
	 *            Message devant �tre affich� advenant un mauvais r�sultat.
	 */
	public final static void precondition(final boolean p_result, final String p_message)
	{
		if (p_result == false)
		{
			throw new IllegalArgumentException(p_message);
		}
	}

	/****************************
	 * V�rifie que le r�sultat soit vrai.
	 * 
	 * @param p_result
	 *            R�sultat � v�rifier
	 * @param p_message
	 *            Message devant �tre affich� advenant un mauvais r�sultat.
	 */
	public final static void postcondition(final boolean p_result, final String p_message)
	{
		if (p_result == false)
		{
			throw new InvalidResultException(p_message);
		}
	}

	/****************************
	 * V�rifie si la valeur enti�re se situe dans entre deux bornes.
	 * 
	 * @param p_value
	 */
	public final static void precondition_between(	final int p_value,
													final int p_min,
													final int p_max,
													final String p_message)
	{
		Assert.precondition(p_value >= p_min && p_value <= p_max, p_message + ":  Value received ("
				+ Integer.toString(p_value) + ") Value expected :[" + Integer.toString(p_min) + ","
				+ Integer.toString(p_max) + "].");
	}

	/****************************
	 * V�rifie si la valeur enti�re se situe dans entre deux bornes.
	 * 
	 * @param p_value
	 */
	public final static void preconditionIsPositive(final int p_value, final String p_message)
	{
		Assert.precondition(p_value >= 0, p_message + ": this value must be positive.");
	}

	/**
	 * V�rifie si la valeur enti�re est comprise dans le tableau d'entiers.
	 * 
	 * @param p_value
	 *            Valeur enti�re.
	 * @param p_optionList
	 *            Valeurs enti�res possibles.
	 * @param p_message
	 *            Message d'erreur.
	 */
	public final static void preconditionIsIn(final int p_value, final int[] p_optionList, final String p_message)
	{

		for (int i = 0; i < p_optionList.length; i++)
		{
			if (p_optionList[i] == p_value)
			{
				return;
			}
		}

		precondition(false, p_message);
	}

	/****************************
	 * V�rifie si l'objet n'est pas nul.
	 * 
	 * @param p_object
	 *            Objet � v�rifier.
	 */
	public final static void preconditionNotNull(final Object p_object)
	{
		Assert.precondition(p_object != null, "Null Ref");
	}

	public final static void preconditionNotNull(final Object p_object, final String p_paramName)
	{
		Assert.precondition(p_object != null, "Null " + p_paramName + " Ref");
	}

	public final static void preconditionIsFile(final File p_file)
	{
		Assert.precondition(p_file.isFile(), "Unable to find this file '" + p_file.getAbsolutePath() + "'.");
	}
	
	public final static void preconditionIsNotFile(final File p_file){
	    Assert.precondition(!p_file.isFile(), "This file already exists : '" + p_file.getAbsolutePath() + "'.");
	}

	public final static void preconditionIsDir(final File p_dir)
	{
		Assert.precondition(p_dir.isDirectory(), "Unable to find this directory '" + p_dir.getAbsolutePath() + "'");
	}

    public final static void preconditionIsNotDir(final File p_dir)
    {
        Assert.precondition(!p_dir.isDirectory(), "This file is a directory '" + p_dir.getAbsolutePath() + "'");
    }

	/****************************
	 * G�n�re une exception de type IllegalStateException.
	 * 
	 *@param p_message
	 *            Contient le message de l'exception.
	 * 
	 */
	public final static void fail(final String p_message)
	{
		throw new IllegalStateException(p_message);
	}

	/****************************
	 * Affiche la trace de l'Exception et g�n�re une exception de type
	 * IllegalStateException.
	 * 
	 *@param p_exception
	 *            Exception.
	 * 
	 */
	public final static void fail(final Exception p_exception)
	{
		p_exception.printStackTrace();
		throw new IllegalStateException(p_exception.getMessage());
	}

	public final static void check(final boolean p_condition, final String p_message)
	{
		if (!p_condition)
			throw new IllegalStateException(p_message);
	}

	public final static void check(final boolean p_condition, final String p_message, final Object p_obj)
	{
		if (!p_condition)
			throw new IllegalStateException(StringManipulatorHelper.buildMessage(p_message, p_obj));
	}

	public final static void check(	final boolean p_condition,
									final String p_message,
									final Object p_obj1,
									final Object p_obj2)
	{
		if (!p_condition)
			throw new IllegalStateException(StringManipulatorHelper.buildMessage(p_message, p_obj1, p_obj2));
	}
}
