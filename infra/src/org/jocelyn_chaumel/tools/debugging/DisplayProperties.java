package org.jocelyn_chaumel.tools.debugging;

import java.util.Enumeration;
import java.util.Properties;


public class DisplayProperties
{

	public DisplayProperties(final Properties p_properties){
		String currentProperty;
		String currentValue;
		final Enumeration<Object> propertyIter = p_properties.keys();
		while (propertyIter.hasMoreElements()){
			currentProperty = (String) propertyIter.nextElement();
			currentValue = p_properties.getProperty(currentProperty);
			System.out.println(currentProperty + "=" + currentValue);
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
	
		new DisplayProperties(System.getProperties());
	}

}
