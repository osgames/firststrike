/*
 * CVSReader.java
 *
 * Created on 1 juin 2004, 18:53
 */

package org.jocelyn_chaumel.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.StringTokenizer;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Comma Seperated String permet de lire un fichier contenant des cha�nes de
 * caract�res s�par�es par une virgule. C'est seulement lorsque le fichier est
 * lu que peuvent �tre correctement utilis�es les m�thodes nextToken et
 * hasToken.
 * 
 * @author Jocelyn Chaumel
 */
public class CSSReader
{
	private final File _File;
	private StringTokenizer _FileContent;
	static private final String DELIM = ",";
	private boolean _IsFileRead = false;

	/** Creates a new instance of CVSReader */
	public CSSReader(final File aCSSFile)
	{
		Assert.precondition(aCSSFile != null, "Null Ref");
		Assert.precondition(aCSSFile.isFile(), "Invalid File");

		_File = aCSSFile;
	}

	/**
	 * Ouvre et charge enti�rement en m�moire le fichier courant.
	 * 
	 * @throws IOException
	 */
	public final void readFile() throws IOException
	{
		Assert.check(!_IsFileRead, "Th file was already read");
		FileInputStream fileStr = null;
		try
		{
			fileStr = new FileInputStream(_File);
			final int fileSize = fileStr.available();

			final byte[] byteArray = new byte[fileSize];

			Assert.postcondition(fileSize == fileStr.read(byteArray), "Invalid File Read Operation");

			_FileContent = new StringTokenizer(new String(byteArray), DELIM, false);
			_IsFileRead = true;
		} finally
		{
			fileStr.close();
		}
	}

	public final boolean hasToken()
	{
		Assert.check(_IsFileRead, "readFile method must be executed before...");

		return _FileContent.hasMoreTokens();
	}

	public final String nextToken()
	{
		Assert.check(_IsFileRead, "readFile method must be executed before...");

		return _FileContent.nextToken();
	}

}