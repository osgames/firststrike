/*
 * InvalidResultException.java
 *
 * Created on 23 f�vrier 2003, 20:52
 */

package org.jocelyn_chaumel.tools.exception;

/**
 * 
 * @author Serveur_Showmel
 */
public final class InvalidResultException extends java.lang.RuntimeException
{

	/**
	 * Creates a new instance of <code>InvalidResultException</code> without
	 * detail message.
	 */
	public InvalidResultException()
	{
	}

	/**
	 * Constructs an instance of <code>InvalidResultException</code> with the
	 * specified detail message.
	 * 
	 * @param msg
	 *            the detail message.
	 */
	public InvalidResultException(String msg)
	{
		super(msg);
	}
}
