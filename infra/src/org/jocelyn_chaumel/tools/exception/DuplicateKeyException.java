/*
 * DuplicateKeyException.java
 *
 * Created on September 16, 2003, 12:12 AM
 */

package org.jocelyn_chaumel.tools.exception;

/**
 * 
 * @author Jocelyn
 */
public class DuplicateKeyException extends Exception
{

	/** Creates a new instance of DuplicateKeyException */
	public DuplicateKeyException(final String aMessage)
	{
		super(aMessage);
	}

}
