/*
 * PropertyNotFoundException.java
 *
 * Created on 23 mai 2003, 16:24
 */

package org.jocelyn_chaumel.tools.exception;

/**
 * Exception lorsqu'une propri�t� n'existe pas ou est introuvable.
 */
public class PropertyNotFoundException extends java.lang.Exception
{

	/**
	 * Creates a new instance of <code>PropertyNotFoundException</code> without
	 * detail message.
	 */
	public PropertyNotFoundException()
	{
	}

	/**
	 * Constructs an instance of <code>PropertyNotFoundException</code> with the
	 * specified detail message.
	 * 
	 * @param msg
	 *            the detail message.
	 */
	public PropertyNotFoundException(String msg)
	{
		super(msg);
	}
}
