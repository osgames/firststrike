/*
 * DataException.java
 *
 * Created on September 20, 2003, 2:46 PM
 */

package org.jocelyn_chaumel.tools.exception;

/**
 * 
 * @author Jocelyn
 */
public class DataException extends Exception
{

	/** Creates a new instance of DataException */
	public DataException(final String aMessage)
	{
		super(aMessage);
	}

}
