/*
 * VersionException.java
 *
 * Created on 23 f�vrier 2003, 09:46
 */

package org.jocelyn_chaumel.tools.exception;

/**
 * 
 * @author Serveur_Showmel
 */
public final class VersionException extends Exception
{

	/** Creates a new instance of VersionException */
	public VersionException()
	{
	}

	public VersionException(String aMessage)
	{
		super(aMessage);
	}

}
