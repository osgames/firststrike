/*
 * FileMgr.java Created on 21 fevrier 2003, 16:48
 */

package org.jocelyn_chaumel.tools;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe simplifie la manipulation des fichiers. Permet de copier et
 * d'effacer les fichiers plus facilement.
 * 
 * @author Jocelyn Chaumel
 */
public final class FileMgr
{
	public final static String RELATIVE_DIR_IDENTIFIER = "~";

	/**
	 * Efface un fichier existant. Si le fichier n'existe pas ou qu'il ne peut
	 * etre efface, une exception est lancee.
	 * 
	 * @param aFileToDelete Fichier a effacer.
	 * @throws IOException Si le fichier ne peut etre efface.
	 */
	public final static void forceDelete(final File aFileToDelete) throws IOException
	{
		Assert.precondition(aFileToDelete != null, "Null Ref");
		Assert.precondition(aFileToDelete.exists(), "File Doesn't Exists");

		if (!aFileToDelete.delete())
		{
			throw new IOException("Unable to delete " + aFileToDelete.getAbsolutePath());
		}
	}

	public final static void forceDeleteIfPresent(final File aFileToDelete) throws IOException
	{
		Assert.precondition(aFileToDelete != null, "Null Ref");

		if (aFileToDelete.exists())
		{
			forceDelete(aFileToDelete);
		}
	}

	/**
	 * Efface le contenu d'un r�pertoire et tous ses sous-r�pertoires. Si le
	 * r�pertoire n'existe pas ou un �lement ne peut �tre supprim�, une
	 * exception est lanc�e.
	 * 
	 * @param aDirToDelete
	 */
	public final static void forceDeleteCascade(final File aDirToDelete) throws IOException
	{
		Assert.precondition(aDirToDelete != null, "Null Ref");
		Assert.precondition(aDirToDelete.isDirectory(), "Invalid Dir");

		// Trouve toutes les informations sur le r�pertoire courant.
		final Dir dir = new Dir(aDirToDelete);

		if (aDirToDelete.getAbsolutePath().equalsIgnoreCase("c:\\temp"))
		{

		}

		// Efface tous les sous-�pertoire
		final File[] dirList = dir.getDirList();
		for (int i = 0; i < dirList.length; i++)
		{
			forceDeleteCascade(dirList[i]);
		}

		// Efface tous les fichiers du r�pertoire
		final File[] fileList = dir.getFileList();
		for (int i = 0; i < fileList.length; i++)
		{
			forceDelete(fileList[i]);
		}

		forceDelete(aDirToDelete);
	}

	public final static void forceDeleteCascadeIfPresent(final File aDirToDelete) throws IOException
	{
		Assert.precondition(aDirToDelete != null, "Null Ref");
		if (aDirToDelete.isDirectory())
		{
			forceDeleteCascade(aDirToDelete);
		}
	}

	/**
	 * Copie un fichier source vers une destination.
	 * 
	 * @param aSource Fichier source.
	 * @param p_destination Fichier de destination.
	 * @param anInfo (facultatif) Cet objet permet de suivre la progression de
	 *        l'op�ration en cours.
	 * @throws IOException
	 */
	public final static void copyFile(final File aSource, final File p_destination, final ProgressInfo anInfo) throws IOException
	{
		// Les pr�conditions sont v�rifi�es par la m�thode appel�e.
		copyFile(aSource, p_destination, 4098, anInfo);
	}

	/**
	 * Copie un fichier source vers une destination.
	 * 
	 * @param p_source Fichier source.
	 * @param p_destination Fichier de destination.
	 * @param aBufferSize La taille de la m�moire tampon utilis�e pendant la
	 *        copie des donn�es.
	 * @param anInfo (facultatif) Cet objet permet de suivre la progression de
	 *        l'op�ration en cours.
	 * @throws IOException
	 */
	public final static void copyFile(	final File p_source,
										final File p_destination,
										final int aBufferSize,
										final ProgressInfo anInfo) throws IOException
	{
		Assert.precondition(p_source != null, "Null Source Ref");
		Assert.precondition(p_destination != null, "Null Destination Ref");
		Assert.precondition(p_source.exists(), "Source Doesn't exists");
		Assert.precondition(!p_destination.exists(), "Destination exists");
		Assert.precondition(aBufferSize > 0, "Invalid Buffer Size");

		ProgressInfo progressInfo = anInfo;
		if (progressInfo != null)
		{
			progressInfo.setTotal(p_source.length());
		} else
		{
			progressInfo = new ProgressInfo();
			progressInfo.setEnable(true);

		}

		FileInputStream fi = null;
		BufferedInputStream bi = null;
		FileOutputStream fo = null;
		BufferedOutputStream bo = null;
		try
		{
			fi = new FileInputStream(p_source);
			bi = new BufferedInputStream(fi);

			fo = new FileOutputStream(p_destination);
			bo = new BufferedOutputStream(fo);

			final byte[] buffer = new byte[aBufferSize];
			int size;
			int totalSize = 0;

			while ((size = bi.read(buffer)) > 0 && progressInfo.isEnabled())
			{
				bo.write(buffer, 0, size);
				totalSize += size;
				progressInfo.setCurrent(totalSize);
			}

			if (p_source.canRead())
				p_destination.setReadOnly();
		} finally
		{
			bi.close();
			fi.close();
			bo.close();
			bi.close();
		}
	}

	/**
	 * Cr�e un fichier avec son contenu sur le disque.
	 * 
	 * @param p_file Fichier � cr�er.
	 * @param p_data Donn�es � introduire dans le fichier.
	 * @throws IOException
	 */
	public final static void createFile(final File p_file, final String p_data) throws IOException
	{
		Assert.preconditionNotNull(p_file);
		Assert.preconditionNotNull(p_data);
		Assert.precondition(!p_file.isFile(), "File already exist");

		final FileOutputStream fos = new FileOutputStream(p_file);
		fos.write(p_data.getBytes());
		fos.close();
	}

	public final static void createFileIfNotExist(final File p_file, final String p_data) throws IOException
	{
		Assert.preconditionNotNull(p_file);
		Assert.preconditionNotNull(p_data);

		if (!p_file.isFile())
		{
			createFile(p_file, p_data);
		}
	}

	/**
	 * Permet de s'assurer de la cr�ation ou de l'existance d'un r�pertoire.
	 * 
	 * @param aDirectory R�pertoire � cr�er.
	 * @throws IOException Lanc�e si le r�pertoire ne peut �tre cr��.
	 */
	public final static void forceCreateDirIfNotPresent(final File aDirectory) throws IOException
	{
		// TU pour FileMgr.forceCreateDir
		Assert.preconditionNotNull(aDirectory, "Directory");

		if (!aDirectory.isDirectory())
		{
			if (!aDirectory.mkdirs())
			{
				throw new IOException("Unable to create directory : " + aDirectory.getPath());
			}
		}
	}

	public final static boolean isDirectory(final String aDirName)
	{

		Assert.preconditionNotNull(aDirName, "Dir Name");

		return new File(aDirName).isDirectory();
	}

	public final static boolean isFile(final String aFileName)
	{

		Assert.preconditionNotNull(aFileName, "File Name");

		return new File(aFileName).isFile();
	}

	/**
	 * Retourne le r�pertoire courant du syst�me d'exploitation.
	 * 
	 * @return R�pertoire courant.
	 */
	public final static File getCurrentDir()
	{
		final String currentDirStr = System.getProperties().getProperty("user.dir");
		Assert.check(currentDirStr != null, "No Current Dir Setted");
		final File currentDir = new File(currentDirStr);
		Assert.check(currentDir.isDirectory(), "Invalid Current Directory");

		return currentDir;
	}

	public final static void setCurrentDir(final File p_currentDir)
	{
		Assert.preconditionNotNull(p_currentDir, "Current Dir");
		Assert.preconditionIsDir(p_currentDir);

		System.getProperties().setProperty("user.dir", p_currentDir.getAbsolutePath());
	}

	/**
	 * Convert a file into a String. If the string is too long, the method
	 * replaces some character by this "...".
	 * 
	 * @param p_file File to convert.
	 * @param p_lenght Max string path size
	 * @return Path String
	 */
	public final static String convertPathToString(final File p_file, final int p_lenght)
	{
		Assert.preconditionNotNull(p_file, "file");
		Assert.precondition(p_lenght > 10, "Invalid lenght");

		final String pathName = p_file.getAbsolutePath();

		if (pathName.length() <= p_lenght)
		{
			return pathName;
		}

		final String fileName = p_file.getName();
		if (fileName.length() + 4 <= p_lenght)
		{
			final int startingLenght = p_lenght - (5 + fileName.length());
			return pathName.substring(0, startingLenght) + " ... " + fileName;
		} else
		{
			final int startingLenght = fileName.length() - (p_lenght -4);
			return "... " + fileName.substring(startingLenght, fileName.length());
		}
	}

	public static final String getRelativePath(final File p_file)
	{
		Assert.preconditionNotNull(p_file, "file");
		final String strFile = p_file.getAbsolutePath();
		final String strCurrentPath = getCurrentDir().getAbsolutePath();
		if (!strFile.startsWith(strCurrentPath))
		{
			return strFile;
		}

		return RELATIVE_DIR_IDENTIFIER + strFile.substring(strCurrentPath.length());
	}

	/**
	 * Build a File object from a relative file name (starting with "~\") and
	 * the OS's current dir.
	 * 
	 * @param p_relativePath relative file name starting with "~\".
	 * @return a File object instance.
	 */
	public static final File getFileFromRelativPath(final String p_relativePath)
	{
		Assert.preconditionNotNull(p_relativePath, "Relative Path");

		return getFileFromRelativPath(getCurrentDir(), p_relativePath);
	}

	/**
	 * Build a File object from a relative file name (starting with "~\") and
	 * based on root dir.
	 * 
	 * @param p_currentDir Root dir.
	 * @param p_relativePath relative file name starting with "~\".
	 * @return a File object instance.
	 */
	public static final File getFileFromRelativPath(final File p_currentDir, final String p_relativePath)
	{
		Assert.preconditionNotNull(p_relativePath, "Relative Path");
		Assert.preconditionNotNull(p_currentDir, "Current Dir");
		Assert.preconditionIsDir(p_currentDir);

		final File file;
		if (p_relativePath.startsWith(RELATIVE_DIR_IDENTIFIER))
		{
			file = new File(p_currentDir, p_relativePath.substring(RELATIVE_DIR_IDENTIFIER.length()));
		} else
		{
			file = new File(p_relativePath);
		}

		return file;
	}

	/**
	 * Return the extention of file.
	 * 
	 * @param p_file file to inspect.
	 * @return File Extention
	 */
	public static final String getFileExtention(final File p_file)
	{
		Assert.preconditionNotNull(p_file, "File");

		final String fileName = p_file.getName();
		final int pos = fileName.lastIndexOf(".");
		if (pos == -1 || pos + 1 > fileName.length())
		{
			return "";
		}
		final String extention = fileName.substring(pos + 1);

		return extention;
	}

	/**
	 * Return the name of file wihtout its extention.
	 * 
	 * @param p_file Expected file.
	 * @return short file name.
	 */
	public static final String getFileNameWithoutExtention(final File p_file)
	{
		Assert.preconditionNotNull(p_file, "File");

		final String fullName = p_file.getName();
		final int position = fullName.lastIndexOf(".");
		final String shortName;
		if (position == -1)
		{
			shortName = fullName;
		} else
		{
			shortName = fullName.substring(0, position);
		}

		return shortName;
	}

	/**
	 * Check if the file finish with the expected extiontion. If not, the
	 * extention is appended and a new File object is generated.
	 * 
	 * @param p_file File
	 * @param p_expectedExtention Expected file extention
	 * @return A file with the expected extention.
	 */
	public static File addExtentionIfNecessairy(final File p_file, final String p_expectedExtention)
	{
		Assert.preconditionNotNull(p_file, "File");
		Assert.preconditionNotNull(p_expectedExtention, "File Extention");
		Assert.precondition(!p_expectedExtention.startsWith("."),
							"Expected extention doesn't start with a dot character");
		Assert.precondition(p_expectedExtention.trim().length() > 0, "Invalid extention");

		final String extention = getFileExtention(p_file).toUpperCase();

		if (extention.equals(p_expectedExtention.toUpperCase()))
		{
			return p_file;
		}

		final String fileName = p_file.getAbsolutePath();
		final File newFileWithExtention;
		if (fileName.endsWith("."))
		{
			newFileWithExtention = new File(fileName + p_expectedExtention);
		} else
		{
			newFileWithExtention = new File(fileName + "." + p_expectedExtention);
		}
		return newFileWithExtention;
	}
}