/*
 * JCArrays.java
 *
 * Created on 17 mars 2003, 22:19
 */

package org.jocelyn_chaumel.tools;

import java.util.Arrays;

/**
 * Cette classe offre des outils pour simplifier la manipulation de tableaux �
 * double dimensions.
 */
public final class DoubleArrays
{
	private DoubleArrays()
	{
	}

	public static final boolean equals(int[][] anArray1, int[][] anArray2)
	{
		if (anArray1 == null && anArray2 == null)
			return true;

		if (anArray1 == null || anArray2 == null)
			return false;

		if (anArray1.length != anArray2.length)
			return false;

		for (int y = 0; y < anArray1.length; y++)
		{
			if (!Arrays.equals(anArray1[y], anArray2[y]))
				return false;
		}

		return true;
	}

}
