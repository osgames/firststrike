Instructions
==================================
1) Introduction & PREREQUISITES
2) How to install & launch First Strike 2.1
3) FAQ
2.1) Unable to start First Strike
2.2) How can i have support?



1] INTRODUCTION & PREREQUISITES
==================================
FirstStrike is a strategic turn based game. This means that each player plays separetly.

and it is developped with Java.  Java is not delivered in the installation package.  If you don't have java installed on your PC, you must download and install Java (before or after the FirstStrike installation).  

To download java, go to http://www.java.com with your favorite browser.




2] HOW TO INSTALL & LAUNCH FIRST STRIKE 2.1
==============================================
2.1] Installation
---------------------------------
double-clic on the FirstStrike_v2.1.0.exe and follow the instruction.


2.2] Launch
---------------------------------
doublic-clic on fs.bat, FirstStrike desktop shortcut or on the FirstStrike Start Menu shortcut.




3] FAQ
==================================
3.1] Unable to start First Strike
---------------------------------
First, check if Java is installed on your PC.  Open the Windows Control Panel.  Display by icon and double-clic on Java.  If no Java icon exists, then you must download & install Java.
URL : http://www.java.com

If Java is installed, visualize the available version.  Choose the highest version and copy the installation folder in the paste the full path in the First Strike file  set_java_location_manually located in the installation folder.

Now, you can launch 2.2] FirstStrike.


3.2] How can i have support?
---------------------------------
Goto on the First Strike forum "Help" and post a message
http://sourceforge.net/projects/firststrikegame/forums/forum/817555

