/*
 * PlayerInfo.java Created on 1 juin 2003, 16:38
 */

package org.jocelyn_chaumel.firststrike.data_type;

import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.firststrike.core.player.cst.PlayerTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Classe permettant de d�finir les caract�risques des joueurs � la cr�ation
 * d'une partie. Un joueur peut �tre un usager ou l'ordinateur.
 * 
 * @author Jocelyn Chaumel
 */
public final class PlayerInfo
{
	private final PlayerTypeCst _owner;
	private final Coord _startLocation;
	private final PlayerLevelCst _level;

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param anOwnerType Type de joueur.
	 * @param p_playerName Nom du joueur.
	 * @param p_playerDescription Description du joueur.
	 * @param p_startLocation Position de d�part du joueur.
	 * @throws InvalidConstantValueException
	 */
	public PlayerInfo(final PlayerTypeCst anOwnerType, final Coord p_startLocation, final PlayerLevelCst aLevel)
	{
		Assert.preconditionNotNull(anOwnerType, "Owner Type");
		Assert.precondition(p_startLocation != null, "Null param.");
		Assert.precondition(p_startLocation.getX() >= 0, "Invalid x.");
		Assert.precondition(p_startLocation.getY() >= 0, "Invalid y.");

		_level = aLevel;

		_owner = anOwnerType;
		_startLocation = p_startLocation;
	}

	/**
	 * Retourne le type du joueur.
	 * 
	 * @return Type de joueur.
	 */
	public final PlayerTypeCst getOwner()
	{
		return _owner;
	}

	public final Coord getStartLocation()
	{
		return _startLocation;
	}

	public final PlayerLevelCst getPlayerLevel()
	{
		return _level;
	}

	@Override
	public final String toString()
	{
		final StringBuffer sb = new StringBuffer();

		sb.append("{PLAYER_INFO= ");
		sb.append("type:").append(_owner);
		sb.append(", ").append(_startLocation);
		sb.append(", level:").append(_level).append("}");

		return sb.toString();
	}
}