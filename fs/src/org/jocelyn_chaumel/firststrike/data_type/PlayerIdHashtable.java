/*
 * Created on Mar 19, 2005
 */
package org.jocelyn_chaumel.firststrike.data_type;

import org.jocelyn_chaumel.firststrike.core.player.PlayerId;
import org.jocelyn_chaumel.tools.data_type.AbstractInstanceHashtable;

/**
 * @author Jocelyn Chaumel
 */
public class PlayerIdHashtable extends AbstractInstanceHashtable
{

	/**
	 * Constructeur paramétrisé.
	 * 
	 * @param anInstanceKey
	 */
	public PlayerIdHashtable()
	{
		super(PlayerId.class);
	}
}
