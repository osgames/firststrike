/*
 * Created on Jan 28, 2006
 */
package org.jocelyn_chaumel.firststrike.data_type;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.CombatResultCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class SingleAttackResult
{

	private final CombatResultCst _combatResult;

	private final int _attack;

	private final int _defense;

	private final AbstractUnit _playerUnit;

	private final AbstractUnit _enemyUnit;

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param aCombatResultCst R�sultat du combat (Aucun gagnant)
	 * @param anAttack Force de l'attaque.
	 * @param aDefense Force de la d�fence.
	 * @param p_playerUnit R�f�rence de l'unit� du joueur.
	 * @param p_enemy R�f�rence de l'unit� ennemie.
	 */
	public SingleAttackResult(	final AbstractUnit p_playerUnit,
								final int anAttack,
								final int aDefense,
								final AbstractUnit p_enemy)
	{
		Assert.preconditionNotNull(p_playerUnit, "Player Unit");
		Assert.precondition(anAttack >= 0, "Invalid Attack");
		Assert.precondition(aDefense >= 0, "Invalid Defense");
		Assert.preconditionNotNull(p_enemy, "Enemy Unit");

		_combatResult = CombatResultCst.NO_WIN;
		_attack = anAttack;
		_defense = aDefense;
		_playerUnit = p_playerUnit;
		_enemyUnit = p_enemy;
	}

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param aCombatResultCst R�sultat du combat (Aucun gagnant)
	 * @param anAttack Force de l'attaque.
	 * @param p_playerUnit R�f�rence de l'unit� du joueur.
	 */
	public SingleAttackResult(final AbstractUnit p_playerUnit, final int anAttack, final int aDefense)
	{
		Assert.precondition(anAttack >= 0, "Invalid Attack");
		Assert.precondition(aDefense >= 0, "Invalid Defense");
		Assert.preconditionNotNull(p_playerUnit, "Player Unit");

		_combatResult = CombatResultCst.ATTACKER_WIN;
		_attack = anAttack;
		_defense = aDefense;
		_playerUnit = p_playerUnit;
		_enemyUnit = null;
	}

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param aCombatResultCst R�sultat du combat (Aucun gagnant)
	 * @param anAttack Force de l'attaque.
	 * @param aDefense Force de la d�fence.
	 * @param p_playerUnit R�f�rence de l'unit� du joueur.
	 * @param p_enemy R�f�rence de l'unit� ennemie.
	 */
	public SingleAttackResult(final int anAttack, final int aDefense, final AbstractUnit p_enemy)
	{
		Assert.precondition(anAttack >= 0, "Invalid Attack");
		Assert.precondition(aDefense >= 0, "Invalid Defense");
		Assert.preconditionNotNull(p_enemy, "Enemy Unit");

		_combatResult = CombatResultCst.DEFENSER_WIN;
		_attack = anAttack;
		_defense = aDefense;
		_playerUnit = null;
		_enemyUnit = p_enemy;
	}

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param aCombatResultCst R�sultat du combat (Aucun gagnant)
	 * @param anAttack Force de l'attaque.
	 * @param aDefense Force de la d�fence.
	 * @param p_playerUnit R�f�rence de l'unit� du joueur.
	 * @param p_enemy R�f�rence de l'unit� ennemie.
	 */
	public SingleAttackResult(final int anAttack, final int aDefense)
	{
		Assert.precondition(anAttack >= 0, "Invalid Attack");
		Assert.precondition(aDefense >= 0, "Invalid Defense");

		_combatResult = CombatResultCst.ATT_AND_DEF_LOOSE;
		_attack = anAttack;
		_defense = aDefense;
		_playerUnit = null;
		_enemyUnit = null;
	}

	public final CombatResultCst getCombatResult()
	{
		return _combatResult;
	}

	public final int getDefense()
	{
		return _defense;
	}

	public final AbstractUnit getEnemyUnit()
	{
		Assert.check(_enemyUnit != null, "Enemy unit was defeated");
		return _enemyUnit;
	}

	public final AbstractUnit getPlayerUnit()
	{
		Assert.check(_playerUnit != null, "Player unit was defeated");
		return _playerUnit;
	}

	public final int getAttack()
	{
		return _attack;
	}
}