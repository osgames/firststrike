package org.jocelyn_chaumel.firststrike.data_type;

import java.util.HashMap;
import java.util.Locale;

import org.jdom2.Element;

public final class I18NMessage
{
	public final static String FR = Locale.FRENCH.getLanguage();
	public final static String EN = Locale.ENGLISH.getLanguage();

	private String[] _supportedLanguage = { EN, FR };

	private HashMap<String, String> _i18nMap = new HashMap<String, String>();

	public I18NMessage(final Element p_element)
	{
		Element currentElement = null;
		for (String language : _supportedLanguage)
		{
			currentElement = p_element.getChild(language);
			if (currentElement == null)
			{
				continue;
			}

			String text = currentElement.getText();
			if (text == null)
			{
				continue;
			}

			_i18nMap.put(language, text);
		}
		if (_i18nMap.isEmpty())
		{
			final String text = p_element.getText();
			if (text == null)
			{
				throw new NullPointerException("No text message found within this Element: " + p_element.getName());
			}

			_i18nMap.put(EN, text);
		}
	}

	public final String getI18NText()
	{
		String text = _i18nMap.get(Locale.getDefault().getLanguage());
		if (text != null)
		{
			return text;
		}

		return _i18nMap.get(EN);
	}
}
