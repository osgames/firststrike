/*
 * Created on Nov 9, 2004
 */
package org.jocelyn_chaumel.firststrike;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.MapScenario;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioCity;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioPlayer;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.player.cst.PlayerTypeCst;
import org.jocelyn_chaumel.firststrike.data_type.PlayerInfo;
import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Gestionnaire de partie First Strike. Elle permet de cr�er, sauvegarder et
 * recharger une partie.
 * 
 * @author Jocelyn Chaumel
 */

public final class FirstStrikeMgr
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(FirstStrikeMgr.class.getName());

	public static FirstStrikeGame createFirstStrikeGame(final String p_gameName,
														final PlayerInfoList p_playerInfoList,
														final FSMap p_map) throws IOException
	{
		Assert.preconditionNotNull(p_playerInfoList, "Player Info List");
		Assert.precondition(p_playerInfoList.size() > 1, "Invalid Player Info List");
		Assert.preconditionNotNull(p_map, "Map");
		Assert.precondition(((PlayerInfo) p_playerInfoList.get(0)).getOwner() == PlayerTypeCst.HUMAN,
							"Invalid Player Info List");

		_logger.system("Creating Game ...");

		FirstStrikeGame fsGame = null;
		final FSMapMgr mapMgr = new FSMapMgr(p_map);
		final AbstractPlayerList playerList = PlayerMgr.createPlayers(p_playerInfoList, mapMgr);
		final PlayerMgr playerMgr = new PlayerMgr(playerList);
		final FSRandomMgr randomMgr = new FSRandomMgr(5);
		final FSOptionMgr optionMgr = new FSOptionMgr();

		// Cr�ation de la partie
		fsGame = new FirstStrikeGame(p_gameName, playerMgr, mapMgr, randomMgr, optionMgr);
		_logger.systemAction("Game created");
		return fsGame;
	}

	public static FirstStrikeGame createFirstStrikeGame(final MapScenario p_scenario, final FSMap p_map) throws IOException,
			InvalidScenarioException
	{
		Assert.preconditionNotNull(p_scenario, "Scenario");
		Assert.preconditionNotNull(p_map, "Map");

		_logger.system("Creating Game ...");

		FirstStrikeGame fsGame = null;
		final FSMapMgr mapMgr = new FSMapMgr(p_map);
		final AbstractPlayerList playerList = PlayerMgr.createPlayers(p_scenario, mapMgr);
		final PlayerMgr playerMgr = new PlayerMgr(playerList);
		final FSRandomMgr randomMgr = new FSRandomMgr(5);
		final FSOptionMgr optionMgr = new FSOptionMgr();
		final CityList cityList = mapMgr.getCityList();

		// Cr�ation de la partie
		fsGame = new FirstStrikeGame(p_scenario.getName(), playerMgr, mapMgr, randomMgr, optionMgr);

		AbstractPlayer currentPlayer = null;
		City currentCity = null;

		for (ScenarioPlayer scnPlayer : p_scenario.getPlayerList())
		{
			currentPlayer = playerList.get(scnPlayer.getIndex() - 1);
			Assert.precondition(currentPlayer.getId().getId() == scnPlayer.getIndex(), "Unexpected index result");

			for (ScenarioCity scnCity : scnPlayer.getCityList())
			{
				currentCity = cityList.getCityAtPosition(scnCity.getPosition());
				currentPlayer.addCity(currentCity, false);
			}
		}

		_logger.systemAction("Game created");
		return fsGame;
	}

	/**
	 * Charge une partie FirstStrike � partir d'un r�pertoire.
	 * 
	 * @param p_file R�pertoire de la partie.
	 * @return Partie First Strike.
	 * @throws IOException
	 * @throws SecurityException
	 * @throws InvalidConstantValueException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static FirstStrikeGame loadFirstStrikeGame(final File p_file) throws Exception
	{
		Assert.preconditionNotNull(p_file);
		Assert.preconditionIsFile(p_file);

		_logger.systemAction("Loading game ...");

		FileInputStream fis = null;
		FirstStrikeGame fsg = null;
		ObjectInputStream ois = null;
		try
		{
			fis = new FileInputStream(p_file);
			ois = new ObjectInputStream(fis);
			fsg = (FirstStrikeGame) ois.readObject();
		} catch (Exception ex)
		{
			_logger.systemAction("Loading error detected : " + ex.getMessage());
			throw ex;
		} finally
		{
			if (fis != null)
			{
				fis.close();
			}
			if (ois != null)
			{
				ois.close();
			}
		}

		return fsg;
	}

	/**
	 * Sauvegarde une partie First Strike.
	 * 
	 * @param p_game Partie � sauvegarder.
	 * @param p_gameFile R�pertoire courant de la partie.
	 * @throws IOException
	 * @throws IOException
	 * @throws SecurityException
	 */
	public static void saveFirstStrikeGame(final FirstStrikeGame p_game, final File p_gameFile) throws IOException
	{
		Assert.preconditionNotNull(p_game, "Game");
		Assert.preconditionNotNull(p_gameFile, "File");

		updateGameName(p_game, p_gameFile);

		FileOutputStream fos = null;
		try
		{
			fos = new FileOutputStream(p_gameFile);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(p_game);
		} finally
		{
			if (fos != null)
			{
				fos.flush();
				fos.close();
			}
		}
	}

	/**
	 * Updates the name of the game. This name is composed of 3 parts :
	 * <ul>
	 * <li>file name without extension</li>
	 * <li>turn number (4 digits)</li>
	 * <li>Time in millisecond</li>
	 * </ul>
	 * Each parts are delimited by this special character '_'.
	 * 
	 * @param p_game
	 * @param p_file
	 */
	private static void updateGameName(FirstStrikeGame p_game, File p_file)
	{
		String shortName = FileMgr.getFileNameWithoutExtention(p_file);

		p_game.setShortGameName(shortName);
		p_game.setSaveDate(System.currentTimeMillis());

	}
}