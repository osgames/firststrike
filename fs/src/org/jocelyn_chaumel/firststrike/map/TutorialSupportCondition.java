package org.jocelyn_chaumel.firststrike.map;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.scenario.IMapCondition;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;

public class TutorialSupportCondition implements IMapCondition, Serializable
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TutorialSupportCondition.class.getName());
	private byte _phase = 1;
	
	@Override
	public boolean executeCondition(final AbstractPlayer p_player, final int p_currentTurn)
	{
		_logger.playerAction("Check Tutorial conditions");
		HumanPlayer player = (HumanPlayer) p_player;
		
		if (_phase == 1)
		{
			checkPhase1(player, p_currentTurn);
			return false;
		}
		
		if (_phase == 2)
		{
			checkPhase2(player, p_currentTurn);
			return false; 
		}
		
		if (_phase == 3)
		{
			checkPhase3(player, p_currentTurn);
			return false; 
		}

		return false;
	}
	
	private boolean checkPhase1(final HumanPlayer p_player, final int p_currentTurn)
	{
		if (p_currentTurn > 3) {
			for (City city : p_player.getCitySet())
			{
				if (city.isUnitUnderConstruction() && city.getProductionType().equals(UnitTypeCst.TRANSPORT_SHIP))
				{
					++_phase;
					return true;
				}
			}
			
			p_player.duplicateReferenceByTurn("phase_1", p_currentTurn);
		}
		return false;
	}
	
	private boolean checkPhase2(final HumanPlayer p_player, final int p_currentTurn)
	{
		for (City city : p_player.getCitySet())
		{
			if (city.isUnitUnderConstruction() && city.getProductionType().equals(UnitTypeCst.TRANSPORT_SHIP) && city.getProductionTime() == 1)
			{

				p_player.replaceReferenceByTurn("phase_2", p_currentTurn);
				++_phase;
				return true;
			}
		}
		
		return false;
	}

	
	private boolean checkPhase3(final HumanPlayer p_player, final int p_currentTurn)
	{
		if (p_player.getUnitList().getUnitByType(UnitTypeCst.DESTROYER).size() == 1)
		{
			p_player.duplicateReferenceByTurn("phase_3-destroyer", p_currentTurn);
			p_player.duplicateReferenceByTurn("phase_3-msg", p_currentTurn);
		}

		if (p_player.getUnitList().getUnitByType(UnitTypeCst.DESTROYER).size() == 0)
		{
			p_player.duplicateReferenceByTurn("phase_3-destroyer", p_currentTurn);
			p_player.duplicateReferenceByTurn("phase_3-msg", p_currentTurn);
			p_player.duplicateReferenceByTurn("phase_3-figter", p_currentTurn);
		}
		final AbstractUnitList units = p_player.getUnitList().getGroundCombatUnitList();
		if (units.getUnitsByAreaName(p_player.getMapMgr().getMap(), "Continent").size() > 1) {
			p_player.replaceReferenceByTurn("continent", p_currentTurn);
			return true;
		}
		
		return false;
	}
}
