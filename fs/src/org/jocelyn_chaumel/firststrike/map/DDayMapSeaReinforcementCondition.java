package org.jocelyn_chaumel.firststrike.map;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.scenario.IMapCondition;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;

public class DDayMapSeaReinforcementCondition implements IMapCondition, Serializable
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(DDayMapSeaReinforcementCondition.class.getName());
	
	@Override
	public boolean executeCondition(final AbstractPlayer p_player, final int p_currentTurn)
	{
		_logger.playerAction("Check DDay condition");
		AbstractPlayer player = (AbstractPlayer) p_player;
		
		if ( player.getUnitList().getUnitByType(UnitTypeCst.TANK).size() <= 7 && player.getCitySet().size() == 0) 
		{
			_logger.playerSubAction("Sea Reinforcement Condition not reached");
			return false;
		}
		_logger.playerSubAction("Sea Reinforcement Condition reached!!");
		
		player.clearScenarioUnitWithReference("");
		player.replaceReferenceByTurn("landing", p_currentTurn);
		

		return true;
	}

}
