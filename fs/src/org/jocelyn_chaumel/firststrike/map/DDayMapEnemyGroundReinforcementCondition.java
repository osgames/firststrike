package org.jocelyn_chaumel.firststrike.map;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.scenario.IMapCondition;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;

public class DDayMapEnemyGroundReinforcementCondition implements IMapCondition, Serializable
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(DDayMapSeaReinforcementCondition.class.getName());
	
	@Override
	public boolean executeCondition(final AbstractPlayer p_player, final int p_currentTurn)
	{
		_logger.playerAction("Check DDay Enemy Ground Reinformcement Condition");
		AbstractPlayer player = (AbstractPlayer) p_player;
		
		if (player.getMapMgr().getCityList().getCityToConquer(player).size() < 3)
		{
			_logger.playerSubAction("Enemy Ground Reinforcement Condition not reached.");
			return false;
		}
		
		_logger.playerSubAction("Enemy Ground Reinforcement Condition reached!!");
		
		final int nbHumanTank = player.getUnitIndex().getEnemyList(player).getUnitByType(UnitTypeCst.TANK).size();
		
		final int nbTurns = Math.max(1, Math.round( nbHumanTank / 4f));
		
		int turnArray [] = new int[nbTurns];
		
		for (int i = 0; i < nbTurns; i++)
		{
			turnArray[i] = p_currentTurn + i;
		}
	
		player.replaceReferenceByTurns("ground_reinforcement", turnArray);
		
		return true;
	}

}
