package org.jocelyn_chaumel.firststrike.map;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.scenario.IMapCondition;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;

public class DDayMapAirReinforcementCondition implements IMapCondition, Serializable
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(DDayMapAirReinforcementCondition.class.getName());
	
	@Override
	public boolean executeCondition(final AbstractPlayer p_player, final int p_currentTurn)
	{
		_logger.playerAction("Check DDay condition");
		AbstractPlayer player = (AbstractPlayer) p_player;
		
		if ( player.getCitySet().size() == 0) 
		{
			_logger.playerSubAction("Bridgehead Condition not reached (number of city == 0");
			return false;
		}
		_logger.playerSubAction("Bridgehead Condition reached!!");
		
		player.replaceReferenceByTurn("bridgehead", p_currentTurn);

		return true;
	}

}
