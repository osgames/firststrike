/*
 * Created on Mar 26, 2005
 */
package org.jocelyn_chaumel.firststrike.core;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.jocelyn_chaumel.tools.data_type.FloatList;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe g�n�re des nombres de facon al�atoire. Il est cependant possible
 * de lui fournir la liste des nombres "al�atoire" ou lui indiquer une constante
 * num�rique comme valeur de retour.
 * 
 * @author Jocelyn Chaumel
 */
public final class FSRandom implements Serializable
{

	/**
	 * Unique instance de cette classe.
	 */
	private static FSRandom _instance;

	/**
	 * Objet permettant de r�cup�rer des nombres al�atoire.
	 */
	private static final Random _random = new Random(System.currentTimeMillis());

	/**
	 * Liste des nombres al�atoire pr�-d�fini.
	 */
	private FloatList _floatList = null;

	/**
	 * Nombre al�atoire pr�-d�fini.
	 */
	private Float _constantValue = null;

	/**
	 * Constructeur priv�. Cette classe est un singleton.
	 */
	private FSRandom()
	{
	}

	/**
	 * Retourne l'instance de la classe FSRandom sans la modifier si elle a d�j�
	 * �t� instanci�e.
	 * 
	 * @return instance de la classe.
	 */
	public final static FSRandom getInstance()
	{
		if (_instance == null)
		{
			_instance = new FSRandom();
		}

		return _instance;
	}

	/**
	 * Retourne la nouvelle instance de la classe FSRandom. De plus, celle-ci
	 * est initialis�e avec une liste de nombres al�atoires d�j� identifi�s.
	 * 
	 * @param anIntegerList Liste de nombre al�atoire d�j� identifi�s.
	 * @return L'instance de la classe FSRandom
	 */
	public final static FSRandom getInstance(final FloatList p_floatList)
	{
		Assert.preconditionNotNull(p_floatList, "Float List");
		if (_instance == null)
		{
			_instance = new FSRandom();
		} else
		{
			_instance.reset();
		}

		_instance._floatList = p_floatList;

		return _instance;
	}

	/**
	 * Retourne la nouvelle instance de la classe FSRandom. Cette instance
	 * retourne toujours le m�me nombre al�atoire.
	 * 
	 * @param p_constantValue Nombre al�atoire � retourner syst�matiquement.
	 * @return L'instance de la classe FSRandom.
	 */
	public final static FSRandom getInstance(final float p_constantValue)
	{
		if (_instance == null)
		{
			_instance = new FSRandom();
		} else
		{
			_instance.reset();
		}
		_instance._constantValue = new Float(p_constantValue);

		return _instance;
	}

	public final static FSRandom getInstance(final float p_float1, final float p_float2)
	{
		return FSRandom.getInstance(new float[] { p_float1, p_float2 });
	}

	public final static FSRandom getInstance(final float p_float1, final float p_float2, final float p_float3)
	{
		return FSRandom.getInstance(new float[] { p_float1, p_float2, p_float3 });
	}

	public final static FSRandom getInstance(final float[] p_floatArray)
	{
		Assert.preconditionNotNull(p_floatArray, "float array");
		final FloatList floatList = new FloatList(p_floatArray);
		return FSRandom.getInstance(floatList);
	}

	/**
	 * Efface tous les nombres al�atoires �tablis d'avance.
	 */
	public final void reset()
	{
		if (_floatList != null)
		{
			_floatList.clear();
			_floatList = null;
		}

		_constantValue = null;
	}

	/**
	 * Retourne le prochain nombre al�atoire.
	 * 
	 * @return Nombre al�atoire entre 0 et 1.
	 */
	public final float getRandomFloat()
	{
		Assert.check(_floatList == null || _constantValue == null, "Constant and list of values are specified");

		float floatNumber;

		if (_floatList != null)
		{
			floatNumber = ((Float) _floatList.get(0)).floatValue();
			_floatList.remove(0);
			if (_floatList.size() == 0)
			{
				_floatList = null;
			}
		} else if (_constantValue != null)
		{
			floatNumber = _constantValue.floatValue();
		} else
		{
			floatNumber = getNewRandomFloat();
		}

		Assert.check(floatNumber >= 0.0f, "Invalid Float Value : lower than 0 (" + floatNumber + ")");
		Assert.check(floatNumber <= 1.0f, "Invalid Float Value : upper than 1 (" + floatNumber + ")");

		return floatNumber;
	}

	final float getNewRandomFloat()
	{
		return Math.abs(((float) _random.nextInt()) / (float) Integer.MAX_VALUE);
	}

	public final Object getRandomObjectFromTheList(final List p_objectList)
	{
		
		Assert.preconditionNotNull(p_objectList, "Object List");
		Assert.precondition(!p_objectList.isEmpty(), "Empty Object List");
		
		// required otherwise this method doesn't provide always the same result.
		Collections.sort(p_objectList);

		final int size = p_objectList.size() - 1;

		final int idx = Math.round(size * getRandomFloat());
		return p_objectList.get(idx);
	}
}