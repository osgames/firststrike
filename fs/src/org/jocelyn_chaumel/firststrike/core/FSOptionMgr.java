package org.jocelyn_chaumel.firststrike.core;

import java.io.File;
import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe g�rer toutes les options du jeux FS lorsqu'elles ne sont pas
 * g�r�es par les autres Mgr.
 * 
 * @author Jocelyn Chaumel
 */
public class FSOptionMgr implements Serializable
{
	private boolean _fogOption = false;
	private boolean _autoSaveOption = false;
	private boolean _turnSaveMode = false;
	private boolean _debugMode = false;
	private File _userSkinDir = null;

	public FSOptionMgr()
	{
		final ApplicationConfigurationMgr confMgr = ApplicationConfigurationMgr.getInstance();

		_fogOption = confMgr.isDefaultFogActivation();
		_autoSaveOption = confMgr.isDefaultAutoSave();
		_turnSaveMode = confMgr.isTurnSaveMode();
		_userSkinDir = confMgr.getDefaultSkin();
		_debugMode = confMgr.isDebugMode();
	}

	public final void setFogActivation(final boolean p_fogActivationFlag)
	{
		_fogOption = p_fogActivationFlag;
	}

	public final boolean isFogActivation()
	{
		return _fogOption;
	}

	public final void setAutoSave(final boolean p_autoSave)
	{
		_autoSaveOption = p_autoSave;
	}

	public final boolean isAutoSave()
	{
		return _autoSaveOption;
	}

	public final boolean isDebutMode()
	{
		return _debugMode;
	}
	
	public final boolean isTurnSaveMode()
	{
		return _turnSaveMode;
	}

	public final void setUserSkin(final File p_userSkinDir)
	{
		if (p_userSkinDir == null)
		{
			_userSkinDir = null;
			return;
		}
		Assert.preconditionIsDir(p_userSkinDir);
		_userSkinDir = p_userSkinDir;
	}

	public final File getUserSkin()
	{
		return _userSkinDir;
	}
}
