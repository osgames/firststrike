package org.jocelyn_chaumel.firststrike.core;

import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.tools.data_type.Coord;

public interface IFirstStrikeEvent
{
	public void eventPlayerStartTurn(final AbstractPlayer p_player);

	public void eventPlayerEndTurn(final AbstractPlayer p_player);

	public void eventPlayerKilled(final AbstractPlayer p_player);

	public void eventUnitCreated(final AbstractUnit p_unit);

	public void eventUnitMoved(final AbstractUnit p_unit, final Coord p_newPosition, final Coord p_oldPosition);

	public void eventUnitLoaded(final AbstractUnit p_loadedUnit, final AbstractUnit p_unitContainer);

	public void eventUnitUnloaded(	final AbstractUnit p_unloadedUnit,
									final AbstractUnit p_unitContainer,
									final Coord p_destination);

	public void eventUnitKilled(final AbstractUnit p_unit);

	public void eventUnitFinished(final AbstractUnit p_unit);

	public void eventCityConquered(final City p_city, final AbstractPlayer p_newPlayer, final AbstractPlayer p_oldPlayer);
}
