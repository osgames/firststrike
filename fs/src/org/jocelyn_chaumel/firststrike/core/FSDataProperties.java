/*
 * Created on Nov 26, 2005
 */
package org.jocelyn_chaumel.firststrike.core;

import java.io.File;
import java.io.IOException;

import org.jocelyn_chaumel.tools.DataProperties;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public abstract class FSDataProperties extends DataProperties
{
	protected abstract String getFilename();

	protected abstract String getComment();

	protected static final String DELIM = ";";

	@Override
	public final void load(final File aDirectory) throws IOException
	{
		Assert.preconditionNotNull(aDirectory, "Directory");
		Assert.precondition(aDirectory.isDirectory(), "Invalid Directory");

		super.load(new File(aDirectory, getFilename()));
	}

	public final void store(final File aDirectory) throws IOException
	{
		Assert.preconditionNotNull(aDirectory, "Directory");
		Assert.precondition(aDirectory.isDirectory(), "Invalid Directory");

		final File file = new File(aDirectory, getFilename());
		super.store(file, getComment());
	}

}
