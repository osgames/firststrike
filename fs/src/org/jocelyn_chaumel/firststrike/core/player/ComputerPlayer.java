/*
 * ComputerPlayer.java Created on 27 avril 2003, 21:18
 */

package org.jocelyn_chaumel.firststrike.core.player;

import java.util.HashMap;
import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.IFirstStrikeEvent;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaList;
import org.jocelyn_chaumel.firststrike.core.area.AreaStatusCst;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.configuration.IConfiguration;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.artillery.ArtilleryAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.battleship.BattleshipAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.destroyer.DestroyerAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.fighter.FighterAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.tank.TankAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.transportship.TransportShipAutoDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMapConditionList;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioUnitList;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioViewAreaList;
import org.jocelyn_chaumel.firststrike.core.player.cst.PlayerTypeCst;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.Bombardment;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.ILoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.UnitCombatInfo;
import org.jocelyn_chaumel.firststrike.core.unit.UnitIndex;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.data_type.CoupleCoord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe repr�sente un joueur de type ordinateur.
 */
public final class ComputerPlayer extends AbstractPlayer
{
	private final FSLogger _logger = FSLoggerManager.getLogger(ComputerPlayer.class);

	// ratio minimum de construction d'avion
	public final static int NB_BOMBARDMENT_ALLOWED_SAME_COORD = 3;

	private final HashMap<TransportShip, Destroyer> _destroyerProtectionMap = new HashMap<TransportShip, Destroyer>();

	// TODO AZ Supprimer la hashMap pour les Artillery et ajouter une propriete
	// isProtected comme pour les tanks
	private final HashMap<City, Integer> _nbProtectedRequiredMap = new HashMap<City, Integer>();
	private final HashMap<City, ComputerTank> _cityTankTargetMap = new HashMap<City, ComputerTank>();
	private final HashMap<Area, Coord> _loadingCoordMap = new HashMap<Area, Coord>();
	private final HashMap<Area, Area> _unloadingAreaMap = new HashMap<Area, Area>();
	private final HashMap<Area, AreaStatusCst> _areaStatusMap = new HashMap<Area, AreaStatusCst>();

	private short _globalStrategy = -1;

	private final short GROUND_ONLY_STRATEGY = 1;
	private final short MINIMUM_SEA_STRATEGY = 2;
	private final short GROUND_AND_SEA_STRATEGY = 3;
	private final CoordProductionCostMap _coordCostMap = new CoordProductionCostMap();

	/**
	 * Constructor.
	 * 
	 * @param p_playerId
	 *            Identifiant du joueur.
	 * @param p_startPosition
	 *            Position de d�part du joueur.
	 * @param p_mapMgr
	 *            map manager.
	 * @param p_level
	 *            Niveau d'intelligence du joueur.
	 */
	public ComputerPlayer(	final PlayerId p_playerId,
							final Coord p_startPosition,
							final FSMapMgr p_mapMgr,
							final PlayerLevelCst p_level,
							final UnitIndex p_unitIndex,
							final ScenarioMapConditionList p_scenarioMapConditionList,
							final ScenarioUnitList p_scenarioUnitList,
							final ScenarioViewAreaList p_scenarioEventList)
	{
		super(p_playerId, PlayerTypeCst.COMPUTER, p_startPosition, p_level, p_mapMgr, p_unitIndex,
				p_scenarioMapConditionList, p_scenarioUnitList, null, p_scenarioEventList);
	}

	/**
	 * At the end of the turn, a computer player select which unit to produce in empty city.
	 */
	@Override
	public final AbstractUnitList finishTurn()
	{
		_logger.playerAction("Check for city without assigned production unit");
		final AreaList groundArea = _mapMgr.getAreaSet().getAllGroundAreaSetWithCity();

		if (_globalStrategy == -1)
		{
			// Identification of the high level strategy to be applied by the
			// computer (What kind of unit to build and with which ratio)

			if (groundArea.size() == 0)
			{
				_logger.playerSubActionDetail("Global strategy identified: Only Ground Army required (no boat)");
				_globalStrategy = GROUND_ONLY_STRATEGY;
			} else
			{
				short seaPercentage = _mapMgr.getPercentageOfSea();
				_logger.playerSubActionDetail("Sea Percentage: " + seaPercentage + "%");
				if (seaPercentage < 40 || groundArea.size() <= 3)
				{
					_logger.playerSubActionDetail("Global strategy identified: Minimum sea Army required (no battleship)");
					_globalStrategy = MINIMUM_SEA_STRATEGY;
				} else
				{
					_logger.playerSubActionDetail("Global strategy identified: Ground and Sea Army required");
					_globalStrategy = GROUND_AND_SEA_STRATEGY;
				}
			}
		}

		for (Area currentArea : groundArea)
		{
			updateCityProduction(currentArea);
		}

		_cityTankTargetMap.clear();

		return super.finishTurn();
	}

	public Coord getBestCoordForBombardment(final Coord p_coord, final int p_range)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		Assert.precondition(p_range >= 1, "Invalid range");

		return _coordCostMap.getBestCoordForBombardment(p_coord, p_range);
	}

	@Override
	public void addBombardment(final Coord p_coord, final Bombardment p_bombardment)
	{
		super.addBombardment(p_coord, p_bombardment);

		_coordCostMap.decHitPoints(p_coord, p_bombardment.getAverageDamage());
	}

	private final void updateCityProduction(final Area p_area)
	{
		Assert.preconditionNotNull(p_area, "Area");

		final CityList areaCityList = p_area.getCityList();
		final CityList cityPlayerList = areaCityList.getCityByPlayer(this);
		final CityList cityWithoutProdSet = cityPlayerList.getCityWithoutProduction();

		if (cityWithoutProdSet.size() == 0)
		{
			_logger.playerSubAction("No city production review required: " + p_area.toString());
			return;
		}
		final AreaStatusCst status = getAreaStatus(p_area);
		_logger.playerSubActionDetail("Area status: " + status);

		final CoordSet areaCoordList = p_area.getCellSet().getCoordSet();

		// Nb Tank assigned to the current Area
		int nbAvailableAreaTank = getUnitList().getTankProtectingArea(p_area).size();
		int nbAvailableAreaArtillery = getUnitList()
				.getUnitByTypeAndCoordSet(UnitTypeCst.ARTILLERY, p_area.getCellSet().getCoordSet()).size();
		int nbAvailableAreaTransport = getUnitList().getTransportAssignedArea(p_area).size();
		int nbAvailableAreaFighter = getUnitList().getUnitByType(UnitTypeCst.FIGHTER).getUnitByCoordList(areaCoordList)
				.size();

		UnitTypeCst productionType;
		for (City currentCity : cityPlayerList)
		{
			if (currentCity.isUnitUnderConstruction())
			{
				productionType = currentCity.getProductionType();
				if (UnitTypeCst.TANK.equals(productionType))
				{
					++nbAvailableAreaTank;
				} else if (UnitTypeCst.FIGHTER.equals(productionType))
				{
					++nbAvailableAreaFighter;
				} else if (UnitTypeCst.TRANSPORT_SHIP.equals(productionType))
				{
					++nbAvailableAreaTransport;

				} else if (UnitTypeCst.ARTILLERY.equals(productionType))
				{
					++nbAvailableAreaArtillery;
				}
			}
		}

		int nbAvailableWorldTransport = getUnitList().getUnitByType(UnitTypeCst.TRANSPORT_SHIP).size();
		nbAvailableWorldTransport += getCitySet().getCityConstructingUnitType(UnitTypeCst.TRANSPORT_SHIP).size();

		int nbAvailableWorldDestroyer = getUnitList().getUnitByType(UnitTypeCst.DESTROYER).size();
		nbAvailableWorldDestroyer += getCitySet().getCityConstructingUnitType(UnitTypeCst.DESTROYER).size();

		int nbAvailableWorldBattleship = getUnitList().getUnitByType(UnitTypeCst.BATTLESHIP).size();
		nbAvailableWorldBattleship += getCitySet().getCityConstructingUnitType(UnitTypeCst.BATTLESHIP).size();
		
		_logger.playerSubAction("Ajusting city's production Area: " + p_area);
		_logger.playerSubActionDetail("Nb unit (Area) =        Tank: " + nbAvailableAreaTank);
		_logger.playerSubActionDetail("Nb unit (Area) =   Artillery: " + nbAvailableAreaArtillery);
		_logger.playerSubActionDetail("Nb unit (Area) =     Fighter: " + nbAvailableAreaFighter);
		_logger.playerSubActionDetail("Nb unit (Area) =   Transport: " + nbAvailableAreaTransport);
		_logger.playerSubActionDetail("Nb unit (World) =  Transport: " + nbAvailableWorldTransport);
		_logger.playerSubActionDetail("Nb unit (Wordl) =  Destroyer: " + nbAvailableWorldDestroyer);
		_logger.playerSubActionDetail("Nb unit (Wordl) = Battleship: " + nbAvailableWorldBattleship);

		final AbstractUnitList areaEnemyList = getEnemySet().getUnitByCoordList(areaCoordList);
		final CityList cityToConquerList = areaCityList.getCityToConquer(this);
		final CityList enemyCityList = areaCityList.getCityWithoutOwner();
		_logger.playerSubActionDetail("Nb enemy unit detected (area) : " + areaEnemyList.size());
		_logger.playerSubActionDetail("Nb enemy city          (area) : " + enemyCityList.size());
		_logger.playerSubActionDetail("Nb city without owner  (area) : "
				+ (cityToConquerList.size() - enemyCityList.size()));

		int nbEnemyTankCloseToCity;
		int nbEnemyNextToCity;
		int nbCombatUnitNextToCity;
		int nbTankNextToCity, nbArtilleryNextToCity, nbFighterNextToCity;
		UnitTypeCst unitType;

		for (City currentCity : cityWithoutProdSet)
		{
			_logger.playerSubAction("City production review : " + currentCity);
			nbEnemyTankCloseToCity = areaEnemyList.getUnitInRange(currentCity.getPosition(), 10)
					.getUnitByType(UnitTypeCst.TANK).size();
			nbEnemyNextToCity = areaEnemyList.getUnitInRange(currentCity.getPosition(), 20).size();
			_logger.playerSubActionDetail("Nb Enemy next to city        (r:10): " + nbEnemyTankCloseToCity);
			_logger.playerSubActionDetail("Nb Enemy next to city        (r:20): " + nbEnemyNextToCity);

			// When the enemy is very close (r:10) to the city or if some city have to be conquered
			if (nbEnemyTankCloseToCity > 0 || nbAvailableAreaTank <= cityToConquerList.size() )
			{
				nbAvailableAreaTank++;
				currentCity.setProductionType(UnitTypeCst.TANK);
				_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());
				continue;
			}

			// When the enemy is close (r:20) and the number of friendly
			// combat unit is not enough
			nbCombatUnitNextToCity = _unitList.getUnitInRange(currentCity.getPosition(), 20).getGroundCombatUnitList()
					.size();
			nbCombatUnitNextToCity += cityPlayerList.getCityConstructingUnitType(UnitTypeCst.GROUND_COMBAT_UNIT_LIST)
					.size();
			_logger.playerSubActionDetail("Nb Combat Unit next to city  (r:20): " + nbCombatUnitNextToCity);
			if (nbEnemyNextToCity > nbCombatUnitNextToCity)
			{
				nbAvailableAreaTank++;
				currentCity.setProductionType(UnitTypeCst.TANK);
				_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());
				continue;
			}

			// When the enemy is close (r:20) and the number of friendly
			// combat unit is enough. So we have the choice between : tank,
			// fighter or artillery
			if (nbEnemyNextToCity > 0)
			{
				nbArtilleryNextToCity = cityPlayerList.getCityInRange(currentCity.getPosition(), 20)
						.getCityConstructingUnitType(UnitTypeCst.ARTILLERY).size();
				nbArtilleryNextToCity += _unitList.getUnitInRange(currentCity.getPosition(), 20)
						.getUnitByType(UnitTypeCst.ARTILLERY).size();
				_logger.playerSubActionDetail("Nb Artillery almost available next to city (r:20): "
						+ nbArtilleryNextToCity);

				if (nbArtilleryNextToCity < 1 || nbArtilleryNextToCity / (float) nbCombatUnitNextToCity < 0.1f)
				{
					nbAvailableAreaArtillery++;
					currentCity.setProductionType(UnitTypeCst.ARTILLERY);
					_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());
					continue;
				}

				nbTankNextToCity = cityPlayerList.getCityConstructingUnitType(UnitTypeCst.TANK).size();
				nbTankNextToCity += _unitList.getUnitInRange(currentCity.getPosition(), 20)
						.getUnitByType(UnitTypeCst.TANK).size();
				_logger.playerSubActionDetail("Nb Tank almost available next to city (r:20): " + nbTankNextToCity);

				if (nbTankNextToCity / (float) nbCombatUnitNextToCity < 0.5f)
				{
					nbAvailableAreaTank++;
					currentCity.setProductionType(UnitTypeCst.TANK);
					_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());
					continue;
				}

				nbFighterNextToCity = cityPlayerList.getCityConstructingUnitType(UnitTypeCst.TANK).size();
				nbFighterNextToCity += _unitList.getUnitInRange(currentCity.getPosition(), 20)
						.getUnitByType(UnitTypeCst.FIGHTER).size();
				_logger.playerSubActionDetail("Nb Fighter almost available next to city (r:20): "
						+ nbFighterNextToCity);

				nbAvailableAreaFighter++;
				currentCity.setProductionType(UnitTypeCst.FIGHTER);
				_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());
				continue;
			}

			// When the enemy is still in the area but far away from the city
			if (enemyCityList.size() > 0 || areaEnemyList.size() > 0)
			{
				unitType = setRandomUnitTypeToConstruct(currentCity, 0.20f, 0.10f, 0.7f, 0f, 0f, 0f);
				_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());

				if (unitType.equals(UnitTypeCst.TANK))
				{
					nbAvailableAreaTank++;

				} else if (unitType.equals(UnitTypeCst.FIGHTER))
				{
					nbAvailableAreaFighter++;
				} else
				{
					nbAvailableAreaArtillery++;
				}

				continue;
			}

			// When no enemy in the area but some free city still exist and not
			// enough tank to continue the campaign.
			if (cityToConquerList.size() > nbAvailableAreaTank / 3)
			{
				nbAvailableAreaTank++;
				currentCity.setProductionType(UnitTypeCst.TANK);
				_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());
			}

			if (_globalStrategy == GROUND_ONLY_STRATEGY)
			{
				unitType = setRandomUnitTypeToConstruct(currentCity, 0.25f, 0.05f, 0.7f, 0f, 0f, 0f);
				_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());

				if (unitType.equals(UnitTypeCst.TANK))
				{
					nbAvailableAreaTank++;

				} else if (unitType.equals(UnitTypeCst.FIGHTER))
				{
					nbAvailableAreaFighter++;
				} else
				{
					nbAvailableAreaArtillery++;
				}

				continue;
			}

			Assert.check(	_globalStrategy == MINIMUM_SEA_STRATEGY || _globalStrategy == GROUND_AND_SEA_STRATEGY,
							"Unexpected global strategy");

			if (nbAvailableAreaTank < Math.max(3, cityPlayerList.size() / 3))
			{
				nbAvailableAreaTank++;
				currentCity.setProductionType(UnitTypeCst.TANK);
				_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());
				continue;
			}

			if (nbAvailableAreaArtillery < Math.max(1, cityPlayerList.size() / 15))
			{
				nbAvailableAreaArtillery++;
				currentCity.setProductionType(UnitTypeCst.ARTILLERY);
				_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());
				continue;
			}

			if (!currentCity.isPortCity())
			{

				unitType = setRandomUnitTypeToConstruct(currentCity, 0.25f, 0f, 0.75f, 0f, 0f, 0f);
				_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());

				if (unitType.equals(UnitTypeCst.TANK))
				{
					nbAvailableAreaTank++;

				} else if (unitType.equals(UnitTypeCst.FIGHTER))
				{
					nbAvailableAreaFighter++;
				} else
				{
					throw new FSFatalException("Unexpected Unit Type randomly selected:" + unitType);
				}

				continue;
			}

			Assert.check(currentCity.isPortCity(), "Unexpected result found");

			if (nbAvailableAreaTransport == 0)
			{
				nbAvailableAreaTransport++;
				nbAvailableWorldTransport++;
				currentCity.setProductionType(UnitTypeCst.TRANSPORT_SHIP);
				_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());
				continue;
			}

			if (nbAvailableWorldDestroyer < 2 * nbAvailableWorldTransport)
			{
				nbAvailableWorldDestroyer++;
				currentCity.setProductionType(UnitTypeCst.DESTROYER);
				_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());
				continue;
			}

			if (_globalStrategy == MINIMUM_SEA_STRATEGY)
			{
				float transportRatio;
				float battleshipRatio;
				if (_mapMgr.getAreaSet().getAllGroundAreaSetWithCity().size() == 1) {
					transportRatio = 0f;
					battleshipRatio = 0.05f;
				} else
				{
					transportRatio = 0.05f;
					battleshipRatio = 0f;
				}
				unitType = setRandomUnitTypeToConstruct(currentCity, 0.25f, 0f, 0.5f, transportRatio, 0.2f, battleshipRatio);
			} else if (_globalStrategy == GROUND_AND_SEA_STRATEGY)
			{
				float transportRatio;
				float battleshipRatio;
				if (_mapMgr.getAreaSet().getAllGroundAreaSetWithCity().size() == 1) {
					transportRatio = 0f;
					battleshipRatio = 0.1f;
				} else
				{
					transportRatio = 0.05f;
					battleshipRatio = 0.05f;
				}
				unitType = setRandomUnitTypeToConstruct(currentCity, 0.225f, 0f, 0.5f, transportRatio, 0.175f, battleshipRatio);
			} else
			{
				throw new FSFatalException("Unexpected global strategy found : " + _globalStrategy);
			}

			_logger.playerSubActionDetail("Set city's production to " + currentCity.getProductionType());
		}
	}

	private UnitTypeCst setRandomUnitTypeToConstruct(	final City p_city,
														float p_tank,
														final float p_artillery,
														float p_fighter,
														float p_transport,
														float p_destroyer,
														float p_battleship)
	{
		Assert.precondition(p_tank + p_artillery + p_fighter + p_transport + p_destroyer + p_battleship <= 1f,
							"Invalid random request");

		float randomNumber = _randomizer.getRandomFloat();
		Assert.check(randomNumber > 0f && randomNumber <= 1f, "Unexpected random number generated");

		float currentRatio = p_tank;
		if (randomNumber < currentRatio)
		{
			p_city.setProductionType(UnitTypeCst.TANK);
			return p_city.getProductionType();

		}

		currentRatio += p_artillery;
		if (randomNumber < currentRatio)
		{
			p_city.setProductionType(UnitTypeCst.ARTILLERY);
			return p_city.getProductionType();
		}

		currentRatio += p_fighter;
		if (randomNumber < currentRatio)
		{
			p_city.setProductionType(UnitTypeCst.FIGHTER);
			return p_city.getProductionType();
		}

		currentRatio += p_transport;
		if (randomNumber < currentRatio)
		{
			p_city.setProductionType(UnitTypeCst.TRANSPORT_SHIP);
			return p_city.getProductionType();
		}

		currentRatio += p_destroyer;
		if (randomNumber < currentRatio)
		{
			p_city.setProductionType(UnitTypeCst.DESTROYER);
			return p_city.getProductionType();
		}

		currentRatio += p_battleship;
		if (randomNumber < currentRatio)
		{
			p_city.setProductionType(UnitTypeCst.BATTLESHIP);
			return p_city.getProductionType();
		}

		throw new FSFatalException("Unexpected random number generated");

	}

	@Override
	protected AbstractUnit retrieveNewUnitFromCity(final City p_city)
	{
		final AbstractUnit unit = super.retrieveNewUnitFromCity(p_city);
		final AbstractUnitDirective dir;
		if (unit instanceof ComputerTank)
		{
			dir = new TankAutoDirective((ComputerTank) unit, p_city);
		} else if (unit instanceof Fighter)
		{
			dir = new FighterAutoDirective((Fighter) unit);
		} else if (unit instanceof ComputerTransportShip)
		{
			final Area area = p_city.getGroundArea();
			dir = new TransportShipAutoDirective((ComputerTransportShip) unit, area);
		} else if (unit instanceof Destroyer)
		{
			dir = new DestroyerAutoDirective((Destroyer) unit);
		} else if (unit instanceof Artillery)
		{
			dir = new ArtilleryAutoDirective((Artillery) unit);
		} else if (unit instanceof Battleship)
		{
			dir = new BattleshipAutoDirective((Battleship) unit);
		} else
		{
			throw new FSFatalException("Unsupported Abstract Unit instance: " + unit.getClass().getName());
		}

		unit.addDirective(dir);
		return unit;
	}

	/**
	 * starts the turn of the player within initializing the enemy set, the unit
	 * move, fuel & hits point properties.
	 */
	@Override
	public void startTurn(final int p_turn)
	{
		super.startTurn(p_turn);

		final CityList cityList = super.getCitySet();
		int nbRequestedTank;
		AbstractUnitList tankList;
		ComputerTank tank;
		for (City city : cityList)
		{
			nbRequestedTank = updateNbProtectorRequiredForCity(city);
			tankList = getUnitList().getTankProtectingCity(city);
			while (tankList.size() > nbRequestedTank)
			{

				tank = (ComputerTank) tankList.get(0);
				tank.removeProtectedCity();
			}
		}

		_logger.playerAction("Area statistics refresh");
		final AreaList groundAreaSet = _mapMgr.getAreaSet().getAllGroundAreaSetWithCity();
		Area currentArea = null;
		final Iterator areaIter = groundAreaSet.iterator();
		_cityTankTargetMap.clear();
		AreaStatusCst areaStatus;
		while (areaIter.hasNext())
		{
			currentArea = (Area) areaIter.next();
			areaStatus = refreshAreaInformation(currentArea);

			if (areaStatus.equals(AreaStatusCst.TO_CONQUER_AREA) || areaStatus.equals(AreaStatusCst.UNDER_CONTROL_AREA))
			{
				selectBestCityToAttackByArea(currentArea);
			}

		}
	}

	@Override
	public void addCity(final City p_city, final boolean p_withEventPropagation)
	{
		_cityTankTargetMap.remove(p_city);
		super.addCity(p_city, p_withEventPropagation);
		refreshAreaInformation(p_city.getGroundArea());
	}

	/**
	 * Play the computer
	 * 
	 * @return True (meaning that the turn can be finished automatically).
	 */
	@Override
	public boolean play()
	{
		_logger.playerAction("Player {0} plays its turn", this);

		AbstractUnitList unitList;
		try
		{
			unitList = getConqueringTanks();
		} catch (PathNotFoundException ex)
		{
			throw new FSFatalException(ex);
		}
		_logger.playerAction("Player {0} plays Conquering TANKs (phase #1)", this);
		if (!playUnitList(unitList))
		{
			return false;
		}

		unitList = getUnitList().getUnitNotMarkedAsFinished();
		_logger.playerAction("Player {0} plays non-used TANKs (phase #2)", this);
		if (!playUnitList(unitList.getUnitByType(UnitTypeCst.TANK)))
		{
			return false;
		}

		unitList = unitList.getUnitNotMarkedAsFinished();
		final AbstractUnitList transportList = unitList.getUnitByType(UnitTypeCst.TRANSPORT_SHIP);
		final AbstractUnitList artilleryList = unitList.getUnitByType(UnitTypeCst.ARTILLERY);
		unitList.removeAll(transportList);
		unitList.removeAll(artilleryList);

		_logger.playerAction("Player {0} plays other UNITs except Transport Ship & Artillery", this);
		if (!playUnitList(unitList))
		{
			return false;
		}

		_logger.playerAction("Player {0} plays other TRANSPORT SHIPs", this);
		if (!playUnitList(transportList))
		{
			return false;
		}

		_logger.playerAction("Player {0} plays other ARTILLERYs", this);
		refreshBombardmentCoord();
		if (!playUnitList(artilleryList))
		{
			return false;
		}
		return true;
	}

	private void selectBestCityToAttackByArea(final Area p_area)
	{
		final CityList cityList = p_area.getCityList().getCityToConquer(this);
		final AbstractUnitList unitList = getUnitList().getTankProtectingArea(p_area);

		for (AbstractUnit currentTank : unitList)
		{
			((ComputerTank) currentTank).refreshTargetCityList(cityList);
			setBestCityToAttackForATank((ComputerTank) currentTank);
		}

		for (City currentCity : cityList)
		{
			_logger.playerSubActionDetail("City " + currentCity.getPosition() + " <- "
					+ _cityTankTargetMap.get(currentCity));
		}

	}

	private void setBestCityToAttackForATank(final ComputerTank p_tank)
	{
		TCD currentTCD = p_tank.getTargetCity();
		City city = currentTCD.getCity();
		if (!_cityTankTargetMap.containsKey(city))
		{
			_logger.playerSubActionDetail("Target found: " + currentTCD.toString());
			_cityTankTargetMap.put(city, p_tank);
			return;
		}

		final ComputerTank alreadyAssignedTank = _cityTankTargetMap.get(city);
		final TCD alreadAssignedtcd = alreadyAssignedTank.getTargetCity();
		if (currentTCD.getDisctance() >= alreadAssignedtcd.getDisctance())
		{
			if (p_tank.setNextTargetCity())
			{
				setBestCityToAttackForATank(p_tank);
				return;
			}
			_logger.playerSubActionDetail("No specific target city found");
			return;
		}

		_logger.playerSubActionDetail("Better target found: " + currentTCD.toString());
		_cityTankTargetMap.put(city, p_tank);

		if (alreadyAssignedTank.setNextTargetCity())
		{
			_logger.playerSubActionDetail("Replacement of Tank(" + alreadyAssignedTank.getId() + "):("
					+ alreadyAssignedTank.getPosition().getX() + "," + alreadyAssignedTank.getPosition().getY() + ")");
			setBestCityToAttackForATank(alreadyAssignedTank);
			return;
		}

		_logger.playerSubAction("Already assigned tank replaced and no more specific target city found");
	}

	public final AreaList getOwnedArea()
	{
		AreaList areaList = new AreaList();
		for (Area area : _areaStatusMap.keySet())
		{
			if (AreaStatusCst.OWNED_AREA.equals(_areaStatusMap.get(area)))
			{
				areaList.add(area);
			}
		}

		return areaList;
	}

	public final AreaList getAreaToConquer()
	{
		AreaList areaList = new AreaList();
		for (Area area : _areaStatusMap.keySet())
		{
			if (AreaStatusCst.TO_CONQUER_AREA.equals(_areaStatusMap.get(area))
					|| AreaStatusCst.UNDER_CONTROL_AREA.equals(_areaStatusMap.get(area)))
			{
				areaList.add(area);
			}
		}

		return areaList;
	}

	private AbstractUnitList getConqueringTanks() throws PathNotFoundException
	{
		final FSMapMgr mapMgr = super.getMapMgr();
		_logger.playerAction("Start Playing Tank under Area");

		AreaStatusCst areaStatus;
		AbstractUnitList tankList = new AbstractUnitList();

		if (getUnitList().isEmpty())
		{
			return tankList;
		}

		// Initialization of the target city for all available tank by Area
		for (Area currentArea : mapMgr.getAreaSet().getAllGroundAreaSetWithCity())
		{
			areaStatus = getAreaStatus(currentArea);
			if (!AreaStatusCst.TO_CONQUER_AREA.equals(areaStatus)
					&& !AreaStatusCst.UNDER_CONTROL_AREA.equals(areaStatus))
			{
				continue;
			}

			tankList.addAll(getUnitList().getTankProtectingArea(currentArea));
		}

		return tankList;
	}

	private boolean playUnitList(AbstractUnitList p_unitList)
	{
		final IFirstStrikeEvent gameListener = super.getGameListener();
		MoveStatusCst moveStatus = null;
		for (AbstractUnit currentUnit : p_unitList)
		{
			_logger.playerSubAction("Play unit: ", currentUnit);
			moveStatus = currentUnit.playDirectives();
			if (MoveStatusCst.UNIT_DEFEATED.equals(moveStatus) || MoveStatusCst.NOT_ENOUGH_FUEL_PTS.equals(moveStatus))
			{
				if (!isLiving())
				{
					return false;
				}
				gameListener.eventUnitKilled(currentUnit);
			} else
			{
				currentUnit.markAsTurnCompleted();
				gameListener.eventUnitFinished(currentUnit);
			}
			Assert.check(!MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus), "Invalid Directive Result");
		}

		return true;
	}

	public final int updateNbProtectorRequiredForCity(final City p_city)
	{
		Assert.preconditionNotNull(p_city, "City");
		Assert.precondition(p_city.getPlayer() != null, "Invalid City");
		Assert.precondition(this == p_city.getPlayer(), "Invalid City Player");

		final PlayerLevelCst level = getPlayerLevel();
		final int nbProtector;
		if (PlayerLevelCst.EASY_LEVEL.equals(level))
		{
			nbProtector = IConfiguration.NB_TANK_DEFEND_CITY_EASY_LEVEL;
		}

		else if (PlayerLevelCst.NORMAL_LEVEL.equals(level) || PlayerLevelCst.HARD_LEVEL.equals(level))
		{
			final CityList enemyCityList = p_city.getGroundArea().getCityList().getEnemyCity(this);
			final CoordSet enemyCoordSet = enemyCityList.getCoordSet()
					.getCoordInRange(p_city.getPosition(), IConfiguration.CITY_SECURITY_RANGE);
			nbProtector = enemyCoordSet.size();
		} else
		{
			throw new IllegalArgumentException("Unsupported Player Level");
		}

		_nbProtectedRequiredMap.put(p_city, new Integer(nbProtector));
		return nbProtector;
	}

	public boolean isTransportProtected(final TransportShip p_transportShip)
	{
		return _destroyerProtectionMap.containsKey(p_transportShip);
	}

	public HashMap<TransportShip, Destroyer> getDestroyerProtectionMap()
	{
		return _destroyerProtectionMap;
	}

	public AbstractUnitList getTransportWithoutProtection()
	{
		final AbstractUnitList transportList = getUnitList().getUnitByType(UnitTypeCst.TRANSPORT_SHIP);
		if (transportList.isEmpty())
		{
			return transportList;
		}

		final AbstractUnitList transportWithoutProtectionList = new AbstractUnitList();
		for (AbstractUnit currentTransport : transportList)
		{
			if (!_destroyerProtectionMap.containsKey(currentTransport))
			{
				transportWithoutProtectionList.add(currentTransport);
			}
		}

		return transportWithoutProtectionList;
	}

	public final int getNbProtectorRequired(final City p_city)
	{
		Assert.preconditionNotNull(p_city, "City");
		Assert.precondition(p_city.getPlayer() == this, "Invalid City Owner");

		final Integer nbProtectorRequired = _nbProtectedRequiredMap.get(p_city);
		if (nbProtectorRequired == null)
		{
			return updateNbProtectorRequiredForCity(p_city);
		}
		return nbProtectorRequired.intValue();
	}

	public final Area getUnloadingArea(final Area p_area)
	{
		Assert.preconditionNotNull(p_area, "Area");

		Area unloadingArea = _unloadingAreaMap.get(p_area);
		if (unloadingArea == null)
		{
			caclulateBestAreaToConquer(p_area);
		}

		return unloadingArea;
	}

	public final AreaStatusCst getAreaStatus(final Area p_area)
	{
		Assert.preconditionNotNull(p_area, "Area");
		AreaStatusCst areaStatus = _areaStatusMap.get(p_area);
		Assert.check(areaStatus != null, "Area status not up to date");

		return areaStatus;
	}

	public final AreaStatusCst refreshAreaInformation(final Area p_area)
	{
		AreaStatusCst areaStatus = refreshAreaStatus(p_area);
		caclulateBestAreaToConquer(p_area);
		_logger.playerSubAction("Area: " + p_area.getFirstCoordOfArea() + ", status:" + getAreaStatus(p_area)
				+ ", Loading coord:" + getLoadingCoordOf(p_area));

		return areaStatus;
	}

	/**
	 * Calculates the status of the area for the current player.
	 * 
	 * @param p_player
	 *            Current player.
	 * @return Area Status.
	 */
	private final AreaStatusCst refreshAreaStatus(final Area p_area)
	{
		Assert.preconditionNotNull(p_area, "Area");
		final CityList cityList = p_area.getCityList();
		final CityList ownedCityList = cityList.getCityByPlayer(this);
		final CityList enemyCityList = (CityList) cityList.getEnemyCity(this);

		final int cityListSize = cityList.size();
		final int ownedCityListSize = ownedCityList.size();
		final int enemyCityListSize = enemyCityList.size();

		AreaStatusCst status = null;
		if (ownedCityListSize == cityListSize)
		{
			status = AreaStatusCst.OWNED_AREA;
			_areaStatusMap.put(p_area, status);
			return status;
		}

		final float ownedRate = ownedCityListSize / (float) cityListSize;
		if (enemyCityListSize > 0)
		{
			status = AreaStatusCst.TO_CONQUER_AREA;
			_areaStatusMap.put(p_area, status);
			return status;
		}

		if (ownedRate > 0.499f)
		{
			status = AreaStatusCst.UNDER_CONTROL_AREA;
			_areaStatusMap.put(p_area, status);

			return status;

		}
		status = AreaStatusCst.TO_CONQUER_AREA;
		_areaStatusMap.put(p_area, status);
		return status;
	}

	/**
	 * Define the best area to conquer from both identified area & player.
	 * 
	 * @param p_fromArea
	 *            Area finding the best enemy area.
	 * @param p_cache
	 *            Player Cache.
	 */
	private final void caclulateBestAreaToConquer(final Area p_fromArea)
	{
		Assert.preconditionNotNull(p_fromArea, "Area");

		final AreaList areaSet = getAreaToConquer();
		if (areaSet.size() <= 1)
		{
			return;
		}
		final AreaList priorAreaList = areaSet.getFirstGroupElement(this);
		final CoordSet availableLoadingCoordSet = p_fromArea.getCityList().getPortList().getCoordSet();
		Area currentDestArea = null;
		Area bestDestArea = null;
		CoupleCoord currentCouple = null;
		CoupleCoord bestCouple = null;
		CoordSet availableUnloadingCoordSet = null;
		double shortestDistance = Double.MAX_VALUE;
		double currentDistance;

		// For each destination area
		final Iterator destAreaIter = priorAreaList.iterator();
		while (destAreaIter.hasNext())
		{
			// Retrieve the current area
			currentDestArea = (Area) destAreaIter.next();
			if (currentDestArea == p_fromArea)
			{
				continue;
			}

			// Calculate all available unloading coord

			availableUnloadingCoordSet = currentDestArea.getGroundCellNearToSea();

			// Find the shortness "path" between loading & unloading area coord.
			currentCouple = availableLoadingCoordSet.getDirectNearestCoord(availableUnloadingCoordSet);
			currentDistance = currentCouple.calculateDistance();

			// If the current destination area is nearest of the previons
			if (currentDistance < shortestDistance)
			{
				// keep in mind that is the nearest area to conquer at this time
				bestDestArea = currentDestArea;
				shortestDistance = currentDistance;
				bestCouple = currentCouple;
			}
		}

		_unloadingAreaMap.put(p_fromArea, bestDestArea);
		_loadingCoordMap.put(p_fromArea, bestCouple.getFirstCoord());
	}

	public final Coord getLoadingCoordOf(final Area p_area)
	{
		Assert.preconditionNotNull(p_area, "Area");
		final Coord loadingCoord = _loadingCoordMap.get(p_area);
		if (loadingCoord == null)
		{
			caclulateBestAreaToConquer(p_area);
			return _loadingCoordMap.get(p_area);
		}

		return loadingCoord;
	}

	@Override
	public String toString()
	{
		final StringBuffer sb = new StringBuffer("{COMPUTER_PLAYER=");
		sb.append("id:").append(_id);
		sb.append(", ").append(_startPosition);
		if (!_isDead)
		{
			sb.append(", unit:").append(getUnitList().size());
			sb.append(", enemy:").append(getEnemySet().size());
			sb.append(", city:").append(getMapMgr().getCityList().getCityByPlayer(this).size());
		}
		sb.append("}");

		return sb.toString();
	}

	@Override
	public final void killUnit(final AbstractUnit p_unit)
	{
		super.killUnit(p_unit);
		if (p_unit instanceof TransportShip)
		{
			final TransportShip transport = (TransportShip) p_unit;
			final Destroyer protector = _destroyerProtectionMap.get(transport);
			if (null != protector)
			{
				_logger.playerSubActionDetail("Destroyed {0} doesn't protect any more this Transport Ship");
				protector.removeProtectedTransport();
			}
		} else if (p_unit instanceof Destroyer)
		{
			final Destroyer destroyer = (Destroyer) p_unit;
			for (TransportShip iter : _destroyerProtectionMap.keySet())
			{
				if (_destroyerProtectionMap.get(iter) == destroyer)
				{
					_destroyerProtectionMap.remove(iter);
					_logger.unitDetail(	"This destroyer '{0}' at {1} do not protected any more the Transport Ship '{2}'.",
										new Object[] { destroyer.getId(), destroyer.getPosition(), iter.getId() });
					break;
				}
			}
		}
	}

	/**
	 * Identify, for each cell (coord) containing an enemy unit, the total
	 * production cost. This coord with the high score will be bombarded.
	 */
	public void refreshBombardmentCoord()
	{
		_coordCostMap.clear();

		final FSMapMgr mapMgr = getMapMgr();
		final FSMap map = mapMgr.getMap();
		final CityList enemyCityList = mapMgr.getCityList().getEnemyCity(this);
		final AbstractUnitList enemyList = new AbstractUnitList();
		enemyList.addAll(getEnemySet());
		final CoordSet emptyCoordSet = new CoordSet();

		// Add enemy stay in city even if not visible by the computer (oups...)
		for (City city : getMapMgr().getCityList().getEnemyCity(this))
		{
			for (AbstractUnit enemy : _unitIndex.getNonNullUnitListAt(city.getPosition()))
			{
				if (!enemyList.contains(enemy))
				{
					enemyList.add(enemy);
				}
			}

			// Add the city itself
			if (city.isUnitUnderConstruction())
			{
				UnitTypeCst unitType = city.getProductionType();
				_coordCostMap.addCoordCost(	city.getPosition(),
											1,
											UnitCombatInfo.getInstance(unitType).getHitPointsCapacity());
			}
		}
		
		// Remove all cells contains computer's units or cities
		final CoordSet coordToExcludeSet = getUnitList().getCoordSet();
		coordToExcludeSet.addAll(getCitySet().getCoordSet());

		Coord enemyPosition;
		CoordSet rangedCoordSet = null;
		for (AbstractUnit enemy : enemyList)
		{
			if (enemy.hasPath())
			{
				enemyPosition = enemy.getPath().getEndTurnPosition();
				if (coordToExcludeSet.contains(enemyPosition))
				{
					continue;
				}
			} else
			{
				enemyPosition = enemy.getPosition();
			}

			if (enemy.getUnitType().equals(UnitTypeCst.FIGHTER))
			{
				if (enemyCityList.hasCityAtPosition(enemyPosition))
				{
					_coordCostMap.addCoordCost(	enemyPosition,
												City.getProductionTimeByType(enemy.getUnitType()),
												enemy.getHitPointsCapacity());
				}

				continue;
			}

			if (enemy.getUnitType().equals(UnitTypeCst.TRANSPORT_SHIP) && ((TransportShip) enemy).isContainingUnit())
			{
				_coordCostMap.addCoordCost(	enemyPosition,
											City.getProductionTimeByType(enemy.getUnitType()),
											enemy.getHitPointsCapacity());
				for (ILoadableUnit loadableUnit : ((TransportShip) enemy).getContainedUnitList())
				{
					_coordCostMap
							.addCoordCost(	enemyPosition,
											City.getProductionTimeByType(((AbstractUnit) loadableUnit).getUnitType()),
											0);
				}
			} else
			{
				_coordCostMap.addCoordCost(	enemyPosition,
											City.getProductionTimeByType(enemy.getUnitType()),
											enemy.getHitPointsCapacity());
			}

			// When an enemy has not path, we add every reachable cells around it as potentially relevant for a bombardment.
			// These cells should be bombarded only if all others positions have been properly bombarded.
			if (!enemy.hasPath() && enemy.getMove() != enemy.getMovementCapacity())
			{
				int randomBombardmentRange;
				if (UnitTypeCst.DESTROYER.equals(enemy.getUnitType()))
				{
					randomBombardmentRange = 4;
				} else if (UnitTypeCst.ARTILLERY.equals(enemy.getUnitType()))
				{
					randomBombardmentRange = 4;
				} else
				{
					randomBombardmentRange = 3;
				}

				rangedCoordSet = CoordSet.createInitiazedCoordSet(	enemyPosition,
																	randomBombardmentRange,
																	map.getWidth(),
																	map.getHeight());
				rangedCoordSet.remove(enemyPosition);
				rangedCoordSet.removeAll(coordToExcludeSet);
				for (Coord coord : rangedCoordSet)
				{
					if (mapMgr.isAccessibleCoord(enemy, coord, emptyCoordSet, false))
					{
						_coordCostMap.addCoordCost(	coord,
						                           	Math.max(_coordCostMap.getCost(coord), 1),
													5);
					}
				}
			}
		}
		

		for (Coord coord : coordToExcludeSet)
		{
			_coordCostMap.remove(coord);
		}
	}
}