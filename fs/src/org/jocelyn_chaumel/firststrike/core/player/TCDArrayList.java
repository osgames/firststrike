package org.jocelyn_chaumel.firststrike.core.player;

import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.core.city.City;

/**
 * Tank City Distance array list
 * @author Jocelyn
 *
 */
public class TCDArrayList extends ArrayList<TCD>
{
	/**
	 * Retrieve the TCD according to the city.
	 * 
	 * @param p_city
	 * @return
	 */
	public TCD getByCity(final City p_city)
	{
		for (TCD tcd : this)
		{
			if (tcd.getCity() == p_city)
			{
				return tcd;
			}
		}
		return null;
	}
}
