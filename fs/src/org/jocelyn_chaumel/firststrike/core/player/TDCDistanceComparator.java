package org.jocelyn_chaumel.firststrike.core.player;

import java.util.Comparator;

public class TDCDistanceComparator implements Comparator<TCD>
{

	@Override
	public int compare(TCD p_o1, TCD p_o2)
	{
		return Integer.compare(p_o1.getDisctance(), p_o2.getDisctance());
	}

}
