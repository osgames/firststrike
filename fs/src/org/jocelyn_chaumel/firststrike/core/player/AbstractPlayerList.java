/*
 * Created on Aug 24, 2005
 */
package org.jocelyn_chaumel.firststrike.core.player;

import java.util.ArrayList;

import org.jocelyn_chaumel.tools.data_type.FSUtil;

/**
 * @author Jocelyn Chaumel
 */
public class AbstractPlayerList extends ArrayList<AbstractPlayer>
{
	private final static String SHORT_NAME = "AbstractPlayerList";

	public AbstractPlayerList getLivingPlayer()
	{
		final AbstractPlayerList livingPlayerList = new AbstractPlayerList();

		for (AbstractPlayer currentPlayer : this)
		{
			if (currentPlayer.isLiving())
			{
				livingPlayerList.add(currentPlayer);
			}
		}
		return livingPlayerList;
	}

	// TODO AZ Ajouter la notion d'�quipe
	public AbstractPlayerList getEnemyPlayer(final AbstractPlayer p_player)
	{
		final AbstractPlayerList enemyPlayerList = new AbstractPlayerList();

		for (AbstractPlayer currentPlayer : this)
		{
			if (currentPlayer != p_player)
			{
				enemyPlayerList.add(currentPlayer);
			}
		}

		return enemyPlayerList;
	}

	@Override
	public final String toString()
	{
		return FSUtil.toStringList(SHORT_NAME, this);
	}
}
