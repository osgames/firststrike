/*
 * Player.java Created on 27 avril 2003, 21:18
 */

package org.jocelyn_chaumel.firststrike.core.player;

import org.jocelyn_chaumel.firststrike.core.IDetection;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioViewAreaList;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMapConditionList;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMessageList;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioUnitList;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioViewArea;
import org.jocelyn_chaumel.firststrike.core.player.cst.PlayerTypeCst;
import org.jocelyn_chaumel.firststrike.core.unit.UnitIndex;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe repr�sente un joueur. Elle contient donc toutes les villes et
 * les unit�s de celui-ci.
 */
public final class HumanPlayer extends AbstractPlayer
{
	private final CoordSet _shadowSet;
	private final CoordSet _noFogSet;
	private int _cityDetectionRange;
	private boolean _isDebugMode;

	public HumanPlayer(	final PlayerId p_playerId,
						final Coord p_startPosition,
						final FSMapMgr p_mapMgr,
						final PlayerLevelCst p_level,
						final UnitIndex p_unitIndex,
						final ScenarioMapConditionList p_scenarioMapConditionList,
						final ScenarioUnitList p_scenarioUnitList,
						final ScenarioMessageList p_scenarioMsgList,
						final ScenarioViewAreaList p_scenarioEventList)
	{
		super(p_playerId, PlayerTypeCst.HUMAN, p_startPosition, p_level, p_mapMgr, p_unitIndex,
				p_scenarioMapConditionList, p_scenarioUnitList, p_scenarioMsgList, p_scenarioEventList);

		final FSMap map = p_mapMgr.getMap();
		final int height = map.getHeight();
		final int width = map.getWidth();
		_shadowSet = CoordSet.createInitiazedCoordSet(new Coord(0, 0), Math.max(height, width), width, height);
		_noFogSet = new CoordSet();

		if (PlayerLevelCst.EASY_LEVEL.equals(p_level))
		{
			_cityDetectionRange = 3;
		} else if (PlayerLevelCst.NORMAL_LEVEL.equals(p_level))
		{
			_cityDetectionRange = 2;
		} else if (PlayerLevelCst.HARD_LEVEL.equals(p_level))
		{
			_cityDetectionRange = 1;
		} else
		{
			throw new IllegalStateException("Unsupported Player Level");
		}
	}

	public CoordSet getShadowSet()
	{
		/*
		 * if (_isDebugMode){ return new CoordSet(); }
		 */
		return _shadowSet;
	}

	public CoordSet getNoFogSet()
	{
		return _noFogSet;
	}

	public boolean isValidCoord(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		if (ApplicationConfigurationMgr.getInstance().isDebugMode())
		{
			return true;
		}

		return !_shadowSet.contains(p_coord);
	}

	/**
	 * Start the human turn. Clear the noFogSet, refresh unit move point,
	 * structure point & fuel if possible & the enemy unit list.
	 */
	@Override
	public void startTurn(final int p_turn)
	{
		_isDebugMode = ApplicationConfigurationMgr.getInstance().isDebugMode();
		_noFogSet.clear();
		super.startTurn(p_turn);
		if (_isDebugMode)
		{
			_enemySet.clear();
			_enemySet.addAll(_unitIndex.getEnemyList(this));
		}
	}

	/**
	 * Permit to launch an enemy detection process. This method return true if
	 * an enemy has been detected.
	 * 
	 * @param p_detector
	 *            A Detector (Unit or City)
	 * @return True or False.
	 */
	@Override
	public boolean refreshEnemyList(final IDetection p_detector)
	{
		boolean isDebugModeDisabled = !ApplicationConfigurationMgr.getInstance().isDebugMode();

		final Coord detectorCoord = p_detector.getPosition();
		final FSMap map = _mapMgr.getMap();

		final CoordSet coordScanList = CoordSet.createInitiazedCoordSet(detectorCoord,
																		p_detector.getDetectionRange(),
																		map.getWidth(),
																		map.getHeight());
		_noFogSet.addAll(coordScanList);
		_shadowSet.removeAll(coordScanList);

		return isDebugModeDisabled && super.refreshEnemyList(coordScanList);
	}

	@Override
	public boolean refreshEnemyList(final ScenarioViewArea p_viewArea)
	{
		Assert.check(!_isDead, "This player is dead");
		Assert.preconditionNotNull(p_viewArea, "View Area");

		final Coord startCoord = p_viewArea.getPosition();
		final FSMap map = _mapMgr.getMap();

		final CoordSet coordScanList = CoordSet.createInitiazedCoordSet(startCoord,
																		p_viewArea.getWidth(),
																		p_viewArea.getHeight(),
																		map.getWidth(),
																		map.getHeight());

		boolean isUnitDetected = false;
		if (p_viewArea.isUnitDetection()) 
		{
			isUnitDetected = super.refreshEnemyList(coordScanList);
			_noFogSet.addAll(coordScanList);
		}

		_shadowSet.removeAll(coordScanList);

		return isUnitDetected;
	}

	public final void setCityDetectionRange(final int p_detectionRange)
	{
		Assert.precondition(p_detectionRange > 0, "Invalid City Detection Range");

		_cityDetectionRange = p_detectionRange;
	}

	public final int getCityDetectionRange(final City p_city)
	{
		return _cityDetectionRange;
	}

	@Override
	public boolean play()
	{
		return false;
	}

	public final ScenarioMessageList consumeScenarioMessageForTurn(final int p_turnNumber)
	{
		final ScenarioMessageList currentScenarioTurnMsgList = _scenarioMessageList.removeMsgByTurn(p_turnNumber);
		_oldScenarioMessageList.addAll(currentScenarioTurnMsgList);
		return currentScenarioTurnMsgList;
	}

	@Override
	public String toString()
	{
		final StringBuffer sb = new StringBuffer("{HUMAN_PLAYER=");
		sb.append("id:").append(_id);
		sb.append(", ").append(_startPosition);
		if (!_isDead)
		{
			sb.append(", unit:").append(getUnitList().size());
			sb.append(", ennemy:").append(getEnemySet().size());
			sb.append(", city:").append(getMapMgr().getCityList().getCityByPlayer(this).size());
		}
		sb.append("}");

		return sb.toString();
	}
}