package org.jocelyn_chaumel.firststrike.core.player;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class EventLog implements Serializable
{
	private String _message;
	private Coord _coord;
	private UnitTypeCst _unitType;
	private PlayerId _playerId;
	private int _cellIdx;
	private CellTypeCst _cellType;

	private short _msgType = 0;
	private int _idMsg = 0;

	public int getIdMsg()
	{
		return _idMsg;
	}

	public void setIdMsg(int p_idMsg)
	{
		_idMsg = p_idMsg;
	}

	public static short EL_TYPE_UNIT_FINISHED = 2;
	public static short EL_TYPE_CITY_LOST = 3;
	public static short EL_TYPE_UNIT_LOST = 4;
	public static short EL_TYPE_UNIT_CREATION = 5;
	public static short EL_TYPE_ENEMY_BOMBARDMENT = 7;

	public EventLog setBombardment(final Coord p_bombardmentCoord, final CellTypeCst p_cellType, final int p_cellIdx)
	{
		Assert.precondition(_msgType == 0, "Message Type already set");
		_msgType = EL_TYPE_ENEMY_BOMBARDMENT;
		_coord = p_bombardmentCoord;
		_cellType = p_cellType;
		_cellIdx = p_cellIdx;
		_playerId = null;
		return this;
	}

	public EventLog setCityLost(final Coord p_cityCoord, final int p_cellIdx, final String p_cityName)
	{
		Assert.precondition(_msgType == 0, "Message Type already set");
		_coord = p_cityCoord;
		_message = p_cityName;
		_unitType = null;
		_msgType = EL_TYPE_CITY_LOST;
		_cellIdx = p_cellIdx;
		_playerId = null;
		return this;
	}
	
	public EventLog setUnitLost(final Coord p_unitCoord,
							final String p_unitName,
							final UnitTypeCst p_unitType, final PlayerId p_playerId)
	{
		_coord = p_unitCoord;
		_message = p_unitName;
		_unitType = p_unitType;
		_playerId = p_playerId;
		_msgType = EL_TYPE_UNIT_LOST;
		
		return this;
	}

	public EventLog setUnitCreated(	final Coord p_unitCoord,
								final String p_unitName,
								final UnitTypeCst p_unitType, final PlayerId p_playerId)
	{
		_coord = p_unitCoord;
		_message = p_unitName;
		_unitType = p_unitType;
		_playerId = p_playerId;
		_msgType = EL_TYPE_UNIT_CREATION;
		return this;
	}

	public String getMessage()
	{
		return _message;
	}

	public Coord getCoord()
	{
		return _coord;
	}
	
	public int getCellIdx()
	{
		return _cellIdx;
	}
	
	public CellTypeCst getCellType()
	{
		return _cellType;
	}

	public UnitTypeCst getUnitType()
	{
		return _unitType;
	}
	
	public final PlayerId getPlayerId()
	{
		return _playerId;
	}

	public short getMsgType()
	{
		return _msgType;
	}

	@Override
	public final String toString()
	{
		final StringBuilder sb = new StringBuilder();

		sb.append("{TURN_MSG = id:").append(_idMsg + " ");
		if (_msgType == EL_TYPE_UNIT_FINISHED)
		{
			sb.append("Computer Progression(" + _msgType + "), ");
			sb.append("Player:" + _message + ",");
		} else if (_msgType == EL_TYPE_UNIT_CREATION || _msgType == EL_TYPE_UNIT_LOST)
		{
			if (_msgType == EL_TYPE_UNIT_CREATION)
			{
				sb.append("Unit Created(" + _msgType + "), ");
			} else
			{
				sb.append("Unit lost(" + _msgType + "), ");
			}

			sb.append("unit:" + _message + "(" + _unitType.toString() + ")");
			sb.append("position:" + _coord);
		} else if (_msgType == EL_TYPE_CITY_LOST)
		{
			sb.append("City Lost(" + _msgType + "), ");
			sb.append("city:" + _message + ", ");
			sb.append("position:" + _coord);
		} else {
			sb.append(" Unknow message type : " + _msgType);
		}

		sb.append("}");

		return sb.toString();
	}

}
