package org.jocelyn_chaumel.firststrike.core.player;

import org.jocelyn_chaumel.tools.data_type.cst.IntCst;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;

public class PlayerLevelCst extends IntCst
{
	private final static String EASY = "easy";
	private final static String NORMAL = "normal";
	private final static String HARD = "hard";

	public final static PlayerLevelCst EASY_LEVEL = new PlayerLevelCst(1, "Easy Level");
	public final static PlayerLevelCst NORMAL_LEVEL = new PlayerLevelCst(5, "Normal Level");
	public final static PlayerLevelCst HARD_LEVEL = new PlayerLevelCst(10, "Hard Level");

	public final static PlayerLevelCst[] _playerLevelArray = { EASY_LEVEL, NORMAL_LEVEL, HARD_LEVEL };

	private PlayerLevelCst(final int p_value, final String p_label)
	{
		super(p_value, p_label);
	}

	/**
	 * Retourne l'instante d'une constante � partir d'une valeur.
	 * 
	 * @param p_value Valeur de la constante.
	 * @return Instance de la constante.
	 * @throws InvalidConstantValueException
	 */
	public static PlayerLevelCst getInstante(final int p_value) throws InvalidConstantValueException
	{
		return (PlayerLevelCst) IntCst.getInstance(p_value, _playerLevelArray, PlayerLevelCst.class);
	}

	public static PlayerLevelCst getInstance(final String p_value) throws InvalidConstantValueException
	{
		if (EASY.equals(p_value))
		{
			return getInstante(EASY_LEVEL.getValue());
		} else if (NORMAL.equals(p_value))
		{
			return getInstante(NORMAL_LEVEL.getValue());
		} else if (HARD.equals(p_value))
		{
			return getInstante(HARD_LEVEL.getValue());
		}
		throw new InvalidConstantValueException("PlayerLevelCst", p_value);
	}
}
