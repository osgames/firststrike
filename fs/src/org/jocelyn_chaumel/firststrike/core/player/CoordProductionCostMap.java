package org.jocelyn_chaumel.firststrike.core.player;

import java.util.HashMap;

import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.Tuple;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class CoordProductionCostMap extends HashMap<Coord, Tuple<Integer>>
{
	public void addCoordCost(final Coord p_coord, final int p_cost, final int p_hitPoints)
	{
		Assert.preconditionNotNull(p_coord, "Null Coord");
		Assert.precondition(p_cost > 0, "Invalid Cost");

		int newCost = p_cost;
		int newHitPoints = p_hitPoints;
		if (containsKey(p_coord))
		{
			Tuple<Integer> tuple = get(p_coord);
			newCost += tuple.getValue1().intValue();
			newHitPoints = Math.max(tuple.getValue2().intValue(), p_hitPoints);
		}

		put(p_coord, new Tuple<Integer>(newCost, newHitPoints));
	}
	
	public final int getCost(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "Null Ref");
		if (!containsKey(p_coord))
		{
			return 0;
		}

		return get(p_coord).getValue1().intValue();
	}

	public final void decHitPoints(final Coord p_coord, int p_damage)
	{
		Assert.preconditionNotNull(p_coord, "Null Ref");
		Assert.precondition(containsKey(p_coord), "Key not found :" + p_coord);
		
		final Tuple<Integer> tuple = get(p_coord);
		int hitPoints = tuple.getValue2() - p_damage;
		if (hitPoints < 0)
		{
			remove(p_coord);
		} else {
			tuple.setValue2(new Integer(hitPoints));
		}
	}

	public Coord getBestCoordForBombardment(final Coord p_coord, final int p_range)
	{
		Coord bestCoord = null;
		int currentCost, bestCost = -1;

		for (Coord currentCoord : super.keySet())
		{
			if (p_coord.calculateIntDistance(currentCoord) > p_range)
			{
				continue;
			}

			currentCost = getCost(currentCoord);
			if (currentCost > bestCost)
			{
				bestCost = currentCost;
				bestCoord = currentCoord;
			}
		}

		return bestCoord;
	}
}
