/*
 * Created on Nov 9, 2004
 */
package org.jocelyn_chaumel.firststrike.core.player.cst;

import org.jocelyn_chaumel.tools.data_type.cst.IntCst;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe d�finit les types de joueurs possibles : Humain ou ordinateur.
 * 
 * @author Jocelyn Chaumel
 */
public class PlayerTypeCst extends IntCst
{
	public final static PlayerTypeCst HUMAN = new PlayerTypeCst(1, "Human");
	public final static PlayerTypeCst COMPUTER = new PlayerTypeCst(2, "Computer");

	private final static PlayerTypeCst _ownerTypeList[] = { HUMAN, COMPUTER };

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param p_value Valeur de la constante
	 */
	private PlayerTypeCst(final int p_value, final String p_label)
	{
		super(p_value, p_label);
	}

	/**
	 * Retourne l'instante d'une constante � partir d'une valeur.
	 * 
	 * @param p_value Valeur de la constante.
	 * @return Instance de la constante.
	 * @throws InvalidConstantValueException
	 */
	public static PlayerTypeCst getInstante(final int p_value) throws InvalidConstantValueException
	{
		return (PlayerTypeCst) IntCst.getInstance(p_value, _ownerTypeList, PlayerTypeCst.class);
	}

	public static PlayerTypeCst getInstance(final String p_value) throws InvalidConstantValueException
	{
		Assert.preconditionNotNull(p_value, "Value");
		return (PlayerTypeCst) IntCst.getInstance(p_value, _ownerTypeList, PlayerTypeCst.class);
	}
}