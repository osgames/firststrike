package org.jocelyn_chaumel.firststrike.core.player;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;

/**
 * Tank City Distance.  This object specify the total distance for a specific tank to reach a specific city.
 * 
 * @author Jocelyn
 *
 */
public class TCD implements Serializable
{
	private final Tank _tank;
	private final City _city;
	private final int _disctance;
	
	public TCD(final Tank p_tank, final City p_city, final int p_distance)
	{
		_tank = p_tank;
		_city = p_city;
		_disctance = p_distance;
	}
	
	public Tank getTank()
	{
		return _tank;
	}
	
	public City getCity()
	{
		return _city;
	}

	public int getDisctance()
	{
		return _disctance;
	}
	
	@Override
	public final String toString()
	{
		final StringBuffer sb = new StringBuffer("{TDC= ");
		sb.append("city:(" + _city.getPosition().getX() + "," + _city.getPosition().getY() + ") ");
		sb.append("tank(" + _tank.getId() + "):(" + _tank.getPosition().getX() + "," + _tank.getPosition().getY() + ") ");
		sb.append("distance:" + _disctance + "}" );
		
		return sb.toString();
	}
}
