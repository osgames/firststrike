package org.jocelyn_chaumel.firststrike.core.player;

import java.util.ArrayList;

import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class EventLogList extends ArrayList<EventLog>
{
	public EventLogList getEventLogListByType(final short p_eventLogType)
	{
		final EventLogList eventLogList = new EventLogList();
		for (EventLog event : this)
		{
			if (event.getMsgType() == p_eventLogType)
			{
				eventLogList.add(event);
			}
		}
		// TODO AZ Trier la liste par ordre selon le type (par type d'unit� par exemple)
		return eventLogList;
	}
	
	public final boolean hasEnemyBombardmentAt(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord);
		
		for (EventLog event : this)
		{
			if (event.getMsgType() == EventLog.EL_TYPE_ENEMY_BOMBARDMENT && p_coord.equals(event.getCoord()))
			{
				return true;
			}
		}
		return false;
	}
}
