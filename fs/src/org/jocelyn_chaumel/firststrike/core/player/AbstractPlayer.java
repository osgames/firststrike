/*
 * AbstractPlayer.java Created on 13 juin 2003, 20:46
 */

package org.jocelyn_chaumel.firststrike.core.player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.IDetection;
import org.jocelyn_chaumel.firststrike.core.IFirstStrikeEvent;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.map.scenario.IMapCondition;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMapConditionList;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMessageList;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioUnit;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioUnitList;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioViewArea;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioViewAreaList;
import org.jocelyn_chaumel.firststrike.core.player.cst.PlayerTypeCst;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Bombardment;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.ILoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.IUnitContainer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.UnitId;
import org.jocelyn_chaumel.firststrike.core.unit.UnitIndex;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.data_type.Id;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe abstraite de basse repr�sente un joueur.
 * 
 * @author Jocelyn Chaumel
 */
public abstract class AbstractPlayer implements Serializable
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(AbstractPlayer.class.getName());

	protected final PlayerId _id;
	protected final PlayerTypeCst _type;
	protected final Coord _startPosition;
	protected final PlayerLevelCst _level;
	protected final FSMapMgr _mapMgr;
	protected final AbstractUnitList _enemySet = new AbstractUnitList();
	protected final AbstractUnitList _unitList = new AbstractUnitList();
	protected final EventLogList _eventLogList = new EventLogList();
	protected final ArrayList<IMapCondition> _mapConditionList;
	protected final ScenarioUnitList _scenarioUnitList;
	protected final ScenarioViewAreaList _scenarioViewAreaList;
	protected final ScenarioMessageList _scenarioMessageList;
	protected final ScenarioMessageList _oldScenarioMessageList = new ScenarioMessageList();
	protected final UnitIndex _unitIndex;
	protected final FSRandom _randomizer = FSRandom.getInstance();
	protected final HashMap<Coord, ArrayList<Bombardment>> _bombardmentMap = new HashMap<Coord, ArrayList<Bombardment>>();

	protected boolean _isDead = false;
	protected IFirstStrikeEvent _gameListener = null;

	private int _currentId = 1;

	public AbstractPlayer(	final PlayerId p_playerId,
							final PlayerTypeCst p_playerType,
							final Coord p_startPosition,
							final PlayerLevelCst p_level,
							final FSMapMgr p_mapMgr,
							final UnitIndex p_unitIndex,
							final ScenarioMapConditionList p_scenarioMapConditionList,
							final ScenarioUnitList p_scenarioUnitList,
							final ScenarioMessageList p_scenarioMessageList,
							final ScenarioViewAreaList p_scenarioEventList)
	{
		Assert.preconditionNotNull(p_playerId, "Player Id");
		Assert.preconditionNotNull(p_playerType, "Player Type");
		Assert.preconditionNotNull(p_level, "Player Level");
		Assert.preconditionNotNull(p_mapMgr, "Map");
		Assert.preconditionNotNull(p_startPosition, "Start Position");
		Assert.preconditionNotNull(p_unitIndex, "Unit Index");

		_id = p_playerId;
		_type = p_playerType;
		_startPosition = p_startPosition;
		_level = p_level;
		_mapMgr = p_mapMgr;
		_unitIndex = p_unitIndex;
		
		if (p_scenarioMapConditionList == null)
		{
			_mapConditionList = null;
		} else {
			_mapConditionList = p_scenarioMapConditionList;
		}
		
		if (p_scenarioUnitList == null)
		{
			_scenarioUnitList = new ScenarioUnitList();
		} else
		{
			_scenarioUnitList = p_scenarioUnitList;
			createScenarioUnitByTurn(0);
		}

		if (p_scenarioMessageList == null)
		{
			_scenarioMessageList = new ScenarioMessageList();
		} else
		{
			_scenarioMessageList = p_scenarioMessageList;
		}

		if (p_scenarioEventList == null)
		{
			_scenarioViewAreaList = new ScenarioViewAreaList();
		} else
		{
			_scenarioViewAreaList = p_scenarioEventList;
		}
	}

	public final UnitIndex getUnitIndex()
	{
		Assert.check(_unitIndex != null, "Unit Index not set");
		return _unitIndex;
	}

	public final Id getId()
	{
		Assert.check(!_isDead, "This player is dead");

		return _id;
	}

	public void setGameListener(final IFirstStrikeEvent p_gameListener)
	{
		Assert.preconditionNotNull(p_gameListener, "Game Listener");
		Assert.check(_gameListener == null, "Game Listener already set");

		_gameListener = p_gameListener;
	}

	public void addUnit(final AbstractUnit p_unit, final boolean p_withEventPropagation)
	{
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.precondition(p_unit.getPlayer() == this, "Invalid player detected");
		_unitList.add(p_unit);
		_unitIndex.newUnit(p_unit);
		_eventLogList.add(new EventLog().setUnitCreated(p_unit.getPosition(),
														p_unit.getUnitName(),
														p_unit.getUnitType(),
														_id));
		if (p_withEventPropagation)
		{
			refreshEnemyList(p_unit);
			if (_gameListener != null)
			{
				_gameListener.eventUnitCreated(p_unit);
			}
		}
	}

	public final boolean isBombardmentPlanned(final Coord p_position)
	{
		return _bombardmentMap.containsKey(p_position);
	}
	
	/**
	 * Set the player of the city and add the City to the owned city list of
	 * this player.
	 * 
	 * @param p_city City to add.
	 */
	public void addCity(final City p_city, final boolean p_withEventPropagation)
	{
		Assert.preconditionNotNull(p_city, "City");
		Assert.precondition(p_city.getPlayer() != this, "City already owned by the player");

		final AbstractPlayer oldPlayer = p_city.getPlayer();
		p_city.setPlayer(this); // and reset production

		// Restore fuel if a city has been conquered
		final Coord currentUnitPos = p_city.getPosition();
		final AbstractUnitList fighterSet = _unitIndex.getNonNullUnitListAt(currentUnitPos)
				.getUnitByType(UnitTypeCst.FIGHTER);
		for (AbstractUnit currentFighter : fighterSet)
		{
			((Fighter) currentFighter).restoreFuel();
		}

		if (!p_withEventPropagation)
		{
			Assert.check(oldPlayer == null, "City already assigned to a player");
		} else
		{
			if (oldPlayer != null)
			{
				oldPlayer.removeCity(p_city);
			}
			_gameListener.eventCityConquered(p_city, this, oldPlayer);
		}
	}

	public void removeCity(final City p_city)
	{
		checkIfPlayerIsStillAlive();
		// TODO AZ Verifier si le message lorsqu'une ville est perdue soit bien
		// affich�
		_eventLogList.add(new EventLog().setCityLost(p_city.getPosition(), p_city.getCell().getIndex(), p_city
				.getCell().getCellName()));
	}

	private boolean checkIfPlayerIsStillAlive()
	{
		if (_isDead)
		{
			return false;
		}

		// Check if nb unit > 0
		if (!_unitList.isEmpty())
		{
			return true;
		}

		// Check if nb city is > 0
		if (_mapMgr.getCityList().hasCityPlayser(this))
		{
			return true;
		}

		_isDead = true;
		_enemySet.clear();
		Assert.check(_unitList.isEmpty(), "Unit list is not empty");

		_logger.playerAction("Player killed: " + this);
		_gameListener.eventPlayerKilled(this);

		return false;
	}

	/**
	 * Indicates if this player is alive.
	 * 
	 * @return True if the player is alive and false otherwise.
	 */
	public final boolean isLiving()
	{
		return checkIfPlayerIsStillAlive();
	}

	public boolean isEnnemyPlayer(final AbstractPlayer p_player)
	{
		if (p_player == this)
		{
			return false;
		}
		return true;
	}

	public final AbstractUnitList getEnemySet()
	{
		// Assert.check(!_isDead, "This player is dead");

		return _enemySet;
	}

	/**
	 * Retourne le type du joueur, c'est � dire s'il s'agit d'un humain ou d'un
	 * ordinateur.
	 * 
	 * @return Type de joueur.
	 */
	public final PlayerTypeCst getPlayerType()
	{
		Assert.check(!_isDead, "This player is dead");

		return _type;
	}

	/**
	 * Retourne le num�ro d'identification du joueur.
	 * 
	 * @return Identification du joueur.
	 */
	public final PlayerId getPlayerId()
	{
		return _id;
	}

	/**
	 * Retourne la position de d�part du joueur.
	 * 
	 * @return Position.
	 */
	public final Coord getStartPosition()
	{
		Assert.check(!_isDead, "This player is dead");

		return _startPosition;
	}

	public final PlayerLevelCst getPlayerLevel()
	{
		return _level;
	}

	/**
	 * Follow the unit's path to the next coord.
	 * 
	 * @param p_unit Unit to move.
	 * @param p_mapMgr Map mgr.
	 * @return Move Status.
	 */
	public MoveStatusCst moveUnit(final AbstractUnit p_unit)
	{
		Assert.check(!_isDead, "This player is dead");

		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.precondition(p_unit.hasPath(), "This unit doesn't contain path");

		final Coord oldPosition = p_unit.getPosition();
		MoveStatusCst moveStatus = p_unit.move();
		Assert.check(!MoveStatusCst.ENEMY_DETECTED.equals(moveStatus), "Unexpected result detected");
		Assert.check(!MoveStatusCst.UNIT_DEFEATED.equals(moveStatus), "Unexpected result detected");
		Assert.check(!MoveStatusCst.NOT_ENOUGH_FUEL_PTS.equals(moveStatus), "Unexpected result detected");
		if (moveStatus == MoveStatusCst.NOT_ENOUGH_MOVE_PTS)
		{
			return moveStatus;
		}
		Assert.check(moveStatus == MoveStatusCst.MOVE_SUCCESFULL, "Unsupported Move Status");

		final Coord newPosition = p_unit.getPosition();

		_unitIndex.moveUnit(oldPosition, p_unit);

		boolean ennemyDetected = refreshEnemyList(p_unit);

		if (UnitTypeCst.TANK.equals(p_unit.getUnitType()))
		{
			if (_mapMgr.getCityList().hasCityAtPosition(newPosition))
			{
				final City city = _mapMgr.getCityList().getCityAtPosition(newPosition);
				if (city.getPlayer() != this)
				{
					addCity(city, true);
					ennemyDetected = ennemyDetected || refreshEnemyList(city);
				}
			}
		}

		// TODO AZ ajouter l'event Unit Attack (comme un mouvement)
		if (_gameListener != null)
		{
			_gameListener.eventUnitMoved(p_unit, newPosition, oldPosition);
		}

		if (ennemyDetected)
		{
			return MoveStatusCst.ENEMY_DETECTED;
		}
		return moveStatus;
	}

	public MoveStatusCst loadUnit(final ILoadableUnit p_loadable, final IUnitContainer p_container)
	{
		Assert.preconditionNotNull(p_loadable, "loadable unit");
		Assert.preconditionNotNull(p_container, "container unit");
		Assert.precondition(p_container.isLoadable((AbstractUnit)p_loadable), "Invalid operation");

		MoveStatusCst moveStatus = p_loadable.loadInContainer(p_container);
		if (!MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
		{
			return moveStatus;
		}
		_unitList.remove(p_loadable);
		_unitIndex.removeUnit((AbstractUnit) p_loadable);
		_gameListener.eventUnitLoaded((AbstractUnit) p_loadable, (AbstractUnit)p_container);

		return MoveStatusCst.MOVE_SUCCESFULL;
	}

	public MoveStatusCst unloadUnit(final ILoadableUnit p_unit, final Coord p_destination)
	{
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.precondition(this == ((AbstractUnit) p_unit).getPlayer(), "Invalid Tank's Player");
		Assert.precondition(p_unit.isLoaded(), "This tank is not loaded");

		final TransportShip transport = (TransportShip) p_unit.getContainer();
		((AbstractUnit) p_unit).setPosition(transport.getPosition());
		final MoveStatusCst moveStatus = p_unit.unloadFromContainer(p_destination);
		if (!MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
		{
			return moveStatus;
		}

		_unitList.add((AbstractUnit) p_unit);
		_unitIndex.newUnit((AbstractUnit) p_unit);

		boolean enemyDetected = refreshEnemyList((AbstractUnit) p_unit);

		if (p_unit instanceof Tank)
		{
			if (_mapMgr.getCityList().hasCityAtPosition(p_destination))
			{
				final City city = _mapMgr.getCityList().getCityAtPosition(p_destination);
				if (city.getPlayer() != this)
				{
					addCity(city, true);
					enemyDetected = enemyDetected || refreshEnemyList(city);
				}
			}
		}

		if (_gameListener != null)
		{
			_gameListener.eventUnitUnloaded((AbstractUnit) p_unit, transport, p_destination);
		}

		_logger.unitDetail("Tank unloaded: " + p_unit);

		if (enemyDetected)
		{
			return MoveStatusCst.ENEMY_DETECTED;
		}
		return moveStatus;
	}

	public final ScenarioMessageList getOldScenarioMessageList()
	{
		return _oldScenarioMessageList;
	}

	/**
	 * starts the turn of the player within initializing the enemy set, the unit
	 * move, fuel & hits point properties.
	 */
	public void startTurn(final int p_currentTurn)
	{
		Assert.check(!_isDead, "This player is dead");

		_logger.player("Player {0} start its turn " + p_currentTurn + ".", this);
		_gameListener.eventPlayerStartTurn(this);
		
		_enemySet.clear();

		startViewArea(p_currentTurn);
		startCondition(p_currentTurn);

		final AbstractUnitList unitList = getUnitList();
		final CityList citySet = getCitySet();

		_logger.playerAction("Unit production review:");
		for (City currentCity : citySet)
		{
			refreshEnemyList(currentCity);
			if (currentCity.isUnitUnderConstruction())
			{
				currentCity.decProductionTime();
			}
			if (currentCity.isUnitReady())
			{
				_logger.playerSubAction(currentCity.toString() + " -> Unit created");
				retrieveNewUnitFromCity(currentCity);
			} else
			{
				_logger.playerSubAction(currentCity.toString());
			}
		}

		_logger.playerSubAction("Total nb unit of current player: " + unitList.size());

		// Update unit's capabilities
		for (AbstractUnit currentUnit : unitList)
		{
			refreshEnemyList(currentUnit);
			currentUnit.restoreUnit();
		}

		startBombarment();
		
		createScenarioUnitByTurn(p_currentTurn);
	}
	
	private void startViewArea(final int p_currentTurn)
	{
		_logger.playerAction("View Area");
		if (_scenarioViewAreaList == null || _scenarioViewAreaList.isEmpty())
		{
			_logger.playerSubAction("No remaining View Area action");
			return;
		}
		final ScenarioViewAreaList viewAreaList = _scenarioViewAreaList.removeViewAreaByTurn(p_currentTurn);
		
		for (ScenarioViewArea viewArea : viewAreaList)
		{
			refreshEnemyList(viewArea);
		}
	}
	private void startCondition(final int p_currentTurn)
	{
		_logger.playerAction("Map Condition");
		if (_mapConditionList == null || _mapConditionList.isEmpty())
		{
			_logger.playerSubAction("No remaining Map Condition ");
			return;
		}
		ArrayList<IMapCondition> completedMapCondition = new ArrayList<IMapCondition>();
		for (IMapCondition condition : _mapConditionList)
		{
			if (condition.executeCondition(this, p_currentTurn)){
				completedMapCondition.add(condition);
			}
		}
		
		if (!completedMapCondition.isEmpty())
		{
			_mapConditionList.removeAll(completedMapCondition);
		}
	}
	
	private void startBombarment()
	{
		_logger.playerAction("Bombardment");
		if (_bombardmentMap.isEmpty())
		{
			_logger.playerSubAction("No Bombardment recorded for this turn");
			return;
		}
		
		int nbBombardment = 0, damage;
		Cell currentCell;
		final CityList allCitySet = _mapMgr.getCityList();
		for (Coord position : _bombardmentMap.keySet())
		{
			_logger.playerSubAction("Bombardment at " + position);
			final AbstractUnitList unitList = getUnitIndex().getNonNullUnitListAt(position);
			if (unitList.isEmpty())
			{
				continue;
			}
			currentCell = _mapMgr.getMap().getCellAt(position);
			unitList.get(0).getPlayer()._eventLogList.add(new EventLog().setBombardment(position,
																						currentCell.getCellType(),
																						currentCell.getIndex()));
			final AbstractUnitList nonFlyingUnitList = unitList.getNonFlyingUnitList(allCitySet);
			if (nonFlyingUnitList.isEmpty())
			{
				continue;
			}
			for (Bombardment bombardment : _bombardmentMap.get(position))
			{

				for (AbstractUnit unit : nonFlyingUnitList)
				{
					if (!unit.isLiving())
					{
						continue;
					}
					nbBombardment++;
					damage = bombardment.getRandomDamage();
					unit.setHitPoints(unit.getHitPoints() - damage);
					if (!unit.isLiving())
					{
						unit.getPlayer().killUnit(unit);
						_logger.playerSubActionDetail("" + damage + " damage on " + unit + " and killed");
					} else
					{
						_logger.playerSubActionDetail("" + damage + " damage on " + unit);
					}
				}
			}
		}

		_logger.playerSubActionDetail("Nb bombardment done: " + nbBombardment);
		_bombardmentMap.clear();
	}

	protected IFirstStrikeEvent getGameListener()
	{
		return _gameListener;
	}

	protected void createScenarioUnitByTurn(final int p_currentTurn)
	{
		final ScenarioUnitList scenarioUnitList = _scenarioUnitList.removeUnitByTurn(p_currentTurn);
		AbstractUnit currentUnit = null;
		if (scenarioUnitList.isEmpty())
		{
			return;
		}
		_logger.playerAction("Introduce unit described into the scenario.");

		for (ScenarioUnit scnUnit : scenarioUnitList)
		{
			try
			{
				currentUnit = ScenarioUnit.createUnit(scnUnit, this);
				_logger.playerSubAction("Unit introduced: " + currentUnit);
				addUnit(currentUnit, true);
			} catch (InvalidScenarioException ex)
			{
				_logger.systemError("Unable to create this unit " + scnUnit.getType(), ex);
				throw new FSFatalException("Error detected into the scenario", ex);
			}
		}
	}

	protected AbstractUnit retrieveNewUnitFromCity(final City p_city)
	{
		final AbstractUnit unit = p_city.getProducedUnit();
		// Unit automatically added to the unitIndex
		addUnit(unit, true);
		return unit;
	}

	/**
	 * Permit to launch an enemy detection process. This method return true if
	 * an enemy has been detected.
	 * 
	 * @param p_detector A Detector (Unit or City)
	 * @return True or False.
	 */
	protected boolean refreshEnemyList(final IDetection p_detector)
	{
		Assert.check(!_isDead, "This player is dead");
		Assert.preconditionNotNull(p_detector, "Detector");
		Assert.precondition(this == p_detector.getPlayer(), "Invalid unit player");

		final Coord detectorCoord = p_detector.getPosition();
		final FSMap map = _mapMgr.getMap();

		final CoordSet coordScanList = CoordSet.createInitiazedCoordSet(detectorCoord,
																		p_detector.getDetectionRange(),
																		map.getWidth(),
																		map.getHeight());
		coordScanList.remove(detectorCoord);
		
		return refreshEnemyList(coordScanList);
	}
	
	protected boolean refreshEnemyList(final CoordSet p_coordScanList)
	{
		final AbstractUnitList playerEnemyList = this.getEnemySet();
		final AbstractUnitList newEnemyDetectedList = _unitIndex.getEnemyListAt(this, p_coordScanList);
		newEnemyDetectedList.removeAll(playerEnemyList);
		if (!newEnemyDetectedList.isEmpty())
		{
			_logger.playerSubActionDetail("Enemy detected: " + newEnemyDetectedList);
		}
		final boolean hasEnemyDetected = playerEnemyList.addAll(newEnemyDetectedList);
		return hasEnemyDetected;
	}

	protected boolean refreshEnemyList(final ScenarioViewArea p_viewArea)
	{
		Assert.check(!_isDead, "This player is dead");
		Assert.preconditionNotNull(p_viewArea, "View Area");

		if (p_viewArea.isUnitDetection())
		{
			return false;
		}
		final Coord startCoord = p_viewArea.getPosition();
		final FSMap map = _mapMgr.getMap();
		
		

		final CoordSet coordScanList = CoordSet.createInitiazedCoordSet(startCoord,
																		p_viewArea.getWidth(),
																		p_viewArea.getHeight(),
																		map.getWidth(),
																		map.getHeight());
		return refreshEnemyList(coordScanList);
		
		
	}
	
	public void killEnemyUnit(final AbstractUnit p_killedUnit)
	{
		Assert.preconditionNotNull(p_killedUnit, "Killed Unit");
		_enemySet.remove(p_killedUnit);
	}

	public AbstractUnitList finishTurn()
	{
		_logger.playerAction("Player {0} finish its turn.", this);

		final AbstractUnitList killedUnitList = new AbstractUnitList();
		final CityList citySet = this.getCitySet();
		Coord currentPosition = null;
		for (AbstractUnit currentUnit : this.getUnitList().getNonMovingFighter())
		{
			currentPosition = currentUnit.getPosition();
			if (!citySet.hasCityAtPosition(currentPosition) && currentUnit.getFuel() <= MoveCostFactory.AIR_MOVE_COST.getMinCost())
			{
				_logger.systemAction("Fighter crash because not enough fuel : {0}", currentUnit);
				if (currentUnit.getFuel() == MoveCostFactory.AIR_MOVE_COST.getMinCost())
				{
					currentUnit.decreaseFuel();
				}
				killUnit(currentUnit);
				killedUnitList.add(currentUnit);
			} else
			{
				currentUnit.decreaseFuel();
			}
		}

		_eventLogList.clear();
		_gameListener.eventPlayerEndTurn(this);
		return killedUnitList;
	}

	public final AbstractUnitList getUnitList()
	{
		return _unitList;
	}

	public final CityList getCitySet()
	{
		return _mapMgr.getCityList().getCityByPlayer(this);
	}

	public final EventLogList getEventLog()
	{
		return _eventLogList;
	}
	
	public final void replaceReferenceByTurn(final String p_nodeReference, final int p_currentTurn)
	{
		_scenarioMessageList.replaceReferenceByTurn(p_nodeReference, p_currentTurn);
		_scenarioUnitList.replaceReferenceByTurn(p_nodeReference, p_currentTurn);
	}
	
	public final void duplicateReferenceByTurn(final String p_nodeReference, final int p_currentTurn)
	{
		
		try
		{
			_scenarioMessageList.duplicateReferenceByTurn(p_nodeReference, p_currentTurn);
			_scenarioUnitList.duplicateReferenceByTurn(p_nodeReference, p_currentTurn);
		} catch (InvalidScenarioException ex)
		{
			throw new FSFatalException(ex);
		}
	}
	
	public final void replaceReferenceByTurns(final String p_nodeReference, final int [] p_turnArry)
	{
		_scenarioUnitList.replaceReferenceByTurns(p_nodeReference, p_turnArry);
	}
	
	public void clearScenarioUnitWithReference(final String p_reference)
	{
		final ScenarioUnitList tempList = new ScenarioUnitList(_scenarioUnitList);
		for (ScenarioUnit unit : tempList)
		{
			if (p_reference.equals(unit.getReference()))
			{
				_scenarioUnitList.remove(unit);
			}
		}
	}


	/**
	 * Remove the unit from the manager and update linked unit (Destroyer to
	 * Transport for example.
	 * 
	 * @param p_unit Unit to remove.
	 */
	public void killUnit(final AbstractUnit p_unit)
	{
		Assert.check(!_isDead, "This player is dead");

		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.check(this == p_unit.getPlayer(), "Invalid unit player");
		Assert.check(_unitList.remove(p_unit), "Unknow Unit detected");
		_unitIndex.removeUnit(p_unit);

		final UnitTypeCst type = p_unit.getUnitType();

		if (UnitTypeCst.TRANSPORT_SHIP.equals(type))
		{
			final TransportShip transport = (TransportShip) p_unit;
			if (transport.isContainingUnit())
			{
				transport.releaseAllUnit();
			}
		}

		_eventLogList.add(new EventLog().setUnitLost(p_unit.getPosition(), p_unit.getBestUnitName(), type, _id));
		_gameListener.eventUnitKilled(p_unit);

		checkIfPlayerIsStillAlive();
	}

	public final FSMapMgr getMapMgr()
	{
		return _mapMgr;
	}

	public UnitId getNewUnitId()
	{
		return new UnitId(_currentId++);
	}

	public void addBombardment(final Coord p_coord, final Bombardment p_bombardment)
	{
		ArrayList<Bombardment> bombardmentList = _bombardmentMap.get(p_coord);
		if (bombardmentList == null)
		{
			bombardmentList = new ArrayList<Bombardment>();
		}

		bombardmentList.add(p_bombardment);
		_bombardmentMap.put(p_coord, bombardmentList);
	}

	public abstract boolean play();

}