/*
 * Created on Dec 4, 2004
 */
package org.jocelyn_chaumel.firststrike.core.player;

import org.jocelyn_chaumel.tools.data_type.Id;

/**
 * @author Jocelyn Chaumel
 */
public final class PlayerId extends Id
{
	public static final PlayerId NO_PLAYER = new PlayerId(Integer.MAX_VALUE);

	/**
	 * Constructeur paramétrisé.
	 * 
	 * @param p_playerId Identifiant du joueur.
	 */
	public PlayerId(final int p_playerId)
	{
		super(p_playerId);
	}
}