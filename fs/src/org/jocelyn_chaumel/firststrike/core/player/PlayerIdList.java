/*
 * Created on Aug 24, 2005
 */
package org.jocelyn_chaumel.firststrike.core.player;

import java.util.Iterator;

import org.jocelyn_chaumel.tools.data_type.AbstractInstanceArrayList;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class PlayerIdList extends AbstractInstanceArrayList
{

	/**
	 * Constructeur par d�faut.
	 */
	public PlayerIdList()
	{
		super(PlayerId.class, "PlayerIdList");
	}

	/**
	 * Constructeur par d�faut.
	 */
	public PlayerIdList(PlayerId[] p_playerIdArray)
	{
		super(PlayerId.class, "PlayerIdList");

		Assert.preconditionNotNull(p_playerIdArray, "Player Id List");

		for (int i = 0; i < p_playerIdArray.length; i++)
		{
			Assert.preconditionNotNull(p_playerIdArray[i], "PlayerIdArray[" + i + "]");
			add(p_playerIdArray[i]);
		}
	}

	public final PlayerId getPlayerId(final int p_playerIdInt)
	{
		final Iterator iter = iterator();
		PlayerId currentPlayerId;
		PlayerId returnedplayerId = null;
		while (iter.hasNext() && returnedplayerId == null)
		{
			currentPlayerId = (PlayerId) iter.next();
			if (currentPlayerId.getId() == p_playerIdInt)
			{
				returnedplayerId = currentPlayerId;
			}
		}

		Assert.postcondition(returnedplayerId != null, "Player Id Unknown :" + p_playerIdInt);

		return returnedplayerId;
	}
}
