/*
 * PlayerMgr.java Created on 27 avril 2003, 21:18
 */

package org.jocelyn_chaumel.firststrike.core.player;

import java.io.Serializable;
import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.IFirstStrikeEvent;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.scenario.MapScenario;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioPlayer;
import org.jocelyn_chaumel.firststrike.core.player.cst.PlayerTypeCst;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.UnitIndex;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.data_type.PlayerInfo;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public final class PlayerMgr implements Serializable
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(PlayerMgr.class.getName());
	private final AbstractPlayerList _playerList;

	private AbstractPlayer _currentPlayer;

	private int _turnNumber = 1;

	public PlayerMgr(final AbstractPlayerList p_playerList)
	{
		Assert.preconditionNotNull(p_playerList);
		Assert.precondition(p_playerList.size() >= 2, "Not enough players");

		_playerList = p_playerList;
		_currentPlayer = (AbstractPlayer) p_playerList.get(0);
	}

	public int getTurnNumber()
	{
		return _turnNumber;
	}

	public static AbstractPlayerList createPlayers(final MapScenario p_scenario, final FSMapMgr p_mapMgr)
	{
		Assert.preconditionNotNull(p_scenario, "Scenario");
		Assert.preconditionNotNull(p_mapMgr, "MapMgr");

		final AbstractPlayerList playerList = new AbstractPlayerList();
		final UnitIndex unitIndex = new UnitIndex();

		for (ScenarioPlayer scnPlayer : p_scenario.getPlayerList())
		{
			playerList.add(ScenarioPlayer.createPlayer(scnPlayer, p_mapMgr, unitIndex));
		}
		return playerList;
	}

	/**
	 * Create a player list and set the starting city.
	 * 
	 * @param p_playerInfoList Information about players
	 * @param p_mapMgr Map Manager used by the game.
	 * 
	 * @return List of player.
	 */
	public static AbstractPlayerList createPlayers(final PlayerInfoList p_playerInfoList, final FSMapMgr p_mapMgr)
	{
		Assert.preconditionNotNull(p_playerInfoList, "Player Info List");
		Assert.preconditionNotNull(p_mapMgr, "MapMgr");
		Assert.precondition(p_playerInfoList.size() >= 2, "Invalid Player Info Array");
		Assert.precondition(PlayerTypeCst.HUMAN.equals(((PlayerInfo) p_playerInfoList.get(0)).getOwner()),
							"Invalid Player Info List");

		final UnitIndex unitIndex = new UnitIndex();
		final AbstractPlayerList playerList = new AbstractPlayerList();
		PlayerId playerId = null;
		PlayerInfo currentPlayerInfo = null;
		Coord startLocation = null;
		int number = 0;
		PlayerTypeCst playerType = null;
		AbstractPlayer player = null;
		ComputerPlayer hardPlayer = null;

		// For each player info
		final Iterator iter = p_playerInfoList.iterator();
		while (iter.hasNext())
		{
			currentPlayerInfo = (PlayerInfo) iter.next();

			// Start Location validation
			startLocation = currentPlayerInfo.getStartLocation();
			Assert.preconditionNotNull(startLocation, "Start Location");

			playerId = new PlayerId((short) (++number));

			playerType = currentPlayerInfo.getOwner();
			if (PlayerTypeCst.COMPUTER.equals(playerType))
			{
				_logger.systemSubAction("Creation of the computer player at {0}", startLocation);
				player = new ComputerPlayer(playerId,
											startLocation,
											p_mapMgr,
											currentPlayerInfo.getPlayerLevel(),
											unitIndex,
											null,
											null, 
											null);
				final City city = p_mapMgr.getMap().getCityList().getCityAtPosition(startLocation);
				city.setPlayer(player);
				if (player.getPlayerLevel().equals(PlayerLevelCst.HARD_LEVEL))
				{
					hardPlayer = (ComputerPlayer) player;
					city.setProductionType(UnitTypeCst.TANK);
					while (city.getProductionTime() > 1)
					{
						city.decProductionTime();
					}
				}
			} else
			{
				_logger.systemSubAction("Creation of the human player at {0}", startLocation);
				player = new HumanPlayer(	playerId,
											startLocation,
											p_mapMgr,
											currentPlayerInfo.getPlayerLevel(),
											unitIndex,
											null,
											null,
											null, 
											null);
				if (p_mapMgr.getCityList().hasCityAtPosition(startLocation)) {
					final City city = p_mapMgr.getMap().getCityList().getCityAtPosition(startLocation);
					city.setPlayer(player);
					final CoordSet shadowSet = ((HumanPlayer) player).getShadowSet();
					
					// Removing shadow around the starting city
					shadowSet.removeAll(shadowSet.getCoordInRange(startLocation, city.getDetectionRange()));
				}
			}

			playerList.add(player);

		}

		if (hardPlayer != null)
		{
			final CityList citySet = p_mapMgr.getMap().getFSMapCells().getCityList();
			City currentCity = null;
			final Iterator<City> cityIter = citySet.iterator();
			while (cityIter.hasNext())
			{
				currentCity = cityIter.next();
				if (currentCity.getPlayer() == null)
				{
					_logger.systemSubActionDetail("Second starting city selected at {0}", currentCity.getPosition());
					currentCity.setPlayer(hardPlayer);
					currentCity.setProductionType(UnitTypeCst.TANK);
					currentCity.decProductionTime();
					break;
				}
			}
		}
		return playerList;
	}

	public final void setGameListener(final IFirstStrikeEvent p_gameListener)
	{
		Assert.preconditionNotNull(p_gameListener, "Game Listener");

		for (AbstractPlayer currentPlayer : getPlayerList())
		{
			currentPlayer.setGameListener(p_gameListener);
		}
	}

	public AbstractPlayerList getPlayerList()
	{
		return _playerList;
	}

	/**
	 * Switch the game control to next player. If their is remaining only zero
	 * or one player, this method return null. Null value indicated the end of
	 * game.
	 * 
	 * @return New current player or null if the game is finished.
	 */
	public final AbstractPlayer nextPlayer()
	{
		final AbstractPlayerList livingPlayerList = _playerList.getLivingPlayer();
		if (livingPlayerList.size() == 1)
		{
			_currentPlayer = null;
			return null;
		}

		AbstractPlayer currentPlayer = null;
		boolean isFound = false;
		int currentPlayerIdx = 1 + livingPlayerList.indexOf(_currentPlayer);
		while (!isFound)
		{
			currentPlayer = (AbstractPlayer) livingPlayerList.get(currentPlayerIdx % livingPlayerList.size());
			if (currentPlayer.isLiving())
			{
				isFound = true;
			} else
			{
				_currentPlayer = null;
				return null;
			}
		}

		if (currentPlayerIdx >= livingPlayerList.size())
		{
			_turnNumber++;
		}

		if (isFound)
		{

			_currentPlayer = currentPlayer;
		} else
		{
			_currentPlayer = null;
		}
		return _currentPlayer;
	}

	public final boolean isGameOver()
	{
		return _playerList.getLivingPlayer().size() <= 1;
	}

	public AbstractPlayer getCurrentPlayer()
	{
		return _currentPlayer;
	}

	public final void killEnemyUnit(final AbstractUnit p_enemy)
	{
		final AbstractPlayer enemyPlayer = p_enemy.getPlayer();
		for (AbstractPlayer currentPlayer : _playerList)
		{
			if (enemyPlayer != currentPlayer)
			{
				currentPlayer.killEnemyUnit(p_enemy);
			}
		}
	}
}