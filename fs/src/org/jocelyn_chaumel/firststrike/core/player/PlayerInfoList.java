/*
 * Created on Aug 27, 2005
 */
package org.jocelyn_chaumel.firststrike.core.player;

import org.jocelyn_chaumel.firststrike.data_type.PlayerInfo;
import org.jocelyn_chaumel.tools.data_type.AbstractInstanceArrayList;

/**
 * @author Jocelyn Chaumel
 */
public class PlayerInfoList extends AbstractInstanceArrayList
{

	/**
	 * Contructeur par d�faut.
	 */
	public PlayerInfoList()
	{
		super(PlayerInfo.class, "PlayerInfo");
	}

}
