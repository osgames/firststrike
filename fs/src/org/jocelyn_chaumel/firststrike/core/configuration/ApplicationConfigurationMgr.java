/*
 * Created on Aug 27, 2005
 */
package org.jocelyn_chaumel.firststrike.core.configuration;

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class ApplicationConfigurationMgr
{
	public static final int NB_LAST_GAME_DIR = 4;
	private static ApplicationConfigurationMgr _managerInstance = null;

	private ApplicationConfiguration _applicationConfig;

	private ApplicationConfigurationMgr(final File p_file) throws FileNotFoundException, IOException
	{
		Assert.preconditionNotNull(p_file, "Application Config File");
		Assert.preconditionIsFile(p_file);
		_applicationConfig = new ApplicationConfiguration(p_file);
	}

	private ApplicationConfigurationMgr(final URL p_url) throws FileNotFoundException, IOException
	{
		Assert.preconditionNotNull(p_url, "URL");
		_applicationConfig = new ApplicationConfiguration(p_url);
	}

	/**
	 * Return an instance of the ApplicationConfigurationMgr. This Manager
	 * create a new ApplicationConfiguration.properties file or load this file
	 * if already exists.
	 * 
	 * @return An App. Conf. Manager.
	 * @throws IOException
	 */
	public final static ApplicationConfigurationMgr getInstance()
	{
		// The instance has been already loaded
		if (_managerInstance == null)
		{
			throw new FSFatalException("No Application Configuration Mgr instance has been created");
		}

		return _managerInstance;
	}

	/**
	 * Return an instance of the ApplicationConfigurationMgr. This Manager
	 * create a new ApplicationConfiguration.properties file or load this file
	 * if already exists.
	 * 
	 * @return An App. Conf. Manager.
	 * @throws IOException
	 */
	public final static ApplicationConfigurationMgr getCreateInstance(final File p_localAppDataDir) throws IOException
	{
		// The instance has been already loaded
		if (_managerInstance != null)
		{
			return _managerInstance;
		}

		// Check if the application.properties file exists
		final File appConfigProperties = new File(	p_localAppDataDir,
													ApplicationConfiguration.APPLICATION_PROPERTIES_FILENAME);
		if (appConfigProperties.isFile())
		{
			// Load the existing properties file
			_managerInstance = new ApplicationConfigurationMgr(appConfigProperties);
			return _managerInstance;
		}

		// Otherwise, we must create a copy from application.properties
		// contained in the FirstStrike's
		// Resources folder.

		final URL url = ClassLoader.getSystemResource(ApplicationConfiguration.APPLICATION_PROPERTIES_RESOURCE);
		_managerInstance = new ApplicationConfigurationMgr(url);
		_managerInstance.setFSLogsDir(new File(p_localAppDataDir, "\\logs"));
		_managerInstance.saveAs(appConfigProperties);
		_managerInstance.setMapSourceDir(new File(p_localAppDataDir, "\\maps"));
		_managerInstance.setGameDir(new File(p_localAppDataDir, "\\games"));
		_managerInstance.save();

		return _managerInstance;
	}

	public final void setDefaultFogActivation(final boolean p_fogActivation)
	{
		_applicationConfig.setDefaultFogActivation(p_fogActivation);
	}

	public final boolean isDefaultFogActivation()
	{
		return _applicationConfig.getDefaultFogActivation();
	}

	public final boolean isOptionDisplayHelpEnabled()
	{
		return _applicationConfig.isDisplayHelpEnabled();
	}

	public final void setOptionDisplayHelp(final boolean p_optionValue)
	{
		_applicationConfig.setOptionDisplayHelp(p_optionValue);
	}

	public final void setGameDir(final File p_savedGameDir)
	{
		_applicationConfig.setGameDir(p_savedGameDir);
	}

	public final File getGameDir()
	{
		return _applicationConfig.getGameDir();
	}

	// TODO AZ Methode a supprimer
	public final void setFSSkinDir(final File p_skinDir)
	{
		_applicationConfig.setFSSkinDir(p_skinDir);
	}

	public final File getFSSkinDir()
	{
		return _applicationConfig.getFSSkinDir();
	}

	public final void setMapSourceDir(final File p_mapSourceDir)
	{
		_applicationConfig.setMapDir(p_mapSourceDir);
	}

	public final File getMapDir()
	{
		return _applicationConfig.getMapDir();
	}

	public final void setFSLogsDir(final File p_logsDir)
	{
		_applicationConfig.setFSLogsDir(p_logsDir);
	}

	public final File getFSLogsDir()
	{
		return _applicationConfig.getFSLogsDir();
	}

	public final void setFSLogsActivation(final boolean p_fogsActivationFlag)
	{
		_applicationConfig.setFSLogsActivation(p_fogsActivationFlag);
	}

	public final boolean isFSLogsActivation()
	{
		return _applicationConfig.getFSLogsActivation();
	}

	public final boolean save() throws IOException
	{
		return _applicationConfig.save();
	}

	public final void saveAs(final File p_propertiesFile) throws IOException
	{
		Assert.preconditionNotNull(p_propertiesFile, "Properties File");

		_applicationConfig.saveAs(p_propertiesFile);
	}

	public final void setLastGameList(final ArrayList<String> p_lastGameList)
	{
		_applicationConfig.setLastGameList(p_lastGameList);
	}

	public final ArrayList<String> getLastGameList()
	{
		return _applicationConfig.getLastGameList();
	}

	public final void updateLastGameList(final File p_dir)
	{
		Assert.preconditionNotNull(p_dir, "File Object");

		final ArrayList<String> lastGameList = _applicationConfig.getLastGameList();
		final String strDir = p_dir.getAbsolutePath();
		if (lastGameList.contains(strDir))
		{
			lastGameList.remove(strDir);
		}
		lastGameList.add(0, strDir);
		while (lastGameList.size() > NB_LAST_GAME_DIR)
		{
			lastGameList.remove(lastGameList.size() - 1);
		}
		_applicationConfig.updateLastGameList(lastGameList);
	}

	public final void removeGameFromLastGameList(final File p_gameFile)
	{
		Assert.preconditionNotNull(p_gameFile, "Game File");
		final ArrayList<String> lastGameList = _applicationConfig.getLastGameList();
		final String strDir = p_gameFile.getAbsolutePath();
		if (lastGameList.contains(strDir))
		{
			lastGameList.remove(strDir);
			_applicationConfig.updateLastGameList(lastGameList);
		}
	}

	public final void setDefaultAutoSave(final boolean p_autoSaveOption)
	{
		_applicationConfig.setDefaultAutoSave(p_autoSaveOption);
	}

	public final boolean isDefaultAutoSave()
	{
		return _applicationConfig.isDefaultAutoSave();
	}
	
	public final boolean isTurnSaveMode()
	{
		return _applicationConfig.isTurnSaveMode();
	}

	public final String getFSVersion()
	{
		return _applicationConfig.getFSVersion();
	}

	public final ArrayList getFSEvolutionDetails()
	{
		return _applicationConfig.getFSEvolutionDetails();
	}

	public final String getFSVersionCompilation()
	{
		return _applicationConfig.getFSVersionCompilation();
	}

	public final void setDefaultSkin(final File p_userSkinDir)
	{
		_applicationConfig.setDefaultSkin(p_userSkinDir);
	}

	public final File getDefaultSkin()
	{
		return _applicationConfig.getDefaultSkin();
	}

	public final void appendChapterNameList(final ArrayList<String> p_chapterNameList)
	{
		_applicationConfig.addHelpChapterVisited(p_chapterNameList);
	}

	public final ArrayList<String> getChapterNameList()
	{
		return _applicationConfig.getChapterNameList();
	}

	public final Dimension getScreenDimension()
	{
		return _applicationConfig.getScreenSize();
	}

	public final void setScreenDimension(final Dimension p_dim)
	{
		_applicationConfig.setScreenSize(p_dim);
	}

	public final Point getScreenLocation()
	{
		return _applicationConfig.getScreenLocation();
	}

	public final void setScreenLocation(final Point p_location)
	{
		_applicationConfig.setScreenLocation(p_location);
	}

	public final boolean isDebugMode()
	{
		return _applicationConfig.isDebugMode();
	}

	public final void setDebugMode(final boolean p_isDebugMode)
	{
		_applicationConfig.setDebugMode(p_isDebugMode);
	}

	/**
	 * Indicates if First Strike properties has been modified.
	 * 
	 * @return True or False
	 */
	public final boolean isModified()
	{
		return _applicationConfig.isModified();
	}
	
	public void setAutoMoveSpeed(final int p_speed) throws IOException
	{
		_applicationConfig.setAutoMoveSpeed(p_speed);
		save();
	}
	
	public int getAutoMoveSpeed()
	{
		return _applicationConfig.getAutoMoveSpeed();
	}
	
	public boolean isAutoMoveActivated()
	{
		return _applicationConfig.isAutoMoveActivated();
	}
	
	public boolean toggleAutoMoveActivation() throws IOException
	{
		final boolean newValue = _applicationConfig.toggleAutMoveActivation();
		save();
		return newValue;
	}
	
}
