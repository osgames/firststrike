package org.jocelyn_chaumel.firststrike.core.configuration;

public interface IConfiguration
{
	public final static int NB_TANK_DEFEND_CITY_EASY_LEVEL = 1;

	/**
	 * Max security range where a city needs protector (tank waiting within the
	 * city)
	 */
	public final static int CITY_SECURITY_RANGE = 10;
}
