/*
 * Created on Aug 27, 2005
 */
package org.jocelyn_chaumel.firststrike.core.configuration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.jocelyn_chaumel.tools.DataProperties;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public abstract class AbstractConfigurationLoader
{

	protected final DataProperties _properties = new DataProperties();

	public AbstractConfigurationLoader(final File p_file) throws IOException
	{
		Assert.preconditionNotNull(p_file, "File");
		Assert.preconditionIsFile(p_file);

		_properties.load(p_file);
	}

	public AbstractConfigurationLoader(final URL p_url) throws IOException
	{
		Assert.preconditionNotNull(p_url, "URL");

		final InputStream inputStream = p_url.openStream();
		try
		{
			_properties.load(inputStream);
		} finally
		{
			inputStream.close();
		}

	}
}