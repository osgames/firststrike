/*
 * Created on Aug 27, 2005
 */
package org.jocelyn_chaumel.firststrike.core.configuration;

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.tools.DataProperties;
import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class ApplicationConfiguration
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(ApplicationConfiguration.class.getName());

	public static final String APPLICATION_PROPERTIES_RESOURCE = "org/jocelyn_chaumel/firststrike/resources/FirstStrike.properties";
	public static final String APPLICATION_PROPERTIES_FILENAME = "\\FirstStrike.properties";

	public static final String SYSTEM_LOG_FILENAME = "fs_system_trace";
	public static final String GAME_LOG_FILENAME = "fs_game_trace";

	private static final String FS_OPTION_FOG_ACTIVATION = "fs.option.fog_activation";
	private static final String FS_OPTION_AUTO_SAVE = "fs.option.auto_save";
	private static final String FS_OPTION_TURN_SAVE_MODE = "fs.option.turn_save_mode";
	private static final String FS_DEFAULT_SKIN = "fs.default.skin";

	private static final String FS_SKIN_DIR = "fs.skin_directory";
	private static final String FS_LOGS_DIR = "fs.logs_directory";
	private static final String FS_LOGS_ACTIVATION = "fs.logs_activation";
	private static final String FS_HELP_CHAPTER_LIST = "fs.help_chapter";
	private static final String FS_DEBUG_MODE = "fs.debug_mode";

	private static final String FS_FIRST_STRIKE_VERSION = "fs.version";
	private static final String FS_FIRST_STRIKE_DATE_VERSION = "fs.version.date";

	private static final String FS_MAP_DIR = "fs.map_directory";
	private static final String FS_GAME_DIR = "fs.game_directory";
	private static final String FS_OPTION_LAST_GAME_LIST = "fs.last_saved_game_directory";
	private static final String FS_OPTION_DISPLAY_HELP = "fs.option.display_help";
	private static final String FS_SCREEN_SIZE_WIDTH = "fs.screen_size.width";
	private static final String FS_SCREEN_SIZE_HEIGHT = "fs.screen_size.height";
	private static final String FS_SCREEN_X_LOCATION = "fs.screen_location.x";
	private static final String FS_SCREEN_Y_LOCATION = "fs.screen_location.y";
	private static final String FS_AUTO_MOVE_SPEED = "fs.auto_move.speed";
	private static final String FS_AUTO_MOVE_ACTIVATED = "fs.auto_move.activated";
	
	

	private static final String[] PROPERTY_LIST = { FS_OPTION_FOG_ACTIVATION, FS_FIRST_STRIKE_VERSION,
			FS_LOGS_ACTIVATION, FS_DEFAULT_SKIN, FS_OPTION_TURN_SAVE_MODE, FS_OPTION_DISPLAY_HELP, FS_DEBUG_MODE, FS_AUTO_MOVE_SPEED, FS_AUTO_MOVE_ACTIVATED };

	private final DataProperties _properties = new DataProperties();
	private String _sourceFile;
	private boolean _isModified = false;
	private boolean _isSaveDisabled;

	/**
	 * Build an application configuration from a specified file (.properties).
	 * 
	 * @param p_fsConfig properties file.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public ApplicationConfiguration(final File p_fsConfig) throws FileNotFoundException, IOException
	{
		Assert.preconditionNotNull(p_fsConfig, "properties file");
		Assert.preconditionIsFile(p_fsConfig);

		_logger.systemAction("Loading Application.properties");
		_logger.systemSubAction("Load properties file");
		_properties.load(p_fsConfig);
		_sourceFile = p_fsConfig.getAbsolutePath();
		validatePropertiesFile();
		_isModified = false;
		_isSaveDisabled = false;
	}

	/**
	 * Build an application configuration from first strike's resources. This is
	 * the default application.properties file. This file can't be modified but
	 * the user can use the save as function to make a copy and customize the
	 * file content.
	 * 
	 * @param p_url URL linked to the first strike's resources
	 * @throws IOException
	 */
	public ApplicationConfiguration(final URL p_url) throws IOException
	{
		Assert.preconditionNotNull(p_url, "URL");
		_logger.systemAction("Loading Application.properties from First Strike's resources");
		_sourceFile = p_url.getPath();
		InputStream inputString = null;
		try
		{
			_logger.systemSubAction("Load properties file");
			inputString = p_url.openStream();
			_properties.load(inputString);
		} finally
		{
			if (inputString != null)
			{
				try
				{
					inputString.close();
				} catch (IOException ex)
				{
					_logger.systemWarning("Unable to close the inputStream associated to this URL : " + _sourceFile);
				}
			}
		}
		validatePropertiesFile();
		_isModified = false;
		_isSaveDisabled = true;
	}

	/**
	 * Check if the file's content is valid.
	 */
	private void validatePropertiesFile()
	{
		Assert.check(_properties != null, "Valid File Mandatory");
		_logger.systemSubAction("Validate properties file content");
		for (int i = 0; i < PROPERTY_LIST.length; i++)
		{
			getMandatoryValue(PROPERTY_LIST[i]);
		}
	}

	/**
	 * Permet de savoir si, par d�faut, le brouillard doit �tre affich� ou non
	 * dans une partie.
	 * 
	 * @return
	 */
	public final boolean getDefaultFogActivation()
	{
		return _properties.getBooleanProperty(FS_OPTION_FOG_ACTIVATION);
	}

	/**
	 * Ajuste la valeur par d�faut pour d�terminer si oui ou non le brouillard
	 * doit �tre appliqu� sur le plateau de jeu dans une partie.
	 * 
	 * @param p_fogActivationFlag
	 */
	public final void setDefaultFogActivation(final boolean p_fogActivationFlag)
	{
		_properties.setBooleanProperty(FS_OPTION_FOG_ACTIVATION, p_fogActivationFlag);
		_isModified = true;
	}
	
	public final void setAutoMoveSpeed(final int p_speed)
	{
		_properties.setIntProperty(FS_AUTO_MOVE_SPEED, p_speed);
		_isModified = true;
	}
	
	public final int getAutoMoveSpeed()
	{
		return _properties.getIntProperty(FS_AUTO_MOVE_SPEED);
	}
	
	public boolean isAutoMoveActivated()
	{
		return _properties.getBooleanProperty(FS_AUTO_MOVE_ACTIVATED);
	}
	
	public boolean toggleAutMoveActivation()
	{
		final boolean value = !_properties.getBooleanProperty(FS_AUTO_MOVE_ACTIVATED);
		_properties.setBooleanProperty(FS_AUTO_MOVE_ACTIVATED, value);
		_isModified = true;
		return value;
	}

	/**
	 * Ajuste l'emplacement par d�faut contenant les cartes FS.
	 * 
	 * @param p_defaultMapDir R�pertoire par d�faut.
	 */
	public final void setMapDir(final File p_defaultMapDir)
	{
		Assert.preconditionNotNull(p_defaultMapDir, "Map Source Dir");
		Assert.preconditionIsDir(p_defaultMapDir);

		_properties.setProperty(FS_MAP_DIR, p_defaultMapDir.getAbsolutePath());
		_isModified = true;
	}

	/**
	 * Retourne l'emplacement par d�faut o� se trouve les cartes FS.
	 * 
	 * @return R�pertoire par d�faut.
	 */
	public final File getMapDir()
	{
		final String mapSourceDirStr = getValue(FS_MAP_DIR);
		if (mapSourceDirStr == null)
		{
			return null;
		}
		return new File(mapSourceDirStr);
	}

	public final boolean isDisplayHelpEnabled()
	{
		return _properties.getBooleanProperty(FS_OPTION_DISPLAY_HELP);
	}

	public final void setOptionDisplayHelp(final boolean p_displayHelpOption)
	{
		final boolean oldValue = isDisplayHelpEnabled();
		if (oldValue == p_displayHelpOption)
		{
			return;
		}

		_properties.setBooleanProperty(FS_OPTION_DISPLAY_HELP, p_displayHelpOption);
		_isModified = true;
	}

	/**
	 * Set the default skin folder.
	 * 
	 * @param p_userSkinDir User skin folder.
	 */
	public final void setDefaultSkin(final File p_userSkinDir)
	{
		final String strOldSkinDir = _properties.getProperty(FS_DEFAULT_SKIN);

		if (p_userSkinDir == null && "".equals(strOldSkinDir))
		{
			return;
		} else if (p_userSkinDir == null)
		{
			_properties.setProperty(FS_DEFAULT_SKIN, "");
		} else
		{
			final String strUserSkinDir = FileMgr.getRelativePath(p_userSkinDir);
			if (strOldSkinDir.equals(strUserSkinDir))
			{
				return;
			}
			_properties.setProperty(FS_DEFAULT_SKIN, strUserSkinDir);
		}

		_isModified = true;
	}

	/**
	 * Return the user skin folder.
	 * 
	 * @return folder.
	 */
	public final File getDefaultSkin()
	{
		final String userSkinDirStr = getMandatoryValue(FS_DEFAULT_SKIN);
		if ("".equals(userSkinDirStr) || userSkinDirStr == null)
		{
			return null;
		}
		final File userSkinDir = FileMgr.getFileFromRelativPath(userSkinDirStr);
		return userSkinDir;
	}

	public final void setFSLogsDir(final File p_logsDir)
	{
		Assert.preconditionNotNull(p_logsDir, "Logs dir");
		Assert.preconditionIsDir(p_logsDir);

		_properties.setProperty(FS_LOGS_DIR, p_logsDir.getAbsolutePath());
		_isModified = true;
	}

	public final File getFSLogsDir()
	{
		final String strLogsDir = getValue(FS_LOGS_DIR);
		return new File(strLogsDir);
	}

	public final void setFSLogsActivation(final boolean p_fogsActivationFlag)
	{
		_properties.setBooleanProperty(FS_LOGS_ACTIVATION, p_fogsActivationFlag);
		_isModified = true;
	}

	public final boolean getFSLogsActivation()
	{
		return _properties.getBooleanProperty(FS_LOGS_ACTIVATION);
	}

	/**
	 * Set the default skin directory
	 * 
	 * @param p_skinDir Skin directory.
	 */
	public final void setFSSkinDir(final File p_skinDir)
	{
		Assert.preconditionNotNull(p_skinDir, "Skin Dir");
		Assert.preconditionIsDir(p_skinDir);
		final File oldDefaultSkinDir = getFSSkinDir();
		if (!oldDefaultSkinDir.equals(p_skinDir))
		{
			final String strSkinDir = FileMgr.getRelativePath(p_skinDir);
			_properties.setProperty(FS_SKIN_DIR, strSkinDir);
			_isModified = true;
		}
	}

	/**
	 * Retrieve the default skin dir.
	 * 
	 * @return Skin dir.
	 */
	public final File getFSSkinDir()
	{
		final String skinDirStr = getValue(FS_SKIN_DIR);
		final File skinDir = FileMgr.getFileFromRelativPath(skinDirStr);
		return skinDir;
	}

	/**
	 * Ajuste l'emplacement par d�faut contenant les cartes FS.
	 * 
	 * @param p_mapSourceDir R�pertoire par d�faut.
	 */
	public final void setGameDir(final File p_savedGameDir)
	{
		Assert.preconditionNotNull(p_savedGameDir, "Saved Game Dir");
		Assert.preconditionIsDir(p_savedGameDir);
		final File oldDefaultGameDir = getGameDir();
		if (!p_savedGameDir.equals(oldDefaultGameDir))
		{
			_properties.setProperty(FS_GAME_DIR, p_savedGameDir.getAbsolutePath());
			_isModified = true;
		}
	}

	/**
	 * Retourne l'emplacement par d�faut o� se trouve les cartes FS.
	 * 
	 * @return R�pertoire par d�faut.
	 */
	public final File getGameDir()
	{
		final String savedGameDirStr = getValue(FS_GAME_DIR);
		if (savedGameDirStr == null)
		{
			return null;
		}
		return new File(savedGameDirStr);
	}

	public final boolean save() throws IOException
	{
		if (_isSaveDisabled)
		{
			throw new UnsupportedOperationException("Properties file loaded from an URL can't be saved. The method 'saveAs' must be used.");
		}
		if (!_isModified)
		{
			return false;
		}
		final File propertiesFile = new File(_sourceFile);
		FileMgr.forceDeleteIfPresent(propertiesFile);

		_properties.store(propertiesFile, null);
		_isModified = false;
		return true;
	}

	public final void saveAs(final File p_propertiesFile) throws IOException
	{
		Assert.preconditionNotNull(p_propertiesFile, "Properties File");
		Assert.preconditionIsNotDir(p_propertiesFile);

		_properties.store(p_propertiesFile, null);
		_sourceFile = p_propertiesFile.getAbsolutePath();
		_isModified = false;
		_isSaveDisabled = false;
	}

	public final void setLastGameList(final ArrayList<String> p_lastGameList)
	{
		Assert.preconditionNotNull(p_lastGameList, "Last Game List");
		_properties.updateSequentialListForProperty(FS_OPTION_LAST_GAME_LIST, p_lastGameList);
		_isModified = true;
	}

	/**
	 * Return the most recently game used by the user.
	 * 
	 * @return Game list.
	 */
	public final ArrayList<String> getLastGameList()
	{
		if (_properties.isSequentialListExist(FS_OPTION_LAST_GAME_LIST))
		{
			return _properties.getSequentialListForProperty(FS_OPTION_LAST_GAME_LIST);
		}
		return new ArrayList<String>();
	}

	public final void updateLastGameList(final ArrayList<String> p_lastGameList)
	{
		Assert.preconditionNotNull(p_lastGameList, "Last Game List");

		_properties.updateSequentialListForProperty(FS_OPTION_LAST_GAME_LIST, p_lastGameList);
		_isModified = true;
	}

	public final boolean isDefaultAutoSave()
	{
		boolean autoSave = _properties.getBooleanProperty(FS_OPTION_AUTO_SAVE);
		return autoSave;
	}

	public final boolean isTurnSaveMode()
	{
		boolean autoSave = _properties.getBooleanProperty(FS_OPTION_TURN_SAVE_MODE);
		return autoSave;
	}
	
	public final void setDefaultAutoSave(final boolean p_autoSave)
	{

		_properties.setBooleanProperty(FS_OPTION_AUTO_SAVE, p_autoSave);
		_isModified = true;
	}

	public final String getFSVersion()
	{
		return _properties.getProperty(FS_FIRST_STRIKE_VERSION);
	}

	public final ArrayList getFSEvolutionDetails()
	{
		ArrayList<String> evolutionList = new ArrayList<String>();
		evolutionList.add("3.0.0    (2018-DEC-01)");
		evolutionList.add("-   Adding of Artillery & Battleship units");
		evolutionList.add("-   Adding of Radar Station (only for D-Day Scenario)");
		evolutionList.add("-   Dynamic Scenario");
		evolutionList.add("-   Deep review of the Dynamic panels");
		evolutionList.add("-   IA Computer improvements");
		evolutionList.add("-   Progress indicator & feedback improvements");
		evolutionList.add("-   Help refresh");
		evolutionList.add("2.2.0    (2014-FEB-01)");
		evolutionList.add("-   Scenario & IA improvements");
		evolutionList.add("-   Installation Script for Windows");
		evolutionList.add("2.1.0    (2011-MAR-01)");
		evolutionList.add("-   Internationalization (English & French)");
		evolutionList.add("-   Bug fixing");
		evolutionList.add("-   Adding of Drag and Drop into the Battle board");
		evolutionList.add("-   Adding of the Vertical tool bar");
		evolutionList.add("-   Improvement of the default skin");
		evolutionList.add("2.0.0    (2010-MAR-01)");
		evolutionList.add("-   Significant refactoring of the engine");
		evolutionList.add("-   Significant refactoring of the interface");
		evolutionList.add("-   New unit type \\: Transport Ship, Destroyer");
		evolutionList.add("-   Skin management");
		evolutionList.add("1.2.2    (2007-OCT-15)");
		evolutionList.add("-   Bug fixing within the First Strike engine");
		evolutionList.add("1.2.1    (2007-OCT-14)");
		evolutionList.add("-   Game title in the Windows's Title Bar");
		evolutionList.add("-   On loading, set automatically the cursor's position");
		evolutionList.add("-   About Popup");
		evolutionList.add("-   Bug fixing within the engine");
		evolutionList.add("-   Bug fixing within the interface");
		evolutionList.add("1.2.0    (2007-SEP-10)");
		evolutionList.add("-   Refactoring of new game Popup");
		evolutionList.add("-   Refactoring of options Popup ");
		evolutionList.add("-   Latest First Strike games management within file menu");
		evolutionList.add("1.1.0    (2007-AUG-26)");
		evolutionList.add("-   Display the current unit on the board");
		evolutionList.add("-   Search City without production button");
		evolutionList.add("-   Hide / show the fog into the board game");
		evolutionList.add("-   Blinking Cursor");
		evolutionList.add("-   Optimization of the double-click");
		evolutionList.add("-   Bug fixing related to map management");
		evolutionList.add("1.0.0    (2007-MAY-21)");

		return evolutionList;
	}

	public final String getFSVersionCompilation()
	{
		return _properties.getProperty(FS_FIRST_STRIKE_DATE_VERSION);
	}

	protected String getMandatoryValue(final String p_propertyName)
	{
		Assert.preconditionNotNull(p_propertyName, "Property Name");

		final String value = _properties.getProperty(p_propertyName);
		if (value == null)
		{
			throw new FSFatalException("Unable to find property '" + p_propertyName + "' in file '" + _sourceFile
					+ "'.");
		}

		return value;
	}

	protected String getValue(final String p_propertyName)
	{
		Assert.preconditionNotNull(p_propertyName, "Property Name");

		if (!_properties.containsKey(p_propertyName))
		{
			return null;
		}
		final String value = _properties.getProperty(p_propertyName);
		return value;
	}

	protected int getMandatoryIntValue(final String aPropertyName)
	{
		final String stringValue = getMandatoryValue(aPropertyName);
		int intValue = 0;
		try
		{
			intValue = Integer.parseInt(stringValue);
		} catch (NumberFormatException ex)
		{
			throw new FSFatalException("Unable to convert to number the value of the property '" + aPropertyName
					+ "' in the file '" + _sourceFile + "'", ex);
		}

		return intValue;
	}

	public boolean isModified()
	{
		return _isModified;
	}

	public boolean isSaveDisabled()
	{
		return _isSaveDisabled;
	}

	public final void addHelpChapterVisited(final ArrayList<String> p_chapterNameList)
	{
		Assert.preconditionNotNull(p_chapterNameList, "Chapter Name List");

		if (p_chapterNameList.isEmpty())
		{
			return;
		}

		_properties.appendStringList(FS_HELP_CHAPTER_LIST, p_chapterNameList);
		_isModified = true;
	}

	public final ArrayList<String> getChapterNameList()
	{
		return _properties.getPropertyAsStringList(FS_HELP_CHAPTER_LIST);
	}

	public final Dimension getScreenSize()
	{
		if (!_properties.containsKey(FS_SCREEN_SIZE_HEIGHT))
		{
			return null;
		}
		if (!_properties.containsKey(FS_SCREEN_SIZE_WIDTH))
		{
			return null;
		}

		final String heightStr = _properties.getProperty(FS_SCREEN_SIZE_HEIGHT);
		final String widthStr = _properties.getProperty(FS_SCREEN_SIZE_WIDTH);
		final int height = Integer.parseInt(heightStr);
		final int width = Integer.parseInt(widthStr);

		return new Dimension(width, height);
	}

	public final void setScreenSize(final Dimension p_dim)
	{
		Assert.preconditionNotNull(p_dim, "Dimension");

		final Dimension oldValue = getScreenSize();

		if (oldValue == null && p_dim != null)
		{
			_isModified = true;
		} else if (oldValue != null && p_dim == null)
		{
			_isModified = true;
		} else if (!oldValue.equals(p_dim))
		{
			_isModified = true;
		}

		if (_isModified)
		{
			_properties.setIntProperty(FS_SCREEN_SIZE_HEIGHT, (int) p_dim.getHeight());
			_properties.setIntProperty(FS_SCREEN_SIZE_WIDTH, (int) p_dim.getWidth());
		}
	}

	public final Point getScreenLocation()
	{
		if (!_properties.containsKey(FS_SCREEN_X_LOCATION))
		{
			return null;
		}
		if (!_properties.containsKey(FS_SCREEN_Y_LOCATION))
		{
			return null;
		}

		final String xStr = _properties.getProperty(FS_SCREEN_X_LOCATION);
		final String yStr = _properties.getProperty(FS_SCREEN_Y_LOCATION);
		if (xStr != null && xStr != null)
		{
			final int x = Integer.parseInt(xStr);
			final int y = Integer.parseInt(yStr);

			return new Point(x, y);
		}
		return null;
	}

	public final void setScreenLocation(final Point p_location)
	{
		Assert.preconditionNotNull(p_location, "location");

		final Point oldValue = getScreenLocation();

		if (oldValue == null && p_location != null)
		{
			_isModified = true;
		} else if (oldValue != null && p_location == null)
		{
			_isModified = true;
		} else if (!oldValue.equals(p_location))
		{
			_isModified = true;
		}

		if (_isModified)
		{
			_properties.setIntProperty(FS_SCREEN_X_LOCATION, (int) p_location.getX());
			_properties.setIntProperty(FS_SCREEN_Y_LOCATION, (int) p_location.getY());
		}
	}

	public final boolean isDebugMode()
	{
		return _properties.getBooleanProperty(FS_DEBUG_MODE);
	}

	public final void setDebugMode(final boolean p_isDebugMode)
	{
		_properties.setBooleanProperty(FS_DEBUG_MODE, p_isDebugMode);
	}
}