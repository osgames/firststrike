/*
 * Created on Dec 18, 2004
 */
package org.jocelyn_chaumel.firststrike.core.area;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class AreaMgr
{
	/**
	 * J'aimerais donner une orange � un amis.
	 */
	private final AreaList _areaList;

	public AreaMgr(final AreaList p_areaList)
	{
		Assert.preconditionNotNull(p_areaList, "Area List");
		Assert.precondition(p_areaList.size() > 0, "Invalid Area List");

		_areaList = p_areaList;
	}

	public int getNbArea()
	{
		return _areaList.size();
	}

	public AreaList getAreaList()
	{
		return _areaList;
	}
}