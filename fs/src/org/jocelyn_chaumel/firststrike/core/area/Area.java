/*
 * Created on Dec 18, 2004
 */
package org.jocelyn_chaumel.firststrike.core.area;

import java.io.Serializable;
import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.CellSet;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.data_type.Id;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class Area implements Serializable
{
	private final AbstractAreaId _areaId;

	private final CellSet _cellSet = new CellSet();
	private final CoordSet _extraEdgeCellSet = new CoordSet();

	private CityList _cityList = null;

	private String _internalAreaName = null;

	public Area(final AbstractAreaId p_areaId)
	{
		Assert.preconditionNotNull(p_areaId, "Area Id");

		_areaId = p_areaId;
	}

	public final CellSet getCellSet()
	{
		return _cellSet;
	}

	public final Id getId()
	{
		return _areaId;
	}

	/**
	 * Calculates and returns the list of cities in this area.
	 * 
	 * @return City Set.
	 */
	public final CityList getCityList()
	{
		if (_cityList == null)
		{
			_cityList = new CityList();
			Cell currentCell = null;
			final Iterator cellIter = _cellSet.iterator();
			while (cellIter.hasNext())
			{
				currentCell = (Cell) cellIter.next();
				if (currentCell.isCity())
				{
					_cityList.add(currentCell.getCity());
				}
			}
		}
		return _cityList;
	}

	public final boolean hasCity()
	{
		final CityList cityList = getCityList();
		return !cityList.isEmpty();
	}
	
	public CoordSet getExtractEdgeCoordSet()
	{
		return _extraEdgeCellSet;
	}

	public CoordSet getGroundCellNearToSea()
	{
		return _cellSet.getGroundCellNearToSea().getCoordSet();
	}

	public final boolean isGroundArea()
	{
		return AreaTypeCst.GROUND_AREA.equals(_areaId.getAreaType());
	}

	public final Coord getFirstCoordOfArea()
	{
		return _cellSet.getCoordSet().getDirectNearestCoord(new Coord(0, 0));
	}

	public final boolean hasAreaName()
	{
		return _internalAreaName != null;
	}

	public final String getInternalAreaName()
	{
		if (_internalAreaName == null)
		{
			final Coord coord = getFirstCoordOfArea();
			return coord.toStringLight();
		}

		return _internalAreaName;
	}

	public final void setInternalAreaName(final String p_areaName)
	{
		_internalAreaName = p_areaName;
	}

	@Override
	public final String toString()
	{
		final StringBuffer sb = new StringBuffer();
		sb.append("{AREA= Id:").append(_areaId);
		sb.append(", name:").append(getInternalAreaName());
		sb.append("}");

		return sb.toString();
	}
}