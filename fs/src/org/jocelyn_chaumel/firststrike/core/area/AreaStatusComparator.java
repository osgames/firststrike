package org.jocelyn_chaumel.firststrike.core.area;

import java.util.ArrayList;
import java.util.Comparator;

import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class AreaStatusComparator implements Comparator<Area>
{

	public final static ArrayList<AreaStatusCst> LOW = new ArrayList<AreaStatusCst>();
	public final static ArrayList<AreaStatusCst> MIDDLE = new ArrayList<AreaStatusCst>();
	public final static ArrayList<AreaStatusCst> HIGH = new ArrayList<AreaStatusCst>();
	public final static ArrayList<AreaStatusCst> VERY_HIGH = new ArrayList<AreaStatusCst>();

	static
	{
		VERY_HIGH.add(AreaStatusCst.NO_CITY_AREA);
		VERY_HIGH.add(AreaStatusCst.TO_CONQUER_AREA);
		HIGH.add(AreaStatusCst.UNDER_CONTROL_AREA);
		LOW.add(AreaStatusCst.OWNED_AREA);
	}
	private final ComputerPlayer _player;

	public AreaStatusComparator(final ComputerPlayer p_player)
	{
		Assert.preconditionNotNull(p_player, "Player");

		_player = p_player;
	}

	@Override
	public int compare(final Area p_o1, final Area p_o2)
	{
		Assert.preconditionNotNull(p_o1, "Area #1");
		Assert.preconditionNotNull(p_o2, "Area #2");
		Assert.precondition(p_o1 instanceof Area, "Invalid instance #1");
		Assert.precondition(p_o2 instanceof Area, "Invalid instance #1");

		final Area area_1 = (Area) p_o1;
		final Area area_2 = (Area) p_o2;
		final AreaStatusCst status_1 = _player.getAreaStatus(area_1);
		final AreaStatusCst status_2 = _player.getAreaStatus(area_2);
		Assert.preconditionNotNull(status_1, "Area #1");
		Assert.preconditionNotNull(status_2, "Area #2");

		final int level_1 = convertToLevel(status_1);
		final int level_2 = convertToLevel(status_2);

		return level_1 - level_2;
	}

	public static int convertToLevel(final AreaStatusCst p_areaStatus)
	{
		Assert.preconditionNotNull(p_areaStatus, "Area Status");
		if (LOW.contains(p_areaStatus))
		{
			return 4;
		} else if (MIDDLE.contains(p_areaStatus))
		{
			return 3;
		} else if (HIGH.contains(p_areaStatus))
		{
			return 2;
		} else if (VERY_HIGH.contains(p_areaStatus))
		{
			return 1;
		}

		throw new IllegalArgumentException("Unsupported Area Status :" + p_areaStatus);
	}

}
