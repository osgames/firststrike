package org.jocelyn_chaumel.firststrike.core.area;

import java.util.Iterator;

import org.jocelyn_chaumel.tools.data_type.AbstractInstanceHashSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class AreaIdSet extends AbstractInstanceHashSet
{
	public AreaIdSet()
	{
		super(AbstractAreaId.class, "Area Id Set");
	}

	/**
	 * Indique si la liste d'Area Id contient belle et bien un Area Id d'un
	 * certain type.
	 * 
	 * @param p_areaType Type d'Area Id recherch�.
	 * 
	 * @return Vrai ou faux.
	 */
	public final boolean isContainingAreaByType(final AreaTypeCst p_areaType)
	{
		Assert.preconditionNotNull(p_areaType, "Area Type");
		Assert.precondition(!AreaTypeCst.ALL_AREAS.equals(p_areaType), "Invalid Area Type");

		AbstractAreaId currentAreaId = null;
		final Iterator iter = iterator();
		while (iter.hasNext())
		{
			currentAreaId = (AbstractAreaId) iter.next();
			if (currentAreaId.getAreaType().equals(p_areaType))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Retourne l'Area Id d'un type particulier de la liste. Cette m�thode
	 * retourne la valeur <code>null</code> si la liste ne contient pas d'Area
	 * Id du type recherch�.
	 * 
	 * @param p_areaType Type d'Area Id recherch�.
	 * 
	 * @return Area Id recherch� ou <code>null</code> si manquant.
	 */
	public final AbstractAreaId getAreaByType(final AreaTypeCst p_areaType)
	{
		Assert.preconditionNotNull(p_areaType, "Area Type");
		Assert.precondition(!AreaTypeCst.ALL_AREAS.equals(p_areaType), "Invalid Area Type");

		AbstractAreaId currentAreaId = null;
		final Iterator iter = iterator();
		while (iter.hasNext())
		{
			currentAreaId = (AbstractAreaId) iter.next();
			if (currentAreaId.getAreaType().equals(p_areaType))
			{
				return currentAreaId;
			}
		}
		return null;
	}
}
