package org.jocelyn_chaumel.firststrike.core.area;

public class RiverAreaId extends AbstractAreaId
{
	public RiverAreaId(final int p_id)
	{
		super(p_id, AreaTypeCst.RIVER_AREA);
	}
}
