package org.jocelyn_chaumel.firststrike.core.area;

import java.util.Comparator;

public class AreaNameComparator implements Comparator<Area>
{

	@Override
	public int compare(Area p_area1, Area p_area2)
	{
		final String areaName1 = p_area1.getInternalAreaName();
		final String areaName2 = p_area2.getInternalAreaName();

		return areaName1.compareTo(areaName2);
	}

}
