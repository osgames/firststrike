package org.jocelyn_chaumel.firststrike.core.area;

public class MountainAreaId extends AbstractAreaId
{
	public MountainAreaId(final int p_id)
	{
		super(p_id, AreaTypeCst.MOUNTAIN_AREA);
	}
}
