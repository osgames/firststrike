/*
 * Created on Dec 18, 2004
 */
package org.jocelyn_chaumel.firststrike.core.area;

import org.jocelyn_chaumel.tools.data_type.Id;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class AbstractAreaId extends Id
{
	private final AreaTypeCst _areaType;

	public AbstractAreaId(final int p_areaId, final AreaTypeCst p_areaType)
	{
		super(p_areaId);
		Assert.preconditionNotNull(p_areaType, "Area Type");
		Assert.precondition(!p_areaType.equals(AreaTypeCst.ALL_AREAS), "Invalid Area Type");

		_areaType = p_areaType;
	}

	public final AreaTypeCst getAreaType()
	{
		return _areaType;
	}
}