package org.jocelyn_chaumel.firststrike.core.area;

public class GroundAreaId extends AbstractAreaId
{
	public GroundAreaId(final int p_id)
	{
		super(p_id, AreaTypeCst.GROUND_AREA);
	}
}
