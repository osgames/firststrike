package org.jocelyn_chaumel.firststrike.core.area;

import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.data_type.cst.IntCst;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;

public class AreaTypeCst extends IntCst
{
	public final static AreaTypeCst GROUND_AREA = new AreaTypeCst(1, "Ground Area");
	public final static AreaTypeCst MOUNTAIN_AREA = new AreaTypeCst(2, "Mountain Area");
	public final static AreaTypeCst SEA_AREA = new AreaTypeCst(3, "Sea Area");
	public final static AreaTypeCst RIVER_AREA = new AreaTypeCst(4, "River Area");
	public final static AreaTypeCst ALL_AREAS = new AreaTypeCst(5, "All Areas");

	private final static AreaTypeCst[] _areaTypeCstList = { SEA_AREA, GROUND_AREA, MOUNTAIN_AREA, RIVER_AREA };

	private AreaTypeCst(int p_value, String p_label)
	{
		super(p_value, p_label);
	}


	/**
	 * Retourne l'instante d'une constante � partir d'une valeur.
	 * 
	 * @param p_value Valeur de la constante.
	 * @return Instance de la constante.
	 * @throws InvalidConstantValueException
	 */
	public static AreaTypeCst getInstance(final int p_value) throws InvalidConstantValueException
	{
		return (AreaTypeCst) IntCst.getInstance(p_value, _areaTypeCstList, AreaTypeCst.class);
	}

	/**
	 * Retourne l'instance d'une constante � partir d'un type de cellule.
	 * 
	 * @param p_cellTypeCst Type de cellule.
	 * @return Instance de la constante.
	 * @throws InvalidConstantValueException
	 */
	public static AreaTypeCst getInstanceFromCellType(final CellTypeCst p_cellTypeCst) throws InvalidConstantValueException
	{

		AreaTypeCst areaTypeCst = null;
		if (p_cellTypeCst == CellTypeCst.SEA)
		{
			areaTypeCst = SEA_AREA;
		} else if (p_cellTypeCst == CellTypeCst.GRASSLAND || p_cellTypeCst == CellTypeCst.LIGHT_WOOD
				|| p_cellTypeCst == CellTypeCst.HEAVY_WOOD || p_cellTypeCst == CellTypeCst.HILL
				|| p_cellTypeCst == CellTypeCst.CITY || p_cellTypeCst == CellTypeCst.ROAD)
		{
			areaTypeCst = GROUND_AREA;
		} else if (p_cellTypeCst == CellTypeCst.MOUNTAIN)
		{
			areaTypeCst = MOUNTAIN_AREA;
		} else if (p_cellTypeCst == CellTypeCst.RIVER)
		{
			areaTypeCst = RIVER_AREA;
		} else
		{
			throw new InvalidConstantValueException(AreaTypeCst.class.getName(), p_cellTypeCst);
		}

		return areaTypeCst;
	}

}
