package org.jocelyn_chaumel.firststrike.core.area;

import org.jocelyn_chaumel.tools.data_type.cst.IntCst;

public class AreaStatusCst extends IntCst
{
	/**
	 * R�gion enti�rement conquise.
	 */
	public final static AreaStatusCst OWNED_AREA = new AreaStatusCst(1, "Owned Area");

	/**
	 * R�gion majoritairement contr�l�e (plus de 50%).
	 */
	public final static AreaStatusCst UNDER_CONTROL_AREA = new AreaStatusCst(3, "Under Control Area");

	/**
	 * R�gion que est partiellement contr�l�e (moins de 50%) ou avec une ville ennemie.
	 */
	public final static AreaStatusCst TO_CONQUER_AREA = new AreaStatusCst(4, "To Conquer Area");

	/**
	 * R�gion sans ville.
	 */
	public final static AreaStatusCst NO_CITY_AREA = new AreaStatusCst(5, "No City Area");

	protected AreaStatusCst(int p_value, final String p_name)
	{
		super(p_value, p_name);
	}

}
