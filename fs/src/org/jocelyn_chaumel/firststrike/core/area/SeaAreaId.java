package org.jocelyn_chaumel.firststrike.core.area;

public class SeaAreaId extends AbstractAreaId
{
	public SeaAreaId(final int p_id)
	{
		super(p_id, AreaTypeCst.SEA_AREA);
	}
}
