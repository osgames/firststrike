/*
 * Created on Aug 27, 2005
 */
package org.jocelyn_chaumel.firststrike.core.area;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.tools.data_type.FSUtil;
import org.jocelyn_chaumel.tools.data_type.Id;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class AreaList extends ArrayList<Area>
{

	private static final String INTERNAL_NAME = "AreaList";

	/**
	 * Constructeur par d�faut.
	 */
	public AreaList()
	{
	}

	/**
	 * Return the first area of the expected type (input parameter).
	 * 
	 * @param p_areaType Expected area type.
	 * @return an Area or null.
	 */
	public final Area getFirstAreaByType(final AreaTypeCst p_areaType)
	{
		Assert.preconditionNotNull(p_areaType, "Area Type");
		Assert.precondition(!p_areaType.equals(AreaTypeCst.ALL_AREAS), "Invalid Area Type");

		AbstractAreaId currentAreaId = null;
		Area currentArea = null;
		final Iterator iter = iterator();
		while (iter.hasNext())
		{
			currentArea = (Area) iter.next();
			currentAreaId = (AbstractAreaId) currentArea.getId();
			if (currentAreaId.getAreaType().equals(p_areaType))
			{
				return currentArea;
			}
		}
		return null;
	}

	/**
	 * Returns all ground area in a new Area Set.
	 * 
	 * @return Ground Area Set.
	 */
	public final AreaList getAllGroundAreaSet()
	{
		final AreaList groundAreaSet = new AreaList();
		Area currentArea = null;
		final Iterator iter = iterator();
		while (iter.hasNext())
		{
			currentArea = (Area) iter.next();
			if (currentArea.isGroundArea())
			{
				groundAreaSet.add(currentArea);
			}
		}

		return groundAreaSet;
	}

	/**
	 * Returns all ground area containing at least one city into a new Area Set.
	 * 
	 * @return Ground Area Set.
	 */
	public final AreaList getAllGroundAreaSetWithCity()
	{
		final AreaList groundAreaSet = new AreaList();
		Area currentArea = null;
		final Iterator iter = iterator();
		while (iter.hasNext())
		{
			currentArea = (Area) iter.next();
			if (currentArea.isGroundArea() && currentArea.hasCity())
			{
				groundAreaSet.add(currentArea);
			}
		}

		return groundAreaSet;

	}

	/**
	 * Return a Set collection containing the AbstractAreaId of each area.
	 * 
	 * @return Abstract Area Id Set
	 */
	public AreaIdSet getIdSet()
	{
		final AreaIdSet idSet = new AreaIdSet();
		Area currentArea = null;
		final Iterator areaIter = iterator();
		while (areaIter.hasNext())
		{
			currentArea = (Area) areaIter.next();
			idSet.add(currentArea.getId());
		}

		return idSet;
	}
	
	public boolean containsArea(final Id p_id)
	{
		return getIdSet().contains(p_id);
	}

	/**
	 * Returns all area with OwnedArea Status.
	 * 
	 * @return AreaSet of owned area.
	 */
	public final AreaList getOwnedArea(ComputerPlayer p_player)
	{
		final AreaList ownedAreaList = new AreaList();
		Area currentArea = null;
		AreaStatusCst currentStatus = null;
		final Iterator areaInfoIter = iterator();
		while (areaInfoIter.hasNext())
		{
			currentArea = (Area) areaInfoIter.next();
			currentStatus = p_player.getAreaStatus(currentArea);
			if (currentStatus == AreaStatusCst.OWNED_AREA)
			{
				ownedAreaList.add(currentArea);
			}
		}

		return ownedAreaList;
	}

	/**
	 * Retrieve all the area with the same priority level.
	 * 
	 * @return Most prior areas.
	 */
	public final AreaList getFirstGroupElement(final ComputerPlayer p_player)
	{
		final AreaList groupedAreaSet = new AreaList();

		// Sort the areas in a new ArrayList
		final ArrayList<Area> sortedAreaList = new ArrayList<Area>();
		sortedAreaList.addAll(this);
		Collections.sort(sortedAreaList, new AreaStatusComparator(p_player));

		// Get the first Area & identify group's level
		final Area firstArea = (Area) sortedAreaList.remove(0);
		groupedAreaSet.add(firstArea);
		final int firstLevel = AreaStatusComparator.convertToLevel(p_player.getAreaStatus(firstArea));
		Area currentArea = null;
		int currentLevel = 0;

		// For each area
		final Iterator iter = sortedAreaList.iterator();
		while (iter.hasNext())
		{
			currentArea = (Area) iter.next();

			// Verify if currentArea's level == first level
			currentLevel = AreaStatusComparator.convertToLevel(p_player.getAreaStatus(currentArea));
			if (currentLevel == firstLevel)
			{
				groupedAreaSet.add(currentArea);
			} else
			{
				break;
			}
		}

		return groupedAreaSet;
	}

	/**
	 * Return in first the area different than SEA Area.
	 * 
	 * @return an Area
	 */
	public final Area getPrincipalArea()
	{
		Area result = null;
		result = getFirstAreaByType(AreaTypeCst.GROUND_AREA);
		if (result != null)
		{
			return result;
		}

		result = getFirstAreaByType(AreaTypeCst.MOUNTAIN_AREA);
		if (result != null)
		{
			return result;
		}

		result = getFirstAreaByType(AreaTypeCst.RIVER_AREA);
		if (result != null)
		{
			return result;
		}

		result = getFirstAreaByType(AreaTypeCst.SEA_AREA);
		if (result != null)
		{
			return result;
		}

		return result;
	}

	public final void sortListByName()
	{
		Collections.sort(this, new AreaNameComparator());
	}

	@Override
	public final String toString()
	{
		return FSUtil.toStringList(INTERNAL_NAME, this);
	}
}
