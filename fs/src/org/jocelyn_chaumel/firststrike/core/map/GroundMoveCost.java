/*
 * Created on May 1, 2005
 */
package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class GroundMoveCost extends AbstractMoveCost
{
	private final static short VERY_LIGHT = 25;
	private final static short LIGHT = 33;
	private final static short MEDIUM = 50;
	private final static short HEAVY = 75;

	/**
	 * Constructeur par d�faut.
	 * 
	 * @param p_uniqueId
	 *            Idenfiant unique.
	 * @see AbstractMoveCost#AbstractMoveCost(int, AreaTypeCst)
	 */
	public GroundMoveCost(final int p_uniqueId)
	{
		super(p_uniqueId, AreaTypeCst.GROUND_AREA);
	}

	@Override
	public int getCost(final Cell p_cell)
	{
		Assert.preconditionNotNull(p_cell, "Cell");

		final CellTypeCst cellType = p_cell.getCellType();
		return getCost(cellType);
	}

	public int getCost(final CellTypeCst p_cellType)
	{
		Assert.preconditionNotNull(p_cellType, "Cell Type");

		if (CellTypeCst.SEA.equals(p_cellType) || CellTypeCst.MOUNTAIN.equals(p_cellType)
				|| CellTypeCst.RIVER.equals(p_cellType))
		{
			return UNREACHABLE_CELL;
		} else if (CellTypeCst.CITY.equals(p_cellType) || CellTypeCst.ROAD.equals(p_cellType))
		{
			return VERY_LIGHT;
		} else if (CellTypeCst.GRASSLAND.equals(p_cellType))
		{
			return LIGHT;
		} else if (CellTypeCst.LIGHT_WOOD.equals(p_cellType))
		{
			return MEDIUM;
		} else if (CellTypeCst.HEAVY_WOOD.equals(p_cellType) || CellTypeCst.HILL.equals(p_cellType))
		{
			return HEAVY;
		} else
		{
			throw new IllegalArgumentException("Unsupported Cell Type");
		}
	}

	@Override
	public final int getMaxCost()
	{
		return HEAVY;
	}

	@Override
	public int getMinCost()
	{
		
		return VERY_LIGHT;
	}
	
}
