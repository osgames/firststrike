package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class MoveCostFactory
{
	public final static GroundMoveCost GROUND_MOVE_COST = new GroundMoveCost(1);

	public final static AirPlaneMoveCost AIR_MOVE_COST = new AirPlaneMoveCost(2);

	public final static DestroyerMoveCost DESTROYER_MOVE_COST = new DestroyerMoveCost(3);

	public final static BattleshipMoveCost BATTLESHIP_MOVE_COST = new BattleshipMoveCost(4);

	public final static ArtilleryMoveCost ARTILLERY_MOVE_COST = new ArtilleryMoveCost(5);

	public final static AbstractMoveCost getMoveCostInstance(final UnitTypeCst p_unitType)
	{
		Assert.preconditionNotNull(p_unitType, "Unit Type");

		if (UnitTypeCst.TANK.equals(p_unitType) || UnitTypeCst.RADAR.equals(p_unitType))
		{
			return GROUND_MOVE_COST;
		} else if (UnitTypeCst.FIGHTER.equals(p_unitType))
		{
			return AIR_MOVE_COST;
		} else if (UnitTypeCst.DESTROYER.equals(p_unitType))
		{
			return DESTROYER_MOVE_COST;
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_unitType) || UnitTypeCst.BATTLESHIP.equals(p_unitType))
		{
			return BATTLESHIP_MOVE_COST;
		} else if (UnitTypeCst.ARTILLERY.equals(p_unitType))
		{
			return ARTILLERY_MOVE_COST;
		} else
		{
			throw new IllegalArgumentException("Unsupported Unit type");
		}
	}
}
