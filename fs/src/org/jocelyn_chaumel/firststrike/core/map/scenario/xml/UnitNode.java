package org.jocelyn_chaumel.firststrike.core.map.scenario.xml;

import java.util.ArrayList;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.DataConversionException;
import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioDirective;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioUnit;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioUnitList;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public final class UnitNode extends AbstractNode
{
	public static final String NODE_NAME = "unit";
	public static final String CONTAINED_UNIT_NODE_NAME = "containedUnit";

	public static ScenarioUnitList getUnitList(final Element p_unitNode) throws InvalidScenarioException
	{
		Assert.preconditionNotNull(p_unitNode, "Unit Node");
		final ScenarioUnitList unitList = new ScenarioUnitList();

		final int x = getMandatoryIntAttributeValue(p_unitNode, "x");
		final int y = getMandatoryIntAttributeValue(p_unitNode, "y");
		String reference = p_unitNode.getAttributeValue(AbstractNode.NODE_REFERENCE);
		final int[] turn;
		if (reference == null)
		{
			reference = "";
			turn = getMandatoryIntArrayAttributeValue(p_unitNode, "turn");
		} else 
		{
			turn = new int []{Integer.MAX_VALUE};
		}

		final Coord position = new Coord(x, y);
		final String type = getMandatoryAttributeValue(p_unitNode, "type");
		if (!ScenarioUnit.TANK.equals(type) && !ScenarioUnit.FIGHTER.equals(type)
				&& !ScenarioUnit.TRANSPORT_SHIP.equals(type) && !ScenarioUnit.DESTROYER.equals(type)
				&& !ScenarioUnit.ARTILLERY.equals(type) && !ScenarioUnit.BATTLESHIP.equals(type) && !ScenarioUnit.RADAR.equals(type))
		{
			throw new InvalidScenarioException("Unsupported Unit Type: " + type);
		}
		final Attribute moveAttribute = p_unitNode.getAttribute("move");
		final int move;
		if (moveAttribute != null)
		{
			try
			{
				move = moveAttribute.getIntValue();
			} catch (DataConversionException ex)
			{
				throw new InvalidScenarioException("Invalid move value detected: " + moveAttribute.getValue(), ex);
			}
		} else
		{
			move = ScenarioUnit.DEFAULT_MOVE_VALUE;
		}

		ScenarioUnitList containedUnitList = retrievedContainedUnit(position, p_unitNode);
		ArrayList<ScenarioDirective> directiveList = retrieveDirectiveList(p_unitNode);
		for (int i = 0; i < turn.length; i++)
		{
			if (turn[i] <= -1)
			{
				throw new InvalidScenarioException("Invalid turn value detected: " + turn[i]);
			}

			final ScenarioUnit unit = new ScenarioUnit(reference, type, position, turn[i], move);
			unit.addAllDirectives(directiveList);
			if (ScenarioUnit.TRANSPORT_SHIP.equals(type) && containedUnitList.size() > 0)
			{
				unit.addAllContainedUnit(containedUnitList);
			}

			unitList.add(unit);

		}
		return unitList;
	}

	private static ScenarioUnitList retrievedContainedUnit(final Coord p_position, final Element p_unitNode) throws InvalidScenarioException
	{
		final List<Element> containedUnitNodeList = p_unitNode.getChildren(CONTAINED_UNIT_NODE_NAME);
		final ScenarioUnitList containedUnitnList = new ScenarioUnitList();
		if (containedUnitNodeList.isEmpty())
		{
			return containedUnitnList;
		}

		ScenarioUnit unit = null;
		for (Element unitNode : containedUnitNodeList)
		{
			Assert.precondition(ScenarioUnit.TANK.equals(unitNode.getAttributeValue("type")),
								"Invalid contained unit type");
			unit = new ScenarioUnit("", ScenarioUnit.TANK, p_position, 1, ScenarioUnit.DEFAULT_MOVE_VALUE);
			unit.addAllDirectives(retrieveDirectiveList(unitNode));
			containedUnitnList.add(unit);
		}

		return containedUnitnList;
	}

	private static ArrayList<ScenarioDirective> retrieveDirectiveList(final Element p_unitNode) throws InvalidScenarioException
	{
		final List<Element> directiveNodeList = p_unitNode.getChildren(DirectiveNode.NODE_NAME);
		final ArrayList<ScenarioDirective> directiveList = new ArrayList<ScenarioDirective>();
		for (Element directiveNode : directiveNodeList)
		{
			directiveList.add(DirectiveNode.getScenarioDirective(directiveNode));
		}

		return directiveList;

	}
}
