package org.jocelyn_chaumel.firststrike.core.map.scenario.xml;

import java.util.ArrayList;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.MapScenario;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioPlayer;
import org.jocelyn_chaumel.firststrike.data_type.I18NMessage;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class ScenarioNode extends AbstractNode
{
	public final static String NODE_NAME = "scenario";
	private final static String ATTRIB_NB_ROW = "nbRow";

	public static MapScenario getScenario(final Element p_mapScenarioNode) throws InvalidScenarioException
	{
		Assert.preconditionNotNull(p_mapScenarioNode, "Map Scenario Node");
		Assert.precondition(NODE_NAME.equals(p_mapScenarioNode.getName()), "Invalid scenario node name");

		final String name = p_mapScenarioNode.getAttributeValue("name");
		final Element descriptionNode = AbstractNode.getMandatoryChildNode(p_mapScenarioNode, "description");
		final String desc = new I18NMessage(descriptionNode).getI18NText();

		final int nbRow = AbstractNode.getMandatoryIntAttributeValue(descriptionNode, ATTRIB_NB_ROW);
		final MapScenario mapScenario = new MapScenario(name, desc, nbRow);
		final Element playersNode = p_mapScenarioNode.getChild(PlayersNode.NODE_NAME);
		final ArrayList<ScenarioPlayer> playerList = PlayersNode.getPlayerList(playersNode);
		for (ScenarioPlayer player : playerList)
		{
			mapScenario.addPlayer(player);
		}

		return mapScenario;
	}
}
