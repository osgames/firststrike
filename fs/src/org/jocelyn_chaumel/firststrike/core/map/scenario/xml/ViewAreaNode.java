package org.jocelyn_chaumel.firststrike.core.map.scenario.xml;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioViewAreaList;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioViewArea;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class ViewAreaNode extends AbstractNode
{
	public static ScenarioViewAreaList getViewAreaList(final Element p_messageNode) throws InvalidScenarioException
	{
		final int[] turn = AbstractNode.getMandatoryIntArrayAttributeValue(p_messageNode, "turn");
		final int x = AbstractNode.getMandatoryIntAttributeValue(p_messageNode, "x");
		final int y = AbstractNode.getMandatoryIntAttributeValue(p_messageNode, "y");
		final int height = AbstractNode.getMandatoryIntAttributeValue(p_messageNode, "height");
		final int width = AbstractNode.getMandatoryIntAttributeValue(p_messageNode, "width");
		final boolean unitDetection = AbstractNode.getBooleanAttributeValue(p_messageNode, "unit-detection");

		final int wait;
		if (p_messageNode.getAttribute("wait") != null)
		{
			wait = AbstractNode.getMandatoryIntAttributeValue(p_messageNode, "wait");
		} else
		{
			wait = 0;
		}
		
		final ScenarioViewAreaList eventList = new ScenarioViewAreaList();
		for (int i = 0; i < turn.length; i++)
		{
			eventList.add(new ScenarioViewArea(new Coord(x, y), height, width, turn[i], wait, unitDetection));
		}
		return eventList;
	}
}
