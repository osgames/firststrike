package org.jocelyn_chaumel.firststrike.core.map.scenario;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithCombatDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithoutCombatDirective;
import org.jocelyn_chaumel.firststrike.core.directive.artillery.ArtilleryAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.artillery.ArtilleryDefendBombardmentDirective;
import org.jocelyn_chaumel.firststrike.core.directive.battleship.BattleshipAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.destroyer.DestroyerAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.fighter.FighterAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.tank.TankAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.tank.TankProtectArtilleryDirective;
import org.jocelyn_chaumel.firststrike.core.directive.tank.TankProtectPositionDirective;
import org.jocelyn_chaumel.firststrike.core.directive.transportship.TransportShipAutoDirective;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class ScenarioDirective implements Serializable
{
	public static final String AUTO = "auto";
	public static final String MOVE_WITH_COMBAT = "moveWithCombat";
	public static final String MOVE_WITHOUT_COMBAT = "moveWithoutCombat";
	public static final String TANK_PROTECT_POSITION = "tankProtectPosition";
	public static final String ARTILLERY_PROTECT_POSITION = "artilleryProtectPosition";
	public static final String TANK_PROTECT_ARTILLERY = "tankProtectArtillery";

	private final String _type;
	private Coord _originCoord;
	private Coord _destination;

	public ScenarioDirective(final String p_type)
	{
		Assert.preconditionNotNull(p_type, "Type");

		_type = p_type;
	}

	public final String getType()
	{
		return _type;
	}

	public final void setOriginCoord(final Coord p_originCoord)
	{
		_originCoord = p_originCoord;
	}

	public final boolean hasOriginCoord()
	{
		return _originCoord != null;
	}

	public final Coord getOriginCoord()
	{
		return _originCoord;
	}

	public final void setDestination(final Coord p_destination)
	{
		_destination = p_destination;
	}

	public final boolean hasDestination()
	{
		return _destination != null;
	}

	public final Coord getDestination()
	{
		return _destination;
	}

	public static AbstractUnitDirective createDirective(final ScenarioDirective p_directive,
														final AbstractUnit p_unit,
														final FSMapMgr p_mapMgr) throws InvalidScenarioException
	{

		Assert.preconditionNotNull(p_directive, "Directive");
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.preconditionNotNull(p_mapMgr, "Map Mgr");

		if (AUTO.equals(p_directive._type))
		{
			if (p_unit instanceof ComputerTank)
			{
				return new TankAutoDirective((ComputerTank) p_unit);
			} else if (p_unit instanceof Fighter)
			{
				return new FighterAutoDirective((Fighter) p_unit);
			} else if (p_unit instanceof TransportShip)
			{
				final Coord areaPosition = p_directive.getOriginCoord();
				final Area area = p_mapMgr.getAreaAt(areaPosition, AreaTypeCst.GROUND_AREA);
				Assert.check(area != null, "No ground area found here : " + areaPosition);

				return new TransportShipAutoDirective((ComputerTransportShip) p_unit, area);
			} else if (p_unit instanceof Destroyer)
			{
				return new DestroyerAutoDirective((Destroyer) p_unit);
			} else if (p_unit instanceof Artillery)
			{
				return new ArtilleryAutoDirective((Artillery) p_unit);
			} else if (p_unit instanceof Battleship)
			{
				return new BattleshipAutoDirective((Battleship) p_unit);
			} else {
				throw new FSFatalException("Unsupported unit type for auto directive");
			}
		}
		
		if (MOVE_WITH_COMBAT.equals(p_directive._type))
		{
			return new FindPathAndMoveWithCombatDirective(p_unit, p_directive._destination, false);
		} else if (MOVE_WITHOUT_COMBAT.equals(p_directive._type))
		{
			return new FindPathAndMoveWithoutCombatDirective(p_unit, p_directive._destination, false, false);
		} else if (TANK_PROTECT_POSITION.equals(p_directive._type))
		{
			final Coord destinationCoord;
			if (p_directive.hasDestination())
			{
				destinationCoord = p_directive.getDestination();
			} else {
				destinationCoord = p_unit.getPosition();
			}
			return new TankProtectPositionDirective(p_unit, destinationCoord);
		} else if (ARTILLERY_PROTECT_POSITION.equals(p_directive._type))
		{
			final Coord destinationCoord;
			if (p_directive.hasDestination())
			{
				destinationCoord = p_directive.getDestination();
			} else {
				destinationCoord = p_unit.getPosition();
			}
			return new ArtilleryDefendBombardmentDirective((Artillery) p_unit, destinationCoord);
		} else if (TANK_PROTECT_ARTILLERY.equals(p_directive._type))
		{
			return new TankProtectArtilleryDirective((ComputerTank) p_unit);
		}
		
		throw new FSFatalException("Unsupported directive");
	}
}
