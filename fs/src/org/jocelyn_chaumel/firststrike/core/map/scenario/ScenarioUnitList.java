package org.jocelyn_chaumel.firststrike.core.map.scenario;

import java.util.ArrayList;

public class ScenarioUnitList extends ArrayList<ScenarioUnit>
{
	public ScenarioUnitList()
	{
		// NOP
	}

	public ScenarioUnitList(ArrayList<ScenarioUnit> p_scenarioUnitList)
	{
		super();
		this.addAll(p_scenarioUnitList);
	}

	public final ScenarioUnitList removeUnitByTurn(final int p_turn)
	{

		final ScenarioUnitList scenarioUnitList = new ScenarioUnitList();
		ScenarioUnit currentUnit = null;
		for (int i = this.size() - 1; i >= 0; i--)
		{
			currentUnit = this.get(i);
			if (currentUnit.getTurn() == p_turn)
			{
				this.remove(i);
				scenarioUnitList.add(currentUnit);
			}
		}

		return scenarioUnitList;
	}

	public final void replaceReferenceByTurn(final String p_reference, final int p_turn)
	{
		String reference;
		for (ScenarioUnit unit : this)
		{
			reference = unit.getReference();
			if (reference != null && reference.equalsIgnoreCase(p_reference))
			{
				unit.setTurn(p_turn);
			}
		}

	}

	public final void replaceReferenceByTurns(final String p_reference, final int[] p_turnArray)
	{
		final ScenarioUnitList tempList = new ScenarioUnitList(this);
		ArrayList<ScenarioDirective> scenarioDirectiveList = null;
		ScenarioUnit duplicatedUnit;
		for (ScenarioUnit unit : tempList)
		{
			if (unit.getReference() != null && unit.getReference().equalsIgnoreCase(p_reference))
			{
				scenarioDirectiveList = unit.getDirectiveList();
				this.remove(unit);
				for (int i = 0; i < p_turnArray.length; i++)
				{
					duplicatedUnit = new ScenarioUnit(unit.getReference(), unit.getType(), unit.getPosition(),
							p_turnArray[i], unit.getMove());
					duplicatedUnit.addAllDirectives(new ArrayList<ScenarioDirective>(scenarioDirectiveList));
					this.add(duplicatedUnit);

				}
			}
		}
	}

	public final void duplicateReferenceByTurn(final String p_reference, final int p_currentTurn)
	{
		final ScenarioUnitList unitList = new ScenarioUnitList(this);
		for (ScenarioUnit unit : unitList)
		{
			if (unit.getReference() != null && unit.getReference().equalsIgnoreCase(p_reference))
			{
				this.add(new ScenarioUnit(null, unit.getType(), unit.getPosition(), p_currentTurn, unit.getMove()));
			}
		}

	}
}
