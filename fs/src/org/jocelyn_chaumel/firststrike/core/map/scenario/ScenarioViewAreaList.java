package org.jocelyn_chaumel.firststrike.core.map.scenario;

import java.io.Serializable;
import java.util.ArrayList;

public class ScenarioViewAreaList extends ArrayList<ScenarioViewArea> implements Serializable
{
	public ScenarioViewAreaList()
	{
		// NOP
	}

	public ScenarioViewAreaList(final ArrayList<ScenarioViewArea> p_scenarioMessageList)
	{
		super();
		this.addAll(p_scenarioMessageList);
	}

	public final ScenarioViewAreaList removeViewAreaByTurn(final int p_turn)
	{
		final ScenarioViewAreaList scenarioEventList = new ScenarioViewAreaList();
		ScenarioViewArea currentUnit = null;
		for (int i = this.size() - 1; i >= 0; i--)
		{
			currentUnit = this.get(i);
			if (currentUnit.getTurn() == p_turn)
			{
				this.remove(i);
				scenarioEventList.add(currentUnit);
			}
		}

		return scenarioEventList;
	}
}
