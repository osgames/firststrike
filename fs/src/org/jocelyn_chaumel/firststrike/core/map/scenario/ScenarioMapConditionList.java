package org.jocelyn_chaumel.firststrike.core.map.scenario;

import java.util.ArrayList;

public class ScenarioMapConditionList extends ArrayList<IMapCondition>
{
	public ScenarioMapConditionList()
	{
		// NOP
	}

	public ScenarioMapConditionList(ArrayList<IMapCondition> p_scenarioMapConditionList)
	{
		super();
		this.addAll(p_scenarioMapConditionList);
	}
}
