package org.jocelyn_chaumel.firststrike.core.map.scenario;

import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;

public interface IMapCondition
{
    public boolean executeCondition(final AbstractPlayer p_player, final int p_currentTurn);
}
