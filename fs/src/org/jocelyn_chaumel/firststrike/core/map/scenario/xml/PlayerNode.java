package org.jocelyn_chaumel.firststrike.core.map.scenario.xml;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMapConditionList;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioPlayer;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public final class PlayerNode extends AbstractNode
{
	public static final String NODE_NAME = "player";

	public static ScenarioPlayer getPlayer(final Element p_playerNode) throws InvalidScenarioException
	{
		Assert.precondition(NODE_NAME.equals(p_playerNode.getName()), "Invalid player node name");
		final String type = getMandatoryAttributeValue(p_playerNode, "type");
		final short index = (short) getMandatoryIntAttributeValue(p_playerNode, "index");
		final short team = (short) getMandatoryIntAttributeValue(p_playerNode, "team");
		final String level = getMandatoryAttributeValue(p_playerNode, "level");
		final int x = getMandatoryIntAttributeValue(p_playerNode, "x");
		final int y = getMandatoryIntAttributeValue(p_playerNode, "y");
		final Coord startPosition = new Coord(x, y);

		final ScenarioPlayer player = new ScenarioPlayer(type, index, team, startPosition, level);
		final ScenarioMapConditionList mapConditionList = new ScenarioMapConditionList();
		for (Element condition : p_playerNode.getChildren("condition"))
		{
			mapConditionList.add(MapConditionNode.getMapCondition(condition));
		}
		if (!mapConditionList.isEmpty())
		{
			player.setMapConditions(mapConditionList);
		}

		for (Element cityNode : p_playerNode.getChildren("city"))
		{
			player.addCity(CityNode.getCity(cityNode));
		}

		for (Element unitNode : p_playerNode.getChildren("unit"))
		{
			player.addAllUnits(UnitNode.getUnitList(unitNode));
		}

		final Element messages = p_playerNode.getChild("messages");
		if (messages != null)
		{
			for (Element messageNode : messages.getChildren("message"))
			{
				player.addAllMessage(MessageNode.getMessage(messageNode));
			}
			
		}

		for (Element eventNode : p_playerNode.getChildren("view-area"))
		{ 
			player.addAllViewArea(ViewAreaNode.getViewAreaList(eventNode));
		}

		
		return player;
	}
}
