package org.jocelyn_chaumel.firststrike.core.map.scenario;

import java.io.Serializable;
import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.UnitFactory;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;
import org.jocelyn_chaumel.tools.debugging.Assert;

public final class ScenarioUnit implements Serializable
{
	public static final String TANK = UnitTypeCst.TANK.getLabel();
	public static final String FIGHTER = UnitTypeCst.FIGHTER.getLabel();
	public static final String TRANSPORT_SHIP = UnitTypeCst.TRANSPORT_SHIP.getLabel();
	public static final String DESTROYER = UnitTypeCst.DESTROYER.getLabel();
	public static final String BATTLESHIP = UnitTypeCst.BATTLESHIP.getLabel();
	public static final String ARTILLERY = UnitTypeCst.ARTILLERY.getLabel();
	public static final String RADAR = UnitTypeCst.RADAR.getLabel();
	public static final int DEFAULT_MOVE_VALUE = -1;

	private final Coord _position;
	private final String _type;
	private int _turn;
	private final String _reference;
	private final int _move;
	private final ArrayList<ScenarioUnit> _containedUnit = new ArrayList<ScenarioUnit>();
	private final ArrayList<ScenarioDirective> _directiveList = new ArrayList<ScenarioDirective>();

	public ScenarioUnit(final String p_reference, final String p_type, final Coord p_position, final int p_turn, final int p_move)
	{
		_reference = p_reference;
		_type = p_type;
		_position = p_position;
		_turn = p_turn;
		_move = p_move;
	}

	public final Coord getPosition()
	{
		return _position;
	}
	
	public final String getReference()
	{
		return _reference;
	}

	public final String getType()
	{
		return _type;
	}

	public final int getTurn()
	{
		return _turn;
	}

	public final int getMove()
	{
		return _move;
	}

	public void addAllContainedUnit(final ScenarioUnitList p_unitList)
	{
		_containedUnit.addAll(p_unitList);
	}

	public final ArrayList<ScenarioUnit> getContainedUnitList()
	{
		return _containedUnit;
	}

	public void addAllDirectives(final ArrayList<ScenarioDirective> p_directive)
	{
		_directiveList.addAll(p_directive);
	}

	public ArrayList<ScenarioDirective> getDirectiveList()
	{
		return _directiveList;
	}

	public final static AbstractUnit createUnit(final ScenarioUnit p_scnUnit, final AbstractPlayer p_player) throws InvalidScenarioException
	{
		Assert.preconditionNotNull(p_scnUnit, "Unit");
		Assert.preconditionNotNull(p_player, "Player");

		AbstractUnit unit = null;
		final Coord position = p_scnUnit.getPosition();

		// Unit creation
		try
		{
			unit = UnitFactory.createUnit(p_player, position, UnitTypeCst.getInstance(p_scnUnit._type));
		} catch (InvalidConstantValueException ex)
		{
			throw new InvalidScenarioException("Unsupported Unit Type : " + p_scnUnit._type);
		}

		final FSMapMgr mapMgr = p_player.getMapMgr();
		// Action list creation
		for (ScenarioDirective directive : p_scnUnit._directiveList)
		{
			unit.addDirective(ScenarioDirective.createDirective(directive, unit, mapMgr));
		}

		final int move = p_scnUnit.getMove();
		if (move != DEFAULT_MOVE_VALUE)
		{
			unit.setMove(move);
		}

		// Contained unit creation
		final ArrayList<ScenarioUnit> containedScnUnitList = p_scnUnit.getContainedUnitList();
		if (containedScnUnitList != null && containedScnUnitList.size() > 1)
		{
			if (!TRANSPORT_SHIP.equals(p_scnUnit._type))
			{
				throw new InvalidScenarioException("Only Transport Ship can contains unit (tank)");
			}

			final TransportShip transport = (TransportShip) unit;
			Tank currentTank = null;
			for (ScenarioUnit scnUnit : containedScnUnitList)
			{
				currentTank = (Tank) createUnit(scnUnit, p_player);
				currentTank.loadInContainer(transport);
			}
		}
		return unit;
	}
	
	public final void setTurn(final int p_turn)
	{
		_turn = p_turn;
	}

	@Override
	public final String toString()
	{
		return "{ScenarioUnit= Type:" + _type + ", Pos:" + _position + ", NbDir:" + _directiveList.size() + "}";
	}
}
