package org.jocelyn_chaumel.firststrike.core.map.scenario;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;

public class ScenarioMessageList extends ArrayList<ScenarioMessage>
{
	public ScenarioMessageList()
	{
		// NOP
	}

	public ScenarioMessageList(ArrayList<ScenarioMessage> p_scenarioMessageList)
	{
		super();
		this.addAll(p_scenarioMessageList);
	}

	public final ScenarioMessageList removeMsgByTurn(final int p_turn)
	{
		final ScenarioMessageList scenarioMessageList = new ScenarioMessageList();
		ScenarioMessage currentUnit = null;
		for (int i = this.size() - 1; i >= 0; i--)
		{
			currentUnit = this.get(i);
			if (currentUnit.getTurn() == p_turn)
			{
				this.remove(i);
				scenarioMessageList.add(currentUnit);
			}
		}

		Collections.sort(scenarioMessageList, new ScenarioMessageOrderComparator());
		return scenarioMessageList;
	}

	public class ScenarioMessageOrderComparator implements Comparator<ScenarioMessage>
	{

		@Override
		public int compare(ScenarioMessage p_arg0, ScenarioMessage p_arg1)
		{
			return p_arg0.getOrder() - p_arg1.getOrder();
		}

	}
	
	public final void replaceReferenceByTurn(final String p_reference, final int p_currentTurn)
	{
		for (ScenarioMessage message : this)
		{
			if (message.getMessageReference().equalsIgnoreCase(p_reference))
			{
				message.setTurn(p_currentTurn);
			}
		}
	}
	
	public final void duplicateReferenceByTurn(final String p_reference, final int p_currentTurn) throws InvalidScenarioException
	{
		final ScenarioMessageList msgList = new ScenarioMessageList(this);
		for (ScenarioMessage message : msgList)
		{
			if (message.getMessageReference().equalsIgnoreCase(p_reference))
			{
				this.add(new ScenarioMessage(p_currentTurn, message.getText(), message.getText(), message.getPictureName(), message.getOrientation()));
			}
		}
	}
	
}
