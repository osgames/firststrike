package org.jocelyn_chaumel.firststrike.core.map.scenario.xml;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioDirective;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class DirectiveNode extends AbstractNode
{
	public static final String NODE_NAME = "directive";
	public static final String DESTINATION_NODE_NAME = "destination";
	public static final String ORIGIN_NODE_NAME = "origin";

	public static ScenarioDirective getScenarioDirective(final Element p_node) throws InvalidScenarioException
	{
		Assert.preconditionNotNull(p_node, "Node");
		Assert.precondition(NODE_NAME.equals(p_node.getName()), "Invalid node name detected");

		final String type = getMandatoryAttributeValue(p_node, "type");
		if (!ScenarioDirective.AUTO.equals(type) 
				&& !ScenarioDirective.MOVE_WITH_COMBAT.equals(type)
				&& !ScenarioDirective.MOVE_WITHOUT_COMBAT.equals(type)
				&& !ScenarioDirective.TANK_PROTECT_POSITION.equals(type) 
				&& !ScenarioDirective.ARTILLERY_PROTECT_POSITION.equals(type)
				&& !ScenarioDirective.TANK_PROTECT_ARTILLERY.equals(type))
		{
			throw new InvalidScenarioException("Unsupported directive type detected: " + type);
		}

		final ScenarioDirective directive = new ScenarioDirective(type);
		final Element origin = p_node.getChild(ORIGIN_NODE_NAME);
		if (origin != null)
		{
			final int x = getMandatoryIntAttributeValue(origin, "x");
			final int y = getMandatoryIntAttributeValue(origin, "y");
			directive.setOriginCoord(new Coord(x, y));
		}

		if (ScenarioDirective.MOVE_WITH_COMBAT.equals(type))
		{
			final Element destinationNode = getMandatoryChildNode(p_node, DESTINATION_NODE_NAME);
			final int x = getMandatoryIntAttributeValue(destinationNode, "x");
			final int y = getMandatoryIntAttributeValue(destinationNode, "y");
			directive.setDestination(new Coord(x, y));
		} else if (ScenarioDirective.MOVE_WITHOUT_COMBAT.equals(type))
		{
			final Element destinationNode = getMandatoryChildNode(p_node, DESTINATION_NODE_NAME);
			final int x = getMandatoryIntAttributeValue(destinationNode, "x");
			final int y = getMandatoryIntAttributeValue(destinationNode, "y");
			directive.setDestination(new Coord(x, y));
		} else if (ScenarioDirective.TANK_PROTECT_POSITION.equals(type))
		{
			final Element destinationNode = getChildNode(p_node, DESTINATION_NODE_NAME);
			if (destinationNode != null)
			{
				final int x = getMandatoryIntAttributeValue(destinationNode, "x");
				final int y = getMandatoryIntAttributeValue(destinationNode, "y");
				directive.setDestination(new Coord(x, y));
			}
		} else if (ScenarioDirective.ARTILLERY_PROTECT_POSITION.equals(type))
		{
			final Element destinationNode =  getChildNode(p_node, DESTINATION_NODE_NAME);
			if (destinationNode != null) 
			{
				final int x = getMandatoryIntAttributeValue(destinationNode, "x");
				final int y = getMandatoryIntAttributeValue(destinationNode, "y");
				directive.setDestination(new Coord(x, y));
			}
		}
		return directive;
	}

}
