package org.jocelyn_chaumel.firststrike.core.map.scenario.xml;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.IMapCondition;
import org.jocelyn_chaumel.tools.debugging.Assert;

public final class MapConditionNode extends AbstractNode
{
	public static final String NODE_NAME = "condition";
	public static final String CLASS_ATTR_NAME = "class";

	public static IMapCondition getMapCondition(final Element p_conditionNode) throws InvalidScenarioException
	{
		Assert.preconditionNotNull(p_conditionNode, "Map Condition Node");

		IMapCondition mapCondition;
		try
		{
			final String className = getMandatoryAttributeValue(p_conditionNode, CLASS_ATTR_NAME);
			mapCondition = (IMapCondition) Class.forName(className).newInstance();
		} catch (Exception ex)
		{
			throw new InvalidScenarioException("", ex);
		}
		return mapCondition;
	}

}