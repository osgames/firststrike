package org.jocelyn_chaumel.firststrike.core.map.scenario;

import java.util.ArrayList;

public class MapScenario
{
	private final String _name;
	private final String _desc;
	private final ArrayList<ScenarioPlayer> _playerList = new ArrayList<ScenarioPlayer>();
	private final int _nbRow;

	public MapScenario(final String p_name, final String p_desc, final int p_nbRow)
	{

		_name = p_name;
		_desc = p_desc;
		_nbRow = p_nbRow;
	}

	public String getName()
	{
		return _name;
	}

	public String getDesc()
	{
		return _desc;
	}

	public int getNbRow()
	{
		return _nbRow;
	}

	public final void addPlayer(final ScenarioPlayer p_player)
	{
		_playerList.add(p_player);
	}

	public final ArrayList<ScenarioPlayer> getPlayerList()
	{
		return _playerList;
	}
}
