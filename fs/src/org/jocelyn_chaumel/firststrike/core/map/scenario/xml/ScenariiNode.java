package org.jocelyn_chaumel.firststrike.core.map.scenario.xml;

import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.MapScenario;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class ScenariiNode extends Element
{
	public final static String NODE_NAME = "scenarii";

	public final static ArrayList<MapScenario> getScenarioList(final Element p_scenarii) throws InvalidScenarioException

	{
		Assert.preconditionNotNull(p_scenarii, "Scenarii Node");
		Assert.precondition(p_scenarii.getName().equals(NODE_NAME), "Invalid Scenarii Nodename");

		ArrayList<MapScenario> scenarioList = new ArrayList<MapScenario>();
		final List<Element> scenarioElementList = p_scenarii.getChildren("scenario");
		for (Element scenarioElement : scenarioElementList)
		{
			scenarioList.add(ScenarioNode.getScenario(scenarioElement));
		}

		return scenarioList;
	}

}
