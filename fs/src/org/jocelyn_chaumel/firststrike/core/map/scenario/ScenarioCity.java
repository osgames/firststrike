package org.jocelyn_chaumel.firststrike.core.map.scenario;

import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class ScenarioCity
{
	private final Coord _position;

	public ScenarioCity(final Coord p_position)
	{
		Assert.preconditionNotNull(p_position, "Position");

		_position = p_position;
	}

	public final Coord getPosition()
	{
		return _position;
	}

	@Override
	public final String toString()
	{
		return "{ScenarioCity= Position:" + _position + "}";
	}
}
