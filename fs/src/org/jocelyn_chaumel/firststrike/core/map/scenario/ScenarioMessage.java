package org.jocelyn_chaumel.firststrike.core.map.scenario;

import java.awt.Dimension;
import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class ScenarioMessage implements Serializable
{
	public final static String NORTH = "north";
	public final static String SOUTH = "south";
	public final static String EAST = "east";
	public final static String WEST = "west";
	public final static int DEFAULT_DLG_WIDTH = 500;
	public final static int DEFAULT_DLG_HEIGHT = 400;
	public final static int DEFAULT_ORDER = Integer.MAX_VALUE;

	private int _turn;
	private final String _text;
	private final String _title;
	private final String _picture;
	private final String _orientation;
	private final int _dlgWidth;
	private final int _dlgHeight;
	private final int _order;
	private final String _msgReference;

	public ScenarioMessage(	final int p_turn,
							final String p_title,
							final String p_text,
							final String p_picture,
							final String p_orientation) throws InvalidScenarioException
	{
		this("", p_turn, p_title, p_text, p_picture, p_orientation, DEFAULT_DLG_WIDTH, DEFAULT_DLG_HEIGHT, DEFAULT_ORDER);
	}

	public ScenarioMessage(	final String p_msgRef,
	                       	final int p_turn,
							final String p_title,
							final String p_text,
							final String p_picture,
							final String p_orientation,
							final int p_dlgWidth,
							final int p_dlgHeight,
							final int p_order) throws InvalidScenarioException
	{
		_msgReference = p_msgRef;
		_dlgWidth = p_dlgWidth;
		_dlgHeight = p_dlgHeight;
		_turn = p_turn;
		_title = p_title;
		_text = p_text;
		_order = p_order;

		if (p_picture == null)
		{
			_picture = null;
			_orientation = null;
		} else
		{
			if (!NORTH.equals(p_orientation) && !SOUTH.equals(p_orientation) && !EAST.equals(p_orientation)
					&& !WEST.equals(p_orientation))
			{
				throw new InvalidScenarioException("Invalid orientation detected: " + p_orientation);
			}
			_picture = p_picture;
			_orientation = p_orientation;
		}
	}

	public final int getTurn()
	{
		return _turn;
	}

	public final String getTitle()
	{
		return _title;
	}

	public final String getText()
	{
		return _text;
	}

	public final int getOrder()
	{
		return _order;
	}

	public final boolean isContainingPicture()
	{
		return _picture != null;
	}

	public final String getPictureName()
	{
		Assert.check(isContainingPicture(), "No picture contained");
		return _picture;
	}

	public final String getOrientation()
	{
		Assert.check(isContainingPicture(), "No picture contained");
		return _orientation;
	}

	public final int getDlgWidth()
	{
		return _dlgWidth;
	}

	public final int getDlgHeight()
	{
		return _dlgHeight;
	}
	
	public final String getMessageReference()
	{
		return _msgReference;
	}

	public final Dimension getDimension()
	{
		return new Dimension(_dlgWidth, _dlgHeight);
	}
	
	public final void setTurn(final int p_turn)
	{
		_turn = p_turn;
	}

	@Override
	public final String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("{ScenarioMessage= Turn:" + _turn);
		sb.append(" ref:").append(_msgReference);
		if (isContainingPicture())
		{
			sb.append(" PictureName:").append(_picture);
		}
		sb.append("}");
		return sb.toString();
	}
}
