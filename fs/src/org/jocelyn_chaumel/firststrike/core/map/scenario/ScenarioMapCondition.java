package org.jocelyn_chaumel.firststrike.core.map.scenario;

import java.io.Serializable;

public final class ScenarioMapCondition implements Serializable
{
	private final String _mapConditionName;

	public ScenarioMapCondition(final String p_mapConditionName)
	{
		_mapConditionName = p_mapConditionName;
	}

	public final String getMapCondition()
	{
		return _mapConditionName;
	}

	@Override
	public final String toString()
	{
		return "{ScenarioMapCondition= class:" + _mapConditionName + "}";
	}
}
