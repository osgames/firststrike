package org.jocelyn_chaumel.firststrike.core.map.scenario.xml;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioCity;
import org.jocelyn_chaumel.tools.data_type.Coord;

public final class CityNode extends AbstractNode
{
	public static final String NODE_NAME = "city";

	public static ScenarioCity getCity(final Element p_cityNode) throws InvalidScenarioException
	{
		final int x = getMandatoryIntAttributeValue(p_cityNode, "x");
		final int y = getMandatoryIntAttributeValue(p_cityNode, "y");
		final Coord position = new Coord(x, y);

		return new ScenarioCity(position);
	}
}
