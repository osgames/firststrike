package org.jocelyn_chaumel.firststrike.core.map.scenario.xml;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMessage;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMessageList;

public class MessageNode extends AbstractNode
{

	public final static ScenarioMessageList getMessage(final Element p_messageNode) throws InvalidScenarioException
	{
		final String msgRef = AbstractNode.getNodeReference(p_messageNode);
		final int[] turn;
		if (msgRef == ""){
			turn = AbstractNode.getMandatoryIntArrayAttributeValue(p_messageNode, "turn");
		} else {
			turn = new int[]{Integer.MAX_VALUE};
		}
		final Element titleElement = AbstractNode.getMandatoryChildNode(p_messageNode, "title");
		final Element textElement = AbstractNode.getMandatoryChildNode(p_messageNode, "text");
		final String text = AbstractNode.getI18NMessage(textElement).getI18NText();
		final String title = AbstractNode.getI18NMessage(titleElement).getI18NText();
		final int dlgWidth;
		final int dlgHeight;
		final int order;
		if (p_messageNode.getAttribute("width") != null)
		{
			dlgWidth = AbstractNode.getMandatoryIntAttributeValue(p_messageNode, "width");
			dlgHeight = AbstractNode.getMandatoryIntAttributeValue(p_messageNode, "height");
		} else
		{
			dlgWidth = ScenarioMessage.DEFAULT_DLG_WIDTH;
			dlgHeight = ScenarioMessage.DEFAULT_DLG_HEIGHT;
		}

		if (p_messageNode.getAttribute("order") != null)
		{
			order = AbstractNode.getMandatoryIntAttributeValue(p_messageNode, "order");
		} else
		{
			order = ScenarioMessage.DEFAULT_ORDER;
		}

		final ScenarioMessageList msgList = new ScenarioMessageList();
		final Element pictureNode = p_messageNode.getChild("picture");
		final String orientation;
		final String pictureName;
		if (pictureNode == null)
		{
			orientation = null;
			pictureName = null;
		} else
		{
			orientation = AbstractNode.getMandatoryAttributeValue(pictureNode, "position");
			pictureName = pictureNode.getText();
		}

		for (int i = 0; i < turn.length; i++)
		{
			msgList.add(new ScenarioMessage(msgRef, turn[i], title, text, pictureName, orientation, dlgWidth, dlgHeight,
					order));
		}
		return msgList;
	}
}
