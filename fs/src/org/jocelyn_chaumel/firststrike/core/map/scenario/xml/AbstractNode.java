package org.jocelyn_chaumel.firststrike.core.map.scenario.xml;

import java.util.StringTokenizer;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.data_type.I18NMessage;

public abstract class AbstractNode
{
	public static final String NODE_REFERENCE = "node_reference";
	
	public final static String getMandatoryAttributeValue(final Element p_element, final String p_attribute) throws InvalidScenarioException
	{
		final String value = p_element.getAttributeValue(p_attribute);
		if (value == null)
		{
			throw new InvalidScenarioException("Mandatory attribute '" + p_attribute + "' not found in '"
					+ p_element.getName() + "'.");
		}

		return value;
	}
	
	public final static String getNodeReference(final Element p_element)
	{
		String nodeRef = p_element.getAttributeValue(NODE_REFERENCE);
		
		if (nodeRef == null)
		{
			return "";
		}
		return nodeRef;
	}

	public final static int getMandatoryIntAttributeValue(final Element p_element, final String p_attribute) throws InvalidScenarioException
	{
		final String strValue = p_element.getAttributeValue(p_attribute);
		if (strValue == null)
		{
			throw new InvalidScenarioException("Mandatory attribute '" + p_attribute + "' not found in '"
					+ p_element.getName() + "'.");
		}

		final int value;
		try
		{
			value = Integer.parseInt(strValue);
		} catch (NumberFormatException ex)
		{
			throw new InvalidScenarioException("Invalid integer value detected for " + p_element.getName() + "."
					+ p_attribute + " : " + strValue);
		}

		return value;
	}

	public final static int[] getMandatoryIntArrayAttributeValue(final Element p_element, final String p_attribute) throws InvalidScenarioException
	{
		String value = getMandatoryAttributeValue(p_element, p_attribute);
		StringTokenizer tokenizer = new StringTokenizer(value, ",");
		final int[] intArray = new int[tokenizer.countTokens()];
		String token;
		int idx = 0;
		while (tokenizer.hasMoreTokens())
		{
			try
			{
				token = tokenizer.nextToken().trim();
				intArray[idx++] = Integer.parseInt(token);
			} catch (NumberFormatException ex)
			{
				throw new InvalidScenarioException("Invalid integer array value detected for " + p_element.getName()
						+ "." + p_attribute + " : " + value);
			}

		}

		return intArray;
	}
	
	public final static boolean getBooleanAttributeValue(final Element p_element, final String p_attributeName)
	{
		final String value = p_element.getAttributeValue(p_attributeName);
		return ("true".equalsIgnoreCase(value) || "yes".equalsIgnoreCase(value));
	}

	public final static Element getMandatoryChildNode(final Element p_element, final String p_childName) throws InvalidScenarioException
	{
		final Element childNode = p_element.getChild(p_childName);
		if (childNode == null)
		{
			throw new InvalidScenarioException("Mandatory child node '" + p_childName + "' not found in '"
					+ p_element.getName() + "'.");
		}

		return childNode;
	}
	
	public final static Element getChildNode(final Element p_element, final String p_childName)
	{
		return p_element.getChild(p_childName);
	}

	public final static I18NMessage getI18NMessage(final Element p_element)
	{
		return new I18NMessage(p_element);
	}
}
