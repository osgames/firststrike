package org.jocelyn_chaumel.firststrike.core.map.scenario.xml;

import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioPlayer;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class PlayersNode extends Element
{
	public final static String NODE_NAME = "players";

	public static ArrayList<ScenarioPlayer> getPlayerList(final Element p_playersNode) throws InvalidScenarioException
	{
		Assert.preconditionNotNull(p_playersNode, "Players node");
		Assert.precondition(NODE_NAME.equals(p_playersNode.getName()), "Invalid Players node name");

		List<Element> playerNodeList = p_playersNode.getChildren(ScenarioPlayer.NODE_NAME);
		ArrayList<ScenarioPlayer> playerList = new ArrayList<ScenarioPlayer>();

		for (Element playerNode : playerNodeList)
		{
			playerList.add(PlayerNode.getPlayer(playerNode));
		}

		return playerList;
	}
}
