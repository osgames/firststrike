package org.jocelyn_chaumel.firststrike.core.map.scenario;

import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.InvalidScenarioException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerId;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.firststrike.core.unit.UnitIndex;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class ScenarioPlayer
{
	public static final String HUMAN_PLAYER = "human";
	public static final String COMPUTER_PLAYER = "computer";
	public static final String NODE_NAME = "player";

	private final String _type;
	private final short _team;
	private final short _index;
	private final Coord _startPosition;
	private final PlayerLevelCst _level;
	private ScenarioMapConditionList _mapConditionList = null;
	private final ArrayList<ScenarioUnit> _unitList = new ArrayList<ScenarioUnit>();
	private final ArrayList<ScenarioCity> _cityList = new ArrayList<ScenarioCity>();
	private final ArrayList<ScenarioMessage> _messageList = new ArrayList<ScenarioMessage>();
	private final ArrayList<ScenarioViewArea> _eventList = new ArrayList<ScenarioViewArea>();

	public ScenarioPlayer(	final String p_type,
							final short p_index,
							final short p_team,
							final Coord p_startPosition,
							final String p_level) throws InvalidScenarioException
	{
		Assert.precondition(p_type.equals(HUMAN_PLAYER) || p_type.equals(COMPUTER_PLAYER),
							"Invalid player type detected");
		Assert.precondition(p_index >= 0, "Invalid index value");

		_type = p_type;
		_team = p_team;
		_index = p_index;
		_startPosition = p_startPosition;

		if (p_level == null)
		{
			_level = PlayerLevelCst.EASY_LEVEL;
		} else
		{
			try
			{
				_level = PlayerLevelCst.getInstance(p_level);
			} catch (InvalidConstantValueException ex)
			{
				throw new InvalidScenarioException("Unsupported Player Level: " + p_level, ex);
			}
		}
	}

	public String getType()
	{
		return _type;
	}

	public short getTeam()
	{
		return _team;
	}

	public short getIndex()
	{
		return _index;
	}

	public final Coord getStartPosition()
	{
		return _startPosition;
	}
	
	public ScenarioMapConditionList getMapConditionList()
	{
		return _mapConditionList;
	}
	
	public final void setMapConditions(final ScenarioMapConditionList p_mapConditionList)
	{
		_mapConditionList = p_mapConditionList;
	}

	public ArrayList<ScenarioUnit> getUnitList()
	{
		return _unitList;
	}

	public final void addAllUnits(final ScenarioUnitList p_unit)
	{
		_unitList.addAll(p_unit);
	}

	public ArrayList<ScenarioCity> getCityList()
	{
		return _cityList;
	}

	public final void addCity(final ScenarioCity p_city)
	{
		_cityList.add(p_city);
	}

	public ArrayList<ScenarioMessage> getMessageList()
	{
		return _messageList;
	}
	
	public ArrayList<ScenarioViewArea> getEventList()
	{
		return _eventList;
	}

	public final void addAllMessage(final ScenarioMessageList p_messageList)
	{
		Assert.preconditionNotNull(p_messageList);

		_messageList.addAll(p_messageList);
	}
	
	public final void addAllViewArea(final ScenarioViewAreaList p_eventList)
	{
		Assert.preconditionNotNull(p_eventList);
		
		_eventList.addAll(p_eventList);
	}

	public final PlayerLevelCst getLevel()
	{
		return _level;
	}

	public final static AbstractPlayer createPlayer(final ScenarioPlayer p_scenarioPlayer,
													final FSMapMgr p_mapMgr,
													final UnitIndex p_unitIndex)
	{
		PlayerId plId = new PlayerId(p_scenarioPlayer.getIndex());
		final ScenarioUnitList scenarioUnitList = new ScenarioUnitList(p_scenarioPlayer.getUnitList());
		final ScenarioMessageList scenarioMsgList = new ScenarioMessageList(p_scenarioPlayer.getMessageList());
		final ScenarioViewAreaList scenarioEventList = new ScenarioViewAreaList(p_scenarioPlayer.getEventList());
		if (ScenarioPlayer.HUMAN_PLAYER.equals(p_scenarioPlayer.getType()))
		{
			HumanPlayer human = new HumanPlayer(plId,
												p_scenarioPlayer.getStartPosition(),
												p_mapMgr,
												p_scenarioPlayer.getLevel(),
												p_unitIndex,
												p_scenarioPlayer.getMapConditionList(),
												scenarioUnitList,
												scenarioMsgList, 
												scenarioEventList);
			return human;
		} else if (ScenarioPlayer.COMPUTER_PLAYER.equals(p_scenarioPlayer.getType()))
		{
			ComputerPlayer computer = new ComputerPlayer(	plId,
															p_scenarioPlayer.getStartPosition(),
															p_mapMgr,
															p_scenarioPlayer.getLevel(),
															p_unitIndex,
															p_scenarioPlayer.getMapConditionList(),
															scenarioUnitList, 
															scenarioEventList);
			return computer;
		} else
		{
			return null;
		}
	}

}
