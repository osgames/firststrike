package org.jocelyn_chaumel.firststrike.core.map.scenario;

import java.io.Serializable;

import org.jocelyn_chaumel.tools.data_type.Coord;

public class ScenarioViewArea implements Serializable
{
	private final Coord _position;
	private final int _turn;
	private final int _height;
	private final int _width;
	private final int _wait;
	private final boolean _unitDetection;

	public ScenarioViewArea(	final Coord p_position, final int p_height, final int p_width,
							final int p_turn,
							final int p_wait, final boolean p_unitDetection)
	{
		_turn = p_turn;
		_position = p_position;
		_height = p_height;
		_wait = p_wait;
		_width = p_width;
		_unitDetection = p_unitDetection;
	}

	public Coord getPosition()
	{
		return _position;
	}

	public int getTurn()
	{
		return _turn;
	}

	public int getWidth()
	{
		return _width;
	}

	public int getWait()
	{
		return _wait;
	}

	public int getHeight()
	{
		return _height;
	}
	
	public final boolean isUnitDetection()
	{
		return _unitDetection;
	}
}
