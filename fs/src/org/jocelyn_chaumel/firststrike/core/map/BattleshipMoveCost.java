/*
 * Created on May 1, 2005
 */
package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public final class BattleshipMoveCost extends AbstractMoveCost
{

	private final int BATTLESHIP_MOVE_COST = 33;
	public BattleshipMoveCost(final int anUniqueId)
	{
		super(anUniqueId, AreaTypeCst.SEA_AREA);
	}

	@Override
	public int getCost(final Cell p_cell)
	{
		Assert.preconditionNotNull(p_cell, "Cell");
		final CellTypeCst cellType = p_cell.getCellType();
		if (CellTypeCst.SEA.equals(cellType))
		{
			return BATTLESHIP_MOVE_COST;
		} else if (CellTypeCst.CITY.equals(cellType) && p_cell.isNearToWather())
		{
			return BATTLESHIP_MOVE_COST;
		} else
		{
			return UNREACHABLE_CELL;
		}
	}

	@Override
	public final int getMaxCost()
	{
		return BATTLESHIP_MOVE_COST;
	}

	@Override
	public int getMinCost()
	{
		return BATTLESHIP_MOVE_COST;
	}
}
