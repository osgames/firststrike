/*
 * FSMapInfo.java Created on 21 f�vrier 2003, 23:27
 */

package org.jocelyn_chaumel.firststrike.core.map;

import java.io.Serializable;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe contient les informations d'une carte.
 */
public final class FSMapInfo implements Serializable
{
	private final String _mapName;
	private final String _mapTitle;
	private final String _description;
	private final String _version;
	private final int _x;
	private final int _y;
	private String _defaultSkin = null;

	/**
	 * Constructeur parametrise.
	 * 
	 * @param p_mapName Nom de la carte.
	 * @param p_description Description facultative de la carte.
	 * @param p_version Version de la carte.
	 * @param p_x Largeur de la carte.
	 * @param p_y Hauteur de la carte.
	 */
	public FSMapInfo(	final String p_mapName,
						final String p_mapTitle,
						final String p_description,
						final String p_version,
						final int p_x,
						final int p_y)
	{
		Assert.precondition(p_mapName != null, "Null Map Name Ref");
		Assert.precondition(p_mapName.length() > 0, "");
		Assert.preconditionNotNull(p_version, "Version");
		Assert.precondition(p_version.startsWith(FSMapMgr.VERSION.substring(0, 1)), "Invalid Version");
		Assert.precondition_between(p_x, FSMapMgr.MAP_MIN_XY, FSMapMgr.MAP_MAX_XY, "Invalid X");
		Assert.precondition_between(p_y, FSMapMgr.MAP_MIN_XY, FSMapMgr.MAP_MAX_XY, "Invalid Y");

		_mapName = p_mapName;
		if (p_mapTitle == null)
		{
			_mapTitle = _mapName;
		} else
		{
			_mapTitle = p_mapTitle;
		}
		_description = p_description;
		_version = p_version;
		_x = p_x;
		_y = p_y;
	}

	public final String getName()
	{
		return _mapName;
	}

	public final String getTitle()
	{
		return _mapTitle;
	}

	public final String getDescription()
	{
		return _description;
	}

	public final String getVersion()
	{
		return _version;
	}

	public final int getX()
	{
		return _x;
	}

	public final int getY()
	{
		return _y;
	}

	public final void setDefaultSkin(final String p_skinFolder)
	{
		_defaultSkin = p_skinFolder;
	}

	public final boolean hasDefaultSkin()
	{
		return _defaultSkin != null;
	}

	public final String getDefaultSkin()
	{
		return _defaultSkin;
	}
}