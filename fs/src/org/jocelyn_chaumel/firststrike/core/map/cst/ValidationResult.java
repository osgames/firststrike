package org.jocelyn_chaumel.firststrike.core.map.cst;

import org.jocelyn_chaumel.tools.data_type.cst.IntCst;

public class ValidationResult extends IntCst
{
	// Lorsqu'une ville touche � plusieurs cellule Wather avec des AreaIds
	// diff�rents
	public final static ValidationResult MULTIPLE_WATHER_CONTACT = new ValidationResult(1,
																						"Bordered by more than one sea",
																						false);

	// Lorsque toutes les villes portuaires ne donnent pas acc�s � la m�me
	// �tentdue d'eau (area id).
	public final static ValidationResult DIFFERENT_WATHER_CONTACT = new ValidationResult(	2,
																							"No main sea detected between city of the map",
																							false);

	// Lorsque qu'un continent ne poss�de pas de ville portuaire.
	public final static ValidationResult AREA_WITHOUT_CONTACT = new ValidationResult(3, "Area without city port", false);

	// Lorsque le type de la cellule n'est pas support� ou inconnu.
	public final static ValidationResult INVALID_CELL_TYPE = new ValidationResult(4, "Invalid Cell type", false);

	// Lorsque le type de la cellule n'est pas support� ou inconnu.
	public final static ValidationResult NOT_ENOUGH_CITY = new ValidationResult(5, "Not enough city in this map", false);

	private final boolean _valid;

	private ValidationResult(final int p_errorCode, final String p_constName, final boolean p_validity)
	{
		super(p_errorCode, p_constName);
		_valid = p_validity;
	}

	public final boolean isValid()
	{
		return _valid;
	}
}
