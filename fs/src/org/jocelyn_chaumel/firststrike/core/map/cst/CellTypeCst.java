/*
 * Created on Oct 24, 2004
 */
package org.jocelyn_chaumel.firststrike.core.map.cst;

import java.util.ArrayList;

import org.jocelyn_chaumel.tools.data_type.cst.IntCst;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;

/**
 * Classe d�finissant toutes les constantes concernant les types des cellules
 * possibles dans une carte.
 * 
 * @author Jocelyn Chaumel
 */
public final class CellTypeCst extends IntCst
{
	public final static CellTypeCst SEA = new CellTypeCst(0, "sea");
	public final static CellTypeCst CITY = new CellTypeCst(1, "city");
	public final static CellTypeCst GRASSLAND = new CellTypeCst(2, "grassland");
	public final static CellTypeCst LIGHT_WOOD = new CellTypeCst(3, "light_wood");
	public final static CellTypeCst HEAVY_WOOD = new CellTypeCst(4, "heavy_wood");
	public final static CellTypeCst HILL = new CellTypeCst(5, "hill");
	public final static CellTypeCst MOUNTAIN = new CellTypeCst(6, "mountain");
	public final static CellTypeCst RIVER = new CellTypeCst(7, "river");
	public final static CellTypeCst ROAD = new CellTypeCst(8, "road");
	public final static CellTypeCst SHADOW = new CellTypeCst(-9, "shadow");

	private final static CellTypeCst[] _cellTypeList = { SEA, CITY, GRASSLAND, LIGHT_WOOD, HEAVY_WOOD, HILL, MOUNTAIN,
			RIVER, ROAD };

	public final static ArrayList<CellTypeCst> GROUND_CELL_TYPE_LIST = new ArrayList<CellTypeCst>();
	static
	{
		GROUND_CELL_TYPE_LIST.add(CITY);
		GROUND_CELL_TYPE_LIST.add(GRASSLAND);
		GROUND_CELL_TYPE_LIST.add(LIGHT_WOOD);
		GROUND_CELL_TYPE_LIST.add(HEAVY_WOOD);
		GROUND_CELL_TYPE_LIST.add(HILL);
		GROUND_CELL_TYPE_LIST.add(ROAD);
	}

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param p_value Valeur de la constante.
	 */
	protected CellTypeCst(final int p_value, final String p_label)
	{
		super(p_value, p_label);
	}

	/**
	 * Retourne l'instante d'une constante � partir d'une valeur.
	 * 
	 * @param p_value Valeur de la constante.
	 * @return Instance de la constante.
	 * @throws InvalidConstantValueException
	 */
	public static CellTypeCst getInstante(final int p_value) throws InvalidConstantValueException
	{
		return (CellTypeCst) IntCst.getInstance(p_value, _cellTypeList, CellTypeCst.class);
	}
}