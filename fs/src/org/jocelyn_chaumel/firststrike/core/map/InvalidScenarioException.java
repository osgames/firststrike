package org.jocelyn_chaumel.firststrike.core.map;

public class InvalidScenarioException extends Exception
{
	public InvalidScenarioException(final String p_description)
	{
		super(p_description);
	}

	public InvalidScenarioException(final Throwable p_cause)
	{
		super(p_cause);
	}

	public InvalidScenarioException(final String p_description, final Throwable p_cause)
	{
		super(p_description, p_cause);
	}
}
