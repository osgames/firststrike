/*
 * Created on May 1, 2005
 */
package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class ArtilleryMoveCost extends AbstractMoveCost
{
	private final static short LOW = 33;
	private final static short HEAVY = 50;

	/**
	 * Constructeur par d�faut.
	 * 
	 * @param p_uniqueId Idenfiant unique.
	 * @see AbstractMoveCost#AbstractMoveCost(int, AreaTypeCst)
	 */
	public ArtilleryMoveCost(final int p_uniqueId)
	{
		super(p_uniqueId, AreaTypeCst.GROUND_AREA);
	}

	@Override
	public int getCost(final Cell p_cell)
	{
		Assert.preconditionNotNull(p_cell, "Cell Type");

		CellTypeCst cellType = p_cell.getCellType();
		if (CellTypeCst.SEA.equals(cellType) || CellTypeCst.MOUNTAIN.equals(cellType) || CellTypeCst.RIVER.equals(cellType))
		{
			return UNREACHABLE_CELL;
		} else if (CellTypeCst.CITY.equals(cellType) || CellTypeCst.ROAD.equals(cellType))
		{
			return LOW;
		} else if (CellTypeCst.GRASSLAND.equals(cellType))
		{
			return HEAVY;
		} else if (CellTypeCst.LIGHT_WOOD.equals(cellType))
		{
			return HEAVY;
		} else if (CellTypeCst.HEAVY_WOOD.equals(cellType))
		{
			return HEAVY;
		} else if (CellTypeCst.HILL.equals(cellType))
		{
			return HEAVY;
		} else
		{
			throw new IllegalArgumentException("Unsupported Cell Type");
		}
	}

	@Override
	public final int getMaxCost()
	{
		return LOW;
	}

	@Override
	public int getMinCost()
	{
		return LOW;
	}
}
