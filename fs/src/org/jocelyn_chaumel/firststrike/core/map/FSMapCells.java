/*
 * FSMapCells.java Created on 23 f�vrier 2003, 12:22
 */

package org.jocelyn_chaumel.firststrike.core.map;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaList;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordHashtable;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * This class is the lowest implementation level for a map. This class contains
 * an array of cells, the map's city coord list and the identifier of the main
 * sea area.
 * 
 * @author Jocelyn Chaumel
 */
public final class FSMapCells implements Serializable {
	private final Cell[][] _cells;
	private final CityList _cityList;
	private final AreaList _areaList;
	private final Area _seaArea;
	private final boolean _validatedMap;
	private final CoordHashtable _cellCoordNearToGroundCell = new CoordHashtable();

	public FSMapCells(final Cell[][] p_cellArray, final AreaNameMap p_areaNameMap, final boolean p_applyValidation)
			throws InvalidMapException {
		Assert.preconditionNotNull(p_cellArray, "Cell Array");

		final AreaBuilder areaBuilder = new AreaBuilder(p_cellArray, p_applyValidation);
		_validatedMap = p_applyValidation;
		_cells = areaBuilder.getCellsDefinition();
		_areaList = areaBuilder.getAreaList();
		_cityList = areaBuilder.getCityList();
		_seaArea = areaBuilder.getSeaArea();

		if (p_areaNameMap != null) {

			// Update the area name
			String areaName = null;
			CoordSet coordSet;
			for (Area currentArea : _areaList) 
			{
				coordSet = currentArea.getCellSet().getCoordSet();
				for (Coord namedAreaCoord : p_areaNameMap.keySet())
				{
					if (coordSet.contains(namedAreaCoord))
					{
						areaName = p_areaNameMap.get(namedAreaCoord);
						currentArea.setInternalAreaName(areaName);
						break;
					}
				}
			}
		}

	}

	/**
	 * Retourne la cellule � la coordonn�e d�crite par la coordonn�e.
	 * 
	 * @param p_coord
	 *            Coordonn�es X et Y.
	 * @return Une cellule.
	 * @see #getCellAt(int, int)
	 */
	public final Cell getCellAt(final Coord p_coord) {
		Assert.precondition(p_coord != null, "Null param");
		// Les autres pr�conditions sont trait�es dans l'autre m�thode publique.

		return getCellAt(p_coord.getX(), p_coord.getY());
	}

	public final Cell[][] getCells() {
		return _cells;
	}

	/**
	 * Retourne la cellule � la coordonn�e d�crite par X et Y.
	 * 
	 * @param x
	 *            Position x.
	 * @param y
	 *            Position y.
	 * @return Une cellule.
	 */
	public final Cell getCellAt(final int x, final int y) {
		Assert.precondition(_cells.length > y && y > -1, "Invalid Y Coord");
		Assert.precondition(_cells[y].length > x && x > -1, "Invalid X Coord");

		return _cells[y][x];
	}

	/**
	 * Retourne la largeur de la carte.
	 * 
	 * @return Largeur de la carte.
	 */
	public final int getWidth() {
		return _cells[0].length;
	}

	/**
	 * Retourne la hauteur de la carte.
	 * 
	 * @return Hauteur de la carte.
	 */
	public final int getHeight() {
		return _cells.length;
	}

	/**
	 * D�termine si deux cellules se retrouve dans une m�me r�gion.
	 * 
	 * @param p_firstCoord
	 *            Emplacement de la premi�re cellule.
	 * @param p_secondCoord
	 *            Emplacement de la deuxi�me cellule.
	 * @param p_areaType
	 * @return Vrai ou faux.
	 */
	final boolean isSameArea(final Coord p_firstCoord, final Coord p_secondCoord, final AreaTypeCst p_areaType) {
		Assert.check(_validatedMap, "This map hasn't validated yet");
		Assert.precondition(p_firstCoord != null, "Start position is null.");
		Assert.precondition(p_secondCoord != null, "End position is null.");
		Assert.preconditionNotNull(p_areaType, "Area Type");

		final int x1 = p_firstCoord.getX();
		final int y1 = p_firstCoord.getY();
		final int x2 = p_secondCoord.getX();
		final int y2 = p_secondCoord.getY();
		Assert.precondition(x1 >= 0 && x1 < this.getWidth(), "X Out of Bound");
		Assert.precondition(x2 >= 0 && x2 < this.getWidth(), "X Out of Bound");
		Assert.precondition(y1 >= 0 && y1 < this.getHeight(), "Y Out of Bound");
		Assert.precondition(y2 >= 0 && y2 < this.getHeight(), "Y Out of Bound");

		final Area area1 = _cells[y1][x1].getAreaList().getFirstAreaByType(p_areaType);
		final Area area2 = _cells[y2][x2].getAreaList().getFirstAreaByType(p_areaType);

		return area1 == area2;
	}

	/**
	 * Return city list of the map.
	 * 
	 * @return City Set
	 */
	public final CityList getCityList() {
		return _cityList;
	}

	/**
	 * Retrieve all sea cell's coord near to ground cell.
	 * 
	 * @param p_groundCellCoord
	 *            Ground Cell Coord.
	 * 
	 * @return Sea Cell coord list.
	 */
	public final CoordSet getWatherCellNearToGroundCell(final Coord p_groundCellCoord) {
		Assert.check(_validatedMap, "This map hasn't validated yet");
		Assert.preconditionNotNull(p_groundCellCoord, "Ground Cell Coord");
		Assert.precondition(_cells[p_groundCellCoord.getY()][p_groundCellCoord.getX()].isNearToWather(),
				"A Cell near to sea is expected");

		CoordSet coordSet = (CoordSet) _cellCoordNearToGroundCell.get(p_groundCellCoord);
		if (coordSet != null) {
			return coordSet;
		}

		coordSet = new CoordSet();
		final int groundX = p_groundCellCoord.getX();
		final int groundY = p_groundCellCoord.getY();

		// X - -
		// - @ -
		// - - -
		if (groundX > 0 && groundY > 0) {
			if (CellTypeCst.SEA.equals(_cells[groundY - 1][groundX - 1].getCellType())) {
				coordSet.add(new Coord(groundX - 1, groundY - 1));
			}
		}

		// - X -
		// - @ -
		// - - -
		if (groundY > 0) {
			if (CellTypeCst.SEA.equals(_cells[groundY - 1][groundX].getCellType())) {
				coordSet.add(new Coord(groundX, groundY - 1));
			}
		}

		// - - X
		// - @ -
		// - - -
		if (groundY > 0 && _cells[groundY - 1].length > groundX + 1) {
			if (CellTypeCst.SEA.equals(_cells[groundY - 1][groundX + 1].getCellType())) {
				coordSet.add(new Coord(groundX + 1, groundY - 1));
			}
		}

		// - - -
		// - @ X
		// - - -
		if (_cells[groundY].length > groundX + 1) {
			if (CellTypeCst.SEA.equals(_cells[groundY][groundX + 1].getCellType())) {
				coordSet.add(new Coord(groundX + 1, groundY));
			}
		}

		// - - -
		// - @ -
		// - - X
		if (groundY + 1 < _cells.length && _cells[groundY + 1].length > groundX + 1) {
			if (CellTypeCst.SEA.equals(_cells[groundY + 1][groundX + 1].getCellType())) {
				coordSet.add(new Coord(groundX + 1, groundY + 1));
			}
		}

		// - - -
		// - @ -
		// - X -
		if (groundY + 1 < _cells.length) {
			if (CellTypeCst.SEA.equals(_cells[groundY + 1][groundX].getCellType())) {
				coordSet.add(new Coord(groundX, groundY + 1));
			}
		}

		// - - -
		// - @ -
		// X - -
		if (groundY + 1 < _cells.length && groundX > 0) {
			if (CellTypeCst.SEA.equals(_cells[groundY + 1][groundX - 1].getCellType())) {
				coordSet.add(new Coord(groundX - 1, groundY + 1));
			}
		}

		// - - -
		// X @ -
		// - - -
		if (groundX > 0) {
			if (CellTypeCst.SEA.equals(_cells[groundY][groundX - 1].getCellType())) {
				coordSet.add(new Coord(groundX - 1, groundY));
			}
		}

		_cellCoordNearToGroundCell.put(p_groundCellCoord, coordSet);
		return coordSet;
	}

	public AreaList getAreaSet() {
		return _areaList;
	}

	/**
	 * Return the unique Sea Area of the map.
	 * 
	 * @return Sea Area.
	 */
	public Area getSeaArea() {
		return _seaArea;
	}
}