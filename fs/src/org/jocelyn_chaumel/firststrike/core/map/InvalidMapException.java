package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.core.map.cst.ValidationResult;

public class InvalidMapException extends Exception
{
	private final ValidationResult _errorDetail;

	public InvalidMapException(final String p_description)
	{
		super(p_description);
		_errorDetail = null;
	}

	public InvalidMapException(final Throwable p_ex)
	{
		super(p_ex);
		_errorDetail = null;
	}

	public InvalidMapException(final String p_desc, final Throwable p_ex)
	{
		super(p_desc, p_ex);
		_errorDetail = null;
	}

	public InvalidMapException(final String p_desc, final ValidationResult p_error)
	{
		super(p_desc);
		_errorDetail = p_error;
	}

	public final boolean containsErrorDetail()
	{
		return _errorDetail != null;
	}

	public ValidationResult getErrorDetail()
	{
		return _errorDetail;
	}
}
