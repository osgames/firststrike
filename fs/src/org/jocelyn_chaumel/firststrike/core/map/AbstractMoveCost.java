/*
 * Created on May 1, 2005
 */
package org.jocelyn_chaumel.firststrike.core.map;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.tools.data_type.Id;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe d'�crit le type de progression d'une unit� et ce qui lui en
 * co�te.
 * 
 * @author Jocelyn Chaumel
 */
public abstract class AbstractMoveCost implements Comparable, Serializable
{

	public final static int UNREACHABLE_CELL = -1;

	/**
	 * Identifiant unique de la table de d�placement.
	 */
	private final Id _id;

	/**
	 * Type de progression.<br>
	 * Exemple : R�gion terrestre, r�gion marine, etc...
	 */
	private final AreaTypeCst _areaType;

	/**
	 * Contructeur par d�faut.
	 * 
	 * @param anUniqueId Identifiant unique.
	 * @param p_areaTypeCst Type de progression.
	 */
	public AbstractMoveCost(final int anUniqueId, final AreaTypeCst p_areaTypeCst)
	{
		Assert.preconditionIsPositive(anUniqueId, "Unique Id must be positive");
		Assert.preconditionNotNull(p_areaTypeCst, "Area Type");
		_id = new Id(anUniqueId);
		_areaType = p_areaTypeCst;
	}

	/**
	 * Retourne le co�t de d�placement pour un type de cellule donn�.
	 * 
	 * @param p_cellType Type de cellule.
	 * @return Co�t de d�placement
	 */
	public abstract int getCost(final Cell p_cellType);

	/**
	 * Utilis� pour d�finir le cout en termes de point de mouvement d'une attaque.
	 * 
	 * @return
	 */
	public abstract int getMinCost();
	
	public abstract int getMaxCost();

	@Override
	public int hashCode()
	{
		return _id.hashCode();
	}

	/**
	 * Identifie le type de progression dont est capable l'unit�.
	 * 
	 * @return Type de r�gion accessible.
	 */
	public final AreaTypeCst getAreaTypeCst()
	{
		return _areaType;
	}

	@Override
	public boolean equals(final Object p_object)
	{
		if (p_object == null)
		{
			return false;
		} else if (!(p_object instanceof AbstractMoveCost))
		{
			return false;
		}

		return _id.equals(((AbstractMoveCost) p_object)._id);
	}

	@Override
	public int compareTo(final Object p_object)
	{
		if (p_object == null)
		{
			return 1;
		}

		Assert.precondition(p_object instanceof AbstractMoveCost, "Invalid compare operation");

		return _id.compareTo(((AbstractMoveCost) p_object)._id);
	}
}
