/*
 * CountryMgr.java Created on 9 mars 2003, 22:31
 */

package org.jocelyn_chaumel.firststrike.core.map;

import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.area.AbstractAreaId;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaList;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.area.GroundAreaId;
import org.jocelyn_chaumel.firststrike.core.area.MountainAreaId;
import org.jocelyn_chaumel.firststrike.core.area.RiverAreaId;
import org.jocelyn_chaumel.firststrike.core.area.SeaAreaId;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityId;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.cst.ValidationResult;
import org.jocelyn_chaumel.tools.StringManipulatorHelper;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;
import org.jocelyn_chaumel.tools.debugging.Assert;
import org.jocelyn_chaumel.tools.filling.FillingTool;

/**
 * Ce manager permet d'identifier les r�gions d'une carte de cellules. Les
 * �tapes � suivre pour g�n�rer les r�gions sont simples :
 * <ul>
 * <li>instancier la classe
 * <li>ajuster la carte via la m�thode setFSMap.
 * <li>d�finir les r�gions via la m�thode defineCountry.
 * </ul>
 * 
 * @author Jocelyn Chaumel
 */
public final class AreaBuilder {
	private final Cell[][] _cells;
	private final CityList _cityList = new CityList();
	private boolean _isCellArrayUpdated = false;
	private final AreaList _areaList = new AreaList();
	private Area _seaArea = null;
	private final boolean _applyValidation;

	/**
	 * Constructor.
	 * 
	 * @param p_cellTypeArray
	 * @throws InvalidMapException
	 */
	public AreaBuilder(final Cell[][] p_cellArray, boolean p_applyValidation) throws InvalidMapException {

		Assert.preconditionNotNull(p_cellArray, "Cell Array");
		final int height = p_cellArray.length;
		final int width = p_cellArray[0].length;
		_cells = p_cellArray;
		_applyValidation = p_applyValidation;

		// Iteration variables
		boolean isNearToWather = false;
		City city = null;
		Cell currentCell = null;
		int cityId = 1;

		// For each line
		for (int y = 0; y < height; y++) {
			Assert.preconditionNotNull(p_cellArray[y], "Cell Array [" + y + "]");

			// For each column
			for (int x = 0; x < width; x++) {
				Assert.preconditionNotNull(p_cellArray[y][x], "Cell Array [" + y + ", " + x + "]");
				currentCell = p_cellArray[y][x];
				isNearToWather = isNearToWather(p_cellArray, x, y);
				currentCell.setNearToWather(isNearToWather);
				if (CellTypeCst.CITY.equals(currentCell.getCellType())) {
					city = new City(new CityId(cityId++));
					_cityList.add(city);
					currentCell.setCity(city);

					city.setCell(currentCell);
				}
			}
		}
	}

	/**
	 * Indicate if the inspected cell is bordered by water.
	 * 
	 * @param p_cellTypeArray
	 *            Cells array
	 * @param p_x
	 *            Inspected cell's X coord
	 * @param p_y
	 *            Inspected cell's Y coord
	 * @return
	 */
	private boolean isNearToWather(final Cell[][] p_cellArray, final int p_x, final int p_y) {

		if (CellTypeCst.SEA.equals(p_cellArray[p_y][p_x].getCellType())) {
			return false;
		}

		if (p_x > 0 && p_y > 0) {
			if (CellTypeCst.SEA.equals(p_cellArray[p_y - 1][p_x - 1].getCellType())) {
				return true;
			}
		}

		if (p_y > 0) {
			if (CellTypeCst.SEA.equals(p_cellArray[p_y - 1][p_x].getCellType())) {
				return true;
			}
		}

		if (p_y > 0 && p_x + 1 < p_cellArray[p_y - 1].length) {
			if (CellTypeCst.SEA.equals(p_cellArray[p_y - 1][p_x + 1].getCellType())) {
				return true;
			}
		}

		if (p_x + 1 < p_cellArray[p_y].length) {
			if (CellTypeCst.SEA.equals(p_cellArray[p_y][p_x + 1].getCellType())) {
				return true;
			}
		}

		if (p_y + 1 < p_cellArray.length && p_x + 1 < p_cellArray[p_y + 1].length) {
			if (CellTypeCst.SEA.equals(p_cellArray[p_y + 1][p_x + 1].getCellType())) {
				return true;
			}
		}

		if (p_y + 1 < p_cellArray.length) {
			if (CellTypeCst.SEA.equals(p_cellArray[p_y + 1][p_x].getCellType())) {
				return true;
			}
		}

		if (p_y + 1 < p_cellArray.length && p_x > 0) {
			if (CellTypeCst.SEA.equals(p_cellArray[p_y + 1][p_x - 1].getCellType())) {
				return true;
			}
		}

		if (p_x > 0) {
			if (CellTypeCst.SEA.equals(p_cellArray[p_y][p_x - 1].getCellType())) {
				return true;
			}
		}
		return false;
	}

	public final Cell[][] getCells() {
		Assert.check(!_isCellArrayUpdated, "This method can't be executed more than once");
		_isCellArrayUpdated = true;
		return _cells;
	}

	/**
	 * Build and return an cells definition for the current Map.
	 * 
	 * @return Cells Definition Array
	 * @throws InvalidMapException
	 * @throws InvalidConstantValueException
	 */
	public final Cell[][] getCellsDefinition() throws InvalidMapException {
		Assert.check(!_isCellArrayUpdated, "This method can't be executed more than once");
		_isCellArrayUpdated = true;

		final int size = _cityList.size();
		if (size < 2) {
			final String message = "This map contains only {0} city.";
			final String finalMessage = StringManipulatorHelper.buildMessage(message, new Integer(size));
			throw new InvalidMapException(finalMessage, ValidationResult.NOT_ENOUGH_CITY);
		}

		// Define the areas & its contained cells
		defineArea();

		return _cells;
	}

	/**
	 * D�finit le num�ro de chacune des r�gions. Pour d�finir les r�gions d'une
	 * carte, la m�thode setFSMapCells doit �tre utilis�.
	 * 
	 * @throws InvalidMapException
	 * @throws InvalidConstantValueException
	 * @see org.jocelyn_chaumel.firststrike.core.map.Cell
	 */
	private void defineArea() throws InvalidMapException {
		Assert.precondition(_cells != null, "This object contains no map.");

		AreaTypeCst areaType = null;
		int id = 1;
		int mapHeight = _cells.length;
		int mapWidth = _cells[0].length;
		Cell currentCell = null;
		CellTypeCst cellType = null;
		final CoordList groundCellMap = new CoordList();
		final CoordList mountainCellMap = new CoordList();
		final CoordList watherCellMap = new CoordList();
		final CoordList riverCellMap = new CoordList();

		Coord currentCoord = null;
		// R�partition des cellules par cat�gorie
		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
				currentCell = _cells[y][x];
				cellType = currentCell.getCellType();
				try {
					areaType = AreaTypeCst.getInstanceFromCellType(cellType);
				} catch (InvalidConstantValueException ex) {
					final String message = "Unsupported ground type at position {0}";
					final String finalMessage = StringManipulatorHelper.buildMessage(message, currentCoord);
					throw new InvalidMapException(finalMessage, ValidationResult.INVALID_CELL_TYPE);
				}

				if (currentCell.isNearToWather() && currentCell.isCity()) {
					watherCellMap.add(currentCell.getCellCoord());
				}

				if (AreaTypeCst.GROUND_AREA.equals(areaType)) {
					groundCellMap.add(currentCell.getCellCoord());
				} else if (AreaTypeCst.MOUNTAIN_AREA.equals(areaType)) {
					mountainCellMap.add(currentCell.getCellCoord());
				} else if (AreaTypeCst.SEA_AREA.equals(areaType)) {
					watherCellMap.add(currentCell.getCellCoord());
				} else if (AreaTypeCst.RIVER_AREA.equals(areaType))
					riverCellMap.add(currentCell.getCellCoord());
				else {
					final String message = "Unsupported ground type at position {0}";
					final String finalMessage = StringManipulatorHelper.buildMessage(message, currentCoord);
					throw new InvalidMapException(finalMessage, ValidationResult.INVALID_CELL_TYPE);
				}
			}
		}

		boolean portCityMandatory = false;
		if (!watherCellMap.isEmpty()) {
			_seaArea = defineAreaByType(watherCellMap, new SeaAreaId(id), false);
			if (!watherCellMap.isEmpty()) {
				final String message = "This map contains more than one sea area.";
				throw new InvalidMapException(message, ValidationResult.DIFFERENT_WATHER_CONTACT);
			}
			portCityMandatory = true;
		}

		while (!groundCellMap.isEmpty()) {
			defineAreaByType(groundCellMap, new GroundAreaId(++id), portCityMandatory && _applyValidation);
		}

		while (!mountainCellMap.isEmpty()) {
			defineAreaByType(mountainCellMap, new MountainAreaId(++id), false);
		}

		while (!riverCellMap.isEmpty()) {
			defineAreaByType(riverCellMap, new RiverAreaId(++id), false);
		}

		defineExtraEdgeCell();
	}

	/**
	 * @param p_coordBag
	 *            Map containing (Coord, Cell)
	 * @param p_id
	 * @throws InvalidMapException
	 */
	private Area defineAreaByType(final CoordList p_coordBag, final AbstractAreaId p_id,
			final boolean p_isPortCityMandatory) throws InvalidMapException {
		Assert.preconditionNotNull(p_coordBag, "Cell Map");
		Assert.preconditionNotNull(p_id, "Area Id");

		Coord currentCoord = (Coord) p_coordBag.get(0);
		final CoordList filledCoord = FillingTool.fillFromCoord(p_coordBag, currentCoord);

		Cell currentCell = null;
		final Area area = new Area(p_id);
		boolean areaWithPortCity = false;
		boolean areaWithCity = false;
		_areaList.add(area);
		final Iterator<Coord> coordIter = filledCoord.iterator();
		while (coordIter.hasNext()) {
			currentCoord = coordIter.next();
			currentCell = _cells[currentCoord.getY()][currentCoord.getX()];
			if (currentCell.isCity()){
				areaWithCity = true;
				if (currentCell.isNearToWather()) {
					areaWithPortCity = true;
				}
			}
			linkAreaAndCell(area, currentCell);
		}

		if (areaWithCity && p_isPortCityMandatory && !areaWithPortCity) {
			throw new InvalidMapException("This ground area didn't have port city: " + area.getInternalAreaName(),
					ValidationResult.AREA_WITHOUT_CONTACT);
		}
		return area;
	}

	private void defineExtraEdgeCell() {
		AreaList currentAreaList;
		Area currentArea;
		AreaList exploredAreaList;
		for (int y = 0; y < _cells.length; y++) {
			for (int x = 0; x < _cells[y].length; x++) {
				currentAreaList = _cells[y][x].getAreaList().getAllGroundAreaSetWithCity();
				if (currentAreaList.size() == 0) {
					continue;
				}
				Assert.check(currentAreaList.size() == 1, "Cell assigned to more than 1 groundArea");
				currentArea = currentAreaList.get(0);

				if (y > 0 && x > 0) {
					exploredAreaList = _cells[y - 1][x - 1].getAreaList();
					if (!exploredAreaList.contains(currentArea)) {
						currentArea.getExtractEdgeCoordSet().add(_cells[y - 1][x - 1].getCellCoord());
					}
				}
				if (y > 0) {
					exploredAreaList = _cells[y - 1][x].getAreaList();
					if (!exploredAreaList.contains(currentArea)) {
						currentArea.getExtractEdgeCoordSet().add(_cells[y - 1][x].getCellCoord());
					}
				}
				if (y > 0 && x + 1 < _cells[y].length) {
					exploredAreaList = _cells[y - 1][x + 1].getAreaList();
					if (!exploredAreaList.contains(currentArea)) {
						currentArea.getExtractEdgeCoordSet().add(_cells[y - 1][x + 1].getCellCoord());
					}
				}
				if (x > 0) {
					exploredAreaList = _cells[y][x - 1].getAreaList();
					if (!exploredAreaList.contains(currentArea)) {
						currentArea.getExtractEdgeCoordSet().add(_cells[y][x - 1].getCellCoord());
					}
				}
				if (x + 1 < _cells[y].length) {
					exploredAreaList = _cells[y][x + 1].getAreaList();
					if (!exploredAreaList.contains(currentArea)) {
						currentArea.getExtractEdgeCoordSet().add(_cells[y][x + 1].getCellCoord());
					}
				}
				if (y + 1 < _cells.length && x > 0) {
					exploredAreaList = _cells[y + 1][x - 1].getAreaList();
					if (!exploredAreaList.contains(currentArea)) {
						currentArea.getExtractEdgeCoordSet().add(_cells[y + 1][x - 1].getCellCoord());
					}
				}
				if (y + 1 < _cells.length) {
					exploredAreaList = _cells[y + 1][x].getAreaList();
					if (!exploredAreaList.contains(currentArea)) {
						currentArea.getExtractEdgeCoordSet().add(_cells[y + 1][x].getCellCoord());
					}
				}
				if (y + 1 < _cells.length && x + 1 < _cells[y + 1].length) {
					exploredAreaList = _cells[y + 1][x + 1].getAreaList();
					if (!exploredAreaList.contains(currentArea)) {
						currentArea.getExtractEdgeCoordSet().add(_cells[y + 1][x + 1].getCellCoord());
					}
				}
			}

		}
	}

	/**
	 * Add the area in the cell's available areas list & add the cell in the
	 * area's available cells list.
	 * 
	 * @param p_area
	 *            Available area to link
	 * @param p_cell
	 *            Available cell to link
	 */
	private void linkAreaAndCell(final Area p_area, final Cell p_cell) {
		p_area.getCellSet().add(p_cell);
		p_cell.addArea(p_area);
	}

	/**
	 * Return the city list of the map.
	 * 
	 * @return City list.
	 */
	public CityList getCityList() {
		Assert.check(_isCellArrayUpdated, "This method can't be executed more than one time");
		return _cityList;
	}

	/**
	 * Return the area list of the map.
	 * 
	 * @return Area List
	 */
	public AreaList getAreaList() {
		Assert.check(_isCellArrayUpdated, "This method can't be executed more than one time");
		return _areaList;
	}

	/**
	 * Return the unique Sea Area of the map.
	 * 
	 * @return Sea Area
	 */
	public Area getSeaArea() {
		return _seaArea;
	}
}