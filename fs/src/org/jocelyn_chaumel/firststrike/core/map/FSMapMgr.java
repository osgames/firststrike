/*
 * FSMapMgr.java Created on 23 f�vrier 2003, 14:01
 */

package org.jocelyn_chaumel.firststrike.core.map;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaList;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.MapScenario;
import org.jocelyn_chaumel.firststrike.core.map.scenario.xml.ScenariiNode;
import org.jocelyn_chaumel.firststrike.core.map.xml.FSAreasArrayNode;
import org.jocelyn_chaumel.firststrike.core.map.xml.FSCellsArrayNode;
import org.jocelyn_chaumel.firststrike.core.map.xml.FSInfoNode;
import org.jocelyn_chaumel.firststrike.core.map.xml.FSMapNode;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.Dir;
import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;
import org.jocelyn_chaumel.tools.debugging.Assert;
import org.jocelyn_chaumel.tools.zip.ZipMgr;

/**
 * Cette classe permet de faire la gestion des cartes. Tout d'abord, cette
 * classe repr�sente un singleton. Le gestionnaire de carte ne peut contenir
 * qu'une seule carte � la fois.
 */

public final class FSMapMgr implements Serializable
{
	private static final FSLogger _logger = FSLoggerManager.getLogger(FSMapMgr.class);
	public static final String VERSION = "2.0.0";
	public static final int MAP_MIN_XY = 5;
	public static final int MAP_MAX_XY = 2000;
	public static final String XML_MAP_FILE_EXTENTION = ".fsm";
	public static final String XML_SCENARIO_FILE_EXTENTION = ".fss";
	public static final String XML_ZIP_MAP_FILE_EXTENTION = ".fsz";
	
	private final PathMgr _pathMgr;
	private final FSMap _map;
	private int _statistic_seaPercentage = -1;

	/**
	 * Constructeur param�tris�.
	 */
	public FSMapMgr(final FSMap p_map)
	{
		Assert.preconditionNotNull(p_map);

		_map = p_map;
		_pathMgr = new PathMgr(p_map);
	}

	public PathMgr getPathMgr()
	{
		return _pathMgr;
	}

	public static void extractMaps() throws IOException
	{
		_logger.systemAction("Extract maps");
		;
		final File mapSourceDir = ApplicationConfigurationMgr.getCreateInstance(FileMgr.getCurrentDir()).getMapDir();
		final File[] zippedMapArray = mapSourceDir.listFiles(new FileFilter()
		{

			@Override
			public boolean accept(File p_arg0)
			{
				String filename = p_arg0.getName().toLowerCase();
				return filename.endsWith(XML_ZIP_MAP_FILE_EXTENTION);
			}
		});

		String zippedMapFilename;
		String mapDirname;
		for (int i = 0; i < zippedMapArray.length; i++)
		{
			zippedMapFilename = zippedMapArray[i].getAbsolutePath();
			mapDirname = zippedMapFilename.substring(	0,
														zippedMapFilename.length()
																- XML_ZIP_MAP_FILE_EXTENTION.length());
			ZipMgr.extractZip(zippedMapArray[i], new File(mapDirname), true);
			_logger.systemSubAction("Map extracted : " + zippedMapFilename);
		}
	}

	public static ArrayList<String> retrieveMapNameList()
	{
		final ArrayList<String> mapNameList = new ArrayList<String>();
		final File mapDir = ApplicationConfigurationMgr.getInstance().getMapDir();
		final Dir dir = new Dir(mapDir);
		final File[] mapArray = dir.getDirList();
		File mapFile;
		for (int i = 0; i < mapArray.length; i++)
		{
			mapFile = new File(mapArray[i], mapArray[i].getName() + FSMapMgr.XML_MAP_FILE_EXTENTION);
			if (mapFile.isFile())
			{
				mapNameList.add(mapArray[i].getName());
			}
		}
		Collections.sort(mapNameList);
		return mapNameList;
	}

	public static ArrayList<MapScenario> retrieveScenarioList(final File p_mapDir) throws IOException,
			InvalidScenarioException
	{
		Assert.preconditionNotNull(p_mapDir, "Map Dir");
		Assert.precondition(p_mapDir.isDirectory(), "Invalid map directory");
		final String mapName = p_mapDir.getName();
		ArrayList<MapScenario> scenarioList = new ArrayList<MapScenario>();
		final File scenariiFile = new File(p_mapDir, mapName + XML_SCENARIO_FILE_EXTENTION);
		if (!scenariiFile.isFile())
		{
			return scenarioList;
		}

		final FileInputStream fileInput = new FileInputStream(scenariiFile);
		final BufferedInputStream bufferInput = new BufferedInputStream(fileInput);

		final SAXBuilder xmlReader = new SAXBuilder();

		try
		{
			Document xmlDoc;
			try
			{
				xmlDoc = xmlReader.build(bufferInput);
				scenarioList = ScenariiNode.getScenarioList(xmlDoc.getRootElement());
			} catch (JDOMException ex)
			{
				_logger.gameError("Unexpected error occurs during scenario loading file (XML).", ex);
				throw new InvalidScenarioException("Invalid XML Format detected", ex);
			}
		} finally
		{
			bufferInput.close();
			fileInput.close();
		}

		return scenarioList;
	}

	/**
	 * Effectue la lecture d'une carte depuis un fichier disque. Lorsque
	 * celle-ci est charg�e, la d�finition des r�gions est entamm�e. Pour
	 * conna�tre la notion d'une r�gion, voir la d�finition d'une cellule
	 * <code>{@link org.jocelyn_chaumel.firststrike.core.map.Cell}</code>.
	 * 
	 * @param p_file Le fichier contenant la carte.
	 * @throws JDOMException
	 * @throws InvalidMapException
	 * @throws MapValidationException
	 * @throws InvalidConstantValueException
	 */
	public static FSMap loadMap(final File p_file, final boolean p_applyValidation) throws IOException,
			FileNotFoundException,
			JDOMException,
			InvalidMapException
	{
		Assert.preconditionNotNull(p_file);
		Assert.preconditionIsFile(p_file);

		// Lecture de la carte
		final FileInputStream fileInput = new FileInputStream(p_file);
		final BufferedInputStream bufferInput = new BufferedInputStream(fileInput);

		final SAXBuilder xmlReader = new SAXBuilder();

		String mapFilename = FileMgr.getFileNameWithoutExtention(p_file);

		try
		{
			final Document xmlDoc = xmlReader.build(bufferInput);
			return FSMapNode.getFSMap(mapFilename, xmlDoc.getRootElement(), p_applyValidation);
		} catch (Exception ex)
		{
			throw new FSFatalException(ex);
		} finally
		{
			bufferInput.close();
			fileInput.close();
		}
	}

	public final FSMap getMap()
	{
		return _map;
	}

	public final String getMapName()
	{
		return _map.getName();
	}

	public final String getMapTitle()
	{
		return _map.getTitle();
	}

	public final AreaList getAreaSet()
	{
		return _map.getAreaSet();
	}

	public static FSMap create(final FSMapInfo p_mapInfo, final Cell[][] p_cellArray, final AreaNameMap p_areaNameMap, final boolean p_applyValidation) throws InvalidMapException
	{
		Assert.preconditionNotNull(p_mapInfo, "Map Info");
		Assert.preconditionNotNull(p_cellArray, "Cell Array");
		final FSMapCells mapCells = new FSMapCells(p_cellArray, p_areaNameMap, p_applyValidation);

		return new FSMap(p_mapInfo, mapCells);
	}

	public static void saveMap(final FSMap p_map, final File p_file) throws IOException
	{
		Assert.preconditionNotNull(p_map, "Map");
		Assert.preconditionNotNull(p_file, "Destination Directory");
		Assert.precondition(!p_file.exists(), "Destination already exists : <" + p_file.getAbsolutePath() + ">");

		FileOutputStream fileOutput = null;
		BufferedOutputStream bufferOutput = null;

		final FSMapInfo mapInfo = p_map.getFSMapInfo();
		final FSInfoNode infoNode = new FSInfoNode(mapInfo);
		final FSAreasArrayNode areaNode = new FSAreasArrayNode(p_map.getAreaSet().getAllGroundAreaSetWithCity());

		final FSCellsArrayNode cellsNode = new FSCellsArrayNode(p_map.getFSMapCells().getCells(),
																mapInfo.getX(),
																mapInfo.getY());
		final FSMapNode mapNode = new FSMapNode(infoNode, areaNode, cellsNode);
		final Document doc = new Document(mapNode);

		try
		{
			fileOutput = new FileOutputStream(p_file);
			bufferOutput = new BufferedOutputStream(fileOutput);

			final Format format = Format.getPrettyFormat();
			final XMLOutputter outputter = new XMLOutputter(format);
			outputter.output(doc, bufferOutput);

		} catch (Exception ex)
		{
			throw new FSFatalException(ex);
		} finally
		{
			if (bufferOutput != null)
			{
				bufferOutput.flush();
				bufferOutput.close();
			}
			if (fileOutput != null)
			{
				fileOutput.flush();
				fileOutput.close();
			}
		}
	}

	/**
	 * Calculates and returns a path from a start point to a destination point
	 * for an unit.
	 * 
	 * @param p_endPos Destination point.
	 * @param p_coordSetToExclude Coord list to exclude from the path (Must be
	 *        include all enemies's cities if necessary. This information is not
	 *        calculated in this method).
	 * @param p_unit Unit to move
	 * @param p_removeDestinationFromPath
	 * @return A path
	 * @throws PathNotFoundException
	 */
	public final Path getPath(	final Coord p_endPos,
								final CoordSet p_coordSetToExclude,
								final AbstractUnit p_unit,
								final boolean p_excludeDestinationCoord) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_endPos, "Destination Coord");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord List to exclude");
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.precondition(p_excludeDestinationCoord == true
									|| (p_excludeDestinationCoord == false && !p_coordSetToExclude.contains(p_endPos)),
							"CoordSetToExclude contains the ending position");

		final CoordSet excludedCoordSet;
		if (p_excludeDestinationCoord && p_coordSetToExclude.contains(p_endPos))
		{
			excludedCoordSet = (CoordSet) p_coordSetToExclude.clone();
			excludedCoordSet.remove(p_endPos);
		} else
		{
			excludedCoordSet = p_coordSetToExclude;
		}
		Assert.precondition(!excludedCoordSet.contains(p_endPos),
							"Destination Coord is contained in the excluded coord list");

		final AbstractMoveCost moveCost = p_unit.getMoveCost();
		final AreaTypeCst areaType = moveCost.getAreaTypeCst();

		final Coord startCoord = p_unit.getPosition();
		final AbstractPlayer player = p_unit.getPlayer();
		final Path path;
		if (areaType.equals(AreaTypeCst.ALL_AREAS))
		{
			final int fuel = ((Fighter) p_unit).getFuel() / MoveCostFactory.AIR_MOVE_COST.getMinCost();
			final int fuelCapacity = ((Fighter) p_unit).getFuelCapacity()/ MoveCostFactory.AIR_MOVE_COST.getMinCost();
			path = _pathMgr.getAirPath(	startCoord,
										p_endPos,
										fuel,
										fuelCapacity,
										player,
										excludedCoordSet,
										p_excludeDestinationCoord);
		} else
		{
			path = _pathMgr.getAreaPath(startCoord,
										p_endPos,
										moveCost,
										p_unit.getMove(),
										p_unit.getMovementCapacity(),
										excludedCoordSet,
										p_excludeDestinationCoord);
		}

		// Les assertions sont faites dans la m�thode appell�e
		return path;
	}

	/**
	 * Return the total distance (move pts) for a specified path.
	 * 
	 * @param p_path Path to inspect
	 * @param p_moveCost
	 * @return A distance
	 */
	public final int getPathDistance(final Path p_path, final AbstractMoveCost p_moveCost)
	{
		Assert.preconditionNotNull(p_path, "Path");
		Assert.preconditionNotNull(p_moveCost, "Move Cost");
		Assert.precondition(p_path.hasNextStep(), "Invalid Path");

		final CoordList coordList = p_path.getCoordList();
		final Iterator iter = coordList.iterator();
		Coord currentCoord;
		Cell currentCell;
		int distance = 0;
		while (iter.hasNext())
		{
			currentCoord = (Coord) iter.next();
			currentCell = _map.getCellAt(currentCoord);
			distance += p_moveCost.getCost(currentCell);
		}

		return distance;
	}

	/**
	 * Indicate if an unit can move a specific cell (Coord).
	 * 
	 * @param p_coord Cell identification.
	 * @param p_moveCost Move Cost unit.
	 * 
	 * @return Accessible or not.
	 */
	public final boolean isAccessibleCell(final Coord p_coord, final AbstractMoveCost p_moveCost)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		Assert.preconditionNotNull(p_moveCost, "Unit Move Cost");

		final Cell cell = _map.getCellAt(p_coord);
		final int moveCost = p_moveCost.getCost(cell);
		return moveCost != -1;
	}

	public final boolean isAccessibleCell(final int p_move, final Coord p_coord, final AbstractMoveCost p_moveCost)
	{
		Assert.precondition(p_move >= 0, "Invalid move");
		Assert.preconditionNotNull(p_coord, "Coord");
		Assert.preconditionNotNull(p_moveCost, "Unit Move Cost");

		if (p_move == 0)
		{
			return false;
		}
		final Cell cell = _map.getCellAt(p_coord);
		final int moveCost = p_moveCost.getCost(cell);
		if (moveCost == AbstractMoveCost.UNREACHABLE_CELL)
		{
			return false;
		}

		return p_move >= moveCost;
	}

	/**
	 * Verify is an unit can move to a specific destination.
	 * 
	 * @param p_unit Unit to move
	 * @param p_destinationPos Destination objective
	 * @param p_coordSetToExclude Coord list to exclude from the path
	 * @return
	 */
	public final boolean isAccessibleCoord(	final AbstractUnit p_unit,
											final Coord p_destinationPos,
											final CoordSet p_coordSetToExclude,
											final boolean p_excludeDestinationCoord)
	{
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.preconditionNotNull(p_destinationPos, "Destination Coord");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord Set to Exclude");
		Assert.precondition(!p_coordSetToExclude.contains(p_unit.getPosition()),
							"Coord Set to exclude contains the start position");
		Assert.precondition(!p_unit.getPosition().equals(p_destinationPos), "Unit already at destination");

		if (!p_excludeDestinationCoord && p_coordSetToExclude.contains(p_destinationPos))
		{
			return false;
		}

		final AbstractMoveCost moveCost = p_unit.getMoveCost();
		final Coord startPosition = p_unit.getPosition();
		final AreaTypeCst areaType = moveCost.getAreaTypeCst();
		final AbstractPlayer player = p_unit.getPlayer();

		if (AreaTypeCst.ALL_AREAS.equals(areaType))
		{
			final Fighter fighter = (Fighter) p_unit;
			final int fuel;
			if (player instanceof HumanPlayer)
			{
				fuel = Integer.MAX_VALUE;
			} else
			{
				fuel = fighter.getFuel() / MoveCostFactory.AIR_MOVE_COST.getMinCost();
			}
			final int fuelCapacity = fighter.getFuelCapacity() / MoveCostFactory.AIR_MOVE_COST.getMinCost();
			return _pathMgr.isAccessibleAirCoord(	startPosition,
													p_destinationPos,
													fuel,
													fuelCapacity,
													player,
													p_coordSetToExclude,
													p_excludeDestinationCoord);
		} else
		{
			return _pathMgr.isAccessibleAreaCoord(	startPosition,
													p_destinationPos,
													moveCost,
													p_coordSetToExclude,
													p_excludeDestinationCoord);
		}
	}

	public final boolean containsAccessibleCoord(	final AbstractUnit p_unit,
													final CoordSet p_coordSet,
													final CoordSet p_coordSetToExclude,
													final boolean p_excludeDestinationCoord)
	{
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.preconditionNotNull(p_coordSet, "Coord Set");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord Set to exclude");
		Assert.precondition(!p_coordSetToExclude.contains(p_unit.getPosition()),
							"Coord Set to exclude contains the start position");
		Assert.precondition(!p_coordSet.isEmpty(), "Empty Coord Set");

		final Coord unitCoord = p_unit.getPosition();
		Coord currentCoord = null;
		final Iterator coordIter = p_coordSet.iterator();
		while (coordIter.hasNext())
		{
			currentCoord = (Coord) coordIter.next();
			if (currentCoord.calculateIntDistance(unitCoord) == 1)
			{
				return true;
			}
			if (isAccessibleCoord(p_unit, currentCoord, p_coordSetToExclude, p_excludeDestinationCoord))
			{
				return true;
			}
		}

		return false;
	}

	public CoordSet calculateCellsToExclude(final AbstractUnit p_unit)
	{
		final AbstractPlayer player = p_unit.getPlayer();
		final CoordSet coordSet = player.getEnemySet().getCoordSet();
		if (!UnitTypeCst.TANK.equals(p_unit.getUnitType()))
		{
			coordSet.addAll(this.getCityList().getEnemyCity(player).getCoordSet());
		}

		return coordSet;
	}

	public CoordSet getAccessibleCells(final AbstractUnit p_unit, final int p_move)
	{
		final CoordSet excludedCoordSet = calculateCellsToExclude(p_unit);

		final CoordSet coordSet = CoordSet.createInitiazedCoordSet(	p_unit.getPosition(),
																	p_move,
																	_map.getWidth(),
																	_map.getHeight());
		coordSet.remove(p_unit.getPosition());

		final CoordSet finalCoordSet = new CoordSet();

		for (Coord currentCoord : coordSet)
		{
			if (this.isAccessibleCoord(p_unit, currentCoord, excludedCoordSet, false))
			{
				try
				{
					final Path path = this.getPath(currentCoord, excludedCoordSet, p_unit, false);
					if (p_move >= this.getPathDistance(path, p_unit.getMoveCost()))
					{
						finalCoordSet.add(currentCoord);
					}
				} catch (PathNotFoundException ex)
				{
					// NOP
				}
			}
		}
		return finalCoordSet;
	}

	public CoordSet getAccessibleCells(final AbstractUnit p_unit)
	{
		final int move;
		if (p_unit.getFuelCapacity() > 0)
		{
			move = Math.min(p_unit.getMove(), p_unit.getFuel());
		} else
		{
			move = p_unit.getMove();
		}

		return getAccessibleCells(p_unit, move);
	}

	public final Area getAreaAt(final Coord p_position, final AreaTypeCst p_areaType)
	{
		Assert.preconditionNotNull(p_position, "Position");
		Assert.precondition(_map.getHeight() > p_position.getY(), "Invalid Y Position");
		Assert.precondition(p_position.getY() >= 0, "Invalid Y Position");

		Assert.precondition(_map.getWidth() > p_position.getX(), "Invalid X Position");
		Assert.precondition(p_position.getX() >= 0, "Invalid X Position");

		final AreaList areaIdSet = _map.getCellAt(p_position).getAreaList();
		return areaIdSet.getFirstAreaByType(p_areaType);
	}

	public final CoordSet getWatherCoordNearToGroundCoord(final Coord p_groundCoord)
	{
		Assert.preconditionNotNull(p_groundCoord, "Ground Coord");
		return _map.getWatherCoordNearToGroundCoord(p_groundCoord);
	}

	public final CityList getCityList()
	{
		return _map.getFSMapCells().getCityList();
	}
	
	public short getPercentageOfSea()
	{
		return (short) (100f * _map.getSeaArea().getCellSet().size() / (float) (_map.getFSMapInfo().getX() * _map.getFSMapInfo().getY()));
	}
}