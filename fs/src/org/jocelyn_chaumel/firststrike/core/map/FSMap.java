/*
 * FSMap.java Created on 21 f�vrier 2003, 23:03
 */

package org.jocelyn_chaumel.firststrike.core.map;

import java.io.Serializable;
import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaList;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe contient toutes les informations et les cellules d'une carte.
 * 
 * @author Jocelyn Chaumel
 */

public final class FSMap implements Serializable
{
	private final FSMapInfo _info;
	private final FSMapCells _cells;
	private CoordSet _cellCoordNearToWather = null;
	private CoordSet _cellCoordWatherAndBeach = null;

	public FSMap(final FSMapInfo p_mapInfo, final FSMapCells p_mapCells)
	{
		Assert.preconditionNotNull(p_mapInfo, "Map Info");
		Assert.preconditionNotNull(p_mapCells, "Map Cells");

		_info = p_mapInfo;
		_cells = p_mapCells;
	}

	/**
	 * REtourne les informations de la carte.
	 * 
	 * @return Information de la carte.
	 */
	public final FSMapInfo getFSMapInfo()
	{
		return _info;
	}

	/**
	 * Retourne les cellules de la carte.
	 * 
	 * @return Ensemble des cellules de la carte.
	 */
	public final FSMapCells getFSMapCells()
	{
		return _cells;
	}

	/**
	 * Retourne la version de la carte.
	 * 
	 * @return La version.
	 */
	public final String getVersion()
	{
		return _info.getVersion();
	}

	/**
	 * Indique si deux position se retrouve dans la m�me r�gion.
	 * 
	 * @param p_startCoord
	 * @param p_endCoord
	 * @param p_areaType
	 * @return
	 */
	public final boolean isSameArea(final Coord p_startCoord, final Coord p_endCoord, final AreaTypeCst p_areaType)
	{
		Assert.precondition(p_startCoord != null, "Start position is null.");
		Assert.precondition(p_endCoord != null, "End position is null.");
		Assert.preconditionNotNull(p_areaType, "Area Type");
		Assert.precondition(_info != null, "Map isn't a valid map.  Info properties needed.");
		Assert.precondition(_cells != null, "Map isn't a valid map. Cell list properties needed.");

		Assert.precondition(isBoundedCoord(p_startCoord), "Invalid Start Coord");
		Assert.precondition(isBoundedCoord(p_endCoord), "Invalid End Coord");

		return _cells.isSameArea(p_startCoord, p_endCoord, p_areaType);
	}

	/**
	 * Retourne la hauteur de la carte courante.
	 * 
	 * @return La hauteur.
	 */
	public final int getHeight()
	{
		Assert.precondition(_cells != null, "No cells set.");
		return _cells.getHeight();
	}

	/**
	 * Retourne la largeur de la carte courante.
	 * 
	 * @return La largeur.
	 */
	public final int getWidth()
	{
		Assert.precondition(_cells != null, "No cells set.");
		return _cells.getWidth();
	}

	/**
	 * Retourne la cellule � aux coordonn�es sp�cifi�es.
	 * 
	 * @param p_coord Coordonn�es.
	 * @return Une cellule.
	 */
	public final Cell getCellAt(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		Assert.precondition(_cells != null, "No cells set.");
		Assert.precondition(p_coord.getX() > -1, "Map out of bound(x axis)[" + p_coord.getX() + "]");
		Assert.precondition(p_coord.getY() > -1, "Map out of bound(y axis)[" + p_coord.getY() + "]");
		Assert.precondition(p_coord.getX() < getWidth(), "Map out of bound(x axis). [" + (getWidth() - 1) + "] "
				+ p_coord.getX() + ".");
		Assert.precondition(p_coord.getY() < getHeight(), "Map out of bound(y axis). [" + (getHeight() - 1) + "] "
				+ p_coord.getY() + ".");

		return _cells.getCellAt(p_coord);
	}

	/**
	 * Retourne l'Area Id d'une cellule et d'apr�s un type donn�.
	 * 
	 * @param p_coord Emplacement de la cellule
	 * @param p_areaType Type de la cellule
	 * @return AreaId
	 */
	public final Area getAreaAt(final Coord p_coord, final AreaTypeCst p_areaType)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		Assert.preconditionNotNull(p_areaType, "Area Type");

		return _cells.getCellAt(p_coord).getAreaList().getFirstAreaByType(p_areaType);
	}

	/**
	 * Retourne la cellule aux coordonn�es x y sp�cifi�es.
	 * 
	 * @param p_x Coordonn� x.
	 * @param p_y Coordonn� y.
	 * @return Une cellule.
	 */
	public final Cell getCellAt(final int p_x, final int p_y)
	{
		Assert.precondition(this.isBoundedCoord(p_x, p_y), "Invalid X Y Coord");
		return _cells.getCellAt(p_x, p_y);
	}

	/**
	 * Indique si la cellule est une ville.
	 * 
	 * @param p_coord Position de la cellule.
	 * @return Vrai ou faux.
	 */
	public final boolean isCity(final Coord p_coord)
	{
		Assert.precondition(p_coord != null, "Null param.");

		return isCity(p_coord.getX(), p_coord.getY());
	}

	/**
	 * Indique si la cellule est une ville.
	 * 
	 * @param x Position x.
	 * @param y Position y.
	 * @return Vrai ou faux.
	 */
	public final boolean isCity(final int x, final int y)
	{
		Assert.precondition(isBoundedCoord(x, y), "Invalid coord.");

		return CellTypeCst.CITY.equals(getCellAt(x, y).getCellType());
	}

	public final boolean isBoundedCoord(final Coord p_coord)
	{
		Assert.precondition(p_coord != null, "Null param.");

		return isBoundedCoord(p_coord.getX(), p_coord.getY());
	}

	public final boolean isBoundedCoord(final int x, final int y)
	{
		if ((x < 0) || (y < 0) || (y >= getHeight()) || (x >= getWidth()))
			return false;

		return true;
	}

	public final String getName()
	{
		return this._info.getName();
	}

	public final String getTitle()
	{
		return _info.getTitle();
	}

	public final String getDescription()
	{
		return _info.getDescription();
	}

	public final CoordSet getWatherCoordNearToGroundCoord(final Coord p_groundCoord)
	{
		Assert.preconditionNotNull(p_groundCoord, "Ground Coord");
		Assert.precondition(_cells.getCellAt(p_groundCoord).isGroundCell(), "Sea Cell Coord expected : "
				+ p_groundCoord);

		final CoordSet coordSet = _cells.getWatherCellNearToGroundCell(p_groundCoord);
		Assert.check(!coordSet.isEmpty(), "this ground cell is too far from sea");
		return coordSet;
	}

	public final AreaList getAreaSet()
	{
		return _cells.getAreaSet();
	}

	public final CityList getCityList()
	{
		return _cells.getCityList();
	}

	/**
	 * Return the unique Sea Area of the map.
	 * 
	 * @return Sea Area.
	 */
	public final Area getSeaArea()
	{
		return _cells.getSeaArea();
	}

	/**
	 * Returns a coordSet containing all cell coord near to wather in the map.
	 * 
	 * @return a coordSet.
	 */
	public final CoordSet getCellCoordNearToWather()
	{
		if (_cellCoordNearToWather != null)
		{
			return _cellCoordNearToWather;

		}
		_cellCoordNearToWather = new CoordSet();
		final AreaList groundAreaSet = getAreaSet().getAllGroundAreaSet();
		Area currentArea = null;
		CoordSet currentCoordSet = null;
		final Iterator<Area> areaIter = groundAreaSet.iterator();
		while (areaIter.hasNext())
		{
			currentArea = areaIter.next();
			currentCoordSet = currentArea.getCellSet().getGroundCellNearToSea().getCoordSet();
			_cellCoordNearToWather.addAll(currentCoordSet);
		}
		return _cellCoordNearToWather;
	}

	// TOOD AAAAAAZ TU
	public final boolean isGroundCellNearToWather(final Coord p_coord, final short p_distance)
	{
		final int maxX = getWidth();
		final int maxY = getHeight();
		final int x = p_coord.getX();
		final int y = p_coord.getY();
		int i = 1;
		for (; i < p_distance; i++)
		{
			// North
			if (y - i > 0 && getCellAt(x, y - i).isNearToWather())
			{
				return false;
			}
			// West
			if (x + i < maxX && getCellAt(x + i, y).isNearToWather())
			{
				return false;
			}
			// South
			if (y + i < maxY && getCellAt(x, y + i).isNearToWather())
			{
				return false;
			}
			// East
			if (x - i > 0 && getCellAt(x - i, y).isNearToWather())
			{
				return false;
			}
		}

		// North
		if (y - i > 0 && getCellAt(x, y - i).isNearToWather())
		{
			return true;
		}
		// West
		if (x + i < maxX && getCellAt(x + i, y).isNearToWather())
		{
			return true;
		}
		// South
		if (y + i < maxY && getCellAt(x, y + i).isNearToWather())
		{
			return true;
		}
		// East
		if (x - i > 0 && getCellAt(x - i, y).isNearToWather())
		{
			return true;
		}

		return false;
	}

	// AAAAAZ plutot utiliser (moins long lorsque la carte comporte beaucoup d'eau
	// final AbstractUnitList enemyList = player.getEnemySet().retrieveAccessibleUnitBySea(player.getMapMgr().getMap());

	public final CoordSet getWatherAndBeachCellCoord()
	{
		if (_cellCoordWatherAndBeach == null)
		{
			final Area seaArea = getSeaArea();
			if (seaArea == null)
			{
				_cellCoordWatherAndBeach = new CoordSet();
			} else
			{
				_cellCoordWatherAndBeach = seaArea.getCellSet().getCoordSet();
				_cellCoordWatherAndBeach.addAll(getCellCoordNearToWather());
			}
		}

		return _cellCoordWatherAndBeach;
	}

	/**
	 * checks if the cells near to a specific coord is a ground cell.
	 * 
	 * @param p_coord current coord
	 * @return True if a ground cell is detected arround the current coord.
	 *         Otherwise, false.
	 */
	public final boolean isGroundCellNearToCoord(final Coord p_coord)
	{
		// AAAZ TU
		final CoordSet set = CoordSet.createInitiazedCoordSet(p_coord, 1, getWidth(), getHeight());

		Cell currentCell = null;
		Coord currentCoord = null;
		final Iterator<Coord> coordIter = set.iterator();
		while (coordIter.hasNext())
		{
			currentCoord = coordIter.next();
			currentCell = getCellAt(currentCoord);
			if (currentCell.isGroundCell())
			{
				return true;
			}
		}
		return false;
	}

	public final AreaNameMap getAreaNameMap()
	{
		final AreaNameMap areaNameMap = new AreaNameMap();
		Coord areaCoord = null;
		String areaName = null;
		for (Area currentArea : getAreaSet().getAllGroundAreaSetWithCity())
		{
			if (currentArea.hasAreaName())
			{
				areaCoord = currentArea.getFirstCoordOfArea();
				areaName = currentArea.getInternalAreaName();
				areaNameMap.put(areaCoord, areaName);
			}
		}

		return areaNameMap;
	}

}