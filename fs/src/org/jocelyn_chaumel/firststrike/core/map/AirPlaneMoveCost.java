/*
 * Created on May 1, 2005
 */
package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public final class AirPlaneMoveCost extends AbstractMoveCost
{

	private final int FIGHTER_MOVE_COST = 10;
	
	/**
	 * Constructeur par d�faut.
	 * 
	 * @param anUniqueId Idenfiant unique.
	 * @see AbstractMoveCost#AbstractMoveCost(int, AreaTypeCst)
	 */
	public AirPlaneMoveCost(final int anUniqueId)
	{
		super(anUniqueId, AreaTypeCst.ALL_AREAS);
	}

	@Override
	public int getCost(final Cell p_cell)
	{
		Assert.preconditionNotNull(p_cell, "Cell");

		return FIGHTER_MOVE_COST;
	}

	@Override
	public final int getMaxCost()
	{
		return FIGHTER_MOVE_COST;
	}

	@Override
	public int getMinCost()
	{
		return FIGHTER_MOVE_COST;
	}
}
