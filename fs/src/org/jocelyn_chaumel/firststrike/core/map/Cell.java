/*
 * Cell.java Created on 23 f�vrier 2003, 12:40
 */

package org.jocelyn_chaumel.firststrike.core.map;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaList;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Une cellule represente la plus petite division d'une carte. Elle est
 * constituee d'un type ainsi que d'un numero de region.
 * <p>
 * Une region definie une surface sur laquelle certains types de vehicules bien
 * precis peuvent se deplacer librement. Par exemple, un bateau ne se deplace
 * que sur l'eau. Il ne pourra egalement rejoindre une mer qui n'est pas
 * directement accessible via une voie maritime. Ainsi, l'ensemble des cellules
 * representees sur la carte accessible par ce bateau represente une region.
 * Cette notion existe egalement pour les vehicules terrestes. Par exemple une
 * ile est une region.
 */
public final class Cell implements Comparable, Serializable
{
	private CellTypeCst _cellTypeCst;
	private int _index = 1;
	private final Coord _cellCoord;
	private City _city;
	private String _cellName;

	private boolean _isNearToWather;
	private AreaList _areaSet = new AreaList();
	private boolean _isFogEnable = false;

	protected Cell(final Cell p_cell)
	{
		_cellTypeCst = p_cell._cellTypeCst;
		_cellCoord = p_cell._cellCoord;
		_city = p_cell._city;

		_areaSet = p_cell._areaSet;
		_isNearToWather = p_cell._isNearToWather;
		_index = p_cell.getIndex();
		_cellName = p_cell.getCellName();
	}

	/**
	 * Constructor.
	 * 
	 * @param p_cellType Type de cellule.
	 * @param p_nearToWather Identifie si la cellule borde une etendue d'eau.
	 */
	public Cell(final CellTypeCst p_cellType,
				final int p_index,
				final Coord p_cellCoord,
				final String p_cellName,
				final boolean p_nearToWather)
	{
		this(p_cellType, p_index, p_cellCoord, p_cellName, p_nearToWather, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param p_cellType
	 * @param p_nearToWather
	 * @param p_city
	 */
	public Cell(final CellTypeCst p_cellType,
				final int p_index,
				final Coord p_cellCoord,
				final String p_cellName,
				final boolean p_nearToWather,
				final City p_city)
	{
		Assert.preconditionNotNull(p_cellType, "Cell Type");
		Assert.preconditionNotNull(p_cellCoord, "Cell Coord");
		Assert.precondition(p_index >= 1, "Invalid Cell Index");

		_cellTypeCst = p_cellType;
		_cellCoord = p_cellCoord;
		_isNearToWather = p_nearToWather;
		_city = p_city;
		_index = p_index;
		_cellName = p_cellName;
	}

	public final City getCity()
	{
		return _city;
	}

	public final void setCity(final City p_city)
	{
		_city = p_city;
	}

	/**
	 * Retourne le type de la cellule.
	 * 
	 * @return Le type de la cellule.
	 */
	public final CellTypeCst getCellType()
	{
		return _cellTypeCst;
	}

	public final void setCellType(final CellTypeCst p_cellType)
	{
		_cellTypeCst = p_cellType;
	}

	public final int getIndex()
	{
		return _index;
	}

	public final void setIndex(final int p_index)
	{
		Assert.precondition(p_index >= 1, "Invalid Cell index");
		_index = p_index;
	}

	public final String getCellName()
	{
		return _cellName;
	}

	public final String getDefaultCellName()
	{
		if (_cellName == null)
		{
			return _cellCoord.toStringLight();
		}

		return _cellName;
	}

	public final boolean hasName()
	{
		return _cellName != null;
	}

	public final void setCellName(final String p_cellName)
	{
		_cellName = p_cellName;
	}

	/**
	 * Return the cell's coord
	 * 
	 * @return
	 */
	public Coord getCellCoord()
	{
		return _cellCoord;
	}

	/**
	 * Indicate if the current cell is a Ground cell type.
	 * 
	 * @return True or false.
	 */
	public final boolean isGroundCell()
	{
		return CellTypeCst.GROUND_CELL_TYPE_LIST.contains(_cellTypeCst);
	}

	public final boolean isSeaCell()
	{
		if (CellTypeCst.CITY.equals(_cellTypeCst))
		{
			return _isNearToWather;
		} else
		{
			return CellTypeCst.SEA.equals(_cellTypeCst);
		}
	}

	/**
	 * Retourne les identifiants des r�gions � laquelle appartient la
	 * cellule.
	 * 
	 * @return Le num�ro d'une r�gion.
	 */
	public AreaList getAreaList()
	{
		return _areaSet;
	}
	
	public void addArea(final Area p_area)
	{
		Assert.precondition(!_areaSet.containsArea(p_area.getId()), "Id already contained");
		_areaSet.add(p_area);
	}

	/**
	 * Indique si la cellule est une ville.
	 * 
	 * @return Vrai ou faux.
	 */
	public final boolean isCity()
	{
		return CellTypeCst.CITY.equals(_cellTypeCst);
	}

	public final boolean isNearToWather()
	{
		return _isNearToWather;
	}

	public final void setNearToWather(final boolean p_isNearToWather)
	{
		_isNearToWather = p_isNearToWather;
	}

	@Override
	public String toString()
	{
		final StringBuffer sb = new StringBuffer();
		sb.append("{CELL= ").append(_cellTypeCst);
		sb.append(", ").append(_cellCoord);
		sb.append(", ").append(_areaSet.getIdSet());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public int compareTo(final Object p_cell)
	{
		if (p_cell == null || !(p_cell instanceof Cell))
		{
			return -1;
		}
		final Cell cellToCompare = (Cell) p_cell;
		final Coord coordCellToCompare = cellToCompare.getCellCoord();

		return _cellCoord.compareTo(coordCellToCompare);
	}

	public boolean isFogEnable()
	{
		return _isFogEnable;
	}

	public void setFogEnable(final boolean p_isFogEnable)
	{
		_isFogEnable = p_isFogEnable;
	}
}