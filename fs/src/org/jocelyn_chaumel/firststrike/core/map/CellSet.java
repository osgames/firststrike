/*
 * Created on Aug 27, 2005
 */
package org.jocelyn_chaumel.firststrike.core.map;

import java.util.Iterator;

import org.jocelyn_chaumel.tools.data_type.AbstractInstanceHashSet;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class CellSet extends AbstractInstanceHashSet
{

	private static final String AREA_LIST_NAME = "CellSet";
	private static final Class INSTANCE_CLASS = Cell.class;

	/**
	 * Constructeur par d�faut.
	 */
	public CellSet()
	{
		super(INSTANCE_CLASS, AREA_LIST_NAME);
	}

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param p_objectArray Tableau contenant les objets � introduire
	 *        imm�diatement dans la liste.
	 */
	public CellSet(final Object[] p_objectArray)
	{
		super(INSTANCE_CLASS, AREA_LIST_NAME, p_objectArray);
	}

	/**
	 * Retrieve the cell with the expected coord.
	 * 
	 * @param p_coord Excepted coord.
	 * 
	 * @return a Cell
	 */
	public final Cell getCellByCoord(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "Coord");

		Cell currentCell = null;

		final Iterator cellIter = iterator();
		while (cellIter.hasNext())
		{
			currentCell = (Cell) cellIter.next();
			if (currentCell.getCellCoord().equals(p_coord))
			{
				return currentCell;
			}
		}

		return null;
	}

	/**
	 * Retrieve a CoordSet containing each position coord of each cell.
	 * 
	 * @return a Coord Set
	 */
	public final CoordSet getCoordSet()
	{
		final CoordSet coordSet = new CoordSet();
		Cell currentCell = null;

		final Iterator cellIter = iterator();
		while (cellIter.hasNext())
		{
			currentCell = (Cell) cellIter.next();
			coordSet.add(currentCell.getCellCoord());
		}

		return coordSet;
	}

	/**
	 * Returns all grounds cell near to sea.
	 * 
	 * @return Cell Set
	 */
	public CellSet getGroundCellNearToSea()
	{
		final CellSet cellSet = new CellSet();
		Cell currentCell = null;

		final Iterator cellIter = iterator();
		while (cellIter.hasNext())
		{
			currentCell = (Cell) cellIter.next();
			if (currentCell.isNearToWather())
			{
				cellSet.add(currentCell);
			}
		}

		return cellSet;
	}
}
