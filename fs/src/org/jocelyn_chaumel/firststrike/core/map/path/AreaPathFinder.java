/*
 * PathFinder.java Created on 27 avril 2003, 18:21
 */

package org.jocelyn_chaumel.firststrike.core.map.path;

import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.AbstractMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe permet de d�finir, � partir d'une carte, d'un point de d�part,
 * d'un point d'arriv� et d'une liste de co�t de d�placement, le trajet le plus
 * court entre deux cellules.
 * 
 * @author Jocelyn Chaumel
 */
public final class AreaPathFinder extends AbstractPathFinder
{
	private FSMap _map = null;

	/**
	 * Constructeur param�tris�.
	 * 
	 * @param p_map Carte sur laquelle doit �tre recherch� les chemins.
	 */
	public AreaPathFinder(final FSMap p_map)
	{
		super(p_map.getWidth(), p_map.getHeight());

		_map = p_map;

	}

	/**
	 * Recherche un trajet optimis� entre deux points.
	 * 
	 * @param p_startPos
	 * @param p_endPos
	 * @param p_moveCost
	 * @param p_coordSetToExclude
	 * @return
	 * @throws PathNotFoundException
	 */
	final Path findAreaPathIncludingDestination(final Coord p_startPos,
												final Coord p_endPos,
												final AbstractMoveCost p_moveCost,
												final CoordSet p_coordSetToExclude) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_startPos, "Start Position");
		Assert.preconditionNotNull(p_endPos, "End Positicon");
		Assert.preconditionNotNull(p_moveCost, "Move Cost List");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord Set To Exclude");
		Assert.precondition(!p_coordSetToExclude.contains(p_startPos), "CoordToExclude contains the starting position");
		Assert.precondition(!p_coordSetToExclude.contains(p_endPos), "Unreachable Destination");

		final AreaTypeCst areaType = p_moveCost.getAreaTypeCst();

		// Check the Start Area
		final Area startArea;
		if (AreaTypeCst.ALL_AREAS.equals(areaType))
		{
			startArea = null;
		} else
		{
			startArea = _map.getCellAt(p_startPos).getAreaList().getFirstAreaByType(areaType);
			Assert.precondition(startArea != null, "Invalid Start area for this kind of unit");
		}

		// Check the Destination Area
		final Area destinationArea;
		if (AreaTypeCst.ALL_AREAS.equals(areaType))
		{
			destinationArea = null;
		} else
		{
			destinationArea = _map.getCellAt(p_endPos).getAreaList().getFirstAreaByType(areaType);
			Assert.precondition(destinationArea != null, "Invalid destination area for this kind of unit");
		}

		// Check the validity of both start and destination area
		if (!AreaTypeCst.ALL_AREAS.equals(areaType))
		{
			Assert.precondition(startArea.equals(destinationArea), "Both start and destination area must be the same.");
		}

		Path returnedPath = null;

		if (p_startPos.calculateIntDistance(p_endPos) == 1)
		{
			final CoordList coordList = new CoordList();
			coordList.add(p_endPos);
			returnedPath = new Path(coordList);
		} else
		{
			// Construit une liste des cellules contenues dans la r�gion en
			// question.

			final CoordSet areaCoordList = startArea.getCellSet().getCoordSet();

			// Exclude non authorized coord
			areaCoordList.removeAll(p_coordSetToExclude);

			// Construit un tableau temporaire de travail pour identifier
			// le chemin le plus court.
			final WorkingCell[][] workingMap = initMapCost(areaCoordList, p_moveCost);

			// Initialisation du point de d�part.
			// Le nombre de point de mouvement pour se rendre au point de d�part
			// est
			// 0.
			workingMap[p_startPos.getY()][p_startPos.getX()]._totalCost = 0;
			Assert.check(areaCoordList.remove(p_startPos), "Invalid result");

			// Permet de trouver un chemin mais sans optimiser le parcours.
			findPath(areaCoordList, workingMap);
			if (workingMap[p_endPos.getY()][p_endPos.getX()]._totalCost == Integer.MAX_VALUE)
			{
				throw new PathNotFoundException();
			}

			// Optimise un chemin d�j� �tabli.
			optimizePath(areaCoordList, workingMap, p_endPos);

			returnedPath = buildPath(workingMap, p_endPos);
		}
		return returnedPath;
	}

	protected WorkingCell[][] initMapCost(final CoordSet p_countryCoordList, final AbstractMoveCost p_moveCost)
	{
		final WorkingCell[][] workingMap = new WorkingCell[_mapHeight][_mapWidth];

		final Iterator iter = p_countryCoordList.iterator();
		Coord coord;
		Cell cell;
		WorkingCell workingCell;

		// Cr�e une cellule de travail uniquement pour les cellules dans la
		// r�gion.
		while (iter.hasNext())
		{
			coord = (Coord) iter.next();
			cell = _map.getCellAt(coord);
			workingCell = new WorkingCell();
			workingCell._cost = p_moveCost.getCost(cell);
			workingMap[coord.getY()][coord.getX()] = workingCell;
		}

		// Le tableau r�sultant se retrouve in�vitablement avec des valeurs
		// nulles.
		return workingMap;
	}
}