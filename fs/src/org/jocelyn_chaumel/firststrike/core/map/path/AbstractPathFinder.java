/*
 * Created on Jun 24, 2005
 */
package org.jocelyn_chaumel.firststrike.core.map.path;

import java.io.Serializable;
import java.util.Iterator;

import org.jocelyn_chaumel.tools.StringManipulatorHelper;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public abstract class AbstractPathFinder implements Serializable
{

	protected final int _mapWidth;
	protected final int _mapHeight;

	/**
     * 
     */
	public AbstractPathFinder(final int p_mapWidth, final int p_mapHeight)
	{
		Assert.precondition(p_mapWidth > 0, "Invalid Map Width");
		Assert.precondition(p_mapHeight > 0, "Invalid Map Height");

		_mapWidth = p_mapWidth;
		_mapHeight = p_mapHeight;
	}

	public class WorkingCell
	{
		public int _cost = -1;
		public int _totalCost = Integer.MAX_VALUE;

		@Override
		public final String toString()
		{
			return "WorkingCell : [cost=" + _cost + ", totalCost=" + _totalCost + "]";
		}

		public final String toString(final int p_length)
		{
			final StringBuffer sb = new StringBuffer();
			sb.append(StringManipulatorHelper.leftPadding("" + _cost, p_length));
			sb.append(" ");
			if (_totalCost == Integer.MAX_VALUE)
			{
				sb.append(StringManipulatorHelper.leftPadding("-1", p_length));
			} else
			{
				sb.append(StringManipulatorHelper.leftPadding("" + _totalCost, p_length));
			}
			return sb.toString();
		}
	}

	protected void findPath(final CoordSet p_areaCoordSet, final WorkingCell[][] workingMap)
	{
		// La copie de de la liste est n�cessaire car elle est modifi�e dans
		// cette m�thode.
		final CoordSet areaCoordSet = (CoordSet) p_areaCoordSet.clone();

		Iterator iter;
		Coord coord = null;
		boolean modified = false;

		// Tant que toutes les cellules n'ont pas �t� mises � jour.
		while (!areaCoordSet.isEmpty())
		{
			iter = areaCoordSet.iterator();
			modified = false;
			while (iter.hasNext())
			{
				coord = (Coord) iter.next();
				// Traitons uniquement les cellules non-initialis�e.
				Assert.check(	workingMap[coord.getY()][coord.getX()]._totalCost == Integer.MAX_VALUE,
								"Cell already initialized detected");
				// Est-ce qu'il existe un chemin pour cette cellule?
				if (findBestStep(coord, workingMap, Integer.MAX_VALUE))
				{
					// Si oui, la cellule a �t� mise � jour.
					modified = true;
					break;
				}
			}

			if (modified)
			{
				Assert.check(coord != null, "Heu!!!");
				/*
				 * Maintenant qu'une distance a �t� attribu�e � la cellule, elle
				 * peut �tre supprim�e de la liste des cellules � calculer.
				 */
				areaCoordSet.remove(coord);
			} else
			{
				areaCoordSet.clear();
			}
		}
	}

	protected boolean findBestStep(final Coord p_coord, final WorkingCell[][] aWorkingMap, final int aLowestPathCost)
	{
		int x = p_coord.getX();
		int y = p_coord.getY();
		long lowestTotalCost = aWorkingMap[y][x]._totalCost;
		int cellCost = aWorkingMap[y][x]._cost;
		boolean hasBestStep = false;

		// O-+
		// | |
		// +-+
		if ((x > 0) && (y > 0) && (aWorkingMap[y - 1][x - 1] != null)
				&& ((long) aWorkingMap[y - 1][x - 1]._totalCost + cellCost < lowestTotalCost))
		{
			lowestTotalCost = aWorkingMap[y - 1][x - 1]._totalCost + cellCost;
			if (lowestTotalCost < aLowestPathCost)
			{
				hasBestStep = true;
			}
		}

		// +O+
		// | |
		// +-+
		if ((y > 0) && (aWorkingMap[y - 1][x] != null)
				&& ((long) aWorkingMap[y - 1][x]._totalCost + cellCost < lowestTotalCost))
		{
			lowestTotalCost = aWorkingMap[y - 1][x]._totalCost + cellCost;
			if (lowestTotalCost < aLowestPathCost)
			{
				hasBestStep = true;
			}
		}

		// +-O
		// | |
		// +-+
		if ((x + 1 < _mapWidth) && (y > 0) && (aWorkingMap[y - 1][x + 1] != null)
				&& ((long) aWorkingMap[y - 1][x + 1]._totalCost + cellCost < lowestTotalCost))
		{
			lowestTotalCost = aWorkingMap[y - 1][x + 1]._totalCost + cellCost;
			if (lowestTotalCost < aLowestPathCost)
			{
				hasBestStep = true;
			}
		}

		// +-+
		// | O
		// +-+
		if ((x + 1 < _mapWidth) && (aWorkingMap[y][x + 1] != null)
				&& ((long) aWorkingMap[y][x + 1]._totalCost + cellCost < lowestTotalCost))
		{
			lowestTotalCost = aWorkingMap[y][x + 1]._totalCost + cellCost;
			if (lowestTotalCost < aLowestPathCost)
			{
				hasBestStep = true;
			}
		}

		// +-+
		// | |
		// +-O
		if ((x + 1 < _mapWidth) && (y + 1 < _mapHeight) && (aWorkingMap[y + 1][x + 1] != null)
				&& ((long) aWorkingMap[y + 1][x + 1]._totalCost + cellCost < lowestTotalCost))
		{
			lowestTotalCost = aWorkingMap[y + 1][x + 1]._totalCost + cellCost;
			if (lowestTotalCost < aLowestPathCost)
			{
				hasBestStep = true;
			}
		}

		// +-+
		// | |
		// +O+
		if ((y + 1 < _mapHeight) && (aWorkingMap[y + 1][x] != null)
				&& ((long) aWorkingMap[y + 1][x]._totalCost + cellCost < lowestTotalCost))
		{
			lowestTotalCost = aWorkingMap[y + 1][x]._totalCost + cellCost;
			if (lowestTotalCost < aLowestPathCost)
			{
				hasBestStep = true;
			}
		}

		// +-+
		// | |
		// O-+
		if ((x > 0) && (y + 1 < _mapHeight) && (aWorkingMap[y + 1][x - 1] != null)
				&& ((long) aWorkingMap[y + 1][x - 1]._totalCost + cellCost < lowestTotalCost))
		{
			lowestTotalCost = aWorkingMap[y + 1][x - 1]._totalCost + cellCost;
			if (lowestTotalCost < aLowestPathCost)
			{
				hasBestStep = true;
			}
		}

		// +-+
		// O |
		// +-+
		if ((x > 0) && (aWorkingMap[y][x - 1] != null)
				&& ((long) aWorkingMap[y][x - 1]._totalCost + cellCost < lowestTotalCost))
		{
			lowestTotalCost = aWorkingMap[y][x - 1]._totalCost + cellCost;
			if (lowestTotalCost < aLowestPathCost)
			{
				hasBestStep = true;
			}
		}

		if (hasBestStep)
			aWorkingMap[y][x]._totalCost = (int) lowestTotalCost;

		return hasBestStep;
	}

	protected void optimizePath(final CoordSet p_areaCoordSet, final WorkingCell[][] aWorkingMap, final Coord anEndPos)
	{
		boolean isOptimized = false;
		Iterator iter;
		Coord coord;
		int lowerPathCost = aWorkingMap[anEndPos.getY()][anEndPos.getX()]._totalCost;

		while (!isOptimized)
		{
			isOptimized = true;
			iter = p_areaCoordSet.iterator();
			while (iter.hasNext())
			{
				coord = (Coord) iter.next();
				if (findBestStep(coord, aWorkingMap, lowerPathCost))
				{
					isOptimized = false;
				}
			}
		}
	}

	/**
	 * Construit le chemin � partir du tableau temporaire de travail.
	 * 
	 * @param aWorkingMap Tableau temporaire de travail.
	 * @param anEndPos Position � laquelle doit mener le chemin.
	 */
	protected Path buildPath(final WorkingCell[][] aWorkingMap, final Coord anEndPos)
	{
		final CoordList coordList = new CoordList();

		// Ajoute la position d'arriv� pour commencer.
		Coord coord = anEndPos;

		// Tant que la position de d�part ne sera pas atteinte,
		// Remonter le chemin le plus court et ajouter les coordonn�es dans
		// la liste.
		do
		{
			// Ajoute la cellule au d�but de la liste.
			coordList.add(0, coord);
			// Trouve la cellule offrant le plus court chemin.
			coord = findNextStep(coord, aWorkingMap, anEndPos);
		} while (aWorkingMap[coord.getY()][coord.getX()]._totalCost != 0);

		// Construit un nouveau chemin � partir de la nouvelle liste.
		return new Path(coordList);
	}

	/**
	 * Retourne la coordonn�e de la cellule adjacente offrant le plus court
	 * chemin pour se rendre � l'origine du chemin (point de d�part).
	 * 
	 * @param p_currentCoord Position courante.
	 * @param aWorkingMap Tableau temporaire de travail.
	 * @return Prochaine coordonn�e.
	 */
	protected Coord findNextStep(final Coord p_currentCoord, final WorkingCell[][] aWorkingMap, final Coord anEndPos)
	{
		int x = p_currentCoord.getX();
		int y = p_currentCoord.getY();
		int currentLowerCost = Integer.MAX_VALUE;
		int currentCost;
		final CoordList lowerCostCoordList = new CoordList();

		// O-+
		// | |
		// +-+
		if (y > 0 && x > 0 && aWorkingMap[y - 1][x - 1] != null)
		{
			currentCost = aWorkingMap[y - 1][x - 1]._totalCost;
			if (currentCost < currentLowerCost)
			{
				currentLowerCost = currentCost;
			}
			lowerCostCoordList.add(new Coord(x - 1, y - 1));
		}

		// +O+
		// | |
		// +-+
		if (y > 0 && aWorkingMap[y - 1][x] != null)
		{
			currentCost = aWorkingMap[y - 1][x]._totalCost;
			if (currentCost < currentLowerCost)
			{
				currentLowerCost = currentCost;
				lowerCostCoordList.clear();
			}
			if (currentCost <= currentLowerCost)
			{
				lowerCostCoordList.add(new Coord(x, y - 1));
			}
		}

		// +-O
		// | |
		// +-+
		if ((y > 0) && (x < aWorkingMap[y].length - 1) && (aWorkingMap[y - 1][x + 1] != null))
		{
			currentCost = aWorkingMap[y - 1][x + 1]._totalCost;
			if (currentCost < currentLowerCost)
			{
				currentLowerCost = currentCost;
				lowerCostCoordList.clear();
			}
			if (currentCost <= currentLowerCost)
			{
				lowerCostCoordList.add(new Coord(x + 1, y - 1));
			}
		}

		// +-+
		// | O
		// +-+
		if ((x < aWorkingMap[y].length - 1) && (aWorkingMap[y][x + 1] != null))
		{
			currentCost = aWorkingMap[y][x + 1]._totalCost;
			if (currentCost < currentLowerCost)
			{
				currentLowerCost = currentCost;
				lowerCostCoordList.clear();
			}
			if (currentCost <= currentLowerCost)
			{
				lowerCostCoordList.add(new Coord(x + 1, y));
			}
		}

		// +-+
		// | |
		// +-O
		if ((y < aWorkingMap.length - 1) && (x < aWorkingMap[y].length - 1) && (aWorkingMap[y + 1][x + 1] != null))
		{
			currentCost = aWorkingMap[y + 1][x + 1]._totalCost;
			if (currentCost < currentLowerCost)
			{
				currentLowerCost = currentCost;
				lowerCostCoordList.clear();
			}
			if (currentCost <= currentLowerCost)
			{
				lowerCostCoordList.add(new Coord(x + 1, y + 1));
			}
		}

		// +-+
		// | |
		// +O+
		if ((y < aWorkingMap.length - 1) && (aWorkingMap[y + 1][x] != null))
		{
			currentCost = aWorkingMap[y + 1][x]._totalCost;
			if (currentCost < currentLowerCost)
			{
				currentLowerCost = currentCost;
				lowerCostCoordList.clear();
			}
			if (currentCost <= currentLowerCost)
			{
				lowerCostCoordList.add(new Coord(x, y + 1));
			}
		}

		// +-+
		// | |
		// O-+
		if ((y < aWorkingMap.length - 1) && (x > 0) && (aWorkingMap[y + 1][x - 1] != null))
		{
			currentCost = aWorkingMap[y + 1][x - 1]._totalCost;
			if (currentCost < currentLowerCost)
			{
				currentLowerCost = currentCost;
				lowerCostCoordList.clear();
			}
			if (currentCost <= currentLowerCost)
			{
				lowerCostCoordList.add(new Coord(x - 1, y + 1));
			}
		}

		// +-+
		// O |
		// +-+
		if ((x > 0) && (aWorkingMap[y][x - 1] != null))
		{
			currentCost = aWorkingMap[y][x - 1]._totalCost;
			if (currentCost < currentLowerCost)
			{
				currentLowerCost = currentCost;
				lowerCostCoordList.clear();
			}
			if (currentCost <= currentLowerCost)
			{
				lowerCostCoordList.add(new Coord(x - 1, y));
			}
		}

		return findBestCoord(lowerCostCoordList, anEndPos);
	}

	private Coord findBestCoord(final CoordList p_coordList, final Coord anEndPos)
	{

		Coord bestCoord = (Coord) p_coordList.get(0);
		if (p_coordList.size() == 1 || bestCoord.equals(anEndPos))
		{
			return bestCoord;
		}

		p_coordList.remove(0);
		int lowerDelta = bestCoord.calculateDelta(anEndPos);
		int currentDelta;
		Coord currentCoord;
		final Iterator iter = p_coordList.iterator();
		while (iter.hasNext())
		{
			currentCoord = (Coord) iter.next();
			currentDelta = currentCoord.calculateDelta(anEndPos);
			if (currentDelta < lowerDelta)
			{
				bestCoord = currentCoord;
				lowerDelta = currentDelta;
			}
		}

		return bestCoord;
	}
}
