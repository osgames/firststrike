package org.jocelyn_chaumel.firststrike.core.map.path;

import java.io.Serializable;
import java.util.Hashtable;

import org.jocelyn_chaumel.firststrike.core.map.AbstractMoveCost;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordHashtable;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe permet de stocker et r�cup�rer des trajets (Path) d�j� calcul�s
 * dans les pr�c�dentes op�rations. Les trajets sont class�s par type, c'est �
 * dire d'apres leur co�t de d�placement (<code>AbstractMoveCost</code>). et
 * leur point de d�part. A noter que si le point de d�part est plus �lev� que le
 * point de destination, le trajet est d'abord invers� avant d'�tre sauv�.
 * (Coord) puis stock�s.
 * 
 * @author Joss
 */
public class PathCache implements Serializable
{
	private final Hashtable _moveCostMap = new Hashtable(2);

	/**
	 * Retourne un trajet s'il existe dans la cache. Si le point de destination
	 * est moins �lev� que le point d�part, la recherche sera adapt�e et
	 * retournera un trajet invers�. Cette m�thode retourne <code>null</code>
	 * lorsque le cache ne contient pas le trajet recherch�.
	 * 
	 * @param p_moveCost Cat�gorie de path.
	 * @param p_startPos Position de d�part.
	 * @param p_destinationPos Position d'arriv�e.
	 * @return Un trajet ou <code>null</code>.
	 */
	public final Path getPathFromCache(	final AbstractMoveCost p_moveCost,
										final Coord p_startPos,
										final Coord p_destinationPos)
	{
		Assert.preconditionNotNull(p_moveCost, "Move Cost");
		Assert.preconditionNotNull(p_startPos, "Start Position");
		Assert.preconditionNotNull(p_destinationPos, "Destination Position");
		Assert.precondition(!p_startPos.equals(p_destinationPos), "Both position are the same");

		final CoordHashtable smallerCoordMap = (CoordHashtable) _moveCostMap.get(p_moveCost);
		if (smallerCoordMap == null)
		{
			return null;
		}

		final Coord smallerCoord;
		final Coord greaterCoord;
		final boolean reversedPath;
		if (p_startPos.compareTo(p_destinationPos) <= 0)
		{
			smallerCoord = p_startPos;
			greaterCoord = p_destinationPos;
			reversedPath = false;
		} else
		{
			smallerCoord = p_destinationPos;
			greaterCoord = p_startPos;
			reversedPath = true;
		}

		final CoordHashtable greaterCoordMap = (CoordHashtable) smallerCoordMap.get(smallerCoord);
		if (greaterCoordMap == null)
		{
			return null;
		}

		Path path = (Path) greaterCoordMap.get(greaterCoord);
		if (path == null)
		{
			return null;
		}

		path = (Path) path.clone();
		if (reversedPath)
		{
			path.reversePath(p_destinationPos);
		}
		return path;
	}

	/**
	 * Ajout un trajet (<code>Path</code>) dans la cache. Celui-ci est class�
	 * selon sa table de co�t de d�placement (<code>AbstractMoveCost</code>) et
	 * son point de d�part.
	 * 
	 * @param p_moveCost Table des co�ts de d�placement
	 * @param p_startPos Point de d�part
	 * @param p_destPos Point d'arriv�e
	 * @param p_path Trajet
	 */
	public final void addPathInCache(	final AbstractMoveCost p_moveCost,
										final Coord p_startPos,
										final Coord p_destPos,
										final Path p_path)
	{
		Assert.preconditionNotNull(p_moveCost, "Move Cost");
		Assert.preconditionNotNull(p_path, "Path");
		Assert.preconditionNotNull(p_startPos, "Start Position");
		Assert.preconditionNotNull(p_destPos, "Destination Position");
		Assert.precondition(!p_startPos.equals(p_destPos), "Both position are the same");
		Assert.precondition(p_path.getNextStepWithoutConsume().calculateIntDistance(p_startPos) == 1,
							"Invalid Start Position");
		Assert.precondition(p_path.getDestination().equals(p_destPos), "Invalid Destination Position");

		final Path path = (Path) p_path.clone();

		CoordHashtable smallerCoordMap = (CoordHashtable) _moveCostMap.get(p_moveCost);
		if (smallerCoordMap == null)
		{
			smallerCoordMap = new CoordHashtable();
			_moveCostMap.put(p_moveCost, smallerCoordMap);
		}
		final Coord smallerCoord;
		final Coord greaterCoord;
		if (p_startPos.compareTo(p_destPos) > 0)
		{
			path.reversePath(p_startPos);
			smallerCoord = p_destPos;
			greaterCoord = p_startPos;
		} else
		{
			smallerCoord = p_startPos;
			greaterCoord = p_destPos;
		}

		CoordHashtable greaterCoordMap = (CoordHashtable) smallerCoordMap.get(smallerCoord);
		if (greaterCoordMap == null)
		{
			greaterCoordMap = new CoordHashtable();
			smallerCoordMap.put(smallerCoord, greaterCoordMap);
		}

		Assert.precondition(!greaterCoordMap.containsKey(greaterCoord), "Path already exist");
		greaterCoordMap.put(greaterCoord, path);
	}
}
