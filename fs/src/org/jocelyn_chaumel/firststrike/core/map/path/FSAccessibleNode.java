package org.jocelyn_chaumel.firststrike.core.map.path;

import org.jocelyn_chaumel.tools.data_type.tree.AccessibleNode;
import org.jocelyn_chaumel.tools.data_type.tree.NodeDefinition;

public final class FSAccessibleNode extends AccessibleNode
{

	private final Path _path;

	public FSAccessibleNode(final NodeDefinition p_nodeDefinition, final Path p_path)
	{
		super(p_nodeDefinition, p_path.getCoordList().size());
		_path = p_path;
	}

	public final Path getPath()
	{
		return _path;
	}

}
