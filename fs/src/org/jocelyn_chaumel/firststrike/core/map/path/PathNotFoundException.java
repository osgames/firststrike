/*
 * Created on Feb 4, 2005
 */
package org.jocelyn_chaumel.firststrike.core.map.path;

/**
 * @author Jocelyn Chaumel
 */
public class PathNotFoundException extends Exception
{
	public PathNotFoundException()
	{
		super();
	}

	public PathNotFoundException(final String p_message)
	{
		super(p_message);
	}
}
