/*
 * Created on May 4, 2005
 */
package org.jocelyn_chaumel.firststrike.core.map.path;

import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.data_type.tree.NodeDefinition;
import org.jocelyn_chaumel.tools.data_type.tree.NodeDefinitionList;
import org.jocelyn_chaumel.tools.data_type.tree.NodeId;
import org.jocelyn_chaumel.tools.data_type.tree.Tree;
import org.jocelyn_chaumel.tools.data_type.tree.TreeNode;
import org.jocelyn_chaumel.tools.data_type.tree.TreeNodeList;
import org.jocelyn_chaumel.tools.data_type.tree.TreePathNotFound;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe calcule le chemin a�rien le plus court entre 2 positions.
 * 
 * @author Jocelyn Chaumel
 */
public class AirPathFinder extends AbstractPathFinder
{

	/**
	 * Contains all map's cities.
	 */
	private final CityList _cityList;

	/**
	 * Constructor.
	 * 
	 * @param p_mapWidth Map's width.
	 * @param p_mapHeight Map's height.
	 * @param p_cityList List of existing city in the map.
	 */
	public AirPathFinder(final int p_mapWidth, final int p_mapHeight, final CityList p_cityList)
	{
		super(p_mapWidth, p_mapHeight);
		Assert.preconditionNotNull(p_cityList, "City List");
		Assert.precondition(!p_cityList.isEmpty(), "Empty City List");

		_cityList = p_cityList;
	}

	/**
	 * Builds & returns a path for a fighter.
	 * 
	 * @param p_startPos Starting position.
	 * @param p_destPos Destination position.
	 * @param p_fuel Available fuel at start.
	 * @param p_fuelCapacity Fuel capacity when the fighter is restored.
	 * @param p_player Player to calculate the cities able to restore the
	 *        fighter during its movement.
	 * @param p_coordSetToExclude Coord Set to exclude.
	 * @return a Path.
	 * @throws PathNotFoundException
	 */
	public final Path findPlayerPath(	final Coord p_startPos,
										final Coord p_destPos,
										final int p_fuel,
										final int p_fuelCapacity,
										final AbstractPlayer p_player,
										final CoordSet p_coordSetToExclude) throws PathNotFoundException
	{
		// All other Assert are done in the next method.
		Assert.preconditionNotNull(p_player, "Player");

		final CoordSet cityCoordSet = _cityList.getCityByPlayer(p_player).getCoordSet()
				.getLimitedCoordSet(p_startPos, p_fuel);
		return findPath(p_startPos, p_destPos, p_fuel, p_fuelCapacity, cityCoordSet, p_coordSetToExclude, true);
	}

	/**
	 * Builds & returns a path for a fighter.
	 * 
	 * @param p_startPos Starting position.
	 * @param p_destPos Destination position.
	 * @param p_fuel Available fuel at start.
	 * @param p_fuelCapacity Fuel capacity when the fighter is restored.
	 * @param p_cityCoordSet List of cities able to restore the fighter during
	 *        its movement.
	 * @param p_coordSetToExclude Coord Set to exclude.
	 * @param p_enableLongPath Enable the option to calculate the long range
	 *        path.
	 * @return a Path.
	 * @throws PathNotFoundException
	 */
	public final Path findPath(	final Coord p_startPos,
								final Coord p_destPos,
								final int p_fuel,
								final int p_fuelCapacity,
								final CoordSet p_cityCoordSet,
								final CoordSet p_coordSetToExclude,
								final boolean p_enableLongPath) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_startPos, "Start Position");
		Assert.preconditionNotNull(p_destPos, "End Position");
		Assert.precondition(p_fuel > 0, "Invalid fuel");
		Assert.precondition(p_fuelCapacity > 1, "Invalid fuel Capacity");
		Assert.preconditionNotNull(p_cityCoordSet, "City Coord Set");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord List to Exclude");
		Assert.precondition(!p_startPos.equals(p_destPos), "Start position equals end position");
		Assert.precondition(!p_coordSetToExclude.contains(p_destPos), "Unreachable Destination");

		Path path = getDirectPath(p_startPos, p_destPos);

		if (path.fallsOnEnemyPosition(p_coordSetToExclude))
		{
			path = getIndirectPath(p_startPos, p_destPos, p_coordSetToExclude);
		}

		if (!path.validateFuelAlimentation(p_fuel, p_fuelCapacity, p_cityCoordSet) && p_enableLongPath
				&& p_cityCoordSet.size() > 0)
		{
			path = getLongPath(p_startPos, p_destPos, p_fuel, p_fuelCapacity, p_cityCoordSet, p_coordSetToExclude);
		}

		return path;
	}

	/**
	 * Calculate a long path for a fighter unit. A long path used city to
	 * restore the fighter during his movement.
	 * 
	 * @param p_startPos Start position.
	 * @param p_destPos Destinsation position.
	 * @param p_fuel Available fuel when the move is starting.
	 * @param p_fuelCapacity Available fuel after fuel restoration.
	 * @param p_cityCoordSet Available city able to restore the fighter during
	 *        the movement.
	 * @param p_coordSetToExclude Coord set to exclude from the path.
	 * @return A path.
	 * @throws PathNotFoundException
	 */
	public final Path getLongPath(	final Coord p_startPos,
									final Coord p_destPos,
									final int p_fuel,
									final int p_fuelCapacity,
									final CoordSet p_cityCoordSet,
									final CoordSet p_coordSetToExclude) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_startPos, "Start position");
		Assert.preconditionNotNull(p_destPos, "Destination");
		Assert.precondition(p_fuel > 0, "Invalid fuel");
		Assert.precondition(p_fuelCapacity > 1, "Invalid fuel capacity");
		Assert.preconditionNotNull(p_cityCoordSet, "City Coord Set");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord Set to exclude");
		Assert.precondition(!p_cityCoordSet.isEmpty(), "Empty City Set");
		Assert.precondition(!p_startPos.equals(p_destPos), "Unit already at destination");

		final NodeDefinitionList nodeDefList = buildNodeDefinitionList(	p_startPos,
																		p_fuel,
																		p_fuelCapacity,
																		p_cityCoordSet,
																		p_coordSetToExclude);
		final Path path;

		if (p_cityCoordSet.contains(p_destPos))
		{
			path = buildPathWithNodeDefinitionList(	nodeDefList,
													p_startPos,
													p_destPos,
													p_fuel,
													p_fuelCapacity,
													p_cityCoordSet,
													p_coordSetToExclude);
		} else
		{
			final Coord nearestCityFromDest = p_cityCoordSet.getDirectNearestCoord(p_destPos);
			if (nearestCityFromDest.equals(p_startPos))
			{
				throw new PathNotFoundException("No better node found than " + p_startPos);
			}
			path = buildPathWithNodeDefinitionList(	nodeDefList,
													p_startPos,
													nearestCityFromDest,
													p_fuel,
													p_fuelCapacity,
													p_cityCoordSet,
													p_coordSetToExclude);
			path.appendToPath(this.findPath(nearestCityFromDest,
											p_destPos,
											p_fuel,
											p_fuelCapacity,
											p_cityCoordSet,
											p_coordSetToExclude,
											false));

		}
		return path;
	}

	/**
	 * Create a node list (one node by city). If the start location and/or the
	 * destination location aren't city, a specific node is created (one for
	 * start & one for destination location).
	 * 
	 * @param p_startPos Start location (can be or not a city coord).
	 * @param p_destPos Destination location (can be or not a city coord).
	 * @param p_fuel Actual fuel of the moving unit.
	 * @param p_fuelCapacity Max fuel capacity of the unit
	 * @param p_cityCoordSet List of city coord controlled by the player's unit.
	 * @return A step by step Path.
	 * @throws PathNotFoundException
	 */
	private NodeDefinitionList buildNodeDefinitionList(	final Coord p_startPos,
														final int p_fuel,
														final int p_fuelCapacity,
														final CoordSet p_cityCoordSet,
														final CoordSet p_coordToExclude) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_cityCoordSet, "City Coord List");
		Assert.precondition(p_cityCoordSet.size() > 0, "Invalid City Coord List");

		final NodeDefinitionList nodeDefList = new NodeDefinitionList();
		Coord currentCityCoord = null;
		NodeDefinition node = null;
		int id = 1;
		// Cr�ation d'une instance NodeDefinition pour toutes les villes
		final Iterator cityIterator = p_cityCoordSet.iterator();
		while (cityIterator.hasNext())
		{
			currentCityCoord = (Coord) cityIterator.next();
			node = new NodeDefinition(new NodeId(id++), currentCityCoord);
			nodeDefList.add(node);
		}

		int curDist;
		Iterator childIterator = null;
		NodeDefinition parentNodeDefinition = null;
		NodeDefinition childNodeDefinition = null;
		FSAccessibleNode currentAccessNode = null;
		Coord parentCoord = null;
		Coord childCoord = null;
		Path currentPath = null;
		// Ajoute les enfants directs des nodes pr�c�demment cr��s
		final Iterator parentIterator = nodeDefList.iterator();
		while (parentIterator.hasNext())
		{
			parentNodeDefinition = (NodeDefinition) parentIterator.next();
			parentCoord = (Coord) parentNodeDefinition.getObjectRef();
			childIterator = nodeDefList.iterator();
			while (childIterator.hasNext())
			{
				childNodeDefinition = (NodeDefinition) childIterator.next();
				if (parentNodeDefinition != childNodeDefinition)
				{
					childCoord = (Coord) childNodeDefinition.getObjectRef();
					curDist = parentCoord.calculateIntDistance(childCoord);
					if (curDist > p_fuelCapacity)
					{
						continue;
					}
					currentPath = this.findPath(parentCoord,
												childCoord,
												Integer.MAX_VALUE,
												Integer.MAX_VALUE,
												p_cityCoordSet,
												p_coordToExclude,
												false);
					if (currentPath.getCoordList().size() > p_fuelCapacity)
					{
						continue;
					}
					currentAccessNode = new FSAccessibleNode(childNodeDefinition, currentPath);
					parentNodeDefinition.addAccessibleNode(currentAccessNode);
				}
			}
		}

		// Ajoute le node de d�but si le point de d�part n'est pas une ville de
		// la liste
		if (!p_cityCoordSet.contains(p_startPos))
		{
			parentNodeDefinition = new NodeDefinition(new NodeId(id++), p_startPos);
			nodeDefList.add(parentNodeDefinition);
			final CoordSet childSet = p_cityCoordSet.getCoordInRange(p_startPos, p_fuel);

			Coord currentCoord = null;
			final Iterator coordIter = childSet.iterator();
			while (coordIter.hasNext())
			{
				currentCoord = (Coord) coordIter.next();
				if (currentCoord.calculateIntDistance(p_startPos) > p_fuel)
				{
					continue;
				}
				currentPath = this.findPath(currentCoord,
											p_startPos,
											Integer.MAX_VALUE,
											Integer.MAX_VALUE,
											p_cityCoordSet,
											p_coordToExclude,
											false);
				if (currentPath.getCoordList().size() > p_fuel)
				{
					continue;
				}
				childNodeDefinition = nodeDefList.getNodeByRef(currentCoord);
				currentAccessNode = new FSAccessibleNode(childNodeDefinition, currentPath);

				parentNodeDefinition.addAccessibleNode(currentAccessNode);
			}
		}

		return nodeDefList;
	}

	private Path buildPathWithNodeDefinitionList(	final NodeDefinitionList p_nodeDefinitionList,
													final Coord p_startPos,
													final Coord p_destPos,
													final int p_fuel,
													final int p_fuelCapacity,
													final CoordSet p_cityCoordSet,
													final CoordSet p_coordSetToExclude) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_nodeDefinitionList, "Node Definition List");
		Assert.preconditionNotNull(p_startPos, "Start position");
		Assert.preconditionNotNull(p_destPos, "Destination coord");

		final Tree tree = new Tree(p_nodeDefinitionList);
		final NodeDefinition startNode = p_nodeDefinitionList.getNodeByRef(p_startPos);
		final NodeDefinition destinationNode = p_nodeDefinitionList.getNodeByRef(p_destPos);
		TreeNodeList nodeList = null;
		try
		{
			nodeList = tree.getPath(startNode, destinationNode);
		} catch (TreePathNotFound ex)
		{
			throw new PathNotFoundException();
		}

		Path finalPath = null;
		Path currentPath = null;
		TreeNode currentNode;
		Coord currentCoord;
		Coord lastCoord;

		final Iterator nodeIter = nodeList.iterator();
		currentNode = (TreeNode) nodeIter.next();
		currentCoord = (Coord) currentNode.getObjectRef();

		while (nodeIter.hasNext())
		{
			currentNode = (TreeNode) nodeIter.next();
			lastCoord = (Coord) currentNode.getObjectRef();
			currentPath = this.findPath(currentCoord,
										lastCoord,
										p_fuelCapacity,
										p_fuelCapacity,
										p_cityCoordSet,
										p_coordSetToExclude,
										false);
			if (finalPath == null)
			{
				finalPath = currentPath;
			} else
			{
				finalPath.appendToPath(currentPath);
			}
			currentCoord = lastCoord;
		}

		return finalPath;
	}

	/**
	 * Returns a direct path between 2 coords.
	 * 
	 * @param p_startPos Start coord (not included in the path).
	 * @param p_destPos Destination coord (included in the returned path).
	 * @return A path.
	 */
	public Path getDirectPath(final Coord p_startPos, final Coord p_destPos)
	{
		Assert.preconditionNotNull(p_startPos, "Start Position");
		Assert.preconditionNotNull(p_destPos, "End Position");
		Assert.precondition(!p_startPos.equals(p_destPos), "Same coord");

		final int startX = p_startPos.getX();
		final int startY = p_startPos.getY();
		final int endX = p_destPos.getX();
		final int endY = p_destPos.getY();

		final CoordList coordList = new CoordList();
		final int deltaX = endX - startX;
		final int deltaY = endY - startY;
		final int deltp_max = Math.max(Math.abs(deltaX), Math.abs(deltaY));
		final float stepX = (float) deltaX / deltp_max;
		final float stepY = (float) deltaY / deltp_max;
		float realX = startX;
		float realY = startY;
		Coord currentCoord = null;

		for (int i = 0; i < deltp_max; i++)
		{
			realX += stepX;
			realY += stepY;
			currentCoord = new Coord(Math.round(realX), Math.round(realY));
			coordList.add(currentCoord);
		}

		return new Path(coordList);
	}

	/**
	 * Returns an indirect path taking into account a list of excluded coord.
	 * 
	 * @param p_startPos Starting position.
	 * @param p_destPos Destination position.
	 * @param p_coordSetToExclude Coord to exclude from the returned path.
	 * @return A path.
	 * @throws PathNotFoundException
	 */
	public Path getIndirectPath(final Coord p_startPos, final Coord p_destPos, final CoordSet p_coordSetToExclude) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_startPos, "Start Position");
		Assert.preconditionNotNull(p_destPos, "End Position");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord Set to Exclude");
		Assert.precondition(!p_startPos.equals(p_destPos), "Same Coord");

		final CoordSet areaCoordSet = buildCellList(p_startPos, p_destPos);

		areaCoordSet.removeAll(p_coordSetToExclude);

		// Construit un tableau temporaire de travail pour identifier
		// le chemin le plus court.
		WorkingCell[][] workingMap = initMapCost(areaCoordSet);

		// Initialisation du point de d�part.
		// Le nombre de point de mouvement pour se rendre au point de d�part est
		// 0.
		workingMap[p_startPos.getY()][p_startPos.getX()]._totalCost = 0;

		Assert.check(areaCoordSet.remove(p_startPos), "Invalid result");

		findPath(areaCoordSet, workingMap);
		if (workingMap[p_destPos.getY()][p_destPos.getX()]._totalCost == Integer.MAX_VALUE)
		{
			throw new PathNotFoundException();
		}

		// Optimise un chemin d�j� �tabli.
		optimizePath(areaCoordSet, workingMap, p_destPos);

		return buildPath(workingMap, p_destPos);
	}

	private WorkingCell[][] initMapCost(final CoordSet p_coordSet)
	{
		WorkingCell[][] workingMap = new WorkingCell[_mapHeight][_mapWidth];

		// initialise les �l�ments du tableau � null
		for (int y = 0; y < workingMap.length; y++)
		{
			for (int x = 0; x < workingMap[y].length; x++)
			{
				workingMap[y][x] = null;
			}
		}

		Coord coord = null;
		WorkingCell workingCell = null;

		// Cr�e une cellule de travail uniquement pour les cellules dans la
		// r�gion.
		final Iterator iter = p_coordSet.iterator();
		while (iter.hasNext())
		{
			coord = (Coord) iter.next();
			workingCell = new WorkingCell();
			workingCell._cost = 1;
			// workingCell._move = -1; D�j� initialiser � -1 via le constructeur
			workingMap[coord.getY()][coord.getX()] = workingCell;
		}

		// Le tableau r�sultant se retrouve in�vitablement avec des valeurs
		// nulles.
		return workingMap;
	}

	/**
	 * Identifie et retourne toutes les cellules qui sont susceptibles d'�tre
	 * inclusent dans le chemin � calculer.
	 * 
	 * @param p_startPos Position de d�part.
	 * @param p_destPos Position de destination.
	 * @return Liste des coordonn�es pouvant faire partie du chemin entre 2
	 *         positions.
	 */
	private CoordSet buildCellList(final Coord p_startPos, final Coord p_destPos)
	{
		Assert.preconditionNotNull(p_startPos, "Start Position");
		Assert.preconditionNotNull(p_destPos, "End Position");

		int minX = Math.min(p_startPos.getX(), p_destPos.getX());
		minX = Math.min(0, Math.abs(minX - 5));

		int maxX = Math.max(p_startPos.getX(), p_destPos.getX());
		maxX = Math.min(_mapWidth, maxX + 5);

		int minY = Math.min(p_startPos.getY(), p_destPos.getY());
		minY = Math.min(0, Math.abs(minY - 5));

		int maxY = Math.max(p_startPos.getY(), p_destPos.getY());
		maxY = Math.min(_mapHeight, maxY + 5);

		final CoordSet coordSet = new CoordSet();
		for (int y = minY; y < maxY; y++)
		{
			for (int x = minX; x < maxX; x++)
			{
				coordSet.add(new Coord(x, y));
			}
		}

		return coordSet;
	}
}
