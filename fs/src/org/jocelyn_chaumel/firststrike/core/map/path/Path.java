/*
 * Path.java Created on 1 mai 2003, 21:57
 */

package org.jocelyn_chaumel.firststrike.core.map.path;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.map.AbstractMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe encapsule un parcours(chemin). Un parcours est constitu� d'une
 * liste de coordonn�es.
 * 
 * @author Jocelyn Chaumel
 */
public final class Path implements Serializable
{

	private CoordList _list;
	private ArrayList<Integer> _moveCostList = new ArrayList<Integer>();
	private ArrayList<Integer> _turnCostList = new ArrayList<Integer>();

	public Path(final Coord[] p_coordArray)
	{
		this(new CoordList(p_coordArray));
	}

	/**
	 * Constructeur param�tr�.
	 * 
	 * @param p_coordList Liste de coordonn�es.
	 */
	public Path(final CoordList p_coordList)
	{
		Assert.precondition(p_coordList != null, "Coord List");
		Assert.precondition(p_coordList.size() > 0, "Path too small.");
		validateCoordList(p_coordList);

		_list = p_coordList;
	}

	public Path(final Coord p_coord)
	{
		Assert.precondition(p_coord != null, "Coord");
		_list = new CoordList();
		_list.add(p_coord);
	}

	// AAA Ajouter un TU pour cette m�thode (voir le constructeur
	private void validateCoordList(final CoordList p_coordList)
	{
		final Iterator iter = p_coordList.iterator();
		Coord currentCoord = (Coord) iter.next();
		Coord secondCoord;
		while (iter.hasNext())
		{
			secondCoord = (Coord) iter.next();
			Assert.precondition(currentCoord.calculateIntDistance(secondCoord) == 1, "Invalid distance between coord");
			currentCoord = secondCoord;
		}
	}

	/**
	 * Retourne la prochaine coordonn�e comprise dans le parcours. Si la
	 * position courante du pointeur de parcours est situ�e � la fin de
	 * celui-ci, la m�thode retourne une exception EndOfPathException.
	 * 
	 * @return La prochaine coordonn�e ou <code>null</code> lorsque la fin est
	 *         atteint.
	 */
	public final Coord getNextStep()
	{

		// Les pr�conditions sont v�rifi�es dans la m�thode appell�e ci-dessous
		Coord coord = getNextStepWithoutConsume();
		_list.remove(0);
		if (_moveCostList.size() > 0)
		{
			_moveCostList.remove(0);
		}
		if (_turnCostList.size() > 0)
		{
			_turnCostList.remove(0);
		}

		if (_list.size() == 0)
		{
			_list = null;
		}
		return coord;
	}

	// AAAAZ a tester
	public final Coord getEndTurnPosition()
	{

		final int currentTurn = _turnCostList.get(0);
		Coord endTurnPosition = null;
		for (int i = 0; i < _turnCostList.size() && _turnCostList.get(i) == currentTurn; i++)
		{
			endTurnPosition = _list.get(i);
		}
		return endTurnPosition;
	}
	
	/**
	 * Return the Turn cost required for the next path position.
	 * 
	 * @return Turn cost
	 */
	public final int getNextTurnCost()
	{
		return _turnCostList.get(0);
	}

	/**
	 * Indicate if the current path includes a specific position.
	 * 
	 * @param p_coord expected position.
	 * @return True or False.
	 */
	public final boolean contains(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		return _list.contains(p_coord);
	}

	public final Coord getNextStepWithoutConsume()
	{
		Assert.check(_list != null, "The path is empty.");

		return (Coord) _list.get(0);
	}

	/**
	 * Indicate if the end of the path have been reached.
	 * 
	 * @return True or False.
	 */
	public final boolean hasNextStep()
	{
		return _list != null;
	}

	public final boolean hasSecondStepInSameTurn()
	{
		if (_list == null)
		{
			return false;
		}

		if (_list.size() <= 1)
		{
			return false;
		}

		return _turnCostList.get(0).intValue() == _turnCostList.get(1).intValue();
	}

	public final void clear()
	{
		if (_list != null)
		{
			_list.clear();
			_list = null;
		}
		_moveCostList.clear();
	}

	/**
	 * Indique si les chemins sont �gaux.
	 * 
	 * @param p_path Chemin � comparer.
	 * @return Vrai ou faux.
	 */
	@Override
	public final boolean equals(final Object p_path)
	{
		Assert.precondition(p_path != null, "Null param.");

		if (!(p_path instanceof Path))
		{
			return false;
		}

		if (((Path) p_path)._list.size() != _list.size())
			return false;

		final Iterator iter1 = _list.iterator();
		final Iterator iter2 = ((Path) p_path)._list.iterator();

		Coord coord1;
		Coord coord2;

		// Compare chacune des coordonn�es des chemins.
		while (iter1.hasNext())
		{
			coord1 = (Coord) iter1.next();
			coord2 = (Coord) iter2.next();
			if (!coord1.equals(coord2))
				return false;
		}

		return true;
	}

	public final Coord getDestination()
	{
		Assert.check(_list != null, "Path is empty");

		return (Coord) _list.get(_list.size() - 1);
	}

	public final CoordList getCoordList()
	{
		Assert.check(_list != null, "Path is empty");

		return _list;
	}

	public final boolean isEmpty()
	{
		return _list == null;
	}

	@Override
	public final int hashCode()
	{
		return _list.hashCode();
	}

	public final boolean fallsOnEnemyPosition(final CoordSet p_enemyCoordSet)
	{
		Assert.preconditionNotNull(p_enemyCoordSet, "Enemy Coord Set");

		if (_list == null)
		{
			return false;
		}

		boolean pathFallsOnEnemy = false;

		Iterator iter = _list.iterator();
		Coord currentPathCoord;
		while (iter.hasNext())
		{
			currentPathCoord = (Coord) iter.next();
			Assert.preconditionNotNull(currentPathCoord, "Enemy Coord Item");
			if (p_enemyCoordSet.contains(currentPathCoord))
			{
				pathFallsOnEnemy = true;
				break;
			}
		}

		return pathFallsOnEnemy;
	}

	public final void removeLastStep()
	{
		Assert.check(_list != null, "Empty Path");
		_list.remove(_list.size() - 1);
		_moveCostList.clear();
	}

	@Override
	public final Object clone()
	{
		final CoordList coordList = new CoordList();
		final Iterator<Coord> iter = _list.iterator();
		while (iter.hasNext())
		{
			coordList.add((Coord) iter.next().clone());
		}
		return new Path(coordList);
	}

	/**
	 * Inverse le chemin. Il faut cependant faire attention � la notion suivante
	 * : un chemin (<code>Path</code>) ne contient pas la coordonn�es d'origine,
	 * car l'unit� y est d�j�. Donc, lorsque le path est invers�, il faut
	 * �galement invers� la position de d�part, "absente" du chemin. Voici un
	 * exemple : Si l'unit� � (0,0) poss�de le path (1,1) et (2,2). Ce chemin a
	 * donc comme point de d�part (0,0) et point de destination (2,2).
	 * L'inversion de ce path implique donc n�cessairement de supprimer la
	 * nouvelle position de d�part, c'est � dire la position (2,2). Par contre,
	 * la nouvelle destination doit �tre ajout�e, c'est-�-dire la position
	 * (0,0). Ainsi, l'ancien path (1,1)->(2,2) devient donc (1,1)->(0,0).
	 * 
	 * @param p_firstPos Position d'origine (non incluse dans le chemin)
	 */
	public void reversePath(final Coord p_firstPos)
	{
		// Supprime le dernier �l�ment lorsque le path est invers�
		_list.remove(_list.size() - 1);
		Collections.reverse(_list);
		_moveCostList.clear();

		// On ajoute la nouvelle destination, � savoir l'ancien point d'origine.
		_list.add(p_firstPos);
	}

	public final void appendToPath(final Path p_path)
	{
		Assert.preconditionNotNull(p_path, "Path");
		Assert.precondition(p_path.hasNextStep(), "This path is empty");
		_list.addAll(p_path._list);
		_moveCostList.clear();
	}

	/**
	 * Indicates if a fighter can follow this path without short of fuel.
	 * 
	 * @param p_fuel Starting fuel capacity.
	 * @param p_fuelCapacity Available fuel after restoration.
	 * @param p_cityCoordSet City where the fighter can be restored.
	 * @return True or false.
	 */
	public boolean validateFuelAlimentation(final int p_fuel, final int p_fuelCapacity, final CoordSet p_cityCoordSet)
	{
		Assert.precondition(p_fuel > 0, "Invalid fuel");
		Assert.precondition(p_fuelCapacity > 1, "Invalid fuel capacity");
		Assert.preconditionNotNull(p_cityCoordSet, "City Coord Set");

		Coord currentCoord = null;
		Coord destination = this.getDestination();
		int currentFuel;
		int comebackFuelNeeded = 0;
		if (p_cityCoordSet.isEmpty())
		{
			if (_list.size() <= p_fuel / 2)
			{
				return true;
			}
		} else
		{
			Coord nearestCityOfTheDestination = p_cityCoordSet.getDirectNearestCoord(destination);
			comebackFuelNeeded = nearestCityOfTheDestination.calculateIntDistance(destination);
		}
		currentFuel = p_fuel;
		for (int i = 0; i < _list.size(); i++)
		{
			if (currentFuel == 0)
			{
				return false;
			}
			currentCoord = (Coord) _list.get(i);
			if (p_cityCoordSet.contains(currentCoord))
			{
				currentFuel = p_fuelCapacity;
			} else
			{
				--currentFuel;
			}
		}

		if (comebackFuelNeeded > currentFuel)
		{
			return false;
		}
		return true;
	}

	/**
	 * Set the move cost for each coord of the path.
	 * 
	 * @param p_map Current Map
	 * @param p_moveCost Current Move Cost
	 * @return the number of turn
	 */
	public final int calculateTurnCost(	final FSMap p_map,
										final AbstractMoveCost p_moveCost,
										final int p_move,
										final int p_turnMoveCapacity)
	{
		Assert.preconditionNotNull(p_map, "Map");
		Assert.preconditionNotNull(p_moveCost, "Move Cost");
		Assert.precondition(hasNextStep(), "This path is empty");
		Assert.precondition(p_move >= 0 && p_move <= p_turnMoveCapacity, "Invalid Move Capacity");
		Assert.precondition(p_turnMoveCapacity > 0, "Invalid Turn Move Capacity");

		_moveCostList.clear();
		_turnCostList.clear();
		int currentMoveCost = 0;
		Coord currentCoord = null;
		Cell currentCell = null;
		int currentTurn = 0;
		int currentMoveCapacity = p_move;
		final Iterator<Coord> coordIter = _list.iterator();
		while (coordIter.hasNext())
		{
			currentCoord = coordIter.next();
			currentCell = p_map.getCellAt(currentCoord);
			currentMoveCost = p_moveCost.getCost(currentCell);
			_moveCostList.add(new Integer(currentMoveCost));

			if (currentMoveCapacity >= currentMoveCost)
			{
				currentMoveCapacity -= currentMoveCost;
			} else
			{
				currentMoveCapacity = p_turnMoveCapacity - currentMoveCost;
				++currentTurn;
			}
			_turnCostList.add(new Integer(currentTurn));
		}

		return currentTurn;
	}

	public final int getTotalDistance(	final FSMap p_map,
									final AbstractMoveCost p_moveCost,
									final int p_moveCapacity,
									final int p_turnMoveCapacity)
	{
		if (_moveCostList.size() != _list.size())
		{
			calculateTurnCost(p_map, p_moveCost, p_moveCapacity, p_turnMoveCapacity);
		} 

		return getTotalDistance();
	}
	
	public final int getTotalDistance()
	{
		Assert.check(_turnCostList.size() == _list.size(), "Invalid turn cost list (no up to date)");
		Assert.check(_moveCostList.size() == _list.size(), "Invalid turn cost list (no up to date)");
		
		int totalMoveCost = 0;
		int nbTurn = _turnCostList.get(_turnCostList.size() - 1);
		totalMoveCost = nbTurn * 100;
		
		for (int i = _moveCostList.size() - 1; i >= 0 && nbTurn == _turnCostList.get(i); i--)
		{
			totalMoveCost += _moveCostList.get(i);
		}

		return totalMoveCost;
	}

	/**
	 * Calculate the total move cost to reach a specific coord in the
	 * current path.
	 * 
	 * @param p_coord Coord.
	 * @return Cumulated move cost.
	 */
	public int getTurnCostForCoord(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		Assert.precondition(hasNextStep(), "This path is empty");
		Assert.check(_moveCostList.size() == _list.size(), "Move Cost List is not synchronized with coord list");
		Assert.precondition(_list.contains(p_coord), "Invalid Coord (not contained in the path)");

		final int idx = _list.lastIndexOf(p_coord);
		return ((Integer) _turnCostList.get(idx)).intValue();
	}

	@Override
	public final String toString()
	{
		final StringBuffer sb = new StringBuffer("{PATH= Coords[");
		sb.append(_list.size()).append("] : ");
		Coord currentCoord = null;
		final Iterator iter = _list.iterator();
		while (iter.hasNext())
		{
			currentCoord = (Coord) iter.next();
			sb.append(" (");
			sb.append(currentCoord.getX());
			sb.append(",");
			sb.append(currentCoord.getY());
			sb.append(")");

		}
		sb.append("]}");
		return sb.toString();
	}
}