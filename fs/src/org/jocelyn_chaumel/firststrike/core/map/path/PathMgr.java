/*
 * PathFinderMgr.java Created on 27 avril 2003, 18:22
 */

package org.jocelyn_chaumel.firststrike.core.map.path;

import java.io.Serializable;
import java.util.HashMap;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.AbstractMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Ce gestionnaire permet de trouver un parcours optimis� entre deux points sur
 * une carte.
 */
public class PathMgr implements Serializable
{
	// private static final FSLogger _logger =
	// FSLoggerManager.getLogger(PathMgr.class.getName());

	private final HashMap<AreaTypeCst, PathCache> _pathCacheMap = new HashMap<AreaTypeCst, PathCache>();

	private final AreaPathFinder _pathFinder;

	private final AirPathFinder _airPathFinder;
	private final FSMap _map;

	/**
	 * Constructeur priv�. Cette classe est un singleton.
	 */
	public PathMgr(final FSMap p_map)
	{
		Assert.preconditionNotNull(p_map);

		_pathFinder = new AreaPathFinder(p_map);
		_airPathFinder = new AirPathFinder(p_map.getWidth(), p_map.getHeight(), p_map.getCityList());
		_map = p_map;
	}

	public final AirPathFinder getAirPathFinder()
	{
		return _airPathFinder;
	}

	/**
	 * Returns an area path. This method use the CoordSet(p_coordSetToExclude)
	 * to identify all invalid coords (not included in the expected path). <br>
	 * For example all enemy cities's coord must be included in the CoordSet.
	 * 
	 * @param p_startPos Start position.
	 * @param p_endPos destination position.
	 * @param p_moveCost Move cost implementation (Ground or Sea are only
	 *        supported)
	 * @param p_coordSetToExclude Contains all coords not accessible (city's
	 *        coord, enemies's coord, and so on...).
	 * @param p_excludeDestinationCoord Indicate if the destination could not be
	 *        reached but only the the nearest bordered area's cell.
	 * @return An area Path.
	 * @throws PathNotFoundException
	 */
	public final Path getAreaPath(	final Coord p_startPos,
									final Coord p_endPos,
									final AbstractMoveCost p_moveCost,
									final int p_moveCapacity,
									final int p_turnMoveCapacity,
									final CoordSet p_coordSetToExclude,
									final boolean p_excludeDestinationCoord) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_startPos, "Start Coord");
		Assert.preconditionNotNull(p_endPos, "Destination coord");
		Assert.preconditionNotNull(p_moveCost, "Move Cost Implementation");
		Assert.precondition(p_moveCapacity >= 0 && p_moveCapacity <= p_turnMoveCapacity, "Invalid Move Capacity");
		Assert.precondition(p_turnMoveCapacity > 0, "Invalid Turn Move Capacity");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord Set to exclude");
		Assert.precondition(!p_startPos.equals(p_endPos), "Start & destination coord must be different.");
		Assert.precondition(!p_coordSetToExclude.contains(p_startPos),
							"CoordSetToExclude contains the starting position");
		Assert.precondition(!p_moveCost.equals(MoveCostFactory.AIR_MOVE_COST), "Unsupported move cost implementation");

		final Area startArea = _map.getCellAt(p_startPos).getAreaList().getFirstAreaByType(p_moveCost.getAreaTypeCst());
		if (!p_excludeDestinationCoord)
		{
			return getAreaPathIncludingDestination(p_startPos, p_endPos, p_moveCost, p_coordSetToExclude);
		}

		final Area destinationArea = _map.getCellAt(p_endPos).getAreaList()
				.getFirstAreaByType(p_moveCost.getAreaTypeCst());

		// Find path in the same area (start and destination)
		if (startArea.equals(destinationArea))
		{
			final Path path = getAreaPathIncludingDestination(p_startPos, p_endPos, p_moveCost, p_coordSetToExclude);
			if (path != null)
			{
				path.removeLastStep();
			}
			return path;
		}

		// Check if the destination is in the same start area

		final int maxX = _map.getWidth() - 1;
		final int maxY = _map.getHeight() - 1;
		final int x = p_endPos.getX();
		final int y = p_endPos.getY();
		Coord currentCoord = null;
		Path bestPath = null;
		Path currentPath = null;
		float bestDistance = Float.MAX_VALUE;
		float currentDistance = Float.MAX_VALUE;

		// x - -
		// - O -
		// - - -
		if (x > 0 && y > 0)
		{
			currentCoord = new Coord(x - 1, y - 1);
			if (isValidAreaPosition(startArea, currentCoord, p_coordSetToExclude))
			{
				if (currentCoord.calculateIntDistance(p_startPos) == 1)
				{
					currentPath = new Path(currentCoord);
				} else
				{
					try
					{
						currentPath = getAreaPathIncludingDestination(	p_startPos,
																		currentCoord,
																		p_moveCost,
																		p_coordSetToExclude);
					} catch (PathNotFoundException ex)
					{
						// Destination not accessible
						currentPath = null;
					}
				}
				if (currentPath != null)
				{
					currentDistance = currentPath.getTotalDistance(_map, p_moveCost, p_moveCapacity, p_turnMoveCapacity);
					if (currentDistance < bestDistance)
					{
						bestDistance = currentDistance;
						bestPath = currentPath;
					}
				}
			}
		}

		// - x -
		// - O -
		// - - -
		if (y > 0)
		{
			currentCoord = new Coord(x, y - 1);
			if (isValidAreaPosition(startArea, currentCoord, p_coordSetToExclude))
			{
				if (currentCoord.calculateIntDistance(p_startPos) == 1)
				{
					currentPath = new Path(currentCoord);
				} else
				{
					try
					{
						currentPath = getAreaPathIncludingDestination(	p_startPos,
																		currentCoord,
																		p_moveCost,
																		p_coordSetToExclude);
					} catch (PathNotFoundException ex)
					{
						// Destination not accessible
						currentPath = null;
					}
				}
				if (currentPath != null)
				{
					currentDistance = currentPath.getTotalDistance(_map, p_moveCost, p_moveCapacity, p_turnMoveCapacity);
					if (currentDistance < bestDistance)
					{
						bestDistance = currentDistance;
						bestPath = currentPath;
					}
				}
			}
		}
		// - - x
		// - O -
		// - - -
		if (x < maxX && y > 0)
		{
			currentCoord = new Coord(x + 1, y - 1);
			if (isValidAreaPosition(startArea, currentCoord, p_coordSetToExclude))
			{
				if (currentCoord.calculateIntDistance(p_startPos) == 1)
				{
					currentPath = new Path(currentCoord);
				} else
				{
					try
					{
						currentPath = getAreaPathIncludingDestination(	p_startPos,
																		currentCoord,
																		p_moveCost,
																		p_coordSetToExclude);
					} catch (PathNotFoundException ex)
					{
						// Destination not accessible
						currentPath = null;
					}
				}
				if (currentPath != null)
				{
					currentDistance = currentPath.getTotalDistance(_map, p_moveCost, p_moveCapacity, p_turnMoveCapacity);
					if (currentDistance < bestDistance)
					{
						bestDistance = currentDistance;
						bestPath = currentPath;
					}
				}
			}
		}

		// - - -
		// - O x
		// - - -
		if (x < maxX)
		{
			currentCoord = new Coord(x + 1, y);
			if (isValidAreaPosition(startArea, currentCoord, p_coordSetToExclude))
			{
				if (currentCoord.calculateIntDistance(p_startPos) == 1)
				{
					currentPath = new Path(currentCoord);
				} else
				{
					try
					{
						currentPath = getAreaPathIncludingDestination(	p_startPos,
																		currentCoord,
																		p_moveCost,
																		p_coordSetToExclude);
					} catch (PathNotFoundException ex)
					{
						// Destination not accessible
						currentPath = null;
					}
				}
				if (currentPath != null)
				{
					currentDistance = currentPath.getTotalDistance(_map, p_moveCost, p_moveCapacity, p_turnMoveCapacity);
					if (currentDistance < bestDistance)
					{
						bestDistance = currentDistance;
						bestPath = currentPath;
					}
				}
			}
		}
		// - - -
		// - O -
		// - - x
		if (x < maxX && y < maxY)
		{
			currentCoord = new Coord(x + 1, y + 1);
			if (isValidAreaPosition(startArea, currentCoord, p_coordSetToExclude))
			{
				if (currentCoord.calculateIntDistance(p_startPos) == 1)
				{
					currentPath = new Path(currentCoord);
				} else
				{
					try
					{
						currentPath = getAreaPathIncludingDestination(	p_startPos,
																		currentCoord,
																		p_moveCost,
																		p_coordSetToExclude);
					} catch (PathNotFoundException ex)
					{
						// Destination not accessible
						currentPath = null;
					}
				}
				if (currentPath != null)
				{
					currentDistance = currentPath.getTotalDistance(_map, p_moveCost, p_moveCapacity, p_turnMoveCapacity);
					if (currentDistance < bestDistance)
					{
						bestDistance = currentDistance;
						bestPath = currentPath;
					}
				}
			}
		}
		// - - -
		// - O -
		// - x -
		if (y < maxY)
		{
			currentCoord = new Coord(x, y + 1);
			if (isValidAreaPosition(startArea, currentCoord, p_coordSetToExclude))
			{
				if (currentCoord.calculateIntDistance(p_startPos) == 1)
				{
					currentPath = new Path(currentCoord);
				} else
				{
					try
					{
						currentPath = getAreaPathIncludingDestination(	p_startPos,
																		currentCoord,
																		p_moveCost,
																		p_coordSetToExclude);
					} catch (PathNotFoundException ex)
					{
						// Destination not accessible
						currentPath = null;
					}
				}
				if (currentPath != null)
				{
					currentDistance = currentPath.getTotalDistance(_map, p_moveCost, p_moveCapacity, p_turnMoveCapacity);
					if (currentDistance < bestDistance)
					{
						bestDistance = currentDistance;
						bestPath = currentPath;
					}
				}
			}
		}
		// - - -
		// - O -
		// x - -
		if (x > 0 && y < maxY)
		{
			currentCoord = new Coord(x - 1, y + 1);
			if (isValidAreaPosition(startArea, currentCoord, p_coordSetToExclude))
			{
				if (currentCoord.calculateIntDistance(p_startPos) == 1)
				{
					currentPath = new Path(currentCoord);
				} else
				{
					try
					{
						currentPath = getAreaPathIncludingDestination(	p_startPos,
																		currentCoord,
																		p_moveCost,
																		p_coordSetToExclude);
					} catch (PathNotFoundException ex)
					{
						// Destination not accessible
						currentPath = null;
					}
				}
				if (currentPath != null)
				{
					currentDistance = currentPath.getTotalDistance(_map, p_moveCost, p_moveCapacity, p_turnMoveCapacity);
					if (currentDistance < bestDistance)
					{
						bestDistance = currentDistance;
						bestPath = currentPath;
					}
				}
			}
		}
		// - - -
		// x O -
		// - - -
		if (x > 0)
		{
			currentCoord = new Coord(x - 1, y);
			if (isValidAreaPosition(startArea, currentCoord, p_coordSetToExclude))
			{
				if (currentCoord.calculateIntDistance(p_startPos) == 1)
				{
					currentPath = new Path(currentCoord);
				} else
				{
					try
					{
						currentPath = getAreaPathIncludingDestination(	p_startPos,
																		currentCoord,
																		p_moveCost,
																		p_coordSetToExclude);
					} catch (PathNotFoundException ex)
					{
						// Destination not accessible
						currentPath = null;
					}
				}
				if (currentPath != null)
				{
					currentDistance = currentPath.getTotalDistance(_map, p_moveCost, p_moveCapacity, p_turnMoveCapacity);
					if (currentDistance < bestDistance)
					{
						bestDistance = currentDistance;
						bestPath = currentPath;
					}
				}
			}
		}

		if (bestPath == null)
		{
			throw new PathNotFoundException("Coord :" + p_endPos.toStringLight());
		}
		return bestPath;

	}

	public final boolean isValidAreaPosition(	final Area p_startArea,
												final Coord p_currentCoord,
												final CoordSet p_coordSetToExclude)
	{
		if (p_coordSetToExclude.contains(p_currentCoord))
		{
			return false;
		}

		final Cell currentCell = _map.getCellAt(p_currentCoord);

		if (!currentCell.getAreaList().contains(p_startArea))
		{
			return false;
		}

		return true;
	}

	/**
	 * Retourne le chemin le plus court pour une unit� terrestre entre 2 points.
	 * 
	 * @param p_startPos Position de d�part.
	 * @param p_endPos Position d'arriv�e.
	 * @param p_moveCostList Capacit� de mouvement de l'unit�.
	 * @param p_coordSetToExcluse Liste des cellules inaxcessibles.
	 * @return Le parcours le plus court.
	 * @throws PathNotFoundException
	 */
	public final Path getAreaPathIncludingDestination(	final Coord p_startPos,
														final Coord p_endPos,
														final AbstractMoveCost p_moveCost,
														final CoordSet p_coordSetToExclude) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_startPos, "Start Position");
		Assert.preconditionNotNull(p_endPos, "End Position");
		Assert.preconditionNotNull(p_moveCost, "Move Cost");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord List To Exclude");
		Assert.precondition(!p_coordSetToExclude.contains(p_startPos),
							"CoordSetToExclude contains the starting position");
		Assert.precondition(!p_startPos.equals(p_endPos), "Depature should be different from arrival.");

		if (p_coordSetToExclude.contains(p_endPos))
		{
			throw new PathNotFoundException("Coord :" + p_endPos.toStringLight());
		}
		final AreaTypeCst areaType = p_moveCost.getAreaTypeCst();
		final PathCache pathCache = getTypePathCache(areaType);
		Path path = null;
		path = pathCache.getPathFromCache(p_moveCost, p_startPos, p_endPos);
		if (path != null && path.fallsOnEnemyPosition(p_coordSetToExclude))
		{
			path = _pathFinder.findAreaPathIncludingDestination(p_startPos, p_endPos, p_moveCost, p_coordSetToExclude);
		} else if (path == null)
		{
			path = _pathFinder.findAreaPathIncludingDestination(p_startPos, p_endPos, p_moveCost, new CoordSet());
			pathCache.addPathInCache(p_moveCost, p_startPos, p_endPos, path);
			if (path.fallsOnEnemyPosition(p_coordSetToExclude))
			{
				path = _pathFinder.findAreaPathIncludingDestination(p_startPos,
																	p_endPos,
																	p_moveCost,
																	p_coordSetToExclude);
			}
		}

		return path;
	}

	/**
	 * Retourne la cache d'apr�s le type de r�gion.
	 * 
	 * @param p_areaType Type de r�gion.
	 * 
	 * @return Cache d'un type de r�gion.
	 */
	private final PathCache getTypePathCache(final AreaTypeCst p_areaType)
	{
		Assert.preconditionNotNull(p_areaType, "Area Type");
		PathCache pathCache = (PathCache) _pathCacheMap.get(p_areaType);
		if (pathCache == null)
		{
			pathCache = new PathCache();
			_pathCacheMap.put(p_areaType, pathCache);
		}

		return pathCache;
	}

	public final Path getAirPath(	final Coord p_startPos,
									final Coord p_endPos,
									final int p_fuel,
									final int p_fuelCapacity,
									final AbstractPlayer p_player,
									final CoordSet p_coordSetToExclude,
									final boolean p_excludeDestinationCoord) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_startPos, "Start Position");
		Assert.preconditionNotNull(p_endPos, "End Position");
		Assert.precondition(p_fuel > 0, "Invalid fuel");
		Assert.precondition(p_fuelCapacity > 1, "Invalid fuel capacity");
		Assert.preconditionNotNull(p_player, "Player");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord Set To Exclude");
		Assert.precondition(!p_coordSetToExclude.contains(p_startPos),
							"CoordSetToExclude contains the starting position");
		Assert.precondition(!p_startPos.equals(p_endPos), "Depature should be different from arrival.");

		if (!p_excludeDestinationCoord)
		{
			return getAirPathIncludingDestinationCoord(	p_startPos,
														p_endPos,
														p_fuel,
														p_fuelCapacity,
														p_player,
														p_coordSetToExclude);
		}

		final int maxX = _map.getWidth() - 1;
		final int maxY = _map.getHeight() - 1;
		final int x = p_endPos.getX();
		final int y = p_endPos.getY();
		Coord currentCoord = null;
		Path bestPath = null;
		Path currentPath = null;
		int bestDistance = Integer.MAX_VALUE;
		int currentDistance = Integer.MAX_VALUE;

		// x - -
		// - O -
		// - - -
		if (x > 0 && y > 0)
		{
			currentCoord = new Coord(x - 1, y - 1);
			try
			{
				currentPath = getAirPathIncludingDestinationCoord(	p_startPos,
																	currentCoord,
																	p_fuel,
																	p_fuelCapacity,
																	p_player,
																	p_coordSetToExclude);
				currentDistance = currentPath.getCoordList().size();
				if (currentDistance < bestDistance)
				{
					bestDistance = currentDistance;
					bestPath = currentPath;
				}
			} catch (PathNotFoundException ex)
			{
				// Destination not accessible
			}
		}

		// - x -
		// - O -
		// - - -
		if (y > 0)
		{
			currentCoord = new Coord(x, y - 1);
			try
			{
				currentPath = getAirPathIncludingDestinationCoord(	p_startPos,
																	currentCoord,
																	p_fuel,
																	p_fuelCapacity,
																	p_player,
																	p_coordSetToExclude);
				currentDistance = currentPath.getCoordList().size();
				if (currentDistance < bestDistance)
				{
					bestDistance = currentDistance;
					bestPath = currentPath;
				}
			} catch (PathNotFoundException ex)
			{
				// Destination not accessible
			}
		}
		// - - x
		// - O -
		// - - -
		if (x < maxX && y > 0)
		{
			currentCoord = new Coord(x + 1, y - 1);
			try
			{
				currentPath = getAirPathIncludingDestinationCoord(	p_startPos,
																	currentCoord,
																	p_fuel,
																	p_fuelCapacity,
																	p_player,
																	p_coordSetToExclude);
				currentDistance = currentPath.getCoordList().size();
				if (currentDistance < bestDistance)
				{
					bestDistance = currentDistance;
					bestPath = currentPath;
				}
			} catch (PathNotFoundException ex)
			{
				// Destination not accessible
			}

		}

		// - - -
		// - O x
		// - - -
		if (x < maxX)
		{
			currentCoord = new Coord(x + 1, y);
			try
			{
				currentPath = getAirPathIncludingDestinationCoord(	p_startPos,
																	currentCoord,
																	p_fuel,
																	p_fuelCapacity,
																	p_player,
																	p_coordSetToExclude);
				currentDistance = currentPath.getCoordList().size();
				if (currentDistance < bestDistance)
				{
					bestDistance = currentDistance;
					bestPath = currentPath;
				}
			} catch (PathNotFoundException ex)
			{
				// Destination not accessible
			}
		}
		// - - -
		// - O -
		// - - x
		if (x < maxX && y < maxY)
		{
			currentCoord = new Coord(x + 1, y + 1);
			try
			{
				currentPath = getAirPathIncludingDestinationCoord(	p_startPos,
																	currentCoord,
																	p_fuel,
																	p_fuelCapacity,
																	p_player,
																	p_coordSetToExclude);
				currentDistance = currentPath.getCoordList().size();
				if (currentDistance < bestDistance)
				{
					bestDistance = currentDistance;
					bestPath = currentPath;
				}
			} catch (PathNotFoundException ex)
			{
				// Destination not accessible
			}
		}
		// - - -
		// - O -
		// - x -
		if (y < maxY)
		{
			currentCoord = new Coord(x, y + 1);
			try
			{
				currentPath = getAirPathIncludingDestinationCoord(	p_startPos,
																	currentCoord,
																	p_fuel,
																	p_fuelCapacity,
																	p_player,
																	p_coordSetToExclude);
				currentDistance = currentPath.getCoordList().size();
				if (currentDistance < bestDistance)
				{
					bestDistance = currentDistance;
					bestPath = currentPath;
				}
			} catch (PathNotFoundException ex)
			{
				// Destination not accessible
			}
		}
		// - - -
		// - O -
		// x - -
		if (x > 0 && y < maxY)
		{
			currentCoord = new Coord(x - 1, y + 1);
			try
			{
				currentPath = getAirPathIncludingDestinationCoord(	p_startPos,
																	currentCoord,
																	p_fuel,
																	p_fuelCapacity,
																	p_player,
																	p_coordSetToExclude);
				currentDistance = currentPath.getCoordList().size();
				if (currentDistance < bestDistance)
				{
					bestDistance = currentDistance;
					bestPath = currentPath;
				}
			} catch (PathNotFoundException ex)
			{
				// Destination not accessible
			}
		}
		// - - -
		// x O -
		// - - -
		if (x > 0)
		{
			currentCoord = new Coord(x - 1, y);
			try
			{
				currentPath = getAirPathIncludingDestinationCoord(	p_startPos,
																	currentCoord,
																	p_fuel,
																	p_fuelCapacity,
																	p_player,
																	p_coordSetToExclude);
				currentDistance = currentPath.getCoordList().size();
				if (currentDistance < bestDistance)
				{
					bestDistance = currentDistance;
					bestPath = currentPath;
				}
			} catch (PathNotFoundException ex)
			{
				// Destination not accessible
			}
		}

		if (bestPath == null)
		{
			throw new PathNotFoundException("Coord :" + p_endPos.toStringLight());
		}
		return bestPath;

	}

	public final Path getAirPathIncludingDestinationCoord(	final Coord p_startPos,
															final Coord p_destPos,
															final int p_fuel,
															final int p_fuelCapacity,
															final AbstractPlayer p_player,
															final CoordSet p_coordSetToExclude) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_startPos, "Start Position");
		Assert.preconditionNotNull(p_destPos, "End Position");
		Assert.precondition(p_fuel > 0, "Invalid fuel");
		Assert.precondition(p_fuelCapacity > 1, "Invalid fuel Capacity");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord List to Exclude");
		Assert.precondition(!p_coordSetToExclude.contains(p_startPos),
							"CoordSetToExclude contains the starting position");
		Assert.precondition(!p_startPos.equals(p_destPos), "Start position equals end position");

		if (p_coordSetToExclude.contains(p_destPos))
		{
			throw new PathNotFoundException("Coord :" + p_destPos.toStringLight());
		}

		final Path path = _airPathFinder.findPlayerPath(p_startPos,
														p_destPos,
														p_fuel,
														p_fuelCapacity,
														p_player,
														p_coordSetToExclude);

		return path;
	}

	public final boolean isAccessibleAreaCoord(	final Coord p_startPos,
												final Coord p_endPos,
												final AbstractMoveCost p_moveCost,
												final CoordSet p_coordSetToExclude,
												final boolean p_excludeDestinationCoord)
	{
		Assert.preconditionNotNull(p_startPos, "Start Coord");
		Assert.preconditionNotNull(p_endPos, "Destination coord");
		Assert.preconditionNotNull(p_moveCost, "Move Cost Implementation");
		Assert.preconditionNotNull(p_coordSetToExclude, "Coord Set to exclude");
		Assert.precondition(!p_coordSetToExclude.contains(p_startPos),
							"CoordSetToExclude contains the starting position");
		Assert.precondition(!p_startPos.equals(p_endPos), "Start & destination coord must be different.");

		// Assert check are done in next method.
		try
		{
			if (!p_excludeDestinationCoord)
			{
				final AreaTypeCst areaType = p_moveCost.getAreaTypeCst();
				final Area startArea = _map.getAreaAt(p_startPos, areaType);
				final Area destArea = _map.getAreaAt(p_endPos, areaType);
				if (!startArea.equals(destArea))
				{
					return false;
				}
			}
			final int defaultMoveCapacity = 100;
			getAreaPath(p_startPos,
						p_endPos,
						p_moveCost,
						defaultMoveCapacity,
						defaultMoveCapacity,
						p_coordSetToExclude,
						p_excludeDestinationCoord);
			return true;
		} catch (PathNotFoundException ex)
		{
			return false;
		}

	}

	/**
	 * Check if the destination coord is accessible for a Fighter.
	 * 
	 * @param p_startPos Start location.
	 * @param p_destPos Destination location.
	 * @param p_fuel Current fuel tank of the fighter.
	 * @param p_fuelCapacity Fuel capacity of the fighter after city restore.
	 * @param p_player Fighter owner.
	 * @param p_coordSetToExclude Excluded coord set from the resulted path.
	 * @return True or false.
	 */
	public final boolean isAccessibleAirCoord(	final Coord p_startPos,
												final Coord p_destPos,
												final int p_fuel,
												final int p_fuelCapacity,
												final AbstractPlayer p_player,
												final CoordSet p_coordSetToExclude,
												final boolean p_excludeDestinationCoord)
	{
		// Assert check are done in next method.
		try
		{
			getAirPath(	p_startPos,
						p_destPos,
						p_fuel,
						p_fuelCapacity,
						p_player,
						p_coordSetToExclude,
						p_excludeDestinationCoord);
			return true;
		} catch (PathNotFoundException ex)
		{
			return false;
		}
	}

	public final boolean isValidPath(final Coord p_startCoord, final Path p_path, final AbstractMoveCost p_moveCost)
	{
		Assert.preconditionNotNull(p_startCoord, "Start Coord");
		Assert.preconditionNotNull(p_path, "Path");
		Assert.preconditionNotNull(p_moveCost, "Move Cost");

		Coord currentCoord = p_startCoord;
		for (Coord nextCoord : p_path.getCoordList())
		{
			if (currentCoord.calculateIntDistance(nextCoord) != 1)
			{
				return false;
			}

			if (AbstractMoveCost.UNREACHABLE_CELL == p_moveCost.getCost(_map.getCellAt(nextCoord)))
			{
				return false;
			}
			currentCoord = nextCoord;
		}
		return true;
	}
}