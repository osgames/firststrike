package org.jocelyn_chaumel.firststrike.core.map.xml;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.AreaNameMap;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapInfo;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.InvalidMapException;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FSMapNode extends Element
{
	public final static String NODE_NAME = "Map";

	public FSMapNode(	final FSInfoNode p_infoNode,
						final FSAreasArrayNode p_areasArrayNode,
						final FSCellsArrayNode p_cellsNode)
	{
		super(NODE_NAME);

		addContent(p_infoNode);
		addContent(p_areasArrayNode);
		addContent(p_cellsNode);
	}

	public final static FSMap getFSMap(final String p_mapFilename, final Element p_mapNode, final boolean p_applyValidation) throws InvalidConstantValueException,
			InvalidMapException
	{
		Assert.preconditionNotNull(p_mapNode, "Info Node");
		Assert.precondition(p_mapNode.getName().equals(NODE_NAME), "Invalid Map Node:" + p_mapNode.getName());

		final Element infoNode = p_mapNode.getChild(FSInfoNode.NODE_NAME);
		final FSMapInfo mapInfo = FSInfoNode.getMapInfo(p_mapFilename, infoNode);

		final Element areasArrayNode = p_mapNode.getChild(FSAreasArrayNode.NODE_NAME);
		final AreaNameMap areaNameMap = FSAreasArrayNode.getAreaNameMap(areasArrayNode);

		final Element cellsArrayNode = p_mapNode.getChild(FSCellsArrayNode.NODE_NAME);
		final Cell[][] mapCells = FSCellsArrayNode.getCellsArray(cellsArrayNode, mapInfo.getX(), mapInfo.getY());

		final FSMap map = FSMapMgr.create(mapInfo, mapCells, areaNameMap, p_applyValidation);

		return map;
	}
}
