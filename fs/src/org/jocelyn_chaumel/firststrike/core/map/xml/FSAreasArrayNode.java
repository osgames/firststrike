package org.jocelyn_chaumel.firststrike.core.map.xml;

import java.util.Iterator;
import java.util.List;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaList;
import org.jocelyn_chaumel.firststrike.core.map.AreaNameMap;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class FSAreasArrayNode extends Element
{
	public final static String NODE_NAME = "AreasArray";
	private final static String AREA_NODE_NAME = "Area";
	private final static String X_ATTRIBUTE = "x";
	private final static String Y_ATTRIBUTE = "y";
	private final static String NAME_NODE_NAME = "Name";

	public FSAreasArrayNode(final AreaList p_groundAreaSet)
	{
		super(NODE_NAME);

		Area currentArea = null;
		Coord currentCoord = null;
		Element currentAreaNode = null;
		Element currentAreaNameNode = null;
		final Iterator<Area> areaIter = p_groundAreaSet.iterator();
		while (areaIter.hasNext())
		{
			currentArea = areaIter.next();
			currentCoord = currentArea.getFirstCoordOfArea();
			currentAreaNode = new Element(AREA_NODE_NAME);
			currentAreaNode.setAttribute(X_ATTRIBUTE, "" + currentCoord.getX());
			currentAreaNode.setAttribute(Y_ATTRIBUTE, "" + currentCoord.getY());
			this.addContent(currentAreaNode);

			currentAreaNameNode = new Element(NAME_NODE_NAME);
			currentAreaNameNode.setText(currentArea.getInternalAreaName());
			currentAreaNode.addContent(currentAreaNameNode);
		}
	}

	public static final AreaNameMap getAreaNameMap(final Element p_areasArrayNode)
	{

		final List areaNodeList = p_areasArrayNode.getChildren(AREA_NODE_NAME);
		final AreaNameMap areaNameMap = new AreaNameMap();

		Element currentAreaNode = null;
		int x, y;
		String currentName = null;
		Coord currentCoord = null;
		final Iterator nodeIter = areaNodeList.iterator();
		while (nodeIter.hasNext())
		{
			currentAreaNode = (Element) nodeIter.next();
			x = Integer.parseInt(currentAreaNode.getAttribute(X_ATTRIBUTE).getValue());
			y = Integer.parseInt(currentAreaNode.getAttribute(Y_ATTRIBUTE).getValue());
			currentCoord = new Coord(x, y);

			currentName = currentAreaNode.getChild(NAME_NODE_NAME).getText();
			areaNameMap.put(currentCoord, currentName);
		}

		return areaNameMap;
	}
}
