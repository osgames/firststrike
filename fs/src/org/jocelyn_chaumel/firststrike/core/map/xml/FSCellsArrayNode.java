package org.jocelyn_chaumel.firststrike.core.map.xml;

import java.util.Iterator;
import java.util.List;

import org.jdom2.Element;
import org.jdom2.filter.ElementFilter;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FSCellsArrayNode extends Element
{
	public final static String NODE_NAME = "CellsArray";

	public FSCellsArrayNode(final Cell[][] p_cellArray, final int p_x, final int p_y)
	{
		super(NODE_NAME);

		FSCellNode cellNode = null;
		Cell cell = null;

		for (int y = 0; y < p_y; y++)
		{
			for (int x = 0; x < p_x; x++)
			{
				cell = p_cellArray[y][x];
				cellNode = new FSCellNode(cell);
				addContent(cellNode);
			}
		}

	}

	public final static Cell[][] getCellsArray(final Element p_cellsArrayNode, final int p_x, final int p_y) throws InvalidConstantValueException
	{
		Assert.preconditionNotNull(p_cellsArrayNode, "Cells Array Node");
		Assert.precondition(p_cellsArrayNode.getName().equals(FSCellsArrayNode.NODE_NAME),
							"Invalide Cells Array Node name");

		final List<Element> cellNodeList = p_cellsArrayNode.getContent(new ElementFilter(FSCellNode.NODE_NAME));
		final Iterator<Element> cellNodeIter = cellNodeList.iterator();
		final Cell[][] cellArray = new Cell[p_y][p_x];
		int x, y;
		Element currentNode = null;
		Cell currentCell = null;
		while (cellNodeIter.hasNext())
		{
			currentNode = cellNodeIter.next();
			currentCell = FSCellNode.getCell(currentNode);
			y = currentCell.getCellCoord().getY();
			x = currentCell.getCellCoord().getX();

			Assert.precondition(cellArray[y][x] == null, "More than one (X,Y) Cell has been detected");
			cellArray[y][x] = currentCell;
		}

		return cellArray;
	}
}
