package org.jocelyn_chaumel.firststrike.core.map.xml;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FSCellNode extends Element
{
	public static final String NODE_NAME = "Cell";

	private static final String ATTR_X = "x";
	private static final String ATTR_Y = "y";
	private static final String ATTR_INDEX = "index";
	private static final String ATTR_TYPE = "type";

	private static final String CHILD_NAME = "Name";

	public FSCellNode(final Cell p_cell)
	{
		super(NODE_NAME);

		setAttribute(ATTR_TYPE, String.valueOf(p_cell.getCellType().getValue()));
		setAttribute(ATTR_INDEX, String.valueOf(p_cell.getIndex()));
		setAttribute(ATTR_X, String.valueOf(p_cell.getCellCoord().getX()));
		setAttribute(ATTR_Y, String.valueOf(p_cell.getCellCoord().getY()));

		final String name = p_cell.getCellName();

		if (name != null && name.length() > 0)
		{
			final Element nameNode = new Element(CHILD_NAME);
			nameNode.addContent(name);
			addContent(nameNode);
		}
	}

	public static final Cell getCell(final Element p_cellNode) throws InvalidConstantValueException
	{
		Assert.preconditionNotNull(p_cellNode, "Cell Node");
		Assert.precondition(p_cellNode.getName().equals(NODE_NAME), "Invalid Node Name");

		final int type = Integer.parseInt(p_cellNode.getAttributeValue(ATTR_TYPE));
		final CellTypeCst cellType = CellTypeCst.getInstante(type);

		final int index = Integer.parseInt(p_cellNode.getAttributeValue(ATTR_INDEX));
		final int x = Integer.parseInt(p_cellNode.getAttributeValue(ATTR_X));
		final int y = Integer.parseInt(p_cellNode.getAttributeValue(ATTR_Y));

		final Element nameNode = p_cellNode.getChild(CHILD_NAME);
		final String name;
		if (nameNode != null)
		{
			name = nameNode.getText();
		} else
		{
			name = null;
		}

		return new Cell(cellType, index, new Coord(x, y), name, false);
	}
}
