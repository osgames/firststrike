package org.jocelyn_chaumel.firststrike.core.map.xml;

import org.jdom2.Element;
import org.jocelyn_chaumel.firststrike.core.map.FSMapInfo;

public class FSInfoNode extends Element
{
	public final static String NODE_NAME = "Info";

	private final static String CHILD_NAME = "Title";
	private final static String CHILD_OPTIONAL_DESC = "Descritpion";
	private final static String CHILD_OPTIONAL_DEFAULT_SKIN = "DefaultSkin";

	private final static String ATTR_X = "x";
	private final static String ATTR_Y = "y";
	private final static String ATTR_VERSION = "version";

	public FSInfoNode(final FSMapInfo p_mapInfo)
	{
		super(NODE_NAME);

		super.setAttribute(ATTR_X, "" + p_mapInfo.getX());
		super.setAttribute(ATTR_Y, "" + p_mapInfo.getY());
		super.setAttribute(ATTR_VERSION, p_mapInfo.getVersion());

		final Element nameNode = new Element(CHILD_NAME);
		nameNode.addContent(p_mapInfo.getName());
		addContent(nameNode);

		if (p_mapInfo.getDescription() != null && p_mapInfo.getDescription().length() > 0)
		{
			final Element descNode = new Element(CHILD_OPTIONAL_DESC);
			descNode.addContent(p_mapInfo.getDescription());
			addContent(descNode);
		}

		if (p_mapInfo.hasDefaultSkin())
		{
			final Element defaultSkinNode = new Element(CHILD_OPTIONAL_DEFAULT_SKIN);
			defaultSkinNode.addContent(p_mapInfo.getDefaultSkin());
			addContent(defaultSkinNode);

		}
	}

	public static final FSMapInfo getMapInfo(final String p_mapFilename, final Element p_infoNode)
	{
		final int x = FSInfoNode.getX(p_infoNode);
		final int y = FSInfoNode.getY(p_infoNode);
		final String title = FSInfoNode.getTitle(p_infoNode);
		final String desc = FSInfoNode.getDescription(p_infoNode);
		final String version = FSInfoNode.getVersion(p_infoNode);

		final FSMapInfo mapInfo = new FSMapInfo(p_mapFilename, title, desc, version, x, y);
		final String defaultSkin = FSInfoNode.getDefaultSkin(p_infoNode);

		if (defaultSkin != null)
		{
			mapInfo.setDefaultSkin(defaultSkin);
		}

		return mapInfo;
	}

	public static final int getX(final Element p_infoNode)
	{
		final String xStr = p_infoNode.getAttribute("x").getValue();

		return Integer.parseInt(xStr);
	}

	public static final int getY(final Element p_infoNode)
	{
		final String yStr = p_infoNode.getAttribute("y").getValue();

		return Integer.parseInt(yStr);
	}

	public static final String getTitle(final Element p_infoNode)
	{
		return p_infoNode.getChild(CHILD_NAME).getText();
	}

	public static final String getDescription(final Element p_infoNode)
	{
		return p_infoNode.getChild(CHILD_OPTIONAL_DESC).getText();
	}

	public static final String getDefaultSkin(final Element p_infoNode)
	{
		final Element defaultSkinNode = p_infoNode.getChild(CHILD_OPTIONAL_DEFAULT_SKIN);
		if (defaultSkinNode == null)
		{
			return null;
		}
		return defaultSkinNode.getText();
	}

	public static final String getVersion(final Element p_infoNode)
	{
		return p_infoNode.getAttributeValue(ATTR_VERSION);
	}

}
