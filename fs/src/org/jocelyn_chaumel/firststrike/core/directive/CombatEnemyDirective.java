package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.CombatResultCst;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class CombatEnemyDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(CombatEnemyDirective.class);

	private final AbstractUnit _enemy;
	private final boolean _useAllAttackPts;

	public CombatEnemyDirective(final AbstractUnit p_unit, final AbstractUnit p_enemy, final boolean p_useAllAttackPts)
	{
		super(p_unit);
		Assert.preconditionNotNull(p_enemy, "Enemy unit");
		Assert.precondition(p_unit.getPlayer().isEnnemyPlayer(p_enemy.getPlayer()), "Invalid enemy unit");

		_enemy = p_enemy;
		_useAllAttackPts = p_useAllAttackPts;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitSubAction("Combat Enemy starting");
		final AbstractUnit unit = getUnit();
		final int minNbAttack;
		if (_useAllAttackPts)
		{
			minNbAttack = 0;
		} else
		{
			minNbAttack = 1;
		}
		if (unit.getNbAttack() <= minNbAttack)
		{
			return MoveStatusCst.MOVE_SUCCESFULL;
		}

		boolean isFirstAttack = true;
		CombatResultCst combatResult = null;
		do
		{
			if (!isFirstAttack)
			{
				_logger.unitDetail("Another attack is necessary...");
			} else
			{
				isFirstAttack = false;
			}
			combatResult = unit.attackEnnemy(_enemy).getCombatResult();

			if (combatResult == CombatResultCst.ATTACKER_WIN)
			{
				break;
			} else if (combatResult == CombatResultCst.DEFENSER_WIN)
			{
				break;
			} else if (combatResult == CombatResultCst.ATT_AND_DEF_LOOSE)
			{
				break;
			}

		} while (unit.getNbAttack() > minNbAttack && unit.getMove() > 0);

		if (CombatResultCst.ATTACKER_WIN.equals(combatResult) && unit.getMove() > 0)
		{
			return MoveStatusCst.MOVE_SUCCESFULL;
		} else if (CombatResultCst.ATT_AND_DEF_LOOSE.equals(combatResult)
				|| CombatResultCst.DEFENSER_WIN.equals(combatResult))
		{
			return MoveStatusCst.UNIT_DEFEATED;
		}

		return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
	}

}
