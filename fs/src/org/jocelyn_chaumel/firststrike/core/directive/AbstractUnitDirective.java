package org.jocelyn_chaumel.firststrike.core.directive;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public abstract class AbstractUnitDirective implements Serializable
{
	private final AbstractUnit _unit;

	public AbstractUnitDirective(final AbstractUnit p_unit)
	{
		Assert.preconditionNotNull(p_unit, "Unit");

		_unit = p_unit;
	}

	public final AbstractUnit getUnit()
	{
		return _unit;
	}

	public abstract MoveStatusCst play() throws PathNotFoundException;

	@Override
	public String toString()
	{
		return "{" + this.getClass().getSimpleName() + "}";
	}
}
