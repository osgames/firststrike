package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.directive.battleship.BattleshipAttackDirective;
import org.jocelyn_chaumel.firststrike.core.directive.destroyer.DestroyerAttackDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class CombatBoatKamikazeDirective extends AbstractUnitDirective
{
	private static final FSLogger _logger = FSLoggerManager.getLogger(CombatBoatKamikazeDirective.class.getName());

	public CombatBoatKamikazeDirective(final AbstractUnit p_unit)
	{
		super(p_unit);
		Assert.precondition((p_unit instanceof Battleship) || (p_unit instanceof Destroyer), "Invalid Combat Boat");
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitSubAction("Play Kamikaze");
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		final AbstractUnit combatBoat = getUnit();
		final ComputerPlayer computer = (ComputerPlayer) combatBoat.getPlayer();

		// Patrol or attack enemy
		final AbstractUnitList enemyList = computer.getEnemySet().getReacheableEnemy(combatBoat);
		if (enemyList.isEmpty())
		{
			_logger.unitDetail("No enemy detected");
			_logger.unitSubAction("Patrol Directive enabled");
			final MoveStatusCst result = new CombatBoatPatrolDirective(	combatBoat,
																		CombatBoatPatrolDirective.NO_PATROL_LIMIT)
					.play();
			if (result.equals(MoveStatusCst.ENEMY_DETECTED) && combatBoat.getMove() > 0)
			{
				_logger.unitSubAction("Kamikaze Directive re-enabled");
				return recursivePlay();
			}
			return result;
		} else
		{
			_logger.unitSubAction("Attack to the nearest enemy");
			final MoveStatusCst result;
			if (combatBoat instanceof Battleship)
			{
				result = new BattleshipAttackDirective((Battleship) combatBoat).play();
			} else if (combatBoat instanceof Destroyer)
			{
				result = new DestroyerAttackDirective((Destroyer) combatBoat).play();
			} else
			{
				throw new IllegalArgumentException("Unsupported Combat Boat instance");
			}

			if (combatBoat.getMove() > 0
					&& (MoveStatusCst.MOVE_SUCCESFULL.equals(result) || MoveStatusCst.ENEMY_DETECTED.equals(result)))
			{
				return recursivePlay();
			} else if (MoveStatusCst.ENEMY_DETECTED.equals(result))
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
			return result;
		}

	}
}
