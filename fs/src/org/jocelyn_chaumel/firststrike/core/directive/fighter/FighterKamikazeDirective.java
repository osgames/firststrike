package org.jocelyn_chaumel.firststrike.core.directive.fighter;

import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.AttackBestEnemyFromPositionDirective;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FighterKamikazeDirective extends AbstractUnitDirective
{

	public FighterKamikazeDirective(Fighter p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{

		final Fighter fighter = (Fighter) getUnit();

		final Coord position = fighter.getPosition();
		final int move = fighter.getMove();
		final int range = fighter.getFuel() - 1;
		Assert.check(move > 0, "This fighter cannot move anymore");

		MoveStatusCst moveStatus = null;
		if (range > 0)
		{
			moveStatus = new AttackBestEnemyFromPositionDirective(fighter, position, range).play();

		} else
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		if (moveStatus == MoveStatusCst.MOVE_SUCCESFULL)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		return moveStatus;
	}

}
