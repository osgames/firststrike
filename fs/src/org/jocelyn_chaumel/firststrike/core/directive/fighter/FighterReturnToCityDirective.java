package org.jocelyn_chaumel.firststrike.core.directive.fighter;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithCombatIfNecessaryDirective;
import org.jocelyn_chaumel.firststrike.core.directive.helper.TargetFinderHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

public class FighterReturnToCityDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(FighterReturnToCityDirective.class);

	public FighterReturnToCityDirective(Fighter p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitSubAction("Fighter returns to a city");
		final Fighter fighter = (Fighter) getUnit();
		// Check if a city is close enough of the fighter
		final ComputerPlayer computer = (ComputerPlayer) fighter.getPlayer();
		final Coord position = fighter.getPosition();
		final CoordSet allCityCoordSet = computer.getCitySet().getCoordSet()
				.getLimitedCoordSet(position, fighter.getFuel() / MoveCostFactory.AIR_MOVE_COST.getMinCost());

		// Check if the fighter is already in a city
		if (allCityCoordSet.contains(fighter.getPosition()))
		{
			_logger.unitDetail("Fighter all ready in a city : {0}", fighter.getPosition());
			return MoveStatusCst.MOVE_SUCCESFULL;
		}

		MoveStatusCst result = null;
		Coord cityCoord = TargetFinderHelper.getNearestPosition(fighter, allCityCoordSet, true, false);
		if (cityCoord == null)
		{
			_logger.unitDetail("No city found in the map");
			_logger.unitSubAction("Fighter plays kamikaze directive");
			result = new FighterKamikazeDirective(fighter).play();
			if (result == MoveStatusCst.MOVE_SUCCESFULL)
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
			return result;
		}

		_logger.unitDetail("Destination City : {0}", cityCoord);

		try
		{
			result = new FindPathAndMoveWithCombatIfNecessaryDirective(fighter, cityCoord, false).play();
		} catch (PathNotFoundException ex)
		{
			throw new FSFatalException(ex);
		}

		return result;
	}

}
