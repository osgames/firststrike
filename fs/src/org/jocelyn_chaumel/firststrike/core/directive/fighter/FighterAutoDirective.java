package org.jocelyn_chaumel.firststrike.core.directive.fighter;

import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitAutoDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

public class FighterAutoDirective extends AbstractUnitAutoDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(FighterAutoDirective.class);

	public FighterAutoDirective(final Fighter p_unit)
	{
		super(p_unit);
	}

	@Override
	protected MoveStatusCst internalPlay() throws PathNotFoundException
	{
		_logger.unitAction("Fighter Auto Directive is performing");
		final Fighter fighter = (Fighter) getUnit();
		// Fighter damaged : return to the nearest city
		if (fighter.getHitPoints() < fighter.getHitPointsCapacity()
				|| fighter.getNbAttack() < fighter.getNbAttackCapacity())
		{
			_logger.unitDetail("Fighter is damaged or not fully restored");
			_logger.unitSubAction("Fighter returns to the nearest city");
			return new FighterReturnToCityDirective(fighter).play();
		}

		// Calculate the accessible cities for this fighter
		final AbstractPlayer player = fighter.getPlayer();
		final Coord fighterCoord = fighter.getPosition();
		final CityList groupedCityList;
		final CityList cityPlayerList = player.getCitySet();
		if (cityPlayerList.isEmpty())
		{
			groupedCityList = cityPlayerList;
		} else
		{
			groupedCityList = cityPlayerList.getGroupedCityList(fighterCoord, (fighter.getFuelCapacity() / MoveCostFactory.AIR_MOVE_COST.getMinCost()) - 4);
		}

		if (groupedCityList.isEmpty())
		{
			_logger.unitSubAction("No city detected near to Fighter.  Kamikaze strategy is executed");
			return new FighterKamikazeDirective(fighter).play();
		}

		AbstractUnitList enemyList = player.getEnemySet();
		if (enemyList.isEmpty())
		{
			_logger.unitSubAction("No enemy detected. Patrol strategy is executed");
			return new FighterPatrolDirective(fighter, groupedCityList).play();
		}

		// Calculate the accessible enemies for this fighter
		final int fighterAttackRange = fighter.getMovementCapacity() - 2;
		final CoordSet attackAccessibleCoordSet = groupedCityList.getCoordSet().getExtendedCoordSet(fighterAttackRange);
		if (enemyList.hasUnitInCoordList(attackAccessibleCoordSet))
		{
			_logger.unitSubAction("Accessible enemy has been detected. Attacking stategy is executed");
			return new FighterAttackDirective(fighter, groupedCityList, attackAccessibleCoordSet).play();
		} else
		{
			_logger.unitSubAction("All detected enemies are too far. Patrol stategy is executed");
			return new FighterPatrolDirective(fighter, groupedCityList).play();
		}
	}

}
