package org.jocelyn_chaumel.firststrike.core.directive.fighter;

import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.HuntEnemyDirective;
import org.jocelyn_chaumel.firststrike.core.directive.helper.TargetFinderHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FighterAttackDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(FighterAttackDirective.class);
	private final CityList _groupedCityList;
	private final CoordSet _coveredCoordList;

	public FighterAttackDirective(final Fighter p_unit, final CityList p_groupedCityList)
	{
		this(p_unit, p_groupedCityList, p_groupedCityList.getCoordSet()
				.getExtendedCoordSet(p_unit.getMovementCapacity() - 2));
	}

	public FighterAttackDirective(	final Fighter p_unit,
									final CityList p_groupedCityList,
									final CoordSet p_coveredCoordList)
	{
		super(p_unit);

		Assert.preconditionNotNull(p_groupedCityList, "Grouped City Set");
		Assert.preconditionNotNull(p_coveredCoordList, "Covered Coord List");
		Assert.precondition(!p_groupedCityList.isEmpty(), "Empty Grouped City List");

		_groupedCityList = p_groupedCityList;
		_coveredCoordList = p_coveredCoordList;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final Fighter fighter = (Fighter) getUnit();
		final ComputerPlayer player = (ComputerPlayer) fighter.getPlayer();
		final AbstractUnitList enemySet = player.getEnemySet();
		final Coord fighterCoord = fighter.getPosition();
		final AbstractUnitList accessibleEnemySet = enemySet.getUnitByCoordList(_coveredCoordList);
		//final AbstractUnitList accessibleEnemySet = enemySet.getUnitByCoordList(_coveredCoordList).getReacheableEnemy(fighter);
		if (accessibleEnemySet.size() == 0)
		{
			_logger.unitDetail("No accessible enemy has been found");
			return new FighterReturnToCityDirective(fighter).play();
		}

		AbstractUnit enemy = TargetFinderHelper.findBestTargetInList(fighter, fighterCoord, accessibleEnemySet);
		if (enemy == null)
		{
			_logger.unitDetail("No accessible enemy has been found");
			return new FighterReturnToCityDirective(fighter).play();
		}

		MoveStatusCst moveStatus = null;
		final Coord enemyCoord = enemy.getPosition();
		final Coord nearestCityCoord = _groupedCityList.getDirectNearestCityFrom(enemyCoord).getPosition();
		final boolean isDangerous = nearestCityCoord.calculateIntDistance(enemyCoord) <= 5;
		try
		{
			_logger.unitDetail("Enemy identified: {0}", enemy);
			_logger.unitDetail(isDangerous ? "This enemy is qualified as 'dangerous'!"
					: "This enemy is qualified as not 'dangerous'.");
			moveStatus = new HuntEnemyDirective(fighter, enemy, isDangerous).play();
		} catch (PathNotFoundException ex)
		{
			_logger.unitDetail("Fighter is unable to find a path to attack the enemy {0}", enemy);
			return new FighterReturnToCityDirective(fighter).play();
		}

		if (moveStatus == MoveStatusCst.ENEMY_DETECTED && fighter.getMove() > 0)
		{
			_logger.unitDetail("Enemy detected during the attack!");
			return play();
		}

		if (moveStatus == MoveStatusCst.MOVE_SUCCESFULL
				&& fighter.getMove() > 0
				&& (fighter.getHitPoints() < fighter.getHitPointsCapacity() || fighter.getNbAttack() < fighter
						.getNbAttackCapacity()))
		{
			_logger.unitSubAction("This Fighter is damaged, it must returns to a city");
			return new FighterReturnToCityDirective(fighter).play();
		}

		return moveStatus;
	}

}
