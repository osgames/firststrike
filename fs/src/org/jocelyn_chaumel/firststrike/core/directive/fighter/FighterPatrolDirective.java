package org.jocelyn_chaumel.firststrike.core.directive.fighter;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithoutCombatDirective;
import org.jocelyn_chaumel.firststrike.core.directive.helper.TargetFinderHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FighterPatrolDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(FighterPatrolDirective.class);

	private final CityList _groupedCityList;

	public FighterPatrolDirective(final AbstractUnit p_unit, final CityList p_groupedCityList)
	{
		super(p_unit);
		Assert.preconditionNotNull(p_groupedCityList, "Grouped City List");
		Assert.precondition(!p_groupedCityList.isEmpty(), "Empty grouped city list detected");
		_groupedCityList = p_groupedCityList;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final Fighter fighter = (Fighter) getUnit();

		if (fighter.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		if (!_groupedCityList.hasCityAtPosition(fighter.getPosition()))
		{
			return new FighterReturnToCityDirective(fighter).play();
		}
		final AbstractPlayer player = fighter.getPlayer();
		final FSMapMgr mapMgr = player.getMapMgr();

		final CityList mapCityList = mapMgr.getCityList();
		final CoordSet coorSetToExclude = mapCityList.getEnemyCity(player).getCoordSet();
		final Coord destCoord = TargetFinderHelper.getRandomPatrolCoord(fighter, coorSetToExclude);
		if (destCoord == null)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		MoveStatusCst moveStatus = null;
		try
		{
			moveStatus = new FindPathAndMoveWithoutCombatDirective(fighter, destCoord, true, false).play();
		} catch (PathNotFoundException ex)
		{
			_logger.gameError("Problem during a Jet Fighter patrol", ex);
			throw new FSFatalException(ex);
		}
		if (moveStatus == MoveStatusCst.ENEMY_DETECTED)
		{
			_logger.unitDetail("Fighter switch in 'fighting mode'.");
			return new FighterAttackDirective(fighter, _groupedCityList).play();
		}

		if (moveStatus.equals(MoveStatusCst.MOVE_SUCCESFULL) && fighter.getMove() > 0)
		{
			_logger.unitSubAction("Fighter is returning to a city");
			return new FighterReturnToCityDirective(fighter).play();
		}

		return moveStatus;
	}

}
