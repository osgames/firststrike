package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.directive.helper.TargetFinderHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class MoveWithCombatDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(MoveWithCombatDirective.class);

	public MoveWithCombatDirective(final AbstractUnit p_unit)
	{
		super(p_unit);
	}

	/**
	 * Finds a path and move the unit through this path. Any enemy detected will
	 * be attacked.
	 * 
	 * @param p_destination
	 * @param p_range
	 * @param p_removeDestinationFromPath
	 * @return
	 * @throws PathNotFoundException
	 */
	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final AbstractUnit unit = getUnit();
		Assert.precondition(unit.hasPath(), "No mandatory path set to unit");

		if (unit.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		final AbstractPlayer player = unit.getPlayer();
		final Path path = unit.getPath();
		_logger.unitDetail("Unit is moving with combat to {0}", path);
		MoveStatusCst moveStatus = MoveStatusCst.MOVE_SUCCESFULL;
		boolean isArrived = false;
		AbstractUnitList enemyList = null;
		AbstractUnit enemy = null;
		do
		{
			enemyList = player.getEnemySet().getUnitAt(path.getNextStepWithoutConsume());
			if (!enemyList.isEmpty())
			{
				enemy = TargetFinderHelper.findBestTargetInList(unit, unit.getPosition(), enemyList);
				moveStatus = new HuntEnemyDirective(unit, enemy, true).play();
				if (MoveStatusCst.UNIT_DEFEATED.equals(moveStatus))
				{
					return moveStatus;
				} else if (unit.getMove() == 0)
				{
					return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
				} else
				{
					continue;
				}
			}

			if (moveStatus == MoveStatusCst.MOVE_SUCCESFULL && unit.getMove() > 0 && !path.isEmpty())
			{
				moveStatus = player.moveUnit(unit);
			}

			isArrived = !path.hasNextStep();

			if (!isArrived && moveStatus == MoveStatusCst.MOVE_SUCCESFULL && unit.getMove() == 0)
			{
				_logger.unitDetail("Move interrupted because not enough move pts at " + unit.getPosition());
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}

		} while (!isArrived && moveStatus == MoveStatusCst.MOVE_SUCCESFULL);

		if (isArrived)
		{
			_logger.unitDetail("Unit arrived to destination : " + unit.getPosition());
			if (moveStatus == MoveStatusCst.ENEMY_DETECTED)
			{
				return MoveStatusCst.MOVE_SUCCESFULL;
			}
		} else if (moveStatus == MoveStatusCst.ENEMY_DETECTED)
		{
			if (unit.getMove() == 0)
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
		}

		return moveStatus;
	}
}
