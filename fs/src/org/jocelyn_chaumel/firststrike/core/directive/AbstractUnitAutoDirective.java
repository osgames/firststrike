package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;

public abstract class AbstractUnitAutoDirective extends AbstractUnitDirective
{
	private static final FSLogger _logger = FSLoggerManager.getLogger(AbstractUnitAutoDirective.class);

	public AbstractUnitAutoDirective(final AbstractUnit p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitAction("Play Unit Auto Directive: " + getUnit());
		final MoveStatusCst moveStatusCst = internalPlay();
		if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatusCst))
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		return moveStatusCst;
	}

	protected abstract MoveStatusCst internalPlay() throws PathNotFoundException;
}
