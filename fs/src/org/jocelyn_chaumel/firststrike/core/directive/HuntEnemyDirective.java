package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.directive.helper.MoveHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * The unit is getting closer to the enemy and attacks him.
 * 
 * @author Joss
 * 
 */
public class HuntEnemyDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(HuntEnemyDirective.class);

	private final AbstractUnit _enemy;
	private final boolean _useAllAttackPts;

	/**
	 * Move, if necessary, and attack an enemy. If a new enemy is detected
	 * during the move, the operation is interrupted.
	 * 
	 * @param p_unit
	 * @param p_enemy
	 * @param p_useAllAttackPts
	 */
	public HuntEnemyDirective(final AbstractUnit p_unit, final AbstractUnit p_enemy, final boolean p_useAllAttackPts)
	{
		super(p_unit);
		Assert.preconditionNotNull(p_enemy, "Enemy Unit");

		_enemy = p_enemy;
		_useAllAttackPts = p_useAllAttackPts;
	}

	/**
	 * Move, if necessary, and attack an enemy. If a new enemy is detected
	 * during the move, the operation is interrupted.
	 * 
	 */
	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		AbstractUnit unit = getUnit();
		Assert.check(unit.getMove() > 0, "No move available");
		Assert.check(!unit.getPlayer().equals(_enemy.getPlayer()), "Enemy param must be an enemy unit");

		if (!_useAllAttackPts && unit.getNbAttack() == 1)
		{
			return MoveStatusCst.MOVE_SUCCESFULL;
		}
		MoveStatusCst moveStatus = MoveStatusCst.MOVE_SUCCESFULL;

		// Check if a move is required
		final Coord enemyPosition = _enemy.getPosition();
		int distance = enemyPosition.calculateIntDistance(unit.getPosition());
		if (distance > 1)
		{
			Path path;
			try {
				path = MoveHelper.getPathWithoutCombat(enemyPosition, unit, true);
			} catch (PathNotFoundException ex)
			{
				_logger.unitSubAction("Target unit not reachable.  Path with combat required.");
				path = MoveHelper.getPathWithCombat(enemyPosition, unit, true);
				_logger.unitDetail(path.toString());
			}
			unit.setPath(path);
			_logger.unitSubAction("Unit moves closer to the enemy {0} : {1}", new Object[] { _enemy.getPosition(), path});
			moveStatus = new MoveDirective(unit).play();
			if (!MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
			{
				_logger.unitDetail("The move stops at " + unit.getPosition());
				return moveStatus;
			}
			_logger.unitDetail("Enemy reached successfully. New position : {0}", unit.getPosition());
		}

		if (unit.getMove() == 0)
		{
			_logger.unitDetail("Not enough move pts to continue");
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		Assert.check(1 == enemyPosition.calculateIntDistance(unit.getPosition()), "Enemy too far");

		// Attack the enemy
		moveStatus = new CombatEnemyDirective(unit, _enemy, _useAllAttackPts).play();
		if (MoveStatusCst.UNIT_DEFEATED.equals(moveStatus))
		{
			return MoveStatusCst.UNIT_DEFEATED;
		}
		if (unit.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		return moveStatus;
	}

}
