package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.directive.helper.MoveHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FindPathAndMoveWithoutCombatDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(FindPathAndMoveWithoutCombatDirective.class);

	private final Coord _destination;
	private final boolean _removeDestinationFromPath;
	private final boolean _enemyDetectionAlert;

	public FindPathAndMoveWithoutCombatDirective(	final AbstractUnit p_unit,
													final Coord p_destination,
													final boolean p_enemyDetectionAlert,
													final boolean p_removeDestinationFromPath)
	{
		super(p_unit);

		Assert.preconditionNotNull(p_destination, "Destination");

		_destination = p_destination;
		_removeDestinationFromPath = p_removeDestinationFromPath;
		_enemyDetectionAlert = p_enemyDetectionAlert;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final AbstractUnit unit = getUnit();
		final Path path = MoveHelper.getPathWithoutCombat(_destination, unit, _removeDestinationFromPath);

		unit.setPath(path);
		_logger.unitSubAction("Unit is moving to : {0}", path);

		final MoveStatusCst moveStatus = new MoveDirective(unit).play();
		if (MoveStatusCst.ENEMY_DETECTED.equals(moveStatus))
		{
			if (_enemyDetectionAlert)
			{
				return MoveStatusCst.ENEMY_DETECTED;
			} else if (unit.getMove() > 0 && unit.hasPath())
			{
				return new FindPathAndMoveWithoutCombatDirective(	unit,
																	_destination,
																	_enemyDetectionAlert,
																	_removeDestinationFromPath).play();
			} else if (unit.hasPath())
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			} else
			{
				return MoveStatusCst.MOVE_SUCCESFULL;
			}
		}

		if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
		{
			_logger.unitDetail("Position successfully reached: " + unit.getPosition());
		} else if (MoveStatusCst.NOT_ENOUGH_MOVE_PTS.equals(moveStatus))
		{
			_logger.unitDetail("Unit stop (move == 0) here: " + unit.getPosition());
		}
		return moveStatus;
	}

}
