package org.jocelyn_chaumel.firststrike.core.directive.battleship;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.HuntEnemyDirective;
import org.jocelyn_chaumel.firststrike.core.directive.helper.TargetFinderHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class BattleshipAttackDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(BattleshipAttackDirective.class);

	/**
	 * Start the directive in selecting the first target into the proposed enemy
	 * list.
	 * 
	 * @param p_unit The destroyer.
	 * @param p_enemyList The proposed enemy list.
	 */
	public BattleshipAttackDirective(final Battleship p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitSubAction("Battleship is attacking enemies...");
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{

		final Battleship battleship = (Battleship) getUnit();
		final AbstractPlayer player = battleship.getPlayer();
		AbstractUnitList enemyList = player.getEnemySet();
		final Coord startPosition = battleship.getPosition();
		enemyList = enemyList.getUnitInRange(startPosition, battleship.getMovementCapacity());
		enemyList = enemyList.retrieveAccessibleUnitBySea(player.getMapMgr().getMap());

		if (!enemyList.isEmpty())
		{
			final AbstractUnit enemy = TargetFinderHelper.findBestTargetInList(battleship, startPosition, enemyList);
			if (enemy != null)
			{
				return attack(enemy);
			}
			_logger.unitDetail("No more enemy in short range");
		}

		_logger.unitDetail("Attacking phasis completed");
		return MoveStatusCst.MOVE_SUCCESFULL;
	}

	private MoveStatusCst attack(final AbstractUnit p_enemy) throws PathNotFoundException
	{
		final Battleship battleship = (Battleship) getUnit();
		MoveStatusCst moveStatus = null;
		try
		{
			_logger.unitDetail("Enemy selected: {0}", p_enemy);
			moveStatus = new HuntEnemyDirective(battleship, p_enemy, true).play();
		} catch (PathNotFoundException ex)
		{
			throw new FSFatalException("Unexpected exception occurs", ex);
		}

		if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
		{
			if (battleship.getMove() > 0)
			{
				_logger.unitSubAction("Attack directive is relaunched");
				return recursivePlay();
			} else
			{
				_logger.unitSubAction("Attack directive is interrupted (no more move pts)");
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
		}

		return moveStatus;
	}

}
