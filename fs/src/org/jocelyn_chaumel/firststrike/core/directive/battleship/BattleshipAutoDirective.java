package org.jocelyn_chaumel.firststrike.core.directive.battleship;

import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.CombatBoatPatrolDirective;
import org.jocelyn_chaumel.firststrike.core.directive.CombatBoatReturnToCityDirective;
import org.jocelyn_chaumel.firststrike.core.directive.destroyer.DestroyerAutoDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;

public class BattleshipAutoDirective extends AbstractUnitAutoDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(DestroyerAutoDirective.class);

	public BattleshipAutoDirective(final Battleship p_battleship)
	{
		super(p_battleship);
	}

	@Override
	protected MoveStatusCst internalPlay() throws PathNotFoundException
	{
		_logger.unitAction("Play Auto Directive: " + getUnit());
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		final Battleship battleship = (Battleship) getUnit();
		if (battleship.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		MoveStatusCst result = null;
		if (battleship.isDamaged())
		{
			_logger.unitDetail("Battleship is damaged.");

			result = new CombatBoatReturnToCityDirective(battleship).play();
			if (MoveStatusCst.MOVE_SUCCESFULL.equals(result))
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
			return result;
		}

		result = new BattleshipAttackDirective(battleship).play();
		if (MoveStatusCst.ENEMY_DETECTED.equals(result))
		{
			return recursivePlay();
		} else if (!MoveStatusCst.MOVE_SUCCESFULL.equals(result))
		{
			return result;
		}

		result = new BattleshipBombardmentDirective(battleship).play();
		if (!MoveStatusCst.MOVE_SUCCESFULL.equals(result))
		{
			return result;
		}

		result = new CombatBoatPatrolDirective(battleship, CombatBoatPatrolDirective.NO_PATROL_LIMIT).play();

		if (MoveStatusCst.MOVE_SUCCESFULL.equals(result) || MoveStatusCst.ENEMY_DETECTED.equals(result))
		{
			return recursivePlay();
		}

		return result;
	}
}
