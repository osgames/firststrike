package org.jocelyn_chaumel.firststrike.core.directive.battleship;

import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class BattleshipBombardmentDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(BattleshipBombardmentDirective.class);

	/**
	 * Search for a target location and bombard it if any.
	 * 
	 * @param p_unit The battleship.
	 */
	public BattleshipBombardmentDirective(final Battleship p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitSubAction("Battleship bombarding enemies...");
		return recursivePlay();
	}

	
	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		final Battleship battleship = (Battleship) getUnit();
		if (battleship.getMove() != battleship.getMovementCapacity())
		{
			_logger.unitDetail("Not enough move point for bombardment...");
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		
		final ComputerPlayer player = (ComputerPlayer) battleship.getPlayer();

		final Coord bombardmentPosition = player.getBestCoordForBombardment(battleship.getPosition(), battleship.getBombardmentRange());
		if (bombardmentPosition != null)
		{
			battleship.bombPosition(bombardmentPosition);
			_logger.unitDetail("Bomb at " + bombardmentPosition);
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		return MoveStatusCst.MOVE_SUCCESFULL;
	}

}
