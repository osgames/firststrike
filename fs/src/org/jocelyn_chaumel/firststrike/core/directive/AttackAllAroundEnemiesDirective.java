package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class AttackAllAroundEnemiesDirective extends AbstractUnitDirective
{

	private final static FSLogger _logger = FSLoggerManager.getLogger(AttackAllAroundEnemiesDirective.class.getName());

	public AttackAllAroundEnemiesDirective(final AbstractUnit p_unit)
	{
		super(p_unit);
		Assert.precondition(p_unit.getNbAttackCapacity() > 0, "Unsupported unit type");

	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		_logger.unitSubAction("Unit attacking all enemies around him");
		
		final AbstractUnit unit = getUnit();
		final AbstractPlayer currentPlayer = unit.getPlayer();

		if (unit.getMove() == 0 || unit.getNbAttack() == 0)
		{
			_logger.unitDetail("Attacking action stopped because not enough move pts");
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		final AbstractUnitList enemyList = currentPlayer.getEnemySet().getUnitInRange(unit.getPosition(), 1);
		if (enemyList.size() == 0)
		{
			_logger.unitDetail("No enemy found around the actual position (distance == 0)");
			return MoveStatusCst.MOVE_SUCCESFULL;

		}
		
		final AbstractUnit enemyUnit = unit.chooseBestEnemyTargetFromList(enemyList);
		_logger.unitDetail("Enemy selected:", enemyUnit);
		final MoveStatusCst moveStatus = new CombatEnemyDirective(unit, enemyUnit, true).play();
		if (MoveStatusCst.UNIT_DEFEATED.equals(moveStatus))
		{
			return MoveStatusCst.UNIT_DEFEATED;
		}


		if (unit.getHitPointsCapacity() / unit.getHitPoints() > 2)
		{
			_logger.unitDetail("No more combat because unit is too damaged : " + unit.getHitPoints() + "/"
					+ unit.getHitPointsCapacity());
			return MoveStatusCst.MOVE_SUCCESFULL;
		}

		return recursivePlay();
	}

}
