package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitAutoDirective;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TankAutoDirective extends AbstractUnitAutoDirective
{
	public TankAutoDirective(final ComputerTank p_tank)
	{
		super(p_tank);
	}

	public TankAutoDirective(final ComputerTank p_tank, final City p_city)
	{
		super(p_tank);
		Assert.preconditionNotNull(p_city, "City");

		ComputerPlayer player = (ComputerPlayer) p_tank.getPlayer();

		if (player.getUnitList().getTankProtectingCity(p_city).size() < player.getNbProtectorRequired(p_city))
		{
			p_tank.setProtectedCity(p_city);
		}
	}

	@Override
	protected MoveStatusCst internalPlay() throws PathNotFoundException
	{
		final ComputerTank tank = (ComputerTank) getUnit();
		if (tank.isLoaded())
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		MoveStatusCst moveStatus = null;
		// Recalculate, each time, the sub action list according to the new
		// situation/configuration.
		try
		{
			if (tank.isProtectingCity())
			{
				moveStatus = new TankProtectCityDirective(tank).play();
				if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
				{
					return play();
				}
				return moveStatus;
			}
			
			moveStatus = new TankAttackCityDirective(tank).play();
			if (!moveStatus.equals(MoveStatusCst.MOVE_SUCCESFULL))
			{
				return moveStatus;
			}
			if (tank.getMove() == 0)
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
			ComputerPlayer player = (ComputerPlayer) tank.getPlayer();
			if (player.getAreaToConquer().size() > 0){
				return new TankWaitingForLoadingDirective(tank).play();
			} else {
				return new TankProtectAreaDirective(tank).play();
			}
		} catch (PathNotFoundException ex)
		{
			throw new FSFatalException(ex);
		}
	}
}
