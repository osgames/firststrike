package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.HuntEnemyDirective;
import org.jocelyn_chaumel.firststrike.core.directive.MoveWithCombatDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

public class TankOneShotAttackDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TankOneShotAttackDirective.class);

	public TankOneShotAttackDirective(final Tank p_tank)
	{
		super(p_tank);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitAction("Tank one shot attack");
		final Tank tank = (Tank) getUnit();
		final Coord currentPosition = tank.getPosition();
		if (tank.getMove() != tank.getMovementCapacity())
		{
			_logger.unitDetail("Not enough move pts");
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		final AbstractPlayer player = tank.getPlayer();
		final AbstractUnitList enemyList = player.getEnemySet().getUnitInRange(currentPosition, 2);
		final FSMapMgr mapMgr = player.getMapMgr();

		if (enemyList == null && enemyList.isEmpty())
		{
			_logger.unitDetail("No enemy found");
			return MoveStatusCst.MOVE_SUCCESFULL;
		}
		final CoordSet coordToExclude = enemyList.getCoordSet();
		Path currentPath;

		while (true)
		{
			if (enemyList.isEmpty())
			{
				_logger.unitDetail("No enemy found");
				return MoveStatusCst.MOVE_SUCCESFULL;
			}
			AbstractUnit enemy = tank.chooseBestEnemyTargetFromList(enemyList);
			if (enemy == null)
			{
				_logger.unitDetail("No enemy found");
				return MoveStatusCst.MOVE_SUCCESFULL;
			}
			
			try
			{
				currentPath = mapMgr.getPath(enemy.getPosition(), coordToExclude, tank, true);
			} catch (PathNotFoundException ex)
			{
				_logger.unitDetail("Enemy not accessible: " + enemy);
				enemyList.remove(enemy);
				continue;
			}

			if (mapMgr.getPathDistance(currentPath, tank.getMoveCost()) >= 34)
			{
				_logger.unitDetail("Enemy too far: " + enemy);
				enemyList.remove(enemy);
				continue;
			}

			_logger.unitDetail("Best enemy Identified: " + enemy);
			new HuntEnemyDirective(tank, enemy, false).play();
			if (!tank.isLiving())
			{
				return MoveStatusCst.UNIT_DEFEATED;
			}

			if (tank.getPosition() != currentPosition)
			{
				_logger.unitSubAction("Return to previous position: " + currentPosition);
				try
				{
					currentPath = mapMgr.getPath(currentPosition, player.getEnemySet().getCoordSet(), tank, false);
				} catch (PathNotFoundException ex)
				{
					throw new FSFatalException(ex);
				}
				tank.setPath(currentPath);
				return new MoveWithCombatDirective(tank).play();
			} else {
				return MoveStatusCst.MOVE_SUCCESFULL;
			}

		}
	}

}
