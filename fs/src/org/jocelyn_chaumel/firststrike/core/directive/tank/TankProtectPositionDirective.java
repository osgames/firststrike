package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.AttackAllAroundEnemiesDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithCombatDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TankProtectPositionDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TankProtectPositionDirective.class);

	private final Coord _position;

	public TankProtectPositionDirective(final AbstractUnit p_unit, final Coord p_position)
	{
		super(p_unit);
		Assert.preconditionNotNull(p_position, "Position");

		_position = p_position;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitAction("Tank protects this position: " + _position);
		final MoveStatusCst result = recursivePlay();
		if (MoveStatusCst.MOVE_SUCCESFULL.equals(result))
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		return result;
	}

	private MoveStatusCst recursivePlay()
	{
		final Tank tank = (Tank) getUnit();
		final Coord position = tank.getPosition();
		if (tank.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		try
		{
			if (!_position.equals(position))
			{
				_logger.unitSubAction("Tank returns to position");
				final MoveStatusCst result = new FindPathAndMoveWithCombatDirective(tank, _position, false).play();
				if (MoveStatusCst.MOVE_SUCCESFULL.equals(result))
				{
					return recursivePlay();
				}
				return result;
			}

			MoveStatusCst result = new AttackAllAroundEnemiesDirective(tank).play();
			if (!MoveStatusCst.MOVE_SUCCESFULL.equals(result))
			{
				return result;
			}

			if (tank.getMove() != tank.getMovementCapacity())
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}

			final int move = tank.getMove();
			result = new TankOneShotAttackDirective(tank).play();
			if (MoveStatusCst.MOVE_SUCCESFULL.equals(result))
			{
				if (move != tank.getMove())
				{
					return recursivePlay();
				}
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
			return result;

		} catch (PathNotFoundException ex)
		{
			throw new FSFatalException("Invalid result detected", ex);
		}
	}

}
