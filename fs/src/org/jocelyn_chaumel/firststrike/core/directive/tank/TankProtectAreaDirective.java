package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.HuntEnemyDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

public class TankProtectAreaDirective extends AbstractUnitDirective {

	private final static FSLogger _logger = FSLoggerManager.getLogger(TankProtectAreaDirective.class);
	
	public TankProtectAreaDirective(ComputerTank p_tank) {
		super(p_tank);
	}

	/**
	 * Attaque les unit�s ennemies � porter ou ne fait rien si pas d'ennemi.
	 * 
	 * @author Jocelyn
	 *
	 */
	@Override
	public MoveStatusCst play() throws PathNotFoundException {
		final ComputerTank tank = (ComputerTank) this.getUnit();
		
		_logger.playerSubAction("Protect Area ");
		
		if (tank.isDamaged())
		{
			_logger.unitDetail("Tank is damaged");
			return new TankReturnCityDirective(tank).play();
		}
		
		final FSMapMgr mapMgr = tank.getPlayer().getMapMgr();
		final Area groundArea = mapMgr.getAreaAt(tank.getPosition(), AreaTypeCst.GROUND_AREA);
		final CoordSet coordSet = new CoordSet();
		coordSet.addAll(groundArea.getExtractEdgeCoordSet());
		coordSet.addAll(groundArea.getCellSet().getCoordSet());
		final AbstractUnitList enemyList = tank.getPlayer().getEnemySet().getUnitByCoordList(coordSet).getReacheableEnemy(tank);
		
		if (enemyList.isEmpty())
		{
			_logger.unitDetail("No enemy found");
			return MoveStatusCst.MOVE_SUCCESFULL;
		} 
		
		MoveStatusCst result;
		AbstractUnit enemy;
		while (!enemyList.isEmpty())
		{
			enemy = tank.chooseBestEnemyTargetFromList(enemyList);
			result = new HuntEnemyDirective(tank, enemy, true).play();
			if (MoveStatusCst.MOVE_SUCCESFULL != result)
			{
				return result;
			}
			if (tank.isDamaged()){
				return new TankReturnCityDirective(tank).play();
			}
			enemyList.remove(enemy);
		}
		return MoveStatusCst.MOVE_SUCCESFULL;
	}

}
