package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TankUnloadDirective extends AbstractUnitDirective
{
	private static final FSLogger _logger = FSLoggerManager.getLogger(TankUnloadDirective.class);

	private final Coord _unloadCoord;

	public TankUnloadDirective(final Tank p_tank, final Coord p_unloadCoord)
	{
		super(p_tank);
		Assert.preconditionNotNull(p_unloadCoord, "Unloading Coord");
		Assert.precondition(p_tank.getPlayer().getMapMgr().getMap().getCellAt(p_unloadCoord).isGroundCell(),
							"Invalid unloading coord");
		Assert.precondition(p_tank.isLoaded(), "This tank is not loaded");
		Assert.precondition(((TransportShip)p_tank.getContainer()).getPosition().calculateIntDistance(p_tank.getPosition()) <= 1, "Invalid unload coord");
		_unloadCoord = p_unloadCoord;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitAction("Tank is unloading " + getUnit() + " at " + _unloadCoord);
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		final Tank tank = (Tank) getUnit();
		final ComputerPlayer computer = (ComputerPlayer) tank.getPlayer();

		AbstractUnitList enemyList = computer.getEnemySet().getUnitAt(_unloadCoord);
		if (enemyList.size() > 0)
		{
			_logger.unitDetail("Unloading aborded : enemy detected on target position");
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		// Est-ce que le tank possede assez de pts de mouvement pour d�barquer
		if (tank.getMove() != tank.getMovementCapacity())
		{
			_logger.unitDetail("Unloading aborded : Tank has not enough moving point");
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		// Introduce the unit on the map & unload tank from the
		// transport ship
		return computer.unloadUnit(tank, _unloadCoord);

	}
}
