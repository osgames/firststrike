package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TankProtectArtilleryDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TankProtectCityDirective.class);
	private final short _protectionRange;

	/**
	 * Cette classe peut facilement �tre modifi�e pour �tre plus g�n�rique.
	 * 
	 * @param p_tank
	 */
	public TankProtectArtilleryDirective(final ComputerTank p_tank)
	{
		super(p_tank);
		_protectionRange = 3;
		_logger.unitSubAction("Protect Artillery (protection range:"+ _protectionRange + ")");
	}

	/**
	 * Si le tank est s�rieusement endomag�, il retournera � la ville la plus
	 * proche et ne reviendra plus prot�ger les unites dans le secteur dans
	 * lequel il se situe.
	 */
	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final ComputerTank tank = (ComputerTank) getUnit();
		if (tank.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		
		if (tank.isDamaged())
		{
			_logger.unitDetail("Tank is damaged");
			return new TankReturnCityDirective(tank).play();
		}

		final ComputerPlayer player = (ComputerPlayer) tank.getPlayer();
		final AbstractUnitList artilleryList = player.getUnitList().getUnitInRange(tank.getPosition(), _protectionRange).getUnitByType(UnitTypeCst.ARTILLERY);
		final Artillery artilleryToProtect = (Artillery) artilleryList.getNearestUnit(tank, player.getMapMgr());

		if (artilleryToProtect ==  null)
		{
			_logger.unitDetail("Protect Artillery : no more artillery to protect.");
			return MoveStatusCst.MOVE_SUCCESFULL;
		}

		MoveStatusCst moveResult = null;
		try
		{
			final Coord coordToProtect;
			int distance = artilleryToProtect.getPosition().calculateIntDistance(tank.getPosition());
			if (distance <= 1)
			{
				// Rester � c�t� de l'unit� � prot�ger c'est encore mieux que d'�tre dans la m�me cellule
				coordToProtect = tank.getPosition();
			} else {
				coordToProtect = artilleryToProtect.getPosition();
			}
			moveResult = new TankProtectPositionDirective(tank, coordToProtect).play();
		} catch (PathNotFoundException ex)
		{
			throw new FSFatalException("Unexpected Exception catched here", ex);
		}

		if (MoveStatusCst.UNIT_DEFEATED.equals(moveResult))
		{
			return MoveStatusCst.UNIT_DEFEATED;
		}

		if (!MoveStatusCst.MOVE_SUCCESFULL.equals(moveResult))
		{
			return moveResult;
		}

		if (tank.getMove() != tank.getMovementCapacity())
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		
		return moveResult;
	}
}
