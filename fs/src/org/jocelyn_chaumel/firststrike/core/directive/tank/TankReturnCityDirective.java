package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithCombatDirective;
import org.jocelyn_chaumel.firststrike.core.directive.helper.TargetFinderHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TankReturnCityDirective extends AbstractUnitDirective {

	public TankReturnCityDirective(final Tank p_tank) {
		super(p_tank);
	}

	private final static FSLogger _logger = FSLoggerManager.getLogger(TankReturnCityDirective.class);
	
	@Override
	public MoveStatusCst play() throws PathNotFoundException {
		final Tank tank = (Tank) getUnit();
		final AbstractPlayer player = tank.getPlayer();
		final Coord tankPosition = tank.getPosition();
		final FSMapMgr mapMgr = player.getMapMgr();
		final CityList playerCityList = player.getCitySet().getCityByArea(mapMgr.getAreaAt(	tankPosition,
																							AreaTypeCst.GROUND_AREA));
		if (playerCityList.hasCityAtPosition(tankPosition))
		{
			return MoveStatusCst.MOVE_SUCCESFULL;
		}

		final Coord cityCoord = TargetFinderHelper.getNearestPosition(	tank,
																		playerCityList.getCoordSet(),
																		false,
																		false);
		Assert.check(cityCoord != null, "Invalid state detected: No city found");

		_logger.unitSubAction("Tank return to city: " + cityCoord);
		return new FindPathAndMoveWithCombatDirective(tank, cityCoord, false).play();
	}
	
}
