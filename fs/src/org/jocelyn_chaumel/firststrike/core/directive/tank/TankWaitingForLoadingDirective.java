package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaStatusCst;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.AttackBestEnemyFromPositionDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithCombatDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TankWaitingForLoadingDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TankWaitingForLoadingDirective.class);
	private final Area _area;
	private final ComputerPlayer _player;

	public TankWaitingForLoadingDirective(final Tank p_tank)
	{
		super(p_tank);
		_player = (ComputerPlayer) p_tank.getPlayer();
		_area = _player.getMapMgr().getAreaAt(p_tank.getPosition(), AreaTypeCst.GROUND_AREA);
	}

	/**
	 * Move the tank to the loading coordinates, in fighting all detected
	 * enemies during the move.
	 */
	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitSubAction("The tank conquer the world");

		final Tank tank = (Tank) getUnit();
		final AreaStatusCst areaStatus = _player.getAreaStatus(_area);
		Assert.precondition(areaStatus.equals(AreaStatusCst.OWNED_AREA), "Invalid Area Status");

		final Coord loadingCoord = _player.getLoadingCoordOf(_area);

		final Coord tankPosition = tank.getPosition();
		MoveStatusCst moveStatus = null;
		if (!loadingCoord.equals(tankPosition))
		{
			_logger.unitSubAction("This unit get closer to the loading position");

			moveStatus = new FindPathAndMoveWithCombatDirective(tank, loadingCoord, false).play();
			if (!MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
			{
				return moveStatus;
			}
		}

		if (tank.getMove() > 0)
		{
			_logger.unitSubAction("This unit track an enemy around its position");
			moveStatus = new AttackBestEnemyFromPositionDirective(tank, loadingCoord, 2).play();
			if (moveStatus == MoveStatusCst.MOVE_SUCCESFULL)
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
		}

		return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
	}
}
