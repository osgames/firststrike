package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaStatusCst;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithCombatDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;

public class TankAttackCityDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TankAttackCityDirective.class);

	public TankAttackCityDirective(final ComputerTank p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final ComputerTank tank = (ComputerTank) getUnit();
		
		final ComputerPlayer player = (ComputerPlayer) tank.getPlayer();
		final FSMapMgr mapMgr = player.getMapMgr();
		final Area area = mapMgr.getAreaAt(tank.getPosition(), AreaTypeCst.GROUND_AREA);
		final AreaStatusCst areaStatus = player.getAreaStatus(area);
		if (!areaStatus.equals(AreaStatusCst.TO_CONQUER_AREA) && !areaStatus.equals(AreaStatusCst.UNDER_CONTROL_AREA))
		{
			return MoveStatusCst.MOVE_SUCCESFULL;
		}
		
		
		City targetCity = tank.getTargetCity().getCity();
		
		_logger.unitSubAction("Attack City : " + targetCity);
		final MoveStatusCst result = new FindPathAndMoveWithCombatDirective(tank, targetCity.getPosition(), false).play();
		if (MoveStatusCst.MOVE_SUCCESFULL.equals(result))
		{
			_logger.unitDetail("Attack City : done!" + targetCity);
			return play();
		}

		return result;
		
	}

}
