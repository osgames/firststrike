package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TankProtectCityDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TankProtectCityDirective.class);

	public TankProtectCityDirective(final ComputerTank p_tank)
	{
		super(p_tank);
		Assert.precondition(p_tank.isProtectingCity(), "This tank doesn't protect any city");
		_logger.unitSubAction("Protect City : " + p_tank.getProtectedCity());
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final ComputerTank tank = (ComputerTank) getUnit();
		if (tank.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		final ComputerPlayer player = (ComputerPlayer) tank.getPlayer();
		final City city = tank.getProtectedCity();

		if (player.getUnitList().getTankProtectingCity(city).size() > player.getNbProtectorRequired(city))
		{
			tank.removeProtectedCity();
			_logger.unitDetail("Protect City : no more required.");
			return MoveStatusCst.MOVE_SUCCESFULL;
		}

		MoveStatusCst moveResult = null;
		try
		{
			moveResult = new TankProtectPositionDirective(tank, city.getPosition()).play();
		} catch (PathNotFoundException ex)
		{
			throw new FSFatalException("Unexpected Exception catched here", ex);
		}

		if (MoveStatusCst.UNIT_DEFEATED.equals(moveResult))
		{
			tank.removeProtectedCity();
			return MoveStatusCst.UNIT_DEFEATED;
		}

		if (!MoveStatusCst.MOVE_SUCCESFULL.equals(moveResult))
		{
			return moveResult;
		}

		if (tank.getMove() != tank.getMovementCapacity())
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		
		return moveResult;
	}
}
