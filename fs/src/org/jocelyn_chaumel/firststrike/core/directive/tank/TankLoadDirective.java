package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.IUnitContainer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TankLoadDirective extends AbstractUnitDirective
{
	private final TransportShip _transport;
	private final static FSLogger _logger = FSLoggerManager.getLogger(TankLoadDirective.class);

	public TankLoadDirective(final Tank p_tank, final TransportShip p_transport)
	{
		super(p_tank);
		Assert.preconditionNotNull(p_transport, "Transport");
		Assert.precondition(p_transport.getPlayer() == p_tank.getPlayer(), "Invalid Player");
		_transport = p_transport;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final Tank currentTank = (Tank) getUnit();
		Assert.check(	((IUnitContainer)_transport).isLoadable(currentTank),
						"Not enough free space available into the transport ship");

		MoveStatusCst status = currentTank.getPlayer().loadUnit(currentTank, _transport);
		_logger.unitSubAction("Tank loaded : " + currentTank);
		return status;
	}

}
