package org.jocelyn_chaumel.firststrike.core.directive.area;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;

public class AreaDirective implements Serializable
{
	private final ComputerPlayer _player;
	private final static FSLogger _logger = FSLoggerManager.getLogger(AreaDirective.class);
	
	public AreaDirective (final ComputerPlayer p_player)
	{
		_player = p_player;
	}
	
	public void play() throws PathNotFoundException
	{
		
		// Play All tank
	}
}
