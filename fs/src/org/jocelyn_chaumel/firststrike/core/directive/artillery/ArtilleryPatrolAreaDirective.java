package org.jocelyn_chaumel.firststrike.core.directive.artillery;

import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithCombatDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

public class ArtilleryPatrolAreaDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(ArtilleryPatrolAreaDirective.class);

	/**
	 * Select the best position to bombard and bombard it.
	 * 
	 * @param p_unit
	 *            The artillery.
	 */
	public ArtilleryPatrolAreaDirective(final Artillery p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitSubAction("Artillery starts a patrol...");
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		final Artillery artillery = (Artillery) getUnit();
		final Coord artilleryPosition = artillery.getPosition();
		final ComputerPlayer player = (ComputerPlayer) artillery.getPlayer();
		Coord targetCoord = artillery.getPatrolCoord();
		
		if (artilleryPosition.equals(targetCoord))
		{
			targetCoord = null;
			artillery.resetPatrolCoord();
		}
		
		if (targetCoord == null)
		{
			final FSMap map =  player.getMapMgr().getMap();
			final CoordSet areaCoordSet = map.getAreaAt(artillery.getPosition(), AreaTypeCst.GROUND_AREA).getCellSet().getCoordSet();
			areaCoordSet.remove(artilleryPosition);
			final FSRandom randomizer = FSRandom.getInstance();
			targetCoord = (Coord) randomizer.getRandomObjectFromTheList(new ArrayList<Coord>(areaCoordSet));
			artillery.setPatrolCoord(targetCoord);
		}

		MoveStatusCst status = new FindPathAndMoveWithCombatDirective(artillery, targetCoord, false).play();
		artillery.markAsTurnCompleted();
		return status;
	}
}
