package org.jocelyn_chaumel.firststrike.core.directive.artillery;

import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitAutoDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class ArtilleryAutoDirective extends AbstractUnitAutoDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(ArtilleryAutoDirective.class);

	/**
	 * 1) If damaged, artillery returns to the nearest city.
	 * 2) If Enemy in the range, artillery bombards it.
	 * 3) Find best position for bombardment and move on.
	 * @param p_artillery
	 */
	public ArtilleryAutoDirective(final Artillery p_artillery)
	{
		super(p_artillery);
	}

	@Override
	protected MoveStatusCst internalPlay() throws PathNotFoundException
	{
		_logger.unitAction("Play Auto Directive: " + getUnit());
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		final Artillery artillery = (Artillery) getUnit();
		if (artillery.getMove() == 0)
		{
			artillery.markAsTurnCompleted();
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		if (artillery.isDamaged())
		{
			_logger.unitDetail("Artillery is damaged.");
			return new ArtilleryReturnToCityDirective(artillery).play();
		}

		MoveStatusCst status = new ArtilleryBombardmentDirective(artillery).play();
		if (artillery.getMove() == 0)
		{
			return status;
		}
		
		final ComputerPlayer player = (ComputerPlayer) artillery.getPlayer();
		Coord bestCoord = player.getBestCoordForBombardment(artillery.getPosition(), 10);
		// Enemy too close!
		if (null != bestCoord)
		{
			_logger.unitDetail("Enemy position to bomb identified:" + bestCoord);
			if (artillery.getMove() != artillery.getMovementCapacity())
			{
				_logger.unitDetail("... but not enough move point for the moment.");
				artillery.markAsTurnCompleted();
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
				
			}
			artillery.bombPosition(bestCoord);
			artillery.markAsTurnCompleted();
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		return new ArtilleryFindBombardmentPositionDirective(artillery).play();
	}
}
