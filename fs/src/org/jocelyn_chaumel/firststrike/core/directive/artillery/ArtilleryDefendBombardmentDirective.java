package org.jocelyn_chaumel.firststrike.core.directive.artillery;

import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithoutCombatDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class ArtilleryDefendBombardmentDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(ArtilleryAutoDirective.class);
	private final Coord _targetCoord;
	
	public ArtilleryDefendBombardmentDirective(final Artillery p_unit, final Coord p_coord)
	{
		super(p_unit);
		Assert.preconditionNotNull(p_coord, "Coord");
		_targetCoord = p_coord;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final Artillery artillery = (Artillery) getUnit();
		if (artillery.isDamaged()) 
		{
			_logger.playerSubAction("Artillery is damaged.");
			MoveStatusCst result = new ArtilleryReturnToCityDirective(artillery).play();
			if (MoveStatusCst.MOVE_SUCCESFULL.equals(result))
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
			return result;
		}
		
		if (artillery.getPosition().equals(_targetCoord))
		{
			MoveStatusCst result = new ArtilleryBombardmentDirective(artillery).play();
			if (MoveStatusCst.MOVE_SUCCESFULL.equals(result))
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
			return result;
		}
		
		MoveStatusCst result = new FindPathAndMoveWithoutCombatDirective(artillery, _targetCoord, false, false).play();
		if (MoveStatusCst.MOVE_SUCCESFULL.equals(result))
		{
			if (artillery.getMove() == artillery.getMovementCapacity())
			{
				return play();
			}
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		return result;
	}

}
