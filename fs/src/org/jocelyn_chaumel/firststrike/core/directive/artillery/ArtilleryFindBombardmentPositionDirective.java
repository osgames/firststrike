package org.jocelyn_chaumel.firststrike.core.directive.artillery;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithoutCombatDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.Bombardment;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

public class ArtilleryFindBombardmentPositionDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(ArtilleryFindBombardmentPositionDirective.class);

	/**
	 * Find and move the Artillery unit at a position enabling the Artillery to bombard the best enemy position. 
	 * 
	 * @param p_unit
	 *            The artillery unit.
	 */
	public ArtilleryFindBombardmentPositionDirective(final Artillery p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitSubAction("Artillery bombarding enemies...");
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		final Artillery artillery = (Artillery) getUnit();
		final ComputerPlayer player = (ComputerPlayer) artillery.getPlayer();

		final Area area = player.getMapMgr().getAreaAt(artillery.getPosition(), AreaTypeCst.GROUND_AREA);

		final CityList enemyCityList = area.getCityList().getEnemyCity(player);
		final CoordSet enemyCoordCityList = enemyCityList.getCoordSet();
		
		if (enemyCoordCityList.isEmpty())
		{
			_logger.unitDetail("No ennemy city found...");
			return new ArtilleryPatrolAreaDirective(artillery).play();
		}
		
		final Coord bestCityCoord = enemyCoordCityList.getDirectNearestCoord(artillery.getPosition());
		
		if (bestCityCoord.isInRange(artillery.getPosition(), artillery.getBombardmentRange())) {
			// Bombard the nearest city
			_logger.unitDetail("Artillery bombards the nearest enemy city at: " + bestCityCoord);
			CellTypeCst cellType = area.getCellSet().getCellByCoord(bestCityCoord).getCellType();
			player.addBombardment(bestCityCoord, new Bombardment(artillery.getMinBombardmentAttack(), artillery.getMaxBombardmentAttack(), artillery.getBombardmentFactor(cellType)));
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		
		// Move close to target enemy city
		new FindPathAndMoveWithoutCombatDirective(artillery, bestCityCoord, true, true).play();
		artillery.markAsTurnCompleted();
		return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
	}
}
