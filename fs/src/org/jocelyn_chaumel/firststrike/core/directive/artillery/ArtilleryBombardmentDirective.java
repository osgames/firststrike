package org.jocelyn_chaumel.firststrike.core.directive.artillery;

import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class ArtilleryBombardmentDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(ArtilleryBombardmentDirective.class);

	/**
	 * Select the best position to bombard and bombard it.
	 * 
	 * @param p_unit
	 *            The artillery.
	 */
	public ArtilleryBombardmentDirective(final Artillery p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitSubAction("Artillery bombarding enemies...");
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		
		final Artillery artillery = (Artillery) getUnit();
		
		if (artillery.getMove() != artillery.getMovementCapacity())
		{
			_logger.unitDetail("Not enough move pts to bombard something...");
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		
		final ComputerPlayer player = (ComputerPlayer) artillery.getPlayer();

		final Coord bombardmentPosition = player.getBestCoordForBombardment(artillery.getPosition(),
																			artillery.getBombardmentRange());

		if (bombardmentPosition != null)
		{
			artillery.bombPosition(bombardmentPosition);
			_logger.unitDetail("Bomb at " + bombardmentPosition);
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		_logger.unitDetail("No ennemy found.");

		return MoveStatusCst.MOVE_SUCCESFULL;
	}
}
