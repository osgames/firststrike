package org.jocelyn_chaumel.firststrike.core.directive.artillery;

import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithoutCombatDirective;
import org.jocelyn_chaumel.firststrike.core.directive.helper.TargetFinderHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class ArtilleryReturnToCityDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(ArtilleryReturnToCityDirective.class);

	public ArtilleryReturnToCityDirective(final AbstractUnit p_unit)
	{
		super(p_unit);
		Assert.precondition(p_unit instanceof Artillery, "Artillery expected");
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final Artillery artillery = (Artillery) getUnit();
		final AbstractPlayer player = artillery.getPlayer();
		final Coord artilleryPosition = artillery.getPosition();
		final FSMapMgr mapMgr = player.getMapMgr();
		final CityList playerCityList = player.getCitySet().getCityByArea(mapMgr.getAreaAt(	artilleryPosition,
																							AreaTypeCst.GROUND_AREA));
		if (playerCityList.hasCityAtPosition(artilleryPosition))
		{
			_logger.playerSubAction("Artillery already in a city.");
			if (artillery.getMove() > 0)
			{
				new ArtilleryBombardmentDirective(artillery).play();
			}
			artillery.markAsTurnCompleted();
			return MoveStatusCst.MOVE_SUCCESFULL;
		}

		final Coord cityCoord = TargetFinderHelper.getNearestPosition(	artillery,
																		playerCityList.getCoordSet(),
																		true,
																		false);

		if (cityCoord != null)
		{
			// Return to a city
			_logger.unitSubAction("Artillery return to city: " + cityCoord);
			return new FindPathAndMoveWithoutCombatDirective(artillery, cityCoord, false, false).play();
		} else
		{
			return new ArtilleryBombardmentDirective(artillery).play();
		}
	}

}
