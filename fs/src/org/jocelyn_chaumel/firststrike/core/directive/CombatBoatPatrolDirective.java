package org.jocelyn_chaumel.firststrike.core.directive;

import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class CombatBoatPatrolDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(CombatBoatPatrolDirective.class);
	public final static int NO_PATROL_LIMIT = -1;

	private final int _patrolRange;
	private final Coord _startCoord;

	public CombatBoatPatrolDirective(final AbstractUnit p_unit, final int p_patrolRange)
	{
		this(p_unit, p_unit.getPosition(), p_patrolRange);
	}

	public CombatBoatPatrolDirective(final AbstractUnit p_unit, final Coord p_startCoord, final int p_patrolRange)
	{
		super(p_unit);
		Assert.preconditionNotNull(p_startCoord, "Start Coord");
		Assert.precondition(p_patrolRange == NO_PATROL_LIMIT || p_patrolRange >= 1, "Invalid Patrol Range");
		Assert.precondition((p_unit instanceof Battleship) || (p_unit instanceof Destroyer), "Invalid Combat Boat");
		_startCoord = p_startCoord;
		_patrolRange = p_patrolRange;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final AbstractUnit destroyer = getUnit();
		Assert.precondition(destroyer.getMove() > 0, "No move point are available for this destroyer");

		final FSMapMgr mapMgr = destroyer.getPlayer().getMapMgr();
		final Coord destroyerCoord = destroyer.getPosition();
		Coord destination = null;
		if (!destroyer.hasPath())
		{
			final FSMap map = mapMgr.getMap();
			// Calculate all sea coord where a patrol can be executed
			final CoordList seaCoordList = getCoordListAvailableForPatrol(map);

			// Remove from this coord set all city coord
			final CoordSet cityCoordSet = map.getCityList().getCoordSet();
			seaCoordList.removeAll(cityCoordSet);
			seaCoordList.remove(destroyerCoord);
			final FSRandom randomizer = FSRandom.getInstance();
			destination = (Coord) randomizer.getRandomObjectFromTheList(new ArrayList<Coord>(seaCoordList));
			_logger.unitDetail("Patrol destination: {0}", destination);
		} else
		{
			destination = destroyer.getPath().getDestination();
			final CoordSet cityCoordSet = mapMgr.getCityList().getCoordSet();
			// If the destroyer is already located into a city, then do not
			// exclude the cell from valid Coord Set (seaCoordSet)
			cityCoordSet.remove(destroyerCoord);

			if (mapMgr.isAccessibleCoord(destroyer, destination, cityCoordSet, false))
			{
				_logger.unitSubAction("The patrol continue to {0}", destination);

			} else
			{
				destroyer.clearPath();
				_logger.unitSubAction("The current destination patrol is no more accessible.");
				return play();
			}
		}

		MoveStatusCst moveStatus = null;
		try
		{
			moveStatus = new FindPathAndMoveWithCombatDirective(destroyer, destination, false).play();
			if (moveStatus.equals(MoveStatusCst.MOVE_SUCCESFULL) && destroyer.getMove() > 0)
			{
				return play();
			}
		} catch (PathNotFoundException ex)
		{
			throw new FSFatalException("Erreur inattendue", ex);
		}
		return moveStatus;
	}

	private CoordList getCoordListAvailableForPatrol(final FSMap p_map)
	{
		if (_patrolRange == NO_PATROL_LIMIT)
		{
			return new CoordList(p_map.getSeaArea().getCellSet().getCoordSet());
		}

		final CoordSet coordSet = CoordSet.createInitiazedCoordSet(	_startCoord,
																	_patrolRange,
																	p_map.getWidth(),
																	p_map.getHeight());
		final CoordList coordList = new CoordList();

		for (Coord currentCoord : coordSet)
		{
			if (p_map.getCellAt(currentCoord).isSeaCell())
			{
				coordList.add(currentCoord);
			}
		}

		return coordList;
	}
}
