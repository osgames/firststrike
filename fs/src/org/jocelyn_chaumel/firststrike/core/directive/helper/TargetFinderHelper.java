/*
 * Created on Jun 30, 2005
 */
package org.jocelyn_chaumel.firststrike.core.directive.helper;

import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.AbstractMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Classe permettant � l'ordinateur d'indentifier la meilleur cible.
 * 
 * @author Jocelyn Chaumel
 */
public final class TargetFinderHelper
{
	
	// TODO AZ Faire du m�nage ici.  Voir aussi AbstractUnitList.getRecheableEnemy()...
	public final static AbstractUnit findBestTargetInRange(	final AbstractUnit p_unit,
															final Coord p_startCoord,
															final int p_range)
	{
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.preconditionNotNull(p_startCoord, "Start Coord");
		Assert.precondition(p_range > 0, "Invalid Range");

		final AbstractPlayer player = p_unit.getPlayer();
		final AbstractUnitList enemyList = player.getEnemySet().getUnitInRange(p_startCoord, p_range);
		if (enemyList.size() == 0)
		{
			return null;
		}

		final boolean isTank = p_unit.getUnitType() == UnitTypeCst.TANK;
		final CoordSet enemyCoordCitySet;

		if (isTank)
		{
			enemyCoordCitySet = new CoordSet();
		} else
		{

			enemyCoordCitySet = player.getMapMgr().getCityList().getEnemyCity(player).getCoordSet();
		}

		return findBestTargetInEnemyList(p_unit, enemyList, enemyCoordCitySet);
	}

	/**
	 * Returns the best enemy target from the enemy list.
	 * 
	 * @param p_unit Unit searching an enemy target.
	 * @param p_startCoord The best target is calculated with this start
	 *        position.
	 * @param p_enemyUnitList Enemy list
	 * @return Best Enemy.
	 */
	public final static AbstractUnit findBestTargetInList(	final AbstractUnit p_unit,
															final Coord p_startCoord,
															final AbstractUnitList p_enemyUnitList)
	{
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.preconditionNotNull(p_startCoord, "Start Coord");
		Assert.preconditionNotNull(p_enemyUnitList, "Enemy List");
		Assert.precondition(!p_enemyUnitList.isEmpty(), "Empty enemy unit list");

		final boolean isTank = p_unit.getUnitType() == UnitTypeCst.TANK;
		final CoordSet enemyCoordCitySet;

		if (isTank)
		{
			enemyCoordCitySet = new CoordSet();
		} else
		{
			final AbstractPlayer player = p_unit.getPlayer();
			enemyCoordCitySet = player.getMapMgr().getCityList().getEnemyCity(player).getCoordSet();
		}

		return findBestTargetInEnemyList(p_unit, p_enemyUnitList, enemyCoordCitySet);
	}

	/**
	 * Trouve la meilleur cible accessible pour une unit�.
	 * 
	 * @param p_unit L'unit� attaquante.
	 * @param p_enemyList La liste des ennemis potentiels
	 * @return La meilleur cible.
	 */
	private final static AbstractUnit findBestTargetInEnemyList(final AbstractUnit p_unit,
																final AbstractUnitList p_enemyList,
																final CoordSet p_enemyCoordCityList)
	{
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.preconditionNotNull(p_enemyList, "Enemy List");
		Assert.precondition(p_enemyList.size() > 0, "Empty Enemy List");
		Assert.precondition(!p_unit.getUnitType().equals(UnitTypeCst.TRANSPORT_SHIP), "Invalid Unit Type");

		// Trie les ennemis ; Index 0 : premi�re cible � d�truire, Index n :
		// derni�re cible � d�truire
		final AbstractUnit enemy = p_unit.chooseBestEnemyTargetFromList(p_enemyList);

		if (enemy == null)
		{
			return null;
		}

		final CoordSet enemyCoordSet = p_enemyList.getCoordSet();
		final Coord enemyPosition = enemy.getPosition();
		enemyCoordSet.addAll(p_enemyCoordCityList);
		// Est-ce que la cible est accessible?
		// Pour les unit�s a�riennes
		if (enemyPosition.calculateIntDistance(p_unit.getPosition()) == 1)
		{
			return enemy;
		}
		// Il faut supprimer la coord o� ce situe la cible
		// temporairement
		// pour permettre � l'algo de bien fonctionner
		enemyCoordSet.remove(enemyPosition);
		final FSMapMgr mapMgr = p_unit.getPlayer().getMapMgr();
		if (mapMgr.isAccessibleCoord(p_unit, enemyPosition, enemyCoordSet, true))
		{
			return enemy;
		}

		if (p_enemyList.size() > 1)
		{
			p_enemyList.remove(enemy);
			return findBestTargetInEnemyList(p_unit, p_enemyList, p_enemyCoordCityList);
		}

		return null;
	}

	/**
	 * Retourne la destination la plus proche pour une unit� donn�e � partir
	 * d'une liste de destinations possibles.
	 * 
	 * @param p_unit
	 * @param p_destinationCoordSet
	 * @param p_escapeEnemyUnits
	 * @param p_excludeDestinationCoord
	 * @return
	 */
	public final static Coord getNearestPosition(	final AbstractUnit p_unit,
													final CoordSet p_destinationCoordSet,
													final boolean p_escapeEnemyUnits,
													final boolean p_excludeDestinationCoord)
	{
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.preconditionNotNull(p_destinationCoordSet, "Destination Coord Set");

		if (p_destinationCoordSet.isEmpty())
		{
			return null;
		}

		final AbstractPlayer currentPlayer = p_unit.getPlayer();
		final FSMapMgr mapMgr = currentPlayer.getMapMgr();
		final CoordSet coordSetToExclude;
		if (!p_unit.getUnitType().equals(UnitTypeCst.TANK))
		{
			// Add, in coordSetToExclude, all enemy cities.
			coordSetToExclude = mapMgr.getCityList().getEnemyCity(currentPlayer).getCoordSet();
		} else
		{
			coordSetToExclude = new CoordSet();
		}

		if (p_escapeEnemyUnits)
		{
			coordSetToExclude.addAll(currentPlayer.getEnemySet().getCoordSet());
		}

		final AbstractMoveCost moveCost = p_unit.getMoveCost();
		Path currentPath;
		Coord currentCoord;
		Coord bestCoord = null;
		int bestDistance = Integer.MAX_VALUE;
		final Coord unitCoord = p_unit.getPosition();
		int currentDistance;
		final Iterator iter = p_destinationCoordSet.iterator();
		while (iter.hasNext())
		{
			currentCoord = (Coord) iter.next();
			if (p_excludeDestinationCoord)
			{
				Assert.check(	!currentCoord.equals(unitCoord),
								"Invalid destination coord set because its contains the start position");
				if (currentCoord.calculateIntDistance(unitCoord) == 1)
				{
					return currentCoord;
				}
			} else
			{
				if (currentCoord.equals(unitCoord))
				{
					return currentCoord;
				}
			}
			try
			{
				if (mapMgr.isAccessibleCoord(p_unit, currentCoord, coordSetToExclude, p_excludeDestinationCoord))
				{
					currentPath = mapMgr.getPath(currentCoord, coordSetToExclude, p_unit, p_excludeDestinationCoord);

					currentDistance = mapMgr.getPathDistance(currentPath, moveCost);
					if (currentDistance < bestDistance)
					{
						bestCoord = currentCoord;
						bestDistance = currentDistance;
					}
				}
			} catch (PathNotFoundException ex)
			{
				// NOP
			}
		}
		return bestCoord;
	}

	/**
	 * Find the shortness "path" between transport ship's position & the area to
	 * conquer.
	 * 
	 * @param p_transport Transport Ship containing the tank to unload.
	 * @param p_areaToConquer Area to conquer
	 * @return Best sea coord destination for an unloading operation.
	 */
	public static final Coord getBestUnloadingCoord(final TransportShip p_transport, final Area p_areaToConquer)
	{
		Assert.preconditionNotNull(p_transport, "Transport Ship");
		Assert.preconditionNotNull(p_areaToConquer, "Destination Area");

		final Coord transportCoord = p_transport.getPosition();
		final AbstractPlayer player = p_transport.getPlayer();
		final AbstractMoveCost moveCost = p_transport.getMoveCost();

		CoordSet availableUnloadingCoordSet = (CoordSet) p_areaToConquer.getGroundCellNearToSea().clone();
		final CoordSet enemyCoordSet = player.getEnemySet().getCoordSet();
		availableUnloadingCoordSet.removeAll(enemyCoordSet);
		if (availableUnloadingCoordSet.isEmpty())
		{
			availableUnloadingCoordSet = (CoordSet) p_areaToConquer.getGroundCellNearToSea().clone();
		}
		Coord bestDestination = null;
		final FSMapMgr mapMgr = player.getMapMgr();

		// For each available
		while (!availableUnloadingCoordSet.isEmpty() && bestDestination == null)
		{
			final Coord groundCoord = availableUnloadingCoordSet.getDirectNearestCoord(transportCoord);
			availableUnloadingCoordSet.remove(groundCoord);
			final CoordSet seaCoordSet = mapMgr.getWatherCoordNearToGroundCoord(groundCoord);
			seaCoordSet.removeAll(enemyCoordSet);
			Coord currentDestination = null;
			Path currentPath = null;
			int currentDist = 0;
			int bestDist = Integer.MAX_VALUE;

			final Iterator destinationIter = seaCoordSet.iterator();
			while (destinationIter.hasNext())
			{
				currentDestination = (Coord) destinationIter.next();
				try
				{
					currentPath = MoveHelper.getPathWithoutCombat(currentDestination, p_transport, false);
				} catch (PathNotFoundException ex)
				{
					continue;
				}

				currentDist = mapMgr.getPathDistance(currentPath, moveCost);
				if (currentDist < bestDist)
				{
					bestDist = currentDist;
					bestDestination = currentDestination;
				}
			}
		}

		return bestDestination;
	}

	/**
	 * Find a random destination and validate if the destination is accessible.
	 * 
	 * @param p_fighter
	 * @param p_coorSetToExclude
	 * @return Destination Coord
	 */
	public final static Coord getRandomPatrolCoord(final Fighter p_fighter, final CoordSet p_coorSetToExclude)
	{
		Assert.preconditionNotNull(p_fighter, "Fighter");
		Assert.preconditionNotNull(p_coorSetToExclude, "Coord Set to exclude");

		Coord currentCoord = null;
		final Coord startCoord = p_fighter.getPosition();
		final int range = (p_fighter.getMove()/ MoveCostFactory.AIR_MOVE_COST.getMinCost()) - 2;
		final FSMapMgr mapMgr = p_fighter.getPlayer().getMapMgr();
		final int mapHeight = mapMgr.getMap().getHeight() - 1;
		final int mapWidth = mapMgr.getMap().getWidth() - 1;

		int nbTry = 10;
		while (nbTry-- > 0)
		{
			currentCoord = getRandomMoveCoord(startCoord, range, mapWidth, mapHeight);
			if (p_coorSetToExclude.contains(currentCoord))
			{
				continue;
			}
			if (mapMgr.isAccessibleCoord(p_fighter, currentCoord, p_coorSetToExclude, false))
			{
				return currentCoord;
			}
		}

		return null;
	}

	/**
	 * Find a destination for a movement.
	 * 
	 * @param p_startCoord
	 * @param p_range
	 * @param p_xMax
	 * @param p_yMax
	 * @return
	 */
	public static final Coord getRandomMoveCoord(	final Coord p_startCoord,
													final int p_range,
													final int p_xMax,
													final int p_yMax)
	{
		Assert.preconditionNotNull(p_startCoord, "Start Coord");
		Assert.precondition(p_range > 0, "Invalid range");
		Assert.precondition(p_xMax > 1, "Invalid xMax");
		Assert.precondition(p_yMax > 1, "Invalid yMax");

		final int x_base = p_startCoord.getX() - p_range;
		final int y_base = p_startCoord.getY() - p_range;
		int axis, x, y, signe;
		float random;
		// Find the patrol axis (horizontal or vertical)
		random = FSRandom.getInstance().getRandomFloat();
		axis = Math.round(random);
		random = FSRandom.getInstance().getRandomFloat();
		signe = Math.round(random);
		if (axis == 0)
		{
			// Vertical axis
			random = FSRandom.getInstance().getRandomFloat();
			x = Math.round(random * (p_range * 2));
			x = Math.max(0, x + x_base);
			if (signe == 0)
			{
				// Top
				y = Math.max(0, y_base);
			} else if (signe == 1)
			{
				// buttom
				y = Math.max(0, y_base + 2 * p_range);
			} else
			{
				throw new FSFatalException("Unexpected result :" + axis);
			}
		} else if (axis == 1)
		{
			// Horizontal axis
			random = FSRandom.getInstance().getRandomFloat();
			y = Math.round(random * (p_range * 2));
			y = Math.max(0, y + y_base);
			if (signe == 0)
			{
				// Left
				x = Math.max(0, x_base);
			} else if (signe == 1)
			{
				// Right
				x = Math.max(0, x_base + 2 * p_range);
			} else
			{
				throw new FSFatalException("Unexpected result :" + axis);
			}
		} else
		{
			throw new FSFatalException("Unexpected result axis :" + axis + " random :" + random);
		}
		x = Math.min(x, p_xMax);
		y = Math.min(y, p_yMax);
		Coord coord = new Coord(x, y);
		if (coord.equals(p_startCoord))
		{
			return getRandomMoveCoord(p_startCoord, p_range, p_xMax, p_yMax);
		}
		return coord;
	}

	public static City findBestCityToAttach(final Tank p_tank)
	{
		final ComputerPlayer player = (ComputerPlayer) p_tank.getPlayer();
		final FSMapMgr mapMgr = player.getMapMgr();
		final Area area = mapMgr.getAreaAt(p_tank.getPosition(), AreaTypeCst.GROUND_AREA);
		final CityList cityList = area.getCityList().getCityToConquer(player);

		int bestDistance = Integer.MAX_VALUE;
		City bestCity = null;
		int currentDistance;
		Path path;
		for (City city : cityList)
		{
			try
			{
				path = mapMgr.getPath(city.getPosition(), new CoordSet(), p_tank, false);
			} catch (PathNotFoundException ex)
			{
				throw new FSFatalException(ex);
			}
			currentDistance = mapMgr.getPathDistance(path, p_tank.getMoveCost());
			if (currentDistance < bestDistance)
			{
				bestDistance = currentDistance;
				bestCity = city;
			}
		}

		return bestCity;
	}
}