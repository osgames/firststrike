/*
 * Created on May 1, 2005
 */
package org.jocelyn_chaumel.firststrike.core.directive.helper;

import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public final class MoveHelper
{

	public static Path getPathWithCombat(	final Coord p_destination,
											final AbstractUnit p_unit,
											final boolean p_removeDestinationFromPath) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_destination, "Destination Coord");
		Assert.preconditionNotNull(p_unit, "Unit");

		final boolean isTankUnit = p_unit.getUnitType().equals(UnitTypeCst.TANK);
		final AbstractPlayer player = p_unit.getPlayer();
		final FSMapMgr mapMgr = player.getMapMgr();
		final CoordSet coordToExclude;
		if (isTankUnit)
		{
			coordToExclude = new CoordSet();
		} else
		{
			// Add, in coordSetToExclude, all enemy cities.
			coordToExclude = mapMgr.getCityList().getEnemyCity(player).getCoordSet();
		}
		final Path path = mapMgr.getPath(p_destination, coordToExclude, p_unit, p_removeDestinationFromPath);
		path.calculateTurnCost(mapMgr.getMap(), p_unit.getMoveCost(), p_unit.getMove(), p_unit.getMovementCapacity());
		return path;
	}

	public static Path getPathWithoutCombat(final Coord p_destination,
											final AbstractUnit p_unit,
											final boolean p_removeDestinationFromPath) throws PathNotFoundException
	{
		Assert.preconditionNotNull(p_destination, "Destination Coord");
		Assert.preconditionNotNull(p_unit, "Unit");

		if (p_unit.getFuelCapacity() > 0)
		{
			Assert.precondition(p_unit.getFuel() > 0, "Not Enough Fuel Pts");
		}

		final AbstractPlayer player = p_unit.getPlayer();
		final boolean isTankUnit = p_unit.getUnitType().equals(UnitTypeCst.TANK);
		final CoordSet enemyCoordSet = player.getEnemySet().getCoordSet();
		final FSMapMgr mapMgr = player.getMapMgr();
		// Only tank unit type can move trough an enemy city.
		if (!isTankUnit)
		{
			// Add, in coordSetToExclude, all enemy cities.
			enemyCoordSet.addAll(mapMgr.getCityList().getEnemyCity(player).getCoordSet());
		}

		final Path path = mapMgr.getPath(p_destination, enemyCoordSet, p_unit, p_removeDestinationFromPath);
		path.calculateTurnCost(mapMgr.getMap(), p_unit.getMoveCost(), p_unit.getMove(), p_unit.getMovementCapacity());
		return path;
	}
}