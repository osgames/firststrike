package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class MoveDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(MoveDirective.class);

	public MoveDirective(final AbstractUnit p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play()
	{
		final AbstractUnit unit = getUnit();
		Assert.precondition(unit.hasPath(), "No mandatory path set to unit");

		final Path path = unit.getPath();
		final AbstractPlayer player = unit.getPlayer();
		MoveStatusCst moveStatus = null;
		boolean isArrived = false;
		do
		{
			moveStatus = player.moveUnit(unit);

			if (MoveStatusCst.ENEMY_DETECTED.equals(moveStatus))
			{
				_logger.unitDetail("Enemy detected");
			}

			isArrived = !path.hasNextStep();

			if (!isArrived && moveStatus == MoveStatusCst.MOVE_SUCCESFULL && unit.getMove() == 0)
			{
				_logger.unitDetail("Move interrupted because not enough move pts at " + unit.getPosition());
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}

		} while (!isArrived && moveStatus == MoveStatusCst.MOVE_SUCCESFULL);

		if (isArrived && moveStatus == MoveStatusCst.ENEMY_DETECTED)
		{
			moveStatus = MoveStatusCst.MOVE_SUCCESFULL;
		}

		return moveStatus;
	}
}
