package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaStatusCst;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitAutoDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TransportShipAutoDirective extends AbstractUnitAutoDirective
{
	private static final FSLogger _logger = FSLoggerManager.getLogger(TransportShipAutoDirective.class);

	public TransportShipAutoDirective(final ComputerTransportShip p_transport, final Area p_area)
	{
		super(p_transport);
		Assert.preconditionNotNull(p_area, "Area");
		p_transport.setAssignedArea(p_area);
	}

	@Override
	protected MoveStatusCst internalPlay() throws PathNotFoundException
	{
		final ComputerTransportShip transport = (ComputerTransportShip) getUnit();
		final Area area = (Area) transport.getAssignedArea();
		final ComputerPlayer player = (ComputerPlayer) transport.getPlayer();
		final AreaStatusCst status = player.getAreaStatus(area);

		// Jouer lorsque la r�gion est controlee.
		if (AreaStatusCst.OWNED_AREA.equals(status))
		{
			_logger.unitSubAction("Apply invasion plan");
			return new TransportShipInvasionDirective(transport).play();
		}

		_logger.unitSubAction("Cancel invasion plan");
		final MoveStatusCst moveStatus = new TransportShipProtectAreaDirective(transport).play();
		if (moveStatus == MoveStatusCst.MOVE_SUCCESFULL && transport.getMove() > 0)
		{
			return play();
		}

		return moveStatus;
	}

}
