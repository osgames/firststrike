package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaList;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;

public class TransportShipProtectAreaDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TransportShipProtectAreaDirective.class);

	public TransportShipProtectAreaDirective(final ComputerTransportShip p_transport)
	{
		super(p_transport);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final ComputerTransportShip transport = (ComputerTransportShip) getUnit();

		// Transport contains units
		if (transport.isContainingUnit())
		{
			// Check if the transport is, right now, ready to unload
			final Area areaToConquer = (Area) transport.getAssignedArea();

			final MoveStatusCst moveStatusCst = new TransportShipMoveAndUnloadDirective(transport, areaToConquer)
					.play();
			if (moveStatusCst == MoveStatusCst.MOVE_SUCCESFULL && transport.getMove() > 0)
			{
				_logger.unit("Re-Play Transport {0}", transport);
				return play();
			}
			return moveStatusCst;
		}

		// Transport switch area
		final ComputerPlayer computer = (ComputerPlayer) transport.getPlayer();
		final AreaList ownedAreaSet = computer.getOwnedArea();
		if (ownedAreaSet.size() == 0)
		{
			_logger.unitDetail("No owned area by the current player.  Transport Ship's turn is skipped.");
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		final Area newArea = (Area) ownedAreaSet.iterator().next();
		_logger.unitSubAction("Transport Ship's area is switched to : {0}", newArea);
		transport.setAssignedArea(newArea);

		if (transport.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		return MoveStatusCst.MOVE_SUCCESFULL;
	}

}
