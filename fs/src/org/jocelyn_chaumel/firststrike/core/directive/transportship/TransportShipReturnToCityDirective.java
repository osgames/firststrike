package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithoutCombatDirective;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;

public final class TransportShipReturnToCityDirective extends AbstractUnitDirective
{

	public TransportShipReturnToCityDirective(final TransportShip p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		// AAAZ TU pour cette classe
		final TransportShip transport = (TransportShip) getUnit();
		final CityList cityList = transport.getPlayer().getCitySet().getPortList();
		final City city = cityList.getDirectNearestCityFrom(transport.getPosition());
		if (city.getPosition().equals(transport.getPosition()))
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		final MoveStatusCst moveStatus = new FindPathAndMoveWithoutCombatDirective(	transport,
																					city.getPosition(),
																					false,
																					false).play();
		if (MoveStatusCst.UNIT_DEFEATED.equals(moveStatus))
		{
			return moveStatus;
		}
		return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
	}

}
