package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaStatusCst;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TransportShipInvasionDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TransportShipInvasionDirective.class);

	public TransportShipInvasionDirective(final ComputerTransportShip p_transport)
	{
		super(p_transport);
	}

	/**
	 * Play a Transport Ship when the protected Area is owned.
	 */
	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final ComputerTransportShip transport = (ComputerTransportShip) getUnit();
		final ComputerPlayer player = (ComputerPlayer) transport.getPlayer();
		final Area area = (Area) transport.getAssignedArea();
		final AreaStatusCst status = player.getAreaStatus(area);
		Assert.precondition(AreaStatusCst.OWNED_AREA.equals(status), "Protected Area is no more owned");

		MoveStatusCst moveStatus = MoveStatusCst.MOVE_SUCCESFULL;
		if (transport.isContainingUnit())
		{
			_logger.unitDetail("Transport is containing tanks");
			moveStatus = new TransportShipMoveAndUnloadDirective(transport).play();
			if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
			{
				if (transport.getMove() == 0)
				{
					return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;

				} else
				{
					return play();
				}
			}
			return moveStatus;
		}
		_logger.unitDetail("Transport doesn't contain tank");
		// The transport does not contains any tank
		_logger.unitSubAction("Transport is trying to load tanks");
		moveStatus = new TransportShipMoveAndLoadDirective(transport).play();
		if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
		{
			if (transport.getMove() == 0)
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;

			} else
			{
				return play();
			}
		}
		return moveStatus;
	}

}
