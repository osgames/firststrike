package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithoutCombatDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TransportShipMoveAndUnloadDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TransportShipMoveAndUnloadDirective.class
			.getName());

	private final Area _unloadArea;

	public TransportShipMoveAndUnloadDirective(final ComputerTransportShip p_transport)
	{
		super(p_transport);

		final ComputerPlayer computer = (ComputerPlayer) p_transport.getPlayer();
		final Area loadArea = p_transport.getAssignedArea();
		_unloadArea = computer.getUnloadingArea(loadArea);
	}

	public TransportShipMoveAndUnloadDirective(final TransportShip p_transport, final Area p_unloadArea)
	{
		super(p_transport);
		Assert.preconditionNotNull(p_unloadArea, "Area");
		_unloadArea = p_unloadArea;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitSubAction("Move and Unload directive performed");
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		final ComputerTransportShip transport = (ComputerTransportShip) getUnit();
		final ComputerPlayer computer = (ComputerPlayer) transport.getPlayer();
		final CoordSet enemyCoordSet = computer.getEnemySet().getCoordSet();
		Coord transportCoord = transport.getPosition();
		CoordSet availableUnloadingCoordSet = (CoordSet) _unloadArea.getGroundCellNearToSea().clone();
		availableUnloadingCoordSet.removeAll(enemyCoordSet);
		if (availableUnloadingCoordSet.isEmpty())
		{
			availableUnloadingCoordSet = (CoordSet) _unloadArea.getGroundCellNearToSea().clone();
			_logger.unitDetail("Unload position will contain enemy unit!");
		}
		final Coord nearestGroundCoord = availableUnloadingCoordSet.getDirectNearestCoord(transportCoord);
		_logger.unitDetail("Unload ground position identified: " + nearestGroundCoord);
		MoveStatusCst moveStatus = null;

		if (nearestGroundCoord.calculateIntDistance(transportCoord) > 1)
		{
			try
			{
				moveStatus = new FindPathAndMoveWithoutCombatDirective(transport, nearestGroundCoord, true, true)
						.play();
			} catch (PathNotFoundException ex)
			{
				_logger.unitSubAction("Unable to find a path for the Transport Ship.  Move reported until the way is free.");
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
			if (MoveStatusCst.ENEMY_DETECTED.equals(moveStatus))
			{
				return recursivePlay();
			}
			if (!MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
			{
				return moveStatus;
			}
			transportCoord = transport.getPosition();
		}
		Assert.postcondition(	nearestGroundCoord.calculateIntDistance(transportCoord) <= 1,
								"Not ready to unload tanks...");

		moveStatus = new TransportShipUnloadDirective(transport, nearestGroundCoord).play();
		if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus) && transport.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		return moveStatus;
	}

}
