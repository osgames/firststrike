package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithoutCombatDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TransportShipMoveAndLoadDirective extends AbstractUnitDirective
{
	private final static FSLogger _Logger = FSLoggerManager
			.getLogger(TransportShipMoveAndLoadDirective.class.getName());

	public TransportShipMoveAndLoadDirective(final ComputerTransportShip p_transport)
	{
		super(p_transport);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final ComputerTransportShip transport = (ComputerTransportShip) getUnit();
		final ComputerPlayer player = (ComputerPlayer) transport.getPlayer();
		final Area protectedArea = transport.getAssignedArea();
		final Coord loadingCoord = player.getLoadingCoordOf(protectedArea);
		if (loadingCoord == null)
		{
			return new TransportShipReturnToCityDirective(transport).play();
		}
		MoveStatusCst moveStatus = null;

		if (transport.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		if (!transport.getPosition().equals(loadingCoord))
		{
			try
			{
				moveStatus = new FindPathAndMoveWithoutCombatDirective(transport, loadingCoord, false, false).play();
			} catch (PathNotFoundException ex)
			{
				_Logger.unitSubAction("Unable to find a path for the Transport Ship.  Move reported until the way is free.");
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
			if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
			{
				if (transport.getMove() == 0)
				{
					return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
				}
			} else
			{
				return moveStatus;
			}
		}

		return new TransportShipLoadDirective(transport).play();
	}

}
