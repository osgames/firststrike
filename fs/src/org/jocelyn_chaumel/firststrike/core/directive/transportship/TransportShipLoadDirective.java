package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.tank.TankLoadDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.IUnitContainer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TransportShipLoadDirective extends AbstractUnitDirective
{
	private final TransportShip _transport;
	private final static FSLogger _logger = FSLoggerManager.getLogger(TransportShipLoadDirective.class);

	public TransportShipLoadDirective(final ComputerTransportShip p_transport)
	{
		super(p_transport);
		Assert.preconditionNotNull(p_transport, "Transport Ship");
		_transport = p_transport;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final ComputerTransportShip transport = (ComputerTransportShip) getUnit();
		final ComputerPlayer player = (ComputerPlayer) transport.getPlayer();
		final Area protectedArea = transport.getAssignedArea();
		final Coord loadingCoord = player.getLoadingCoordOf(protectedArea);

		final AbstractUnitList tankList = player.getUnitIndex().getUnitListAt(loadingCoord)
				.getTankProtectingArea(protectedArea).getLoadableUnit(transport);
		if (tankList.getTotalSpaceCost() < ((IUnitContainer) transport).getFreeSpace())
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		_logger.unitSubAction("Loading tank in progress...");
		for (AbstractUnit tank : tankList)
		{
			if (((IUnitContainer) transport).getFreeSpace() < tank.getSpaceCost())
			{
				break;
			}
			Assert.check(	MoveStatusCst.MOVE_SUCCESFULL.equals(new TankLoadDirective((Tank) tank, _transport).play()),
							"Unexpected result");
		}
		if (((IUnitContainer) transport).getFreeSpace() == 0)
		{
			return MoveStatusCst.MOVE_SUCCESFULL;
		}
		return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
	}

}
