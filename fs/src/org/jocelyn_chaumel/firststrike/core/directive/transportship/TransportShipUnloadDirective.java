package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.tank.TankUnloadDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.ILoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TransportShipUnloadDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TransportShipUnloadDirective.class);

	private final Coord _unloadCoord;

	public TransportShipUnloadDirective(final TransportShip p_transport, final Coord p_unloadCoord)
	{
		super(p_transport);

		Assert.preconditionNotNull(p_unloadCoord, "Unloading Coord");
		_unloadCoord = p_unloadCoord;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final TransportShip transport = (TransportShip) getUnit();

		_logger.unitSubAction("The transport ship unloads its tank");
		final ArrayList<ILoadableUnit> loadableList = transport.getContainedUnitList();
		Tank currentTank = null;
		MoveStatusCst moveResult = null;
		for (int i = loadableList.size() - 1; i >= 0; i--)
		{
			currentTank = (Tank) loadableList.get(i);
			currentTank.setPosition(transport.getPosition());
			moveResult = new TankUnloadDirective(currentTank, _unloadCoord).play();
			if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveResult) && currentTank.getMove() > 0)
			{
				currentTank.playDirectives();
			}
		}

		if (loadableList.isEmpty())
		{
			return MoveStatusCst.MOVE_SUCCESFULL;
		} else
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
	}

}
