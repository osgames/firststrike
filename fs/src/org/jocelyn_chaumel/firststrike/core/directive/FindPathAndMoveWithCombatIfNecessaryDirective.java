package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FindPathAndMoveWithCombatIfNecessaryDirective extends AbstractUnitDirective
{
	private final Coord _destination;
	private final boolean _removeDestinationFromPath;

	public FindPathAndMoveWithCombatIfNecessaryDirective(	final AbstractUnit p_unit,
															final Coord p_destination,
															final boolean p_removeDestinationFromPath)
	{
		super(p_unit);

		Assert.preconditionNotNull(p_destination, "Destination");

		_destination = p_destination;
		_removeDestinationFromPath = p_removeDestinationFromPath;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final AbstractUnit unit = getUnit();

		MoveStatusCst moveStatus = null;
		try
		{
			moveStatus = new FindPathAndMoveWithoutCombatDirective(	unit,
																	_destination,
																	false,
																	_removeDestinationFromPath).play();
		} catch (PathNotFoundException ex)
		{
			moveStatus = new FindPathAndMoveWithCombatDirective(unit, _destination, _removeDestinationFromPath).play();
		}

		return moveStatus;
	}

}
