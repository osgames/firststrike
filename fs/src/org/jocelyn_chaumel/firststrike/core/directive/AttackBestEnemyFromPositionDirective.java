package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.directive.helper.TargetFinderHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class AttackBestEnemyFromPositionDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(AttackBestEnemyFromPositionDirective.class);
	private final Coord _startCoord;
	private final int _maxRange;

	public AttackBestEnemyFromPositionDirective(final AbstractUnit p_unit, final Coord p_startCood, final int p_maxRange)
	{
		super(p_unit);
		Assert.preconditionNotNull(p_startCood, "Starting position");
		_startCoord = p_startCood;
		_maxRange = p_maxRange;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		Assert.check(_maxRange > 0, "Invalid Max Range");
		AbstractUnit unit = getUnit();
		Assert.precondition(unit.getNbAttack() > 0, "No attack available");
		Assert.precondition(unit.getMove() > 0, "No move available");

		final AbstractUnit bestEnemy = TargetFinderHelper.findBestTargetInRange(unit, _startCoord, _maxRange);
		if (bestEnemy == null)
		{
			return MoveStatusCst.MOVE_SUCCESFULL;
		}

		_logger.unitDetail("Best enemy identified :" + bestEnemy);
		MoveStatusCst moveStatus = MoveStatusCst.MOVE_SUCCESFULL;
		try
		{
			moveStatus = new HuntEnemyDirective(unit, bestEnemy, true).play();
			if ((moveStatus == MoveStatusCst.ENEMY_DETECTED || moveStatus == MoveStatusCst.MOVE_SUCCESFULL)
					&& unit.getMove() > 0)
			{
				moveStatus = play();
			}
		} catch (PathNotFoundException e)
		{
			throw new FSFatalException(e);
		}

		return moveStatus;
	}

}
