package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.directive.helper.MoveHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FindPathAndMoveWithCombatDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(FindPathAndMoveWithCombatDirective.class);

	private final Coord _destination;
	private final boolean _removeDestinationFromPath;

	public FindPathAndMoveWithCombatDirective(	AbstractUnit p_unit,
												final Coord p_destination,
												final boolean p_removeDestinationFromPath)
	{
		super(p_unit);
		Assert.preconditionNotNull(p_destination, "Destination Coord");

		_destination = p_destination;
		_removeDestinationFromPath = p_removeDestinationFromPath;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		AbstractUnit unit = getUnit();

		if (unit.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		final Path path = MoveHelper.getPathWithCombat(_destination, unit, _removeDestinationFromPath);
		unit.setPath(path);

		_logger.unitDetail("Unit is moving to {0}", path);
		MoveStatusCst moveStatus = new MoveWithCombatDirective(unit).play();

		if (moveStatus == MoveStatusCst.ENEMY_DETECTED)
		{
			_logger.unitDetail("Enemy detected!");
			return play();
		}

		return moveStatus;
	}

}
