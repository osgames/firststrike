package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.directive.helper.TargetFinderHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class CombatBoatReturnToCityDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(CombatBoatReturnToCityDirective.class);

	public CombatBoatReturnToCityDirective(final AbstractUnit p_unit)
	{
		super(p_unit);
		Assert.precondition((p_unit instanceof Battleship) || (p_unit instanceof Destroyer),
							"Combat Boat expected (Destroyer or Battleship)");
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final AbstractUnit combatBoat = getUnit();

		final AbstractPlayer player = combatBoat.getPlayer();
		final FSMapMgr mapMgr = player.getMapMgr();
		final CityList playerCityList = mapMgr.getCityList().getCityByPlayer(player).getPortList();
		
		if (playerCityList.isEmpty())
		{
			return new CombatBoatKamikazeDirective(combatBoat).play();
		}
		
		final Coord cityCoord = TargetFinderHelper.getNearestPosition(	combatBoat,
																		playerCityList.getCoordSet(),
																		true,
																		false);

		if (cityCoord == null)
		{
			return new CombatBoatKamikazeDirective(combatBoat).play();
		}
		
		if (combatBoat.getPosition().equals(cityCoord))
		{
			// Already in a city
			_logger.unitDetail("Destroyer already positionned in a City :", cityCoord);
			return MoveStatusCst.MOVE_SUCCESFULL;
		} else 
		{
			// Return to a city
			_logger.unitSubAction("Destroyer return to city: " + cityCoord);
			final MoveStatusCst result = new FindPathAndMoveWithCombatIfNecessaryDirective(combatBoat, cityCoord, false)
					.play();
			if (MoveStatusCst.MOVE_SUCCESFULL.equals(result))
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
			return result;
		}
	}

}
