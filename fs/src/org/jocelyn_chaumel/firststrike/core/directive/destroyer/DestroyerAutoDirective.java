package org.jocelyn_chaumel.firststrike.core.directive.destroyer;

import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.CombatBoatPatrolDirective;
import org.jocelyn_chaumel.firststrike.core.directive.CombatBoatReturnToCityDirective;
import org.jocelyn_chaumel.firststrike.core.directive.helper.TargetFinderHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class DestroyerAutoDirective extends AbstractUnitAutoDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(DestroyerAutoDirective.class);

	public DestroyerAutoDirective(final Destroyer p_destroyer)
	{
		super(p_destroyer);
	}

	@Override
	protected MoveStatusCst internalPlay() throws PathNotFoundException
	{
		_logger.unitAction("Play Auto Directive: " + getUnit());
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		final Destroyer destroyer = (Destroyer) getUnit();
		if (destroyer.getMove() == 0)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		final ComputerPlayer player = (ComputerPlayer) destroyer.getPlayer();
		MoveStatusCst result = null;
		if (destroyer.isDamaged())
		{
			_logger.unitDetail("Destroyer is damaged.");
			if (destroyer.isProtectingTransport())
			{
				_logger.unitDetail("Destoyer gives no more protection to its assigned Transport Ship");
				destroyer.removeProtectedTransport();
			}
			result = new CombatBoatReturnToCityDirective(destroyer).play();
			if (MoveStatusCst.MOVE_SUCCESFULL.equals(result))
			{
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
			return result;
		}

		if (!destroyer.isProtectingTransport())
		{
			final AbstractUnitList transportList = player.getTransportWithoutProtection();
			if (!transportList.isEmpty())
			{
				Coord nearestCoord = TargetFinderHelper.getNearestPosition(	destroyer,
																			transportList.getCoordSet(),
																			false,
																			false);
				TransportShip transportToProtect = (TransportShip) transportList.getFirstUnitAt(nearestCoord);
				destroyer.setProtectedTransport(transportToProtect);
				_logger.unitDetail("Destroyer is protecting a new Transport Ship: " + transportToProtect);
			}
		}

		if (destroyer.isProtectingTransport())
		{
			Assert.check(destroyer.getProtectedTransportShip().isLiving(), "Invalid Protected Transport Ship");
			return new DestroyerProtectTransportDirective(destroyer).play();
		}

		final AbstractUnitList enemyList = player.getEnemySet().getReacheableEnemy(destroyer);

		if (enemyList.isEmpty())
		{
			result = new CombatBoatPatrolDirective(destroyer, CombatBoatPatrolDirective.NO_PATROL_LIMIT).play();
		} else
		{
			result = new DestroyerAttackDirective(destroyer).play();
		}

		if (MoveStatusCst.MOVE_SUCCESFULL.equals(result) || MoveStatusCst.ENEMY_DETECTED.equals(result))
		{
			return recursivePlay();
		}

		return result;
	}
}
