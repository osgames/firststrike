package org.jocelyn_chaumel.firststrike.core.directive.destroyer;

import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.CombatBoatPatrolDirective;
import org.jocelyn_chaumel.firststrike.core.directive.CombatBoatReturnToCityDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class DestroyerProtectTransportDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(DestroyerProtectTransportDirective.class);
	private final int _patrolRange;
	private final int _unloadPositionClearRange;

	public DestroyerProtectTransportDirective(Destroyer p_destroyer)
	{
		super(p_destroyer);
		_patrolRange = 5;
		_unloadPositionClearRange = 3;
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		final Destroyer destroyer = (Destroyer) getUnit();
		Assert.precondition(destroyer.isProtectingTransport(), "This transport doesn't protect any transport");
		_logger.unitSubAction("Protecting Transport Ship: " + destroyer.getProtectedTransportShip());
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		final Destroyer destroyer = (Destroyer) getUnit();

		final TransportShip transport = destroyer.getProtectedTransportShip();
		final Coord transportPosition = transport.getPosition();
		MoveStatusCst result = null;

		if (destroyer.getMove() == 0)
		{
			_logger.unitDetail("No more move available");
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		if (destroyer.isDamaged())
		{
			_logger.unitDetail("Destroyed is damaged. The transport ship protection directive is stopped");
			destroyer.removeProtectedTransport();
			return new CombatBoatReturnToCityDirective(destroyer).play();
		}

		final int range;
		// Clean the unload position
		if (transport.isContainingUnit()
				&& (!transport.hasPath() || transport.getPath().getCoordList().size() < _patrolRange))
		{
			range = _unloadPositionClearRange;
		} else
		{
			range = _patrolRange;
		}

		final Coord coordToProtect;
		if (!transport.hasPath())
		{
			coordToProtect = transportPosition;
		} else
		{
			final CoordList coordList = transport.getPath().getCoordList();
			coordToProtect = coordList.get(Math.min(2, coordList.size() - 1));
		}

		_logger.unitDetail("Destroyer protecting a Transport from " + coordToProtect + " in range " + range);

		final ComputerPlayer computer = (ComputerPlayer) destroyer.getPlayer();
		final FSMap map = computer.getMapMgr().getMap();
		// Get Coords around the targeted coord to protect + coord right next to
		// the destroyer
		final CoordSet coordSetSearchArea = getEnemyCoordListSearch(map, coordToProtect, range);
		coordSetSearchArea.addAll(getEnemyCoordListSearch(map, destroyer.getPosition(), range));

		final AbstractUnitList enemyList = computer.getEnemySet().getUnitByCoordList(coordSetSearchArea).getReacheableEnemy(destroyer);
		if (!enemyList.isEmpty())
		{
			_logger.playerSubAction("Ennemy detected around Transport Ship to protect.  Destroyer will clean the zone.");
			result = new DestroyerAttackDirective(destroyer).play();
			if (MoveStatusCst.MOVE_SUCCESFULL.equals(result) || MoveStatusCst.ENEMY_DETECTED.equals(result))
			{
				return recursivePlay();
			}
		}

		if (destroyer.getMove() > 0)
		{
			if (destroyer.hasPath())
			{
				final Coord destination = destroyer.getPath().getDestination();
				if (destination.calculateIntDistance(coordToProtect) > range)
				{
					_logger.unitDetail(	"Current destroyer destination {0} is out of range.  Current path is cleared",
										destination);
					destroyer.clearPath();
				}
			}
			result = new CombatBoatPatrolDirective(destroyer, coordToProtect, range).play();
		} else
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;

		}
		if (MoveStatusCst.ENEMY_DETECTED.equals(result))
		{
			return recursivePlay();
		}

		return result;

	}

	private CoordSet getEnemyCoordListSearch(final FSMap p_map, final Coord p_startCoord, final int p_range)
	{

		final CoordSet allCoordSet = CoordSet.createInitiazedCoordSet(	p_startCoord,
																		p_range,
																		p_map.getWidth(),
																		p_map.getHeight());
		final CoordSet coordSearchArea = new CoordSet();

		Cell currentCell = null;
		for (Coord currentCoord : allCoordSet)
		{
			currentCell = p_map.getCellAt(currentCoord);
			if (currentCell.isSeaCell() || currentCell.isNearToWather())
			{
				coordSearchArea.add(currentCoord);
			}
		}

		return coordSearchArea;
	}
}
