package org.jocelyn_chaumel.firststrike.core.directive.destroyer;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.directive.HuntEnemyDirective;
import org.jocelyn_chaumel.firststrike.core.directive.helper.TargetFinderHelper;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class DestroyerAttackDirective extends AbstractUnitDirective
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(DestroyerAttackDirective.class);

	/**
	 * Start the directive in selecting the first target into the proposed enemy
	 * list.
	 * 
	 * @param p_unit The destroyer.
	 * @param p_enemyList The proposed enemy list.
	 */
	public DestroyerAttackDirective(final Destroyer p_unit)
	{
		super(p_unit);
	}

	@Override
	public MoveStatusCst play() throws PathNotFoundException
	{
		_logger.unitSubAction("Destroyer attacking enemies...");
		return recursivePlay();
	}

	private MoveStatusCst recursivePlay() throws PathNotFoundException
	{
		final Destroyer destroyer = (Destroyer) getUnit();

		final AbstractPlayer player = destroyer.getPlayer();
		final AbstractUnitList enemyList = player.getEnemySet().getReacheableEnemy(destroyer);
		if (enemyList.isEmpty())
		{
			return MoveStatusCst.MOVE_SUCCESFULL;
		}
		
		final AbstractUnit enemy = destroyer.chooseBestEnemyTargetFromList(enemyList);
		if (enemy == null)
		{
			return MoveStatusCst.MOVE_SUCCESFULL;
		}

		MoveStatusCst moveStatus = null;
		try
		{
			_logger.unitDetail("Enemy selected: {0}", enemy);
			moveStatus = new HuntEnemyDirective(destroyer, enemy, true).play();
		} catch (PathNotFoundException ex)
		{
			throw new FSFatalException("Unexpected exception occurs", ex);
		}

		if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
		{
			if (destroyer.getMove() > 0)
			{
				_logger.unitSubAction("Attack directive is relaunched");
				return recursivePlay();
			} else
			{
				_logger.unitSubAction("Attack directive is interrupted (no more move pts)");
				return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
			}
		}

		return moveStatus;
	}

}
