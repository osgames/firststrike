package org.jocelyn_chaumel.firststrike.core.help;

import java.util.ResourceBundle;

public class Chapter
{

	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private final boolean _groupChapter;
	private boolean _isDeployed = true;
	private final String _id;
	private final String _textI18N;
	private final String _category;
	private final String _titleI18N;
	private final String _imageName;

	public Chapter(final String p_id, final String p_category, final String p_chapterTitle, final String p_chapterText, final String p_imageName)
	{
		_id = p_id;
		_category =  p_category;
		_titleI18N = _bundle.getString(p_chapterTitle);
		_textI18N = _bundle.getString(p_chapterText);
		_imageName = p_imageName;
		_groupChapter = false;
	}
	
	public Chapter(final String p_category)
	{
		_id = null;
		_category = p_category;
		_textI18N = null;
		_titleI18N = _bundle.getString("fs.help.category." + p_category);
		_imageName = null;
		_isDeployed = true;
		_groupChapter = true;
	}
	
	public final boolean isGroupChapter()
	{
		return _groupChapter;
	}
	
	public final boolean isDeployed()
	{
		return _isDeployed;
	}
	
	public final void setIsDeployed(final boolean p_isDeployed) {
		_isDeployed = p_isDeployed;
	}

	public final String getId()
	{
		return _id;
	}
	
	public final String getCategory()
	{
		return _category;
	}

	public final String getText()
	{
		return _textI18N;
	}

	public final String getImageName()
	{
		return _imageName;
	}

	public final String getTitle()
	{
		return _titleI18N;
	}
	
	@Override
	public final String toString()
	{
		return HelpMgr.getCatPriority(_category) + " - " + _titleI18N;
	}
}
