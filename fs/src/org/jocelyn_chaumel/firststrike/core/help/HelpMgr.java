package org.jocelyn_chaumel.firststrike.core.help;

import java.awt.Dimension;
import java.awt.Window;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JDialog;
import javax.swing.JFrame;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.gui.commun.help_popup.HelpDialog;
import org.jocelyn_chaumel.tools.DataProperties;
import org.jocelyn_chaumel.tools.StringManipulatorHelper;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class HelpMgr
{

	private static HelpMgr _instance = null;
	public static final String APPLICATION_PROPERTIES_RESOURCE = "org/jocelyn_chaumel/firststrike/resources/help/helpChapterDefinition.properties";
	private final static FSLogger _logger = FSLoggerManager.getLogger(HelpMgr.class.getName());

	private final ApplicationConfigurationMgr _appConf = ApplicationConfigurationMgr.getInstance();
	public final static String HELP_WELCOME_CHAPTER = "welcome";
	public final static String HELP_LEFT_PANEL_CHAPTER = "left_panel";
	public final static String HELP_CITIES_CHAPTER = "cities";
	public final static String HELP_FOG_CHAPTER = "fog";
	public final static String HELP_PATH_CHAPTER = "path";
	public final static String HELP_ARTILLERY_CHAPTER = "artillery";
	public final static String HELP_TANK_CHAPTER = "tank";
	public final static String HELP_FIGHTER_CHAPTER = "fighter";
	public final static String HELP_TRANSPORT_SHIP_CHAPTER = "transport_ship";
	public final static String HELP_DESTROYER_CHAPTER = "destroyer";
	public final static String HELP_BATTLESHIP_CHAPTER = "battleship";
	public final static String HELP_MOVE_UNIT_CHAPTER = "move_unit";
	public final static String HELP_FOG_MOVE_CHAPTER = "fog_move";
	public final static String HELP_ATTACK_ENEMY_CHAPTER = "attack_enemy";
	public final static String HELP_LOADING_TANK_CHAPTER = "loading_tank";
	public final static String HELP_UNLOAD_ACTION_CHAPTER = "unload_action";
	
	public final static Byte CAT_WELCOME = 0;
	public final static Byte CAT_INTERFACE = 1;
	public final static Byte CAT_UNIT = 2;
	public final static Byte CAT_ACTION = 3;

	public final static String[] HELP_TOPIC_ARRAY = new String[] { HELP_WELCOME_CHAPTER, HELP_LEFT_PANEL_CHAPTER,
			HELP_CITIES_CHAPTER, HELP_TANK_CHAPTER, HELP_FIGHTER_CHAPTER,
			HELP_DESTROYER_CHAPTER, HELP_BATTLESHIP_CHAPTER, HELP_ARTILLERY_CHAPTER, HELP_TRANSPORT_SHIP_CHAPTER,
			HELP_MOVE_UNIT_CHAPTER, HELP_FOG_CHAPTER, HELP_FOG_MOVE_CHAPTER,
			HELP_ATTACK_ENEMY_CHAPTER, HELP_PATH_CHAPTER, HELP_LOADING_TANK_CHAPTER,
			HELP_UNLOAD_ACTION_CHAPTER };
	private final DataProperties _properties;
	private final HashMap<String, Chapter> _chapterMap = new HashMap<String, Chapter>();

	private HelpMgr() throws IOException
	{
		_properties = new DataProperties();
		final URL url = ClassLoader.getSystemResource(APPLICATION_PROPERTIES_RESOURCE);
		InputStream inputStream = url.openStream();
		try
		{

			_properties.load(inputStream);
		} finally
		{
			inputStream.close();
		}

		String title, text, image, category;
		for (int i=0; i < HELP_TOPIC_ARRAY.length; i++ )
		{
			 title = _properties.getProperty("chapter." + HELP_TOPIC_ARRAY[i] + ".title");
			 text = _properties.getProperty("chapter." +  HELP_TOPIC_ARRAY[i] + ".text");
			 category = _properties.getProperty("chapter." +  HELP_TOPIC_ARRAY[i] + ".category");
			 image = _properties.getProperty("chapter." +  HELP_TOPIC_ARRAY[i] + ".image");
			 _chapterMap.put(HELP_TOPIC_ARRAY[i], new Chapter(HELP_TOPIC_ARRAY[i], category, title, StringManipulatorHelper.replace(text, "\\n", "\n"), image));
		}

		
	}
	
	public final static byte getCatPriority(final String p_category)
	{
		if ("welcome".equals(p_category)) {
			return CAT_WELCOME;
		} else if ("interface".equals(p_category)) {
			return CAT_INTERFACE;
		} else if ("unit".equals(p_category)) {
			return CAT_UNIT;
		} else if ("action".equals(p_category)) {
			return CAT_ACTION;
		} else {
			throw new FSFatalException("Unsupport Help Category detected: " + p_category);
		}
	}

	public final static HelpMgr getOrCreateInstance() throws IOException
	{
		if (_instance == null)
		{
			_instance = new HelpMgr();
		}

		return _instance;
	}

	public final static HelpMgr getInstance()
	{
		Assert.check(_instance != null, "Null instance");

		return _instance;
	}

	public final Chapter getChapter(final String p_chapterID)
	{
		Assert.precondition(_chapterMap.containsKey(p_chapterID), "Unknow chapter ID: " + p_chapterID);
		
		return _chapterMap.get(p_chapterID);
	}

	public final void setOptionDisplayHelp(final boolean p_newValue)
	{
		_logger.systemSubActionDetail("Display Help Option =" + p_newValue);
		_appConf.setOptionDisplayHelp(p_newValue);
	}

	public final void appendConsultedChapterNameList(final ArrayList<String> p_chapterNameList)
	{
		_appConf.appendChapterNameList(p_chapterNameList);
	}

	public final boolean save()
	{
		try
		{
			return _appConf.save();
		} catch (IOException ex)
		{
			_logger.systemError("Unable to save the application configuration file", ex);
		}

		return false;
	}

	public final boolean isAlreadyDisplayed(final ArrayList<String> p_chapterNameList)
	{
		final ArrayList<String> displayedChapterNameList = _appConf.getChapterNameList();

		return displayedChapterNameList.containsAll(p_chapterNameList);
	}
	
	public final void displayHelpOnDemand(final Window p_frame, final String[] p_chapterNameArray)
	{
		final ArrayList<String> chapterNameList = new ArrayList<String>(p_chapterNameArray.length);
		for (int i = 0; i < p_chapterNameArray.length; i++)
		{
			chapterNameList.add(p_chapterNameArray[i]);
		}
		displayHelp(p_frame, chapterNameList);
	}


	public final void displayHelpIfRequired(final Window p_frame, final String[] p_chapterNameArray)
	{
		final ArrayList<String> chapterNameList = new ArrayList<String>(1);
		for (int i = 0; i < p_chapterNameArray.length; i++)
		{
			chapterNameList.add(p_chapterNameArray[i]);
		}
		
		if (!_appConf.isOptionDisplayHelpEnabled() || isAlreadyDisplayed(chapterNameList))
		{
			return;
		}
		
		displayHelp(p_frame, chapterNameList);
	}

	private void displayHelp(final Window p_frame, final ArrayList<String> p_chapterNameList)
	{
		final HelpDialog dlg;
		if (p_frame instanceof JFrame)
		{
			dlg = new HelpDialog((JFrame) p_frame, p_chapterNameList);
		} else
		{
			dlg = new HelpDialog((JDialog) p_frame, p_chapterNameList);
		}
		dlg.setSize(1100, 600);
		dlg.setPreferredSize(new Dimension(1100, 600));
		dlg.setLocationRelativeTo(p_frame);
		dlg.setVisible(true);
		dlg.dispose();
	}
}
