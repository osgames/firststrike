/*
 * Created on Mar 23, 2005
 */
package org.jocelyn_chaumel.firststrike.core;

import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public interface IDetection
{

	/**
	 * Permet de savoir si cette objet d�tecte un ennemi donn�.
	 * 
	 * @param aEnemyUnit L'ennemi � d�tecter.
	 * @return Vrai ou faux.
	 */
	public boolean detect(final AbstractUnit aEnemyUnit);

	/**
	 * Retourne la capacit� de d�tection de l'objet.
	 * 
	 * @return Capacit� de d�tection.
	 */
	public int getDetectionRange();

	/**
	 * Retourne la position de l'objet.
	 * 
	 * @return
	 */
	public Coord getPosition();

	/**
	 * Retourne l'identifiant du joueur propri�taire de l'objet.
	 * 
	 * @return Identifiant du joueur propri�taire.
	 */
	public AbstractPlayer getPlayer();
}
