/*
 * Created on May 8, 2005
 */
package org.jocelyn_chaumel.firststrike.core;

/**
 * @author Jocelyn Chaumel
 */
public class FSFatalException extends RuntimeException
{

	/**
	 * Constructeur par défaut.
	 */
	public FSFatalException()
	{
		super();
	}

	/**
	 * Constructeur paramétrisé.
	 * 
	 * @param anErrorMsg Message d'erreur.
	 */
	public FSFatalException(final String anErrorMsg)
	{
		super(anErrorMsg);
	}

	/**
	 * Constructeur paramétrisé.
	 * 
	 * @param arg0 Cause de l'erreur.
	 */
	public FSFatalException(final Throwable arg0)
	{
		super(arg0);
	}

	/**
	 * Constructeur paramétrisé.
	 * 
	 * @param anErrorMsg Message d'erreur.
	 * @param arg1 Cause de l'erreur.
	 */
	public FSFatalException(final String anErrorMsg, final Throwable arg1)
	{
		super(anErrorMsg, arg1);
	}

}
