package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public interface IBombardment
{
	public final static float BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_SEA = 0.4f;
	public final static float BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_GRASSLAND = 1f;
	public final static float BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_CITY = 0.8f;
	public final static float BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_LIGHT_WOOD = 0.8f;
	public final static float BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_HEAVY_WOOD = 0.5f;
	public final static float BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_HILL = 0.75f;
	public final static float BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_MOUNTAIN = 0.6f;
	public final static float BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_RIVER = 0.6f;

	public int getBombardmentRange();

	public int getMinBombardmentAttack();

	public int getMaxBombardmentAttack();

	public void bombPosition(final Coord p_coord);

	public int getNbBombardment();

	public int getNbBombardmentCapacity();

	/**
	 * Return the % of efficiency of a bombardment depending on the cell type.
	 * 
	 * @param p_cellType Cell Type
	 * @return a % (0 - 1)
	 */
	public float getBombardmentFactor(final CellTypeCst p_cellType);
}
