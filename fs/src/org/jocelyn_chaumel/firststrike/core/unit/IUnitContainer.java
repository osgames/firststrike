package org.jocelyn_chaumel.firststrike.core.unit;

import java.util.ArrayList;
import java.util.HashSet;

import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;

public interface IUnitContainer
{
	/**
	 * Identification des type d'unit� support� par le transporteur
	 * 
	 * @return
	 */
	public HashSet<UnitTypeCst> getSupportedTypeOfContainedUnit();

	/**
	 * Supprimer l'unit� de la list des unit�s contenues. <br>
	 * ATTENTION: L'intelligence est principalement port�e par l'unit�
	 * transport�e.
	 * 
	 * @param p_loadableUnit
	 */
	public void unloadUnit(ILoadableUnit p_loadableUnit);
	
	/**
	 * Add a loadable unit in the contained list. <br>
	 * WARNING: The intelligence is bring by the unit itself.  See loadable unit for more details.
	 * 
	 * @param p_loadableUnit
	 */
	public void loadUnit(ILoadableUnit p_loadableUnit);
	
	public boolean isContained(ILoadableUnit p_unit);
	
	/**
	 * This method should performs all the following tests/checks:<br>
	 * - if the unit type is supported<br>
	 * - if the enough move points (move == move capacity)<br>
	 * - if the player is the same for both units<br>
	 * - if enough free space<br>
	 * - if the unit is close to the container (distance <= 1)
	 * If at least one of these condition is note reached, the method return false.
	 * 
	 * @param p_unit
	 * @return
	 */
	public boolean isLoadable(AbstractUnit p_unit);
	public boolean isContainingUnit();
	public ArrayList<ILoadableUnit> getContainedUnitList();
	public int getFreeSpace();
	
	/**
	 * This method must be used for memory release purpose (when the unit is killed for example).
	 * WARNING: The intelligence is bring by the unit itself.  See loadable unit for more details.
	 */
	public ArrayList<ILoadableUnit> releaseAllUnit();
}
