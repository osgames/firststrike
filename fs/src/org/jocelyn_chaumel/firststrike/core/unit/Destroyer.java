/*
 * ComputerUnit.java Created on 14 juin 2003, 13:31
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import java.util.HashMap;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.map.AbstractMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.DestroyerMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public final class Destroyer extends AbstractUnit
{
	private static final DestroyerMoveCost _seaMoveCost = (DestroyerMoveCost) MoveCostFactory
			.getMoveCostInstance(UnitInfo.DESTROYER_INFO.getUnitType());

	private TransportShip _protectedTransport = null;

	/**
	 * Constructor.
	 */
	public Destroyer(final Coord p_positionCoord, final AbstractPlayer p_player)
	{
		super(UnitInfo.DESTROYER_INFO, UnitCombatInfo.DESTROYER_COMBAT_INFO, _seaMoveCost, p_positionCoord, p_player);

	}

	@Override
	public AbstractMoveCost getMoveCost()
	{
		return _seaMoveCost;
	}

	@Override
	public final int getDetectionRange()
	{
		int range = super.getDetectionRange();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				range--;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				range++;
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				range *= 2;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return range;
	}

	@Override
	public final int getMinAttack()
	{
		int minAttack = super.getMinAttack();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				minAttack = minAttack + 5;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				minAttack = minAttack - 5;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				minAttack = minAttack - 10;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				minAttack = minAttack + 25;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return minAttack;
	}

	@Override
	public final int getMinDefense()
	{
		int minDef = super.getMinDefense();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				minDef = minDef + 5;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				minDef = minDef - 5;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				minDef = minDef - 10;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				minDef = minDef + 15;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return minDef;
	}

	public boolean isProtectingTransport()
	{
		return _protectedTransport != null;
	}

	public TransportShip getProtectedTransportShip()
	{
		return _protectedTransport;
	}

	public void removeProtectedTransport()
	{
		final ComputerPlayer player = (ComputerPlayer) getPlayer();
		final HashMap<TransportShip, Destroyer> transportProtectMap = player.getDestroyerProtectionMap();

		Assert.check(transportProtectMap.containsKey(_protectedTransport), "Invalid Protected Transport Detected");
		Assert.check(transportProtectMap.get(_protectedTransport) == this, "Invalid Destroyer Detected");

		transportProtectMap.remove(_protectedTransport);
		_protectedTransport = null;

	}

	public void setProtectedTransport(final TransportShip p_transport)
	{
		Assert.preconditionNotNull(p_transport, "Transport Ship");

		final ComputerPlayer player = (ComputerPlayer) getPlayer();
		final HashMap<TransportShip, Destroyer> transportProtectMap = player.getDestroyerProtectionMap();
		Assert.check(!transportProtectMap.containsKey(p_transport), "This transport is already protected");

		transportProtectMap.put(p_transport, this);
		_protectedTransport = p_transport;
	}
}