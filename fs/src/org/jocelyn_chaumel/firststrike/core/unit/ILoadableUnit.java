package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.tools.data_type.Coord;

public interface ILoadableUnit
{
	public boolean isLoaded();
	public IUnitContainer getContainer();
	
	/**
	 * Charge l'unit� dans une unit� ayant la capacit� de la transporter.<BR>
	 * ATTENTION: L'intelligence est port�e par l'unit� transport�e.
	 * 
	 * @param p_unitContainer
	 * @return
	 */
	public MoveStatusCst loadInContainer(final IUnitContainer p_unitContainer);
	
	/**
	 * Unload an unit from its container.<br>
	 * WARNING: The intelligence is bring by the loadable unit.
	 * 
	 * @param p_targetCoord
	 * @return
	 */
	public MoveStatusCst unloadFromContainer(final Coord p_targetCoord);
	
	
	public int getSpaceCost();
	 
}
