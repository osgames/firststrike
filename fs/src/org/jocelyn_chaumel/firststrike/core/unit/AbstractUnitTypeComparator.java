package org.jocelyn_chaumel.firststrike.core.unit;

import java.util.Comparator;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class AbstractUnitTypeComparator implements Comparator<AbstractUnit>
{

	@Override
	public int compare(final AbstractUnit p_unit1, final AbstractUnit p_unit2)
	{
		Assert.preconditionNotNull(p_unit1, "Unit 1");
		Assert.preconditionNotNull(p_unit2, "Unit 2");

		final int unitType1 = getWeight(p_unit1.getUnitType());
		final int unitType2 = getWeight(p_unit2.getUnitType());

		return unitType1 - unitType2;
	}

	private int getWeight(final UnitTypeCst p_unitType)
	{

		if (UnitTypeCst.CARRIER.equals(p_unitType))
		{
			return 8;
		} else if (UnitTypeCst.BATTLESHIP.equals(p_unitType))
		{
			return 7;
		} else if (UnitTypeCst.DESTROYER.equals(p_unitType))
		{
			return 6;
		} else if (UnitTypeCst.SUBMARIN.equals(p_unitType))
		{
			return 5;
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_unitType))
		{
			return 4;
		} else if (UnitTypeCst.FIGHTER.equals(p_unitType))
		{
			return 3;
		} else if (UnitTypeCst.ARTILLERY.equals(p_unitType))
		{
			return 2;
		} else if (UnitTypeCst.TANK.equals(p_unitType))
		{
			return 1;
		} else
		{
			throw new FSFatalException("Unsupported UnitType: " + p_unitType);
		}
	}

}
