/*
 * Created on Apr 10, 2005
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import java.util.ArrayList;
import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.data_type.FSUtil;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class AbstractUnitList extends ArrayList<AbstractUnit>
{

	public final String SHORT_NAME = "UnitList";

	public AbstractUnitList()
	{

	}

	public AbstractUnitList(final AbstractUnitList p_unitList)
	{
		super(p_unitList);
	}

	/**
	 * Retourne une nouvelle liste ne contenant que les coordonn�es des unit�es
	 * avec doublon.
	 * 
	 * @return Liste de coordonn�es.
	 */
	public final CoordList getCoordList()
	{
		final CoordList coordList = new CoordList();
		final int size = size();
		for (int i = 0; i < size; i++)
		{
			coordList.add(((AbstractUnit) get(i)).getPosition());
		}

		return coordList;
	}

	/**
	 * Retourne une nouvelle liste ne contenant que les coordonn�es des unit�es
	 * sans doublon.
	 * 
	 * @return Liste de coordonn�es.
	 */
	public final CoordSet getCoordSet()
	{
		final CoordSet coordSet = new CoordSet();
		coordSet.addAll(getCoordList());

		return coordSet;
	}

	public final int getNbUnitAtPosition(final Coord p_position)
	{
		Assert.preconditionNotNull(p_position, "Position");

		final Iterator iter = iterator();

		int nbUnit = 0;
		while (iter.hasNext())
		{
			if (((AbstractUnit) iter.next()).getPosition().equals(p_position))
			{
				nbUnit++;
			}
		}

		return nbUnit;
	}

	public final AbstractUnit getNearestUnit(final AbstractUnit p_unit, final FSMapMgr p_map)
	{
		final CoordSet coordToExclude = p_unit.getPlayer().getEnemySet().getCoordSet();
		if (UnitTypeCst.FIGHTER.equals(p_unit.getUnitType()))
		{
			coordToExclude.addAll(p_map.getCityList().getEnemyCity(p_unit.getPlayer()).getCoordSet());
		}

		AbstractUnit nearestTargetUnit = null;
		int shortestDistance = Integer.MAX_VALUE;
		int currentDistance;
		Path currentPath;
		for (AbstractUnit currentUnit : this)
		{
			if (currentUnit.getPosition().calculateIntDistance(p_unit.getPosition()) == 0)
			{
				return currentUnit;
			} else if (currentUnit.getPosition().calculateIntDistance(p_unit.getPosition()) == 1)
			{
				nearestTargetUnit = currentUnit;
				shortestDistance = 0;
			} else
			{
				try
				{
					currentPath = p_map.getPath(currentUnit.getPosition(), coordToExclude, p_unit, true);
					currentDistance = p_map.getPathDistance(currentPath, currentUnit.getMoveCost());
					if (currentDistance < shortestDistance)
					{
						nearestTargetUnit = currentUnit;
						shortestDistance = currentDistance;
					}
				} catch (PathNotFoundException ex)
				{
					// Unit not accessible, removed from the list
				}
			}
		}

		return nearestTargetUnit;
	}

	public final AbstractUnitList getUnitByType(final UnitTypeCst p_unitType)
	{
		Assert.preconditionNotNull(p_unitType, "Unit Type");
		Assert.precondition(!UnitTypeCst.NULL_TYPE.equals(p_unitType), "Null type unit is not supported");

		final AbstractUnitList unitListFinal = new AbstractUnitList();
		final Iterator unitIter = iterator();
		AbstractUnit currentUnit;
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			if (currentUnit.getUnitType().equals(p_unitType))
			{
				unitListFinal.add(currentUnit);
			}
		}

		return unitListFinal;
	}

	public final AbstractUnitList getTankProtectingCity(final City p_city)
	{
		AbstractUnitList tankList = new AbstractUnitList();
		for (AbstractUnit unit : this)
		{
			if (unit instanceof Tank)
			{
				if (((ComputerTank) unit).isProtectingCity() && ((ComputerTank) unit).getProtectedCity() == p_city)
				{
					tankList.add(unit);
				}
			}
		}

		return tankList;
	}

	public final AbstractUnitList getTransportAssignedArea(final Area p_area)
	{
		AbstractUnitList transportList = new AbstractUnitList();
		for (AbstractUnit unit : this)
		{
			if (unit instanceof ComputerTransportShip)
			{
				if (((ComputerTransportShip) unit).isAssignedToArea()
						&& ((ComputerTransportShip) unit).getAssignedArea() == p_area)
				{
					transportList.add(unit);
				}
			}
		}

		return transportList;

	}

	public final AbstractUnitList getUnitByTypeAndCoordSet(final UnitTypeCst p_unitType, final CoordSet p_coordSet)
	{
		Assert.preconditionNotNull(p_unitType, "Unit Type");
		Assert.precondition(!UnitTypeCst.NULL_TYPE.equals(p_unitType), "Null type unit is not supported");

		final AbstractUnitList unitListFinal = new AbstractUnitList();
		final Iterator unitIter = iterator();
		AbstractUnit currentUnit;
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			if (!currentUnit.getUnitType().equals(p_unitType))
			{
				continue;
			}

			if (!p_coordSet.contains(currentUnit.getPosition()))
			{
				continue;
			}
			unitListFinal.add(currentUnit);
		}

		return unitListFinal;
	}

	public final AbstractUnitList getNonFullTransportShip()
	{
		final AbstractUnitList transportList = new AbstractUnitList();
		for (AbstractUnit unit : this)
		{
			if (!UnitTypeCst.TRANSPORT_SHIP.equals(unit.getUnitType()))
			{
				continue;
			}

			if (((IUnitContainer) unit).getFreeSpace() == 0)
			{
				continue;
			}

			transportList.add(unit);
		}

		return transportList;
	}

	/**
	 * Retrieve the Tank of the Area not assigned to a city.
	 * 
	 * @param p_area
	 * @return
	 */
	public final AbstractUnitList getTankProtectingArea(final Area p_area)
	{
		Assert.preconditionNotNull(p_area, "Area");

		final AbstractUnitList tankList = new AbstractUnitList();
		Area currentArea = null;
		for (AbstractUnit unit : this)
		{
			if (unit instanceof Tank)
			{
				if (!((ComputerTank) unit).isProtectingCity())
				{
					currentArea = unit.getPlayer().getMapMgr().getAreaAt(unit.getPosition(), AreaTypeCst.GROUND_AREA);
					if (currentArea == p_area)
					{
						tankList.add(unit);
					}
				}
			}
		}

		return tankList;
	}

	public final AbstractUnitList getLoadableUnit(final IUnitContainer p_container)
	{
		AbstractUnitList loadableUnit = new AbstractUnitList();

		for (AbstractUnit unit : this)
		{
			if (p_container.isLoadable(unit))
			{
				loadableUnit.add(unit);
			}
		}

		return loadableUnit;
	}

	public final int getTotalSpaceCost()
	{
		final Iterator unitIter = this.iterator();
		AbstractUnit currentUnit;
		int currentLoadCost;
		int totalLoadCost = 0;
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			currentLoadCost = currentUnit.getSpaceCost();
			Assert.check(currentLoadCost != UnitInfo.NA, "This unit {0} can't be loaded", currentUnit);
			totalLoadCost += currentLoadCost;
		}

		return totalLoadCost;
	}

	/**
	 * Retourne la liste des unit�s se situant � une position donn�e.
	 * 
	 * @param p_coord
	 *            Position.
	 * 
	 * @return Liste d'unit�
	 */
	public final AbstractUnitList getUnitAt(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "Coord");

		final AbstractUnitList unitList = new AbstractUnitList();
		final Iterator unitIter = this.iterator();
		AbstractUnit currentUnit = null;
		Coord currentCoord = null;
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			currentCoord = currentUnit.getPosition();
			if (currentCoord.equals(p_coord))
			{
				unitList.add(currentUnit);
			}
		}
		return unitList;
	}

	/**
	 * Returns a new unit set based on this unit set. But, the new set contains
	 * only unit of the expected player.
	 * 
	 * @param p_player
	 *            expected player.
	 * @return A new unit set.
	 */
	public final AbstractUnitList getUnitByPlayer(final AbstractPlayer p_player)
	{
		Assert.preconditionNotNull(p_player, "Player");

		final AbstractUnitList unitList = new AbstractUnitList();
		AbstractUnit currentUnit = null;
		final Iterator iter = iterator();
		while (iter.hasNext())
		{
			currentUnit = (AbstractUnit) iter.next();
			if (currentUnit.getPlayer() == p_player)
			{
				unitList.add(currentUnit);
			}
		}
		return unitList;
	}

	public final boolean containsEnnemyUnit(final AbstractPlayer p_player)
	{
		for (AbstractUnit currentUnit : this)
		{
			if (p_player.isEnnemyPlayer(currentUnit.getPlayer()))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Retourne la premi�re unit� rencontr�e � la position attendue.
	 * 
	 * @param p_coord
	 *            Position recherch�e.
	 * 
	 * @return Unit�.
	 */
	public final AbstractUnit getFirstUnitAt(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "Coord");

		final Iterator unitIter = this.iterator();
		AbstractUnit currentUnit;
		Coord currentCoord;
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			currentCoord = currentUnit.getPosition();
			if (currentCoord.equals(p_coord))
			{
				return currentUnit;
			}
		}

		return null;
	}

	public final boolean containsGroundUnit()
	{
		return containsUnitByType(UnitTypeCst.TANK);
	}

	public final boolean containsUnitByType(final UnitTypeCst p_unitType)
	{
		AbstractUnit currentUnit = null;
		final Iterator iter = iterator();
		while (iter.hasNext())
		{
			currentUnit = (AbstractUnit) iter.next();
			if (currentUnit.getUnitType().equals(p_unitType))
			{
				return true;
			}
		}
		return false;

	}

	public final boolean containsAirUnit()
	{
		return containsUnitByType(UnitTypeCst.FIGHTER);
	}

	public final boolean containsSeaUnit()
	{
		final Iterator iter = iterator();
		AbstractUnit currentUnit;
		UnitTypeCst currentType;
		while (iter.hasNext())
		{
			currentUnit = (AbstractUnit) iter.next();
			currentType = currentUnit.getUnitType();
			if (UnitTypeCst.DESTROYER.equals(currentType) || UnitTypeCst.TRANSPORT_SHIP.equals(currentType))
			{
				return true;
			}
		}

		return false;
	}

	public final boolean hasUnitInRange(final Coord p_coord, final int p_range)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		Assert.preconditionIsPositive(p_range, "Invalid Range.");

		final Iterator unitIter = iterator();
		AbstractUnit currentUnit;
		Coord currentCoord;
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			currentCoord = currentUnit.getPosition();
			if (p_coord.calculateIntDistance(currentCoord) <= p_range)
			{
				return true;
			}
		}
		return false;
	}

	public final AbstractUnitList getUnitInRange(final Coord p_coord, final int p_range)
	{
		Assert.preconditionNotNull(p_coord, "Coord");
		Assert.preconditionIsPositive(p_range, "Invalid Range.");

		final AbstractUnitList unitList = new AbstractUnitList();
		AbstractUnit currentUnit = null;
		Coord currentCoord = null;
		final Iterator unitIter = iterator();
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			currentCoord = currentUnit.getPosition();
			if (p_coord.calculateIntDistance(currentCoord) <= p_range)
			{
				unitList.add(currentUnit);
			}
		}
		return unitList;
	}

	public final AbstractUnitList getGroundCombatUnitList()
	{
		final AbstractUnitList unitList = new AbstractUnitList();

		for (AbstractUnit unit : this)
		{
			if (UnitTypeCst.GROUND_COMBAT_UNIT_LIST.contains(unit.getUnitType()))
			{
				unitList.add(unit);
			}
		}
		return unitList;
	}

	public final AbstractUnitList getUnitInRange(final CoordSet p_coordSet, final int p_range)
	{
		Assert.preconditionNotNull(p_coordSet, "Coord Set");
		Assert.preconditionIsPositive(p_range, "Invalid range");

		final AbstractUnitList unitList = new AbstractUnitList();
		if (p_coordSet.isEmpty() || isEmpty())
		{
			return unitList;
		}

		final Iterator unitIter = iterator();
		AbstractUnit currentUnit;
		Coord currentPosition;
		Coord nearestCoord;
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			currentPosition = currentUnit.getPosition();
			nearestCoord = p_coordSet.getDirectNearestCoord(currentPosition);
			if (p_range >= currentPosition.calculateIntDistance(nearestCoord))
			{
				unitList.add(currentUnit);
			}
		}

		return unitList;
	}

	/**
	 * Retourne la plus courte distance entre un point les ennemis de la liste.
	 * La recherche de cette plus courte distance est interrompue si un ennemi
	 * est d�tect� plus proche encore.
	 * 
	 * @param p_startCoord
	 *            Point de recherche.
	 * @param p_minRangeLimit
	 *            Distance minimale recherch�e.
	 * 
	 * @return Distance la plus courte entre un point et un ennemi.
	 */
	public final int getMinimumRange(final Coord p_startCoord, final int p_minRangeLimit)
	{
		Assert.preconditionNotNull(p_startCoord, "Start Coord");
		Assert.preconditionIsPositive(p_minRangeLimit, "Range Limit");

		int bestRange = Integer.MAX_VALUE;
		int currentRange = Integer.MAX_VALUE;
		final Iterator unitIter = this.iterator();
		AbstractUnit currentUnit;
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			currentRange = currentUnit.getPosition().calculateIntDistance(p_startCoord);
			if (currentRange < bestRange)
			{
				bestRange = currentRange;
				if (bestRange < p_minRangeLimit)
				{
					bestRange = p_minRangeLimit;
					break;
				}
			}
		}

		return bestRange;
	}

	@Override
	public Object clone()
	{
		final AbstractUnitList clonedUnitList = new AbstractUnitList();
		clonedUnitList.addAll(this);

		return clonedUnitList;
	}

	public final boolean hasUnitInCoordList(final CoordSet p_coordSet)
	{
		Assert.preconditionNotNull(p_coordSet, "Coord Set");

		Coord unitCoord = null;
		AbstractUnit currentUnit = null;

		final Iterator unitIter = iterator();
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();

			unitCoord = currentUnit.getPosition();
			if (p_coordSet.contains(unitCoord))
			{
				return true;
			}
		}

		return false;

	}

	/**
	 * Returns all units where their position is contained the CoordSet.
	 * 
	 * @param p_coordSet
	 * @return
	 */
	public final AbstractUnitList getUnitByCoordList(final CoordSet p_coordSet)
	{
		Assert.preconditionNotNull(p_coordSet, "Coord Set");

		final AbstractUnitList unitList = new AbstractUnitList();
		AbstractUnit currentUnit = null;
		Coord unitCoord = null;

		final Iterator unitIter = iterator();
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			unitCoord = currentUnit.getPosition();

			if (p_coordSet.contains(unitCoord))
			{
				unitList.add(currentUnit);
			}
		}

		return unitList;
	}

	public final AbstractUnitList getReacheableEnemy(final AbstractUnit p_unit)
	{

		final AbstractUnitList enemyList = new AbstractUnitList();
		final AbstractPlayer player = p_unit.getPlayer();
		final FSMapMgr mapMgr = player.getMapMgr();
		final CoordSet coordToExclude = player.getEnemySet().getCoordSet();

		if (!UnitTypeCst.TANK.equals(p_unit.getUnitType()))
		{
			coordToExclude.addAll(mapMgr.getCityList().getEnemyCity(player).getCoordSet());
		}

		Coord enemyPosition;
		for (AbstractUnit enemy : this)
		{
			enemyPosition = enemy.getPosition();
			if (enemyPosition.calculateIntDistance(p_unit.getPosition()) <= 1)
			{
				enemyList.add(enemy);
			} else
			{
				coordToExclude.remove(enemyPosition);
				if (mapMgr.isAccessibleCoord(p_unit, enemyPosition, coordToExclude, true))
				{
					enemyList.add(enemy);
				}
				coordToExclude.add(enemyPosition);
			}
		}
		return enemyList;
	}

	/**
	 * Return all Fighters who did not move during the turn or with 0 fuel.
	 * 
	 * @return Fighter list
	 */
	public final AbstractUnitList getNonMovingFighter()
	{
		final AbstractUnitList unitList = new AbstractUnitList();
		AbstractUnit currentUnit = null;
		final Iterator unitIter = iterator();
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			if (UnitTypeCst.FIGHTER.equals(currentUnit.getUnitType()))
			{
				if (currentUnit.getMove() == currentUnit.getMovementCapacity() || currentUnit.getFuel() == 0)
				{
					unitList.add(currentUnit);

				}
			}
		}
		return unitList;
	}

	/**
	 * Indicate if the unit list contains almost 1 unit at the expected
	 * position.
	 * 
	 * @param p_coord
	 *            Expected position.
	 * @return True of false.
	 */
	public final boolean hasUnitAt(final Coord p_coord)
	{
		AbstractUnit currentUnit = null;
		final Iterator unitIter = iterator();
		while (unitIter.hasNext())
		{
			currentUnit = (AbstractUnit) unitIter.next();
			if (currentUnit.getPosition().equals(p_coord))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Build an return a new AbstractUnitList containing only unit on the sea or
	 * near to sea cells.
	 * 
	 * @param p_map
	 *            Map
	 * @return Unit on the sea or near to sea.
	 */
	public final AbstractUnitList retrieveAccessibleUnitBySea(final FSMap p_map)
	{
		Assert.preconditionNotNull(p_map, "Map");
		final AbstractUnitList unitList = new AbstractUnitList();

		AbstractUnit currentUnit = null;
		Cell currentCell = null;
		final Iterator<AbstractUnit> unitIter = iterator();
		while (unitIter.hasNext())
		{
			currentUnit = unitIter.next();
			currentCell = p_map.getCellAt(currentUnit.getPosition());
			if (currentCell.isSeaCell() || currentCell.isNearToWather())
			{
				unitList.add(currentUnit);
			}
		}

		return unitList;
	}

	public final AbstractUnitList getNonFlyingUnitList(final CityList p_cityList)
	{
		final AbstractUnitList unitList = new AbstractUnitList();
		City currentCity;
		for (AbstractUnit unit : this)
		{
			if (unit.getUnitType().equals(UnitTypeCst.FIGHTER))
			{
				if (unit.hasPath())
				{
					continue;
				}

				if (!p_cityList.hasCityAtPosition(unit.getPosition()))
				{
					continue;
				} else
				{
					currentCity = p_cityList.getCityAtPosition(unit.getPosition());
					if (currentCity.getPlayer() != unit.getPlayer())
					{
						continue;
					}
				}
				// Fighter in a city without path
				// is considered as under the ground (impacted by bombardment)
			} else if (unit.getUnitType().equals(UnitTypeCst.SUBMARIN))
			{
				continue;
			}

			unitList.add(unit);
		}
		return unitList;
	}

	@Override
	public final String toString()
	{
		return FSUtil.toStringList(SHORT_NAME, this);
	}

	public final synchronized AbstractUnitList getUnitMarkedAsFinished()
	{
		final AbstractUnitList unitList = new AbstractUnitList();
		for (AbstractUnit currentUnit : this)
		{
			if (currentUnit.isTurnCompleted())
			{
				unitList.add(currentUnit);
			}
		}
		return unitList;
	}

	public final synchronized AbstractUnitList getUnitNotMarkedAsFinished()
	{
		final AbstractUnitList unitList = new AbstractUnitList();
		for (AbstractUnit currentUnit : this)
		{
			if (!currentUnit.isTurnCompleted())
			{
				unitList.add(currentUnit);
			}
		}
		return unitList;
	}

	public final AbstractUnitList getUnitsByAreaName(final FSMap p_map, final String p_areaName)
	{
		final AbstractUnitList units = new AbstractUnitList();

		Area currentArea;
		for (AbstractUnit unit : this)
		{
			currentArea = p_map.getAreaAt(unit.getPosition(), AreaTypeCst.GROUND_AREA);
			if (currentArea != null && p_areaName.equals(currentArea.getInternalAreaName()))
			{
				units.add(unit);
			}
		}

		return units;
	}
}
