/*
 * Created on Mar 26, 2005
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.tools.data_type.cst.IntCst;

/**
 * @author Jocelyn Chaumel
 */
public class CombatResultCst extends IntCst
{

	public final static CombatResultCst ATTACKER_WIN = new CombatResultCst(1, "Attacker win");
	public final static CombatResultCst DEFENSER_WIN = new CombatResultCst(2, "Defenser win");
	public final static CombatResultCst ATT_AND_DEF_LOOSE = new CombatResultCst(3, "Attack & Defenser lose");
	public final static CombatResultCst NO_WIN = new CombatResultCst(4, "Attack & Defenser leave");
	public final static CombatResultCst NO_COMBAT = new CombatResultCst(5, "No Combat");

	private CombatResultCst(final int aConstantValue, final String p_label)
	{
		super(aConstantValue, p_label);
	}

	public final boolean isAlive()
	{
		if (this == ATTACKER_WIN || this == NO_WIN || this == NO_COMBAT)
		{
			return true;
		}
		return false;
	}
}
