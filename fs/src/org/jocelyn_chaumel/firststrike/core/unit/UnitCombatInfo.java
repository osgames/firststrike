/*
 * CombatProperty.java Created on 28 avril 2003, 12:27
 */

package org.jocelyn_chaumel.firststrike.core.unit;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe d�finit un �l�ment du tableau contenant l'ensemble des
 * caract�risques de combat des diff�rentes unit�es existantes dans le jeu.
 * 
 * @author Jocelyn Chaumel
 */
public final class UnitCombatInfo implements Serializable
{
	public final static UnitCombatInfo TANK_COMBAT_INFO = new UnitCombatInfo(20, 110, 10, 50, 110, 2, 1, 0, 0, 0, 0);
	public final static UnitCombatInfo FIGHTER_COMBAT_INFO = new UnitCombatInfo(30, 135, 5, 20, 50, 2, 1, 0, 0, 0, 0);
	public final static UnitCombatInfo TRANSPORT_SHIP_COMBAT_INFO = new UnitCombatInfo(0, 0, 0, 0, 70, 0, 0, 0, 0, 0, 0);
	public final static UnitCombatInfo DESTROYER_COMBAT_INFO = new UnitCombatInfo(	30,
																					150,
																					10,
																					52,
																					300,
																					2,
																					2,
																					0,
																					0,
																					0,
																					0);
	public final static UnitCombatInfo BATTLESHIP_COMBAT_INFO = new UnitCombatInfo(	50,
																					180,
																					10,
																					60,
																					650,
																					2,
																					3,
																					50,
																					200,
																					1,
																					10);
	public final static UnitCombatInfo ARTILLERY_COMBAT_INFO = new UnitCombatInfo(0, 0, 0, 0, 100, 0, 0, 75, 300, 1, 12);
	public final static UnitCombatInfo RADAR_COMBAT_INFO = new UnitCombatInfo(0, 0, 0, 0, 150, 0, 0, 0, 0, 0, 0);

	private final int _minAttack;
	private final int _maxAttack;
	private final int _minDefense;
	private final int _maxDefense;
	private final int _hitPointsCapacity;
	private final int _nbAttackCapacity;
	private final int _nbDefenseCapacity;
	private final int _minBombardmentAttack;
	private final int _maxBombardmentAttack;
	private final int _nbBombardmentCapacity;
	private final int _bombardmentRange;

	/**
	 * Creates a new instance of CombatProperty
	 */
	private UnitCombatInfo(	final int p_minAttack,
							final int p_maxAttack,
							final int p_minDefense,
							final int p_maxDefense,
							final int p_hitPointsCapacity,
							final int p_nbAttackCapacity,
							final int p_nbDefenceCapacity,
							final int p_minBombardmentAttack,
							final int p_maxBombardmentAttack,
							final int p_nbBombardmentCapacity,
							final int p_bombardmentRange)
	{
		_minAttack = p_minAttack;
		_maxAttack = p_maxAttack;
		_minDefense = p_minDefense;
		_maxDefense = p_maxDefense;
		_hitPointsCapacity = p_hitPointsCapacity;
		_nbAttackCapacity = p_nbAttackCapacity;
		_nbDefenseCapacity = p_nbDefenceCapacity;
		_minBombardmentAttack = p_minBombardmentAttack;
		_maxBombardmentAttack = p_maxBombardmentAttack;
		_nbBombardmentCapacity = p_nbBombardmentCapacity;
		_bombardmentRange = p_bombardmentRange;
	}

	public final int getMinAttack()
	{
		return _minAttack;
	}

	public final int getMaxAttack()
	{
		return _maxAttack;
	}

	public final int getNbAttackCapacity()
	{
		return _nbAttackCapacity;
	}

	// defense
	public final int getMinDefense()
	{
		return _minDefense;
	}

	public final int getMaxDefense()
	{
		return _maxDefense;
	}

	public final int getNbDefenseCapacity()
	{
		return _nbDefenseCapacity;
	}

	public final int getHitPointsCapacity()
	{
		return _hitPointsCapacity;
	}

	// Bombardment
	public final int getMinBombardmentAttack()
	{
		return _minBombardmentAttack;
	}

	public final int getMaxBombardmentAttack()
	{
		return _maxBombardmentAttack;
	}

	public final int getNbBombardmentCapacity()
	{
		return _nbBombardmentCapacity;
	}

	public final int getBombardmentRange()
	{
		return _bombardmentRange;
	}
	
	public static final UnitCombatInfo getInstance(final UnitTypeCst p_unitTypeCst)
	{
		Assert.preconditionNotNull(p_unitTypeCst);
		
		if (UnitTypeCst.TANK.equals(p_unitTypeCst)) {
			return UnitCombatInfo.TANK_COMBAT_INFO;
		} else if (UnitTypeCst.ARTILLERY.equals(p_unitTypeCst))
		{
			return UnitCombatInfo.ARTILLERY_COMBAT_INFO;
		} else if (UnitTypeCst.FIGHTER.equals(p_unitTypeCst))
		{
			return UnitCombatInfo.FIGHTER_COMBAT_INFO;
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_unitTypeCst))
		{
			return UnitCombatInfo.TRANSPORT_SHIP_COMBAT_INFO;
		} else if (UnitTypeCst.DESTROYER.equals(p_unitTypeCst))
		{
			return UnitCombatInfo.DESTROYER_COMBAT_INFO;
		} else if (UnitTypeCst.BATTLESHIP.equals(p_unitTypeCst))
		{
			return UnitCombatInfo.BATTLESHIP_COMBAT_INFO;
		} else if (UnitTypeCst.RADAR.equals(p_unitTypeCst))
		{
			return UnitCombatInfo.RADAR_COMBAT_INFO;
		} 
		
		throw new FSFatalException("Unsupported Unit Type : " + p_unitTypeCst.getLabel());
	}
}