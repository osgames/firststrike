package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.core.map.AbstractMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.debugging.Assert;

public abstract class AbstractGroundLoadableUnit extends AbstractUnit implements ILoadableUnit
{
	protected IUnitContainer _transportContainer = null;
	
	public AbstractGroundLoadableUnit(final UnitInfo p_unitInfo,
	          						final UnitCombatInfo p_unitCombatInfo,
	          						final AbstractMoveCost p_moveCost,
	        						final Coord p_positionCoord,
	        						final AbstractPlayer p_player)
	{
		super(p_unitInfo, p_unitCombatInfo, p_moveCost, p_positionCoord, p_player);
	}

	@Override
	public final boolean isLoaded()
	{
		return _transportContainer != null;
	}

	@Override
	public IUnitContainer getContainer()
	{
		return _transportContainer;
	}

	@Override
	public MoveStatusCst loadInContainer(final IUnitContainer p_unitContainer)
	{
		Assert.preconditionNotNull(p_unitContainer, "Transport Ship");
		Assert.precondition(p_unitContainer.isLoadable(this), "Unloadable unit detected");
		Assert.check(_transportContainer == null, "Tank already loaded");

		final Coord unitCoord = getPosition();
		final Coord containerCoord = ((AbstractUnit)p_unitContainer).getPosition();
		final int distance = unitCoord.calculateIntDistance(containerCoord);
		Assert.precondition(distance <= 1, "Tank too far from the transport");

		p_unitContainer.loadUnit(this);
		setMove(0);
		_transportContainer = p_unitContainer;

		return MoveStatusCst.MOVE_SUCCESFULL;
	}

	@Override
	public MoveStatusCst unloadFromContainer(Coord p_targetCoord)
	{
		Assert.preconditionNotNull(p_targetCoord, "Unloading Coord");
		Assert.check(isLoaded(), "This tank is not loaded");

		if (getMove() != getMovementCapacity())
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		
		final MoveStatusCst moveStatus;
		if (p_targetCoord.equals(getPosition()))
		{
			moveStatus = MoveStatusCst.MOVE_SUCCESFULL;
		} else
		{
			final CoordList coordSet = new CoordList();
			coordSet.add(p_targetCoord);
			final Path path = new Path(coordSet);
			setPath(path);
			moveStatus = move();
		}
		
		if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
		{
			_transportContainer.unloadUnit(this);
			_transportContainer = null;
			setMove(0);
		}

		return moveStatus;
	}
}
