package org.jocelyn_chaumel.firststrike.core.unit;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.FSRandom;

public final class Bombardment implements Serializable
{
	final static FSRandom _randomizer = FSRandom.getInstance();

	private final int _minDamage;
	private final int _maxDamage;
	private final float _damageFactor;

	public Bombardment(final int p_minDamage, final int p_maxDamage, final float p_damageFactor)
	{
		_minDamage = p_minDamage;
		_maxDamage = p_maxDamage;
		_damageFactor = p_damageFactor;
	}

	public final int getRandomDamage()
	{
		int minimumDamage = getMinDamage();
		int variableDamage = Math.round((_maxDamage - _minDamage) * _damageFactor * _randomizer.getRandomFloat());
		return minimumDamage + variableDamage;
	}
	
	public final int getMinDamage()
	{
		return Math.round((_minDamage * _damageFactor));
	}
	
	public final int getAverageDamage()
	{
		int minimumDamage = getMinDamage();
		int variableDamage = Math.round((_maxDamage - _minDamage) * _damageFactor * 0.5f);
		return minimumDamage + variableDamage;
	}

}
