/*
 * Created on Jun 30, 2005
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public final class EnemyToDestroy implements Comparable
{

	private final AbstractUnit _enemyRef;

	private double _destructionPriority;

	public EnemyToDestroy(final AbstractUnit p_unitRef, final double p_destructionPriority)
	{
		Assert.preconditionNotNull(p_unitRef, "Enemy Ref");
		Assert.precondition(p_destructionPriority >= 0, "Invalid Destruction Priority");

		_enemyRef = p_unitRef;
		_destructionPriority = p_destructionPriority;
	}

	@Override
	public int compareTo(Object p_enemy)
	{
		Assert.preconditionNotNull(p_enemy, "Enemy To Destroy");
		Assert.precondition(p_enemy instanceof EnemyToDestroy, "Invalid EnemyToDestroy Class Instance");

		return Double.compare(((EnemyToDestroy) p_enemy)._destructionPriority, _destructionPriority);
	}

	public final AbstractUnit getEnemyRef()
	{
		return _enemyRef;
	}

	public final Coord getPosition()
	{
		return _enemyRef.getPosition();
	}

	public final double getDestructionPriority()
	{
		return _destructionPriority;
	}

	public final void setDestructionPriority(final int p_destructionPriority)
	{
		Assert.precondition(p_destructionPriority >= 0, "Invalid Destruction Priority");

		_destructionPriority = p_destructionPriority;
	}

	@Override
	public final String toString()
	{
		final StringBuffer sb = new StringBuffer();
		sb.append("{EnemyToDestroy=");
		sb.append("priority:").append(_destructionPriority);
		sb.append(", enemy:").append(_enemyRef);
		sb.append("}");
		return sb.toString();
	}
}