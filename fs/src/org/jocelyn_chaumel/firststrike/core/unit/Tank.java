/*
 * ComputerUnit.java Created on 14 juin 2003, 13:31
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.map.GroundMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public class Tank extends AbstractGroundLoadableUnit 
{
	public static final GroundMoveCost TANK_MOVE_COST = (GroundMoveCost) MoveCostFactory
			.getMoveCostInstance(UnitInfo.TANK_INFO.getUnitType());

	/**
	 * Constructor.
	 * 
	 * @param p_positionCoord
	 * @param p_player
	 */
	public Tank(final Coord p_positionCoord, final AbstractPlayer p_player)
	{
		super(UnitInfo.TANK_INFO, UnitCombatInfo.TANK_COMBAT_INFO, TANK_MOVE_COST, p_positionCoord, p_player);
	}


	@Override
	public final int getMinDefense()
	{
		int minDef = super.getMinDefense();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				minDef = minDef + 5;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				minDef = minDef - 5;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				minDef = minDef - 5;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				minDef = minDef + 15;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return minDef;
	}

	@Override
	public final int getDetectionRange()
	{
		int range = super.getDetectionRange();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				// NOP;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP;
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				range++;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return range;
	}
}