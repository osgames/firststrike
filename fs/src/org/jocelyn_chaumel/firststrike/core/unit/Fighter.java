/*
 * Created on Jan 2, 2005
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.map.AirPlaneMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class Fighter extends AbstractUnit
{
	private static final AirPlaneMoveCost _airPlaneMoveCost = (AirPlaneMoveCost) MoveCostFactory
			.getMoveCostInstance(UnitInfo.FIGHTER_INFO.getUnitType());

	/**
	 * Constructor.
	 * 
	 * @param p_positionCoord
	 * @param p_player
	 */
	public Fighter(final Coord p_positionCoord, final AbstractPlayer p_player)
	{
		// Les instructions assert sont effectuées dans le constructeur.
		super(UnitInfo.FIGHTER_INFO, UnitCombatInfo.FIGHTER_COMBAT_INFO, _airPlaneMoveCost, p_positionCoord, p_player);
	}


	@Override
	protected String internalToString()
	{
		final StringBuffer sb = new StringBuffer(", fuel:");
		sb.append(getFuel());

		return sb.toString();
	}

	@Override
	public final int getDetectionRange()
	{
		int range = super.getDetectionRange();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				range--;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				range++;
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				range *= 2;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return range;
	}

	@Override
	public final int getMinDefense()
	{
		int minDef = super.getMinDefense();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				minDef = minDef + 5;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				// NOP;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				// NOP;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				minDef = minDef + 10;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return minDef;
	}

	@Override
	public final int getMaxDefense()
	{
		int maxDef = super.getMaxDefense();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				maxDef = maxDef + 5;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				// NOP;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				// NOP;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				maxDef = maxDef + 10;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return maxDef;
	}

	@Override
	public final int getMaxAttack()
	{
		int maxAttack = super.getMaxAttack();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				maxAttack = maxAttack + 10;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				maxAttack = maxAttack - 5;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				maxAttack = maxAttack - 10;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				maxAttack = maxAttack + 10;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return maxAttack;
	}
	
	@Override
	public final MoveStatusCst move()
	{
		Assert.precondition(getFuel() > 0, "No enough fuel");
		final MoveStatusCst moveStatus =  super.move();

		if (getPlayer().getCitySet().hasCityAtPosition(getPosition()))
		{
			this.restoreFuel();
		}
		
		return moveStatus;
	}
	
	@Override
	public final void restoreCityUnit()
	{
		super.restoreCityUnit();
		restoreFuel();
	}

	public final void restoreFuel()
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");

		setFuel(_unitInfo.getMaxFuelCapacity());
	}
}