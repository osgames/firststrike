package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class ComputerTransportShip extends TransportShip
{

	private Area _assignedArea = null;
	
	public ComputerTransportShip(final Coord p_positionCoord, final AbstractPlayer p_playerId)
	{
		super(p_positionCoord, p_playerId);
	}
	
	public final boolean isAssignedToArea()
	{
		return _assignedArea != null;
	}
	
	public final Area getAssignedArea()
	{
		return _assignedArea;
	}
	
	public final void setAssignedArea(final Area p_area)
	{
		Assert.preconditionNotNull(p_area);
		_assignedArea = p_area;
	}

}
