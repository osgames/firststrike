package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.tools.data_type.cst.IntCst;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class MoveStatusCst extends IntCst
{
	public static final MoveStatusCst MOVE_SUCCESFULL = new MoveStatusCst(1, "Move Succesfull");
	public static final MoveStatusCst NOT_ENOUGH_FUEL_PTS = new MoveStatusCst(2, "Not Enough Fuel Pts");
	public static final MoveStatusCst NOT_ENOUGH_MOVE_PTS = new MoveStatusCst(3, "Not Enough Move Pts");
	public static final MoveStatusCst ENEMY_DETECTED = new MoveStatusCst(4, "Enemy Detected");
	public static final MoveStatusCst UNIT_DEFEATED = new MoveStatusCst(5, "Unit Defeated");

	protected MoveStatusCst(final int p_value, final String p_label)
	{
		super(p_value, p_label);
	}

	/**
	 * Détermine si l'unité est toujours vivantes en fonction du résultat de
	 * déplacement
	 * 
	 * @param p_moveStatus
	 * @return
	 */
	public static boolean isAlive(final MoveStatusCst p_moveStatus)
	{
		Assert.preconditionNotNull(p_moveStatus, "Move Status");

		if (p_moveStatus != NOT_ENOUGH_FUEL_PTS && p_moveStatus != UNIT_DEFEATED)
		{
			return true;
		}

		return false;
	}

}
