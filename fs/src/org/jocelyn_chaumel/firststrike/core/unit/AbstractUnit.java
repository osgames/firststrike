/*
 * AbstractUnit.java Created on 14 juin 2003, 12:52
 */

package org.jocelyn_chaumel.firststrike.core.unit;

import java.io.Serializable;
import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.IDetection;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.AbstractMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.data_type.SingleAttackResult;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.data_type.Id;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 *  
 */
public abstract class AbstractUnit implements IDetection, Serializable
{
	private static final FSLogger _logger = FSLoggerManager.getLogger(AbstractUnit.class);

	private UnitId _unitId;

	private String _unitName = null;

	protected UnitInfo _unitInfo;
	protected UnitCombatInfo _unitCombatInfo;
	protected final AbstractMoveCost _moveCost;
	private int _fuel;

	private int _hitpoints = 1;

	private int _move;

	private int _nbAttack;

	private int _nbDefense;

	private Path _path;
	
	private Coord _patrolCoord = null;

	private AbstractPlayer _player;

	private Coord _position;
	private boolean _turnCompleted = false;

	protected final FSRandom randomizer = FSRandom.getInstance();
	private final ArrayList<AbstractUnitDirective> _directiveList = new ArrayList<AbstractUnitDirective>();

	/**
	 * @param p_unitInfo
	 *            Information sur l'unit�.
	 * @param p_unitCombatInfo
	 *            Information de combat sur l'unit�.
	 * @param p_positionCoord
	 *            Position de l'unit�.
	 * @param p_player
	 *            Indentifiant du joueur � qui appartient l'unit�.
	 * @param p_unitIndex
	 *            Unit index.
	 * @param p_unitController
	 *            Controleur de l'unit�.
	 */
	public AbstractUnit(final UnitInfo p_unitInfo,
						final UnitCombatInfo p_unitCombatInfo,
						final AbstractMoveCost p_moveCost,
						final Coord p_positionCoord,
						final AbstractPlayer p_player)
	{
		Assert.preconditionNotNull(p_unitInfo, "Unit Info");
		Assert.preconditionNotNull(p_unitCombatInfo, " Unit Combat Info");
		Assert.preconditionNotNull(p_moveCost, "Move Cost");
		Assert.preconditionNotNull(p_positionCoord, "Position");
		Assert.preconditionNotNull(p_player, "Player");

		_unitInfo = p_unitInfo;
		_unitCombatInfo = p_unitCombatInfo;
		_moveCost = p_moveCost;
		_player = p_player;
		_unitId = _player.getNewUnitId();

		setPosition(p_positionCoord);
		restoreUnit();
		restoreCityUnit();
	}

	public void addDirective(final AbstractUnitDirective p_directive)
	{
		Assert.preconditionNotNull(p_directive, "Directive");

		_directiveList.add(p_directive);
	}

	public final ArrayList<AbstractUnitDirective> getDirectiveList()
	{
		return _directiveList;
	}

	public final void markAsTurnCompleted()
	{
		_turnCompleted = true;
	}

	public final void resetTurnCompletedFlag()
	{
		_turnCompleted = false;
	}

	public final boolean isTurnCompleted()
	{
		return _turnCompleted;
	}
	
	public final void setPatrolCoord(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord);
		_patrolCoord = p_coord;
	}
	
	public final void resetPatrolCoord()
	{
		_patrolCoord = null;
	}
	
	public final Coord getPatrolCoord()
	{
		return _patrolCoord;
	}

	/**
	 * Indicate if the unit is seriously damaged.
	 * 
	 * @return
	 */
	public final boolean isDamaged()
	{

		if (getPlayer().getPlayerLevel().equals(PlayerLevelCst.EASY_LEVEL))
		{
			return false;
		}

		boolean isDamaged = false;
		final float ratio = getHitPoints() / (float) getHitPointsCapacity();

		if (ratio < 0.5f)
		{
			isDamaged = true;
		}
		return isDamaged;
	}

	public final MoveStatusCst playDirectives()
	{
		_logger.playerSubActionDetail("Player plays this unit {0}", this);
		_logger.unitDetail("Nb directive for this unit : " + _directiveList.size());
		if (_directiveList.isEmpty())
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		if (_move == 0 || (getFuelCapacity() > 0 && _fuel == 0))
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}

		MoveStatusCst moveStatus = null;
		try
		{
			AbstractUnitDirective currentDirective = null;
			while (!_directiveList.isEmpty())
			{
				currentDirective = _directiveList.get(0);
				_logger.unitSubAction("Play directive : " + currentDirective);
				moveStatus = currentDirective.play();
				if (MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus))
				{
					_directiveList.remove(0);
					if (getMove() == 0)
					{
						return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
					}
				} else
				{
					return moveStatus;
				}
			}
		} catch (PathNotFoundException ex)
		{
			throw new FSFatalException(ex);
		}
		return MoveStatusCst.MOVE_SUCCESFULL;
	}

	public final void setUnitName(final String p_name)
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");
		_unitName = p_name;
	}

	public final String getUnitName()
	{
		return _unitName;
	}

	public final String getBestUnitName()
	{
		if (_unitName == null)
		{
			return _unitId.toString();
		}

		final StringBuffer sb = new StringBuffer();
		sb.append(_unitName);
		sb.append("(");
		sb.append(_unitId.toString());
		sb.append(")");
		return sb.toString();
	}

	public final UnitId getUnitId()
	{
		return _unitId;
	}

	public final Id getId()
	{
		return _unitId;
	}

	@Override
	public final AbstractPlayer getPlayer()
	{
		return _player;
	}

	public void setPosition(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord);
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");

		Assert.precondition(!_player.getUnitIndex().getNonNullUnitListAt(p_coord).containsEnnemyUnit(_player),
							"Position containing ennemy unit");
		_position = p_coord;
	}

	@Override
	public final Coord getPosition()
	{
		return _position;
	}

	public final int getMove()
	{
		return _move;
	}

	/**
	 * Restore move of the unit & move of its contained unit.
	 */
	public void restoreMove()
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");

		setMove(_unitInfo.getMaxMovementCapacity());
		if (_path != null)
		{
			restoreTurnCost(_player.getMapMgr().getMap());
		}
	}

	public final void restoreTurnCost(final FSMap p_map)
	{
		if (_path == null)
		{
			return;
		}

		_path.calculateTurnCost(p_map, getMoveCost(), _move, getMovementCapacity());
	}

	public final boolean isLiving()
	{
		return _hitpoints > 0;
	}

	public void restoreUnit()
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");

		resetTurnCompletedFlag();

		restoreNbAttack();
		restoreNbDefence();
		restoreMove();
		if (_player.getCitySet().hasCityAtPosition(_position))
		{
			restoreCityUnit();
		}
	}

	public void restoreCityUnit()
	{
		restoreHitPoints();
	}

	public final void restoreHitPoints()
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");
		_hitpoints = getHitPointsCapacity();
	}

	public final void restoreNbAttack()
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");
		_nbAttack = getNbAttackCapacity();
	}

	public final void restoreNbDefence()
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");
		_nbDefense = getNbDefenseCapacity();
	}

	public final int getFuel()
	{
		return _fuel;
	}

	public final int getFuelCapacity()
	{
		return _unitInfo.getMaxFuelCapacity();
	}

	public final int getMovementCapacity()
	{
		return _unitInfo.getMaxMovementCapacity();
	}

	public final UnitTypeCst getUnitType()
	{
		return _unitInfo.getUnitType();
	}

	public final int getHitPoints()
	{
		return _hitpoints;
	}

	public final int getHitPointsCapacity()
	{
		return _unitCombatInfo.getHitPointsCapacity();
	}

	// Attack
	public final int getNbAttackCapacity()
	{
		return _unitCombatInfo.getNbAttackCapacity();
	}

	public int getMaxAttack()
	{
		return _unitCombatInfo.getMaxAttack();
	}

	public final int getNbAttack()
	{
		return _nbAttack;
	}

	public int getMinAttack()
	{
		return _unitCombatInfo.getMinAttack();
	}

	// Defense
	public final int getNbDefenseCapacity()
	{
		return _unitCombatInfo.getNbDefenseCapacity();
	}

	public final int getNbDefense()
	{
		return _nbDefense;
	}

	public int getMaxDefense()
	{
		return _unitCombatInfo.getMaxDefense();
	}

	public int getMinDefense()
	{
		return _unitCombatInfo.getMinDefense();
	}

	public void setPath(final Path p_path)
	{
		Assert.preconditionNotNull(p_path, "Path");
		Assert.precondition(!p_path.isEmpty(), "This path is empty");
		Assert.precondition(p_path.getNextStepWithoutConsume().calculateIntDistance(_position) == 1, "Invalid Path");
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");

		if (_path != null)
		{
			_path.clear();
		}
		_path = p_path;
	}

	public final void clearPath()
	{
		if (_path != null)
		{
			_path.clear();
			_path = null;
		}
	}

	public final boolean hasPath()
	{
		if (_path != null)
		{
			Assert.check(!_path.isEmpty(), "Invalid state : path should be set to null");
		}
		return _path != null;
	}

	public void refreshTurnCost(final FSMap p_map)
	{
		if (_path == null)
		{
			return;
		}

		_path.calculateTurnCost(p_map, getMoveCost(), getMove(), getMovementCapacity());
	}

	public final Path getPath()
	{
		Assert.check(_path != null, "No path to return");

		return _path;
	}

	public SingleAttackResult attackEnnemy(final AbstractUnit p_enemy)
	{
		Assert.preconditionNotNull(p_enemy, "Enemy");
		Assert.precondition(_nbAttack > 0, "No attack available");
		Assert.precondition(_position.calculateIntDistance(p_enemy.getPosition()) == 1,
							"Distance between unit is invalid");
		Assert.precondition(_player != p_enemy.getPlayer(), "Invalid Enemy Unit");

		final int defense;
		if (p_enemy.getNbDefense() == 0)
		{
			defense = 0;
		} else
		{
			final int minDef = p_enemy.getMinDefense();
			final int defenseVariation = p_enemy.getMaxDefense() - minDef;
			p_enemy.decreaseNbDefence();
			defense = Math.round(defenseVariation * randomizer.getRandomFloat()) + minDef;
		}

		final int minAttack = getMinAttack();
		final int attackVariation = getMaxAttack() - minAttack;
		final int attack = Math.round(attackVariation * randomizer.getRandomFloat()) + minAttack;
		decreaseNbAttack();
		_logger.unitDetail("Attack: " + attack);
		_logger.unitDetail("Defense: " + defense);

		final int attackerHitpoint = _hitpoints - defense;
		final int defenserHitpoint = p_enemy.getHitPoints() - attack;
		setHitPoints(attackerHitpoint);
		p_enemy.setHitPoints(defenserHitpoint);

		final CombatResultCst combatResult;
		if (attackerHitpoint <= 0 && defenserHitpoint <= 0)
		{
			_player.killUnit(this);
			p_enemy.getPlayer().killUnit(p_enemy);
			combatResult = CombatResultCst.ATT_AND_DEF_LOOSE;
		} else if (attackerHitpoint <= 0)
		{
			_player.killUnit(this);
			combatResult = CombatResultCst.DEFENSER_WIN;
		} else if (defenserHitpoint <= 0)
		{
			p_enemy.getPlayer().killUnit(p_enemy);
			combatResult = CombatResultCst.ATTACKER_WIN;
		} else
		{
			combatResult = CombatResultCst.NO_WIN;
		}

		_logger.unitDetail("Attack result : " + combatResult);

		SingleAttackResult attackResult = null;
		if (combatResult == CombatResultCst.ATTACKER_WIN)
		{
			attackResult = new SingleAttackResult(this, attack, defense);
		} else if (combatResult == CombatResultCst.DEFENSER_WIN)
		{
			attackResult = new SingleAttackResult(attack, defense, p_enemy);
		} else if (combatResult == CombatResultCst.ATT_AND_DEF_LOOSE)
		{
			attackResult = new SingleAttackResult(attack, defense);
		} else if (combatResult == CombatResultCst.NO_WIN)
		{
			attackResult = new SingleAttackResult(this, attack, defense, p_enemy);
		} else
		{
			Assert.check(false, "Unsupported Combat Result");
		}

		Assert.postcondition(attackResult != null, "Null Attack Result Ref");

		return attackResult;
	}

	/**
	 * BE CAREFULL: THIS METHOD MUST BE CALLED ONLY BY THE AbstractPlayer CLASS.
	 * Some event & updating data process are not properly managed by this
	 * method.
	 */
	public MoveStatusCst move()
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");
		Assert.check(_move > 0, "No more movement point");
		Assert.check(hasPath(), "No Path defined");

		final Path path = getPath();

		final CoordSet enemyCoordSet = _player.getEnemySet().getCoordSet();
		final Coord nextPosition = path.getNextStepWithoutConsume();
		Assert.check(!enemyCoordSet.contains(nextPosition), "Enemy at destination position");
		Assert.check(nextPosition.calculateIntDistance(_position) == 1, "Invalid next coord destination");

		final Cell cell = _player.getMapMgr().getMap().getCellAt(nextPosition);

		final int costMove = getMoveCost(cell);
		Assert.check(costMove >= 0, "Invalid Destination Position");

		// Est-ce qu'elle poss�de suffissament de points de mouvement?
		if (_move < costMove)
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
		path.getNextStep();
		// Est-ce qu'elle utilise du carburant?
		if (getFuelCapacity() > 0)
		{
			_fuel -= costMove;
		}

		setMove(_move - costMove);
		_turnCompleted = _move == 0;
		setPosition(nextPosition);

		if (!_path.hasNextStep())
		{
			_path = null;
		}

		return MoveStatusCst.MOVE_SUCCESFULL;
	}

	/**
	 * Indique si l'unit� peut d�tecter l'unit� ennemie.
	 * 
	 * @param p_enemyUnit
	 *            Unit� ennemie pouvant �tre d�tect�e.
	 * @return Vrai lorsque l'unit� ennemie est d�tect�e par l'unit� courante.
	 */
	@Override
	public boolean detect(final AbstractUnit p_enemyUnit)
	{
		Assert.preconditionNotNull(p_enemyUnit, "Enemy Unit");
		Assert.precondition(p_enemyUnit.getPlayer() != getPlayer(), "Incoherent Player");
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");

		final boolean result = p_enemyUnit.getPosition().calculateIntDistance(getPosition()) <= getDetectionRange();
		return result;
	}

	@Override
	public int getDetectionRange()
	{
		return _unitInfo.getDetectionRange();
	}

	public AbstractMoveCost getMoveCost()
	{
		return _moveCost;
	}
	
	public int getMoveCost(final Cell p_cell)
	{
		Assert.preconditionNotNull(p_cell, "Cell");

		return _moveCost.getCost(p_cell);
	}
	

	public void setHitPoints(final int p_value)
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");
		Assert.precondition(p_value <= getHitPointsCapacity(), "Hit Points too high");
		_hitpoints = p_value;

	}

	void setFuel(final int p_fuel)
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");
		_fuel = p_fuel;
	}

	public void setMove(final int p_move)
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");
		
		if (p_move < getMoveCost().getMinCost())
		{
			_move = 0;
			_turnCompleted = true;
		} else {
			_move = p_move;
		}
	}

	void setNbAttack(final int p_nbAttack)
	{
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");
		_nbAttack = p_nbAttack;
	}

	protected Object clone(final AbstractUnit p_newInstance)
	{
		Assert.preconditionNotNull(p_newInstance, "New Instance");
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");
		if (hasPath())
		{
			p_newInstance.setPath((Path) getPath().clone());
		}

		p_newInstance.setFuel(getFuel());
		p_newInstance.setHitPoints(getHitPoints());
		p_newInstance.setMove(getMove());

		return p_newInstance;
	}

	public void decreaseNbAttack()
	{
		Assert.check(_nbAttack > 0, "Not enough nb attack");
		Assert.check(_move > 0, "Not enough movement point");
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");

		--_nbAttack;

		if (_nbAttack == 0)
		{
			setMove(0);
		} else
		{
			setMove(Math.max(0, _move - getMoveCost().getMinCost()));
		}
	}

	public void decreaseNbDefence()
	{
		Assert.check(_nbDefense > 0, "Not enough nb defence");
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");
		--_nbDefense;
	}

	public void decreaseFuel()
	{
		Assert.check(_fuel > 0, "Not enough fuel point");
		Assert.check(isLiving(), "Invalid command, this unit is destroyed.");

		
		_fuel -= MoveCostFactory.AIR_MOVE_COST.getMinCost();
	}

	@Override
	public final String toString()
	{
		StringBuilder stringBuffer = new StringBuilder();

		stringBuffer.append("{UNIT= ");
		stringBuffer.append(_unitInfo.getUnitType().getLabel()).append(", ");
		stringBuffer.append("id:").append(_unitId.getId()).append(", ");
		stringBuffer.append("player:").append(_player.getId()).append(", ");
		stringBuffer.append(_position).append(", ");
		stringBuffer.append("move:").append(_move);
		stringBuffer.append(internalToString());
		stringBuffer.append("}");

		return stringBuffer.toString();
	}

	protected String internalToString()
	{
		return "";
	}

	/**
	 * Identifie l'ennemi la meilleure unit� � d�truire d'un point de vue
	 * strat�gique en prenant en compte la distance, la difficilt�, son contenu
	 * (s'il y a lieu) et plus encore.
	 * 
	 * @param p_enemyList
	 * @return
	 */
	public AbstractUnit chooseBestEnemyTargetFromList(final AbstractUnitList p_enemyList)
	{
		Assert.preconditionNotNull(p_enemyList);
		Assert.precondition(p_enemyList.size() > 0, "Empty Enemy List");
		Assert.check(getNbAttack() >= 1, "Invalid Nbr of Attack");

		final FSMapMgr mapMgr = _player.getMapMgr();
		final CoordSet coordToExcludeList = this.getPlayer().getEnemySet().getCoordSet();
		if (!UnitTypeCst.TANK.equals(_unitInfo.getUnitType()))
		{
			coordToExcludeList.addAll(mapMgr.getCityList().getEnemyCity(_player).getCoordSet());
		}

		final int productionTime = City.getProductionTimeByType(getUnitType());
		int enemyProductionTime, distance;

		final float averageSingleAttack = (getMaxAttack() + getMinAttack()) / 2f;
		float averageDefence;
		Path currentPath;
		final float hitPoints = getHitPoints();
		float currentFactor, distanceFact, killFact, autoDestructionFact, productionFact;
		float bestFactor = Float.NEGATIVE_INFINITY;
		int averageNbAttackToKill, nbAttackToBeDone;
		AbstractUnit bestEnemy = null;
		Coord ennemyPosition;

		for (AbstractUnit enemy : p_enemyList)
		{

			// Calcul de la distance entre l'unit� et l'ennemi
			ennemyPosition = enemy.getPosition();
			if (ennemyPosition.calculateIntDistance(_position) <= 1)
			{
				distance = 0;
			} else
			{
				coordToExcludeList.remove(ennemyPosition);
				try
				{
					currentPath = mapMgr.getPath(enemy.getPosition(), coordToExcludeList, this, true);
				} catch (PathNotFoundException ex)
				{
					continue;
				}
				coordToExcludeList.add(ennemyPosition);
				currentPath.calculateTurnCost(mapMgr.getMap(), getMoveCost(), getMove(), getMovementCapacity());
				distance =  currentPath.getTotalDistance();
			}
			
			// Distance Factor
			distanceFact = (-4.0f * distance / 100f);

			
			averageNbAttackToKill = (int) Math.ceil((float) enemy.getHitPoints() / (float) averageSingleAttack);
			nbAttackToBeDone = Math.min(averageNbAttackToKill, getNbAttack());
			
			// Kill Enemy Factor
			killFact = nbAttackToBeDone * averageSingleAttack / enemy.getHitPoints();
			
			averageDefence = Math.min(nbAttackToBeDone, enemy.getNbDefense()) 
					* (enemy.getMinDefense() + enemy.getMaxDefense()) / 2f;
			
			// Autodestruction Factor
			autoDestructionFact = averageDefence / hitPoints * -1f;

			// Production Factor
			enemyProductionTime = City.getProductionTimeByType(enemy.getUnitType());
			if (enemy.isContainer())
			{
				for (ILoadableUnit containedEnemy : ((IUnitContainer)enemy).getContainedUnitList())
				{
					enemyProductionTime += City.getProductionTimeByType(((AbstractUnit)containedEnemy).getUnitType());
				}
			}
			productionFact = ((float) enemyProductionTime / (float) productionTime);
			productionFact *= killFact;
			
			
			currentFactor = distanceFact + killFact + autoDestructionFact + productionFact;
			
			if (currentFactor > bestFactor)
			{
				bestFactor = currentFactor;
				bestEnemy = enemy;
			} 
		}

		return bestEnemy;
	}
	
	// ==========================================================
	// ==========================================================
	// Method for CONTAINED UNIT
	// ==========================================================
	
	/**
	 * Return the space needed to load this unit in a container unit (Transport
	 * Ship for example).
	 */
	public final int getSpaceCost()
	{
		return _unitInfo.getSpaceCost();
	}
	
	public boolean isContainer()
	{
		return getSpaceCapacity() > 0;
	}

	
	// ==========================================================
	// ==========================================================
	// Method for UNIT CONTAINER
	// ==========================================================

	/**
	 * Return the max space capacity of the unit.
	 * 
	 * @return Space capacity
	 */
	public final int getSpaceCapacity()
	{
		return _unitInfo.getSpaceCapacity();
	}

}