package org.jocelyn_chaumel.firststrike.core.unit;

import java.io.Serializable;
import java.util.HashMap;

import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class UnitIndex implements Serializable
{
	private final HashMap<Coord, AbstractUnitList> _unitListMap = new HashMap<Coord, AbstractUnitList>();

	public final void newUnit(final AbstractUnit p_unit)
	{
		Assert.preconditionNotNull(p_unit, "Unit");
		internalAddUnit(p_unit);
	}

	public final void removeUnit(final AbstractUnit p_unit)
	{
		Assert.preconditionNotNull(p_unit, "Unit");

		final AbstractUnitList unitList = _unitListMap.get(p_unit.getPosition());
		Assert.check(unitList.remove(p_unit), "Unit to remove not found");
	}

	private final void internalAddUnit(final AbstractUnit p_unit)
	{
		Coord unitLocation = p_unit.getPosition();
		AbstractUnitList unitList = null;
		if (!_unitListMap.containsKey(unitLocation))
		{
			unitList = new AbstractUnitList();
			_unitListMap.put(unitLocation, unitList);
		} else
		{
			unitList = _unitListMap.get(unitLocation);
			Assert.check(!unitList.contains(p_unit), "Unit already stored into this UnitIndex");
		}
		unitList.add(p_unit);

	}

	public final void moveUnit(final Coord p_oldLocation, final AbstractUnit p_unit)
	{
		Assert.preconditionNotNull(p_oldLocation, "Old Location");
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.precondition(!p_oldLocation.equals(p_unit.getPosition()), "");
		AbstractUnitList unitList = _unitListMap.get(p_oldLocation);
		Assert.check(unitList.remove(p_unit), "Unit to remove not found");
		internalAddUnit(p_unit);
	}

	public final boolean hasUnitAt(final Coord p_position)
	{
		AbstractUnitList unitList = _unitListMap.get(p_position);
		return unitList == null || unitList.isEmpty();

	}

	public final AbstractUnitList getUnitListAt(final Coord p_position)
	{
		Assert.preconditionNotNull(p_position, "Position");

		AbstractUnitList unitList = _unitListMap.get(p_position);
		if (unitList == null || unitList.isEmpty())
		{
			return null;
		}
		return unitList;
	}

	public final synchronized AbstractUnitList getEnemyList(final AbstractPlayer p_player) 
	{
		Assert.preconditionNotNull(p_player, "Player");
		final AbstractUnitList enemyList = new AbstractUnitList();
		AbstractUnitList currentUnitList = null;
		for (Coord currentCoor : _unitListMap.keySet())
		{
			currentUnitList = _unitListMap.get(currentCoor);
			if (currentUnitList.size() > 0 && currentUnitList.get(0).getPlayer() != p_player)
			{
				enemyList.addAll(currentUnitList);
			}
		}

		return enemyList;
	}

	public final AbstractUnitList getEnemyListAt(final AbstractPlayer p_player, CoordSet p_coordSet)
	{
		final AbstractUnitList enemyList = new AbstractUnitList();
		AbstractUnitList unitList = null;

		for (Coord currentCoord : p_coordSet)
		{
			unitList = _unitListMap.get(currentCoord);
			if (unitList == null || unitList.size() == 0)
			{
				continue;
			}

			if (unitList.get(0).getPlayer() == p_player)
			{
				continue;
			}

			enemyList.addAll(unitList);
		}

		return enemyList;
	}

	public final AbstractUnitList getNonNullUnitListAt(final Coord p_position)
	{
		Assert.preconditionNotNull(p_position, "Position");

		AbstractUnitList unitList = _unitListMap.get(p_position);
		if (unitList == null)
		{
			return new AbstractUnitList();
		}
		return unitList;

	}
}
