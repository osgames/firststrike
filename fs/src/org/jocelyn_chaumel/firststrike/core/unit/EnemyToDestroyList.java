/*
 * Created on Jun 30, 2005
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import java.util.ArrayList;

import org.jocelyn_chaumel.tools.data_type.FSUtil;

/**
 * @author Jocelyn Chaumel
 */
public class EnemyToDestroyList extends ArrayList<EnemyToDestroy>
{

	private final static String INTERNAL_NAME = "EnemyToDestroyList";

	@Override
	public final String toString()
	{
		return FSUtil.toStringList(INTERNAL_NAME, this);
	}

}
