/*
 * UnitProperties.java Created on 28 avril 2003, 13:01
 */

package org.jocelyn_chaumel.firststrike.core.unit;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;

/**
 * Cette classe regroupe l'ensemble des capacit�s de base d'une unit�. C'est �
 * dire sa capacit� en carburant, sa capacit� de mouvement, etc... Les
 * caract�ristiques de combat sont concerv�es dans la classe UnitCombatInfo.
 * 
 * @author Jocelyn Chaumel
 */

public final class UnitInfo implements Serializable
{
	// TODO AZ Fusionner UnitInfo et CombatUnitInfo
	public final static int NA = -1;
	public final static UnitInfo TANK_INFO = new UnitInfo(NA, 100, NA, 1, UnitTypeCst.TANK, 1);
	public final static UnitInfo FIGHTER_INFO = new UnitInfo(200, 100, NA, 1, UnitTypeCst.FIGHTER, 2);
	public final static UnitInfo DESTROYER_INFO = new UnitInfo(NA, 100, NA, NA, UnitTypeCst.DESTROYER, 2);
	public final static UnitInfo BATTLESHIP_INFO = new UnitInfo(NA, 100, NA, NA, UnitTypeCst.BATTLESHIP, 1);
	public final static UnitInfo TRANSPORT_INFO = new UnitInfo(NA, 100, 3, NA, UnitTypeCst.TRANSPORT_SHIP, 1);
	public final static UnitInfo ARTILLERY_INFO = new UnitInfo(NA, 100, NA, 1, UnitTypeCst.ARTILLERY, 1);
	public final static UnitInfo SUBMARIN_INFO = new UnitInfo(NA, 100, NA, NA, UnitTypeCst.SUBMARIN, 1);
	public final static UnitInfo CARRIER_INFO = new UnitInfo(NA, 100, 6, NA, UnitTypeCst.CARRIER, 1);
	public final static UnitInfo RADAR_INFO = new UnitInfo(NA, NA, NA, NA, UnitTypeCst.RADAR, 10);

	private final int _maxFuelCapacity;
	private final int _maxMoveCapacity;
	private final int _maxSpaceCapacity;
	private final int _spaceCost;
	private final UnitTypeCst _unitTypeCst;
	private final int _detectionRange;

	/** Creates a new instance of UnitProperties */
	private UnitInfo(	final int p_maxFuelCapacity,
						final int p_moveCapacity,
						final int p_maxSpaceCapacity,
						final int p_spaceCost,
						final UnitTypeCst p_unitType,
						final int p_detectionRange)
	{
		_maxFuelCapacity = p_maxFuelCapacity;
		_maxMoveCapacity = p_moveCapacity;
		_maxSpaceCapacity = p_maxSpaceCapacity;
		_spaceCost = p_spaceCost;
		_unitTypeCst = p_unitType;
		_detectionRange = p_detectionRange;
	}

	/**
	 * Retourne la capacit� maximum d'embarquement de l'unit�.
	 * 
	 * @return Capacit� d'embarquement.
	 */
	public final int getSpaceCapacity()
	{
		return _maxSpaceCapacity;
	}

	/**
	 * Retourne la capicit� de carburant de l'unit�.
	 * 
	 * @return Capacit� de carburant.
	 */
	public final int getMaxFuelCapacity()
	{
		return _maxFuelCapacity;
	}

	/**
	 * Retourne la capacit� de mouvement de l'unit�.
	 * 
	 * @return Capacit� de mouvement.
	 */
	public final int getMaxMovementCapacity()
	{
		return _maxMoveCapacity;
	}

	/**
	 * Retourne l'espace qu'occupe une unit� dans un transporteur.
	 * 
	 * @return Le poids.
	 */
	public final int getSpaceCost()
	{
		return _spaceCost;
	}

	public final UnitTypeCst getUnitType()
	{
		return _unitTypeCst;
	}

	public final int getDetectionRange()
	{
		return _detectionRange;
	}
}