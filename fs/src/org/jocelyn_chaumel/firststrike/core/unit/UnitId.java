/*
 * Created on Sep 4, 2005
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.tools.data_type.Id;

/**
 * @author Jocelyn Chaumel
 */
public final class UnitId extends Id
{
	/**
	 * @param anId
	 */
	public UnitId(final int anId)
	{
		super(anId);
	}
}
