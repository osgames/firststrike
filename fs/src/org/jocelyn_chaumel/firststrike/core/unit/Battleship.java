/*
 * ComputerUnit.java Created on 14 juin 2003, 13:31
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.map.AbstractMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.BattleshipMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public final class Battleship extends AbstractUnit implements IBombardment
{
	private int _nbBombardment;
	private static final BattleshipMoveCost _battleshipMoveCost = (BattleshipMoveCost) MoveCostFactory
			.getMoveCostInstance(UnitInfo.BATTLESHIP_INFO.getUnitType());

	/**
	 * Constructor.
	 */
	public Battleship(final Coord p_positionCoord, final AbstractPlayer p_player)
	{
		super(UnitInfo.BATTLESHIP_INFO, UnitCombatInfo.BATTLESHIP_COMBAT_INFO, _battleshipMoveCost, p_positionCoord, p_player);
	}

	@Override
	public final void restoreUnit()
	{
		super.restoreUnit();
		restoreNbBombardement();
	}

	public final void restoreNbBombardement()
	{
		_nbBombardment = _unitCombatInfo.getNbBombardmentCapacity();
	}

	@Override
	public AbstractMoveCost getMoveCost()
	{
		return _battleshipMoveCost;
	}

	
	@Override
	public final int getDetectionRange()
	{
		int range = super.getDetectionRange();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				range--;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				range++;
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				range *= 2;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return range;
	}

	@Override
	public final int getMinAttack()
	{
		int minAttack = super.getMinAttack();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				minAttack = minAttack + 5;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				minAttack = minAttack - 5;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				minAttack = minAttack - 10;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				minAttack = minAttack + 25;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return minAttack;
	}

	@Override
	public final int getMinDefense()
	{
		int minDef = super.getMinDefense();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				minDef = minDef + 5;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				minDef = minDef - 5;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				minDef = minDef - 10;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				minDef = minDef + 15;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return minDef;
	}


	@Override
	public int getMinBombardmentAttack()
	{
		return _unitCombatInfo.getMinBombardmentAttack();
	}

	@Override
	public int getMaxBombardmentAttack()
	{
		return _unitCombatInfo.getMaxBombardmentAttack();
	}

	@Override
	public int getBombardmentRange()
	{
		return _unitCombatInfo.getBombardmentRange();
	}

	@Override
	public final int getNbBombardment()
	{
		return _nbBombardment;
	}

	@Override
	public int getNbBombardmentCapacity()
	{
		return _unitCombatInfo.getNbBombardmentCapacity();
	}

	public final void decreaseNbBombardment()
	{
		Assert.precondition(_nbBombardment > 0, "Invalid state");
		Assert.precondition(getMove() == getMovementCapacity(), "No enough move point");
		_nbBombardment--;
		setMove(0);
	}

	@Override
	public float getBombardmentFactor(CellTypeCst p_cellType)
	{
		if (CellTypeCst.CITY.equals(p_cellType))
		{
			return IBombardment.BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_CITY;
		} else if (CellTypeCst.GRASSLAND.equals(p_cellType) || CellTypeCst.ROAD.equals(p_cellType))
		{
			return IBombardment.BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_GRASSLAND;
		} else if (CellTypeCst.LIGHT_WOOD.equals(p_cellType))
		{
			return IBombardment.BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_LIGHT_WOOD;
		} else if (CellTypeCst.HEAVY_WOOD.equals(p_cellType))
		{
			return IBombardment.BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_HEAVY_WOOD;
		} else if (CellTypeCst.HILL.equals(p_cellType))
		{
			return IBombardment.BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_HILL;
		} else if (CellTypeCst.MOUNTAIN.equals(p_cellType))
		{
			return IBombardment.BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_MOUNTAIN;
		} else if (CellTypeCst.RIVER.equals(p_cellType))
		{
			return IBombardment.BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_RIVER;
		} else if (CellTypeCst.SEA.equals(p_cellType))
		{
			return IBombardment.BATTLESHIP_BOMBARDMENT_EFFICIENCY_ON_SEA;
		} else
		{
			throw new IllegalArgumentException("Unsupported Cell Type: " + p_cellType);
		}
	}

	@Override
	public void bombPosition(final Coord p_position)
	{
		Assert.preconditionNotNull(p_position, "Coord");
		Assert.precondition(_nbBombardment > 0, "Invalid nb bombardment");
		Assert.precondition(getMove() == getMovementCapacity(), "Not enough move point");

		final int minDamage = getMinBombardmentAttack();
		final int maxDamage = getMaxBombardmentAttack();
		final CellTypeCst cellType = getPlayer().getMapMgr().getMap().getCellAt(p_position).getCellType();
		final float factorDamage = getBombardmentFactor(cellType);

		getPlayer().addBombardment(p_position, new Bombardment(minDamage, maxDamage, factorDamage));
		decreaseNbBombardment();
	}
}