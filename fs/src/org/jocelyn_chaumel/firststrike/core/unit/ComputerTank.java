package org.jocelyn_chaumel.firststrike.core.unit;

import java.util.Collections;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.TCD;
import org.jocelyn_chaumel.firststrike.core.player.TCDArrayList;
import org.jocelyn_chaumel.firststrike.core.player.TDCDistanceComparator;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class ComputerTank extends Tank
{
	private City _protectedCity = null;
	private final TCDArrayList _cityTargetList = new TCDArrayList();
	private int _targetListIdx = -1;

	public ComputerTank(final Coord p_positionCoord, final AbstractPlayer p_player)
	{
		super(p_positionCoord, p_player);
	}


	/**
	 * Protecting or attacking a City.
	 * 
	 * @return
	 */
	public boolean isProtectingCity()
	{
		return _protectedCity != null;
	}

	public City getProtectedCity()
	{
		return _protectedCity;
	}

	public void removeProtectedCity()
	{
		_protectedCity = null;
	}

	public void setProtectedCity(final City p_city)
	{
		Assert.preconditionNotNull(p_city, "City");
		_protectedCity = p_city;
	}
	
	@Override
	public void restoreUnit()
	{
		super.restoreUnit();
		if (_cityTargetList != null)
		{
			_cityTargetList.clear();
		}
		_targetListIdx = -1;
	}
	
	public void refreshTargetCityList(final CityList p_cityList)
	{
		final FSMapMgr mapMgr = getPlayer().getMapMgr();
		Path path;
		_targetListIdx = 0;
		int distance;
		_cityTargetList.clear();
		for (City currentCity : p_cityList)
		{
			try
			{
				path = mapMgr.getPath(currentCity.getPosition(), new CoordSet(), this, false);
				distance = mapMgr.getPathDistance(path, this.getMoveCost());
				_cityTargetList.add(new TCD(this, currentCity, distance));
			} catch (PathNotFoundException ex)
			{
				// NOP
			}
		}
		Collections.sort(_cityTargetList, new TDCDistanceComparator());
	}
	
	public TCD getTargetCity()
	{
		Assert.check(_targetListIdx != -1, "Target City List not refreshed");
		TCD currentCity;
		while (_targetListIdx < _cityTargetList.size())
		{
			currentCity = _cityTargetList.get(_targetListIdx);
			if (currentCity.getCity().getPlayer() != getPlayer())
			{
				return currentCity;
			}
			_targetListIdx++;
		}
		throw new FSFatalException("No more target city available");
	}
	
	public boolean setNextTargetCity()
	{
		if (_targetListIdx + 1 < _cityTargetList.size())
		{
			_targetListIdx++;
			if (_cityTargetList.get(_targetListIdx).getCity().getPlayer() != getPlayer())
			{
				return true;
			}
			return setNextTargetCity();
		}
		
		return false;
	}
}
