/*
 * Created on May 8, 2005
 */
package org.jocelyn_chaumel.firststrike.core.unit.cst;

import org.jocelyn_chaumel.tools.data_type.cst.IntCst;

/**
 * @author Jocelyn Chaumel
 */
public class DestructionPriorityLevel extends IntCst
{

	public final static DestructionPriorityLevel HIGH_LEVEL = new DestructionPriorityLevel(	1,
																							"High Destruction Priority Level");
	public final static DestructionPriorityLevel MEDIUM_LEVEL = new DestructionPriorityLevel(	2,
																								"Medium Destruction Priority Level");
	public final static DestructionPriorityLevel LOW_LEVEL = new DestructionPriorityLevel(	3,
																							"Low Destruction Priority Level");

	/**
	 * @param p_value
	 * @param p_label
	 */
	private DestructionPriorityLevel(int p_value, String p_label)
	{
		super(p_value, p_label);
	}

}
