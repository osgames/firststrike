/*
 * Created on Dec 27, 2004
 */
package org.jocelyn_chaumel.firststrike.core.unit.cst;

import org.jocelyn_chaumel.tools.data_type.cst.IntCst;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;

/**
 * @author Jocelyn Chaumel
 */
public class ControlerTypeCst extends IntCst
{
	public final static ControlerTypeCst CITY_CONTROLER = new ControlerTypeCst(2, "City Controler");

	public final static ControlerTypeCst AREA_CONTROLER = new ControlerTypeCst(3, "Area Controler");

	public final static ControlerTypeCst WOLRD_CONTROLER = new ControlerTypeCst(4, "World Controler");

	public final static ControlerTypeCst TRANSPORT_CONTROLER = new ControlerTypeCst(5, "Transport Controler");

	public final static ControlerTypeCst PLAYER_CONTROLER = new ControlerTypeCst(1, "Player Controler");

	private final static ControlerTypeCst[] _controlerTypeList = { PLAYER_CONTROLER, CITY_CONTROLER, AREA_CONTROLER,
			WOLRD_CONTROLER, TRANSPORT_CONTROLER };

	/**
	 * Contructeur paramétrisé.
	 * 
	 * @param p_value Valeur de la constante.
	 * @param p_label Valeur alphanumérique de la constante. Cette identifiant
	 *        n'est cependant pas nécessairement unique.
	 */
	protected ControlerTypeCst(final int p_value, final String p_label)
	{
		super(p_value, p_label);
	}

	public static ControlerTypeCst getInstance(final int p_value) throws InvalidConstantValueException
	{
		return (ControlerTypeCst) IntCst.getInstance(p_value, _controlerTypeList, ControlerTypeCst.class);
	}
}