/*
 * Created on Dec 27, 2004
 */
package org.jocelyn_chaumel.firststrike.core.unit.cst;

import java.util.ArrayList;

import org.jocelyn_chaumel.tools.data_type.cst.Cst;
import org.jocelyn_chaumel.tools.data_type.cst.IntCst;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;

/**
 * @author Jocelyn Chaumel
 */
public class UnitTypeCst extends IntCst
{
	public final static UnitTypeCst NULL_TYPE = new UnitTypeCst(0, "Null Type");

	public final static UnitTypeCst TANK = new UnitTypeCst(1, "Tank");

	public final static UnitTypeCst FIGHTER = new UnitTypeCst(2, "Fighter");

	public final static UnitTypeCst TRANSPORT_SHIP = new UnitTypeCst(4, "Transport_Ship");

	public final static UnitTypeCst DESTROYER = new UnitTypeCst(5, "Destroyer");

	public final static UnitTypeCst BATTLESHIP = new UnitTypeCst(6, "Battleship");

	public final static UnitTypeCst SUBMARIN = new UnitTypeCst(7, "Submarin");

	public final static UnitTypeCst ARTILLERY = new UnitTypeCst(8, "Artillery");

	public final static UnitTypeCst CARRIER = new UnitTypeCst(9, "Carrier");

	public final static UnitTypeCst RADAR = new UnitTypeCst(10, "Radar");

	private final static UnitTypeCst[] _unitTypeList = { NULL_TYPE, TANK, FIGHTER, TRANSPORT_SHIP, DESTROYER,
			BATTLESHIP, SUBMARIN, ARTILLERY, CARRIER, RADAR };
	
	public final static ArrayList<UnitTypeCst> GROUND_COMBAT_UNIT_LIST = new ArrayList<UnitTypeCst>();
	
	static {
		GROUND_COMBAT_UNIT_LIST.add(ARTILLERY);
		GROUND_COMBAT_UNIT_LIST.add(TANK);
		GROUND_COMBAT_UNIT_LIST.add(FIGHTER);
	}

	/**
	 * @param p_value
	 */
	protected UnitTypeCst(final int p_value, final String p_label)
	{
		super(p_value, p_label);
	}

	@Override
	public final String toString()
	{
		return "Unit Type:'" + getLabel() + "'";
	}

	/**
	 * Retourne l'instante d'une constante � partir d'une valeur.
	 * 
	 * @param p_value Valeur de la constante.
	 * @return Instance de la constante.
	 * @throws InvalidConstantValueException
	 */
	public static UnitTypeCst getInstance(final int p_value) throws InvalidConstantValueException
	{
		return (UnitTypeCst) IntCst.getInstance(p_value, _unitTypeList, UnitTypeCst.class);
	}
	
	public static UnitTypeCst getInstance(final String p_value) throws InvalidConstantValueException
	{
		return (UnitTypeCst) Cst.getInstance(p_value, _unitTypeList, UnitTypeCst.class);
	}
	
	@Override
	public boolean equals(final Object p_object)
	{
		if (!super.equals(p_object))
		{
			return this.getValue() == ((UnitTypeCst)p_object).getValue();
		}
		return true;
	}
	
	@Override
	public int hashCode()
	{
		return super.getValue();
	}
}