package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class UnitFactory
{
	public static AbstractUnit createUnit(	final AbstractPlayer p_player,
											final Coord p_position,
											final UnitTypeCst p_unitType)
	{
		if (UnitTypeCst.TANK.equals(p_unitType))
		{
			if (p_player instanceof ComputerPlayer)
			{
				return new ComputerTank(p_position, p_player);
			}

			return new Tank(p_position, p_player);
		} else if (UnitTypeCst.ARTILLERY.equals(p_unitType))
		{
			return new Artillery(p_position, p_player);
		} else if (UnitTypeCst.FIGHTER.equals(p_unitType))
		{
			return new Fighter(p_position, p_player);
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_unitType))
		{
			if (p_player instanceof ComputerPlayer)
			{
				return new ComputerTransportShip(p_position, p_player);
			}
			return new TransportShip(p_position, p_player);
		} else if (UnitTypeCst.DESTROYER.equals(p_unitType))
		{
			return new Destroyer(p_position, p_player);
		} else if (UnitTypeCst.BATTLESHIP.equals(p_unitType))
		{
			return new Battleship(p_position, p_player);
		} else if (UnitTypeCst.RADAR.equals(p_unitType))
		{
			return new RadarStation(p_position, p_player);
		} else
		{
			throw new FSFatalException("Invalid Production Type");
		}
	}
}
