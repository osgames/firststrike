package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.tools.data_type.Coord;

public interface IUnitActionListener
{
	public void addUnit(final AbstractUnit p_unit);

	public void moveUnit(final Coord p_oldPosition, final AbstractUnit p_unit);

	public void killUnit(final AbstractUnit p_unit);

	/**
	 * Event launched when a computer unit finish its turn.
	 * 
	 * @param p_unit
	 */
	public void finishUnitTurn(final AbstractUnit p_unit);

	// public void loadedUnit(final AbstractUnit p_unit, final AbstractUnit
	// p_transporter);
	// public void unloadedUnit (final AbstractUnit p_unit, final AbstractUnit
	// p_transporter);
}
