/*
 * ComputerUnit.java Created on 14 juin 2003, 13:31
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.directive.AbstractUnitDirective;
import org.jocelyn_chaumel.firststrike.core.map.AbstractMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.GroundMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public final class RadarStation extends AbstractUnit
{
	private static final GroundMoveCost _groundMoveCost = (GroundMoveCost) MoveCostFactory
			.getMoveCostInstance(UnitInfo.RADAR_INFO.getUnitType());

	/**
	 * Constructor.
	 */
	public RadarStation(final Coord p_positionCoord, final AbstractPlayer p_player)
	{
		super(UnitInfo.RADAR_INFO, UnitCombatInfo.RADAR_COMBAT_INFO, _groundMoveCost, p_positionCoord, p_player);
	}
	
	@Override
	public final void setPath(final Path p_path)
	{
		throw new FSFatalException("SetPath methode not possible on RadarStation unit");
	}
	
	@Override
	public final void addDirective(final AbstractUnitDirective p_directive)
	{
		throw new FSFatalException("AddDirective methode not possible on RadarStation unit");
	}

	@Override
	public final void restoreUnit()
	{
		super.restoreUnit();
	}


	@Override
	public AbstractMoveCost getMoveCost()
	{
		return _groundMoveCost;
	}

	@Override
	public final int getDetectionRange()
	{
		int range = super.getDetectionRange();
		final AbstractPlayer player = getPlayer();
		final PlayerLevelCst level = player.getPlayerLevel();
		if (player instanceof HumanPlayer)
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				// NOP
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an human player :" + level);
			}
		} else
		{
			if (level.equals(PlayerLevelCst.EASY_LEVEL))
			{
				range--;
			} else if (level.equals(PlayerLevelCst.NORMAL_LEVEL))
			{
				// NOP
			} else if (level.equals(PlayerLevelCst.HARD_LEVEL))
			{
				range++;
			} else
			{
				throw new FSFatalException("Unsupported this 'Player Level' for an computer player :" + level);
			}
		}
		return range;
	}

}