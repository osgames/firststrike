/*
 * ComputerUnit.java Created on 14 juin 2003, 13:31
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.map.BattleshipMoveCost;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class TransportShip extends AbstractUnit implements IUnitContainer
{
	private static final BattleshipMoveCost _battleshipMoveCost = (BattleshipMoveCost) MoveCostFactory
			.getMoveCostInstance(UnitInfo.TRANSPORT_INFO.getUnitType());

	private ArrayList<ILoadableUnit> _containedUnitList = new ArrayList<ILoadableUnit>();
	private static final HashSet<UnitTypeCst> _supportedContainedUnitTypeSet = new HashSet<UnitTypeCst>();

	static
	{
		_supportedContainedUnitTypeSet.add(UnitTypeCst.TANK);
		_supportedContainedUnitTypeSet.add(UnitTypeCst.ARTILLERY);
	}

	/**
	 * Constructor.
	 * 
	 * @param p_positionCoord
	 * @param p_playerId
	 */
	public TransportShip(final Coord p_positionCoord, final AbstractPlayer p_playerId)
	{
		super(UnitInfo.TRANSPORT_INFO, UnitCombatInfo.TRANSPORT_SHIP_COMBAT_INFO, _battleshipMoveCost, p_positionCoord,
				p_playerId);
	}

	@Override
	public AbstractUnit chooseBestEnemyTargetFromList(final AbstractUnitList aList)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public final void setPosition(final Coord p_coord)
	{
		super.setPosition(p_coord);

		if (isContainingUnit())
		{
			AbstractUnit currentUnit = null;
			final Iterator unitIter = _containedUnitList.iterator();
			while (unitIter.hasNext())
			{
				currentUnit = (AbstractUnit) unitIter.next();
				currentUnit.setPosition(p_coord);
			}
		}
	}
	
	@Override
	public void restoreMove()
	{
		super.restoreMove();
		
		if (isContainingUnit())
		{
			for (ILoadableUnit currentUnit : _containedUnitList)
			{
				((AbstractGroundLoadableUnit) currentUnit).restoreMove();
				((AbstractGroundLoadableUnit) currentUnit).restoreNbAttack();
				((AbstractGroundLoadableUnit) currentUnit).restoreNbDefence();
			}
		}
	}

	@Override
	public final void unloadUnit(final ILoadableUnit p_loadedTank)
	{
		Assert.precondition(_containedUnitList.contains(p_loadedTank), "The tank is not loaded in this Transport Ship");
		_containedUnitList.remove(p_loadedTank);
	}

	@Override
	public void loadUnit(final ILoadableUnit p_unit)
	{
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.precondition(isLoadable((AbstractUnit) p_unit), "This unit cannot be loaded in this container");

		_containedUnitList.add(p_unit);
	}

	/**
	 * Indicate if this unit contains almost one unit.
	 * 
	 * @return Containing state of the unit.
	 */
	@Override
	public final boolean isContainingUnit()
	{
		if (_containedUnitList == null)
		{
			return false;
		}
		
		return !_containedUnitList.isEmpty();
	}

	@Override
	public ArrayList<ILoadableUnit> getContainedUnitList()
	{
		return _containedUnitList;
	}

	@Override
	public HashSet<UnitTypeCst> getSupportedTypeOfContainedUnit()
	{
		return TransportShip._supportedContainedUnitTypeSet;
	}

	@Override
	public boolean isContained(ILoadableUnit p_unit)
	{
		return false;
	}

	@Override
	public int getFreeSpace()
	{
		int currentCapacity = ((AbstractUnit) this).getSpaceCapacity();

		for (ILoadableUnit unit : _containedUnitList)
		{
			currentCapacity -= unit.getSpaceCost();
		}

		Assert.check(currentCapacity >= 0, "Invalid Available Space calcul result");
		return currentCapacity;
	}

	@Override
	public boolean isLoadable(AbstractUnit p_unit)
	{
		if (!TransportShip._supportedContainedUnitTypeSet.contains(p_unit.getUnitType()))
		{
			return false;
		}

		if (p_unit.getMove() != p_unit.getMovementCapacity())
		{
			return false;
		}
		
		if (p_unit.getPosition().calculateDelta(getPosition()) > 1)
		{
			return false;
		}

		if (p_unit.getSpaceCost() > getFreeSpace())
		{
			return false;
		}

		if (p_unit.getPlayer() != getPlayer())
		{
			return false;
		}

		return true;
	}

	@Override
	public final ArrayList<ILoadableUnit> releaseAllUnit()
	{
		Assert.check(isContainingUnit(), "Their is no unit to release");

		final ArrayList<ILoadableUnit> unitList = _containedUnitList;
		_containedUnitList = null;
		return unitList;
	}

}