/*
 * City.java Created on 10 juin 2003, 23:21
 */

package org.jocelyn_chaumel.firststrike.core.city;

import java.io.Serializable;
import java.util.HashMap;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.IDetection;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaList;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.area.GroundAreaId;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.UnitFactory;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.Id;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe représente une ville.
 */
public class City implements IDetection, Comparable, Serializable
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(City.class.getName());

	// TODO AZ Déplacer ces valeurs dans UnitInfo
	public static final int TANK_TIME_PRODUCTION = 3;

	public static final int FIGHTER_TIME_PRODUCTION = 5;

	public static final int TRANSPROT_SHIP_TIME_PRODUCTION = 6;

	public static final int DESTROYER_TIME_PRODUCTION = 7;

	public static final int SUBMARIN_TIME_PRODUCTION = 6;

	public static final int ARTILLERY_TIME_PRODUCTION = 8;

	public final static int BATTLESHIP_TIME_PRODUCTION = 12;

	public final static int CARRIER_TIME_PRODUCTION = 11;

	public final static int DETECTION_RANGE = 3;

	private final CityId _cityId;
	private Cell _cell = null;

	private boolean _continueProduction = false;

	private AbstractPlayer _player = null;

	private int _productionTime = 0;
	private UnitTypeCst _productionTypeDone = null;

	private UnitTypeCst _nextProductionType = UnitTypeCst.NULL_TYPE;
	
	private final HashMap<String, Object> _properties = new  HashMap<String, Object>();

	/**
	 * Constructor.
	 * 
	 * @param p_coord City position.
	 * @param p_areaId Area Id containing this city.
	 * @param p_cityId City Identifier.
	 * @param p_isPortCity Indicate if this city is a port city.
	 */
	public City(final CityId p_cityId)
	{
		Assert.preconditionNotNull(p_cityId, "City Id");

		_cityId = p_cityId;
	}

	public final Id getId()
	{
		return _cityId;
	}

	@Override
	public final Coord getPosition()
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");

		return _cell.getCellCoord();
	}

	@Override
	public final AbstractPlayer getPlayer()
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		return _player;
	}

	/**
	 * Set the owner of this city.
	 * 
	 * @param p_player Owner identifier.
	 */
	public final void setPlayer(final AbstractPlayer p_player)
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		Assert.preconditionNotNull(p_player, "Player");
		Assert.check(_player != p_player, "Player already updated");

		_logger.unitDetail("City {0} has been conquered by {1}", new Object[] { this, p_player });
		_player = p_player;
		_nextProductionType = UnitTypeCst.NULL_TYPE;
	}

	/**
	 * Return the nested time to produce the next unit.
	 * 
	 * @return Nested turn.
	 */
	public final int getProductionTime()
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		Assert.check(_player != null, "No Owner for this city");
		Assert.check(isUnitUnderConstruction(), "No unit under construction");

		return _productionTime;
	}

	/**
	 * Decrease by one the nested turn to produce the next unit.
	 */
	public final void decProductionTime()
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		Assert.check(_player != null, "No Owner for this city");
		Assert.check(!UnitTypeCst.NULL_TYPE.equals(_nextProductionType), "No unit type specified");
		Assert.check(isUnitUnderConstruction(), "No unit under construction");

		_productionTime--;
		if (_productionTime == 0)
		{
			_productionTypeDone = _nextProductionType;
			_nextProductionType = UnitTypeCst.NULL_TYPE;
			if (_continueProduction)
			{
				setProductionType(_productionTypeDone);
			}

		}
	}

	/**
	 * Return the city production.
	 * 
	 * @return Production type.
	 */
	public final UnitTypeCst getProductionType()
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		Assert.check(_player != null, "No Owner for this city");

		return _nextProductionType;
	}

	/**
	 * Set city production.
	 * 
	 * @param p_unitType Unit type to produce.
	 */
	public final void setProductionType(final UnitTypeCst p_unitType)
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		Assert.check(_player != null, "No Owner for this city");
		Assert.preconditionNotNull(p_unitType, "Unit Type");

		if (UnitTypeCst.TANK.equals(p_unitType))
		{
			_productionTime = TANK_TIME_PRODUCTION;
		} else if (UnitTypeCst.FIGHTER.equals(p_unitType))
		{
			_productionTime = FIGHTER_TIME_PRODUCTION;
		} else if (UnitTypeCst.DESTROYER.equals(p_unitType))
		{
			Assert.precondition(_cell.isNearToWather(), "A port city is expected to produce a destroyer");
			_productionTime = DESTROYER_TIME_PRODUCTION;
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_unitType))
		{
			Assert.precondition(_cell.isNearToWather(), "A port city is expected to produce a transport ship");
			_productionTime = TRANSPROT_SHIP_TIME_PRODUCTION;
		} else if (UnitTypeCst.BATTLESHIP.equals(p_unitType))
		{
			Assert.precondition(_cell.isNearToWather(), "A port city is expected to produce a transport ship");
			_productionTime = BATTLESHIP_TIME_PRODUCTION;
		} else if (UnitTypeCst.ARTILLERY.equals(p_unitType))
		{
			_productionTime = ARTILLERY_TIME_PRODUCTION;
		} else if (UnitTypeCst.SUBMARIN.equals(p_unitType))
		{
			Assert.precondition(_cell.isNearToWather(), "A port city is expected to produce a transport ship");
			_productionTime = SUBMARIN_TIME_PRODUCTION;
		} else if (UnitTypeCst.CARRIER.equals(p_unitType))
		{
			Assert.precondition(_cell.isNearToWather(), "A port city is expected to produce a transport ship");
			_productionTime = CARRIER_TIME_PRODUCTION;
		} else if (UnitTypeCst.NULL_TYPE.equals(p_unitType))
		{
			_productionTime = 0;
		} else
		{
			throw new FSFatalException("Invalid Production Type");
		}
		_nextProductionType = p_unitType;
	}

	public final boolean isUnitUnderConstruction()
	{
		return !UnitTypeCst.NULL_TYPE.equals(_nextProductionType);
	}

	/**
	 * Indicate if the unit production is finish.
	 * 
	 * @return True or false.
	 */
	public final boolean isUnitReady()
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		Assert.check(_player != null, "No Owner for this city");

		return _productionTypeDone != null;
	}

	public final AbstractUnit getProducedUnit()
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		Assert.check(_player != null, "No Owner for this city");
		Assert.check(isUnitReady(), "Production isn't finished");
		Assert.check(_productionTypeDone != null, "No production finished");

		final AbstractUnit currentUnit = UnitFactory.createUnit(_player, _cell.getCellCoord(), _productionTypeDone);
		_productionTypeDone = null;
		return currentUnit;
	}

	/**
	 * Returns if the enemy unit is detected by this city.
	 * 
	 * @see org.jocelyn_chaumel.firststrike.core.IDetection#detect(org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit)
	 */
	@Override
	public final boolean detect(final AbstractUnit p_enemyUnit)
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		Assert.preconditionNotNull(p_enemyUnit, "Enemy Unit");

		final int range = getDetectionRange();
		final Coord cityCoord = _cell.getCellCoord();
		final Coord unitCoord = p_enemyUnit.getPosition();

		return unitCoord.calculateIntDistance(cityCoord) <= range;
	}

	@Override
	public final int getDetectionRange()
	{
		final PlayerLevelCst level = _player.getPlayerLevel();
		if (_player instanceof HumanPlayer)
		{
			final HumanPlayer player = (HumanPlayer) _player;
			return player.getCityDetectionRange(this);
		}

		if (PlayerLevelCst.EASY_LEVEL.equals(level))
		{
			return DETECTION_RANGE - 1;
		} else if (PlayerLevelCst.NORMAL_LEVEL.equals(level))
		{
			return DETECTION_RANGE;
		} else if (PlayerLevelCst.HARD_LEVEL.equals(level))
		{
			return DETECTION_RANGE + 5;
		} else
		{
			throw new IllegalStateException("");
		}
	}

	@Override
	public int compareTo(final Object p_object)
	{
		if (p_object == null)
		{
			return 1;
		} else if (!(p_object instanceof City))
		{
			throw new IllegalArgumentException("Invalid object instance");
		}
		return _cityId.compareTo(((City) p_object).getId());
	}

	@Override
	public final boolean equals(final Object p_object)
	{
		if (p_object == null)
		{
			return false;
		}

		if (!(p_object instanceof City))
		{
			return false;
		}

		return _cityId.equals(((City) p_object).getId());
	}

	public final void setContinueProduction(final boolean aContinueProductionFlag)
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		_continueProduction = aContinueProductionFlag;
	}

	public final boolean isContinueProduction()
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		return _continueProduction;
	}

	public boolean isPortCity()
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");
		return _cell.isNearToWather();
	}

	public AreaList getAreaList()
	{
		Assert.check(_cell != null, "No cell has been identified for this city");
		return _cell.getAreaList();
	}

	public final Area getGroundArea()
	{
		Assert.check(_cell != null, "No cell has been identified for this city");
		return _cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
	}

	public final GroundAreaId getAreaId()
	{
		Assert.check(_cell != null, "Bad City initialization : no cell setted");

		return (GroundAreaId) _cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA).getId();
	}

	public Cell getCell()
	{
		Assert.check(_cell != null, "No cell has been identified for this city");
		return _cell;
	}

	public void setCell(final Cell p_cell)
	{
		Assert.preconditionNotNull(p_cell, "Cell");
		Assert.check(_cell == null, "Cell already setted for this city");
		Assert.precondition(p_cell.isCity() && p_cell.getCity() == this, "Invalid Cell");
		_cell = p_cell;
	}

	public final static int getProductionTimeByType(final UnitTypeCst p_unitType)
	{
		if (UnitTypeCst.TANK.equals(p_unitType))
		{
			return TANK_TIME_PRODUCTION;
		} else if (UnitTypeCst.FIGHTER.equals(p_unitType))
		{
			return FIGHTER_TIME_PRODUCTION;
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_unitType))
		{
			return TRANSPROT_SHIP_TIME_PRODUCTION;
		} else if (UnitTypeCst.BATTLESHIP.equals(p_unitType))
		{
			return BATTLESHIP_TIME_PRODUCTION;
		} else if (UnitTypeCst.DESTROYER.equals(p_unitType))
		{
			return DESTROYER_TIME_PRODUCTION;
		} else if (UnitTypeCst.ARTILLERY.equals(p_unitType))
		{
			return ARTILLERY_TIME_PRODUCTION;
		} else if (UnitTypeCst.SUBMARIN.equals(p_unitType))
		{
			return SUBMARIN_TIME_PRODUCTION;
		} else if (UnitTypeCst.CARRIER.equals(p_unitType))
		{
			return CARRIER_TIME_PRODUCTION;
		} else
		{
			throw new IllegalArgumentException("Unsupported Unit Type: " + p_unitType);
		}
	}
	
	public final Object getProperty(final String p_property)
	{
		return _properties.get(p_property);
	}
	
	public final void removeProperty(final String p_property)
	{
		_properties.remove(p_property);
	}
	
	public final Object addProperty(final String p_property, final Object p_value)
	{
		return _properties.put(p_property, p_value);
	}
	

	@Override
	public final String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("{CITY=");
		sb.append(" id:").append(_cityId.getId());
		if (_cell != null)
		{
			sb.append(", ").append(_cell.getCellCoord());
			if (_player == null)
			{
				sb.append(", player: none");
			} else
			{
				sb.append(", player: ").append(_player.getId());

			}
		}

		sb.append(", prod: ").append(_nextProductionType.getLabel());
		if (!_nextProductionType.equals(UnitTypeCst.NULL_TYPE))
		{
			sb.append("(").append(_productionTime).append(" turn(s))");
		}
		sb.append("}");

		return sb.toString();
	}
}