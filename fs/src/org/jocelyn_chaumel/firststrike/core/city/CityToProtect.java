/*
 * Created on Aug 18, 2005
 */
package org.jocelyn_chaumel.firststrike.core.city;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class CityToProtect implements Comparable
{

	private final City _cityRef;
	private final int _nbUnit;
	private final int _distance;

	/**
	 * Contructeur paramétrisé
	 */
	public CityToProtect(final City p_cityRef, final int aNbUnit, final int aDistance)
	{
		Assert.preconditionNotNull(p_cityRef, "City Ref");
		Assert.precondition(aNbUnit >= 0, "Invalid Nb Unit");
		Assert.precondition(aDistance >= 0, "Invalid Distance");

		_cityRef = p_cityRef;
		_nbUnit = aNbUnit;
		_distance = aDistance;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Object p_cityToProtect)
	{
		Assert.preconditionNotNull(p_cityToProtect, "City To Protect");
		Assert.precondition(p_cityToProtect instanceof CityToProtect, "Invalid CityToProtect Class Instance");

		int result = ((CityToProtect) p_cityToProtect)._nbUnit - _nbUnit;

		if (result == 0)
		{
			result = ((CityToProtect) p_cityToProtect)._distance - _distance;
		}

		return result;
	}

	/**
	 * @return Returns the cityRef.
	 */
	public City getCityRef()
	{
		return _cityRef;
	}

	/**
	 * @return Returns the nbUnit.
	 */
	public int getNbUnit()
	{
		return _nbUnit;
	}

	/**
	 * @return Returns the distance.
	 */
	public int getDistance()
	{
		return _distance;
	}

	@Override
	public final String toString()
	{
		final StringBuffer sb = new StringBuffer();
		sb.append("CityToProtect:[");
		sb.append("Ref=").append(_cityRef);
		sb.append(", Nb Unit=").append(_nbUnit);
		sb.append(", Distance=").append(_distance);
		sb.append("]");
		return sb.toString();
	}
}
