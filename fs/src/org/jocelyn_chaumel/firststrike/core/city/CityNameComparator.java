package org.jocelyn_chaumel.firststrike.core.city;

import java.util.Comparator;

public class CityNameComparator implements Comparator<City>
{

	@Override
	public int compare(final City p_city1, City p_city2)
	{
		final String cityName1 = p_city1.getCell().getDefaultCellName();
		final String cityName2 = p_city2.getCell().getDefaultCellName();

		return cityName1.compareTo(cityName2);
	}

}
