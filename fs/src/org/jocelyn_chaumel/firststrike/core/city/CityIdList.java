/*
 * Created on Aug 21, 2005
 */
package org.jocelyn_chaumel.firststrike.core.city;

import org.jocelyn_chaumel.tools.data_type.AbstractInstanceArrayList;

/**
 * @author Jocelyn Chaumel
 */
public class CityIdList extends AbstractInstanceArrayList
{

	/**
	 * Contructeur par d�faut.
	 */
	public CityIdList()
	{
		super(CityId.class, "CityIdList");
	}

}
