/*
 * Created on Jan 8, 2005
 */
package org.jocelyn_chaumel.firststrike.core.city;

import org.jocelyn_chaumel.tools.data_type.Id;

/**
 * @author Jocelyn Chaumel
 */
public class CityId extends Id
{
	/**
	 * @param anId
	 */
	public CityId(int anId)
	{
		super(anId);
	}

}
