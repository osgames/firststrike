package org.jocelyn_chaumel.firststrike.core.city;

import org.jocelyn_chaumel.tools.debugging.Assert;

public class CityCouple
{
	private final City _firstCity;
	private final City _secondCity;

	public CityCouple(final City p_firstCity, final City p_secondCity)
	{
		Assert.preconditionNotNull(p_firstCity, "First City");
		Assert.preconditionNotNull(p_secondCity, "Second City");

		_firstCity = p_firstCity;
		_secondCity = p_secondCity;
	}

	public City getFirstCity()
	{
		return _firstCity;
	}

	public City getSecondCity()
	{
		return _secondCity;
	}

	public final double getDistanceBetweenCity()
	{
		return _firstCity.getPosition().calculateDistanceDouble(_secondCity.getPosition());
	}
}
