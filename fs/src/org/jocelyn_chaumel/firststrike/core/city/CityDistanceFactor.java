/*
 * Created on Jul 14, 2005
 */
package org.jocelyn_chaumel.firststrike.core.city;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class CityDistanceFactor implements Comparable
{

	private final City _city;
	private final double _distance;

	public CityDistanceFactor(final City p_city, final Coord p_position)
	{
		Assert.preconditionNotNull(p_city, "City");
		Assert.preconditionNotNull(p_position, "Position");

		_city = p_city;
		_distance = p_city.getPosition().calculateDistanceDouble(p_position);
	}

	@Override
	public int compareTo(final Object p_object)
	{
		if (p_object == null)
		{
			return 1;
		}

		if (!(p_object instanceof CityDistanceFactor))
		{
			throw new FSFatalException("Invalid comparation operation");
		}
		int distance = Double.compare(((CityDistanceFactor) p_object)._distance, _distance);
		return distance;
	}

	public final City getCityRef()
	{
		return _city;
	}

	public final double getDistance()
	{
		return _distance;
	}

	public final int getIntDistance()
	{
		return (int) Math.floor(_distance);
	}

	@Override
	public final String toString()
	{
		StringBuffer sb = new StringBuffer();

		sb.append("CityDistanceFactor:[");
		sb.append(_city).append(", ");
		sb.append("distance=").append(_distance);
		sb.append("]");

		return sb.toString();
	}
}
