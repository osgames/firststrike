/*
 * Created on May 1, 2005
 */
package org.jocelyn_chaumel.firststrike.core.city;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.data_type.FSUtil;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Liste contenant exclusivement des instances de "City".
 * 
 * @author Jocelyn Chaumel
 */
public class CityList extends ArrayList<City>
{
	private final static String INTERNAL_NAME = "CityList";
	
	public CityList()
	{
		
	}
	public CityList(final CityList p_cityList)
	{
		super(p_cityList);
	}

	public final boolean hasCityAtPosition(final Coord p_position)
	{
		Assert.preconditionNotNull(p_position, "Position");

		final Iterator iter = iterator();
		City currentCity;
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			if (currentCity.getPosition().equals(p_position))
			{
				return true;
			}
		}

		return false;
	}

	public final boolean hasCityPlayser(final AbstractPlayer p_player)
	{
		Assert.preconditionNotNull(p_player, "Player");

		for (City city : this)
		{
			if (city.getPlayer() == p_player)
			{
				return true;
			}
		}

		return false;
	}

	public final boolean hasCityPlayerAtPosition(final Coord p_position, final AbstractPlayer p_player)
	{
		Assert.preconditionNotNull(p_position, "Position");
		Assert.preconditionNotNull(p_player, "Player");

		final Iterator iter = iterator();
		City currentCity;
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			if (currentCity.getPosition().equals(p_position) && currentCity.getPlayer() == p_player)
			{
				return true;
			}
		}

		return false;
	}

	public final City getCityAtPosition(final Coord p_position)
	{
		Assert.preconditionNotNull(p_position, "Position");
		Assert.precondition(hasCityAtPosition(p_position), "Invalid City Position");

		Iterator iter = iterator();
		boolean isValid = false;
		City currentCity = null;
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			if (currentCity.getPosition().equals(p_position))
			{
				isValid = true;
				break;
			}
		}

		if (!isValid)
		{
			Assert.fail("Invalid Coord");
		}

		return currentCity;
	}

	public final CityList getCityByArea(final Area p_area)
	{
		final CityList cityList = new CityList();

		for (City currentCity : this)
		{
			if (currentCity.getAreaId().equals(p_area.getId()))
			{
				cityList.add(currentCity);
			}
		}

		return cityList;
	}

	/**
	 * Return the nearest city from a specific position.
	 * 
	 * @param p_position Start position.
	 * @return a city.
	 */
	public final City getDirectNearestCityFrom(final Coord p_position)
	{
		Assert.preconditionNotNull(p_position, "Position");

		final Coord nearestCityCoord = getCoordSet().getDirectNearestCoord(p_position);

		return getCityAtPosition(nearestCityCoord);
	}

	/**
	 * Return all accessible cities inside an expected range. A city is
	 * accessible if at least one of the grouped cities have a distance smaller
	 * or equal to expected range.
	 * 
	 * @param p_position Starting point
	 * @param p_range Max range between all cities.
	 * @return Grouped city list.
	 */
	public final CityList getGroupedCityList(final Coord p_position, final int p_range)
	{
		Assert.preconditionNotNull(p_position, "Position");
		Assert.precondition(p_range >= 1, "Invalid Range");
		Assert.check(this.size() > 0, "Empty City List");

		final CityList finalCityList = new CityList();
		final CityList newAddedCityList = new CityList();
		final CityList cityList = (CityList) this.clone();
		City currentCity;
		Coord currentPosition = p_position;

		Iterator iter = cityList.iterator();

		// Ajoute d'abord les villes en partand du point de d�part
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			if (currentPosition.calculateIntDistance(currentCity.getPosition()) <= p_range)
			{
				newAddedCityList.add(currentCity);
			}
		}

		if (newAddedCityList.isEmpty())
		{
			return finalCityList;
		}

		Iterator iterFinal;
		City currentFinalCity;
		// Avec toutes les nouvelles villes ajout�es,
		// l'inspection est relanc�e...
		while (newAddedCityList.size() > 0)
		{
			finalCityList.addAll(newAddedCityList);
			cityList.removeAll(newAddedCityList);
			newAddedCityList.clear();

			iter = cityList.iterator();
			while (iter.hasNext())
			{
				currentCity = (City) iter.next();
				currentPosition = currentCity.getPosition();
				iterFinal = finalCityList.iterator();
				while (iterFinal.hasNext())
				{
					currentFinalCity = (City) iterFinal.next();
					if (currentPosition.calculateIntDistance(currentFinalCity.getPosition()) <= p_range)
					{
						newAddedCityList.add(currentCity);
						break;
					}
				}
			}
		}

		return finalCityList;
	}

	public final CityIdList getCityIdList()
	{
		final CityIdList idList = new CityIdList();

		final Iterator iter = iterator();
		while (iter.hasNext())
		{
			idList.add(((City) iter.next()).getId());
		}

		return idList;
	}

	/**
	 * Return a city list owned by a specific player.
	 * 
	 * @param p_player Player.
	 * @return City list
	 */
	public final CityList getCityByPlayer(final AbstractPlayer p_player)
	{
		Assert.preconditionNotNull(p_player, "Player");
		final CityList cityList = new CityList();
		final Iterator iter = iterator();
		City currentCity;
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			if (currentCity.getPlayer() == p_player)
			{
				cityList.add(currentCity);
			}
		}

		return cityList;
	}

	/**
	 * Return a city list containing city without owner.
	 * 
	 * @return City list
	 */
	public final CityList getCityWithoutOwner()
	{
		final CityList cityList = new CityList();
		final Iterator iter = iterator();
		City currentCity;
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			if (currentCity.getPlayer() == null)
			{
				cityList.add(currentCity);
			}
		}

		return cityList;
	}

	public final CityList getCityWithoutProduction()
	{
		final CityList cityList = new CityList();
		final Iterator iter = iterator();
		City currentCity;
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			if (currentCity.getPlayer() != null && currentCity.getProductionType().equals(UnitTypeCst.NULL_TYPE))
			{
				if (currentCity.isPortCity())
				{
					cityList.add(currentCity);
				} else {
					// Set the city without sea port at the beginning of the list
					cityList.add(0, currentCity);
				}
			}
		}

		return cityList;
	}

	/**
	 * Return a city list containing all cities without owner and all enemy
	 * cities.
	 * 
	 * @param p_player Player
	 * 
	 * @return City list
	 */
	public final CityList getCityToConquer(final AbstractPlayer p_player)
	{
		Assert.preconditionNotNull(p_player, "Player");

		final CityList cityToConquerList = new CityList();
		City currentCity = null;

		// For each city of the list
		final Iterator iter = iterator();
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			if (p_player != currentCity.getPlayer())
			{
				cityToConquerList.add(currentCity);
			}
		}

		return cityToConquerList;
	}

	/**
	 * Return a city list containing all enemy cities (owned by the enemy
	 * player).
	 * 
	 * @param p_playerId Player Identifier
	 * 
	 * @return City list
	 */
	public final CityList getEnemyCity(final AbstractPlayer p_player)
	{
		Assert.preconditionNotNull(p_player, "Player");

		final CityList enemyCityList = new CityList();
		final Iterator iter = iterator();
		City currentCity = null;
		AbstractPlayer currentPlayer = null;
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			currentPlayer = currentCity.getPlayer();
			if (currentPlayer != null && currentPlayer != p_player)
			{
				enemyCityList.add(currentCity);
			}
		}

		return enemyCityList;
	}

	public final CityList getPortList()
	{
		final CityList portCityList = new CityList();
		final Iterator iter = iterator();
		City currentCity = null;
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			if (currentCity.isPortCity())
			{
				portCityList.add(currentCity);
			}
		}

		return portCityList;
	}

	public final CoordSet getCoordSet()
	{
		final Iterator iter = iterator();
		final CoordSet coordSet = new CoordSet();
		City currentCity;
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			coordSet.add(currentCity.getPosition());
		}

		return coordSet;
	}

	public final CityDistanceFactor getShortestDistanceBetweenCoord(final Coord p_position)
	{
		Assert.preconditionNotNull(p_position, "Position");
		Assert.check(!isEmpty(), "This list is empty");

		double bestDistance = Integer.MAX_VALUE;
		CityDistanceFactor bestFactor = null;
		CityDistanceFactor currentFactor = null;
		City currentCity = null;
		Iterator iter = this.iterator();
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			currentFactor = new CityDistanceFactor(currentCity, p_position);
			if (bestDistance > currentFactor.getDistance())
			{
				bestDistance = currentFactor.getDistance();
				bestFactor = currentFactor;
			}
		}

		// Construction de la liste des villes les plus proches
		return bestFactor;
	}

	public final CityCouple getShortestDistanceBetweenCitys(final CityList p_cityList)
	{
		Assert.preconditionNotNull(p_cityList, "City List");
		Assert.precondition(!p_cityList.isEmpty(), "Empty City List");
		Assert.check(!isEmpty(), "Current city set is empty");

		final Iterator iter = iterator();
		City currentCity = null;
		City bestCity = null;
		double bestDistance = Integer.MAX_VALUE;
		CityDistanceFactor currentCityFactor = null;
		CityDistanceFactor bestCityFactor = null;

		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			currentCityFactor = p_cityList.getShortestDistanceBetweenCoord(currentCity.getPosition());
			if (currentCityFactor.getDistance() < bestDistance)
			{
				bestDistance = currentCityFactor.getDistance();
				bestCityFactor = currentCityFactor;
				bestCity = currentCity;
			}
		}
		final CityCouple cityCouple = new CityCouple(bestCity, bestCityFactor.getCityRef());

		return cityCouple;
	}

	public final boolean hasCityPlayerInRange(	final Coord p_position,
												final AbstractPlayer p_player,
												final int p_maxRange)
	{
		Assert.preconditionNotNull(p_position, "Start position");
		Assert.preconditionNotNull(p_player, "Player");
		Assert.precondition(p_maxRange > 0, "Invalid Max Range");

		City currentCity = null;
		final Iterator iter = iterator();
		while (iter.hasNext())
		{
			currentCity = (City) iter.next();
			if (p_position.calculateIntDistance(currentCity.getPosition()) <= p_maxRange)
			{
				return true;
			}
		}

		return false;
	}
	
	public final CityList getCityInRange(final Coord p_position, final int p_range)
	{
		Assert.preconditionNotNull(p_position);
		Assert.preconditionIsPositive(p_range, "Range must be a positive value");
		CityList cityList = new CityList();
		for (City city : this)
		{
			if (city.getPosition().calculateIntDistance(p_position) <= p_range)
			{
				cityList.add(city);
			}
		}

		return cityList;
		
	}
	
	public final CityList getCityConstructingUnitType(final UnitTypeCst p_unitTypeCst)
	{
		Assert.preconditionNotNull(p_unitTypeCst);
		CityList cityList = new CityList();
		for (City city : this)
		{
			if (city.isUnitUnderConstruction() && city.getProductionType().equals(p_unitTypeCst))
			{
				cityList.add(city);
			}
		}

		return cityList;
	}
	
	public final CityList getCityConstructingUnitType(final ArrayList<UnitTypeCst> p_unitTypeList)
	{
		Assert.preconditionNotNull(p_unitTypeList);
		CityList cityList = new CityList();
		for (City city : this)
		{
			if (city.isUnitUnderConstruction() && p_unitTypeList.contains(city.getProductionType()))
			{
				cityList.add(city);
			}
		}

		return cityList;
	}

	public final void sortByName()
	{
		Collections.sort(this, new CityNameComparator());
	}

	@Override
	public final String toString()
	{
		return FSUtil.toStringList(INTERNAL_NAME, this);
	}

}