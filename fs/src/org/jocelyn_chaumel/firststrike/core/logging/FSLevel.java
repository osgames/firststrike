package org.jocelyn_chaumel.firststrike.core.logging;

import java.util.logging.Level;

public class FSLevel extends Level
{
	public static final FSLevel SYSTEM = new FSLevel("SYSTEM", Level.INFO.intValue() - 1, true);
	public static final FSLevel SYSTEM_ACTION = new FSLevel("SYSTEM_ACTION", Level.CONFIG.intValue() - 1, true);
	public static final FSLevel SYSTEM_SUBACTION = new FSLevel("SYSTEM_SUBACTION", Level.FINE.intValue() - 2, true);
	public static final FSLevel SYSTEM_SUBACTION_DETAIL = new FSLevel(	"SYSTEM_SUBACTION_DETAIL",
																		Level.FINE.intValue() - 1,
																		true);
	public static final FSLevel SYSTEM_WARNING = new FSLevel("SYSTEM_WARNING", Level.WARNING.intValue() - 1, true);
	public static final FSLevel SYSTEM_ERROR = new FSLevel("SYSTEM_ERROR", Level.SEVERE.intValue() - 1, true);

	public static final FSLevel PLAYER = new FSLevel("GAME", Level.INFO.intValue(), false);
	public static final FSLevel PLAYER_ACTION = new FSLevel("GAME_ACTION", Level.CONFIG.intValue(), false);
	public static final FSLevel PLAYER_SUBACTION = new FSLevel("GAME_SUBACTION", Level.CONFIG.intValue() - 10, false);
	public static final FSLevel PLAYER_SUBACTION_DETAIL = new FSLevel(	"GAME_SUBACTION_DETAIL",
																		Level.CONFIG.intValue() - 9,
																		false);
	public static final FSLevel UNIT = new FSLevel("GAME_UNIT", Level.FINE.intValue(), false);
	public static final FSLevel UNIT_ACTION = new FSLevel("GAME_UNIT_ACTION", Level.FINER.intValue(), false);
	public static final FSLevel UNIT_SUBACTION = new FSLevel("GAME_UNIT_SUBACTION", Level.FINEST.intValue(), false);
	public static final FSLevel UNIT_SUBACTION_DETAIL = new FSLevel("GAME_UNIT_DETAIL",
																	Level.FINEST.intValue() - 100,
																	false);
	public static final FSLevel GAME_WARNING = new FSLevel("GAME_WARNING", Level.WARNING.intValue() - 1, false);
	public static final FSLevel GAME_ERROR = new FSLevel("GAME_ERROR", Level.SEVERE.intValue() - 1, false);

	private final boolean _isSystemLevelFlag;

	public FSLevel(String p_name, int p_value, final boolean p_isSystemLevelFlag)
	{
		super(p_name, p_value, null);
		_isSystemLevelFlag = p_isSystemLevelFlag;
	}

	public final boolean isSystemLevel()
	{
		return _isSystemLevelFlag;
	}

	public final boolean isGameLevel()
	{
		return !_isSystemLevelFlag;
	}
}
