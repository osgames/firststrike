package org.jocelyn_chaumel.firststrike.core.logging;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Filter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Handler n'enregistrant que les traces du systeme.
 * 
 * @author Joss
 */
public class FSSystemFileHandler extends FileHandler
{
	private final static FSLogger _FSLOGGER = FSLoggerManager.getLogger(FSSystemFileHandler.class.getName());

	public FSSystemFileHandler(final String p_patternFile) throws IOException, SecurityException
	{
		super(p_patternFile, 1024 * 500, 2, true);
		setFormatter(new FSFormatter());
		setLevel(Level.ALL);
		setFilter(new FSSystemFilter());
		_FSLOGGER.systemAction("System logger started here : {0}", p_patternFile);
	}

	private class FSSystemFilter implements Filter
	{

		@Override
		public boolean isLoggable(final LogRecord p_record)
		{
			final Level level = p_record.getLevel();
			if (level instanceof FSLevel)
			{
				final FSLevel fsLevel = (FSLevel) level;
				if (fsLevel.isSystemLevel())
				{
					return true;
				}
			}
			return false;
		}

	}

}
