package org.jocelyn_chaumel.firststrike.core.logging;

import java.io.Serializable;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.jocelyn_chaumel.tools.StringManipulatorHelper;
import org.jocelyn_chaumel.tools.debugging.Assert;

public final class FSLogger implements Serializable
{
	private String _loggerName = null;
	private transient Logger _logger;

	public FSLogger(final String p_loggerName)
	{
		Assert.preconditionNotNull(p_loggerName, "Logger");
		_loggerName = p_loggerName;
	}

	private Logger getLogger()
	{
		if (_logger == null)
		{
			_logger = Logger.getLogger(_loggerName);
		}

		return _logger;
	}

	public final void player(final String p_message)
	{
		getLogger().log(FSLevel.PLAYER, p_message);
	}

	public final void player(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.PLAYER, message);
	}

	public final void playerAction(final String p_message)
	{
		getLogger().log(FSLevel.PLAYER_ACTION, p_message);
	}

	public final void playerAction(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.PLAYER_ACTION, message);
	}

	public final void playerSubAction(final String p_message)
	{
		getLogger().log(FSLevel.PLAYER_SUBACTION, p_message);
	}

	public final void playerSubAction(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.PLAYER_SUBACTION, message);
	}

	public final void playerSubActionDetail(final String p_message)
	{
		getLogger().log(FSLevel.PLAYER_SUBACTION_DETAIL, p_message);
	}

	public final void playerSubActionDetail(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.PLAYER_SUBACTION_DETAIL, message);
	}

	public final void playerSubActionDetail(final String p_message, final Object[] p_messageParamArray)
	{
		String finalMessage = p_message;
		for (int i = 0; i < p_messageParamArray.length; i++)
		{
			finalMessage = StringManipulatorHelper.buildMessage(finalMessage, p_messageParamArray[i], i);
		}
		getLogger().log(FSLevel.PLAYER_SUBACTION_DETAIL, finalMessage);
	}

	public final void unit(final String p_message)
	{
		getLogger().log(FSLevel.UNIT, p_message);
	}

	public final void unit(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.UNIT, message);
	}

	public final void unitAction(final String p_message)
	{
		getLogger().log(FSLevel.UNIT_ACTION, p_message);
	}

	public final void unitAction(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.UNIT_ACTION, message);
	}

	public final void unitAction(final String p_message, final Object p_messageParam1, final Object p_messageParam2)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam1, p_messageParam2);
		getLogger().log(FSLevel.UNIT_ACTION, message);
	}

	public final void unitSubAction(final String p_message)
	{
		getLogger().log(FSLevel.UNIT_SUBACTION, p_message);
	}

	public final void unitSubAction(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.UNIT_SUBACTION, message);

	}

	public final void unitSubAction(final String p_message, final Object[] p_messageParamArray)
	{
		String finalMessage = p_message;
		for (int i = 0; i < p_messageParamArray.length; i++)
		{
			finalMessage = StringManipulatorHelper.buildMessage(finalMessage, p_messageParamArray[i], i);
		}
		getLogger().log(FSLevel.UNIT_SUBACTION, finalMessage);

	}

	public final void unitDetail(final String p_message)
	{
		getLogger().log(FSLevel.UNIT_SUBACTION_DETAIL, p_message);
	}

	public final void unitDetail(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.UNIT_SUBACTION_DETAIL, message);
	}

	public final void unitDetail(final String p_message, final Object[] p_messageParamArray)
	{
		String finalMessage = p_message;
		for (int i = 0; i < p_messageParamArray.length; i++)
		{
			finalMessage = StringManipulatorHelper.buildMessage(finalMessage, p_messageParamArray[i], i);
		}
		getLogger().log(FSLevel.UNIT_SUBACTION_DETAIL, finalMessage);
	}

	public final void gameWarning(final String p_message)
	{
		getLogger().log(FSLevel.GAME_WARNING, p_message);
	}

	public final void gameError(final String p_message, final Throwable p_ex)
	{
		if (p_message != null)
		{
			final LogRecord record = new LogRecord(FSLevel.GAME_ERROR, p_message);
			getLogger().log(record);
		}

		if (p_ex != null)
		{
			final LogRecord record = new LogRecord(FSLevel.GAME_ERROR, "");
			record.setThrown(p_ex);
			getLogger().log(record);
		}
	}

	public final void system(final String p_message)
	{
		getLogger().log(FSLevel.SYSTEM, p_message);
	}

	public final void system(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.SYSTEM, message);
	}

	public final void systemAction(final String p_message)
	{
		getLogger().log(FSLevel.SYSTEM_ACTION, p_message);
	}

	public final void systemAction(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.SYSTEM_ACTION, message);
	}

	public final void systemSubAction(final String p_message)
	{
		getLogger().log(FSLevel.SYSTEM_SUBACTION, p_message);
	}

	public final void systemSubAction(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.SYSTEM_SUBACTION, message);
	}

	public final void systemSubActionDetail(final String p_message)
	{
		getLogger().log(FSLevel.SYSTEM_SUBACTION_DETAIL, p_message);
	}

	public final void systemSubActionDetail(final String p_message, final Object p_messageParam)
	{
		final String message = StringManipulatorHelper.buildMessage(p_message, p_messageParam);
		getLogger().log(FSLevel.SYSTEM_SUBACTION_DETAIL, message);
	}

	public final void systemWarning(final String p_message)
	{
		getLogger().log(FSLevel.SYSTEM_WARNING, p_message);
	}

	public final void systemWarning(final String p_message, final Throwable p_ex)
	{
		getLogger().log(FSLevel.SYSTEM_WARNING, p_message);

		if (p_ex != null)
		{
			final LogRecord record = new LogRecord(FSLevel.SYSTEM_WARNING, "");
			record.setThrown(p_ex);
			getLogger().log(record);
		}
	}

	public final void systemError(final String p_message, final Throwable p_ex)
	{
		if (p_message != null)
		{
			final LogRecord record = new LogRecord(FSLevel.SYSTEM_ERROR, p_message);
			getLogger().log(record);
		}

		if (p_ex != null)
		{
			final LogRecord record = new LogRecord(FSLevel.SYSTEM_ERROR, "");
			record.setThrown(p_ex);
			getLogger().log(record);
		}
	}
}
