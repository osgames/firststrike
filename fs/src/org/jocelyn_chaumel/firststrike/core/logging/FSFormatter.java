package org.jocelyn_chaumel.firststrike.core.logging;

import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class FSFormatter extends Formatter
{

	@Override
	public String format(final LogRecord p_record)
	{
		final Level level = p_record.getLevel();

		String prefix;
		String suffix = "";
		if (level instanceof FSLevel)
		{
			final FSLevel fsLevel = (FSLevel) level;

			if (FSLevel.PLAYER.equals(fsLevel))
			{
				prefix = "\n - - - o PLAYER : ";
			} else if (FSLevel.PLAYER_ACTION.equals(fsLevel))
			{
				prefix = " | | | | o ACTION : ";
			} else if (FSLevel.PLAYER_SUBACTION.equals(fsLevel))
			{
				prefix = " | | | | | o SUB ACTION : ";
			} else if (FSLevel.PLAYER_SUBACTION_DETAIL.equals(fsLevel))
			{
				prefix = " | | | | | | o SUB ACTION DETAIL: ";
			} else if (FSLevel.UNIT.equals(fsLevel))
			{
				prefix = " | | | | | | | o UNIT : ";
			} else if (FSLevel.UNIT_ACTION.equals(fsLevel))
			{
				prefix = " | | | | | | | | o ACTION : ";
			} else if (FSLevel.UNIT_SUBACTION.equals(fsLevel))
			{
				prefix = " | | | | | | | | | o Sub action : ";
			} else if (FSLevel.UNIT_SUBACTION_DETAIL.equals(fsLevel))
			{
				prefix = " | | | | | | | | | | o Detail : ";
			} else if (FSLevel.GAME_WARNING.equals(fsLevel))
			{
				prefix = "\n--> Warning : ";
				suffix = "\n";
			} else if (FSLevel.GAME_ERROR.equals(fsLevel))
			{
				prefix = "\n==> ERROR : ";
				final Throwable throwable = p_record.getThrown();
				if (throwable != null)
				{
					suffix = getStackTrace(throwable);
				} else
				{
					suffix = "\n";
				}

			} else if (FSLevel.SYSTEM.equals(fsLevel))
			{
				prefix = "\nSYSTEM : ";
			} else if (FSLevel.SYSTEM_ACTION.equals(fsLevel))
			{
				prefix = " o SYSTEM Action : ";
			} else if (FSLevel.SYSTEM_SUBACTION.equals(fsLevel))
			{
				prefix = " | o System Sub Action : ";
			} else if (FSLevel.SYSTEM_SUBACTION_DETAIL.equals(fsLevel))
			{
				prefix = " | | o System Sub Action detail : ";
			} else if (FSLevel.SYSTEM_WARNING.equals(fsLevel))
			{
				prefix = "\n--> Warning : ";
				suffix = "\n";
			} else if (FSLevel.SYSTEM_ERROR.equals(fsLevel))
			{
				prefix = "\n==> ERROR : ";
				final Throwable throwable = p_record.getThrown();
				if (throwable != null)
				{
					suffix = getStackTrace(throwable);
				} else
				{
					suffix = "\n";
				}
			} else
			{
				throw new UnsupportedOperationException("Level unknown");
			}
		} else
		{
			prefix = "";
		}

		return prefix + p_record.getMessage() + "\n" + suffix;
	}

	private String getStackTrace(final Throwable p_ex)
	{
		final StringBuffer sb = new StringBuffer();
		sb.append(p_ex.getClass().getName());
		sb.append(" : ").append(p_ex.getMessage());
		sb.append("\n");

		final StackTraceElement[] elementArray = p_ex.getStackTrace();
		for (int i = 0; i < elementArray.length; i++)
		{
			sb.append("   ");
			sb.append(elementArray[i]);
			sb.append("\n");
		}

		final Throwable cause = p_ex.getCause();
		if (cause != null)
		{
			sb.append("\nCause :\n");
			sb.append(getStackTrace(cause));
		}
		sb.append("\n");

		return sb.toString();
	}
}
