package org.jocelyn_chaumel.firststrike.core.logging;

import java.io.File;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfiguration;
import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FSLoggerManager
{
	private final static Logger _fsLogger = Logger.getLogger("org.jocelyn_chaumel.firststrike");
	private static boolean _consolerLoggerActive = false;

	private FSLoggerManager()
	{

	}

	public final static void setSystemFileLogger(final File p_systemLogDir) throws SecurityException, IOException
	{
		Assert.preconditionNotNull(p_systemLogDir, "System Log Dir");

		// D�sactivation du default handler
		Logger.getLogger("").getHandlers()[0].setLevel(Level.OFF);

		// Supprime tous les handlers existant pour FS
		removeAllHandlerFormLogger(_fsLogger);

		if (!p_systemLogDir.isDirectory())
		{
			FileMgr.forceCreateDirIfNotPresent(p_systemLogDir);
		}

		final String patternFile = p_systemLogDir.getAbsolutePath() + File.separator
				+ ApplicationConfiguration.SYSTEM_LOG_FILENAME + "_%g.log";
		final FSSystemFileHandler systemFileHandler = new FSSystemFileHandler(patternFile);

		// Supprimer le system File handler s'il existe d�j�
		removeSystemFileLogger();

		// Ajoute le system file handler systeme au loggeur
		_fsLogger.addHandler(systemFileHandler);
	}

	public final static void removeSystemFileLogger()
	{
		final Handler[] handlerArray = _fsLogger.getHandlers();
		Handler currentHandler = null;
		if (handlerArray != null)
		{
			for (int i = 0; i < handlerArray.length; i++)
			{
				currentHandler = handlerArray[i];
				if (currentHandler instanceof FSSystemFileHandler)
				{
					currentHandler.close();
					_fsLogger.removeHandler(currentHandler);
				}
			}
		}
	}

	private final static void removeAllHandlerFormLogger(final Logger p_logger)
	{
		Handler[] handlerArray = p_logger.getHandlers();
		if (handlerArray != null)
		{
			for (int i = 0; i < handlerArray.length; i++)
			{
				p_logger.removeHandler(handlerArray[i]);
			}
		}
	}

	/**
	 * Ajoute le handler standard de FS : ConsoleHandler.
	 * 
	 * @param p_level
	 */
	public final static void setConsoleLogger(final Level p_level)
	{
		if (!_consolerLoggerActive)
		{
			_consolerLoggerActive = true;
			removeAllHandlerFormLogger(_fsLogger);
			Handler handler = new ConsoleHandler();
			handler.setFormatter(new FSFormatter());
			handler.setLevel(p_level);
			_fsLogger.setLevel(p_level);
			_fsLogger.addHandler(handler);
		}
	}

	public final static FSLogger getLogger(final String p_loggerName)
	{
		return new FSLogger(p_loggerName);
	}

	public final static FSLogger getLogger(final Class p_class)
	{
		return getLogger(p_class.getName());
	}

	public final static void main(String[] p_params)
	{
		setConsoleLogger(FSLevel.ALL);
		Logger log = Logger.getLogger("org.jocelyn_chaumel.firststrike");

		log.log(FSLevel.PLAYER, "Joss");
		log.log(FSLevel.PLAYER_ACTION, "Start turn");
		log.log(FSLevel.UNIT, "Start first unit");
		log.log(FSLevel.UNIT_ACTION, "Move at 1,1");
		log.log(FSLevel.UNIT_ACTION, "Move at 1,2");
		log.log(FSLevel.UNIT_ACTION, "Attack first enemy");
		log.log(FSLevel.UNIT_SUBACTION, "Domage : 53");
		log.log(FSLevel.UNIT_SUBACTION, "Domage : 53");
		log.log(FSLevel.UNIT, "Start second unit");
		log.log(FSLevel.GAME_WARNING, "Attention, les fruits sont rouges");
		try
		{
			genException();
		} catch (Exception ex)
		{
			LogRecord rec = new LogRecord(FSLevel.GAME_ERROR, "un message");
			rec.setThrown(ex);
			log.log(rec);
		}
		log.log(FSLevel.PLAYER_ACTION, "Finish turn");
		log.log(FSLevel.PLAYER, "Ordi");
	}

	private final static void genException()
	{
		String s = null;
		s.equals("");
	}
}
