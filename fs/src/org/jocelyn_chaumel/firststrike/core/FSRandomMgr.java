package org.jocelyn_chaumel.firststrike.core;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import org.jocelyn_chaumel.tools.data_type.FloatList;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe est un gestionnaire qui facilite la manipulation de la classe
 * Random, classe permettant de g�n�rer les nombres al�atoire de l'application.
 * 
 * @author Jocelyn Chaumel
 */
public class FSRandomMgr implements Serializable
{
	private int _nbRandomNumber = 10;
	private final int _nbRandomizedTurn;
	private ArrayList<FloatList> _randomizedFloatListList = new ArrayList<FloatList>();
	private FSRandom _random = null;

	public FSRandomMgr(final int p_nbRandomizedTurn) throws IOException
	{
		Assert.precondition(p_nbRandomizedTurn > 1, "Nb Randomized Must be greater than 2");

		_random = FSRandom.getInstance();
		_nbRandomizedTurn = p_nbRandomizedTurn;
		for (int i = 0; i < _nbRandomizedTurn; i++)
		{
			_randomizedFloatListList.add(generateFloatList(_nbRandomNumber));
		}
		_random = FSRandom.getInstance((FloatList) _randomizedFloatListList.get(0));
	}

	/**
	 * Remove the current random number list & increase the number of random
	 * number if necessary in all other list.
	 */
	public final void finishTurn()
	{
		final FloatList floatList = (FloatList) _randomizedFloatListList.get(0);
		if (((float) floatList.size()) < ((float) _nbRandomNumber) / 2f)
		{
			_nbRandomNumber *= 2;
			for (int i = 1; i < _nbRandomizedTurn; i++)
			{
				completeFloatList((FloatList) _randomizedFloatListList.get(i));
			}

		}
		_randomizedFloatListList.remove(0);
		_randomizedFloatListList.add(generateFloatList(_nbRandomNumber));
		_random = FSRandom.getInstance((FloatList) _randomizedFloatListList.get(0));
	}

	private final FloatList generateFloatList(final int p_nbGeneratedFloat)
	{
		Assert.precondition(p_nbGeneratedFloat > 0, "Invalid Nb Float Number");

		final FloatList floatList = new FloatList();

		float currentFloat;
		for (int i = 0; i < p_nbGeneratedFloat; i++)
		{
			currentFloat = _random.getNewRandomFloat();
			floatList.add(new Float(currentFloat));
		}
		return floatList;
	}

	private final void completeFloatList(final FloatList p_floatList)
	{
		int i = _nbRandomNumber - p_floatList.size();
		while (i-- > 0)
		{
			p_floatList.add(new Float(_random.getNewRandomFloat()));
		}
	}

	/**
	 * Return the list of random number list. This method should not be used in
	 * normal mode, only for debugging mode.
	 * 
	 * @return list of random number list.
	 */
	final ArrayList getRandomFloatListList()
	{
		return _randomizedFloatListList;
	}
}
