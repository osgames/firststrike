/*
 * Created on Nov 9, 2004
 */
package org.jocelyn_chaumel.firststrike;

import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.IFirstStrikeEvent;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaList;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.area.GroundAreaId;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractGroundLoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.ILoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.data_type.SingleAttackResult;
import org.jocelyn_chaumel.tools.StringManipulatorHelper;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * Cette classe repr�sente une partie Frist Strike.
 * 
 * @author Jocelyn Chaumel
 */
public final class FirstStrikeGame implements IFirstStrikeEvent, Serializable, Comparable
{
	public final static String FIELD_SEPARETOR = "_";
	private final static FSLogger _logger = FSLoggerManager.getLogger(FirstStrikeGame.class.getName());

	private final PlayerMgr _playerMgr;

	private final FSMapMgr _mapMgr;

	private final FSRandomMgr _randomMgr;

	private final FSOptionMgr _optionMgr;

	private transient IFirstStrikeEvent _frameListener = null;

	private String _shortName;

	private AbstractPlayer _currentPlayer = null;

	private long _saveDate = 0;
	private boolean _isGameOver = false;

	/**
	 * Constructor.
	 * 
	 * @param p_playerMgr
	 * @param p_mapMgr
	 * @param p_shadowMgr
	 * @param p_randomMgr
	 * @param p_optionMgr
	 */
	public FirstStrikeGame(	final String p_gameName,
							final PlayerMgr p_playerMgr,
							final FSMapMgr p_mapMgr,
							final FSRandomMgr p_randomMgr,
							final FSOptionMgr p_optionMgr)
	{
		Assert.preconditionNotNull(p_gameName, "Game Name");
		Assert.preconditionNotNull(p_playerMgr, "Player Mgr");
		Assert.preconditionNotNull(p_mapMgr, "Map Mgr");
		Assert.preconditionNotNull(p_randomMgr, "Randomized Float List List");
		Assert.preconditionNotNull(p_optionMgr, "Option Mgr");

		_shortName = p_gameName;
		_playerMgr = p_playerMgr;
		_playerMgr.setGameListener(this);
		_mapMgr = p_mapMgr;
		_randomMgr = p_randomMgr;
		_optionMgr = p_optionMgr;
	}

	public final void startGame()
	{
		Assert.check(_currentPlayer == null, "Game already started");
		_currentPlayer = _playerMgr.getCurrentPlayer();
		_currentPlayer.startTurn(_playerMgr.getTurnNumber());
	}

	/**
	 * Retourne la d�finition du joueur courant.
	 * 
	 * @return D�finition d'un joueur.
	 */
	public final AbstractPlayer getCurrentPlayer()
	{
		return _playerMgr.getCurrentPlayer();
	}

	/**
	 * Permet a un joueur humain d'ajuster la production d'une ville.
	 * 
	 * @param p_position
	 *            Position de la ville.
	 * @param p_unitType
	 *            Type d'unit� � produire.
	 */
	public final void performSetCityProductionAt(final Coord p_position, final UnitTypeCst p_unitType)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(!_isGameOver, "The game is finished");
		Assert.preconditionNotNull(p_position, "Position");
		Assert.preconditionNotNull(p_unitType, "Unit Type");
		Assert.precondition(_mapMgr.getCityList().hasCityAtPosition(p_position), "Invalid position");
		Assert.precondition(_mapMgr.getCityList().getCityAtPosition(p_position).getPlayer() == _currentPlayer,
							"Invalid city's player");
		Assert.check(_currentPlayer instanceof HumanPlayer, "Invalid Current Player");

		final City city = _mapMgr.getCityList().getCityAtPosition(p_position);
		city.setProductionType(p_unitType);
		_logger.playerSubActionDetail("City production updated :" + city.toString());
	}

	public final void performSetCityContinueProductionAt(final Coord p_position, final boolean p_continueProductionFlag)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(!_isGameOver, "The game is finished");
		Assert.preconditionNotNull(p_position, "Position");
		Assert.precondition(_mapMgr.getCityList().hasCityAtPosition(p_position), "Invalid position");
		Assert.precondition(_mapMgr.getCityList().getCityAtPosition(p_position).getPlayer() == _currentPlayer,
							"Invalid city's player");
		Assert.check(_currentPlayer instanceof HumanPlayer, "Invalid Current Player");

		final City city = _mapMgr.getCityList().getCityAtPosition(p_position);
		city.setContinueProduction(p_continueProductionFlag);
	}

	public final boolean isValidDestinationForUnit(final AbstractUnit p_unit, final Coord p_destination)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.preconditionNotNull(p_destination, "Destination");

		if (p_unit instanceof Fighter)
		{
			return isValidDestinationForAirUnit(p_destination);
		} else if (p_unit instanceof Tank)
		{
			return isValidDestinationForGroundUnit(p_unit.getPosition(), p_destination);
		} else
		{
			Assert.check(	p_unit instanceof TransportShip || p_unit instanceof Destroyer || p_unit instanceof Battleship,
							"Unsupported unit type: " + p_unit.getClass().getName());
			return isValidDestinationForSeaUnit(p_unit.getPosition(), p_destination);
		}
	}

	/**
	 * Sp�cifie si la destination s�lectionn�e est une position valide pour les
	 * unit� a�rienne du joueur courant.
	 * 
	 * @param p_startPosition
	 *            Position de d�part.
	 * @param p_destinationPosition
	 *            Position d'arriv�e souhait�e.
	 * @return Accessibilit� d'une position a�rienne.
	 */
	public final boolean isValidDestinationForAirUnit(final Coord p_destinationPosition)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(!_isGameOver, "The game is finished");
		Assert.preconditionNotNull(p_destinationPosition, "Destination Position");

		final HumanPlayer human = (HumanPlayer) _currentPlayer;
		if (!human.isValidCoord(p_destinationPosition))
		{
			return false;
		}
		final CoordSet citySet = _mapMgr.getCityList().getEnemyCity(_playerMgr.getCurrentPlayer()).getCoordSet();

		return !citySet.contains(p_destinationPosition);
	}

	/**
	 * Sp�cifie si la destination s�lectionn�e est une position valide pour les
	 * unit� a�rienne du joueur courant.
	 * 
	 * @param p_startPosition
	 *            Position de d�part.
	 * @param p_destinationPosition
	 *            Position d'arriv�e souhait�e.
	 * @return Accessibilit� d'une position a�rienne.
	 */
	public final boolean isValidDestinationForGroundUnit(final Coord p_startPosition, final Coord p_destinationPosition)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(!_isGameOver, "The game is finished");
		Assert.preconditionNotNull(p_startPosition, "Start Position");
		Assert.preconditionNotNull(p_destinationPosition, "Destination Position");
		Assert.precondition(!p_startPosition.equals(p_destinationPosition),
							"Start and destination position are equals");

		final HumanPlayer human = (HumanPlayer) _currentPlayer;
		if (!human.isValidCoord(p_destinationPosition))
		{
			return false;
		}

		final FSMap map = _mapMgr.getMap();
		final GroundAreaId startAreaId = (GroundAreaId) map.getCellAt(p_startPosition).getAreaList()
				.getFirstAreaByType(AreaTypeCst.GROUND_AREA).getId();
		final AreaList destinationAreaList = map.getCellAt(p_destinationPosition).getAreaList();
		final Area destinationArea = destinationAreaList.getFirstAreaByType(AreaTypeCst.GROUND_AREA);
		if (destinationArea == null)
		{
			return false;
		}

		final GroundAreaId destinationAreaId = (GroundAreaId) destinationArea.getId();

		return startAreaId.equals(destinationAreaId);
	}

	public final boolean isValidDestinationForSeaUnit(final Coord p_startPosition, final Coord p_destinationPosition)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(!_isGameOver, "The game is finished");
		Assert.preconditionNotNull(p_startPosition, "Start Position");
		Assert.preconditionNotNull(p_destinationPosition, "Destination Position");
		Assert.precondition(!p_startPosition.equals(p_destinationPosition),
							"Start and destination position are equals");

		final HumanPlayer human = (HumanPlayer) _currentPlayer;
		if (!human.isValidCoord(p_destinationPosition))
		{
			return false;
		}

		final FSMap map = _mapMgr.getMap();

		Cell cell = map.getCellAt(p_destinationPosition);
		if (cell.isCity())
		{
			final City city = cell.getCity();
			if (!city.isPortCity())
			{
				return false;
			}

			if (city.getPlayer() == human)
			{
				return true;
			} else if (city.getPlayer() == null)
			{
				return true;
			}
			return false;
		}
		if (!cell.isSeaCell())
		{
			return false;
		}
		return true;
	}

	public final boolean isAbleToMoveHere(final AbstractUnit p_unit, final Coord p_destination)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(!_isGameOver, "The game is finished");
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.preconditionNotNull(p_destination, "Destination");
		Assert.precondition(p_unit.getPosition().calculateIntDistance(p_destination) == 1, "Invalid destination");

		final int move;
		if (p_unit.getFuelCapacity() > 0)
		{
			move = Math.min(p_unit.getFuel(), p_unit.getMove());
		} else
		{
			move = p_unit.getMove();
		}

		if (!_mapMgr.isAccessibleCell(move, p_destination, p_unit.getMoveCost()))
		{
			return false;
		}
		return true;
	}

	/**
	 * Move the unit to the next coord contained within its path.
	 * 
	 * @param p_unit
	 *            Unit to move.
	 * @return Move Status.
	 * @throws PathNotFoundException
	 */
	public final MoveStatusCst performMoveUnitToNextCoord(final AbstractUnit p_unit)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(!_isGameOver, "The game is finished");
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.check(_playerMgr.getCurrentPlayer() instanceof HumanPlayer, "Invalid Current Player");
		Assert.precondition(_currentPlayer.getUnitList().contains(p_unit), "Invalid Player Unit");
		Assert.precondition(p_unit.hasPath(), "This unit doesn't contains a path");

		final Coord nextCoord = p_unit.getPath().getNextStepWithoutConsume();
		if (p_unit.getPlayer().getEnemySet().hasUnitAt(nextCoord))
		{
			_logger.playerAction("Enemy detected at " + nextCoord);
			return MoveStatusCst.ENEMY_DETECTED;
		}

		/*  I put this code to comment on 4-nov-2018 
		if (!isAbleToMoveHere(p_unit, nextCoord))
		{
			return MoveStatusCst.NOT_ENOUGH_MOVE_PTS;
		}
        */
		final HumanPlayer human = (HumanPlayer) _playerMgr.getCurrentPlayer();
		MoveStatusCst moveStatus = human.moveUnit(p_unit);
		if (p_unit.getFuelCapacity() > 0 && p_unit.getFuel() == 0)
		{
			human.killUnit(p_unit);
			moveStatus = MoveStatusCst.UNIT_DEFEATED;
		}
		_isGameOver = _playerMgr.isGameOver();

		return moveStatus;
	}

	public final SingleAttackResult performAttackUnitAt(final AbstractUnit p_unit, final AbstractUnit p_enemy)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(!_isGameOver, "The game is finished");
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.preconditionNotNull(p_enemy, "Enemy");
		Assert.precondition(p_unit.getPosition().calculateIntDistance(p_enemy.getPosition()) == 1,
							"Invalid Move Distance");
		Assert.precondition(p_unit.getPlayer() == _currentPlayer, "Invalid unit player");
		Assert.precondition(p_unit.getPlayer() != p_enemy.getPlayer(), "Invalid Enemy Unit");
		Assert.check(_currentPlayer instanceof HumanPlayer, "Invalid Current Player");

		SingleAttackResult result = p_unit.attackEnnemy(p_enemy);
		_isGameOver = _playerMgr.isGameOver();
		return result;
	}

	public final MoveStatusCst performLoadUnit(	final AbstractGroundLoadableUnit p_loadableUnit,
												TransportShip p_transport)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(!_isGameOver, "The game is finished");
		Assert.preconditionNotNull(p_loadableUnit, "Loadable unit");
		Assert.preconditionNotNull(p_transport, "Transport");
		Assert.precondition(p_transport.isLoadable(p_loadableUnit), "Invalid Transport's Player");

		final HumanPlayer human = (HumanPlayer) p_loadableUnit.getPlayer();
		final MoveStatusCst moveStatus = human.loadUnit(p_loadableUnit, p_transport);

		return moveStatus;
	}

	public final MoveStatusCst performUnloadUnit(final ILoadableUnit p_loadableUnit, final Coord p_destination)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(!_isGameOver, "The game is finished");
		Assert.preconditionNotNull(p_loadableUnit, "loadable unit");
		Assert.preconditionNotNull(p_destination, "Destination");
		Assert.precondition(((AbstractUnit) p_loadableUnit).getPlayer() == _currentPlayer, "Invalid Tank's Player");
		Assert.precondition(p_loadableUnit.isLoaded(), "This tank is not loaded into a Transport Ship");

		final HumanPlayer human = (HumanPlayer) ((AbstractUnit) p_loadableUnit).getPlayer();
		final MoveStatusCst moveStatus = human.unloadUnit(p_loadableUnit, p_destination);
		_isGameOver = _playerMgr.isGameOver();
		return moveStatus;
	}

	public Path getPath(final AbstractUnit p_unit, final Coord p_destinationPosition) throws PathNotFoundException
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(_currentPlayer instanceof HumanPlayer, "Invalid Current Player");
		Assert.preconditionNotNull(p_destinationPosition, "Destination Position");
		Assert.preconditionNotNull(p_unit, "Unit");
		Assert.precondition(p_unit.getPlayer() == _currentPlayer, "Invalid Player Unit");

		final HumanPlayer human = (HumanPlayer) _currentPlayer;

		Assert.precondition(human.isValidCoord(p_unit.getPosition()), "Invalid Start Position");
		Assert.precondition(human.isValidCoord(p_destinationPosition), "Invalid Destination Position");

		// Le path retourn� �vitera les ennemis sauf si l'ennemi cible s'il y a
		// lieu.
		final CoordSet enemyCoordSet = human.getEnemySet().getCoordSet();
		if (!(p_unit instanceof Tank))
		{
			enemyCoordSet.addAll(_mapMgr.getCityList().getEnemyCity(_currentPlayer).getCoordSet());
		}
		Assert.check(!enemyCoordSet.contains(p_unit.getPosition()), "Invalid Start Position");

		// On ajoute au coordonn�es � �viter les zones non-explor�es par le
		// joueur.
		if (!_optionMgr.isDebutMode())
		{
			final CoordSet shadowCoordList = human.getShadowSet();
			enemyCoordSet.addAll(shadowCoordList);
		}

		final Path path = _mapMgr.getPath(p_destinationPosition, enemyCoordSet, p_unit, false);

		return path;
	}

	/**
	 * Termine le tour d'un joueur et retourne l'identifiant du prochain joueur.
	 * 
	 * @return Identifiant du prochain joueur.
	 * @throws FSGameResultException
	 *             Est lanc�e lorsque la partie est termin�e.
	 */
	public final HumanPlayer performNextTurn()
	{
		Assert.check(_currentPlayer != null, "Game not started");
		Assert.check(!_isGameOver, "The game is finished");

		_randomMgr.finishTurn();
		_currentPlayer.finishTurn();

		_logger.systemAction("========= NEXT TURN ========= : " + _playerMgr.getTurnNumber());

		do
		{

			_currentPlayer = _playerMgr.nextPlayer();
			if (_currentPlayer == null)
			{
				_logger.system("The game is over.");
				_isGameOver = true;
				return null;
			}
			if (_currentPlayer instanceof ComputerPlayer)
			{
				_currentPlayer.startTurn(_playerMgr.getTurnNumber());
				_currentPlayer.play();
				_currentPlayer.finishTurn();

			} else
			{
				_currentPlayer.startTurn(_playerMgr.getTurnNumber());
				return (HumanPlayer) _currentPlayer;
			}
		} while (true);
	}

	public final boolean isGameOver()
	{
		Assert.check(_currentPlayer != null, "Game not started");
		return _isGameOver;
	}

	public final AbstractPlayer getWinner()
	{
		Assert.check(_isGameOver, "The game is not over");

		final AbstractPlayerList playerList = _playerMgr.getPlayerList();
		if (playerList.isEmpty())
		{
			return null;
		}

		return playerList.getLivingPlayer().get(0);
	}

	/**
	 * Retourne le num�ro du tour courant.
	 * 
	 * @return Num�ro du tour.
	 */
	public final int getTurnNumber()
	{
		return _playerMgr.getTurnNumber();
	}

	/**
	 * Retourne le gestionnaire de la carte.
	 * 
	 * @return Gestionnaire de la carte.
	 */
	public final FSMapMgr getMapMgr()
	{
		return _mapMgr;
	}

	/**
	 * Retourne le gestionnaire des joueurs.
	 * 
	 * @return Gestionnaire des joueurs.
	 */
	public final PlayerMgr getPlayerMgr()
	{
		return _playerMgr;
	}

	public final FSOptionMgr getOptionMgr()
	{
		return _optionMgr;
	}

	public final void setShortGameName(final String p_shortName)
	{
		_shortName = p_shortName;
	}

	public final String getShortGameName()
	{
		return _shortName;
	}

	public long getSaveDate()
	{
		return _saveDate;
	}

	public void setSaveDate(long p_saveDate)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		_saveDate = p_saveDate;
	}

	public String getUniqueGameName()
	{
		Assert.check(_currentPlayer != null, "Game not started");
		final StringBuffer sb = new StringBuffer(_shortName);
		final String turn = "" + _playerMgr.getTurnNumber();
		sb.append(FIELD_SEPARETOR);
		sb.append(StringManipulatorHelper.leftPadding(turn, 4, '0'));
		sb.append(FIELD_SEPARETOR);
		sb.append(_saveDate);

		return sb.toString();
	}

	@Override
	public int compareTo(Object p_object)
	{
		if (!(p_object instanceof FirstStrikeGame))
		{
			return -1;
		}
		FirstStrikeGame game = (FirstStrikeGame) p_object;
		final String shortName = game.getShortGameName();
		final int result = this.getShortGameName().compareTo(shortName);
		if (result != 0)
		{
			return result;
		}
		final int turn = game.getTurnNumber();
		if (this.getTurnNumber() < turn)
		{
			return -1;
		} else if (this.getTurnNumber() > turn)
		{
			return 1;
		}
		final long time = game.getSaveDate();
		if (this.getSaveDate() < time)
		{
			return -1;
		} else if (this.getSaveDate() > time)
		{
			return 1;
		}

		return 0;
	}

	public void setFrameListener(final IFirstStrikeEvent p_frameListener)
	{
		Assert.preconditionNotNull(p_frameListener, "Frame Listener");

		_frameListener = p_frameListener;
	}

	public IFirstStrikeEvent getFrameListener()
	{
		return _frameListener;
	}

	public void removeFrameListener()
	{
		_frameListener = null;
	}

	@Override
	public void eventCityConquered(City p_city, AbstractPlayer p_newPlayer, AbstractPlayer p_oldPlayer)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		if (_frameListener != null)
		{
			_frameListener.eventCityConquered(p_city, p_newPlayer, p_oldPlayer);
		}
	}

	@Override
	public void eventPlayerEndTurn(AbstractPlayer p_player)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		if (_frameListener != null)
		{
			_frameListener.eventPlayerEndTurn(p_player);
		}
	}

	@Override
	public void eventPlayerStartTurn(AbstractPlayer p_player)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		if (_frameListener != null)
		{
			_frameListener.eventPlayerStartTurn(p_player);
		}
	}

	@Override
	public void eventPlayerKilled(AbstractPlayer p_player)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		// _playerMgr.getPlayerList().remove(p_player);

		_isGameOver = _playerMgr.isGameOver();

		if (_frameListener != null)
		{
			_frameListener.eventPlayerKilled(p_player);
		}

	}

	@Override
	public void eventUnitCreated(AbstractUnit p_unit)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		if (_frameListener != null)
		{
			_frameListener.eventUnitCreated(p_unit);
		}
	}

	@Override
	public void eventUnitKilled(AbstractUnit p_unit)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		// Remove the unit from the enemy list of all other players.
		_playerMgr.killEnemyUnit(p_unit);

		if (_frameListener != null)
		{
			_frameListener.eventUnitKilled(p_unit);
		}
	}

	@Override
	public void eventUnitMoved(AbstractUnit p_unit, Coord p_newPosition, Coord p_oldPosition)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		if (_frameListener != null)
		{
			_frameListener.eventUnitMoved(p_unit, p_newPosition, p_oldPosition);
		}
	}

	@Override
	public void eventUnitLoaded(AbstractUnit p_loadedUnit, AbstractUnit p_unitContainer)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		if (_frameListener != null)
		{
			_frameListener.eventUnitLoaded(p_loadedUnit, p_unitContainer);
		}
	}

	@Override
	public void eventUnitUnloaded(AbstractUnit p_unloadedUnit, AbstractUnit p_unitContainer, Coord p_destination)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		if (_frameListener != null)
		{
			_frameListener.eventUnitUnloaded(p_unloadedUnit, p_unitContainer, p_destination);
		}
	}

	@Override
	public void eventUnitFinished(AbstractUnit p_unit)
	{
		Assert.check(_currentPlayer != null, "Game not started");
		p_unit.markAsTurnCompleted();
		if (_frameListener != null)
		{
			_frameListener.eventUnitFinished(p_unit);
		}
	}
}