package org.jocelyn_chaumel.firststrike.gui.commun;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;
import java.io.File;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * This class set the location of the dialog according to the main frame.
 * 
 * @author Joss
 */
public class FSDialog extends JDialog
{
	public static final short WIZARD_STATUS_CANCEL = -1;
	public static final short WIZARD_STATUS_PREVIOUS = 0;
	public static final short WIZARD_STATUS_NEXT = 1;

	private final Window _frame;
	private short _wizardStatus = WIZARD_STATUS_NEXT;

	/**
	 * Constructor. Getting the main frame's location & size to permit the
	 * relocation of the dialog according to main frame.
	 * 
	 * @param p_frame Main frame.
	 * @param p_title Title of the dialog.
	 * @param p_modal Dialog's state.
	 */
	public FSDialog(final JFrame p_frame, final String p_title, final boolean p_modal)
	{
		super(p_frame, p_title, p_modal);
		_frame = p_frame;
	}

	public FSDialog(final JDialog p_dialog, final String p_title, final boolean p_modal)
	{
		super(p_dialog, p_title, p_modal);
		_frame = p_dialog;
	}

	@Override
	public void setVisible(final boolean p_isVisible)
	{
		if (p_isVisible)
		{
			setDialogLocation();
		}
		super.setVisible(p_isVisible);
	}

	/**
	 * Set the dialog's location according to the main frame.
	 */
	private void setDialogLocation()
	{
		// final int dialogHeight = this.getPreferredSize().height;
		// final int dialogWidth = this.getPreferredSize().width;
		final int dialogHeight = this.getSize().height;
		final int dialogWidth = this.getSize().width;
		final Dimension frameDim = _frame.getSize();
		final int frameHeight = frameDim.height;
		final int frameWidth = frameDim.width;

		int relativeX = (frameWidth - dialogWidth) / 2;
		int relativeY = (frameHeight - dialogHeight) / 2;
		if (dialogWidth > frameWidth)
		{
			relativeX *= -1;
		}

		if (dialogHeight > frameHeight)
		{
			relativeY *= -1;
		}
		final Point point = _frame.getLocation();
		final int x = point.x;
		final int y = point.y;
		setLocation((int) x + (int) relativeX, (int) y + (int) relativeY);
	}

	public File browseDrive(final String p_title, final File p_currentDir)
	{
		final JFileChooser dialog = new JFileChooser();
		dialog.setDialogTitle(p_title);
		dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if (p_currentDir != null)
		{
			dialog.setCurrentDirectory(p_currentDir);
		}
		final int returnVal = dialog.showOpenDialog(this);

		if (returnVal != JFileChooser.APPROVE_OPTION)
		{
			return null;
		}

		return dialog.getSelectedFile();
	}

	public final void setWizardStatus(final short p_ws)
	{
		Assert.precondition(p_ws == WIZARD_STATUS_CANCEL || p_ws == WIZARD_STATUS_PREVIOUS
				|| p_ws == WIZARD_STATUS_NEXT, "Invalid Wizard Status");

		_wizardStatus = p_ws;
	}

	public final short getWizardStatus()
	{
		return _wizardStatus;
	}

}
