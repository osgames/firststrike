/*
 * Created on Dec 11, 2005
 */
package org.jocelyn_chaumel.firststrike.gui.commun;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;

/**
 * @author Jocelyn Chaumel
 */
public final class ColumnHeaderRenderer implements TableCellRenderer
{
	private final Color _headerForeground;
	private final Color _headerBackground;
	private final Font _headerFont;

	/**
	 * @param p_table
	 */
	public ColumnHeaderRenderer(final JTable p_table)
	{
		final JTableHeader header = p_table.getTableHeader();
		_headerForeground = header.getForeground();
		_headerBackground = header.getBackground();
		_headerFont = header.getFont();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax
	 * .swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	public Component getTableCellRendererComponent(	JTable p_table,
													Object p_value,
													boolean anIsSelectedFlag,
													boolean anHasFocusFlag,
													int p_row,
													int p_column)
	{
		final JLabel label = new JLabel("" + p_column, SwingConstants.CENTER);
		label.setOpaque(true);
		label.setBorder(UIManager.getBorder("TableHeader.cellBorder"));

		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(_headerForeground);
		label.setBackground(_headerBackground);
		label.setFont(_headerFont);
		Dimension dim = label.getPreferredSize();
		dim.width = IconFactory.CELL_SIZE;

		label.setMaximumSize(dim);
		label.setMinimumSize(dim);
		return label;
	}
}
