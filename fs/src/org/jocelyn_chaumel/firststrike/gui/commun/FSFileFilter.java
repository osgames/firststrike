package org.jocelyn_chaumel.firststrike.gui.commun;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import org.jocelyn_chaumel.tools.FileMgr;

/**
 * This is used to display only the First Strike's game file.
 * 
 * @author Joss
 * 
 */
public class FSFileFilter extends FileFilter
{
	public final static String FIRST_STRIKE_MAP_EXTENSION = "fsm";
	public final static String FIRST_STRIKE_MAP_DESC = "First Strike Map (FSM)";

	public final static String FIRSTSTRIKE_FILE_EXTENTION = "fsg";
	public final static String FIRSTSTRIKE_FILE_DESC = "First Strike Game (FSG)";

	private final String _extenstion;
	private final String _description;

	public FSFileFilter(final String p_extension, final String p_description)
	{
		_extenstion = p_extension;
		_description = p_description;
	}

	/**
	 * Indicates if the file is a First Strike's game file.
	 * 
	 * @param p_pathname file to inspect.
	 */
	@Override
	public boolean accept(final File p_pathname)
	{
		if (p_pathname.isDirectory())
		{
			return true;
		}

		final String fileExtention = FileMgr.getFileExtention(p_pathname).toLowerCase();
		return fileExtention.matches(_extenstion + "[0-9]?[0-9]?[0-9]?[0-9]?");
	}

	/**
	 * Returns the description
	 */
	@Override
	public String getDescription()
	{
		// return "First Strike Game (FSG)";
		return _description;
	}

}
