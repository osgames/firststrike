/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jocelyn_chaumel.firststrike.gui.commun.list_cell_renderer;

import java.awt.Component;

import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.jocelyn_chaumel.firststrike.core.map.Cell;

/**
 * 
 * @author Client
 */
public class FSTwoIconsListCellRenderer extends FSTwoIconsListCellPanel implements ListCellRenderer
{

	@Override
	public Component getListCellRendererComponent(	JList list,
													Object p_cell,
													int index,
													boolean isSelected,
													boolean cellHasFocus)
	{
		initComponents(list, (Cell) p_cell, isSelected || cellHasFocus);

		return this;
	}

}
