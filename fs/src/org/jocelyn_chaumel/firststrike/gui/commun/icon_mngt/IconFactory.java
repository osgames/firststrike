/*
 * Created on Dec 10, 2005
 */
package org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.InvalidMapException;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerId;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.StringManipulatorHelper;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class IconFactory
{
	public final static String GIF_EXTENSION = ".gif";
	public final static String JPG_EXTENSION = ".jpg";
	public final static String PNG_EXTENSION = ".png";
	public final static String FOG_FILE = "_fog";
	public final static int CELL_1 = 0;
	public final static int CELL_9 = 1;
	public final static int CELL_25 = 2;

	public final static int SIZE_SIMPLE_SIZE = 1;
	public final static int SIZE_DOUBLE_SIZE = 2;
	public final static int SIZE_UNIQUE_SIZE = 0;
	public final static int CELL_SIZE = 20;

	// D�finition des noms des fichiers contenant les images du jeu.
	public final static String SEA = "sea";

	public final static String RIVER = "river";

	public final static String GRASSLAND = "grassland";

	public final static String SHADOW = "shadow";

	public final static String CITY = "city";

	public final static String HILL = "hill";

	public final static String MOUNTAIN = "mountain";

	public final static String LIGHT_WOOD = "light_wood";

	public final static String HEAVY_WOOD = "heavy_wood";

	public final static String ROAD = "road";

	public final static String RESOURCES_PACKAGE = "org/jocelyn_chaumel/firststrike/resources/";

	public final static int MAX_PLAYER = 2;

	public final static int NO_FOG = 1;
	public final static int FOG = 2;

	private final IconsContainer _defaultSkinIconsContainer;
	private IconsContainer _userSkinIconsContainer = null;

	private final static FSLogger _logger = FSLoggerManager.getLogger(IconFactory.class);

	private static IconFactory _instance;

	public final static ArrayList<String> _singleCommunImageNameList = new ArrayList<String>();
	public final static String COMMUN_RECURSIVE_ICON = "recursive";
	public final static String COMMUN_FS_ICON = "fs";
	public final static String COMMON_INVISIBLE_SPACE = "invisible_space";
	public final static String COMMON_NEXT_TURN = "next_turn";
	public final static String COMMON_AUTO_MOVE_SLOWER_ON = "auto_move_slower_on";
	public final static String COMMON_AUTO_MOVE_ON = "auto_move_on";
	public final static String COMMON_AUTO_MOVE_OFF = "auto_move_off";
	public final static String COMMON_AUTO_MOVE_FASTER_ON = "auto_move_faster_on";
	public final static String COMMON_AUTO_MOVE_PAUSE_ON_DETECT_ON = "auto_move_pause_on_detection_on";
	public final static String COMMON_AUTO_MOVE_PAUSE_ON_DETECT_OFF = "auto_move_pause_on_detection_off";
	public final static String COMMON_FOOTPRINT = "footprint";
	public final static String COMMON_SHIELD = "shield";
	public final static String COMMON_ATTACK = "attack";
	public final static String COMMON_EXPLOSION = "explosion";
	public final static String COMMON_BOMBARDMENT = "bombardment";
	public final static String COMMON_BOMBARDED = "bombarded";
	public final static String COMMON_PLUS = "+";
	public final static String COMMON_MINUS = "-";
	public final static String UNIT_STATUS_OK = "unitStatus_i1";
	public final static String UNIT_STATUS_KO = "unitStatus_i2";
	public final static String CURSOR_TANK = "cursor_tank";
	public final static String CURSOR_ARTILLERY = "cursor_artillery";
	public final static String CURSOR_FIGHTER = "cursor_fighter";
	public final static String CURSOR_TRANSPORT = "cursor_transport";
	public final static String CURSOR_DESTROYER = "cursor_destroyer";
	public final static String CURSOR_BATTLESHIP = "cursor_battleship";
	public final static String CURSOR_INVALID = "cursor_invalid";
	public final static String CURSOR_ATTACK = "cursor_attack";
	public final static String CURSOR_LOADING = "cursor_loading";
	
	private final static RescaleOp _rescale = new RescaleOp(0.4f, 60.0f, null);

	private static final String[] IconNameArray = new String[] {
			// Map Builder Icons
			// ----------------
			"map_builder" + File.separator + "cell_01x_fog",
			"map_builder" + File.separator + "cell_01x",
			"map_builder" + File.separator + "cell_09x_fog",
			"map_builder" + File.separator + "cell_09x",
			"map_builder" + File.separator + "cell_25x_fog",
			"map_builder" + File.separator + "cell_25x",
			// Unit icon name
			// ----------------
			// 20x20
			"size_20x20" + File.separator + "unit" + File.separator + "tank_p1",
			"size_20x20" + File.separator + "unit" + File.separator + "tank_p2",
			"size_20x20" + File.separator + "unit" + File.separator + "fighter_p1",
			"size_20x20" + File.separator + "unit" + File.separator + "fighter_p2",
			"size_20x20" + File.separator + "unit" + File.separator + "destroyer_p1",
			"size_20x20" + File.separator + "unit" + File.separator + "destroyer_p2",
			"size_20x20" + File.separator + "unit" + File.separator + "transport_p1",
			"size_20x20" + File.separator + "unit" + File.separator + "transport_p2",
			"size_20x20" + File.separator + "unit" + File.separator + "battleship_p1",
			"size_20x20" + File.separator + "unit" + File.separator + "battleship_p2",
			"size_20x20" + File.separator + "unit" + File.separator + "artillery_p1",
			"size_20x20" + File.separator + "unit" + File.separator + "artillery_p2",
			"size_20x20" + File.separator + "unit" + File.separator + "submarin_p1",
			"size_20x20" + File.separator + "unit" + File.separator + "submarin_p2",
			"size_20x20" + File.separator + "unit" + File.separator + "carrier_p1",
			"size_20x20" + File.separator + "unit" + File.separator + "carrier_p2",
			"size_20x20" + File.separator + "unit" + File.separator + "radar_p1",
			"size_20x20" + File.separator + "unit" + File.separator + "radar_p2",
			// 40x40
			"size_40x40" + File.separator + "unit" + File.separator + "tank_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "tank_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "fighter_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "fighter_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "destroyer_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "destroyer_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "transport_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "transport_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "battleship_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "battleship_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "artillery_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "artillery_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "radar_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "radar_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_tank_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_tank_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_fighter_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_fighter_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_destroyer_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_destroyer_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_transport_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_transport_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_battleship_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_battleship_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_artillery_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_artillery_p2",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_radar_p1",
			"size_40x40" + File.separator + "unit" + File.separator + "killed_radar_p2",
			// ground icon name
			// ----------------
			// 20x20
			"size_20x20" + File.separator + "shadow",
			"size_20x20" + File.separator + "land" + File.separator + "sea",
			"size_20x20" + File.separator + "land" + File.separator + "river",
			"size_20x20" + File.separator + "land" + File.separator + "grassland",
			"size_20x20" + File.separator + "land" + File.separator + "light_wood",
			"size_20x20" + File.separator + "land" + File.separator + "heavy_wood",
			"size_20x20" + File.separator + "land" + File.separator + "hill",
			"size_20x20" + File.separator + "land" + File.separator + "mountain",
			"size_20x20" + File.separator + "land" + File.separator + "city",
			"size_20x20" + File.separator + "land" + File.separator + "road",

			// 40x40
			"size_40x40" + File.separator + "shadow",
			"size_40x40" + File.separator + "land" + File.separator + "sea",
			"size_40x40" + File.separator + "land" + File.separator + "river",
			"size_40x40" + File.separator + "land" + File.separator + "grassland",
			"size_40x40" + File.separator + "land" + File.separator + "light_wood",
			"size_40x40" + File.separator + "land" + File.separator + "heavy_wood",
			"size_40x40" + File.separator + "land" + File.separator + "hill",
			"size_40x40" + File.separator + "land" + File.separator + "mountain",
			"size_40x40" + File.separator + "land" + File.separator + "city",
			"size_40x40" + File.separator + "land" + File.separator + "road",

			// Cell Type
			"cell-type_" + CITY,
			"cell-type_" + GRASSLAND,
			"cell-type_" + HEAVY_WOOD,
			"cell-type_" + HILL,
			"cell-type_" + LIGHT_WOOD,
			"cell-type_" + MOUNTAIN,
			"cell-type_" + RIVER,
			"cell-type_" + SEA,
			"cell-type_" + ROAD,

			// direction
			"direction_i1",
			"direction_i2",
			"direction_i3",
			"direction_i4",
			"direction_i5",
			"direction_i6",
			"direction_i7",
			"direction_i8",
			"direction_i9",

			// cursor
			CURSOR_TANK,
			CURSOR_ARTILLERY,
			CURSOR_FIGHTER,
			CURSOR_DESTROYER,
			CURSOR_BATTLESHIP,
			CURSOR_TRANSPORT,
			CURSOR_INVALID,
			CURSOR_ATTACK,
			CURSOR_LOADING,

			// status
			UNIT_STATUS_OK,
			UNIT_STATUS_KO,

			// help
			"help" + File.separator + "help_left_panel",
			"help" + File.separator + "help_tank", "help" + File.separator + "help_fighter",
			"help" + File.separator + "help_transport", "help" + File.separator + "help_destroyer",
			"help" + File.separator + "help_artillery", "help" + File.separator + "help_battleship",
			"help" + File.separator + "help_cities",
			"help" + File.separator + "help_move_unit", "help" + File.separator + "help_fog",
			"help" + File.separator + "help_fog_move", "help" + File.separator + "help_attack",
			"help" + File.separator + "help_path_and_trace_path", 
			"help" + File.separator + "help_load_tank", "help" + File.separator + "help_unload_action",
			"help" + File.separator + "fs_frontpage",
			// other icon name
			// ----------------
			COMMUN_FS_ICON, COMMUN_RECURSIVE_ICON, COMMON_INVISIBLE_SPACE, COMMON_NEXT_TURN, COMMON_AUTO_MOVE_OFF, COMMON_AUTO_MOVE_ON,
			COMMON_AUTO_MOVE_SLOWER_ON, COMMON_AUTO_MOVE_FASTER_ON, COMMON_AUTO_MOVE_PAUSE_ON_DETECT_ON, COMMON_AUTO_MOVE_PAUSE_ON_DETECT_OFF, 
			COMMON_FOOTPRINT, COMMON_SHIELD, COMMON_ATTACK, COMMON_EXPLOSION, COMMON_BOMBARDMENT, COMMON_BOMBARDED, COMMON_PLUS, COMMON_MINUS };

	protected IconFactory() throws IOException
	{
		_logger.systemAction("Pictures loading");
		_logger.systemSubAction("Picture loaded from FirstStrike's resources");

		_defaultSkinIconsContainer = loadIcons(null);

		_instance = this;
	}

	private IconsContainer loadIcons(final File p_iconDir)
	{
		final IconsContainer iconsContainer = new IconsContainer();

		ImageIcon icon = null;
		int index;
		for (int i = 0; i < IconNameArray.length; i++)
		{
			if (IconNameArray[i].startsWith("size_20x20" + File.separator + "land")
					|| IconNameArray[i].startsWith("size_40x40" + File.separator + "land"))
			{
				index = 0;
				do
				{
					index++;
					icon = loadIcon(p_iconDir, IconNameArray[i] + "_i" + index, iconsContainer);
					if (icon != null)
					{
						BufferedImage bi = new BufferedImage(	icon.getIconWidth(),
																icon.getIconHeight(),
																BufferedImage.TYPE_INT_RGB);
						final Graphics graphics = bi.getGraphics();
						graphics.drawImage(icon.getImage(), 0, 0, null);
						graphics.dispose();

						BufferedImage biOut = _rescale.filter(bi, null);
						iconsContainer.addIcon(IconNameArray[i] + "_fog_i" + index, new ImageIcon(biOut));
					}
				} while (icon != null);
				if (index == 1 && p_iconDir == null)
				{
					throw new UnsupportedOperationException("Unable to find this icon (.gif|.jpg): " + IconNameArray[i]
							+ "_i1");
				}
			} else
			{
				icon = loadIcon(p_iconDir, IconNameArray[i], iconsContainer);
				if (icon == null && p_iconDir == null)
				{
					throw new UnsupportedOperationException("Unable to find this icon (.gif|.jpg): " + IconNameArray[i]);

				}
			}
		}
		return iconsContainer;
	}

	private ImageIcon loadIcon(final File p_iconDir, final String p_iconName, final IconsContainer p_iconsContainer)
	{
		final ImageIcon icon;
		if (p_iconDir == null)
		{
			icon = loadFromRessource(p_iconName);
		} else
		{
			icon = loadFromFile(p_iconName, p_iconDir);
		}

		if (icon != null)
		{
			p_iconsContainer.addIcon(p_iconName, icon);
		}

		return icon;
	}

	public final void setUserSkin(final File p_imageDirectorySource)
	{
		if (p_imageDirectorySource == null)
		{
			_userSkinIconsContainer = null;
			return;
		}
		Assert.preconditionIsDir(p_imageDirectorySource);

		_logger.systemSubAction("Pictures loaded from user's directory : " + p_imageDirectorySource.getPath());

		_userSkinIconsContainer = loadIcons(p_imageDirectorySource);
	}

	private ImageIcon loadFromFile(final String p_pictureName, final File p_iconsDir)
	{
		String filePath = p_iconsDir.getAbsolutePath() + File.separator + p_pictureName;

		final File file = checkForFile(filePath);
		if (file == null)
		{
			return null;
		}

		return loadFromFile(file);
	}

	private ImageIcon loadFromFile(final File p_imageFile)
	{
		URL url;
		try
		{
			url = p_imageFile.toURL();
		} catch (MalformedURLException ex)
		{
			throw new UnsupportedOperationException(ex);
		}
		final ImageIcon icon = new ImageIcon(url);
		return icon;

	}

	private ImageIcon loadFromRessource(final String p_pictureName)
	{
		final String stringUrl = RESOURCES_PACKAGE
				+ StringManipulatorHelper.replace(p_pictureName, File.separator, "/");

		URL url;
		url = ClassLoader.getSystemResource(stringUrl + GIF_EXTENSION);
		if (url != null)
		{
			return new ImageIcon(url);
		}
		url = ClassLoader.getSystemResource(stringUrl + JPG_EXTENSION);
		if (url != null)
		{
			return new ImageIcon(url);
		}
		url = ClassLoader.getSystemResource(stringUrl + PNG_EXTENSION);
		if (url != null)
		{
			return new ImageIcon(url);
		}

		return null;
	}

	private final File checkForFile(final String p_fileName)
	{
		Assert.preconditionNotNull(p_fileName, "File name");

		// check if GIF extension exists
		File file = new File(p_fileName + GIF_EXTENSION);
		if (file.isFile())
		{
			return file;
		}

		file = new File(p_fileName + JPG_EXTENSION);
		if (file.isFile())
		{
			return file;
		}
		final String message = "Unable to find this file : '" + p_fileName + "'";
		_logger.systemSubActionDetail(message);
		return null;
	}

	public static final IconFactory getInstance()
	{
		Assert.check(_instance != null, "Invalid ImageFactory");
		return _instance;
	}

	public static final IconFactory createInstance() throws IOException
	{
		Assert.check(_instance == null, "UIComponent instance already created");

		_instance = new IconFactory();

		return _instance;
	}

	public final int getSimpleCellIconMaxIndex(final CellTypeCst p_cellTypeCst, final int p_hasFog, final int p_size)
	{

		if (_userSkinIconsContainer != null)
		{
			int maxSize = _userSkinIconsContainer.getCellIconMaxIndex(p_hasFog, p_size, p_cellTypeCst);
			if (maxSize > 0)
			{
				return maxSize;
			}
		}

		return _defaultSkinIconsContainer.getCellIconMaxIndex(p_hasFog, p_size, p_cellTypeCst);
	}

	public final ImageIcon getSimpleCellIcon(	final CellTypeCst p_cellTypeCst,
												final int p_hasFog,
												final int p_size,
												final int p_index)
	{
		Assert.precondition(p_index >= 1, "Invalid Index");
		ImageIcon currentIcon = null;
		if (_userSkinIconsContainer != null)
		{
			currentIcon = _userSkinIconsContainer.getCellIcon(p_hasFog, p_size, p_cellTypeCst, p_index, true);
		}

		if (currentIcon == null)
		{
			currentIcon = _defaultSkinIconsContainer.getCellIcon(p_hasFog, p_size, p_cellTypeCst, p_index, true);
		}

		if (currentIcon == null && p_index > 1)
		{
			currentIcon = _defaultSkinIconsContainer.getCellIcon(p_hasFog, p_size, p_cellTypeCst, 1, true);
		}

		if (currentIcon == null)
		{
			StringBuilder sb = new StringBuilder();
			sb.append("Image not found=");
			sb.append(" cellType:").append(p_cellTypeCst.getLabel());
			sb.append(" size:").append(p_size);
			sb.append(" fog:").append(p_hasFog);
			throw new FSFatalException(sb.toString());
		}

		return currentIcon;
	}

	public final ImageIcon getContextualCellIconByPlayer(	final Cell p_currentCell,
															final HumanPlayer p_player,
															final int p_fogIdx,
															final int p_size,
															final int p_index,
															final boolean isDebugModeEnabled)
	{
		Assert.precondition(p_index >= 1, "Invalid Index");

		ImageIcon currentIcon = null;
		if (_userSkinIconsContainer != null)
		{
			currentIcon = _userSkinIconsContainer.getContextualCellIcon(p_currentCell,
																		p_player,
																		p_fogIdx,
																		p_size,
																		p_index,
																		isDebugModeEnabled);
		}
		if (currentIcon == null)
		{
			currentIcon = _defaultSkinIconsContainer.getContextualCellIcon(	p_currentCell,
																			p_player,
																			p_fogIdx,
																			p_size,
																			p_index,
																			isDebugModeEnabled);
		}

		if (currentIcon == null && p_index > 1)
		{
			currentIcon = _defaultSkinIconsContainer.getContextualCellIcon(	p_currentCell,
																			p_player,
																			p_fogIdx,
																			p_size,
																			1,
																			isDebugModeEnabled);
		}
		return currentIcon;
	}

	public final ImageIcon getUnitIconByType(	final UnitTypeCst p_unitType,
												final PlayerId p_playerId,
												final int p_sizeIdx, final boolean p_isLiving)
	{
		Assert.preconditionNotNull(p_playerId, "Player Id");
		final int playerId = p_playerId.getId();

		Assert.precondition_between(playerId, 1, MAX_PLAYER, "Invalid Player Id");

		ImageIcon currentIcon = null;
		if (_userSkinIconsContainer != null)
		{
			currentIcon = _userSkinIconsContainer.getUnitIcon(p_unitType, playerId, p_sizeIdx, p_isLiving);
		}
		if (currentIcon == null)
		{
			currentIcon = _defaultSkinIconsContainer.getUnitIcon(p_unitType, playerId, p_sizeIdx, p_isLiving);
		}
		return currentIcon;
	}

	public final ImageIcon getCellModifier(final int p_cellModifierIdx)
	{
		ImageIcon currentIcon = null;
		if (_userSkinIconsContainer != null)
		{
			currentIcon = _userSkinIconsContainer.getCellModifier(p_cellModifierIdx);
		}
		if (currentIcon == null)
		{
			currentIcon = _defaultSkinIconsContainer.getCellModifier(p_cellModifierIdx);
		}
		return currentIcon;
	}

	public final ImageIcon getSelectedCellModifier(final int p_cellModifierIdx)
	{
		{
			ImageIcon currentIcon = null;
			if (_userSkinIconsContainer != null)
			{
				currentIcon = _userSkinIconsContainer.getSelectedCellModifier(p_cellModifierIdx);
			}
			if (currentIcon == null)
			{
				currentIcon = _defaultSkinIconsContainer.getSelectedCellModifier(p_cellModifierIdx);
			}
			return currentIcon;
		}
	}

	public final ImageIcon getOrientionIcon(final int p_idx)
	{
		ImageIcon currentIcon = null;
		if (_userSkinIconsContainer != null)
		{
			currentIcon = _userSkinIconsContainer.getOrientationIcon(p_idx);
		}

		if (currentIcon == null)
		{
			currentIcon = _defaultSkinIconsContainer.getOrientationIcon(p_idx);
		}

		return currentIcon;
	}

	public final ImageIcon getPictureMap(final String p_mapName, final String p_pictureName) throws InvalidMapException
	{
		final ApplicationConfigurationMgr appConf = ApplicationConfigurationMgr.getInstance();
		final File mapDir = appConf.getMapDir();
		final File pictureFile = new File(mapDir, p_mapName + "/pictures/" + p_pictureName);

		if (!pictureFile.isFile())
		{
			throw new InvalidMapException("Invalid picture nanme: " + pictureFile.getAbsolutePath());
		}

		ImageIcon image = null;
		try
		{
			image = loadFromFile(pictureFile);
		} catch (Exception ex)
		{
			InvalidMapException exToThrow = new InvalidMapException("Invalid picture: " + pictureFile.getAbsolutePath(),
																	ex);
			_logger.gameError(exToThrow.getMessage(), exToThrow);
			throw exToThrow;
		}

		return image;
	}

	public final ImageIcon getCommunIcon(final String p_iconName)
	{
		ImageIcon currentIcon = null;
		if (_userSkinIconsContainer != null)
		{
			currentIcon = _userSkinIconsContainer.getOtherIcon(p_iconName);
		}

		if (currentIcon == null)
		{
			currentIcon = _defaultSkinIconsContainer.getOtherIcon(p_iconName);
		}

		return currentIcon;
	}

	public final ImageIcon getHelpIcon(final String p_iconName)
	{
		ImageIcon currentIcon = null;
		if (_userSkinIconsContainer != null)
		{
			currentIcon = _userSkinIconsContainer.getHelpIcon(p_iconName);
		}

		if (currentIcon == null)
		{
			currentIcon = _defaultSkinIconsContainer.getHelpIcon(p_iconName);
		}

		return currentIcon;
	}

	public final ImageIcon getCellTypeIcon(final CellTypeCst p_cellType)
	{
		ImageIcon currentIcon = null;
		if (_userSkinIconsContainer != null)
		{
			currentIcon = _userSkinIconsContainer.getCellTypeIcon(p_cellType);
		}

		if (currentIcon == null)
		{
			currentIcon = _defaultSkinIconsContainer.getCellTypeIcon(p_cellType);
		}

		return currentIcon;
	}
}