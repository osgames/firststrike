package org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt;

import java.io.File;
import java.util.HashMap;

import javax.swing.ImageIcon;

import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class IconsContainer
{
	// private final static FSLogger _logger =
	// FSLoggerManager.getLogger(IconsContainer.class.getName());
	private HashMap<String, ImageIcon> _imageMap = new HashMap<String, ImageIcon>();

	public final ImageIcon getUnitIcon(final UnitTypeCst p_unitType, final int p_playerIdx, final int p_sizeIdx, final boolean p_isLiving)
	{
		String iconName = translateSizeIdx(p_sizeIdx) + File.separator + "unit" + File.separator;
		String prefix;
		if (p_isLiving)
		{
			prefix = "";
		} else {
			prefix = "killed_";
		}
		
		if (UnitTypeCst.TANK.equals(p_unitType))
		{
			iconName = iconName + prefix + "tank_p" + p_playerIdx;
			return _imageMap.get(iconName);
		} else if (UnitTypeCst.FIGHTER.equals(p_unitType))
		{
			iconName = iconName + prefix + "fighter_p" + p_playerIdx;
			return _imageMap.get(iconName);
		} else if (UnitTypeCst.DESTROYER.equals(p_unitType))
		{
			iconName = iconName + prefix + "destroyer_p" + p_playerIdx;
			return _imageMap.get(iconName);
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_unitType))
		{
			iconName = iconName + prefix + "transport_p" + p_playerIdx;
			return _imageMap.get(iconName);
		} else if (UnitTypeCst.BATTLESHIP.equals(p_unitType))
		{
			iconName = iconName + prefix + "battleship_p" + p_playerIdx;
			return _imageMap.get(iconName);
		} else if (UnitTypeCst.ARTILLERY.equals(p_unitType))
		{
			iconName = iconName + prefix + "artillery_p" + p_playerIdx;
			return _imageMap.get(iconName);
		} else if (UnitTypeCst.RADAR.equals(p_unitType))
		{
			iconName = iconName + prefix + "radar_p" + p_playerIdx;
			return _imageMap.get(iconName);
			// } else if (UnitTypeCst.SUBMARINE.equals(p_unitType))
			// {
			// iconName = iconName + "submarine_p" + p_playerIdx;
			// return _imageMap.get(iconName);
			// } else if (UnitTypeCst.CARRIER.equals(p_unitType))
			// {
			// iconName = iconName + "carrier_p" + p_playerIdx;
			// return _imageMap.get(iconName);
		} else
		{
			throw new IllegalArgumentException("Unit Type unknown : " + p_unitType);
		}
	}

	private String translateSizeIdx(final int p_sizeIdx)
	{
		if (p_sizeIdx == IconFactory.SIZE_SIMPLE_SIZE)
		{
			return "size_20x20";
		} else if (p_sizeIdx == IconFactory.SIZE_DOUBLE_SIZE)
		{
			return "size_40x40";
		} else if (p_sizeIdx == IconFactory.SIZE_UNIQUE_SIZE)
		{
			return "";
		} else
		{
			throw new IllegalArgumentException("Unknown Size Idx");
		}
	}

	private String translateFogIdx(final int p_fogIdx, final boolean p_withPrefix)
	{
		if (p_fogIdx == IconFactory.FOG)
		{
			if (p_withPrefix)
			{
				return "_fog";
			} else
			{
				return "fog";

			}
		} else if (p_fogIdx == IconFactory.NO_FOG)
		{
			return "";
		} else
		{
			throw new IllegalArgumentException("Unknown fog idx");
		}
	}

	public final ImageIcon getOrientationIcon(final int p_idx)
	{
		final String iconName = "direction_i" + p_idx;
		return _imageMap.get(iconName);
	}

	public final int getCellIconMaxIndex(final int p_fogIdx, final int p_sizeIdx, final CellTypeCst p_cellType)
	{
		ImageIcon icon = getCellIcon(p_fogIdx, p_sizeIdx, p_cellType, 1, false);
		if (icon == null)
		{
			return 0;
		}

		int index = 1;
		while (icon != null)
		{

			icon = getCellIcon(p_fogIdx, p_sizeIdx, p_cellType, index + 1, false);
			if (icon == null)
			{
				return index;
			} else
			{
				++index;
			}
		}

		return index;
	}

	public final ImageIcon getCellTypeIcon(final CellTypeCst p_cellType)
	{
		if (CellTypeCst.CITY.equals(p_cellType))
		{
			return _imageMap.get("cell-type_" + IconFactory.CITY);
		} else if (CellTypeCst.GRASSLAND.equals(p_cellType))
		{
			return _imageMap.get("cell-type_" + IconFactory.GRASSLAND);
		} else if (CellTypeCst.HEAVY_WOOD.equals(p_cellType))
		{
			return _imageMap.get("cell-type_" + IconFactory.HEAVY_WOOD);
		} else if (CellTypeCst.ROAD.equals(p_cellType))
		{
			return _imageMap.get("cell-type_" + IconFactory.ROAD);
		} else if (CellTypeCst.HILL.equals(p_cellType))
		{
			return _imageMap.get("cell-type_" + IconFactory.HILL);
		} else if (CellTypeCst.LIGHT_WOOD.equals(p_cellType))
		{
			return _imageMap.get("cell-type_" + IconFactory.LIGHT_WOOD);
		} else if (CellTypeCst.MOUNTAIN.equals(p_cellType))
		{
			return _imageMap.get("cell-type_" + IconFactory.MOUNTAIN);
		} else if (CellTypeCst.RIVER.equals(p_cellType))
		{
			return _imageMap.get("cell-type_" + IconFactory.RIVER);
		} else if (CellTypeCst.SEA.equals(p_cellType))
		{
			return _imageMap.get("cell-type_" + IconFactory.SEA);
		} else
		{
			throw new IllegalArgumentException("Cell Type Unknown :" + p_cellType);
		}
	}

	public final ImageIcon getCellIcon(	final int p_fogIdx,
										final int p_sizeIdx,
										final CellTypeCst p_cellType,
										final int p_index,
										final boolean p_defaultIconOption)
	{
		Assert.precondition(p_index >= 1, "Invalid Index");

		String iconName = null;
		ImageIcon icon = null;
		if (CellTypeCst.SHADOW.equals(p_cellType))
		{
			iconName = translateSizeIdx(p_sizeIdx) + File.separator + "shadow";
			icon = _imageMap.get(iconName);
			return icon;
		}

		String iconDir = translateSizeIdx(p_sizeIdx) + File.separator + "land" + File.separator;
		if (CellTypeCst.GRASSLAND.equals(p_cellType))
		{
			iconName = iconDir + "grassland" + translateFogIdx(p_fogIdx, true) + "_i" + p_index;
		} else if (CellTypeCst.SEA.equals(p_cellType))
		{
			iconName = iconDir + "sea" + translateFogIdx(p_fogIdx, true) + "_i" + p_index;
		} else if (CellTypeCst.MOUNTAIN.equals(p_cellType))
		{
			iconName = iconDir + "mountain" + translateFogIdx(p_fogIdx, true) + "_i" + p_index;
		} else if (CellTypeCst.RIVER.equals(p_cellType))
		{
			iconName = iconDir + "river" + translateFogIdx(p_fogIdx, true) + "_i" + p_index;
		} else if (CellTypeCst.LIGHT_WOOD.equals(p_cellType))
		{
			iconName = iconDir + "light_wood" + translateFogIdx(p_fogIdx, true) + "_i" + p_index;
		} else if (CellTypeCst.HEAVY_WOOD.equals(p_cellType))
		{
			iconName = iconDir + "heavy_wood" + translateFogIdx(p_fogIdx, true) + "_i" + p_index;
		} else if (CellTypeCst.HILL.equals(p_cellType))
		{
			iconName = iconDir + "hill" + translateFogIdx(p_fogIdx, true) + "_i" + p_index;
		} else if (CellTypeCst.CITY.equals(p_cellType))
		{
			iconName = iconDir + "city" + translateFogIdx(p_fogIdx, true) + "_i" + p_index;
		} else if (CellTypeCst.ROAD.equals(p_cellType))
		{
			iconName = iconDir + "road" + translateFogIdx(p_fogIdx, true) + "_i" + p_index;
		} else
		{
			throw new IllegalArgumentException("Cell Type Unknown :" + p_cellType);
		}

		icon = _imageMap.get(iconName);
		return icon;
	}

	public final ImageIcon getContextualCellIcon(	final Cell p_currentCell,
													final HumanPlayer p_player,
													final int p_fogIdx,
													final int p_sizeIdx,
													final int p_index,
													final boolean isDebugModeEnabled)
	{
		Assert.precondition(p_index >= 1, "Invalid Index");

		final Coord cellCoord = p_currentCell.getCellCoord();
		final CellTypeCst cellTypeCst = p_currentCell.getCellType();

		if (!isDebugModeEnabled && p_player.getShadowSet().contains(cellCoord))
		{
			String iconName = translateSizeIdx(p_sizeIdx) + File.separator;
			return _imageMap.get(iconName + "shadow");
		}

		return getCellIcon(p_fogIdx, p_sizeIdx, cellTypeCst, p_index, true);
	}

	public final ImageIcon getOtherIcon(final String p_iconName)
	{
		return _imageMap.get(p_iconName);
	}

	public final ImageIcon getHelpIcon(final String p_iconName)
	{
		return _imageMap.get("help" + File.separator + p_iconName);
	}

	public final void addIcon(final String p_iconName, final ImageIcon p_icon)
	{
		_imageMap.put(p_iconName, p_icon);
	}

	public final ImageIcon getCellModifier(final int p_cellModifierIdx)
	{
		Assert.precondition_between(p_cellModifierIdx, 0, 2, "Invalid Cell Modifier Type");

		final String iconName = "map_builder" + File.separator + "cell_" + translateCellModifierIdx(p_cellModifierIdx)
				+ "_fog";

		return _imageMap.get(iconName);
	}

	public final ImageIcon getSelectedCellModifier(final int p_cellModifierIdx)
	{
		Assert.precondition_between(p_cellModifierIdx, 0, 2, "Invalid Cell Modifier Type");

		final String iconName = "map_builder" + File.separator + "cell_" + translateCellModifierIdx(p_cellModifierIdx);

		return _imageMap.get(iconName);
	}

	private String translateCellModifierIdx(final int p_cellModifierIdx)
	{
		if (p_cellModifierIdx == IconFactory.CELL_1)
		{
			return "01x";
		} else if (p_cellModifierIdx == IconFactory.CELL_9)
		{
			return "09x";
		} else if (p_cellModifierIdx == IconFactory.CELL_25)
		{
			return "25x";
		} else
		{
			throw new IllegalArgumentException("Cell Modifier Unknown :" + p_cellModifierIdx);
		}
	}
}
