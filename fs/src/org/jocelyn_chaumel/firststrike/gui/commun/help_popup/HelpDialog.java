package org.jocelyn_chaumel.firststrike.gui.commun.help_popup;

import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JDialog;
import javax.swing.JFrame;

import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class HelpDialog extends FSDialog
{
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");
	private static final FSLogger _logger = FSLoggerManager.getLogger(HelpDialog.class.getName());

	private final HelpPanel _panel;

	public HelpDialog(final JDialog p_frame, final ArrayList<String> p_chapterNameList)
	{
		super(p_frame, bundle.getString("fs.help.title"), true);
		Assert.preconditionNotNull(p_chapterNameList, "Chapter Name List");
		Assert.precondition(p_chapterNameList.size() > 0, "Empty Chapter Name List");

		_panel = new HelpPanel(this, p_chapterNameList);

		this.getContentPane().add(_panel);
		_logger.systemSubAction("Display Help");

	}

	public HelpDialog(final JFrame p_frame, final ArrayList<String> p_chapterNameList)
	{
		super(p_frame, bundle.getString("fs.help.title"), true);

		Assert.preconditionNotNull(p_chapterNameList, "Chapter Name List");
		Assert.precondition(p_chapterNameList.size() > 0, "Empty Chapter Name List");

		_panel = new HelpPanel(this, p_chapterNameList);

		this.getContentPane().add(_panel);
		_logger.systemSubAction("Display Help");

	}
}
