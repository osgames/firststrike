/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * HelpPanel.java
 *
 * Created on 16 sept. 2009, 20:12:30
 */

package org.jocelyn_chaumel.firststrike.gui.commun.help_popup;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.ResourceBundle;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.help.Chapter;
import org.jocelyn_chaumel.firststrike.core.help.HelpMgr;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * 
 * @author Client
 */
public class HelpPanel extends javax.swing.JPanel implements ListSelectionListener
{
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle"); //$NON-NLS-1$

	private static final HelpMgr _helpMgr = HelpMgr.getInstance();

	private final HashSet<String> _chapterViewedList = new HashSet<String>();
	private final IconFactory _iconFactory = IconFactory.getInstance();
	private final FSDialog _dialog;
	private final DefaultListModel<Chapter> _model = new DefaultListModel<Chapter>();
	private ArrayList<Chapter> _originalChapterList;

	/** Creates new form HelpPanel */
	public HelpPanel()
	{

		setBorder(new CompoundBorder(new EmptyBorder(3, 3, 3, 3), null));
		initComponents();
		_dialog = null;
	}

	public HelpPanel(final FSDialog p_dialog, final ArrayList<String> p_chapterNameList)
	{
		Assert.preconditionNotNull(p_chapterNameList, "Chapter Array");
		Assert.precondition(p_chapterNameList.size() > 0, "Empty Chapter Name Array");

		_originalChapterList = buildChapterList(p_chapterNameList);
		initComponents();
		_dialog = p_dialog;
		String categoryName = "";

		for (Chapter currentChapter : _originalChapterList)
		{
			if (!currentChapter.getCategory().equals(categoryName))
			{
				if (categoryName == "")
				{
					categoryName = currentChapter.getCategory();
				} else {
					categoryName = currentChapter.getCategory();
					_model.addElement(new Chapter(categoryName));
				}
			}
			_model.addElement(currentChapter);
		}
		_list.setCellRenderer(new ChapterJListItemRenderer());
		_list.setModel(_model);
		_list.setSelectedIndex(0);
		_list.addListSelectionListener(this);
		_displayHelpOption.setSelected(true);

		initComponentChapter();
	}

	public boolean isDisplayHelpOptionSelected()
	{
		return _displayHelpOption.isSelected();
	}

	private ArrayList<Chapter> buildChapterList(final ArrayList<String> p_chapterNameList)
	{
		Assert.preconditionNotNull(p_chapterNameList, "Chapter Name List");
		Assert.precondition(p_chapterNameList.size() > 0, "Empty Chapter Name List");

		final ArrayList<Chapter> chapterList = new ArrayList<Chapter>();

		for (int i = 0; i < p_chapterNameList.size(); i++)
		{
			chapterList.add(_helpMgr.getChapter(p_chapterNameList.get(i)));
		}

		chapterList.sort(new Comparator<Chapter>()
		{

			@Override
			public int compare(Chapter p_o1, Chapter p_o2)
			{
				String cat1 = p_o1.getCategory();
				String cat2 = p_o2.getCategory();
				if (!cat1.equals(cat2))
				{
					return Byte.compare(HelpMgr.getCatPriority(cat1), HelpMgr.getCatPriority(cat2));
				}

				return p_o1.getTitle().compareTo(p_o2.getTitle());
			}
		});

		return chapterList;
	}

	private void initComponentChapter()
	{
		final Chapter currentChapter = _model.getElementAt(_list.getSelectedIndex());
		_chapterViewedList.add(currentChapter.getId());
		final String image = currentChapter.getImageName();
		final ImageIcon icon = _iconFactory.getHelpIcon(image);

		_dialog.setTitle(BUNDLE.getString("fs.help.title") + " - " + currentChapter.getTitle());
		_helpTextLabel.setText(currentChapter.getText());

		_pictureLbl.setIcon(icon);

		_nextButton.setEnabled(checkIfNextButtonIsEnabled());
		_previousButton.setEnabled(checkIfPreviousButtonIsEnabled());
	}
	
	private boolean checkIfPreviousButtonIsEnabled()
	{
		int idx = _list.getSelectedIndex() - 1;
		
		while (idx >=0)
		{
			if (!_model.get(idx).isGroupChapter())
			{
				return true;
			}
			--idx;
		}
		
		return false;
	}
	
	private boolean checkIfNextButtonIsEnabled()
	{
		int idx = _list.getSelectedIndex() + 1;
		
		while (idx <= _model.getSize() - 1)
		{
			if (!_model.get(idx).isGroupChapter())
			{
				return true;
			}
			++idx;
		}
		
		return false;
	}
	
	private int findFirstChapterByChapter(final String p_category)
	{
		for (int i = 0; i < _originalChapterList.size(); i++)
		{
			if (_originalChapterList.get(i).getCategory().equals(p_category))
			{
				return i;
			}
		}
		
		throw new FSFatalException("Unexpected Category found: " + p_category);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents()
	{
		_pictureLbl = new javax.swing.JLabel();
		_pictureLbl.setOpaque(true);
		_pictureLbl.setForeground(Color.WHITE);
		_pictureLbl.setBackground(Color.WHITE);

		_pictureLbl.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		_pictureLbl.setMaximumSize(new java.awt.Dimension(320, 320));
		_pictureLbl.setMinimumSize(new java.awt.Dimension(320, 320));

		setLayout(new BorderLayout(2, 0));
		add(_pictureLbl, BorderLayout.WEST);
		_helpTextLabel = new javax.swing.JLabel();
		_toolBarPnl = new JPanel();
		add(_toolBarPnl, BorderLayout.NORTH);
		jScrollPane1 = new javax.swing.JScrollPane();

		jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		add(jScrollPane1);

		jScrollPane1.setViewportView(_helpTextLabel);
		_toolBarPnl.setLayout(new GridLayout(0, 4, 0, 0));

		_previousChapterTitleLbl = new JLabel("");
		_previousChapterTitleLbl.setPreferredSize(new Dimension(300, 14));
		_previousChapterTitleLbl.setBorder(new EmptyBorder(0, 0, 0, 0));
		_previousChapterTitleLbl.setHorizontalAlignment(SwingConstants.RIGHT);
		_toolBarPnl.add(_previousChapterTitleLbl);

		_panel_1 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) _panel_1.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		_panel_1.setBorder(new EmptyBorder(0, 16, 0, 8));
		_toolBarPnl.add(_panel_1);
		_previousButton = new javax.swing.JButton();
		_previousButton.setMargin(new Insets(0, 0, 0, 0));
		_previousButton.setPreferredSize(new Dimension(200, 20));
		_previousButton.setMinimumSize(new Dimension(200, 9));
		_previousButton.setMaximumSize(new Dimension(200, 9));
		_previousButton.setSize(new Dimension(200, 0));
		_previousButton.setBounds(new Rectangle(0, 0, 40, 0));
		_panel_1.add(_previousButton);

		 _previousButton.setText(BUNDLE.getString("fs.common.previous_btn"));
		_previousButton.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(final java.awt.event.ActionEvent evt)
			{
				previousButtonActionPerformed(evt);
			}
		});

		_panel_2 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) _panel_2.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		_panel_2.setBorder(new EmptyBorder(0, 8, 0, 16));
		_toolBarPnl.add(_panel_2);
		_nextButton = new javax.swing.JButton();
		_nextButton.setMargin(new Insets(0, 0, 0, 0));
		_nextButton.setPreferredSize(new Dimension(200, 20));
		_nextButton.setMaximumSize(new Dimension(200, 9));
		_nextButton.setMinimumSize(new Dimension(200, 9));
		_nextButton.setSize(new Dimension(200, 0));
		_nextButton.setBounds(new Rectangle(0, 0, 200, 0));
		_panel_2.add(_nextButton);

		_nextButton.setText(BUNDLE.getString("fs.common.next_btn"));
		_nextButton.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(final java.awt.event.ActionEvent evt)
			{
				nextButtonActionPerformed(evt);
			}
		});

		_nextChapterTitleLbl = new JLabel("");
		_nextChapterTitleLbl.setPreferredSize(new Dimension(300, 14));
		_nextChapterTitleLbl.setBorder(new EmptyBorder(0, 0, 0, 0));
		_nextChapterTitleLbl.setHorizontalAlignment(SwingConstants.LEFT);
		_toolBarPnl.add(_nextChapterTitleLbl);

		_bottomBar = new JPanel();
		add(_bottomBar, BorderLayout.SOUTH);
		_bottomBar.setLayout(new BorderLayout(0, 0));
		_displayHelpOption = new javax.swing.JCheckBox();
		_bottomBar.add(_displayHelpOption, BorderLayout.WEST);

		_displayHelpOption.setText(BUNDLE.getString("fs.help.display_popup"));

		_panel = new JPanel();
		_bottomBar.add(_panel, BorderLayout.EAST);
		_closeButton = new javax.swing.JButton();
		_panel.add(_closeButton);
		_closeButton.setText(BUNDLE.getString("fs.common.close_btn"));

		_list = new JList();
		_list.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		add(_list, BorderLayout.EAST);
		_closeButton.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(final java.awt.event.ActionEvent evt)
			{
				closeButtonActionPerformed(evt);
			}
		});
	}// </editor-fold>//GEN-END:initComponents

	private void closeButtonActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_closeButtonActionPerformed
		_dialog.setVisible(false);
		if (!isDisplayHelpOptionSelected())
		{
			_helpMgr.setOptionDisplayHelp(false);
		}

		final ArrayList<String> chapterNameList = new ArrayList<String>();
		for (int i = 0; i < _model.size(); i++)
		{
			if (!_model.getElementAt(i).isGroupChapter())
			{
				chapterNameList.add(_model.getElementAt(i).getId());
			}
		}
		if (!chapterNameList.isEmpty())
		{
			_helpMgr.appendConsultedChapterNameList(chapterNameList);
			_helpMgr.save();
		}

	}// GEN-LAST:event_closeButtonActionPerformed

	private void nextButtonActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_nextButtonActionPerformed

		int currentIndex = _list.getSelectedIndex() + 1;
		while (_model.getElementAt(currentIndex).isGroupChapter()) {
			++currentIndex;
		}
		_list.setSelectedIndex(currentIndex);
		_dialog.pack();
	}// GEN-LAST:event_nextButtonActionPerformed

	private void previousButtonActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_previousButtonActionPerformed
		int currentIndex = _list.getSelectedIndex() - 1;
		while (_model.getElementAt(currentIndex).isGroupChapter()) {
			--currentIndex;
		}
		_list.setSelectedIndex(currentIndex);
		_dialog.pack();
	}// GEN-LAST:event_previousButtonActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton _closeButton;
	private javax.swing.JCheckBox _displayHelpOption;
	private javax.swing.JLabel _helpTextLabel;
	private javax.swing.JButton _nextButton;
	private javax.swing.JLabel _pictureLbl;
	private javax.swing.JButton _previousButton;
	private javax.swing.JScrollPane jScrollPane1;
	private JPanel _bottomBar;
	private JPanel _panel;
	private JList<Chapter> _list;
	private JPanel _toolBarPnl;
	private JLabel _previousChapterTitleLbl;
	private JLabel _nextChapterTitleLbl;
	private JPanel _panel_1;
	private JPanel _panel_2;
	// End of variables declaration//GEN-END:variables

	@Override
	public void valueChanged(ListSelectionEvent p_arg0)
	{
		int previousIdx;
		int currentIdx = _list.getSelectedIndex();
		if (p_arg0.getFirstIndex() == currentIdx)
		{
			previousIdx = p_arg0.getLastIndex();
		} else {
			previousIdx = p_arg0.getFirstIndex();
		}
		if (previousIdx == currentIdx)
		{
			return;
		}
		if (!_list.getSelectedValue().isGroupChapter())
		{
			initComponentChapter();
		} else {
			final Chapter previousChapter = _model.getElementAt(previousIdx);
			Chapter group = _list.getSelectedValue();
			if (group.isDeployed()) {
				for (int i=_model.getSize() - 1;i > 0 ; i--)
				{
					if (_model.getElementAt(i).getCategory().equals(group.getCategory()))
					{
						if (_model.getElementAt(i).isGroupChapter()) {
							_model.getElementAt(i).setIsDeployed(!_model.getElementAt(i).isDeployed());
						} else {
							_model.removeElementAt(i);
						}
					}
				}
				
				if (previousChapter.getCategory().equals(group.getCategory()))
				{
					_list.setSelectedIndex(0);
				} else {
					_list.setSelectedIndex(previousIdx);
				}
			} else 
			{
				String category = group.getCategory();
				group.setIsDeployed(!group.isDeployed());
				int i = findFirstChapterByChapter(category);
				++currentIdx;
				while(i< _originalChapterList.size() &&  _originalChapterList.get(i).getCategory().equals(category))
				{
					_model.add(currentIdx, _originalChapterList.get(i));
					if (currentIdx <= previousIdx) {
						++previousIdx;
					}
					++currentIdx;
					++i;
				}
				_list.setSelectedIndex(previousIdx);
			}

			//_dialog.pack();
		}

	}

}
