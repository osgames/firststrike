package org.jocelyn_chaumel.firststrike.gui.commun.help_popup;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.jocelyn_chaumel.firststrike.core.help.Chapter;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;

public class ChapterJListItemRenderer implements ListCellRenderer<Chapter>
{
	private final JLabel _emptyLabel = new JLabel();
	private final JLabel _chapterLabel = new JLabel();
	private final JLabel _groupLabel = new JLabel();
	private final IconFactory _iconFactory = IconFactory.getInstance();

	
	public ChapterJListItemRenderer()
	{
		_groupLabel.setBorder(BorderFactory.createRaisedSoftBevelBorder());
		_groupLabel.setBackground(Color.lightGray);
		_groupLabel.setOpaque(true);
	}

	@Override
	public Component getListCellRendererComponent(	JList<? extends Chapter> p_list,
													Chapter p_value,
													int p_index,
													boolean p_isSelected,
													boolean p_cellHasFocus)
	{
		if (p_value == null)
		{
			return _emptyLabel;
		}
		
		final Chapter chapter = (Chapter) p_value;
		
		if (!chapter.isGroupChapter())
		{
			_chapterLabel.setText(chapter.getTitle());
			
			if (p_isSelected)
			{
				_chapterLabel.setBorder(BorderFactory.createLineBorder(Color.black));
				
			} else {
				_chapterLabel.setBorder(BorderFactory.createLineBorder(Color.white));
			}
			
			return _chapterLabel;
		}
		
		final ImageIcon icon;
		if (chapter.isDeployed())
		{
			icon = _iconFactory.getCommunIcon(IconFactory.COMMON_MINUS);
		} else 
		{
			icon = _iconFactory.getCommunIcon(IconFactory.COMMON_PLUS);
		}
		_groupLabel.setIcon(icon);
		_groupLabel.setIconTextGap(2);
		_groupLabel.setText(chapter.getTitle());
		
		return _groupLabel;
	}

}
