package org.jocelyn_chaumel.firststrike.gui.commun;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.tools.StringManipulatorHelper;
import org.jocelyn_chaumel.tools.debugging.GCResult;

public class MemoryMgr
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(MemoryMgr.class.getName());

	public final static int log(final FSLogger p_logger, final String p_message)
	{

		final FSLogger logger = getLogger(p_logger);
		final GCResult result = GCResult.getMemoryUsed();
		final long memoryUsed = result.getMemoryUsedAfterGC();
		final long memoryCapacity = result.getMemoryCapacity();

		final String memoryUsedStr = StringManipulatorHelper.formatNumber(memoryUsed);
		final String memoryCapacityStr = StringManipulatorHelper.formatNumber(result.getMemoryCapacity());

		final int memoryUsedPercent = (int) (memoryUsed * 100 / memoryCapacity);
		final StringBuilder sb;
		if (p_message != null)
		{
			sb = new StringBuilder(p_message);
		} else
		{
			sb = new StringBuilder("Memory used:");
		}

		sb.append(memoryUsedStr);
		sb.append("/");
		sb.append(memoryCapacityStr);
		sb.append(" (").append(memoryUsedPercent).append("%)");

		if (result.isGCSuccesfull())
		{
			sb.append(" OK");
		} else
		{
			sb.append(" KO");
		}
		logger.systemSubAction(sb.toString());

		return memoryUsedPercent;
	}

	public final static long getSize(final FSLogger p_logger, final Serializable p_object, final String p_message)
	{
		final FSLogger logger = getLogger(p_logger);
		long size = -1;
		try
		{
			final ByteArrayOutputStream bos = new ByteArrayOutputStream();
			final ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(p_object);
			size = bos.size();
			oos.close();
			bos.close();
			logger.systemAction(p_message + ":" + size);

		} catch (IOException ex)
		{
			logger.systemError("Error detected", ex);
		}

		return size;
	}

	private static FSLogger getLogger(final FSLogger p_logger)
	{
		if (p_logger == null)
		{
			return _logger;
		} else
		{
			return p_logger;
		}

	}
}
