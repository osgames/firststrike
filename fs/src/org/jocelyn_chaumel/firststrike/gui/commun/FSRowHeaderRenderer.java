package org.jocelyn_chaumel.firststrike.gui.commun;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.table.JTableHeader;

import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;

public class FSRowHeaderRenderer extends JLabel implements ListCellRenderer
{
	public FSRowHeaderRenderer(final JTable p_table)
	{
		JTableHeader header = p_table.getTableHeader();
		setOpaque(true);
		setBorder(UIManager.getBorder("TableHeader.cellBorder"));
		setHorizontalAlignment(CENTER);
		setForeground(header.getForeground());
		setBackground(header.getBackground());
		setFont(header.getFont());
		final Dimension dim = new Dimension();
		dim.height = IconFactory.CELL_SIZE;
		dim.width = IconFactory.CELL_SIZE;

		setPreferredSize(dim);
		setMinimumSize(dim);

		setSize(dim);
	}

	@Override
	public final Component getListCellRendererComponent(final JList aList,
														final Object p_value,
														final int p_index,
														final boolean anIsSelectedFlag,
														final boolean p_cellHasFocusFlag)
	{

		setText("" + p_index);
		return this;
	}
}
