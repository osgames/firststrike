/*
 * Created on Feb 5, 2006
 */
package org.jocelyn_chaumel.firststrike.gui.map_builder;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.AreaNameMap;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapInfo;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.InvalidMapException;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.FSFileFilter;
import org.jocelyn_chaumel.firststrike.gui.commun.FSRowHeaderRenderer;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.map_builder.board.FSMapBuilderJTable;
import org.jocelyn_chaumel.firststrike.gui.map_builder.board.FSMapBuilderTableModel;
import org.jocelyn_chaumel.firststrike.gui.map_builder.contextual_bar.CellListContextualToolBar;
import org.jocelyn_chaumel.firststrike.gui.map_builder.menu.manage_area.AreaNameManagementDlg;
import org.jocelyn_chaumel.firststrike.gui.map_builder.menu.new_map.FSNewMapDialog;
import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.debugging.Assert;
import org.jocelyn_chaumel.tools.gui.jtable.ScrollableJTable;

/**
 * @author Jocelyn Chaumel
 */
public class FSMapBuilderFrame extends JFrame implements IFSMapBuilderGlobalOperation, IFSCellTypeUpdateListener,
		TableModelListener
{

	private final static String DLG_WARNING_TITLE = "Warning";

	private final static String DLG_ERROR_TITLE = "Error";

	private final static String CLOSE_MODIFIED_MAP = "This map has been modified.\nDo you wish to continue this operation and lose the last modifications?";

	private final static String OVER_WRITE_EXISTING_MAP = "This filename already exists.\nDo you want overwrite this file anyway?";

	private final static String UNABLE_DELETE_FILE = "Unable to delete this file.  This file may be in use or write protected?";

	private final static String FOUND_A_BUG = "You found a bug!!!";

	private final static String FRAME_TITLE = "First Strike Map Editor";

	private final static String FRAME_TITLE_NEW_MAP = "First Strike Map Editor : New Map*";

	private final FSMapBuilderGlobalMenu _globalMenu;

	private final FSMapBuilderToolBar _toolBar;
	private final CellListContextualToolBar _contextualToolBar;

	private JPanel _mainPanel = new JPanel(new BorderLayout());

	private Component _currentMapPanel = null;

	private FSMapBuilderJTable _table = null;

	private File _defaultMapDir = null;

	private File _currentFile = null;
	private AreaNameMap _currentAreaNameMap = null;

	private boolean _isModifiedMap = false;

	private final static FSLogger _fsLogger = FSLoggerManager.getLogger(FSMapBuilderFrame.class.getName());

	/**
	 * @throws java.awt.HeadlessException
	 */
	public FSMapBuilderFrame() throws HeadlessException
	{
		_globalMenu = new FSMapBuilderGlobalMenu(this);

		setJMenuBar(_globalMenu.getMenuBarWithoutMapFile());
		_toolBar = new FSMapBuilderToolBar(this);
		_mainPanel.add(_toolBar, BorderLayout.NORTH);
		_contextualToolBar = new CellListContextualToolBar(this);

		_mainPanel.add(_contextualToolBar, BorderLayout.EAST);
		setContentPane(_mainPanel);
		setEmptyPanel();

		_defaultMapDir = ApplicationConfigurationMgr.getInstance().getMapDir();

	}

	private void setEmptyPanel()
	{
		setTitle(FRAME_TITLE);
		final JPanel panel = new JPanel(new FlowLayout());
		panel.add(new JLabel("(No Map Selected)"));

		if (_currentMapPanel != null)
		{
			_mainPanel.remove(_currentMapPanel);
		}
		_mainPanel.add(panel, BorderLayout.CENTER);
		_currentMapPanel = panel;

		IconFactory.getInstance().setUserSkin(null);
		_contextualToolBar.refreshCurrentCellType();

		_globalMenu.updateItemMenu(false);
		_isModifiedMap = false;
	}

	@Override
	public void newMap()
	{
		_fsLogger.systemAction("New map");
		if (_table != null)
		{
			if (!closeMap())
			{
				_fsLogger.systemSubAction("New map canceled");
				return;
			}
		}
		final FSNewMapDialog newMapDialog = new FSNewMapDialog(this);
		newMapDialog.pack();
		newMapDialog.setVisible(true);
		newMapDialog.dispose();
		if (!newMapDialog.getStatus())
		{
			_fsLogger.systemSubAction("New map canceled");
			return;
		}

		final int mapHeight = newMapDialog.getMapHeight();
		final int mapWidth = newMapDialog.getMapWidth();

		final FSMapBuilderTableModel model = new FSMapBuilderTableModel(mapHeight, mapWidth);
		_isModifiedMap = true;
		setTitle(FRAME_TITLE_NEW_MAP);
		setMapPanel(model);
		_contextualToolBar.setEnabledToolBar(true);
		_toolBar.setEnabledToolBar(true);
		_currentAreaNameMap = null;
		_fsLogger.systemSubAction("New map finish succesfully");
	}

	public final FSMapBuilderTableModel getMapModel()
	{
		return (FSMapBuilderTableModel) _table.getModel();
	}

	private void setMapPanel(final FSMapBuilderTableModel p_model)
	{
		Assert.preconditionNotNull(p_model, "Table Model");

		Assert.preconditionNotNull(p_model, "Table Model");
		p_model.addTableModelListener(this);
		_table = new FSMapBuilderJTable(this, p_model);

		final ScrollableJTable scrollPane = new ScrollableJTable(_table, new FSRowHeaderRenderer(_table));

		final Container container = getContentPane();
		container.remove(_currentMapPanel);
		_currentMapPanel = scrollPane;
		container.add(scrollPane, BorderLayout.CENTER);

		_globalMenu.updateItemMenu(true);
		setVisible(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jocelyn_chaumel.firststrike.gui.map_builder.IFSMapBuilderGlobalOperation
	 * #openMap()
	 */
	@Override
	public void openMap()
	{
		_fsLogger.systemAction("Open map");
		if (_isModifiedMap)
		{
			if (!closeMap())
			{
				_fsLogger.systemSubAction("Open map canceled");
				return;
			}
		}

		final JFileChooser dialog = new JFileChooser("Map Destination Selection");

		dialog.setCurrentDirectory(_defaultMapDir);
		dialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		final int returnVal = dialog.showOpenDialog(this);

		if (returnVal != JFileChooser.APPROVE_OPTION)
		{
			_fsLogger.systemSubAction("Open map canceled");
			return;
		}
		final File selectedFile = dialog.getSelectedFile();
		_fsLogger.systemSubAction("Selected file : " + selectedFile.getAbsolutePath());

		if (!selectedFile.exists())
		{
			JOptionPane.showMessageDialog(	this,
											"Unable to found the file " + selectedFile.getAbsolutePath(),
											DLG_ERROR_TITLE,
											JOptionPane.ERROR_MESSAGE);
			_fsLogger.systemSubActionDetail("Unable to found the file {0}", selectedFile.getAbsolutePath());
		}

		try
		{
			final FSMap map = FSMapMgr.loadMap(selectedFile, false);
			_currentAreaNameMap = map.getAreaNameMap();
			final FSMapBuilderTableModel model = new FSMapBuilderTableModel(map.getFSMapCells().getCells());
			_isModifiedMap = false;
			setTitle(FRAME_TITLE + " : " + map.getName());
			_currentFile = selectedFile;
			_defaultMapDir = _currentFile.getParentFile();
			_contextualToolBar.setEnabledToolBar(true);
			_toolBar.setEnabledToolBar(true);
			final FSMapInfo info = map.getFSMapInfo();
			if (info.hasDefaultSkin())
			{
				final File defaultSkinDir = FileMgr.getFileFromRelativPath(info.getDefaultSkin());
				if (!defaultSkinDir.isDirectory())
				{
					final String message = "Default Skin folder usually used with this map is not found: "
							+ defaultSkinDir.getAbsolutePath() + "\n The default First Strike skin will be used.";
					JOptionPane.showMessageDialog(this, message, DLG_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
				} else
				{
					IconFactory.getInstance().setUserSkin(defaultSkinDir);
					_contextualToolBar.refreshCurrentCellType();
				}
			}
			setMapPanel(model);
		} catch (FileNotFoundException ex)
		{
			final String message = "Unable to found the file " + selectedFile.getAbsolutePath();
			_fsLogger.systemError(message, ex);
			JOptionPane.showMessageDialog(this, message, DLG_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
			return;
		} catch (IOException ex)
		{
			final String message = "An unexpected error occurs.";
			_fsLogger.systemError(message, ex);
			JOptionPane.showMessageDialog(this, message, DLG_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
		} catch (Exception ex)
		{
			_fsLogger.systemError(ex.getMessage(), ex);
			JOptionPane.showMessageDialog(this, ex.getMessage(), DLG_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
			return;
		}
	}

	@Override
	public boolean closeMap()
	{
		_fsLogger.systemAction("Close map");
		if (_isModifiedMap)
		{
			_fsLogger.systemSubAction("Map modified, Map builder ask a confirmation");
			final Object[] options = { "Ok", "Cancel" };
			final int choise = JOptionPane.showOptionDialog(null,
															CLOSE_MODIFIED_MAP,
															DLG_WARNING_TITLE,
															JOptionPane.DEFAULT_OPTION,
															JOptionPane.WARNING_MESSAGE,
															null,
															options,
															options[0]);
			if (choise == 1)
			{
				_fsLogger.systemSubAction("Close map canceled");
				return false;
			}
			_fsLogger.systemSubAction("Close map confirm");
		}

		setEmptyPanel();
		setVisible(true);

		_contextualToolBar.setEnabledToolBar(false);
		_toolBar.setEnabledToolBar(false);

		_fsLogger.systemSubAction("Close map succesful");
		return true;
	}

	@Override
	public void saveMap()
	{
		if (_currentFile == null)
		{
			saveAsMap();
			return;

		}
		_fsLogger.systemAction("Save Map");
		saveMapMethod();
	}

	private void saveMapMethod()
	{
		Assert.check(_currentFile != null, "Current File is null");

		final FSMapBuilderTableModel model = (FSMapBuilderTableModel) _table.getModel();
		final Cell[][] cellArray = model.getCellArray();
		try
		{
			final String mapName = FileMgr.getFileNameWithoutExtention(_currentFile);
			final FSMapInfo mapInfo = new FSMapInfo(mapName,
													mapName,
													FRAME_TITLE,
													FSMapMgr.VERSION,
													cellArray[0].length,
													cellArray.length);
			if (_table.hasDefaultSkinDir())
			{
				final File defaultSkinDir = _table.getDefaultSkinDir();
				mapInfo.setDefaultSkin(FileMgr.getRelativePath(defaultSkinDir));
			}
			final FSMap map = FSMapMgr.create(mapInfo, cellArray, _currentAreaNameMap, false);
			_currentAreaNameMap = map.getAreaNameMap();
			if (!manageAreaName(map))
			{
				return;
			}

			try
			{
				if (_currentFile.exists())
				{
					FileMgr.forceDelete(_currentFile);
				}
			} catch (IOException ex)
			{
				JOptionPane.showMessageDialog(this, UNABLE_DELETE_FILE, DLG_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
				_fsLogger.systemError("", ex);
				return;
			}

			FSMapMgr.saveMap(map, _currentFile);

			setTitle(FRAME_TITLE + " : " + mapName);
			_isModifiedMap = false;
			_fsLogger.systemSubAction("Map saved succesfully");
		} catch (InvalidMapException ex)
		{
			_currentFile = null;
			JOptionPane.showMessageDialog(this, ex.getMessage(), DLG_WARNING_TITLE, JOptionPane.WARNING_MESSAGE);
			_fsLogger.systemError("", ex);
		} catch (IOException ex)
		{
			_currentFile = null;
			JOptionPane.showMessageDialog(this, FOUND_A_BUG, DLG_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
			_fsLogger.systemError("", ex);
		}

	}

	private boolean manageAreaName(final FSMap p_map)
	{
		final String msg = "Do you wish manage the area name?";
		final Object[] options = { "Yes", "No", "Cancel" };
		final int choice = JOptionPane.showOptionDialog(this,
														msg,
														FRAME_TITLE,
														JOptionPane.DEFAULT_OPTION,
														JOptionPane.QUESTION_MESSAGE,
														null,
														options,
														options[0]);

		// Choice : No
		if (choice == 1)
		{
			_fsLogger.systemSubAction("No managment of the area name has been requested by the user");
			return true;

			// Choice : Cancel
		} else if (choice == 2)
		{
			_fsLogger.systemSubAction("Cancel the current operation");
			return false;
		}

		_fsLogger.systemSubAction("Managment of the area name requested by the user");

		AreaNameManagementDlg dlg = new AreaNameManagementDlg(this, p_map);
		dlg.pack();
		dlg.setVisible(true);
		dlg.dispose();
		if (!dlg.getStatus())
		{
			_fsLogger.systemSubAction("Cancel the current operation");
			return false;
		}
		_currentAreaNameMap = p_map.getAreaNameMap();
		return true;
	}

	@Override
	public void saveAsMap()
	{
		_fsLogger.systemAction("Save As map");
		final JFileChooser dialog = new JFileChooser("Map Destination Selection");

		dialog.setCurrentDirectory(_defaultMapDir);
		dialog.setFileFilter(new FSFileFilter(	FSFileFilter.FIRST_STRIKE_MAP_EXTENSION,
												FSFileFilter.FIRST_STRIKE_MAP_DESC));

		final int returnVal = dialog.showSaveDialog(this);

		if (returnVal != JFileChooser.APPROVE_OPTION)
		{
			_fsLogger.systemSubAction("Save As map canceld");
			return;
		}
		File selectedFile = dialog.getSelectedFile();
		_fsLogger.systemSubAction("Selected file : " + selectedFile.getAbsolutePath());

		selectedFile = FileMgr.addExtentionIfNecessairy(selectedFile, FSFileFilter.FIRST_STRIKE_MAP_EXTENSION);
		if (selectedFile.exists())
		{
			_fsLogger.systemSubAction("File exist.  Overwrite confirmation needed");
			final Object[] options = { "Ok", "Cancel" };
			final int choise = JOptionPane.showOptionDialog(this,
															OVER_WRITE_EXISTING_MAP,
															DLG_WARNING_TITLE,
															JOptionPane.DEFAULT_OPTION,
															JOptionPane.WARNING_MESSAGE,
															null,
															options,
															options[0]);
			if (choise == 1)
			{
				_fsLogger.systemSubAction("Save As map canceld");
				return;
			}

		}

		_currentFile = selectedFile;
		_defaultMapDir = selectedFile.getParentFile();

		saveMapMethod();
		_fsLogger.systemSubAction("Save As map succesful");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jocelyn_chaumel.firststrike.gui.map_builder.IFSMapBuilderGlobalOperation
	 * #exitMapBuilder()
	 */
	@Override
	public void exitMapBuilder()
	{
		_fsLogger.systemAction("Exit Map Builder");
		if (_isModifiedMap)
		{
			_fsLogger.systemSubAction("Current map is modified.  Confirmation needed");
			final Object[] options = { "Ok", "Cancel" };
			final int choise = JOptionPane.showOptionDialog(null,
															CLOSE_MODIFIED_MAP,
															DLG_WARNING_TITLE,
															JOptionPane.DEFAULT_OPTION,
															JOptionPane.WARNING_MESSAGE,
															null,
															options,
															options[0]);
			if (choise == 1)
			{
				_fsLogger.systemSubAction("Exit Map builder canceled");
				return;
			}
			_fsLogger.systemSubAction("Exit Map builder confirm");
		}
		dispose();
	}

	@Override
	public void selectionCellTypeChanged(	final Object p_source,
											final CellTypeCst p_cellType,
											final int p_minIdxCellType,
											final int p_maxIdxCellType)
	{
		_fsLogger.systemAction("Change cell type :" + p_cellType + "(" + p_minIdxCellType + "-" + p_maxIdxCellType
				+ ")");
		if (_toolBar == p_source)
		{
			_table.setCellType(p_cellType, p_minIdxCellType, p_maxIdxCellType);
			_contextualToolBar.refreshCellType(p_cellType);
		} else if (_table == p_source)
		{
			_contextualToolBar.refreshCellType(p_cellType);
		}
	}

	@Override
	public void selectionIdxCellTypeChanged(final int p_minIdxCellType, final int p_maxIdxCellType)
	{
		if (_table != null)
		{
			_table.setIdxCellType(p_minIdxCellType, p_maxIdxCellType);
			_fsLogger.systemAction("Change idx cell type :" + p_minIdxCellType + "-" + p_maxIdxCellType);
		}
	}

	@Override
	public void selectionModifierToggleButtonChanged(int p_cellModifier)
	{
		if (_table != null)
		{
			_table.setCellModifier(p_cellModifier);
			_fsLogger.systemAction("Change cell range :" + p_cellModifier);
		}
	}

	@Override
	public void tableChanged(final TableModelEvent arg0)
	{
		Assert.preconditionNotNull(arg0);

		if (!_isModifiedMap)
		{
			_isModifiedMap = true;
			setTitle(getTitle() + "*");
		}
	}
}