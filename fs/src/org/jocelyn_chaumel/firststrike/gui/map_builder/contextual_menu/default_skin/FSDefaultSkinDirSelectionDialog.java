package org.jocelyn_chaumel.firststrike.gui.map_builder.contextual_menu.default_skin;

import java.io.File;

import javax.swing.JFrame;

import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;

public class FSDefaultSkinDirSelectionDialog extends FSDialog
{
	private final FSUserIconDirSelectionPnl _panel;

	public FSDefaultSkinDirSelectionDialog(final JFrame p_frame, final File p_userDir)
	{
		super(p_frame, "Select a User Icon Directory", true);

		_panel = new FSUserIconDirSelectionPnl(this, p_userDir);
		super.getContentPane().add(_panel);
	}

	public final boolean getStatus()
	{
		return _panel.getStatus();
	}

	public final File getDefaultSkinDir()
	{
		return _panel.getDefaultSkinDir();
	}
}
