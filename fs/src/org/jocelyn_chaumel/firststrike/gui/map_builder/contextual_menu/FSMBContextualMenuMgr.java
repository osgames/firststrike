package org.jocelyn_chaumel.firststrike.gui.map_builder.contextual_menu;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.gui.map_builder.FSMapBuilderFrame;
import org.jocelyn_chaumel.firststrike.gui.map_builder.board.FSMapBuilderJTable;
import org.jocelyn_chaumel.firststrike.gui.map_builder.board.IMapBoard;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FSMBContextualMenuMgr extends JPopupMenu
{
	private Coord _menuCoord;
	private final IMapBoard _mapBoard;

	private final JMenuItem _manageAreaItem;
	private final JMenuItem _renameCellItem;
	private final JMenuItem _setDefaultSkinItem;

	public FSMBContextualMenuMgr(final IMapBoard p_mapBoard)
	{

		_mapBoard = p_mapBoard;

		_renameCellItem = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_mapBoard.renameCell(_menuCoord);
			}
		});
		_renameCellItem.setText("rename the cell");

		_manageAreaItem = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				// _mapBoard.moveUnit(null);
			}
		});
		_manageAreaItem.setText("manage Areas' name...");

		_setDefaultSkinItem = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent p_event)
			{
				Assert.preconditionNotNull(p_event, "Action Event");
				_mapBoard.selectDefaultSkinDir();
			}
		});
		_setDefaultSkinItem.setText("set Default Skin...");

		add(_renameCellItem);
		addSeparator();
		add(_manageAreaItem);
		add(_setDefaultSkinItem);
	}

	@Override
	public void show(Component p_invoker, int p_x, int p_y)
	{
		final FSMapBuilderJTable board = (FSMapBuilderJTable) p_invoker;
		Component currentComponent = board.getParent();
		while (!(currentComponent instanceof FSMapBuilderFrame))
		{
			currentComponent = currentComponent.getParent();
		}

		final Point point = new Point(p_x, p_y);
		final int row = board.rowAtPoint(point);
		final int col = board.columnAtPoint(point);
		_menuCoord = new Coord(col, row);
		final Cell cell = (Cell) board.getModel().getValueAt(row, col);
		final CellTypeCst cellType = cell.getCellType();
		if (cellType.equals(CellTypeCst.SEA) || cellType.equals(CellTypeCst.RIVER)
				|| cellType.equals(CellTypeCst.MOUNTAIN))
		{
			_renameCellItem.setEnabled(false);
		} else
		{
			_renameCellItem.setEnabled(true);
		}
		super.show(p_invoker, p_x, p_y);
	}

}
