package org.jocelyn_chaumel.firststrike.gui.map_builder.contextual_menu.rename_city;

import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.map_builder.FSMapBuilderFrame;

public class RenameCityDlg extends FSDialog
{
	// private final static ResourceBundle bundle =
	// ResourceBundle.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private final Cell _cell;
	private final RenameCityPanel _panel;
	private boolean _status;

	public RenameCityDlg(final FSMapBuilderFrame p_frame, final Cell p_cityCell)
	{
		super(p_frame, "Rename a City", true);
		_cell = p_cityCell;
		_panel = new RenameCityPanel(p_cityCell, this);
		getContentPane().add(_panel);
	}

	@Override
	public void setVisible(final boolean p_isVisible)
	{

		super.setVisible(p_isVisible);
		if (!p_isVisible)
		{
			_status = _panel.getStatus();
			if (_panel.getStatus())
			{
				final String newName = _panel.getNewName();
				_cell.setCellName(newName);
			}
		}
	}

	public final boolean getStatus()
	{
		return _status;
	}
}
