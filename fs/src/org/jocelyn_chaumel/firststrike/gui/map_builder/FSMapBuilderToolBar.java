/*
 * Created on Feb 11, 2006
 */
package org.jocelyn_chaumel.firststrike.gui.map_builder;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public final class FSMapBuilderToolBar extends JToolBar implements ActionListener
{
	private final IconFactory _iconFactory = IconFactory.getInstance();

	private final IFSCellTypeUpdateListener _listener;

	private final JToggleButton _seaButton;

	private final JToggleButton _riverButton;

	private final JToggleButton _cityButton;

	private final JToggleButton _grasslandButton;

	private final JToggleButton _hillButton;

	private final JToggleButton _mountainButton;

	private final JToggleButton _lightWoodButton;

	private final JToggleButton _heavyWoodButton;

	private final JToggleButton _roadButton;

	private final JToggleButton _cell_1xButton;
	private final JToggleButton _cell_9xButton;
	private final JToggleButton _cell_25xButton;

	public static final String DISPLAY_SIZE_20x20 = "20x20";
	public static final String DISPLAY_SIZE_40x40 = "40x40";

	public FSMapBuilderToolBar(final IFSCellTypeUpdateListener p_listener)
	{
		super("Map Builder Tool Bar");

		_listener = p_listener;
		// setBorder(BorderFactory.createRaisedBevelBorder());

		final ButtonGroup CellTypeGroup = new ButtonGroup();

		final Dimension dimension = new Dimension(26, 26);
		ImageIcon icon = _iconFactory.getSimpleCellIcon(CellTypeCst.SEA,
														IconFactory.FOG,
														IconFactory.SIZE_SIMPLE_SIZE,
														1);
		_seaButton = new JToggleButton(icon);
		_seaButton.setPreferredSize(dimension);
		_seaButton.setSelectedIcon(_iconFactory.getSimpleCellIcon(	CellTypeCst.SEA,
																	IconFactory.NO_FOG,
																	IconFactory.SIZE_SIMPLE_SIZE,
																	1));
		_seaButton.setSelected(true);
		_seaButton.addActionListener(this);
		add(_seaButton);
		CellTypeGroup.add(_seaButton);

		icon = _iconFactory.getSimpleCellIcon(CellTypeCst.RIVER, IconFactory.FOG, IconFactory.SIZE_SIMPLE_SIZE, 1);
		_riverButton = new JToggleButton(icon);
		_riverButton.setPreferredSize(dimension);
		_riverButton.setSelectedIcon(_iconFactory.getSimpleCellIcon(CellTypeCst.RIVER,
																	IconFactory.NO_FOG,
																	IconFactory.SIZE_SIMPLE_SIZE,
																	1));
		_riverButton.addActionListener(this);
		add(_riverButton);
		CellTypeGroup.add(_riverButton);

		icon = _iconFactory.getSimpleCellIcon(CellTypeCst.CITY, IconFactory.FOG, IconFactory.SIZE_SIMPLE_SIZE, 1);
		_cityButton = new JToggleButton(icon);
		_cityButton.setPreferredSize(dimension);
		_cityButton.setSelectedIcon(_iconFactory.getSimpleCellIcon(	CellTypeCst.CITY,
																	IconFactory.NO_FOG,
																	IconFactory.SIZE_SIMPLE_SIZE,
																	1));
		_cityButton.addActionListener(this);
		add(_cityButton);
		CellTypeGroup.add(_cityButton);

		icon = _iconFactory.getSimpleCellIcon(CellTypeCst.GRASSLAND, IconFactory.FOG, IconFactory.SIZE_SIMPLE_SIZE, 1);
		_grasslandButton = new JToggleButton(icon);
		_grasslandButton.setPreferredSize(dimension);
		_grasslandButton.setSelectedIcon(_iconFactory.getSimpleCellIcon(CellTypeCst.GRASSLAND,
																		IconFactory.NO_FOG,
																		IconFactory.SIZE_SIMPLE_SIZE,
																		1));
		_grasslandButton.addActionListener(this);
		add(_grasslandButton);
		CellTypeGroup.add(_grasslandButton);

		icon = _iconFactory.getSimpleCellIcon(CellTypeCst.HILL, IconFactory.FOG, IconFactory.SIZE_SIMPLE_SIZE, 1);
		_hillButton = new JToggleButton(icon);
		_hillButton.setPreferredSize(dimension);
		_hillButton.setSelectedIcon(_iconFactory.getSimpleCellIcon(	CellTypeCst.HILL,
																	IconFactory.NO_FOG,
																	IconFactory.SIZE_SIMPLE_SIZE,
																	1));
		_hillButton.addActionListener(this);
		add(_hillButton);
		CellTypeGroup.add(_hillButton);

		icon = _iconFactory.getSimpleCellIcon(CellTypeCst.MOUNTAIN, IconFactory.FOG, IconFactory.SIZE_SIMPLE_SIZE, 1);
		_mountainButton = new JToggleButton(icon);
		_mountainButton.setPreferredSize(dimension);
		_mountainButton.setSelectedIcon(_iconFactory.getSimpleCellIcon(	CellTypeCst.MOUNTAIN,
																		IconFactory.NO_FOG,
																		IconFactory.SIZE_SIMPLE_SIZE,
																		1));
		_mountainButton.addActionListener(this);
		add(_mountainButton);
		CellTypeGroup.add(_mountainButton);

		icon = _iconFactory.getSimpleCellIcon(CellTypeCst.LIGHT_WOOD, IconFactory.FOG, IconFactory.SIZE_SIMPLE_SIZE, 1);
		_lightWoodButton = new JToggleButton(icon);
		_lightWoodButton.setPreferredSize(dimension);
		_lightWoodButton.setSelectedIcon(_iconFactory.getSimpleCellIcon(CellTypeCst.LIGHT_WOOD,
																		IconFactory.NO_FOG,
																		IconFactory.SIZE_SIMPLE_SIZE,
																		1));
		_lightWoodButton.addActionListener(this);
		add(_lightWoodButton);
		CellTypeGroup.add(_lightWoodButton);

		icon = _iconFactory.getSimpleCellIcon(CellTypeCst.HEAVY_WOOD, IconFactory.FOG, IconFactory.SIZE_SIMPLE_SIZE, 1);
		_heavyWoodButton = new JToggleButton(icon);
		_heavyWoodButton.setPreferredSize(dimension);
		_heavyWoodButton.setSelectedIcon(_iconFactory.getSimpleCellIcon(CellTypeCst.HEAVY_WOOD,
																		IconFactory.NO_FOG,
																		IconFactory.SIZE_SIMPLE_SIZE,
																		1));
		_heavyWoodButton.addActionListener(this);
		add(_heavyWoodButton);
		CellTypeGroup.add(_heavyWoodButton);


		icon = _iconFactory.getSimpleCellIcon(CellTypeCst.ROAD, IconFactory.FOG, IconFactory.SIZE_SIMPLE_SIZE, 1);
		_roadButton = new JToggleButton(icon);
		_roadButton.setPreferredSize(dimension);
		_roadButton.setSelectedIcon(_iconFactory.getSimpleCellIcon(CellTypeCst.ROAD,
																		IconFactory.NO_FOG,
																		IconFactory.SIZE_SIMPLE_SIZE,
																		1));
		_roadButton.addActionListener(this);
		add(_roadButton);
		CellTypeGroup.add(_roadButton);
		
		
		addSeparator();

		final ButtonGroup cellModifierGroup = new ButtonGroup();
		_cell_1xButton = new JToggleButton(_iconFactory.getCellModifier(IconFactory.CELL_1));
		_cell_1xButton.setPreferredSize(dimension);
		_cell_1xButton.setSelectedIcon(_iconFactory.getSelectedCellModifier(IconFactory.CELL_1));
		_cell_1xButton.addActionListener(this);
		_cell_1xButton.setSelected(true);
		add(_cell_1xButton);
		cellModifierGroup.add(_cell_1xButton);

		_cell_9xButton = new JToggleButton(_iconFactory.getCellModifier(IconFactory.CELL_9));
		_cell_9xButton.setPreferredSize(dimension);
		_cell_9xButton.setSelectedIcon(_iconFactory.getSelectedCellModifier(IconFactory.CELL_9));
		_cell_9xButton.addActionListener(this);
		add(_cell_9xButton);
		cellModifierGroup.add(_cell_9xButton);

		_cell_25xButton = new JToggleButton(_iconFactory.getCellModifier(IconFactory.CELL_25));
		_cell_25xButton.setPreferredSize(dimension);
		_cell_25xButton.setSelectedIcon(_iconFactory.getSelectedCellModifier(IconFactory.CELL_25));
		_cell_25xButton.addActionListener(this);
		add(_cell_25xButton);
		cellModifierGroup.add(_cell_25xButton);
		setEnabledToolBar(false);
	}

	public final void setEnabledToolBar(final boolean p_enabled)
	{
		_seaButton.setEnabled(p_enabled);
		_cityButton.setEnabled(p_enabled);
		_grasslandButton.setEnabled(p_enabled);
		_heavyWoodButton.setEnabled(p_enabled);
		_roadButton.setEnabled(p_enabled);
		_hillButton.setEnabled(p_enabled);
		_riverButton.setEnabled(p_enabled);
		_lightWoodButton.setEnabled(p_enabled);
		_mountainButton.setEnabled(p_enabled);
		_cell_1xButton.setEnabled(p_enabled);
		_cell_9xButton.setEnabled(p_enabled);
		_cell_25xButton.setEnabled(p_enabled);
		super.setEnabled(p_enabled);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent anActionEvent)
	{
		if (_seaButton == anActionEvent.getSource())
		{
			_listener.selectionCellTypeChanged(this, CellTypeCst.SEA, 1, 1);
		} else if (_riverButton == anActionEvent.getSource())
		{
			_listener.selectionCellTypeChanged(this, CellTypeCst.RIVER, 1, 1);
		} else if (_cityButton == anActionEvent.getSource())
		{
			_listener.selectionCellTypeChanged(this, CellTypeCst.CITY, 1, 1);
		} else if (_grasslandButton == anActionEvent.getSource())
		{
			_listener.selectionCellTypeChanged(this, CellTypeCst.GRASSLAND, 1, 1);
		} else if (_lightWoodButton == anActionEvent.getSource())
		{
			_listener.selectionCellTypeChanged(this, CellTypeCst.LIGHT_WOOD, 1, 1);
		} else if (_heavyWoodButton == anActionEvent.getSource())
		{
			_listener.selectionCellTypeChanged(this, CellTypeCst.HEAVY_WOOD, 1, 1);
		} else if (_roadButton == anActionEvent.getSource())
		{
			_listener.selectionCellTypeChanged(this, CellTypeCst.ROAD, 1, 1);
		} else if (_hillButton == anActionEvent.getSource())
		{
			_listener.selectionCellTypeChanged(this, CellTypeCst.HILL, 1, 1);
		} else if (_mountainButton == anActionEvent.getSource())
		{
			_listener.selectionCellTypeChanged(this, CellTypeCst.MOUNTAIN, 1, 1);
		} else if (_cell_1xButton == anActionEvent.getSource())
		{
			_listener.selectionModifierToggleButtonChanged(IconFactory.CELL_1);
		} else if (_cell_9xButton == anActionEvent.getSource())
		{
			_listener.selectionModifierToggleButtonChanged(IconFactory.CELL_9);
		} else if (_cell_25xButton == anActionEvent.getSource())
		{
			_listener.selectionModifierToggleButtonChanged(IconFactory.CELL_25);
		} else
		{
			Assert.fail("Unsopported Action Event Source");
		}

	}
}