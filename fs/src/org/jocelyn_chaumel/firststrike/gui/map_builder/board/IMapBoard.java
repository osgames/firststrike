package org.jocelyn_chaumel.firststrike.gui.map_builder.board;

import org.jocelyn_chaumel.tools.data_type.Coord;

public interface IMapBoard
{
	/**
	 * This method is called when the default skin folder has been updated.
	 */
	public void selectDefaultSkinDir();

	/**
	 * This method is called when the user would like modify the cell's name.
	 * 
	 * @param p_cell Current cell.
	 */
	public void renameCell(final Coord p_cell);
}
