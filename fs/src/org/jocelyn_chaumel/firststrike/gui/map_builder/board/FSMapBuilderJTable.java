/*
 * Created on Dec 3, 2005
 */
package org.jocelyn_chaumel.firststrike.gui.map_builder.board;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.ColumnHeaderRenderer;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.map_builder.FSMapBuilderFrame;
import org.jocelyn_chaumel.firststrike.gui.map_builder.contextual_menu.FSMBContextualMenuMgr;
import org.jocelyn_chaumel.firststrike.gui.map_builder.contextual_menu.default_skin.FSDefaultSkinDirSelectionDialog;
import org.jocelyn_chaumel.firststrike.gui.map_builder.contextual_menu.rename_city.RenameCityDlg;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.RangeStrut;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class FSMapBuilderJTable extends JTable implements MouseListener, IMapBoard
{
	private CellTypeCst _currentCellType = CellTypeCst.SEA;
	private int _minIdxCellType = 0;
	private int _maxIdxCellType = 0;
	private int _curIdxCellType = 0;
	private int _currentCellModifier = 0;
	private final static FSLogger _fsLogger = FSLoggerManager.getLogger(FSMapBuilderJTable.class.getName());
	private File _defaultSkinFolder = null;
	private final FSMapBuilderFrame _mainFrame;

	private final FSMBContextualMenuMgr _contextualMenuMgr;

	/**
	 * Contructeur param�tris�.
	 * 
	 * @param p_model Mod�le de donn�es.
	 */
	public FSMapBuilderJTable(final FSMapBuilderFrame p_mainFrame, final FSMapBuilderTableModel p_model)
	{
		super(p_model);
		_contextualMenuMgr = new FSMBContextualMenuMgr(this);
		_mainFrame = p_mainFrame;
		setComponentPopupMenu(_contextualMenuMgr);

		// Pour emp�cher le d�placement des colonnes
		tableHeader.setReorderingAllowed(false);
		final FSMapBuilderTableCellRenderer cellRenderer = new FSMapBuilderTableCellRenderer();
		final ColumnHeaderRenderer colRenderer = new ColumnHeaderRenderer(this);
		setDefaultRenderer(CellTypeCst.class, cellRenderer);
		setShowGrid(false);
		setIntercellSpacing(new Dimension(0, 0));
		setCellSelectionEnabled(false);
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		addMouseListener(this);
		setAutoscrolls(true);
		getTableHeader().setTable(this);
		setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		TableColumn column = null;
		Component comp = null;
		int cellWidth = 0;
		Dimension dimension;
		final int size = columnModel.getColumnCount();
		TableCellRenderer currentRenderer = null;
		Class columnClass = null;

		for (int i = 0; i < size; i++)
		{
			column = columnModel.getColumn(i);
			// Set the header
			column.setHeaderRenderer(colRenderer);

			// Set the column width in 4 steps
			// 1- Get the renderer
			columnClass = p_model.getColumnClass(i);
			currentRenderer = getDefaultRenderer(columnClass);

			// 2- Create a fake Cell
			comp = currentRenderer.getTableCellRendererComponent(this, p_model.getValueAt(0, i), false, false, 0, i);

			// 3- Get the width
			dimension = comp.getPreferredSize();
			cellWidth = dimension.width;

			// 4- Set the prefered width to the column with the fake cell's
			// width
			column.setPreferredWidth(cellWidth);

			// No resizable
			// column.setResizable(false);
		}
		setRowHeight(IconFactory.CELL_SIZE);
	}

	/**
	 * Refresh all cells of the board.
	 */
	public final void refreshAllCells()
	{
		((FSMapBuilderTableModel) getModel()).refreshAll();
	}

	public final boolean hasDefaultSkinDir()
	{
		return _defaultSkinFolder != null;
	}

	public File getDefaultSkinDir()
	{
		Assert.check(_defaultSkinFolder != null, "No default skin dir has been specified");
		return _defaultSkinFolder;
	}

	@Override
	public void mouseClicked(final MouseEvent aMouseEvent)
	{
		Assert.preconditionNotNull(aMouseEvent, "Mouse Event");

		final int mouseX = aMouseEvent.getX();
		final int mouseY = aMouseEvent.getY();
		final Point point = new Point(mouseX, mouseY);
		final int row = rowAtPoint(point);
		final int col = columnAtPoint(point);
		Cell currentCell = null;
		CellTypeCst currentCellType = null;
		int currentCellIdx = 0;

		final int maxCol = dataModel.getColumnCount();
		final int maxRow = dataModel.getRowCount();

		final RangeStrut range = new RangeStrut(col, row, _currentCellModifier, maxCol, maxRow);

		for (int y = range._minY; y <= range._maxY; y++)
		{
			for (int x = range._minX; x <= range._maxX; x++)
			{
				currentCell = (Cell) dataModel.getValueAt(y, x);
				currentCellType = currentCell.getCellType();
				currentCellIdx = currentCell.getIndex();
				if (currentCellType != _currentCellType || currentCellIdx != _curIdxCellType)
				{
					currentCellType = _currentCellType;
					currentCell.setCellType(_currentCellType);
					currentCell.setIndex(_curIdxCellType);
					dataModel.setValueAt(currentCell, y, x);
				}

				_curIdxCellType++;
				if (_curIdxCellType > _maxIdxCellType)
				{
					_curIdxCellType = _minIdxCellType;
				}
			}
		}
	}

	public final void setCellType(final CellTypeCst p_cellType, final int p_minIdxCellType, int p_maxIdxCellType)
	{
		_currentCellType = p_cellType;
		_curIdxCellType = p_minIdxCellType;
		_minIdxCellType = p_minIdxCellType;
		_maxIdxCellType = p_maxIdxCellType;
	}

	public final void setIdxCellType(final int p_minIdxCellType, final int p_maxIdxCellType)
	{
		_curIdxCellType = p_minIdxCellType;
		_minIdxCellType = p_minIdxCellType;
		_maxIdxCellType = p_maxIdxCellType;
	}

	public final void setCellModifier(final int p_cellModifier)
	{
		Assert.precondition_between(p_cellModifier, 0, 2, "Invalid Cell Modifier");

		_currentCellModifier = p_cellModifier;
	}

	/*
		private final void resizingRowCol()
		{
			final int newCellSize;
			newCellSize = _displaySizeCell == IconFactory.SIZE_DOUBLE_SIZE ? 40 : 20;

			final int size = columnModel.getColumnCount();
			TableColumn column = null;
			for (int i = 0; i < size; i++)
			{
				column = columnModel.getColumn(i);
				column.setPreferredWidth(newCellSize);
				column.setMinWidth(newCellSize);
				column.setMaxWidth(newCellSize);
				column.setWidth(newCellSize);
			}

			setRowHeight(newCellSize);
		}
	*/

	@Override
	public void selectDefaultSkinDir()
	{
		_fsLogger.systemAction("Open user icon dir");

		final FSDefaultSkinDirSelectionDialog dialog = new FSDefaultSkinDirSelectionDialog(	_mainFrame,
																							_defaultSkinFolder);
		dialog.pack();
		dialog.setVisible(true);
		dialog.dispose();
		if (!dialog.getStatus())
		{
			_fsLogger.systemSubAction("Select default skin dir canceled");
			return;
		}

		_defaultSkinFolder = dialog.getDefaultSkinDir();
		if (_defaultSkinFolder == null)
		{
			_fsLogger.systemSubAction("Default skin dir selected: null");
		} else
		{
			_fsLogger.systemSubAction("Default skin dir selected: " + _defaultSkinFolder.getAbsolutePath());
		}
		IconFactory.getInstance().setUserSkin(_defaultSkinFolder);
		_curIdxCellType = 1;
		_minIdxCellType = 1;
		_maxIdxCellType = 1;
		refreshAllCells();

		_mainFrame.selectionCellTypeChanged(this, _currentCellType, _minIdxCellType, _maxIdxCellType);
	}

	@Override
	public void mousePressed(MouseEvent arg0)
	{
		Assert.preconditionNotNull(arg0, "Mouse Event");

	}

	@Override
	public void mouseReleased(final MouseEvent aMouseEvent)
	{
		Assert.preconditionNotNull(aMouseEvent, "Mouse Event");
	}

	@Override
	public void mouseEntered(final MouseEvent arg0)
	{
		Assert.preconditionNotNull(arg0, "Mouse Event");

	}

	@Override
	public void mouseExited(final MouseEvent arg0)
	{
		Assert.preconditionNotNull(arg0, "Mouse Event");

	}

	@Override
	public final void renameCell(final Coord p_cell)
	{
		final Cell currentCell = (Cell) getValueAt(p_cell.getY(), p_cell.getX());

		final RenameCityDlg dlg = new RenameCityDlg(_mainFrame, currentCell);
		dlg.pack();
		dlg.setVisible(true);
		dlg.dispose();
	}
}