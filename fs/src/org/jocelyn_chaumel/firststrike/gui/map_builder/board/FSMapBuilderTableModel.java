/*
 * Created on Feb 11, 2006
 */
package org.jocelyn_chaumel.firststrike.gui.map_builder.board;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class FSMapBuilderTableModel implements TableModel
{
	private final Cell[][] _cellArray;
	private final ArrayList<TableModelListener> _tableModelListener = new ArrayList<TableModelListener>();

	public FSMapBuilderTableModel(final int p_mapHeight, final int p_mapWidth)
	{
		Assert.precondition_between(p_mapHeight, 20, 2000, "Invalid Map Height");
		Assert.precondition_between(p_mapWidth, 20, 2000, "Invalid Map Width");

		_cellArray = new Cell[p_mapHeight][p_mapWidth];
		for (int y = 0; y < p_mapHeight; y++)
		{
			for (int x = 0; x < p_mapWidth; x++)
			{
				_cellArray[y][x] = new Cell(CellTypeCst.SEA, 1, new Coord(x, y), null, false);
			}
		}
	}

	public FSMapBuilderTableModel(final Cell[][] p_cellArray)
	{
		Assert.preconditionNotNull(p_cellArray, "Cell Array");
		Assert.precondition_between(p_cellArray.length, 5, 2000, "Invalid cell array height");
		Assert.precondition_between(p_cellArray[0].length, 5, 2000, "Invalid cell array width");

		_cellArray = p_cellArray;
	}

	public final Cell[][] getCellArray()
	{
		// Suppression des Regions car elles seront recalculées un peu plus loin
		for (int y=0;y < _cellArray.length; y++)
		{
			for (int x=0;x < _cellArray[y].length; x++)
			{
				_cellArray[y][x].getAreaList().clear();
			}
		}
		return _cellArray;
	}

	@Override
	public int getColumnCount()
	{
		return _cellArray[0].length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount()
	{
		return _cellArray.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(final int p_row, final int p_col)
	{
		Assert.precondition(p_row >= 0, "Invalid Row Index");
		Assert.precondition(p_col >= 0, "Invalid Column Index");
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnClass(int)
	 */
	@Override
	public Class<CellTypeCst> getColumnClass(int p_index)
	{
		Assert.preconditionIsPositive(p_index, "invalid Column");

		return CellTypeCst.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(final int p_row, final int p_col)
	{
		return _cellArray[p_row][p_col];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#setValueAt(java.lang.Object, int, int)
	 */
	@Override
	public void setValueAt(final Object p_cell, final int p_row, final int p_col)
	{
		Assert.preconditionNotNull(p_cell, "Map Builder Cell");
		Assert.precondition(p_cell instanceof Cell, "Invalid Map builder Table Cell Object");

		_cellArray[p_row][p_col] = (Cell) p_cell;

		fireValueChanged(new TableModelEvent(this, p_row, p_row, p_col, TableModelEvent.UPDATE));
	}

	public final void refreshAll()
	{
		for (int y = 0; y < _cellArray.length; y++)
		{
			for (int x = 0; x < _cellArray[y].length; x++)
			{

				setValueAt(getValueAt(y, x), y, x);
			}
		}
	}

	private void fireValueChanged(final TableModelEvent anEvent)
	{
		final Iterator iter = _tableModelListener.iterator();
		while (iter.hasNext())
		{
			((TableModelListener) iter.next()).tableChanged(anEvent);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int aColIndex)
	{
		return "" + aColIndex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.table.TableModel#addTableModelListener(javax.swing.event.
	 * TableModelListener)
	 */
	@Override
	public void addTableModelListener(final TableModelListener p_listener)
	{
		_tableModelListener.add(p_listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.table.TableModel#removeTableModelListener(javax.swing.event
	 * .TableModelListener)
	 */
	@Override
	public void removeTableModelListener(final TableModelListener p_listener)
	{
		_tableModelListener.remove(p_listener);
	}

}
