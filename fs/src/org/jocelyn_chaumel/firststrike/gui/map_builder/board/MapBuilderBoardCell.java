/*
 * Created on Dec 10, 2005
 */
package org.jocelyn_chaumel.firststrike.gui.map_builder.board;

import java.awt.Dimension;

import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * @author Jocelyn Chaumel
 */
public class MapBuilderBoardCell extends JLabel
{

	/**
	 * @param p_cellIcon
	 */
	public MapBuilderBoardCell(Icon p_cellIcon)
	{
		super(p_cellIcon);

		final int height = p_cellIcon.getIconHeight();
		final int width = p_cellIcon.getIconWidth();
		final Dimension dimension = new Dimension(width, height);
		setPreferredSize(dimension);
		setSize(dimension);
		setMaximumSize(dimension);
		setMinimumSize(dimension);

	}
}