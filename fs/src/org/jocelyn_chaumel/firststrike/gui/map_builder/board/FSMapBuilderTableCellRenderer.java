/*
 * Created on Feb 11, 2006
 */
package org.jocelyn_chaumel.firststrike.gui.map_builder.board;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class FSMapBuilderTableCellRenderer implements TableCellRenderer
{
	private final static IconFactory _imageFactory = IconFactory.getInstance();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax
	 * .swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	public Component getTableCellRendererComponent(	final JTable p_table,
													final Object p_cell,
													final boolean anIsSelectedFlag,
													final boolean anHasFocusFlag,
													final int y,
													final int x)
	{
		Assert.preconditionNotNull(p_table, "Table");

		Assert.precondition(y >= 0, "Invalid Y");
		Assert.precondition(x >= 0, "Invalid X");

		final Cell cell = (Cell) p_cell;
		final CellTypeCst cellType = cell.getCellType();

		return new MapBuilderBoardCell(_imageFactory.getSimpleCellIcon(	cellType,
																		IconFactory.NO_FOG,
																		IconFactory.SIZE_SIMPLE_SIZE,
																		cell.getIndex()));
	}

}
