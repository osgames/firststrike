package org.jocelyn_chaumel.firststrike.gui.map_builder.contextual_bar;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.commun.list_cell_renderer.FSTwoIconsListCellRenderer;
import org.jocelyn_chaumel.firststrike.gui.map_builder.IFSCellTypeUpdateListener;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class IconListPanel extends JPanel implements ListSelectionListener
{

	private final IconFactory _iconFactory = IconFactory.getInstance();
	private final IFSCellTypeUpdateListener _frame;
	private CellTypeCst _currentCellTypeCst = null;

	private final JList _cellList = new JList();

	public IconListPanel(final IFSCellTypeUpdateListener p_frame)
	{
		_frame = p_frame;
		DefaultComboBoxModel listModel = new DefaultComboBoxModel();
		_cellList.setModel(listModel);
		_cellList.setCellRenderer(new FSTwoIconsListCellRenderer());
		refreshCellList(CellTypeCst.SEA);
		_cellList.addListSelectionListener(this);
		_cellList.setEnabled(false);
		_cellList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		JScrollPane jScrollPane1 = new JScrollPane();
		jScrollPane1.setViewportView(_cellList);

		add(jScrollPane1);

	}

	public void setEnabledPanel(final boolean p_enable)
	{
		_cellList.setEnabled(p_enable);
		super.setEnabled(p_enable);
	}

	public void refreshCellList(final CellTypeCst p_cellType)
	{
		_currentCellTypeCst = p_cellType;
		_cellList.setEnabled(false);
		final Coord coord = new Coord(1, 1);
		final DefaultComboBoxModel model = (DefaultComboBoxModel) _cellList.getModel();
		model.removeAllElements();
		int max = _iconFactory.getSimpleCellIconMaxIndex(p_cellType, IconFactory.NO_FOG, IconFactory.SIZE_SIMPLE_SIZE);
		for (int i = 1; i <= max; i++)
		{
			model.addElement(new Cell(p_cellType, i, coord, null, false));
		}
		_cellList.setSelectedIndex(0);
		_cellList.setEnabled(true);
	}

	public void refreshCurrentCellType()
	{
		refreshCellList(_currentCellTypeCst);
	}

	@Override
	public void valueChanged(ListSelectionEvent p_arg0)
	{

		if (_cellList.isEnabled())
		{
			final int max = _cellList.getMaxSelectionIndex() + 1;
			final int min = _cellList.getMinSelectionIndex() + 1;
			_frame.selectionIdxCellTypeChanged(min, max);
		}
	}
}
