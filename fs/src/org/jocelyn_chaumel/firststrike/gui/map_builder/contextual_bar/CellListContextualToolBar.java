package org.jocelyn_chaumel.firststrike.gui.map_builder.contextual_bar;

import javax.swing.JToolBar;

import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.gui.map_builder.IFSCellTypeUpdateListener;

public class CellListContextualToolBar extends JToolBar
{

	private final IconListPanel _iconListPanel;

	public CellListContextualToolBar(final IFSCellTypeUpdateListener p_frame)
	{
		_iconListPanel = new IconListPanel(p_frame);

		add(_iconListPanel);
	}

	@Override
	public void setEnabled(final boolean p_enable)
	{
		_iconListPanel.setEnabled(p_enable);
		super.setEnabled(p_enable);
	}

	public void refreshCellType(final CellTypeCst p_cellType)
	{
		_iconListPanel.refreshCellList(p_cellType);
	}

	public final void refreshCurrentCellType()
	{
		_iconListPanel.refreshCurrentCellType();
	}

	public final void setEnabledToolBar(final boolean p_enabled)
	{
		_iconListPanel.setEnabledPanel(p_enabled);
		super.setEnabled(p_enabled);
	}
}
