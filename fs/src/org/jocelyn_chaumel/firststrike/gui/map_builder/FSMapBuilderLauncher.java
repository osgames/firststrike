/*
 * Created on Feb 5, 2006
 */
package org.jocelyn_chaumel.firststrike.gui.map_builder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.JFrame;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLevel;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.tools.FileMgr;

/**
 * @author Jocelyn Chaumel
 */
public class FSMapBuilderLauncher
{

	private final static FSLogger _fsLogger = FSLoggerManager.getLogger(FSMapBuilderLauncher.class.getName());

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 * 
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws SecurityException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */

	private static void createAndShowLocalGUI() throws MalformedURLException
	{
		try
		{
			_fsLogger.systemAction("Instance Icon Factory");
			IconFactory.createInstance();
		} catch (Exception ex)
		{
			_fsLogger.systemError(ex.getMessage(), ex);
			System.exit(1);
		}

		_fsLogger.systemAction("Build main window");
		// Make sure we have nice window decorations.
		JFrame.setDefaultLookAndFeelDecorated(true);
		final FSMapBuilderFrame frame = new FSMapBuilderFrame();

		// Display the window.
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setSize(700, 500);
		frame.setVisible(true);
	}

	public static void main(final String[] p_argArray)
	{
		FSLoggerManager.setConsoleLogger(FSLevel.ALL);
		_fsLogger.system("Map builder lauched");

		ApplicationConfigurationMgr confMgr = null;
		try
		{
			if (p_argArray != null && p_argArray.length >= 1)
			{
				final File localAppDataDir = new File(p_argArray[0]);
				if (!localAppDataDir.isDirectory())
				{
					System.out.println("The local Application Data Folder is not a valid folder: '" + p_argArray[0]
							+ "'.");
				}
				confMgr = ApplicationConfigurationMgr.getCreateInstance(localAppDataDir);
			} else
			{
				confMgr = ApplicationConfigurationMgr.getCreateInstance(FileMgr.getCurrentDir());
			}
		} catch (IOException ex)
		{
			_fsLogger.systemError("Unable to load FirstStrike.Properties File", ex);
		}
		_fsLogger.systemAction("First Strike version :" + confMgr.getFSVersion());

		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					createAndShowLocalGUI();
				} catch (Exception ex)
				{
					_fsLogger.systemError("", ex);
				}
			}
		});
	}
}