/*
 * Created on Feb 11, 2006
 */
package org.jocelyn_chaumel.firststrike.gui.map_builder;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public final class FSMapBuilderGlobalMenu
{
	private final IFSMapBuilderGlobalOperation _parentFrame;

	private final JMenuBar _menuBar = new JMenuBar();

	private final JMenu _fileMenu = new JMenu("Fichier");

	private final JMenuItem _newMapItemMenu;

	private final JMenuItem _openMapItemMenu;

	private final JMenuItem _closeMapItemMenu;

	private final JMenuItem _saveMapItemMenu;

	private final JMenuItem _saveAsMapItemMenu;

	private final JMenuItem _exitItemMenu;

	public FSMapBuilderGlobalMenu(final IFSMapBuilderGlobalOperation aParentFrame)
	{
		Assert.preconditionNotNull(aParentFrame, "Parent Frame");

		_parentFrame = aParentFrame;

		_newMapItemMenu = new JMenuItem(new AbstractAction()
		{

			@Override
			public void actionPerformed(final ActionEvent anEvent)
			{
				Assert.preconditionNotNull(anEvent, "Action Event");
				_parentFrame.newMap();
			}
		});
		_newMapItemMenu.setText("New Map ...");

		_openMapItemMenu = new JMenuItem(new AbstractAction()
		{

			@Override
			public void actionPerformed(final ActionEvent anEvent)
			{
				Assert.preconditionNotNull(anEvent, "Action Event");
				_parentFrame.openMap();
			}
		});
		_openMapItemMenu.setText("Open Map ...");

		_closeMapItemMenu = new JMenuItem(new AbstractAction()
		{

			@Override
			public void actionPerformed(final ActionEvent anEvent)
			{
				Assert.preconditionNotNull(anEvent, "Action Event");
				_parentFrame.closeMap();
			}
		});
		_closeMapItemMenu.setText("Close Map");

		_saveMapItemMenu = new JMenuItem(new AbstractAction()
		{

			@Override
			public void actionPerformed(final ActionEvent anEvent)
			{
				Assert.preconditionNotNull(anEvent, "Action Event");
				_parentFrame.saveMap();
			}
		});
		_saveMapItemMenu.setText("Save Map");

		_saveAsMapItemMenu = new JMenuItem(new AbstractAction()
		{

			@Override
			public void actionPerformed(final ActionEvent anEvent)
			{
				Assert.preconditionNotNull(anEvent, "Action Event");
				_parentFrame.saveAsMap();
			}
		});
		_saveAsMapItemMenu.setText("Save Map As ...");

		_exitItemMenu = new JMenuItem(new AbstractAction()
		{

			@Override
			public void actionPerformed(final ActionEvent anEvent)
			{
				Assert.preconditionNotNull(anEvent, "Action Event");
				_parentFrame.exitMapBuilder();
			}
		});
		_exitItemMenu.setText("Quit");

		_fileMenu.add(_newMapItemMenu);
		_fileMenu.add(_openMapItemMenu);
		_fileMenu.add(_closeMapItemMenu);
		_fileMenu.addSeparator();
		_fileMenu.add(_saveMapItemMenu);
		_fileMenu.add(_saveAsMapItemMenu);
		_fileMenu.addSeparator();
		_fileMenu.add(_exitItemMenu);
		_menuBar.add(_fileMenu);
	}

	public final JMenuBar getMenuBarWithoutMapFile()
	{
		updateItemMenu(false);
		return _menuBar;
	}

	public void updateItemMenu(final boolean anHasMapFileFlag)
	{
		_newMapItemMenu.setEnabled(true);
		_openMapItemMenu.setEnabled(true);
		_closeMapItemMenu.setEnabled(anHasMapFileFlag);

		_saveMapItemMenu.setEnabled(anHasMapFileFlag);
		_saveAsMapItemMenu.setEnabled(anHasMapFileFlag);
	}
}