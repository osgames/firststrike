/*
 * Created on Feb 12, 2006
 */
package org.jocelyn_chaumel.firststrike.gui.map_builder;

import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;

/**
 * @author Jocelyn Chaumel
 */
public interface IFSCellTypeUpdateListener
{
	public void selectionCellTypeChanged(	final Object p_source,
											final CellTypeCst p_cellType,
											final int p_minIdxCellType,
											final int p_maxIdxCellType);

	/**
	 * The method is executed when a specific icon index has been selected.
	 * 
	 * @param p_idxCellType New Cell Type.
	 */
	public void selectionIdxCellTypeChanged(final int p_minIdxCellType, final int p_maxIdxCellType);

	public void selectionModifierToggleButtonChanged(final int p_cellModifier);
}
