package org.jocelyn_chaumel.firststrike.gui.map_builder.menu.manage_area;

import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.map_builder.FSMapBuilderFrame;

public class AreaNameManagementDlg extends FSDialog
{

	private final FSMap _map;
	private final AreaNameManagmentPnl _panel;

	public AreaNameManagementDlg(final FSMapBuilderFrame p_mainFrame, final FSMap p_map)
	{
		super(p_mainFrame, "Areas' Name Management", true);
		_map = p_map;
		_panel = new AreaNameManagmentPnl(_map);
		getContentPane().add(_panel);
	}

	@Override
	public final void setVisible(final boolean p_isVisible)
	{
		if (!p_isVisible)
		{

		}
		super.setVisible(p_isVisible);
	}

	public boolean getStatus()
	{
		return _panel.getStatus();
	}
}
