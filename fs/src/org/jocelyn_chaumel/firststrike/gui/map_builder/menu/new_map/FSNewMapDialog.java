/*
 * Created on Feb 11, 2006
 */
package org.jocelyn_chaumel.firststrike.gui.map_builder.menu.new_map;

import java.awt.HeadlessException;

import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.map_builder.FSMapBuilderFrame;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class FSNewMapDialog extends FSDialog
{

	private final FSMapDimensionPanel _panel;

	/**
	 * @param p_frame
	 * @param aTitle
	 * @param aDialogWidth
	 * @param aDialogHeight
	 * @param aQuestion
	 * @throws HeadlessException
	 */
	public FSNewMapDialog(final FSMapBuilderFrame p_frame)
	{
		super(p_frame, "New Map", true);
		_panel = new FSMapDimensionPanel(this);
		getContentPane().add(_panel);
	}

	public final boolean getStatus()
	{
		return _panel.getStatus();
	}

	public final int getMapHeight()
	{
		Assert.precondition(_panel.getStatus(), "New map has been canceled");
		return _panel.getMapHeight();
	}

	public final int getMapWidth()
	{
		Assert.precondition(_panel.getStatus(), "New map has been canceled");
		return _panel.getMapWidth();
	}
}