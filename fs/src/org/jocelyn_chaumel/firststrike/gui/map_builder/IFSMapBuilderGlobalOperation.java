/*
 * Created on Feb 11, 2006
 */
package org.jocelyn_chaumel.firststrike.gui.map_builder;

/**
 * @author Jocelyn Chaumel
 */
public interface IFSMapBuilderGlobalOperation
{
	public void newMap();

	public void openMap();

	public boolean closeMap();

	public void saveMap();

	public void saveAsMap();

	public void exitMapBuilder();
}
