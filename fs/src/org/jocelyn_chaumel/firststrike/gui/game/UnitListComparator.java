package org.jocelyn_chaumel.firststrike.gui.game;

import java.util.Comparator;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;

public class UnitListComparator implements Comparator<AbstractUnit>
{

	@Override
	public int compare(final AbstractUnit p_unit1, final AbstractUnit p_unit2)
	{
		
		// 654 321
		// 2: Contained Unit
		// 3: Hit Points
		// 6 & 5: Move
		// 7: Turn Completed

		// UNIT #1
		// -----------------
		
		int unit1 = 0;
		int unit2 = 0;
		if (!p_unit1.isTurnCompleted())
		{
			unit1 = 1;
		}
		if (!p_unit2.isTurnCompleted())
		{
			unit2 = 1;
		}
		
		if (unit1 != unit2)
		{
			return unit2 - unit1;
		}
		
		unit1 = Math.round((p_unit1.getMove() / (float) p_unit1.getMovementCapacity()) * 100f);
		unit2 = Math.round((p_unit2.getMove() / (float) p_unit2.getMovementCapacity()) * 100f);

		if (unit1 != unit2)
		{
			return unit2 - unit1;
		}
		
		
		unit1 = Math.round((p_unit1.getHitPoints() / (float) p_unit1.getHitPointsCapacity()) * 1000f);
		unit2 = Math.round((p_unit2.getHitPoints() / (float) p_unit2.getHitPointsCapacity()) * 1000f);
			
		if (unit1 != unit2)
		{
			return unit2 - unit1;
		}

		unit1 = unit2 = 0;
		if (p_unit1 instanceof TransportShip)
		{
			unit1 = ((TransportShip) p_unit1).getContainedUnitList().size();
		}
		
		if (p_unit2 instanceof TransportShip)
		{
			unit2 = ((TransportShip) p_unit2).getContainedUnitList().size();
		}
		
		
		if (unit1 != unit2)
		{
			return unit2 - unit1;
		}
		
		
		unit2 += p_unit1.getId().getId();
		unit1 += p_unit2.getId().getId();


		return unit2 - unit1;
	}

}
