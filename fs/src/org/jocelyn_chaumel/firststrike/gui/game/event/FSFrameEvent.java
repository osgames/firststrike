package org.jocelyn_chaumel.firststrike.gui.game.event;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FSFrameEvent
{
	private final Object _source;
	private final Cell _currentCell;
	private final AbstractUnit _currentUnit;
	private final FirstStrikeGame _game;
	private final Object _otherData;

	public FSFrameEvent(final Object p_source,
						final FirstStrikeGame p_game,
						final Cell p_currentCell,
						final AbstractUnit p_currentUnit)
	{
		this(p_source, p_game, p_currentCell, p_currentUnit, null);
	}

	public FSFrameEvent(final Object p_source,
						final FirstStrikeGame p_game,
						final Cell p_currentCell,
						final AbstractUnit p_currentUnit,
						final Object p_otherData)
	{
		_source = p_source;
		_currentCell = p_currentCell;
		_currentUnit = p_currentUnit;
		_game = p_game;
		_otherData = p_otherData;
	}

	public final Object getSource()
	{
		return _source;
	}

	public final AbstractUnit getCurrentUnit()
	{
		return _currentUnit;
	}

	public final Cell getCurrentCell()
	{
		Assert.check(_currentCell != null, "Current Cell is null; This event doesn't describe the current cell.");

		return _currentCell;
	}

	public final FirstStrikeGame getGame()
	{
		return _game;
	}

	public final Object getOtherData()
	{
		Assert.preconditionNotNull(_otherData, "Other Data");
		return _otherData;
	}
}
