package org.jocelyn_chaumel.firststrike.gui.game.event;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;

public class FSWindowListener implements ComponentListener, WindowListener
{

	private final ApplicationConfigurationMgr _appConf = ApplicationConfigurationMgr.getInstance();
	private final FSLogger _logger = FSLoggerManager.getLogger(FSWindowListener.class.getName());

	@Override
	public void windowClosing(WindowEvent p_e)
	{
		FSFrame frame = (FSFrame) p_e.getSource();
		if (frame.isGameInProgesss())
		{
			if (!frame.closeGame(true))
			{
				return;
			}
		}
		System.exit(0);
	}

	@Override
	public void componentMoved(ComponentEvent p_e)
	{
		final Point point = p_e.getComponent().getLocation();
		_appConf.setScreenLocation(point);

		if (_appConf.isModified())
		{
			try
			{
				_appConf.save();
			} catch (IOException ex)
			{
				_logger.systemError("Unable to update the first strike property file", ex);
			}
		}
	}

	@Override
	public void componentResized(ComponentEvent p_e)
	{
		final Dimension dimension = p_e.getComponent().getSize();
		_appConf.setScreenDimension(dimension);

		if (_appConf.isModified())
		{
			try
			{
				_appConf.save();
			} catch (IOException ex)
			{
				_logger.systemError("Unable to update the first strike property file", ex);
			}
		}

	}

	@Override
	public void componentHidden(ComponentEvent p_e)
	{
		// NOP
	}

	@Override
	public void componentShown(ComponentEvent p_e)
	{
		// NOP
	}

	@Override
	public void windowActivated(WindowEvent p_e)
	{
		// NOP
	}

	@Override
	public void windowClosed(WindowEvent p_e)
	{
		// NOP
	}

	@Override
	public void windowDeactivated(WindowEvent p_e)
	{
		// NOP

	}

	@Override
	public void windowDeiconified(WindowEvent p_e)
	{
		// NOP
	}

	@Override
	public void windowIconified(WindowEvent p_e)
	{
		// NOP
	}

	@Override
	public void windowOpened(WindowEvent p_e)
	{
		// NOP
	}

}
