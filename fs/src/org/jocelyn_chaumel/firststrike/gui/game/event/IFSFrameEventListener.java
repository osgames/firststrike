package org.jocelyn_chaumel.firststrike.gui.game.event;

public interface IFSFrameEventListener
{
	/**
	 * Event launched when a game has been created or opened.
	 * 
	 * @param p_event Event.
	 */
	public void loadGameEvent(final FSFrameEvent p_event);

	/**
	 * Event launched when the game has been closed.
	 * 
	 * @param p_event Event.
	 */
	public void closeGameEvent(final FSFrameEvent p_event);

	/**
	 * Event launched when the current cell has been updated
	 * 
	 * @param p_event Event.
	 */
	public void updateCurrentCellEvent(final FSFrameEvent p_event);

	/**
	 * Event launched when the player conquer a city.
	 * 
	 * @param p_event Event.
	 */
	public void conquerCityEvent(final FSFrameEvent p_event);

	/**
	 * Event launched when the player lost a city.
	 * 
	 * @param p_event Event.
	 */
	public void lostCityEvent(final FSFrameEvent p_event);

	/**
	 * Event launched when the player lost an unit.
	 * 
	 * @param p_event Event.
	 */
	public void lostUnitEvent(final FSFrameEvent p_event);

	/**
	 * Event launched when the player's cities built at least one unit.
	 * 
	 * @param p_event Event.
	 */
	public void unitCreatedEvent(final FSFrameEvent p_event);

	/**
	 * Event launched when the selected unit has been changed.
	 * 
	 * @param p_event Event.
	 */
	public void updateCurrentUnitEvent(final FSFrameEvent p_event);

	/**
	 * Event launched when a unit has been performed any kind of action (move,
	 * loading, unloading).
	 * 
	 * @param p_event
	 */
	public void unitMovedEvent(final FSFrameEvent p_event);
}
