package org.jocelyn_chaumel.firststrike.gui.game.board;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractGroundLoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class DragNDropMgr implements MouseListener, MouseMotionListener
{
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");
	private final static FSLogger _logger = FSLoggerManager.getLogger(DragNDropMgr.class.getName());

	private boolean _dragInProgress = false;
	private final Cursor _cursorTank;
	private final Cursor _cursorFighter;
	private final Cursor _cursorTransport;
	private final Cursor _cursorDestroyer;
	private final Cursor _cursorBattleship;
	private final Cursor _cursorArtillery;
	private final Cursor _cursorInvalid;
	private final Cursor _cursorAttack;
	private final Cursor _cursorLoading;
	private FirstStrikeGame _currentGame;
	private AbstractUnit _currentUnit;
	private final BattleBoard _battleBoard;

	public DragNDropMgr(final BattleBoard p_battleBoard)
	{

		final Toolkit toolkit = Toolkit.getDefaultToolkit();
		final IconFactory iconFactory = IconFactory.getInstance();

		Image image = iconFactory.getCommunIcon(IconFactory.CURSOR_TANK).getImage();
		_cursorTank = toolkit.createCustomCursor(image, new Point(0, 0), IconFactory.CURSOR_TANK);

		image = iconFactory.getCommunIcon(IconFactory.CURSOR_ARTILLERY).getImage();
		_cursorArtillery = toolkit.createCustomCursor(image, new Point(0, 0), IconFactory.CURSOR_ARTILLERY);

		image = iconFactory.getCommunIcon(IconFactory.CURSOR_FIGHTER).getImage();
		_cursorFighter = toolkit.createCustomCursor(image, new Point(0, 0), IconFactory.CURSOR_FIGHTER);

		image = iconFactory.getCommunIcon(IconFactory.CURSOR_DESTROYER).getImage();
		_cursorDestroyer = toolkit.createCustomCursor(image, new Point(0, 0), IconFactory.CURSOR_DESTROYER);

		image = iconFactory.getCommunIcon(IconFactory.CURSOR_BATTLESHIP).getImage();
		_cursorBattleship = toolkit.createCustomCursor(image, new Point(0, 0), IconFactory.CURSOR_BATTLESHIP);

		image = iconFactory.getCommunIcon(IconFactory.CURSOR_TRANSPORT).getImage();
		_cursorTransport = toolkit.createCustomCursor(image, new Point(0, 0), IconFactory.CURSOR_TRANSPORT);

		image = iconFactory.getCommunIcon(IconFactory.CURSOR_INVALID).getImage();
		_cursorInvalid = toolkit.createCustomCursor(image, new Point(0, 0), IconFactory.CURSOR_INVALID);

		image = iconFactory.getCommunIcon(IconFactory.CURSOR_ATTACK).getImage();
		_cursorAttack = toolkit.createCustomCursor(image, new Point(0, 0), IconFactory.CURSOR_ATTACK);

		image = iconFactory.getCommunIcon(IconFactory.CURSOR_LOADING).getImage();
		_cursorLoading = toolkit.createCustomCursor(image, new Point(0, 0), IconFactory.CURSOR_LOADING);

		_battleBoard = p_battleBoard;
	}

	@Override
	public void mouseReleased(MouseEvent p_e)
	{
		if (_dragInProgress)
		{
			_logger.systemSubAction("Ending of the Drag and Drop operation");
			_dragInProgress = false;

			BattleBoard board = (BattleBoard) p_e.getSource();
			board.setCursor(Cursor.getDefaultCursor());

			final int x = p_e.getX();
			final int y = p_e.getY();
			final Point point = new Point(x, y);
			final int row = board.rowAtPoint(point);
			final int col = board.columnAtPoint(point);
			if (row == -1 || col == -1)
			{
				_logger.systemSubActionDetail("Drop location : outside of the board");
				return;
			}

			final Coord destination = new Coord(col, row);
			final Coord startLocation = _currentUnit.getPosition();

			if (startLocation.equals(destination))
			{
				_logger.systemSubActionDetail("Start location = drop location : No move has been launched");
				_battleBoard.setCurrentCell(startLocation);
				return;
			}

			final HumanPlayer player = (HumanPlayer) _currentGame.getCurrentPlayer();
			CoordSet coordSet = null;
			if (!_currentGame.getOptionMgr().isDebutMode())
			{
				coordSet = player.getShadowSet();

				if (coordSet.contains(destination))
				{
					_logger.systemSubActionDetail("Drop location into a shadowed cell : No move has been launched");
					displayInvalidDestinationMessage(_currentUnit.getUnitType());
					_battleBoard.setCurrentCell(startLocation);
					return;
				}
			}
			coordSet = player.getEnemySet().getCoordSet();
			if (coordSet.contains(destination))
			{
				if (startLocation.calculateIntDistance(destination) != 1 && _currentUnit instanceof TransportShip)
				{
					_logger.systemSubActionDetail("Drop location into a enemy cell : No move has been launched");
					displayInvalidDestinationMessage(_currentUnit.getUnitType());
					_battleBoard.setCurrentCell(startLocation);
					return;
				}

				if (startLocation.calculateIntDistance(destination) != 1 || _currentUnit.getMove() == 0)
				{
					_logger.systemSubActionDetail("Enemy too far or the current unit have not enough move point.");
					displayInvalidDestinationMessage(_currentUnit.getUnitType());
					_battleBoard.setCurrentCell(startLocation);
					return;
				}

				_battleBoard.attackEnnemy(destination);
				return;
			}

			final FSMapMgr mapMgr = _currentGame.getMapMgr();
			if (mapMgr.isAccessibleCoord(_currentUnit, destination, new CoordSet(), false))
			{
				if (_currentUnit.getMove() > 0) {
					_logger.systemSubActionDetail("Unit moved: " + destination);
					_battleBoard.tracePathAndGo(destination);
				} else {
					_logger.systemSubActionDetail("Unit trace new path: " + destination);
					_battleBoard.tracePath(destination);
				}
				return;
			}

			if (_currentUnit instanceof TransportShip)
			{
				final TransportShip transport = (TransportShip) _currentUnit;
				if (transport.isContainingUnit())
				{
					final Cell destinationCell = mapMgr.getMap().getCellAt(destination);
					if (destinationCell.isGroundCell() && startLocation.calculateIntDistance(destination) == 1)
					{
						_logger.systemSubActionDetail("Transport Ship unload Tank at : " + destination.toString());
						_battleBoard.unloadAnUnit(transport, destination);
						_battleBoard.setCurrentCell(startLocation);
						return;
					}
				}
			}

			if (_currentUnit instanceof AbstractGroundLoadableUnit && _currentUnit.getMove() == _currentUnit.getMovementCapacity()
					&& startLocation.calculateIntDistance(destination) == 1)
			{
				final AbstractUnitList transportList = _currentUnit.getPlayer().getUnitList().getUnitAt(destination)
						.getUnitByType(UnitTypeCst.TRANSPORT_SHIP);
				if (transportList.size() == 1)
				{
					final TransportShip transport = (TransportShip) transportList.get(0);
					if (transport.isLoadable(_currentUnit))
					{
						_logger.systemSubActionDetail("Tank loaded into the Transport Ship : " + transportList.get(0));
						_battleBoard.loadTank(transport, (AbstractGroundLoadableUnit) _currentUnit);
						return;
					}
				}
			}

			_logger.systemSubActionDetail("Drop location into an invalid cell : No move has been launched");
			displayInvalidDestinationMessage(_currentUnit.getUnitType());
			_battleBoard.setCurrentCell(startLocation);
			return;
		}

	}

	public void loadGame(final FirstStrikeGame p_game)
	{
		_currentGame = p_game;
	}

	public final void closeGame()
	{
		_currentGame = null;
	}

	public void setCurrentUnit(final AbstractUnit p_unit)
	{
		_currentUnit = p_unit;
	}

	private void displayInvalidDestinationMessage(final UnitTypeCst p_unitType)
	{
		String message = null;
		if (UnitTypeCst.TANK.equals(p_unitType))
		{
			message = bundle.getString("fs.frame.error.tank_invalid_destination");

		} else if (UnitTypeCst.FIGHTER.equals(p_unitType))
		{
			message = bundle.getString("fs.frame.error.fighter_invalid_destination");

		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_unitType) || UnitTypeCst.DESTROYER.equals(p_unitType) || UnitTypeCst.BATTLESHIP.equals(p_unitType))
		{
			message = bundle.getString("fs.frame.error.boat_invalid_destination");
		} else
		{
			throw new IllegalArgumentException("Unsupported UnitType : " + p_unitType);
		}

		JOptionPane.showInternalMessageDialog(	FSFrame.getFSFrameContainer(_battleBoard).getContentPane(),
												message,
												bundle.getString("fs.error.title"),
												JOptionPane.OK_OPTION);

	}

	@Override
	public void mouseDragged(final MouseEvent p_e)
	{
		final boolean mouseLeftButton = (MouseEvent.BUTTON1_DOWN_MASK == (MouseEvent.BUTTON1_DOWN_MASK & p_e
				.getModifiersEx()));

		if (_currentUnit == null || !mouseLeftButton)
		{
			_dragInProgress = false;
		} else
		{
			final BattleBoard board = (BattleBoard) p_e.getSource();
			if (!_dragInProgress)
			{
				_dragInProgress = true;
				_logger.systemAction("Drag operation started");
			}

			final int x = p_e.getX();
			final int y = p_e.getY();
			final Point point = new Point(x, y);
			final int row = board.rowAtPoint(point);
			final int col = board.columnAtPoint(point);
			if (row == -1 || col == -1)
			{
				return;
			}
			final Coord destination = new Coord(col, row);
			final Coord startLocation = _currentUnit.getPosition();

			if (startLocation.equals(destination))
			{
				board.setCursor(getCursorByType(_currentUnit.getUnitType()));
				return;
			}
			final HumanPlayer player = (HumanPlayer) _currentGame.getCurrentPlayer();
			CoordSet coordSet = null;
			if (!_currentGame.getOptionMgr().isDebutMode())
			{

				coordSet = player.getShadowSet();
				if (coordSet.contains(destination))
				{
					board.setCursor(_cursorInvalid);
					return;
				}
			}

			coordSet = player.getEnemySet().getCoordSet();
			if (coordSet.contains(destination))
			{
				if (startLocation.calculateIntDistance(destination) != 1)
				{
					board.setCursor(_cursorInvalid);
					return;
				}

				if (_currentUnit instanceof TransportShip || _currentUnit.getMove() == 0)
				{
					board.setCursor(_cursorInvalid);
					return;
				}

				board.setCursor(_cursorAttack);
				return;
			}

			final FSMapMgr mapMgr = _currentGame.getMapMgr();
			if (mapMgr.isAccessibleCoord(_currentUnit, destination, new CoordSet(), false))
			{
				board.setCursor(getCursorByType(_currentUnit.getUnitType()));
				return;
			}

			if (_currentUnit instanceof TransportShip)
			{
				final TransportShip transport = (TransportShip) _currentUnit;
				if (transport.isContainingUnit())
				{
					final Cell destinationCell = mapMgr.getMap().getCellAt(destination);
					if (destinationCell.isGroundCell() && startLocation.calculateIntDistance(destination) == 1)
					{
						board.setCursor(_cursorLoading);
						return;
					}
				}
			}

			if (_currentUnit instanceof AbstractGroundLoadableUnit && startLocation.calculateIntDistance(destination) == 1
					&& _currentUnit.getMove() > 0)
			{
				final AbstractUnitList transportList = _currentUnit.getPlayer().getUnitList().getUnitAt(destination)
						.getUnitByType(UnitTypeCst.TRANSPORT_SHIP);
				if (transportList.size() == 1)
				{
					final TransportShip transport = (TransportShip) transportList.get(0);
					if (transport.getFreeSpace() > 0)
					{
						board.setCursor(_cursorLoading);
						return;
					}
				}
			}
			board.setCursor(_cursorInvalid);
			return;

		}
	}

	private Cursor getCursorByType(final UnitTypeCst p_unitType)
	{
		Assert.preconditionNotNull(p_unitType, "Unit Type");

		if (UnitTypeCst.TANK.equals(p_unitType))
		{
			return _cursorTank;
		} else if (UnitTypeCst.ARTILLERY.equals(p_unitType))
		{
			return _cursorArtillery;
		} else if (UnitTypeCst.FIGHTER.equals(p_unitType))
		{
			return _cursorFighter;
		} else if (UnitTypeCst.DESTROYER.equals(p_unitType))
		{
			return _cursorDestroyer;
		} else if (UnitTypeCst.BATTLESHIP.equals(p_unitType))
		{
			return _cursorBattleship;
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_unitType))
		{
			return _cursorTransport;
		} else
		{
			throw new IllegalArgumentException("Unsupported Unit Type :" + p_unitType);
		}
	}

	@Override
	public void mouseMoved(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mouseClicked(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mouseEntered(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mouseExited(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mousePressed(MouseEvent p_e)
	{
		// NOP
	}
}
