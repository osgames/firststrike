/*
 * Created on Dec 3, 2005
 */
package org.jocelyn_chaumel.firststrike.gui.game.board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class BoardModel implements TableModel
{
	private final FSMap _map;

	private final HashMap<Coord, Cell> _tableCellMap = new HashMap<Coord, Cell>();

	private final ArrayList<TableModelListener> _listenerList = new ArrayList<TableModelListener>();

	public BoardModel(final FSMap p_map)
	{
		Assert.preconditionNotNull(p_map, "Map");

		_map = p_map;

		buildModel();
	}

	@Override
	public int getColumnCount()
	{
		return _map.getWidth();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount()
	{
		return _map.getHeight();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(final int aRow, final int aCol)
	{
		Assert.precondition(aRow >= 0, "Invalid Row Index");
		Assert.precondition(aCol >= 0, "Invalid Column Index");

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnClass(int)
	 */
	@Override
	public Class<Cell> getColumnClass(final int p_colIndex)
	{
		Assert.preconditionIsPositive(p_colIndex, "invalid Column");

		// return BoardCell.class;
		return Cell.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int aRow, int aColumn)
	{
		final Coord position = new Coord(aColumn, aRow);
		return getValueAt(position);
	}

	public final Cell getValueAt(final Coord p_position)
	{
		final Cell cell = (Cell) _tableCellMap.get(p_position);
		Assert.postcondition(cell != null, "Invalid Row and/or Column");
		return cell;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#setValueAt(java.lang.Object, int, int)
	 */
	@Override
	public void setValueAt(final Object p_value, final int aRow, final int aColumn)
	{
		Assert.preconditionNotNull(p_value, "Cell");
		Assert.precondition(p_value instanceof Cell, "Invalid Table Cell Value");

		_tableCellMap.put(new Coord(aColumn, aRow), (Cell) p_value);
		fireTableEvent(aRow, aColumn);

	}

	private void fireTableEvent(final int aRow, final int aColumn)
	{
		final Iterator iter = _listenerList.iterator();
		TableModelListener currentListener;
		final TableModelEvent event = new TableModelEvent(this, aRow, aRow, aColumn, TableModelEvent.UPDATE);
		while (iter.hasNext())
		{
			currentListener = (TableModelListener) iter.next();
			currentListener.tableChanged(event);
		}
	}

	public final void setValueAt(final Cell p_cell, final Coord p_position)
	{
		_tableCellMap.put(p_position, p_cell);
		fireTableEvent(p_position.getY(), p_position.getX());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(final int aColIndex)
	{
		return "" + aColIndex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#addTableModelListener(javax.swing.event.
	 *      TableModelListener)
	 */
	@Override
	public void addTableModelListener(final TableModelListener arg0)
	{
		_listenerList.add(arg0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#removeTableModelListener(javax.swing.event
	 *      .TableModelListener)
	 */
	@Override
	public void removeTableModelListener(final TableModelListener arg0)
	{
		_listenerList.remove(arg0);

	}

	private void buildModel()
	{
		final int mapHeight = _map.getHeight();
		final int mapWidth = _map.getWidth();
		Coord currentCoord = null;
		Cell cell = null;

		for (int y = 0; y < mapHeight; y++)
		{
			for (int x = 0; x < mapWidth; x++)
			{
				currentCoord = new Coord(x, y);

				cell = _map.getCellAt(x, y);
				_tableCellMap.put(currentCoord, cell);
			}
		}
	}

	public final void setValueAtWithoutPropagation(final Cell p_cell)
	{
		final Coord cellCoord = p_cell.getCellCoord();
		final int row = cellCoord.getY();
		final int col = cellCoord.getX();
		final Iterator iter = _listenerList.iterator();
		TableModelListener currentListener;
		final TableModelEvent event = new TableModelEvent(this, row, row, col, TableModelEvent.UPDATE);
		while (iter.hasNext())
		{
			currentListener = (TableModelListener) iter.next();
			if (currentListener instanceof JTable)
			{
				currentListener.tableChanged(event);
			}
		}
	}
}