/*
 * Created on Dec 3, 2005
 */
package org.jocelyn_chaumel.firststrike.gui.game.board;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.table.TableColumn;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.AirPathFinder;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathNotFoundException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractGroundLoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.IBombardment;
import org.jocelyn_chaumel.firststrike.core.unit.ILoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.ColumnHeaderRenderer;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.contextual_menu.FSContextualMenuMgr;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSFrameEvent;
import org.jocelyn_chaumel.firststrike.gui.game.event.IFSFrameEventListener;
import org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic.UnitPickerDlg;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class BattleBoard extends JTable implements IFSFrameEventListener, MouseListener
{
	public final static short NO_ACTION_IN_PROGRESS = 0;
	public final static short ACTION_IN_PROGRESS_LOAD_UNIT = 1;
	public final static short ACTION_IN_PROGRESS_UNLOAD_UNIT = 2;
	public final static short ACTION_IN_PROGRESS_MOVE_UNIT = 3;
	public final static short ACTION_IN_PROGRESS_ATTACK_ENNEMY = 4;
	public final static short ACTION_IN_PROGRESS_BOMB_POSITION = 5;
	private final static FSLogger _logger = FSLoggerManager.getLogger(BattleBoard.class.getName());
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private HumanPlayer _player = null;
	private final FSFrame _frame;
	private final FSContextualMenuMgr _contextuelMenu;
	private final DragNDropMgr _dragNDropMgr;

	private Cell _currentCell = null;
	private AbstractUnit _currentUnit = null;
	private FirstStrikeGame _currentGame = null;
	private short _actionInProgress = NO_ACTION_IN_PROGRESS;
	private CoordSet _actionInProgressCoordSet = null;

	/**
	 * @param arg0
	 * @throws IOException
	 */
	public BattleBoard(final FSFrame p_frame)
	{
		super();

		_frame = p_frame;
		_contextuelMenu = new FSContextualMenuMgr(this);

		// Pour emp�cher le d�placement des colonnes
		tableHeader.setReorderingAllowed(false);
		setCellSelectionEnabled(false);
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		_dragNDropMgr = new DragNDropMgr(this);
		addMouseListener(_dragNDropMgr);
		addMouseMotionListener(_dragNDropMgr);

		setShowGrid(false);
		setIntercellSpacing(new Dimension(0, 0));
	}

	public HumanPlayer getPlayer()
	{
		return _player;
	}

	public FSFrame getFrame()
	{
		return _frame;
	}

	public final FirstStrikeGame getCurrentGame()
	{
		return _currentGame;
	}

	public final AbstractUnit getCurrentUnit()
	{
		return _currentUnit;
	}

	public void setMouseEnabled(final boolean p_enabled)
	{
		this.setEnabled(p_enabled);

	}

	public final void release()
	{
		removeAll();

		final MouseListener[] array = this.getMouseListeners();
		if (array.length > 0)
		{
			for (int i = 0; i < array.length; i++)
			{
				this.removeMouseListener(array[i]);
			}
		}
	}

	public final void resizingRowCol(final int p_rowColSize)
	{
		// TODO AZ Supprimer cette m�thode
		final int size = columnModel.getColumnCount();
		final ColumnHeaderRenderer colRenderer = new ColumnHeaderRenderer(this);
		TableColumn column = null;
		for (int i = 0; i < size; i++)
		{
			column = columnModel.getColumn(i);
			column.setHeaderRenderer(colRenderer);
			column.setPreferredWidth(p_rowColSize);
			column.setMinWidth(p_rowColSize);
			column.setMaxWidth(p_rowColSize);
			column.setWidth(p_rowColSize);
			column.setResizable(false);
		}
		setRowHeight(p_rowColSize);

	}

	/**
	 * Refresh only the label displayed in the battle board.
	 * 
	 * @param p_coord
	 *            Cell's coord to refresh.
	 */
	private final void refreshBoardCell(final Coord p_coord)
	{
		final int row = p_coord.getY();
		final int col = p_coord.getX();
		final Cell cell = (Cell) getValueAt(row, col);
		setValueAt(cell, row, col);
	}

	public final void refreshBoardCell(final CoordList p_coordList)
	{
		Coord currentCoord = null;
		final Iterator coordIter = p_coordList.iterator();
		while (coordIter.hasNext())
		{
			currentCoord = (Coord) coordIter.next();
			refreshBoardCell(currentCoord);
		}
	}

	public final void refreshBoardCell(final CoordSet p_coordList)
	{
		Coord currentCoord = null;
		final Iterator coordIter = p_coordList.iterator();
		while (coordIter.hasNext())
		{
			currentCoord = (Coord) coordIter.next();
			refreshBoardCell(currentCoord);
		}
	}

	public final void setValueAtWithoutPropagation(final Cell p_cell)
	{
		((BoardModel) getModel()).setValueAtWithoutPropagation(p_cell);
	}

	public final void displayContextualMenu(final int p_x, final int p_y)
	{
		_contextuelMenu.pack();
		_contextuelMenu.show(this, p_x, p_y);
	}

	@Override
	public void closeGameEvent(FSFrameEvent p_event)
	{
		removeMouseListener(this);
		_dragNDropMgr.closeGame();
	}

	@Override
	public void conquerCityEvent(final FSFrameEvent p_event)
	{
		final Coord coord = p_event.getCurrentCell().getCellCoord();
		refreshBoardCell(coord);
	}

	@Override
	public void lostCityEvent(FSFrameEvent p_event)
	{
		final Coord coord = ((City) p_event.getOtherData()).getPosition();
		refreshBoardCell(coord);
	}

	@Override
	public void lostUnitEvent(FSFrameEvent p_event)
	{
		final Coord coord = p_event.getCurrentUnit().getPosition();
		refreshBoardCell(coord);
	}

	@Override
	public void loadGameEvent(FSFrameEvent p_event)
	{
		loadGame();
	}

	private void loadGame()
	{
		_currentGame = _frame.getGame();
		_dragNDropMgr.loadGame(_currentGame);
		final FSMap map = _currentGame.getMapMgr().getMap();
		final BoardModel model = new BoardModel(map);
		super.setModel(model);
		addMouseListener(this);
		_player = (HumanPlayer) _currentGame.getCurrentPlayer();
		setDefaultRenderer(Cell.class, new BoardCellRenderer(_frame.getCursorMgr(), this));
		resizingRowCol(IconFactory.CELL_SIZE);

	}

	@Override
	public void unitCreatedEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void unitMovedEvent(final FSFrameEvent p_event)
	{
		final AbstractUnit unit = p_event.getCurrentUnit();
		if (unit != null && unit.getPlayer() instanceof HumanPlayer)
		{
			final int range = unit.getDetectionRange();
			final FSMap map = p_event.getGame().getMapMgr().getMap();
			CoordSet coordToRefresh = CoordSet.createInitiazedCoordSet(	unit.getPosition(),
																		range,
																		map.getWidth(),
																		map.getHeight());
			refreshBoardCell(coordToRefresh);
		}
	}

	@Override
	public void updateCurrentCellEvent(final FSFrameEvent p_event)
	{
		updateCell(p_event);
	}

	/**
	 * Center the specified cell in the center of the viewport.
	 * 
	 * @param p_coord
	 *            Target Cell
	 */
	public final void setCenterOfBattleBoard(final Coord p_coord)
	{
		Assert.preconditionNotNull(p_coord, "position");

		final JViewport view = (JViewport) getParent();
		final Rectangle cellRect = this.getCellRect(p_coord.getY(), p_coord.getX(), true);
		int targetX = ((int) cellRect.getCenterX()) - (view.getWidth() / 2);
		int targetY = ((int) cellRect.getCenterY()) - (view.getHeight() / 2);
		if (targetX < 0)
		{
			targetX = 0;
		}
		if (targetX + view.getWidth() > this.getWidth())
		{
			targetX = this.getWidth() - view.getWidth();
		}

		if (targetY < 0)
		{
			targetY = 0;
		}
		if (targetY + view.getHeight() > this.getHeight())
		{
			targetY = this.getHeight() - view.getHeight();
		}

		view.setViewPosition(new Point(targetX, targetY));
		this.grabFocus();
	}

	@Override
	public void updateCurrentUnitEvent(final FSFrameEvent p_event)
	{
		if (_actionInProgress != NO_ACTION_IN_PROGRESS)
		{
			_actionInProgress = NO_ACTION_IN_PROGRESS;
			this.refreshBoardCell(_actionInProgressCoordSet);
		}
		updateCell(p_event);
	}

	private void updateCell(final FSFrameEvent p_event)
	{
		// Remove the old displayed path
		if (_currentUnit != null && _currentUnit.hasPath())
		{
			hideCurrentPath();
		}

		_currentCell = p_event.getCurrentCell();
		_currentUnit = p_event.getCurrentUnit();
		_dragNDropMgr.setCurrentUnit(_currentUnit);
		final Coord coord = _currentCell.getCellCoord();
		refreshBoardCell(coord);

		if (_currentUnit != null && _currentUnit.hasPath())
		{
			// Show the unit's path
			final Path path = _currentUnit.getPath();
			final CoordList coordList = path.getCoordList();
			refreshBoardCell(coordList);
		}

		changeSelection(coord.getY(), coord.getX(), false, false);
		requestFocus();

	}

	public final void setCurrentCell(final Coord p_coord)
	{
		System.out.println("current cell:" + p_coord);
		_frame.setCurrentCell(this, p_coord);
	}

	public final void tracePathAndGo(final Coord p_destination)
	{
		if (tracePath(p_destination))
		{
			final Coord currentPosition = _currentUnit.getPosition();
			moveUnit(null);
			if (!_currentGame.isGameOver())
			{
				setCurrentCell(currentPosition);
			}
		}
	}

	public final void tracePathAndGoAll(final Coord p_destination)
	{
		if (tracePath(p_destination))
		{
			final Path pathModel = _currentUnit.getPath();
			final Coord unitPosition = _currentUnit.getPosition();
			final UnitTypeCst unitType = _currentUnit.getUnitType();
			final AbstractPlayer player = _currentUnit.getPlayer();
			AbstractUnitList unitList = player.getUnitList().getUnitAt(unitPosition);
			unitList = unitList.getUnitByType(unitType);
			unitList.remove(_currentUnit);
			Path currentPath = null;
			final FSMap map = _currentGame.getMapMgr().getMap();
			for (AbstractUnit currentUnit : unitList)
			{
				currentPath = (Path) pathModel.clone();
				currentPath.calculateTurnCost(	map,
												currentUnit.getMoveCost(),
												currentUnit.getMove(),
												currentUnit.getMovementCapacity());
				currentUnit.setPath(currentPath);
				moveUnit(currentUnit);
				if (_currentGame.isGameOver())
				{
					break;
				}
			}
			moveUnit(null);
			if (!_currentGame.isGameOver())
			{
				setCurrentCell(unitPosition);
			}
		}
	}

	public final void removePath()
	{
		hideCurrentPath();
		_currentUnit.clearPath();
	}

	public final boolean tracePath(final Coord p_destination)
	{
		Assert.preconditionNotNull(_currentUnit, "CurrentUnit");

		final UnitTypeCst unitType = _currentUnit.getUnitType();
		final Coord startCoord = _currentUnit.getPosition();
		Assert.check(!startCoord.equals(p_destination), "Same starting & destination coord");
		if (unitType.equals(UnitTypeCst.TANK)
				&& !_currentGame.isValidDestinationForGroundUnit(startCoord, p_destination))
		{
			JOptionPane.showMessageDialog(	_frame.getContentPane(),
											_bundle.getString("fs.frame.error.tank_invalid_destination"),
											_bundle.getString("fs.error.invalid_command_title"),
											JOptionPane.WARNING_MESSAGE);
			return false;
		} else if (unitType.equals(UnitTypeCst.FIGHTER) && !_currentGame.isValidDestinationForAirUnit(p_destination))
		{
			JOptionPane.showMessageDialog(	_frame.getContentPane(),
											_bundle.getString("fs.frame.error.fighter_invalid_destination"),
											_bundle.getString("fs.error.invalid_command_title"),
											JOptionPane.WARNING_MESSAGE);
			return false;
		} else if ((unitType.equals(UnitTypeCst.TRANSPORT_SHIP) || unitType.equals(UnitTypeCst.DESTROYER))
				&& !_currentGame.isValidDestinationForSeaUnit(startCoord, p_destination))
		{
			JOptionPane.showMessageDialog(	_frame.getContentPane(),
											_bundle.getString("fs.frame.error.boat_invalid_destination"),
											_bundle.getString("fs.error.invalid_command_title"),
											JOptionPane.WARNING_MESSAGE);
			return false;
		}
		if (_currentUnit.hasPath())
		{
			hideCurrentPath();
		}
		try
		{
			final Path path;
			if (UnitTypeCst.FIGHTER.equals(unitType))
			{
				final FSMapMgr mapMgr = _currentGame.getMapMgr();
				final AirPathFinder pathFinder = mapMgr.getPathMgr().getAirPathFinder();
				final HumanPlayer player = (HumanPlayer) _currentUnit.getPlayer();
				final CoordSet coordToExclude = (CoordSet) mapMgr.getCityList().getEnemyCity(player).getCoordSet()
						.clone();
				if (!ApplicationConfigurationMgr.getInstance().isDebugMode())
				{
					coordToExclude.addAll(player.getShadowSet());
				}
				path = pathFinder.findPath(	startCoord,
											p_destination,
											Integer.MAX_VALUE,
											Integer.MAX_VALUE,
											new CoordSet(),
											coordToExclude,
											false);
			} else
			{
				path = _currentGame.getPath(_currentUnit, p_destination);
			}
			_currentUnit.setPath(path);
			path.calculateTurnCost(	_currentGame.getMapMgr().getMap(),
									_currentUnit.getMoveCost(),
									_currentUnit.getMove(),
									_currentUnit.getMovementCapacity());
			final CoordList coordList = path.getCoordList();
			refreshBoardCell(coordList);
		} catch (PathNotFoundException ex)
		{
			_logger.systemError("Unable to build path", ex);
			JOptionPane.showMessageDialog(	_frame.getContentPane(),
											_bundle.getString("fs.frame.error.unaccessible_destination"),
											_bundle.getString("fs.error.invalid_command_title"),
											JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	public final void bombPosition(final Coord p_position)
	{
		Assert.preconditionNotNull(p_position);
		Assert.check(_currentUnit != null, "No current unit");

		if (_currentUnit instanceof Battleship)
		{
			final Battleship battleship = (Battleship) _currentUnit;
			battleship.bombPosition(p_position);
		} else
		{
			throw new UnsupportedOperationException("This type is not supported: " + _currentUnit.getClass().getName());
		}
		setCurrentCell(_currentUnit.getPosition());
	}

	// TODO AZ supprimer le param�tre car semble ne pas �tre util
	public final void moveUnit(final AbstractUnit p_unit)
	{
		Assert.preconditionNotNull(_currentUnit, "Current Unit");
		Assert.precondition(_currentUnit.hasPath(), "No Path assigned to current unit");
		_logger.playerSubAction("Move current unit : {0}", _currentUnit);

		final AbstractUnit unit;
		if (p_unit == null)
		{
			unit = _currentUnit;
			hideCurrentPath();
		} else
		{
			unit = p_unit;
		}

		final HumanPlayer human = (HumanPlayer) _currentGame.getCurrentPlayer();
		final AbstractUnitList enemyList = human.getEnemySet();
		final int enemyCount = enemyList.size();
		int detectionRange = 0;
		final int unitRange = unit.getDetectionRange();
		final FSMap map = _currentGame.getMapMgr().getMap();
		final CityList citySet;

		if (unit instanceof Tank)
		{
			citySet = map.getCityList().getCityToConquer(human);
		} else
		{
			citySet = new CityList();
		}

		final int mapHeight = map.getHeight();
		final int mapWidth = map.getWidth();
		Coord nextCoord = null;
		final Path path = unit.getPath();
		
		MoveStatusCst moveStatus;
		while (path.hasNextStep() && unit.getMove() > 0)
		{
			nextCoord = path.getNextStepWithoutConsume();

			// Move
			moveStatus = _currentGame.performMoveUnitToNextCoord(unit);

				
			if (MoveStatusCst.UNIT_DEFEATED.equals(moveStatus))
			{
				// TODO AZ Add customized popup with a fighter crashed on the ground
				JOptionPane.showMessageDialog(	_frame.getContentPane(),
												_bundle.getString("fs.frame.fighter.no_more_fuel.text"),
												_bundle.getString("fs.frame.fighter.no_more_fuel.title"),
												JOptionPane.INFORMATION_MESSAGE);
				break;
			}
					
			if (!(MoveStatusCst.MOVE_SUCCESFULL.equals(moveStatus)))
			{
				break;
			}

			// Refresh BattleBoard range zone only
			if (citySet.hasCityAtPosition(nextCoord))
			{
				final City city = citySet.getCityAtPosition(nextCoord);
				detectionRange = city.getDetectionRange();
			} else
			{
				detectionRange = unitRange;
			}
			final CoordSet refreshCoordList = CoordSet.createInitiazedCoordSet(	unit.getPosition(),
																				detectionRange,
																				mapWidth,
																				mapHeight);
			refreshBoardCell(refreshCoordList);
			// Check if an enemy has been detected during the move
			if (enemyList.size() != enemyCount)
			{
				_logger.playerSubActionDetail("Enemy detected");
				break;
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent p_mouseEvent)
	{
		if (p_mouseEvent.getButton() == MouseEvent.BUTTON1)
		{
			select(p_mouseEvent);
		} else if (p_mouseEvent.getButton() == MouseEvent.BUTTON3)
		{
			_logger.systemSubActionDetail(" select x,y :" + p_mouseEvent.getX() + ", " + p_mouseEvent.getY());
			displayContextualMenu(p_mouseEvent.getX(), p_mouseEvent.getY());
		}
	}

	@Override
	public void mouseEntered(MouseEvent p_e)
	{
	}

	@Override
	public void mouseExited(MouseEvent p_e)
	{
	}

	@Override
	public void mousePressed(MouseEvent p_mouseEvent)
	{
		if (p_mouseEvent.getButton() == MouseEvent.BUTTON1)
		{
			select(p_mouseEvent);
		}
	}

	@Override
	public void mouseReleased(MouseEvent p_e)
	{
		// NOP
	}

	private final void select(MouseEvent p_mouseEvent)
	{
		final int row = getSelectedRow();
		final int column = getSelectedColumn();
		final Coord currentCoord = _currentCell.getCellCoord();
		_currentUnit = _frame.getCurrentUnit();
		final Coord clickedCoord = new Coord(column, row);
		if (_actionInProgress != NO_ACTION_IN_PROGRESS)
		{
			short actionInProgress = _actionInProgress;
			_actionInProgress = NO_ACTION_IN_PROGRESS;
			refreshBoardCell(_actionInProgressCoordSet);
			if (_actionInProgressCoordSet.contains(clickedCoord))
			{
				final AbstractPlayer player = _frame.getCurrentUnit().getPlayer();

				if (actionInProgress == ACTION_IN_PROGRESS_LOAD_UNIT)
				{
					final AbstractGroundLoadableUnit unitToLoad = (AbstractGroundLoadableUnit) _currentUnit;
					final AbstractUnitList transportSet = player.getUnitList().getUnitAt(clickedCoord)
							.getNonFullTransportShip();
					TransportShip selectedTransport = null;
					if (transportSet.size() == 1)
					{
						selectedTransport = (TransportShip) transportSet.get(0);
					} else
					{
						UnitPickerDlg dlg = new UnitPickerDlg(_frame, transportSet,
								_bundle.getString("fs.contextual_menu.select_one_transport"));
						dlg.pack();
						dlg.setVisible(true);

						selectedTransport = (TransportShip) dlg.getSelectedUnit();
					}
					if (selectedTransport != null)
					{
						unitToLoad.getPlayer().loadUnit(unitToLoad, selectedTransport);
						_frame.setCurrentCell(this, clickedCoord);
					}
				} else if (actionInProgress == ACTION_IN_PROGRESS_MOVE_UNIT)
				{
					tracePathAndGo(clickedCoord);
				} else if (actionInProgress == ACTION_IN_PROGRESS_BOMB_POSITION)
				{
					IBombardment bomber = (IBombardment) _currentUnit;
					bomber.bombPosition(clickedCoord);
				} else if (actionInProgress == ACTION_IN_PROGRESS_ATTACK_ENNEMY)
				{
					_frame.attackEnemy(clickedCoord);
				} else if (actionInProgress == ACTION_IN_PROGRESS_UNLOAD_UNIT)
				{
					final ILoadableUnit currentTank = _frame.getCurrentContainedUnit();
					_currentGame.performUnloadUnit(currentTank, clickedCoord);
					_frame.setCurrentContainedUnit(null);

				} else
				{
					throw new IllegalStateException("Unsupported 'Action In Progres' Type");
				}
			}

			_actionInProgressCoordSet.clear();
			_actionInProgressCoordSet = null;

		}
		if (currentCoord.getX() == column && currentCoord.getY() == row)
		{
			// Already selected
			if (p_mouseEvent.getClickCount() >= 2)
			{
				final int modifier = p_mouseEvent.getModifiersEx();
				if (modifier == InputEvent.BUTTON1_DOWN_MASK)
				{
					doubleClick(currentCoord);
				}
			}
		} else
		{
			_logger.systemSubActionDetail("mousePressed : {0}", "" + column + ", " + row);
			_frame.setCurrentCell(this, new Coord(column, row));
		}
	}

	public void doubleClick(final Coord p_currentCoord)
	{
		if (_currentCell.isCity())
		{
			final City city = _currentCell.getCity();
			if (city.getPlayer() instanceof HumanPlayer)
			{
				_frame.openCity(_currentCell.getCellCoord());
				return;
			}
		}
		if (_currentUnit != null)
		{

			if (_currentUnit.hasPath())
			{
				if (_currentUnit.getMove() > 0) 
				{
					moveUnit(null);
				}
				_frame.setCurrentCell(this, p_currentCoord);
				return;
			} else
			{
				final AbstractPlayer currentPlayer = _currentUnit.getPlayer();
				final City nearestCity;
				if (UnitTypeCst.DESTROYER.equals(_currentUnit.getUnitType())
						|| UnitTypeCst.BATTLESHIP.equals(_currentUnit.getUnitType())
						|| UnitTypeCst.TRANSPORT_SHIP.equals(_currentUnit.getUnitType()))
				{
					nearestCity = currentPlayer.getCitySet().getPortList().getDirectNearestCityFrom(p_currentCoord);
				} else if (UnitTypeCst.TANK.equals(_currentUnit.getUnitType())
						|| UnitTypeCst.ARTILLERY.equals(_currentUnit.getUnitType()))
				{
					final Area area = _player.getMapMgr().getAreaAt(p_currentCoord, AreaTypeCst.GROUND_AREA);
					nearestCity = currentPlayer.getCitySet().getCityByArea(area).getDirectNearestCityFrom(p_currentCoord);
				} else
				{
					nearestCity = currentPlayer.getCitySet().getDirectNearestCityFrom(p_currentCoord);
				}
				if (nearestCity == null)
				{
					JOptionPane.showMessageDialog(	_frame,
													"No owned city found.  The command has been ignored",
													"title",
													JOptionPane.WARNING_MESSAGE);
					return;
				}
				final Coord nearestCityCoord = nearestCity.getPosition();
				if (!_currentGame.isValidDestinationForUnit(_currentUnit, nearestCityCoord))
				{
					JOptionPane.showMessageDialog(	_frame,
													"No owned city found.  The command has been ignored",
													"title",
													JOptionPane.WARNING_MESSAGE);
					return;
				}
				final Path path;
				try
				{
					path = _currentGame.getPath(_currentUnit, nearestCityCoord);
					path.calculateTurnCost(	_currentGame.getMapMgr().getMap(),
											_currentUnit.getMoveCost(),
											_currentUnit.getMove(),
											_currentUnit.getMovementCapacity());
					_currentUnit.setPath(path);
				} catch (PathNotFoundException ex)
				{
					JOptionPane.showMessageDialog(	_frame,
													"No accessible city found.  The command has been ignored",
													"title",
													JOptionPane.WARNING_MESSAGE);
					return;
				}
				if (_currentUnit.getMove() > 0) {
					moveUnit(_currentUnit);
				} else {
					refreshBoardCell(path.getCoordList());
				}
				_frame.setCurrentCell(this, p_currentCoord);
			}
		}
	}

	public final void attackEnnemy(final Coord p_cellCoord)
	{
		_frame.attackEnemy(p_cellCoord);
		this.refreshBoardCell(p_cellCoord);
	}
	
	/**
	 * D�charge un tank depuis un transport de troupe vers la terre ferme.
	 * 
	 * @param p_destination
	 * @param p_unit
	 */
	public final void unloadTank(final Coord p_destination, final AbstractUnit p_unit)
	{
		_currentGame.performUnloadUnit((AbstractGroundLoadableUnit) p_unit, p_destination);
	}

	/**
	 * D�charge un tank au hasard de la liste disponible dans le transport de troupe.
	 * 
	 * @param p_transport
	 * @param p_destination
	 */
	public final void unloadAnUnit(final TransportShip p_transport, final Coord p_destination)
	{
		final ArrayList<ILoadableUnit> tankList = p_transport.getContainedUnitList();
		final Cell cell = _currentGame.getMapMgr().getMap().getCellAt(p_destination);
		int currentMove = 0;
		for (ILoadableUnit currentUnit : tankList)
		{
			currentMove = ((AbstractUnit)currentUnit).getMove();
			if (currentMove == 0)
			{
				continue;
			}

			final int moveCost = ((Tank) currentUnit).getMoveCost(cell);
			if (moveCost > currentMove)
			{
				continue;
			}
			unloadTank(p_destination, (AbstractGroundLoadableUnit)currentUnit);
			return;
		}

		JOptionPane.showMessageDialog(	_frame,
										_bundle.getString("fs.board.error.invalid_unload_command"),
										_bundle.getString("fs.error.invalid_command_title"),
										JOptionPane.WARNING_MESSAGE);
	}

	public final void loadTank(final TransportShip p_transport, final ILoadableUnit p_tank)
	{
		final int availableSpace = p_transport.getFreeSpace();
		final int spaceNeeded = p_tank.getSpaceCost();
		if (availableSpace < spaceNeeded)
		{
			JOptionPane.showMessageDialog(	this,
											_bundle.getString("fs.frame.error.transport_overloaded"),
											_bundle.getString("fs.error.invalid_command_title"),
											JOptionPane.INFORMATION_MESSAGE);
			_logger.systemSubActionDetail("Transport too much loaded");
			return;
		}
		final Coord tankCoord = ((AbstractGroundLoadableUnit) p_tank).getPosition();
		final Coord transportCoord = p_transport.getPosition();
		Assert.check(tankCoord.calculateIntDistance(transportCoord) <= 1, "Too long distance between transport and tank");
		_currentGame.performLoadUnit((AbstractGroundLoadableUnit)p_tank, p_transport);
		setCurrentCell(tankCoord);
	}

	public void hideCurrentPath()
	{
		final Path oldPath = _currentUnit.getPath();
		final CoordList coordList = oldPath.getCoordList();
		refreshBoardCell(coordList);
	}

	public void activateMoveUnitAction(final AbstractUnit p_unit)
	{
		if (_actionInProgress != NO_ACTION_IN_PROGRESS)
		{
			final short oldAction = _actionInProgress;
			_actionInProgress = NO_ACTION_IN_PROGRESS;
			this.refreshBoardCell(_actionInProgressCoordSet);
			if (oldAction == ACTION_IN_PROGRESS_MOVE_UNIT)
			{
				return;
			}
		}

		_actionInProgressCoordSet = p_unit.getPlayer().getMapMgr().getAccessibleCells(p_unit);
		if (_actionInProgressCoordSet.isEmpty())
		{
			JOptionPane.showMessageDialog(	this,
											_bundle.getString("fs.frame.error.no_cell_accessible"),
											_bundle.getString("fs.error.title"),
											JOptionPane.ERROR_MESSAGE);
			return;
		}

		_actionInProgress = ACTION_IN_PROGRESS_MOVE_UNIT;
		this.refreshBoardCell(_actionInProgressCoordSet);
	}

	public void activateUnloadUnitAction(final ILoadableUnit p_unit, final CoordSet p_unloadCoordSet)
	{
		if (_actionInProgress != NO_ACTION_IN_PROGRESS)
		{
			final short oldAction = _actionInProgress;
			_actionInProgress = NO_ACTION_IN_PROGRESS;
			this.refreshBoardCell(_actionInProgressCoordSet);
			if (oldAction == ACTION_IN_PROGRESS_UNLOAD_UNIT)
			{
				return;
			}
		}
		_frame.setCurrentContainedUnit(p_unit);

		_actionInProgressCoordSet = p_unloadCoordSet;

		if (_actionInProgressCoordSet.isEmpty())
		{
			JOptionPane.showMessageDialog(	this,
											_bundle.getString("fs.frame.error.no_cell_accessible"),
											_bundle.getString("fs.error.title"),
											JOptionPane.ERROR_MESSAGE);
			return;
		}
		_actionInProgress = ACTION_IN_PROGRESS_UNLOAD_UNIT;
		this.refreshBoardCell(_actionInProgressCoordSet);

	}

	public void activateLoadTankAction(final AbstractGroundLoadableUnit p_loadableUnit)
	{
		if (_actionInProgress != NO_ACTION_IN_PROGRESS)
		{
			final short oldAction = _actionInProgress;
			_actionInProgress = NO_ACTION_IN_PROGRESS;
			this.refreshBoardCell(_actionInProgressCoordSet);
			if (oldAction == ACTION_IN_PROGRESS_LOAD_UNIT)
			{
				return;
			}
		}

		final HumanPlayer player = (HumanPlayer) p_loadableUnit.getPlayer();
		final AbstractUnitList transportList = player.getUnitList().getUnitInRange(p_loadableUnit.getPosition(), 1)
				.getUnitByType(UnitTypeCst.TRANSPORT_SHIP);
		final AbstractUnitList transportListWithSpace = new AbstractUnitList();
		for (AbstractUnit transport : transportList)
		{
			if (((TransportShip) transport).getFreeSpace() >= p_loadableUnit.getSpaceCost())
			{
				transportListWithSpace.add(transport);
			}
		}

		if (transportListWithSpace.isEmpty())
		{
			JOptionPane.showMessageDialog(	this,
											_bundle.getString("fs.frame.error.no_transport_available"),
											_bundle.getString("fs.error.title"),
											JOptionPane.ERROR_MESSAGE);
			return;
		}

		_actionInProgress = ACTION_IN_PROGRESS_LOAD_UNIT;
		_actionInProgressCoordSet = transportListWithSpace.getCoordSet();
		this.refreshBoardCell(_actionInProgressCoordSet);
	}

	public void activateAttackAction(final AbstractUnit p_unit)
	{
		if (_actionInProgress != NO_ACTION_IN_PROGRESS)
		{
			final short oldAction = _actionInProgress;
			_actionInProgress = NO_ACTION_IN_PROGRESS;
			this.refreshBoardCell(_actionInProgressCoordSet);
			if (oldAction == ACTION_IN_PROGRESS_ATTACK_ENNEMY)
			{
				return;
			}
		}

		_actionInProgressCoordSet = p_unit.getPlayer().getEnemySet().getUnitInRange(p_unit.getPosition(), 1)
				.getCoordSet();
		if (_actionInProgressCoordSet.size() == 0)
		{
			JOptionPane.showMessageDialog(	this,
											_bundle.getString("fs.frame.error.no_ennemy_detected"),
											_bundle.getString("fs.error.title"),
											JOptionPane.ERROR_MESSAGE);
			_actionInProgressCoordSet.clear();
			_actionInProgressCoordSet = null;
			return;
		}

		_actionInProgress = ACTION_IN_PROGRESS_ATTACK_ENNEMY;
		this.refreshBoardCell(_actionInProgressCoordSet);
	}

	public void activateBombAction(final AbstractUnit p_unit)
	{
		if (_actionInProgress != NO_ACTION_IN_PROGRESS)
		{
			final short oldAction = _actionInProgress;
			_actionInProgress = NO_ACTION_IN_PROGRESS;
			this.refreshBoardCell(_actionInProgressCoordSet);
			if (oldAction == ACTION_IN_PROGRESS_BOMB_POSITION)
			{
				return;
			}
		}

		IBombardment bombUnit = (IBombardment) p_unit;
		final FSMap map = p_unit.getPlayer().getMapMgr().getMap();
		_actionInProgressCoordSet = CoordSet.createInitiazedCoordSet(	p_unit.getPosition(),
																		bombUnit.getBombardmentRange(),
																		map.getWidth(),
																		map.getHeight());
		_actionInProgressCoordSet.remove(p_unit.getPosition());

		_actionInProgress = ACTION_IN_PROGRESS_BOMB_POSITION;
		this.refreshBoardCell(_actionInProgressCoordSet);
	}

	public final boolean isActionInProgress()
	{
		return _actionInProgress != 0;
	}

	public final short getActionInProgress()
	{
		return _actionInProgress;
	}

	public final CoordSet getActionInProgressCoordSet()
	{
		Assert.check(_actionInProgress > 0, "Invalid state");
		return _actionInProgressCoordSet;
	}
}