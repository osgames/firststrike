package org.jocelyn_chaumel.firststrike.gui.game.board;

public class MoveTypeMsg
{
	public static short SET_INITIAL_POSITION = 1;
	public static short SET_MOVE_ONE_STEP = 2;
	
	private final short _moveType;
	private final Object _object;
	
	public MoveTypeMsg(final short p_moveType, final Object p_object)
	{
		_moveType = p_moveType;
		_object = p_object;
	}

	public short getMoveType()
	{
		return _moveType;
	}

	public Object getObject()
	{
		return _object;
	}
	
	
}
