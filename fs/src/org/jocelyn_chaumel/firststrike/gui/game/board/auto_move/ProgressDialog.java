package org.jocelyn_chaumel.firststrike.gui.game.board.auto_move;

import java.util.ResourceBundle;

import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;

public class ProgressDialog extends FSDialog 
{
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");
	
	private ProgressPanel _progressPnl;

	public ProgressDialog(final FSFrame p_frame)
	{
		super(p_frame, ProgressDialog.bundle.getString("fs.progress.title"), true);
		_progressPnl = new ProgressPanel(p_frame.getGame());
		getContentPane().add(_progressPnl);

	}
	
	@Override
	public void setVisible(boolean p_visible)
	{
		super.setVisible(p_visible);
	}
	
	public final boolean isFatalErrorDetected()
	{
		return _progressPnl.isFatalErrorDetected();
	}

}
