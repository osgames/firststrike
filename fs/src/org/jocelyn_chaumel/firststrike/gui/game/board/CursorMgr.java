package org.jocelyn_chaumel.firststrike.gui.game.board;

import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSFrameEvent;
import org.jocelyn_chaumel.firststrike.gui.game.event.IFSFrameEventListener;

public class CursorMgr extends Thread implements IFSFrameEventListener
{
	private final BattleBoard _battleBoard;
	private boolean _status = false;
	private final int CURSOR_DELAY = 500;
	private final int QUICK_CURSOR_DELAY = CURSOR_DELAY / 5;
	private Cell _currentCell = null;
	private boolean _pause = false;

	public CursorMgr(final BattleBoard p_battleBoard)
	{
		_battleBoard = p_battleBoard;
		start();
	}

	@Override
	public void run()
	{
		try
		{
			while (true)
			{
				if (!_pause)
				{
					updateCursorStatus();
					displayCursor();
					Thread.sleep(CURSOR_DELAY);
				} else
				{
					Thread.sleep(QUICK_CURSOR_DELAY);
				}
			}
		} catch (InterruptedException ex)
		{
			ex.printStackTrace();
		}
	}

	private final void displayCursor()
	{
		if (_currentCell != null)
		{
			_battleBoard.setValueAtWithoutPropagation(_currentCell);

		}
	}

	public synchronized void pause()
	{
		_pause = true;
	}

	public synchronized void restart()
	{
		_pause = false;
	}

	private synchronized void updateCursorStatus()
	{
		_status = !_status;
	}

	public synchronized boolean getCursorStatus()
	{
		return _status;
	}

	@Override
	public void closeGameEvent(FSFrameEvent p_event)
	{
		_currentCell = null;
	}

	@Override
	public void conquerCityEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void loadGameEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostCityEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostUnitEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void unitCreatedEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void updateCurrentCellEvent(FSFrameEvent p_event)
	{
		_currentCell = p_event.getCurrentCell();
	}

	@Override
	public void updateCurrentUnitEvent(FSFrameEvent p_event)
	{
		_currentCell = p_event.getCurrentCell();
	}

	@Override
	public void unitMovedEvent(FSFrameEvent p_event)
	{
		// NOP
	}
}
