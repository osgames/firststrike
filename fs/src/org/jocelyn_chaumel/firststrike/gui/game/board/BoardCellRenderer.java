/*
 * Created on Dec 4, 2005
 */
package org.jocelyn_chaumel.firststrike.gui.game.board;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public class BoardCellRenderer extends BoardLabel implements TableCellRenderer
{
	private static ApplicationConfigurationMgr _conf = ApplicationConfigurationMgr.getInstance();

	private boolean isDebugModeEnabled = _conf.isDebugMode();

	public BoardCellRenderer(final CursorMgr p_cursorMgr, final BattleBoard p_battleBoard)
	{
		super(p_cursorMgr, p_battleBoard);
	}

	@Override
	public final Component getTableCellRendererComponent(	final JTable p_fsTable,
															final Object p_cell,
															final boolean p_isSelectedFlag,
															final boolean p_hasFocusFlag,
															final int y,
															final int x)
	{
		final Cell cell = (Cell) p_cell;
		final BattleBoard table = (BattleBoard) p_fsTable;
		final FSFrame frame = table.getFrame();
		final HumanPlayer player = table.getPlayer();
		final AbstractUnit currentUnit = frame.getCurrentUnit();

		int fogIdx;
		final Coord currentCoord = cell.getCellCoord();
		/*
		if (_conf.isDebugMode())
		{
			fogIdx = IconFactory.NO_FOG;
		} else */
			
		if (!player.getNoFogSet().contains(currentCoord))
		{
			fogIdx = IconFactory.FOG;
		} else
		{
			fogIdx = IconFactory.NO_FOG;
		}

		final ImageIcon cellIcon = IconFactory.getInstance()
				.getContextualCellIconByPlayer(	cell,
												player,
												fogIdx,
												IconFactory.SIZE_SIMPLE_SIZE,
												cell.getIndex(),
												isDebugModeEnabled);

		super.update(cellIcon, cell, p_hasFocusFlag, currentUnit, player);
		return this;

	}

}