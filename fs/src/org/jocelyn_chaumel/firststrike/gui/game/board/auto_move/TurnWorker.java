package org.jocelyn_chaumel.firststrike.gui.game.board.auto_move;

import java.util.List;

import javax.swing.SwingWorker;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.core.IFirstStrikeEvent;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TurnWorker extends SwingWorker<HumanPlayer, TurnLog>
{
	private final FirstStrikeGame _game;
	private final IFirstStrikeEvent _frameListener;
	private final ITurnWorkerMessageInterpretor _panel;
	private int _idMsg = 0;
	private boolean _isFatalErrorDetected = false;

	private final static FSLogger _logger = FSLoggerManager.getLogger(TurnWorker.class);

	public TurnWorker(final FirstStrikeGame p_game, final ITurnWorkerMessageInterpretor p_panel)
	{
		_game = p_game;
		_frameListener = p_game.getFrameListener();
		_panel = p_panel;
	}

	@Override
	protected HumanPlayer doInBackground() throws Exception
	{
		Thread.sleep(250);
		Thread.yield();
		IFirstStrikeEvent fsEvent = new IFirstStrikeEvent()
		{
			@Override
			public void eventPlayerStartTurn(final AbstractPlayer p_player)
			{
				if (p_player instanceof ComputerPlayer)
				{
					publish(new TurnLog().setPlayerStarted(" Computer Player (" + p_player.getId() + ")", p_player));
					Thread.yield();
				}
			};

			@Override
			public void eventPlayerEndTurn(final AbstractPlayer p_player)
			{
			};

			@Override
			public void eventPlayerKilled(final AbstractPlayer p_player)
			{
				publish(new TurnLog().setPlayerKilled("" + p_player.getPlayerId()));
				Thread.yield();
			};

			@Override
			public void eventUnitCreated(final AbstractUnit p_unit)
			{
				if (p_unit.getPlayer() instanceof ComputerPlayer)
				{
					if (ApplicationConfigurationMgr.getInstance().isDebugMode() == true)
					{
						publish(new TurnLog().setUnitCreated(p_unit));
						
					}
				} else {
					publish(new TurnLog().setUnitCreated(p_unit));
				}
				Thread.yield();
			};

			@Override
			public void eventUnitMoved(final AbstractUnit p_unit, final Coord p_newPosition, final Coord p_oldPosition)
			{
			};

			@Override
			public void eventUnitLoaded(final AbstractUnit p_loadedUnit, final AbstractUnit p_unitContainer)
			{
			};

			@Override
			public void eventUnitUnloaded(	final AbstractUnit p_unloadedUnit,
											final AbstractUnit p_unitContainer,
											final Coord p_destination)
			{
			};

			@Override
			public void eventUnitKilled(final AbstractUnit p_unit)
			{
				final AbstractPlayer player = p_unit.getPlayer();
				if (player instanceof ComputerPlayer)
				{
					publish(new TurnLog().setEnnemyUnitKilled(p_unit.getPosition(), "" + p_unit.getId(), p_unit.getUnitType()));
				} else {
					publish(new TurnLog().setUnitLost(p_unit.getPosition(), "" + p_unit.getId(), p_unit.getUnitType()));
				}
				Thread.yield();
			};

			@Override
			public void eventUnitFinished(final AbstractUnit p_unit)
			{
				final AbstractPlayer player = p_unit.getPlayer();
				publish(new TurnLog().setUnitFinished(player));
				Thread.yield();
			};

			@Override
			public void eventCityConquered(	final City p_city,
											final AbstractPlayer p_newPlayer,
											final AbstractPlayer p_oldPlayer)
			{
				if (p_oldPlayer instanceof HumanPlayer)
				{
					publish(new TurnLog().setCityLost(p_city.getPosition(), p_city.getCell().getCellName()));
					Thread.yield();
				}
			}
		};

		HumanPlayer human = null;
		try
		{
			_game.setFrameListener(fsEvent);
			human = (HumanPlayer) _game.performNextTurn();
			publish(new TurnLog().setEndOfProcess());
		} catch (Exception ex)
		{
			_logger.systemError("Unexpected error occured during a computer's turn.", ex);
			_isFatalErrorDetected = true;
			publish(new TurnLog().setEndOfProcessWithError());
		}
		_game.setFrameListener(_frameListener);
		Thread.yield();
		return human;
	}
	
	public final boolean isFatalErrorDetected()
	{
		return _isFatalErrorDetected;
	}

	@Override
	protected void process(final List<TurnLog> p_messageList)
	{
		for (TurnLog current : p_messageList)
		{
			current.setIdMsg(++_idMsg);
			_panel.setMessage(current);
		}
	}

}
