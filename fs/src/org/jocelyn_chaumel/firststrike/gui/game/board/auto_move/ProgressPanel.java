/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ProgressPanel.java
 *
 * Created on 26 oct. 2009, 20:44:51
 */

package org.jocelyn_chaumel.firststrike.gui.game.board.auto_move;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;

/**
 * 
 * @author Client
 */
public class ProgressPanel extends javax.swing.JPanel implements ITurnWorkerMessageInterpretor
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(ProgressPanel.class);
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private JProgressBar _unitProgressBar;
	private JLabel _progressionLbl;
	private JProgressBar _playerProgressBar;
	private JList _messageList;
	private DefaultListModel<String> _listModel = new DefaultListModel<String>();
	public JButton _okBtn;
	private TurnWorker _turnWorker;
	private JLabel _lblNewLabel;
	private JLabel _lblNewLabel_1;
	private JScrollPane _scrollPane;
	private boolean _isFatalErrorDetected = false;

	/** Creates new form ProgressPanel */
	public ProgressPanel(final FirstStrikeGame p_game)
	{
		initComponents();
		_playerProgressBar.setModel(new DefaultBoundedRangeModel(0, 0, 0, 100));
		_unitProgressBar.setStringPainted(true);
		_turnWorker = new TurnWorker(p_game, this);
		setLayout(new MigLayout("", "[430px,grow]", "[14px][17px][14px][17px][14px][][23px]"));

		_scrollPane = new JScrollPane();
		add(_scrollPane, "cell 0 5,grow");

		_messageList = new JList();
		_scrollPane.setViewportView(_messageList);
		_messageList.setBackground(new Color(245, 245, 245));
		_messageList.setModel(_listModel);
		add(_okBtn, "cell 0 6,alignx center,aligny top");
		add(_playerProgressBar, "cell 0 1,growx,aligny top");
		add(_progressionLbl, "cell 0 2,growx,aligny top");
		add(_unitProgressBar, "cell 0 3,growx,aligny top");
		add(_lblNewLabel, "cell 0 4,growx,aligny top");
		add(_lblNewLabel_1, "cell 0 0,growx,aligny top");
		_turnWorker.execute();
	}

	@SuppressWarnings("unchecked")
	private void initComponents()
	{

		_unitProgressBar = new javax.swing.JProgressBar();
		_unitProgressBar.setBackground(new Color(255, 255, 255));
		_progressionLbl = new javax.swing.JLabel();

		_unitProgressBar.setStringPainted(true);

		_progressionLbl.setText(ResourceBundle.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle")
				.getString("fs.progress.unit"));

		_playerProgressBar = new JProgressBar();
		_playerProgressBar.setStringPainted(true);

		_lblNewLabel_1 = new JLabel(ResourceBundle
				.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle").getString("fs.progress.player"));

		_lblNewLabel = new JLabel(ResourceBundle.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle")
				.getString("fs.progress.messages"));

		_okBtn = new JButton();
		_okBtn.setAction(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_turnWorker = null;

				Component currentComponent = (Component) p_e.getSource();
				while (!(currentComponent instanceof FSDialog))
				{
					currentComponent = currentComponent.getParent();
				}

				((FSDialog) currentComponent).setVisible(false);
			}
		});
		_okBtn.setText(ResourceBundle.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle")
				.getString("fs.common.ok_btn"));
		_okBtn.setEnabled(false);
	}

	@Override
	public void setMessage(final TurnLog p_msg)
	{
		try
		{
			if (p_msg.getMsgType() == TurnLog.EL_TYPE_END_OF_PROCESS)
			{
				_unitProgressBar.setMaximum(1);
				_unitProgressBar.setValue(1);
				_okBtn.setEnabled(true);
				return;
			} else if (p_msg.getMsgType() == TurnLog.EL_TYPE_END_OF_PROCESS_WITH_ERROR)
			{
				_listModel.addElement(_bundle.getString("fs.error.unexpected_fatal_error"));
				_unitProgressBar.setMaximum(1);
				_unitProgressBar.setValue(1);
				_okBtn.setEnabled(true);
				_isFatalErrorDetected = true;
				return;
			}
			if (p_msg.getMsgType() == TurnLog.EL_TYPE_UNIT_FINISH)
			{
				final AbstractPlayer player = p_msg.getPlayer();
				final int nbTotalUnit = player.getUnitList().size();
				final int nbUnitFinished = player.getUnitList().getUnitMarkedAsFinished().size();
				_unitProgressBar.setMaximum(nbTotalUnit);
				_unitProgressBar.setValue(nbUnitFinished);
				return;
			} else if (p_msg.getMsgType() == TurnLog.EL_TYPE_UNIT_LOST)
			{
				String unitType = _bundle.getString("fs.common.unit_type." + p_msg.getUnitType().getLabel());
				String msg = _bundle.getString("fs.progress_panel.killedat");
				_listModel.addElement(unitType + " " + msg + " " + p_msg.getCoord().toStringLight());
				return;
			} else if (p_msg.getMsgType() == TurnLog.EL_TYPE_UNIT_CREATED)
			{
				String unitType = _bundle.getString("fs.common.unit_type." + p_msg.getUnitType().getLabel());
				String msg = _bundle.getString("fs.progress_panel.createdat");
				_listModel.addElement(unitType + " " + msg + " " + p_msg.getCoord().toStringLight());
				return;
			} else if (p_msg.getMsgType() == TurnLog.EL_TYPE_CITY_LOST)
			{
				_listModel.addElement("City Lost at " + p_msg.getCoord().toStringLight());
				return;
			} else if (p_msg.getMsgType() == TurnLog.EL_TYPE_PLAYER_START_TURN)
			{
				_playerProgressBar.setString(p_msg.getMessage());
				return;
			} else if (p_msg.getMsgType() == TurnLog.EL_TYPE_ENNEMY_UNIT_KILLED){
				_listModel.addElement("Enemy Unit (" + p_msg.getUnitType() + ") Killed at " + p_msg.getCoord().toStringLight());
				return;
			}
			_logger.systemWarning("Invalid message detected (type unknow) : " + p_msg);
		} catch (Exception ex)
		{
			_logger.systemWarning("Invalid message detected", ex);
		}
	}

	public final boolean isFatalErrorDetected()
	{
		return _isFatalErrorDetected;
	}

}
