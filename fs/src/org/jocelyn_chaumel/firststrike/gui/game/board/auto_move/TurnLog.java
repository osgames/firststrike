package org.jocelyn_chaumel.firststrike.gui.game.board.auto_move;

import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class TurnLog
{
	private String _message;
	private Coord _coord;
	private UnitTypeCst _unitType;
	private AbstractPlayer _player;

	private short _msgType = 0;
	private int _idMsg = 0;

	public int getIdMsg()
	{
		return _idMsg;
	}

	public void setIdMsg(int p_idMsg)
	{
		_idMsg = p_idMsg;
	}

	public static short EL_TYPE_PLAYER_KILLED = 1;
	public static short EL_TYPE_UNIT_FINISH = 2;
	public static short EL_TYPE_CITY_LOST = 3;
	public static short EL_TYPE_UNIT_LOST = 4;
	public static short EL_TYPE_PLAYER_START_TURN = 5;
	public static short EL_TYPE_BOMBARDMENT = 6;
	public static short EL_TYPE_ENNEMY_UNIT_KILLED = 7;
	public static short EL_TYPE_UNIT_CREATED = 8;
	public static short EL_TYPE_END_OF_PROCESS_WITH_ERROR = 98;
	public static short EL_TYPE_END_OF_PROCESS = 99;

	public TurnLog setPlayerStarted(final String p_playerName, final AbstractPlayer p_player)
	{
		Assert.precondition(_msgType == 0, "Message Type already set");
		_message = p_playerName;
		_msgType = EL_TYPE_PLAYER_START_TURN;
		_player = p_player;
		return this;
	}

	public TurnLog setEndOfProcess()
	{
		Assert.precondition(_msgType == 0, "Message Type already set");
		_msgType = EL_TYPE_END_OF_PROCESS;
		return this;
	}

	public TurnLog setEndOfProcessWithError()
	{
		Assert.precondition(_msgType == 0, "Message Type already set");
		_msgType = EL_TYPE_END_OF_PROCESS_WITH_ERROR;
		return this;
	}

	public TurnLog setBombardment(final Coord p_bombardmentCoord)
	{
		Assert.precondition(_msgType == 0, "Message Type already set");
		_msgType = EL_TYPE_BOMBARDMENT;
		_coord = p_bombardmentCoord;
		return this;
	}

	public TurnLog setUnitFinished(final AbstractPlayer p_player)
	{
		Assert.precondition(_msgType == 0, "Message Type already set");
		_message = null;
		_coord = null;
		_player = p_player;
		_unitType = null;
		_msgType = EL_TYPE_UNIT_FINISH;
		return this;
	}

	public TurnLog setCityLost(final Coord p_cityCoord, final String p_cityName)
	{
		Assert.precondition(_msgType == 0, "Message Type already set");
		_coord = p_cityCoord;
		_message = p_cityName;
		_unitType = null;
		_msgType = EL_TYPE_CITY_LOST;
		return this;
	}

	public TurnLog setPlayerKilled(final String p_playerName)
	{
		Assert.precondition(_msgType == 0, "Message Type already set");
		_msgType = EL_TYPE_PLAYER_KILLED;
		_message = p_playerName;
		return this;
	}

	public TurnLog setUnitLost(final Coord p_unitCoord, final String p_unitName, final UnitTypeCst p_unitType)
	{
		_coord = p_unitCoord;
		_message = p_unitName;
		_unitType = p_unitType;
		_msgType = EL_TYPE_UNIT_LOST;

		return this;
	}
	
	public TurnLog setUnitCreated(final AbstractUnit p_unit)
	{
		_coord = p_unit.getPosition();
		_message = "Unit Created";
		_unitType = p_unit.getUnitType();
		_msgType = EL_TYPE_UNIT_CREATED;
		
		return this;
	}
	
	public TurnLog setEnnemyUnitKilled(final Coord p_unitCoord, final String p_unitName, final UnitTypeCst p_unitType)
	{
		_coord = p_unitCoord;
		_message = p_unitName;
		_unitType = p_unitType;
		_msgType = EL_TYPE_ENNEMY_UNIT_KILLED;

		return this;
	}

	public String getMessage()
	{
		return _message;
	}

	public Coord getCoord()
	{
		return _coord;
	}

	public UnitTypeCst getUnitType()
	{
		return _unitType;
	}

	public short getMsgType()
	{
		return _msgType;
	}

	public AbstractPlayer getPlayer()
	{
		return _player;
	}

	@Override
	public final String toString()
	{
		final StringBuilder sb = new StringBuilder();

		sb.append("{TURN_MSG = id:").append(_idMsg + " - ");
		if (_msgType == EL_TYPE_PLAYER_KILLED)
		{
			sb.append("Player killed (" + _msgType + "), ");
			sb.append("Player:" + _message);
		}
		else if (_msgType == EL_TYPE_UNIT_FINISH)
		{
			sb.append("Computer Progression(" + _msgType + "), ");
			sb.append("Player:" + _message );
		} else if (_msgType == EL_TYPE_CITY_LOST)
		{
			sb.append("City Lost(" + _msgType + "), ");
			sb.append("city:" + _message + ", ");
			sb.append("position:" + _coord);
		} else if (_msgType == EL_TYPE_UNIT_LOST)
		{
			sb.append("Unit lost(" + _msgType + "), ");
			sb.append("unit:" + _message + "(" + _unitType.toString() + "), ");
			sb.append("position:" + _coord);
		} else if (_msgType == EL_TYPE_UNIT_CREATED)
		{
			sb.append("Unit created(" + _msgType + "), ");
			sb.append("unit:" + _message + "(" + _unitType.toString() + "), ");
			sb.append("position:" + _coord);
		} else if (_msgType == EL_TYPE_PLAYER_START_TURN) {
			sb.append("Player start turn (" + _msgType + "), ");
			sb.append("player :" + _message );
		} else if (_msgType == EL_TYPE_BOMBARDMENT)
		{
			sb.append("Bombardment (" + _msgType + "), ");
			sb.append("coord :" + _coord);
		} else if (_msgType == EL_TYPE_ENNEMY_UNIT_KILLED)
		{
			sb.append("Enemy unit killed (" + _msgType + "), ");
			sb.append("coord :" + _coord + ", ");
			sb.append("Unit : " + _message + ", ");
			sb.append("Unit Type : " + _unitType);
		} else {
			sb.append("Message type unknow (" + _msgType + ")");
		}

		sb.append("}");

		return sb.toString();
	}

}
