/*
 * Created on Dec 10, 2005
 */
package org.jocelyn_chaumel.firststrike.gui.game.board;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Collections;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerId;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.UnitListComparator;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public class BoardLabel extends JLabel
{
	private static ApplicationConfigurationMgr _conf = ApplicationConfigurationMgr.getInstance();

	private boolean _isDebugModeEnabled = _conf.isDebugMode();
	private Cell _cell;

	private boolean _hasFocus;
	private boolean _isShadowedCell;
	private AbstractUnitList _enemyUnitsList;
	private AbstractUnitList _friendlyUnitsList;
	private final CursorMgr _cursorMgr;
	private final BattleBoard _battleBoard;
	private AbstractUnit _currentUnit;
	private final UnitListComparator _unitListComparator = new UnitListComparator();
	private boolean _isBombardmentLocation;
	private boolean _isEnemyBombardmentLocation;

	/**
	 * @param p_cellIcon
	 */
	public BoardLabel(final CursorMgr p_cursorMgr, final BattleBoard p_battleBoard)
	{
		_cursorMgr = p_cursorMgr;
		_battleBoard = p_battleBoard;
	}

	public final void update(	final Icon p_cellIcon,
								final Cell p_boardCell,
								final boolean p_hasFocusFlag,
								final AbstractUnit p_currentUnit,
								final HumanPlayer p_currentPlayer)
	{
		setIcon(p_cellIcon);
		_cell = p_boardCell;
		_hasFocus = p_hasFocusFlag;
		final Coord cellCoord = _cell.getCellCoord();
		_isShadowedCell = false;
		if (p_currentPlayer.getShadowSet().contains(cellCoord))
		{
			_isShadowedCell = true;
			if (!_isDebugModeEnabled)
			{
				return;
			}
		}

		_currentUnit = p_currentUnit;
		if (_conf.isDebugMode() && _battleBoard.getFrame().getGame().getCurrentPlayer() instanceof HumanPlayer) {
			_enemyUnitsList = p_currentPlayer.getUnitIndex().getEnemyList(p_currentPlayer);
		} else {
			_enemyUnitsList = p_currentPlayer.getEnemySet();
		}
		_friendlyUnitsList = p_currentPlayer.getUnitList();
		_isBombardmentLocation = p_currentPlayer.isBombardmentPlanned(cellCoord);
		_isEnemyBombardmentLocation = p_currentPlayer.getEventLog().hasEnemyBombardmentAt(cellCoord);

		final int height = p_cellIcon.getIconHeight();
		final int width = p_cellIcon.getIconWidth();
		final Dimension dimension = new Dimension(width, height);
		setPreferredSize(dimension);
		setSize(dimension);
		setMaximumSize(dimension);
		setMinimumSize(dimension);

	}

	@Override
	public final void paint(final Graphics p_graphics)
	{

		super.paint(p_graphics);
		if (_isShadowedCell && !_isDebugModeEnabled)
		{
			if (_hasFocus)
			{
				drawFocus(p_graphics);
			}
			return;
		}

		final Coord cellCoord = _cell.getCellCoord();
		final AbstractUnit currentUnit = _currentUnit;
		AbstractUnitList unitList = _friendlyUnitsList.getUnitAt(cellCoord);
		AbstractUnit unit = null;
		if (!unitList.isEmpty())
		{

			if (currentUnit != null && unitList.contains(currentUnit))
			{
				unit = currentUnit;
			} else
			{

				Collections.sort(unitList, _unitListComparator);
				unit = (AbstractUnit) unitList.get(0);
			}
		} else
		{
			unitList = _enemyUnitsList.getUnitAt(cellCoord);
			if (!unitList.isEmpty())
			{
				Collections.sort(unitList, _unitListComparator);
				unit = (AbstractUnit) unitList.get(0);
			}
		}

		if (_cell.getCellType().equals(CellTypeCst.CITY))
		{
			AbstractPlayer player = _cell.getCity().getPlayer();

			if (player == null)
			{
				// NOP
			} else
			{
				PlayerId idPlayer = player.getPlayerId();
				int id = idPlayer.getId();
				if (id == 1)
				{
					p_graphics.setColor(Color.BLACK);
				} else if (id == 2)
				{
					p_graphics.setColor(Color.RED);
				} else if (id == 3)
				{
					p_graphics.setColor(Color.BLUE);
				}

				final Dimension dimension = this.getSize();
				final int totalZoneWidth = dimension.width - 1;
				p_graphics.drawRect(0, 0, totalZoneWidth, totalZoneWidth);
				p_graphics.drawRect(1, 1, totalZoneWidth - 2, totalZoneWidth - 2);
			}
		}

		// 1- Display unit
		if (unit != null)
		{
			drawUnit(p_graphics, unit);
		}

		int size = unitList.size();
		if (size > 0)
		{
			if (unit.getPlayer() instanceof HumanPlayer || ApplicationConfigurationMgr.getInstance().isDebugMode())
			{

				// 2- Drawing the unit properties (HitPoints & Fuel)
				drawHp(p_graphics, unit);
				if (unit.getUnitType() == UnitTypeCst.FIGHTER)
				{
					drawFuel(p_graphics, unit);
				}
			}

			// 3- Drawing the unit count
			if (size > 1)
			{
				drawNumber(p_graphics, size);
			}

		}

		if (_isBombardmentLocation)
		{
			drawBombardment(p_graphics);
		}
		
		if (_isEnemyBombardmentLocation)
		{
			drawEnemyBombardment(p_graphics);
		}

		// 5- Drawing path
		if (calculateHasInCurrentPath(currentUnit, cellCoord))
		{
			drawPath(p_graphics);
		}

		if (_battleBoard.isActionInProgress())
		{
			if (_battleBoard.getActionInProgressCoordSet().contains(cellCoord))
			{
				drawGreenRectangle(p_graphics);
			}
		}

		// 6- Drawing focus
		if (_hasFocus)
		{
			drawFocus(p_graphics);
		}
	}

	/**
	 * Draw a path symbol if the current cell is included in the path of the
	 * current unit.
	 * 
	 * @param p_graphics graphics swing object
	 */
	private final void drawPath(final Graphics p_graphics)
	{
		final Path path = _currentUnit.getPath();
		boolean hasEnoughMovePth = false;
		if (!(_currentUnit.getPlayer() instanceof ComputerPlayer))
		{
			final Coord currentCoord = _cell.getCellCoord();

			final int nbTurnCost = path.getTurnCostForCoord(currentCoord);

			if (nbTurnCost == 0)
			{
				hasEnoughMovePth = true;
			} else
			{
				hasEnoughMovePth = false;

				final String text = "" + nbTurnCost;
				final int y = IconFactory.CELL_SIZE - 3;
				drawText(p_graphics, text, Color.RED, 2, y);

			}
		}
		if (hasEnoughMovePth)
		{
			p_graphics.setColor(Color.BLUE);
		} else
		{
			p_graphics.setColor(Color.RED);
		}
		final Dimension dimension = this.getSize();
		final int totalZoneWidth1 = dimension.width - 1;
		final int totalZoneWidth2 = dimension.width - 3;
		p_graphics.drawRect(0, 0, totalZoneWidth1, totalZoneWidth1);
		p_graphics.setColor(Color.CYAN);
		p_graphics.drawRect(1, 1, totalZoneWidth2, totalZoneWidth2);
	}

	private final void drawGreenRectangle(final Graphics p_graphics)
	{
		p_graphics.setColor(Color.GREEN);
		final Dimension dimension = this.getSize();
		final int totalZoneWidth1 = dimension.width - 1;
		final int totalZoneWidth2 = dimension.width - 3;
		p_graphics.drawRect(0, 0, totalZoneWidth1, totalZoneWidth1);
		p_graphics.drawRect(1, 1, totalZoneWidth2, totalZoneWidth2);
	}

	private final void drawFocus(final Graphics g)
	{

		final boolean status = _cursorMgr.getCursorStatus();
		if (status)
		{
			g.setColor(Color.BLACK);
		} else
		{
			g.setColor(Color.WHITE);
		}

		final Dimension dimension = this.getSize();
		final int totalZoneWidth = dimension.width - 1;
		g.drawRect(0, 0, totalZoneWidth, totalZoneWidth);
	}

	private final void drawUnit(final Graphics p_graphics, final AbstractUnit p_unit)
	{
		final UnitTypeCst unitTypeCst = p_unit.getUnitType();
		final PlayerId playerId = p_unit.getPlayer().getPlayerId();

		final Image image = IconFactory.getInstance()
				.getUnitIconByType(unitTypeCst, playerId, IconFactory.SIZE_SIMPLE_SIZE, p_unit.isLiving()).getImage();

		p_graphics.drawImage(image, 0, 0, null, null);
	}

	private final void drawNumber(final Graphics p_graphics, final int p_number)
	{
		final String text = "" + p_number;
		final int textWidth = p_graphics.getFontMetrics().stringWidth(text);
		final Dimension dimension = this.getSize();
		final int x = dimension.width - 2 - textWidth;
		final int y = IconFactory.CELL_SIZE - 3;
		drawText(p_graphics, text, Color.BLACK, x, y);
	}

	private final void drawBombardment(final Graphics p_graphics)
	{
		final ImageIcon fsIcon = IconFactory.getInstance().getCommunIcon(IconFactory.COMMON_BOMBARDMENT);
		p_graphics.drawImage(fsIcon.getImage(), 10, 10, null, null);
	}

	private final void drawEnemyBombardment(final Graphics p_graphics)
	{
		final ImageIcon fsIcon = IconFactory.getInstance().getCommunIcon(IconFactory.COMMON_BOMBARDED);
		p_graphics.drawImage(fsIcon.getImage(), 0, 0, null, null);
	}

	private final void drawText(final Graphics p_graphics,
								final String p_text,
								final Color p_color,
								final int p_x,
								final int p_y)
	{
		p_graphics.setColor(Color.WHITE);
		p_graphics.drawString(p_text, p_x - 1, p_y);
		p_graphics.drawString(p_text, p_x, p_y - 1);
		p_graphics.drawString(p_text, p_x + 1, p_y);
		p_graphics.drawString(p_text, p_x, p_y + 1);
		p_graphics.setColor(p_color);
		p_graphics.drawString(p_text, p_x, p_y);
	}

	private final void drawHp(final Graphics p_graphics, final AbstractUnit p_playerUnit)
	{
		final int hp = p_playerUnit.getHitPoints();
		final int hpCapacity = p_playerUnit.getHitPointsCapacity();
		float hpPercent = ((float) hp) / hpCapacity;
		int red;
		int green;
		if (hpPercent > 0.5f)
		{
			green = 220;
			red = 220 - Math.round(440 * (hpPercent - 0.5f));
		} else
		{
			green = Math.round(440 * hpPercent);
			red = 220;
		}

		final Dimension dimension = this.getSize();
		final int totalZoneWidth = dimension.width - 3;
		final int halfZoneHeight = dimension.height / 2;
		final int width = Math.round(totalZoneWidth * hpPercent);
		final Color hpColor = new Color(red, green, 0);

		p_graphics.setColor(Color.WHITE);
		p_graphics.drawRect(1, halfZoneHeight + 1, totalZoneWidth, 1);
		p_graphics.setColor(hpColor);
		p_graphics.drawLine(2, halfZoneHeight + 1, width, halfZoneHeight + 1);
		p_graphics.drawLine(2, halfZoneHeight + 2, width, halfZoneHeight + 2);

	}

	private final void drawFuel(final Graphics p_graphics, final AbstractUnit p_playerUnit)
	{
		final int fuel = p_playerUnit.getFuel();
		final int fuelCapacity = p_playerUnit.getFuelCapacity();
		float fuelPercent = ((float) fuel) / fuelCapacity;
		int red;
		int blue;
		if (fuelPercent > 0.5f)
		{
			blue = 220;
			red = 220 - Math.round(440 * (fuelPercent - 0.5f));
		} else
		{
			blue = Math.round(440 * fuelPercent);
			red = 220;
		}

		final Dimension dimension = this.getSize();
		final int totalZoneWidth = dimension.width - 3;
		final int halfZoneHeight = dimension.height / 2;

		final int width = Math.round(totalZoneWidth * fuelPercent);
		final Color fuelColor = new Color(red, 0, blue);

		p_graphics.setColor(Color.WHITE);
		p_graphics.drawRect(1, halfZoneHeight + 3, totalZoneWidth, 1);
		p_graphics.setColor(fuelColor);
		p_graphics.drawLine(1, halfZoneHeight + 3, width + 1, halfZoneHeight + 3);
		p_graphics.drawLine(1, halfZoneHeight + 4, width + 1, halfZoneHeight + 4);
	}

	/**
	 * Return if the current cell is included in the path of the current unit.
	 * If no current unit have been selected or if the current unit with a path
	 * is not an human unit, this method return false.
	 * 
	 * @param p_currentUnit
	 * @return
	 */
	private boolean calculateHasInCurrentPath(final AbstractUnit p_currentUnit, final Coord p_cellCord)
	{

		if (p_currentUnit == null)
		{
			return false;
		}

		if (p_currentUnit.getPlayer() instanceof ComputerPlayer
				&& !ApplicationConfigurationMgr.getInstance().isDebugMode())
		{
			return false;
		}

		if (!p_currentUnit.hasPath())
		{
			return false;
		}

		final Path path = p_currentUnit.getPath();
		return path.contains(p_cellCord);
	}
}