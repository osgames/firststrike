package org.jocelyn_chaumel.firststrike.gui.game.board.auto_move;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.board.MoveTypeMsg;

public class AutoMoveWorker extends SwingWorker<MoveStatusCst, MoveTypeMsg>
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TurnWorker.class);
	private final FSFrame _frame;
	private final ApplicationConfigurationMgr _config = ApplicationConfigurationMgr.getInstance();
	private List<MoveTypeMsg> _inProgressMessageList = new ArrayList<MoveTypeMsg>();
	private boolean _interruptionRequest = false;
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	public AutoMoveWorker(final FSFrame p_frame)
	{
		_frame = p_frame;
	}

	@Override
	protected MoveStatusCst doInBackground() throws Exception
	{
		HumanPlayer player = (HumanPlayer) _frame.getGame().getCurrentPlayer();
		_frame.setMouseEnabled(false);
		_frame.setCurrentCell(this, _frame.getCurrentCell().getCellCoord());
		final AbstractUnitList unitList = new AbstractUnitList(player.getUnitList());
		MoveTypeMsg msg;
		_interruptionRequest = false;
		try
		{
			for (AbstractUnit currentUnit : unitList)
			{
				if (_interruptionRequest)
				{
					_logger.playerSubAction("Auto Move Interrupted");
					break;
				}
				if (!currentUnit.hasPath() || currentUnit.isTurnCompleted() || currentUnit.getMove() == 0)
				{
					continue;
				}
				
				// On position l'unit� courante
				msg = new MoveTypeMsg(MoveTypeMsg.SET_INITIAL_POSITION, currentUnit);
				_inProgressMessageList.add(msg);
				_logger.unitSubAction("Message Set current unit:" + currentUnit + " " + currentUnit.getPath());
				publish(msg);
				Thread.yield();
				Thread.sleep(_config.getAutoMoveSpeed() * 2);

				// On attend la consommation du message
				while (!_inProgressMessageList.isEmpty())
				{
					Thread.yield();
					Thread.sleep(10);
				}
				
				
				
				// On d�place l'unit� autant que possible
				while (currentUnit.hasPath() && currentUnit.getPath().getNextTurnCost() == 0
						&& !currentUnit.isTurnCompleted() && !_interruptionRequest)
				{

					//Cr�ation du message
					_logger.unitSubAction("Message One step move:" + currentUnit + " -->" + currentUnit.getPath());
					msg = new MoveTypeMsg(MoveTypeMsg.SET_MOVE_ONE_STEP, currentUnit);
					_inProgressMessageList.add(msg);
					publish(msg);
					Thread.sleep(_config.getAutoMoveSpeed());
					
					// V�rification de la consommation du message
					while (!_inProgressMessageList.isEmpty())
					{
						Thread.yield();
						Thread.sleep(10);
					}
				}
			}
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
		_logger.playerSubAction("Auto Move completed");
		_frame.setMouseEnabled(true);
		_frame.setCurrentCell(this, _frame.getCurrentCell().getCellCoord());
		_frame.setFocusOnBoard();
		return MoveStatusCst.MOVE_SUCCESFULL;
	}

	@Override
	protected void process(final List<MoveTypeMsg> p_messageList)
	{
		for (MoveTypeMsg currentMsg : p_messageList)
		{
			if (currentMsg.getMoveType() == MoveTypeMsg.SET_INITIAL_POSITION)
			{
				final AbstractUnit currentUnit = (AbstractUnit) currentMsg.getObject();
				_logger.unitDetail("Execute set current unit:" + currentUnit + " " + currentUnit.getPath());
				_frame.setCurrentUnit(this, currentUnit);
				_inProgressMessageList.remove(currentMsg);
			} else if (currentMsg.getMoveType() == MoveTypeMsg.SET_MOVE_ONE_STEP)
			{
				try
				{
					final AbstractUnit currentUnit = (AbstractUnit) currentMsg.getObject();
					_logger.unitDetail("Execute One step move:" + currentUnit + " -->" + currentUnit.getPath());
					MoveStatusCst status = _frame.getGame().performMoveUnitToNextCoord(currentUnit);
					if (MoveStatusCst.UNIT_DEFEATED.equals(status)) {
						// TODO AZ Add customized popup with a fighter crashed on the ground
						_interruptionRequest = true;
						JOptionPane.showMessageDialog(	_frame.getContentPane(),
														_bundle.getString("fs.frame.fighter.no_more_fuel.text"),
														_bundle.getString("fs.frame.fighter.no_more_fuel.title"),
														JOptionPane.INFORMATION_MESSAGE);
						
					}
					if (MoveStatusCst.ENEMY_DETECTED.equals(status))
					{
						_interruptionRequest = true;
					}
				} catch (Exception ex)
				{
					ex.printStackTrace();
				}
				_inProgressMessageList.remove(currentMsg);
			}
		}
	}

}
