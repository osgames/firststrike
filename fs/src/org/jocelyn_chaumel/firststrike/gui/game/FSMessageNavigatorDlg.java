package org.jocelyn_chaumel.firststrike.gui.game;

import javax.swing.JFrame;

import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMessageList;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;

public class FSMessageNavigatorDlg extends FSDialog
{

	private final MessageNavigatorPnl _messagePnl;

	public FSMessageNavigatorDlg(final JFrame p_fsFrame, final String p_mapName, final ScenarioMessageList p_scnMsgList)
	{
		super(p_fsFrame, "No title", true);
		_messagePnl = new MessageNavigatorPnl(this, p_mapName, p_scnMsgList);
		setContentPane(_messagePnl);
	}

}
