package org.jocelyn_chaumel.firststrike.gui.game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.ResourceBundle;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.FirstStrikeMgr;
import org.jocelyn_chaumel.firststrike.core.IFirstStrikeEvent;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.help.HelpMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMessageList;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.ILoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.gui.commun.FSFileFilter;
import org.jocelyn_chaumel.firststrike.gui.commun.FSRowHeaderRenderer;
import org.jocelyn_chaumel.firststrike.gui.commun.MemoryMgr;
import org.jocelyn_chaumel.firststrike.gui.game.board.BattleBoard;
import org.jocelyn_chaumel.firststrike.gui.game.board.CursorMgr;
import org.jocelyn_chaumel.firststrike.gui.game.board.auto_move.AutoMoveWorker;
import org.jocelyn_chaumel.firststrike.gui.game.board.auto_move.ProgressDialog;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSFrameEvent;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSWindowListener;
import org.jocelyn_chaumel.firststrike.gui.game.event.IFSFrameEventListener;
import org.jocelyn_chaumel.firststrike.gui.game.menu.FSMenuBarMgr;
import org.jocelyn_chaumel.firststrike.gui.game.menu.attack.AttackDlg;
import org.jocelyn_chaumel.firststrike.gui.game.menu.city.CityDialog;
import org.jocelyn_chaumel.firststrike.gui.game.menu.newgame.FSNewGameWzd;
import org.jocelyn_chaumel.firststrike.gui.game.toolbar.button.ButtonToolBar;
import org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic.CurrentSelectionMgrPnl;
import org.jocelyn_chaumel.firststrike.gui.game.toolbar.horizontal.UnitListMgr;
import org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.VerticalToolBar;
import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;
import org.jocelyn_chaumel.tools.gui.jtable.ScrollableJTable;

public class FSFrame extends JFrame implements IFirstStrikeEvent
{
	// TODO AAZ BUG : Ne plus g�n�rer d'erreur lorsqu'une unit� sc�nario est
	// cr��e sur un emplacement occup�
	// TODO BZ Evolution : Affichage d'une mini carte
	// TODO BZ Evolution : Unite - sous-marin, porte-avion, Awac, Bombardier, infantrie, helicopter
	// TODO BZ Evolution : Unite - drone (patrouilleur, attaquant et r�parant)
	// TODO BZ Evolution : Ajouter notion d'exp�rience aux unites
	// TODO BZ Evolution : Ajouter notion de recherche dans les villes
	// TODO BZ Evolution : Ajouter option patrouille pour les avions
	// TODO AZ Evolution : Ajouter point de ralliment pour les villes

	private final static FSLogger _logger = FSLoggerManager.getLogger(FSFrame.class.getName());
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private final static SimpleDateFormat _dateFormat = new SimpleDateFormat("yy/MM/dd kk:mm:ss");
	/**
	 * Panel Cell's Definition.
	 */
	private Component _battleBoardCenterComponent = null;
	private final JPanel _battleBoardPanel;
	private final Component _genericBattleBoardCenterComponent = new BlueLable();
	private BattleBoard _battleBoard = null;
	private UnitListMgr _unitListPanel;

	private final ApplicationConfigurationMgr _appConf;

	private FirstStrikeGame _currentGame = null;
	private AbstractUnit _currentUnit = null;
	private ILoadableUnit _currentContainedUnit = null;
	private Cell _currentCell = null;
	private File _currentGameDir = null;
	private File _currentGameFile = null;
	private JDialog _currentDialog = null;

	private final FSMenuBarMgr _menuBar;
	private final ArrayList<IFSFrameEventListener> _actionListener = new ArrayList<IFSFrameEventListener>();
	private final UnitListComparator _unitComparator = new UnitListComparator();
	private CurrentSelectionMgrPnl _currentSelectionMgrPnl;
	private final VerticalToolBar _verticalToolBar;
	private final ButtonToolBar _toolBar;

	private final CursorMgr _cursorMgr;

	public FSFrame() throws IOException
	{
		super();

		_appConf = ApplicationConfigurationMgr.getInstance();
		_currentGameDir = _appConf.getGameDir();

		_currentSelectionMgrPnl = new CurrentSelectionMgrPnl(this);
		addActionListener(_currentSelectionMgrPnl);
		_battleBoard = new BattleBoard(this);
		addActionListener(_battleBoard);
		_cursorMgr = new CursorMgr(_battleBoard);
		addActionListener(_cursorMgr);

		Container mainPanel = getContentPane();
		final FSWindowListener fsWindowListener = new FSWindowListener();
		super.addComponentListener(fsWindowListener);
		super.addWindowListener(fsWindowListener);

		_menuBar = new FSMenuBarMgr(this);

		// TOOL BAR
		_toolBar = new ButtonToolBar(this);

		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(_toolBar, BorderLayout.NORTH);

		_battleBoardPanel = new JPanel(new BorderLayout());
		_verticalToolBar = new VerticalToolBar(this);

		// CONTEXT BAR
		_battleBoardPanel.add(_currentSelectionMgrPnl, BorderLayout.WEST);
		_battleBoardPanel.add(_verticalToolBar, BorderLayout.EAST);

		// JTABLE
		_battleBoardCenterComponent = _genericBattleBoardCenterComponent;
		_battleBoardPanel.add(_battleBoardCenterComponent, BorderLayout.CENTER);

		mainPanel.add(_battleBoardPanel, BorderLayout.CENTER);

		setFrameTitle();
	}

	public final CursorMgr getCursorMgr()
	{
		return _cursorMgr;
	}

	public final Cell getCurrentCell()
	{
		return _currentCell;
	}

	public AbstractUnit getCurrentUnit()
	{
		return _currentUnit;
	}

	public ILoadableUnit getCurrentContainedUnit()
	{
		return _currentContainedUnit;
	}

	public final UnitListMgr getUnitListContext()
	{
		return _unitListPanel;
	}

	private void loadGenericBoard()
	{
		_battleBoardCenterComponent = _genericBattleBoardCenterComponent;
		_battleBoardPanel.add(_battleBoardCenterComponent, BorderLayout.CENTER);
	}

	private void loadGameBoard(final Coord p_startPosition) throws IOException
	{
		// Update the frame title
		setFrameTitle();

		fireLoadGameEvent();
		switchBlueBackgroundToBattleBoard();
		fireUpdateCurrentCellEvent(this, p_startPosition);
		_battleBoard.requestFocusInWindow();
	}

	public final void setCurrentCell(final Object p_source, final Coord p_cellCoord)
	{
		setCurrentContainedUnit(null);
		fireUpdateCurrentCellEvent(p_source, p_cellCoord);
		_battleBoard.requestFocusInWindow();
	}

	public final void setCenterOfBattleBoard(final Coord p_position)
	{
		_battleBoard.setCenterOfBattleBoard(p_position);
	}

	private void setFrameTitle()
	{
		final ApplicationConfigurationMgr conf = ApplicationConfigurationMgr.getInstance();

		final String version = conf.getFSVersion();
		final StringBuffer title = new StringBuffer("First Strike V");
		title.append(version);
		if (_currentGame != null)
		{
			title.append(" - ").append(_currentGame.getShortGameName());
			title.append(" - ").append(_currentGame.getTurnNumber());
			final long lgSaveDate = _currentGame.getSaveDate();
			final Date saveDate;
			if (lgSaveDate == 0)
			{
				saveDate = new Date();
			} else
			{
				saveDate = new Date(lgSaveDate);
			}
			title.append(" - ").append(_dateFormat.format(saveDate));
		}

		setTitle(title.toString());
	}

	public final FirstStrikeGame getGame()
	{
		if (_currentGame == null)
		{
			throw new UnsupportedOperationException("No current game is loaded");
		}
		return _currentGame;
	}

	public final BattleBoard getBattleBoard()
	{
		return _battleBoard;
	}

	public final void refreshAllBattleBoard(final boolean p_resizingNeeded)
	{
		final Coord currentCoord = _currentCell.getCellCoord();
		if (!p_resizingNeeded)
		{
			final HumanPlayer player = (HumanPlayer) _currentGame.getCurrentPlayer();
			final FSMap map = _currentGame.getMapMgr().getMap();
			final int mapWidth = map.getWidth();
			final int mapHeight = map.getHeight();
			final int range = Math.max(mapWidth, mapHeight);
			final CoordSet coordToRefresh = CoordSet
					.createInitiazedCoordSet(new Coord(0, 0), range, mapWidth, mapHeight);
			if (!_appConf.isDebugMode())
			{
				final CoordSet shadowSet = player.getShadowSet();
				coordToRefresh.removeAll(shadowSet);
			}
			_battleBoard.refreshBoardCell(coordToRefresh);
			return;
		}

		// Release the current board. (but get the board's model)
		_battleBoardPanel.remove(_battleBoardCenterComponent);

		// Create a new Board.
		_battleBoardCenterComponent = new ScrollableJTable(_battleBoard, new FSRowHeaderRenderer(_battleBoard));
		_battleBoardPanel.add(_battleBoardCenterComponent, BorderLayout.CENTER);

		// Set the cursor on the user's start location
		setCurrentCell(this, currentCoord);
	}

	private void fireUpdateCurrentUnitEvent(final Object p_source, final AbstractUnit p_unit)
	{
		_currentUnit = p_unit;
		final Coord position = _currentUnit.getPosition();

		final int row = position.getY();
		final int col = position.getX();
		_currentCell = (Cell) _battleBoard.getModel().getValueAt(row, col);

		final FSFrameEvent event = new FSFrameEvent(p_source, _currentGame, _currentCell, _currentUnit);
		for (IFSFrameEventListener listener : _actionListener)
		{
			listener.updateCurrentUnitEvent(event);
		}
	}

	private void fireMoveUnitEvent(final Object p_source, final AbstractUnit p_unit)
	{
		final FSFrameEvent event = new FSFrameEvent(p_source, _currentGame, _currentCell, _currentUnit, p_unit);
		for (IFSFrameEventListener listener : _actionListener)
		{
			listener.unitMovedEvent(event);
		}
	}
	
	private void fireKillUnitEvent(final Object p_source, final AbstractUnit p_unit)
	{
		final FSFrameEvent event = new FSFrameEvent(p_source, _currentGame, _currentCell, _currentUnit, p_unit);
		for (IFSFrameEventListener listener : _actionListener)
		{
			listener.lostUnitEvent(event);
		}
	}

	private void fireUpdateCurrentCellEvent(final Object p_source, final Coord p_coord)
	{
		final int row = p_coord.getY();
		final int col = p_coord.getX();
		_currentCell = (Cell) _battleBoard.getModel().getValueAt(row, col);
		final HumanPlayer player = (HumanPlayer) _currentGame.getCurrentPlayer();

		final AbstractUnitList completeUnitList = player.getUnitList();
		if (completeUnitList.size() >= 3)
		{
			HelpMgr.getInstance()
					.displayHelpIfRequired(this, new String[] { HelpMgr.HELP_FOG_CHAPTER, HelpMgr.HELP_FOG_MOVE_CHAPTER });
		}

		_currentUnit = null;

		AbstractUnitList unitList = completeUnitList.getUnitAt(p_coord);
		if (unitList.isEmpty())
		{
			final AbstractUnitList enemySet;

			if (_appConf.isDebugMode())
			{
				enemySet = player.getUnitIndex().getNonNullUnitListAt(p_coord);
			} else
			{
				enemySet = player.getEnemySet().getUnitAt(p_coord);
			}
			if (!enemySet.isEmpty())
			{
				HelpMgr.getInstance().displayHelpIfRequired(this, new String[] { HelpMgr.HELP_ATTACK_ENEMY_CHAPTER });

			}
			unitList = enemySet.getUnitAt(p_coord);
		}
		if (unitList.size() >= 2)
		{
			Collections.sort(unitList, _unitComparator);
		}

		if (unitList.size() > 0)
		{
			_currentUnit = unitList.get(0);
		} else
		{
			_currentUnit = null;
		}

		final FSFrameEvent event = new FSFrameEvent(p_source, _currentGame, _currentCell, _currentUnit);
		for (IFSFrameEventListener listener : _actionListener)
		{
			listener.updateCurrentCellEvent(event);
		}
	}

	public final void setCurrentUnit(final Object p_source, final AbstractUnit p_unit)
	{
		if (!_currentCell.getCellCoord().equals(p_unit.getPosition()))
		{
			setCurrentCell(p_source, p_unit.getPosition());
		}
		setCurrentContainedUnit(null);
		fireUpdateCurrentUnitEvent(p_source, p_unit);
		if (_currentUnit instanceof TransportShip)
		{
			HelpMgr.getInstance().displayHelpIfRequired(this, new String[] { HelpMgr.HELP_LOADING_TANK_CHAPTER });
		}

		if (_currentUnit != null && _currentUnit.hasPath())
		{
			HelpMgr.getInstance().displayHelpIfRequired(	this,
												new String[] { HelpMgr.HELP_MOVE_UNIT_CHAPTER,
														HelpMgr.HELP_PATH_CHAPTER });
		}
	}

	public final void setCurrentContainedUnit(final ILoadableUnit p_currentContainedUnit)
	{
		_currentContainedUnit = p_currentContainedUnit;
	}

	public final void saveGame()
	{
		if (_currentGameFile == null)
		{
			saveAsGame();
			return;
		}

		if (_appConf.isTurnSaveMode())
		{
			String directory = _currentGameFile.getParentFile().getAbsolutePath() + File.separator;
			File newGameFile = new File(directory + FileMgr.getFileNameWithoutExtention(_currentGameFile) + "."
					+ FSFileFilter.FIRSTSTRIKE_FILE_EXTENTION + _currentGame.getTurnNumber());
			try
			{
				FileMgr.forceDeleteIfPresent(newGameFile);
			} catch (IOException ex)
			{
				ex.printStackTrace();
			}
			_currentGameFile = newGameFile;
		}

		_logger.systemAction("Saving current game...");
		MemoryMgr.log(_logger, "MEMORY STATUS - Saving Game:");
		try
		{
			FirstStrikeMgr.saveFirstStrikeGame(_currentGame, _currentGameFile);
			setFrameTitle();

		} catch (IOException ex)
		{
			_logger.systemError("", ex);
			JOptionPane.showInternalMessageDialog(	this.getContentPane(),
													_bundle.getString("fs.error.unable_to_save"),
													_bundle.getString("fs.error.title"),
													JOptionPane.ERROR_MESSAGE);
			return;
		}
		_logger.systemAction("Saving current game command finished succesfully");
		MemoryMgr.log(_logger, "MEMORY STATUS - Game Saved:");
	}

	public final void saveAsGame()
	{
		_logger.systemAction("Save as a game ...");

		try
		{
			Assert.check(_currentGame != null, "Main Board isn't Null");

			// Identify the map source folder
			final File gameDir = _currentGameDir;

			// Create the dialog box "New Game"
			final JFileChooser dialog = new JFileChooser(_bundle.getString("fs.frame.save_as"));
			dialog.setDialogType(JFileChooser.SAVE_DIALOG);
			dialog.setCurrentDirectory(gameDir);
			dialog.setFileFilter(new FSFileFilter(FSFileFilter.FIRSTSTRIKE_FILE_EXTENTION,
					FSFileFilter.FIRSTSTRIKE_FILE_DESC));

			final int returnVal = dialog.showSaveDialog(this);

			if (returnVal != JFileChooser.APPROVE_OPTION)
			{
				_logger.systemSubAction("Save As command is canceled");
				return;
			}
			File gameFile = dialog.getSelectedFile();
			gameFile = FileMgr.addExtentionIfNecessairy(gameFile, FSFileFilter.FIRSTSTRIKE_FILE_EXTENTION);
			if (_appConf.isTurnSaveMode())
			{
				gameFile = new File(gameFile.getAbsolutePath() + _currentGame.getTurnNumber());
			}

			if (gameFile.isFile())
			{
				final int result = JOptionPane.showConfirmDialog(	this.getContentPane(),
																	_bundle.getString("fs.frame.file_already_exists"),
																	_bundle.getString("fs.frame.saving_game"),
																	JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.NO_OPTION)
				{
					_logger.systemSubAction("Save as canceled");
				}
				FileMgr.forceDelete(gameFile);
			}
			FirstStrikeMgr.saveFirstStrikeGame(_currentGame, gameFile);
			_currentGameDir = gameFile.getParentFile();
			_currentGameFile = gameFile;
			_appConf.updateLastGameList(gameFile);
			_appConf.setGameDir(_currentGameDir);
			_appConf.save();
			_menuBar.saveAs();
			setFrameTitle();
			_logger.systemAction("Save as command finished successufully");
		} catch (Exception ex)
		{
			_logger.systemError("", ex);
			JOptionPane.showInternalMessageDialog(	this.getContentPane(),
													_bundle.getString("fs.error.unexpected_error") + ex.getMessage(),
													_bundle.getString("fs.error.title"),
													JOptionPane.ERROR_MESSAGE);
		}
	}

	public void newGame()
	{
		_logger.systemAction("Creating a new game...");

		// Check for confirmation
		if (_currentGame != null)
		{
			final int result = JOptionPane.showConfirmDialog(	this.getContentPane(),
																_bundle.getString("fs.frame.finish_game"),
																_bundle.getString("fs.frame.closing_game"),
																JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.NO_OPTION)
			{
				_logger.systemSubAction("New game canceled");
				return;
			}
		}
		try
		{

			// Identify the map source folder

			// Create the dialog box "New Game"
			MemoryMgr.log(_logger, "MEMORY STATUS - Creating new game:");
			final String defaultGameName = _bundle.getString("fs.frame.default_game_name");
			final FSNewGameWzd newGameWzd = new FSNewGameWzd(this, defaultGameName);
			if (!newGameWzd.launch())
			{
				_logger.systemSubAction("New game creation canceled");
				return;
			}

			if (_currentGame != null)
			{
				closeGame(false);
			}
			Assert.check(_currentGame == null, "Main Board isn't Null");

			// Create the game
			_currentGame = newGameWzd.getGame();

			_currentGame.setFrameListener(this);
			loadGameBoard(_currentGame.getCurrentPlayer().getStartPosition());

			_currentGameFile = null;
			_logger.systemSubAction("New game created successfuly");

			HelpMgr.getInstance()
					.displayHelpIfRequired(this, new String[] { HelpMgr.HELP_LEFT_PANEL_CHAPTER, HelpMgr.HELP_CITIES_CHAPTER });

			MemoryMgr.log(_logger, "MEMORY STATUS - New game created:");
			this.paint(this.getGraphics());
			_battleBoard.setRequestFocusEnabled(true);
			_currentGame.startGame();

			final int currentTurn = _currentGame.getTurnNumber();
			final ScenarioMessageList msgList = ((HumanPlayer) _currentGame.getCurrentPlayer())
					.consumeScenarioMessageForTurn(currentTurn);
			final FSMap map = _currentGame.getMapMgr().getMap();
			if (msgList.size() > 0)
			{
				final String mapName = map.getName();
				final FSMessageNavigatorDlg dlg = new FSMessageNavigatorDlg(this, mapName, msgList);
				dlg.setVisible(true);
				dlg.dispose();
			}

		} catch (Exception ex)
		{
			closeGame(false);
			_logger.systemError("", ex);
			JOptionPane.showInternalMessageDialog(	this.getContentPane(),
													_bundle.getString("fs.error.unexpected_error") + ex.getMessage(),
													_bundle.getString("fs.error.title"),
													JOptionPane.ERROR_MESSAGE);
		}
	}

	private final void switchBlueBackgroundToBattleBoard()
	{
		// Release the current board. (but get the board's model)
		_battleBoardPanel.remove(_battleBoardCenterComponent);

		// Create a new Board.
		_battleBoardCenterComponent = new ScrollableJTable(_battleBoard, new FSRowHeaderRenderer(_battleBoard));
		_battleBoardPanel.add(_battleBoardCenterComponent, BorderLayout.CENTER);

	}

	private final void fireLoadGameEvent()
	{
		final FSFrameEvent event = new FSFrameEvent(this, _currentGame, null, null);
		for (IFSFrameEventListener currentListener : _actionListener)
		{
			currentListener.loadGameEvent(event);
		}
	}

	private final void fireCloseGameEvent()
	{
		final FSFrameEvent event = new FSFrameEvent(this, null, null, null);
		for (IFSFrameEventListener currentListener : _actionListener)
		{
			currentListener.closeGameEvent(event);
		}
	}

	private final void fireConquerCityEvent(final City p_city)
	{
		if (p_city.getPlayer() instanceof HumanPlayer)
		{
			final FSFrameEvent event = new FSFrameEvent(this, _currentGame, _currentCell, _currentUnit, p_city);
			for (IFSFrameEventListener currentListener : _actionListener)
			{
				currentListener.conquerCityEvent(event);
			}
		}
	}

	private void checkGameStatus()
	{
		if (_currentGame == null)
		{
			return;
		}

		if (!_currentGame.isGameOver())
		{
			return;
		}

		final AbstractPlayer winner = _currentGame.getWinner();
		final Container currentContainer;
		if (_currentDialog != null)
		{
			currentContainer = _currentDialog.getContentPane();
		} else
		{
			currentContainer = this.getContentPane();
		}
		if (winner instanceof HumanPlayer)
		{
			JOptionPane.showInternalMessageDialog(	currentContainer,
													_bundle.getString("fs.frame.win_game_text"),
													_bundle.getString("fs.frame.win_game_title"),
													JOptionPane.INFORMATION_MESSAGE);
		} else
		{
			JOptionPane.showInternalMessageDialog(	currentContainer,
													_bundle.getString("fs.frame.loose_game_text"),
													_bundle.getString("fs.frame.loose_game_title"),
													JOptionPane.INFORMATION_MESSAGE);
		}

		if (_currentDialog == null)
		{
			closeGame(false);
		}
	}

	public final void addActionListener(final IFSFrameEventListener p_actionListener)
	{
		Assert.preconditionNotNull(p_actionListener, "Action Listener");
		Assert.precondition(!_actionListener.contains(p_actionListener),
							"This action listener has been already registered");

		_actionListener.add(p_actionListener);
	}

	private final void fireLostCityEvent(final City p_city, final AbstractPlayer p_oldOwner)
	{
		if (p_oldOwner instanceof HumanPlayer)
		{
			final FSFrameEvent event = new FSFrameEvent(this, _currentGame, _currentCell, _currentUnit, p_city);
			for (IFSFrameEventListener currentListener : _actionListener)
			{
				currentListener.lostCityEvent(event);
			}
		}
	}

	private final void fireLostUnitEvent(final AbstractUnit p_unit)
	{
		final FSFrameEvent event = new FSFrameEvent(this, _currentGame, _currentCell, _currentUnit, p_unit);
		for (IFSFrameEventListener currentListener : _actionListener)
		{
			currentListener.lostUnitEvent(event);
		}
	}

	private final void fireNewUnitEvent(final AbstractUnit p_unit)
	{
		final FSFrameEvent event = new FSFrameEvent(this, _currentGame, _currentCell, _currentUnit, null);
		for (IFSFrameEventListener currentListener : _actionListener)
		{
			currentListener.unitCreatedEvent(event);
		}
	}

	/**
	 * Close First Strike.
	 */
	public void exitFirstStrike()
	{
		_logger.systemAction("Closing Firts Strike...");
		if (_currentGame != null)
		{
			final int result = JOptionPane.showConfirmDialog(	this.getContentPane(),
																_bundle.getString("fs.frame.exit_fs"),
																_bundle.getString("fs.frame.closing_fs"),
																JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.NO_OPTION)
			{
				_logger.systemSubAction("Closing First Strike canceled");
				return;
			}
		}
		_logger.systemSubAction("Closing Firts Strike confirm");
		dispose();
		System.exit(0);
	}

	public final void nextTurn()
	{
		_logger.systemAction("Next Turn");
		MemoryMgr.log(_logger, "MEMORY STATUS - Next Turn:");
		try
		{
			_cursorMgr.pause();
			if (_currentGame.getOptionMgr().isAutoSave())
			{
				saveGame();
			}

			ProgressDialog pd = new ProgressDialog(this);
			pd.pack();
			pd.setVisible(true);
			if (pd.isFatalErrorDetected())
			{
				JOptionPane.showMessageDialog(	this,
												_bundle.getString("fs.error.unexpected_fatal_error"),
												_bundle.getString("fs.error.title"),
												JOptionPane.ERROR_MESSAGE);
				closeGame(false);
				return;
			}
			
			AbstractPlayer currentPlayer = _currentGame.getCurrentPlayer();

			if (currentPlayer == null)
			{
				final AbstractPlayer lastPlayer = _currentGame.getPlayerMgr().getPlayerList().getLivingPlayer().get(0);
				if (lastPlayer instanceof HumanPlayer)
				{
					JOptionPane.showMessageDialog(	this,
													_bundle.getString("fs.frame.win_game_text"),
													_bundle.getString("fs.frame.win_game_title"),
													JOptionPane.INFORMATION_MESSAGE);
				} else
				{
					JOptionPane.showMessageDialog(	this,
													_bundle.getString("fs.frame.loose_game_text"),
													_bundle.getString("fs.frame.loose_game_title"),
													JOptionPane.INFORMATION_MESSAGE);

				}
				closeGame(false);
				return;
			}
			_verticalToolBar.refreshAllList();

			final int currentTurn = _currentGame.getTurnNumber();
			final ScenarioMessageList msgList = ((HumanPlayer) currentPlayer)
					.consumeScenarioMessageForTurn(currentTurn);
			if (msgList.size() > 0)
			{
				final String mapName = _currentGame.getMapMgr().getMapName();
				final FSMessageNavigatorDlg dlg = new FSMessageNavigatorDlg(this, mapName, msgList);
				dlg.setVisible(true);
				dlg.dispose();
			}

			_cursorMgr.restart();
			refreshAllBattleBoard(false);
			fireUpdateCurrentCellEvent(this, _currentCell.getCellCoord());
			// fireNewUnitsEvent(_newUnitsCreatedDuringThisTurnList);
			_verticalToolBar.refreshAllList();

			if (_appConf.isAutoMoveActivated())
			{

				if (currentPlayer.getEnemySet().size() == 0)
				{
					startAutoMove();
				}
			}

		} catch (Exception ex)
		{
			_logger.systemError("An unexpected error occurs during the operation.\nThis game will be automatically closed.",
								ex);

			JOptionPane.showMessageDialog(	this,
											_bundle.getString("fs.error.unexpected_fatal_error"),
											_bundle.getString("fs.common.popup_message"),
											JOptionPane.ERROR_MESSAGE);
			closeGame(false);
		}
	}

	public final void displayOldInstantMessage()
	{

		final ScenarioMessageList msgList = _currentGame.getCurrentPlayer().getOldScenarioMessageList();
		if (msgList.size() == 0)
		{
			JOptionPane.showInternalMessageDialog(	this.getContentPane(),
													_bundle.getString("fs.frame.no_old_instant_message"),
													_bundle.getString("fs.common.popup_message"),
													JOptionPane.OK_OPTION);
		} else
		{
			final String mapName = _currentGame.getMapMgr().getMapName();
			final FSMessageNavigatorDlg dlg = new FSMessageNavigatorDlg(this, mapName, msgList);
			dlg.setVisible(true);
			dlg.dispose();
		}

	}

	public final void openGame(final File p_gameFile)
	{
		if (_currentGame == null)
		{
			_logger.systemAction("Opening game...");
		} else
		{
			_logger.systemAction("Closing and opening game...");
			final int result = JOptionPane.showConfirmDialog(	this.getContentPane(),
																_bundle.getString("fs.frame.finish_game"),
																_bundle.getString("fs.common.popup_message"),
																JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.NO_OPTION)
			{
				_logger.systemSubAction("Closing current game is canceled");
				return;
			}
			closeGame(false);
		}

		File gameFile;
		if (p_gameFile != null)
		{
			gameFile = p_gameFile;
		} else
		{
			final JFileChooser dialog = new JFileChooser(_bundle.getString("fs.frame.open_game"));
			dialog.setDialogType(JFileChooser.OPEN_DIALOG);
			dialog.setCurrentDirectory(_currentGameDir);
			dialog.setFileFilter(new FSFileFilter(FSFileFilter.FIRSTSTRIKE_FILE_EXTENTION,
					FSFileFilter.FIRSTSTRIKE_FILE_DESC));
			final int returnVal = dialog.showOpenDialog(this);

			if (returnVal != JFileChooser.APPROVE_OPTION)
			{
				_logger.systemSubAction("Open game canceled");
				loadGenericBoard();
				return;
			}

			gameFile = dialog.getSelectedFile();
		}

		try
		{
			MemoryMgr.log(_logger, "MEMORY STATUS - Loading Game:");
			final FirstStrikeGame gameArray = FirstStrikeMgr.loadFirstStrikeGame(gameFile);
			if (gameArray == null)
			{
				JOptionPane.showInternalMessageDialog(	this.getContentPane(),
														_bundle.getString("fs.frame.error.incompatible_game"),
														_bundle.getString("fs.error.title"),
														JOptionPane.OK_OPTION);
				_appConf.removeGameFromLastGameList(gameFile);
				return;
			}

			_currentGame = gameArray;
			_currentGameFile = gameFile;
			_appConf.updateLastGameList(_currentGameFile);

			try
			{
				loadGameBoard(_currentGame.getCurrentPlayer().getStartPosition());

				_appConf.updateLastGameList(_currentGameFile);
				_currentGame.setFrameListener(this);
			} catch (Exception ex)
			{
				_logger.systemError("Unable to load game board", ex);
				JOptionPane.showInternalMessageDialog(	this.getContentPane(),
														_bundle.getString("fs.frame.error.incompatible_game"),
														_bundle.getString("fs.error.title"),
														JOptionPane.OK_OPTION);
				_appConf.removeGameFromLastGameList(gameFile);
				closeGame(false);
				return;
			}
		} catch (Exception e)
		{
			_logger.systemError("Unable to load game board", e);
			JOptionPane.showInternalMessageDialog(	this.getContentPane(),
													_bundle.getString("fs.frame.error.incompatible_game"),
													_bundle.getString("fs.error.title"),
													JOptionPane.OK_OPTION);
			_appConf.removeGameFromLastGameList(gameFile);
			return;

		} finally
		{
			if (_appConf.isModified())
			{
				try
				{
					_appConf.save();
				} catch (IOException ex)
				{
					_logger.systemError("Unable to save Application Configuration", ex);
				}
				_menuBar.updateLastGameList();
			}

		}
		MemoryMgr.log(_logger, "MEMORY STATUS - Game Loaded:");

	}

	public final boolean closeGame(final boolean p_confirmationFlag)
	{
		_logger.systemAction("Close game");
		MemoryMgr.log(_logger, "MEMORY STATUS - Closing Game:");
		// Confirm the operation
		if (p_confirmationFlag)
		{
			final int result = JOptionPane.showConfirmDialog(	this.getContentPane(),
																_bundle.getString("fs.frame.finish_game"),
																_bundle.getString("fs.common.popup_message"),
																JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.NO_OPTION)
			{
				_logger.systemSubAction("Close game canceled");
				return false;
			}
		}

		// Release the battle board
		_battleBoardPanel.remove(_battleBoardCenterComponent);
		_currentGame.removeFrameListener();
		_currentGame = null;
		_currentUnit = null;
		_currentGameFile = null;

		// Load the generic board and card
		fireCloseGameEvent();
		setFrameTitle();

		loadGenericBoard();

		paint(this.getGraphics());

		MemoryMgr.log(_logger, "MEMORY STATUS - Game closed:");
		_logger.systemSubAction("Game successfuly closed");
		return true;
	}

	public final void openCity(final Coord p_cityCoord)
	{
		final City city = _currentGame.getMapMgr().getCityList().getCityAtPosition(p_cityCoord);
		final CityDialog dlg = new CityDialog(this, city);
		dlg.pack();
		dlg.setVisible(true);
		final boolean updateNeeded = dlg.isUpdateNeeded();
		dlg.dispose();
		if (updateNeeded)
		{
			_currentSelectionMgrPnl.updateCurrentCellEvent(null);
		}
		_battleBoard.grabFocus();
	}

	public final void attackEnemy(final Coord p_enemyPosition)
	{

		final Coord unitCoord = _currentUnit.getPosition();
		_currentDialog = new AttackDlg(this, _currentUnit, p_enemyPosition);
		_currentDialog.pack();
		_currentDialog.setVisible(true);
		_currentDialog.dispose();
		_currentDialog = null;

		if (_currentGame == null || _currentGame.isGameOver())
		{
			closeGame(false);
			return;
		}
		if (!_currentUnit.isLiving())
		{
			fireLostUnitEvent(_currentUnit);
		}

		setCurrentCell(this, unitCoord);

		final FSMap map = _currentGame.getMapMgr().getMap();
		final int mapHeight = map.getHeight();
		final int mapWidth = map.getWidth();
		final CoordSet refreshCoordList = CoordSet.createInitiazedCoordSet(unitCoord, 1, mapWidth, mapHeight);
		_battleBoard.refreshBoardCell(refreshCoordList);
		checkGameStatus();
	}

	public final void gotoCityWithoutProduction()
	{
		final AbstractPlayer player = _currentGame.getCurrentPlayer();
		final CityList playerCitySet = _currentGame.getMapMgr().getCityList().getCityByPlayer(player)
				.getCityWithoutProduction();
		if (playerCitySet.isEmpty())
		{
			JOptionPane.showMessageDialog(	this,
											_bundle.getString("fs.frame.no_empty_city"),
											_bundle.getString("fs.error.invalid_command_title"),
											JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		final City city = playerCitySet.getDirectNearestCityFrom(_currentCell.getCellCoord());
		final Coord cityCoord = city.getPosition();
		setCurrentCell(this, cityCoord);
	}

	public final boolean isGameInProgesss()
	{
		return _currentGame != null;
	}

	private class BlueLable extends JLabel
	{

		@Override
		public void paint(final Graphics p_graph)
		{
			final Dimension dim = this.getSize();

			float step = 100f / dim.height;
			float currentStep = 100;
			for (int i = 0; i < dim.height; i++)
			{
				p_graph.setColor(new Color((int) currentStep, (int) currentStep, 254));
				currentStep += step;
				p_graph.drawLine(0, i, dim.width, i);
			}
		}
	}

	public final static FSFrame getFSFrameContainer(final Component p_component)
	{

		boolean isFrameFound = false;
		Container currentParentContainer = p_component.getParent();
		do
		{
			currentParentContainer = currentParentContainer.getParent();
			if (currentParentContainer instanceof FSFrame)
			{
				isFrameFound = true;
			}
		} while (!isFrameFound && currentParentContainer != null);

		return (FSFrame) currentParentContainer;
	}

	/**
	 * Enable of disable all components of the frame, except of menu and
	 * toolbar, during the unit auto move phase.
	 * 
	 * @param p_enabled
	 */
	public final void setMouseEnabled(final boolean p_enabled)
	{
		_verticalToolBar.setMouseEnabled(p_enabled);
		_battleBoard.setMouseEnabled(p_enabled);
		_currentSelectionMgrPnl.setMouseEnabled(p_enabled);
		_toolBar.setMouseEnabled(p_enabled);
	}

	@Override
	public final void setVisible(final boolean isVisible)
	{
		super.setVisible(isVisible);
		if (isVisible)
		{
			HelpMgr helpMgr = null;
			try
			{
				helpMgr = HelpMgr.getOrCreateInstance();
			} catch (IOException ex)
			{
				ex.printStackTrace();
			}
			// Display the help (if required)
			helpMgr.displayHelpIfRequired(this, new String [] {HelpMgr.HELP_WELCOME_CHAPTER});
		}
	}

	/**
	 * executed when a city is conquered by the player IFirstStrikeEvent
	 */
	@Override
	public void eventCityConquered(City p_city, AbstractPlayer p_newPlayer, AbstractPlayer p_oldPlayer)
	{
		if (p_newPlayer instanceof HumanPlayer)
		{
			this.fireConquerCityEvent(p_city);
		} else if (p_oldPlayer instanceof HumanPlayer)
		{
			fireLostCityEvent(p_city, p_oldPlayer);
		}
	}

	@Override
	public void eventPlayerEndTurn(AbstractPlayer p_player)
	{
	}

	@Override
	public void eventPlayerKilled(AbstractPlayer p_player)
	{
		checkGameStatus();
	}

	@Override
	public void eventPlayerStartTurn(AbstractPlayer p_player)
	{
	}

	@Override
	public void eventUnitCreated(AbstractUnit p_unit)
	{
		fireNewUnitEvent(p_unit);
	}

	@Override
	public void eventUnitFinished(AbstractUnit p_unit)
	{
	}

	@Override
	public void eventUnitKilled(AbstractUnit p_unit)
	{
		fireLostUnitEvent(p_unit);
		if (p_unit.getPlayer() instanceof ComputerPlayer)
		{
			checkGameStatus();
		}
	}

	@Override
	public void eventUnitLoaded(AbstractUnit p_loadedUnit, AbstractUnit p_unitContainer)
	{
		if (_currentGame.getCurrentPlayer() instanceof HumanPlayer)
		{
			fireLostUnitEvent(p_loadedUnit);
		}
	}

	@Override
	public void eventUnitMoved(AbstractUnit p_unit, Coord p_newPosition, Coord p_oldPosition)
	{
		fireMoveUnitEvent(p_oldPosition, p_unit);
	}

	@Override
	public void eventUnitUnloaded(AbstractUnit p_unloadedUnit, AbstractUnit p_unitContainer, Coord p_destination)
	{
		final FSMapMgr mapMgr = _currentGame.getMapMgr();
		final FSMap map = mapMgr.getMap();
		final CityList citySet = mapMgr.getCityList();
		// human.moveUnit(p_tank);
		CoordSet coordToRefresh = CoordSet.createInitiazedCoordSet(	p_destination,
																	p_unloadedUnit.getDetectionRange(),
																	map.getWidth(),
																	map.getHeight());
		if (citySet.hasCityAtPosition(p_destination))
		{
			final City city = citySet.getCityAtPosition(p_destination);
			coordToRefresh.addAll(CoordSet
					.createInitiazedCoordSet(p_destination, city.getDetectionRange(), map.getWidth(), map.getHeight()));
		}

		_logger.unitDetail("Tank is unloaded : {0}", p_unloadedUnit);
		// Refresh Battle board
		_battleBoard.refreshBoardCell(coordToRefresh);

		_currentSelectionMgrPnl.updateCurrentCellEvent(null);

		if (_currentGame.getCurrentPlayer() instanceof HumanPlayer)
		{
			fireNewUnitEvent(p_unloadedUnit);
		}
	}

	public void setFocusOnBoard()
	{
		_battleBoard.requestFocus();
	}

	public void startAutoMove()
	{
		_logger.playerAction("Start Auto Move");
		AutoMoveWorker moveThread = new AutoMoveWorker(this);
		moveThread.execute();
	}

	public void pauseAutoMove()
	{
		_logger.playerAction("Auto Move paused");
		try
		{
			Assert.check(!_appConf.toggleAutoMoveActivation(), "Invalid state detected");
		} catch (IOException ex)
		{
			ex.printStackTrace();
		}
	}

}
