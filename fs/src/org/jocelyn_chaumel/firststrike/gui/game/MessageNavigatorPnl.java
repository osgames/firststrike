/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jocelyn_chaumel.firststrike.gui.game;

import java.awt.Color;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.border.EtchedBorder;

import org.jocelyn_chaumel.firststrike.core.map.InvalidMapException;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMessage;
import org.jocelyn_chaumel.firststrike.core.map.scenario.ScenarioMessageList;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
 * 
 * @author jchaumel
 */
public class MessageNavigatorPnl extends javax.swing.JPanel
{

	private final ScenarioMessageList _messageList;
	private final String _mapName;
	private final FSDialog _dlg;
	private int _currentIdx = 0;
	private static final ResourceBundle _bundle = java.util.ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	/**
	 * Creates new form MessageNavigatorPnl
	 */
	public MessageNavigatorPnl(final FSDialog p_dlg, final String p_mapName, final ScenarioMessageList p_scnMsgList)
	{

		_dlg = p_dlg;
		_mapName = p_mapName;
		initComponents();
		_messageList = p_scnMsgList;
		_pictureNorth.setText("");
		_pictureEast.setText("");
		_pictureWest.setText("");

		updateContent();
	}

	private void updateContent()
	{

		_pictureNorth.setIcon(null);
		_pictureNorth.setBorder(null);
		_pictureEast.setIcon(null);
		_pictureEast.setBorder(null);
		_pictureWest.setIcon(null);
		_pictureWest.setBorder(null);

		ScenarioMessage currentMsg = _messageList.get(_currentIdx);
		_dlg.setTitle("[" + currentMsg.getTurn() + "] " + currentMsg.getTitle());
		_dlg.setSize(currentMsg.getDimension());
		_textLbl.setText(currentMsg.getText());
		String orientation = currentMsg.getOrientation();
		final IconFactory iconFactory = IconFactory.getInstance();

		if (ScenarioMessage.NORTH.equals(orientation))
		{
			try
			{
				_pictureNorth.setBorder(new EtchedBorder());
				_pictureNorth.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
				_pictureNorth.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
				final ImageIcon icon = iconFactory.getPictureMap(_mapName, currentMsg.getPictureName());
				_pictureNorth.setIcon(icon);
				_pictureNorth.setOpaque(true);
				_pictureNorth.setBackground(Color.WHITE);
			} catch (InvalidMapException ex)
			{
				_pictureNorth.setText(ex.getMessage());
			}
		}

		if (ScenarioMessage.WEST.equals(orientation))
		{
			try
			{
				_pictureWest.setBorder(new EtchedBorder());
				_pictureWest.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
				_pictureWest.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
				final ImageIcon icon = iconFactory.getPictureMap(_mapName, currentMsg.getPictureName());
				_pictureWest.setIcon(icon);
				_pictureWest.setOpaque(true);
				_pictureWest.setBackground(Color.WHITE);
			} catch (InvalidMapException ex)
			{
				_pictureWest.setText(ex.getMessage());
			}
		}

		if (ScenarioMessage.EAST.equals(orientation))
		{
			try
			{
				_pictureEast.setBorder(new EtchedBorder());
				_pictureEast.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
				_pictureEast.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
				final ImageIcon icon = iconFactory.getPictureMap(_mapName, currentMsg.getPictureName());
				_pictureEast.setIcon(icon);
				_pictureEast.setOpaque(true);
				_pictureEast.setBackground(Color.WHITE);
			} catch (InvalidMapException ex)
			{
				_pictureEast.setText(ex.getMessage());
			}
		}

		_previousBtn.setEnabled(_currentIdx != 0);

		final int msgCount = _messageList.size();
		_nextBtn.setEnabled(_currentIdx + 1 < msgCount);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents()
	{

		_okBtn = new javax.swing.JButton();
		_nextBtn = new javax.swing.JButton();
		_previousBtn = new javax.swing.JButton();
		jSeparator1 = new javax.swing.JSeparator();
		_messagePnl = new javax.swing.JPanel();
		_pictureNorth = new javax.swing.JLabel();
		_pictureEast = new javax.swing.JLabel();
		_pictureWest = new javax.swing.JLabel();
		_textLbl = new javax.swing.JLabel();

		_okBtn.setText(_bundle.getString("fs.common.ok_btn"));
		_okBtn.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				okBtnActionPerformed(evt);
			}
		});

		_nextBtn.setText(_bundle.getString("fs.common.next_btn"));
		_nextBtn.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				nextBtnActionPerformed(evt);
			}
		});

		_previousBtn.setText(_bundle.getString("fs.common.previous_btn"));
		_previousBtn.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				previousBtnActionPerformed(evt);
			}
		});

		_messagePnl.setLayout(new java.awt.BorderLayout());

		_pictureNorth.setText("North");
		_messagePnl.add(_pictureNorth, java.awt.BorderLayout.NORTH);

		_pictureEast.setText("East");
		_messagePnl.add(_pictureEast, java.awt.BorderLayout.EAST);

		_pictureWest.setText("West");
		_messagePnl.add(_pictureWest, java.awt.BorderLayout.WEST);

		_textLbl.setText("Text un text vraiment long Text un text vraiment long Text un text vraiment long Text un text vraiment long Text un text vraiment long Text un text vraiment long Text un text vraiment long Text un text vraiment long Text un text vraiment long Text un text vraiment long Text un text vraiment long Text un text vraiment long Text un text vraiment long Text un text vraiment long ");
		_textLbl.setVerticalAlignment(javax.swing.SwingConstants.TOP);
		_textLbl.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
		_textLbl.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		_textLbl.setFocusable(false);
		_textLbl.setMaximumSize(null);
		_textLbl.setMinimumSize(null);
		_textLbl.setPreferredSize(null);
		_textLbl.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
		_messagePnl.add(_textLbl, java.awt.BorderLayout.CENTER);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(layout.createParallelGroup(Alignment.TRAILING)
						.addComponent(_messagePnl, GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
						.addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(Alignment.LEADING, layout.createSequentialGroup()
							.addComponent(_previousBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(_nextBtn)
							.addPreferredGap(ComponentPlacement.RELATED, 163, Short.MAX_VALUE)
							.addComponent(_okBtn)))
					.addContainerGap())
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(_messagePnl, GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, 5, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(_previousBtn)
						.addComponent(_nextBtn)
						.addComponent(_okBtn))
					.addContainerGap())
		);
		this.setLayout(layout);
	}// </editor-fold>//GEN-END:initComponents

	private void previousBtnActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_previousBtnActionPerformed
		--_currentIdx;
		updateContent();
	}// GEN-LAST:event_previousBtnActionPerformed

	private void nextBtnActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_nextBtnActionPerformed
		++_currentIdx;
		updateContent();
	}// GEN-LAST:event_nextBtnActionPerformed

	private void okBtnActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_okBtnActionPerformed
		_dlg.setVisible(false);
	}// GEN-LAST:event_okBtnActionPerformed
	private javax.swing.JPanel _messagePnl;
	private javax.swing.JButton _nextBtn;
	private javax.swing.JButton _okBtn;
	private javax.swing.JLabel _pictureEast;
	private javax.swing.JLabel _pictureNorth;
	private javax.swing.JLabel _pictureWest;
	private javax.swing.JButton _previousBtn;
	private javax.swing.JLabel _textLbl;
	private javax.swing.JSeparator jSeparator1;
	// End of variables declaration//GEN-END:variables
}
