package org.jocelyn_chaumel.firststrike.gui.game.menu.newgame;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.jocelyn_chaumel.tools.data_type.Coord;

public class CoordListCellRenderer implements ListCellRenderer
{

	@Override
	public Component getListCellRendererComponent(	JList p_list,
													Object p_value,
													int p_index,
													boolean p_isSelected,
													boolean p_cellHasFocus)
	{
		final Coord coord = (Coord) p_value;
		final String text;
		if (coord == null)
		{
			text = "";
		} else
		{
			text = coord.toStringLight();
		}
		final JLabel label = new JLabel(text);
		return label;
	}

}
