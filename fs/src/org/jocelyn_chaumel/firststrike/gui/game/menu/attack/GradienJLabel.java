package org.jocelyn_chaumel.firststrike.gui.game.menu.attack;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JLabel;

import org.jocelyn_chaumel.tools.debugging.Assert;

public class GradienJLabel extends JLabel
{
	private final Color _startColor;
	private final Color _endColor;
	private float _currentPercent;
	
	public GradienJLabel(final Color p_startColor, final Color p_endColor, final float p_percent)
	{
		Assert.preconditionNotNull(p_startColor, "Start Color");
		Assert.preconditionNotNull(p_endColor, "End Color");
		Assert.precondition(0.00f >= p_percent && p_percent <= 1.00f, "Invalid percent");
		
		_startColor = p_startColor;
		_endColor = p_endColor;
		_currentPercent = p_percent;
	}
	
	public void setText(final String p_string, final float p_percent)
	{
		Assert.precondition(0.00f >= p_percent && p_percent <= 1.00f, "Invalid percent");
		_currentPercent = p_percent;
		super.setText(p_string);
	}
	
	@Override
	public final void paint(final Graphics p_graphics)
	{
		super.paint(p_graphics);
		final Dimension dimension = this.getSize();

		int width = (int) dimension.getWidth();
		int height = (int) dimension.getHeight();

		float startRed = _startColor.getRed() / 255f;
		float startBlue = _startColor.getBlue() / 255f;
		float startGreen = _startColor.getGreen() / 255f;
		float endRed = _endColor.getRed() / 255f;
		float endBlue = _endColor.getBlue() / 255f;
		float endGreen = _endColor.getGreen() / 255f;
		float redStep = (endRed - startRed) / width;
		float blueStep = (endBlue - startBlue) / width;
		float greenStep = (endGreen - startGreen) / width;

		for (int i = 0; i < width; i++)
		{
			p_graphics.setColor(new Color(startRed, startGreen, startBlue));
			p_graphics.drawLine(i, 0, i, height);
			startRed += redStep;
			startGreen += greenStep;
			startBlue += blueStep;
		}
		int textWidth = p_graphics.getFontMetrics().stringWidth(getText());
		int startPos = (width - textWidth) / 2;
		p_graphics.setColor(Color.WHITE);
		p_graphics.drawString(getText(), startPos, height - 2);
		
		p_graphics.setColor(Color.BLACK);
		p_graphics.drawString(getText(), startPos + 1, height - 3);
	}

}
