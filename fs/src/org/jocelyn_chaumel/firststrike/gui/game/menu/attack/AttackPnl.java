package org.jocelyn_chaumel.firststrike.gui.game.menu.attack;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.CombatResultCst;
import org.jocelyn_chaumel.firststrike.data_type.SingleAttackResult;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic.UnitPickerDlg;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.exception.InvalidResultException;

public class AttackPnl extends JPanel
{
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private JLabel _unit1HitPoints;
	private JLabel _unit1MovePoints;
	private JLabel _unit1FuelPoints;
	private JLabel _unit1Lbl;
	private JLabel _unit2Lbl;
	private JLabel _unit1AttackLbl;
	private JLabel _unit2DefenceLbl;
	private final Coord _coord;

	private AbstractUnit _unit1;
	private AbstractUnit _unit2;
	private JButton _unitPickerBtn;
	private JButton _attackBtn;
	private IconFactory _factory = IconFactory.getInstance();
	private final FSFrame _frame;
	private final JDialog _dialog;

	public AttackPnl()
	{
		initComponents();
		_coord = null;
		_frame = null;
		_dialog = null;
	}

	/**
	 * Create the panel.
	 */
	public AttackPnl(final FSFrame p_frame, final JDialog p_dialog, final AbstractUnit p_unit, final Coord p_coord)
	{
		initComponents();
		_unit1 = p_unit;

		_coord = p_coord;
		_frame = p_frame;
		_dialog = p_dialog;
		_unit1Lbl.setIcon(_factory.getUnitIconByType(	_unit1.getUnitType(),
														_unit1.getPlayer().getPlayerId(),
														IconFactory.SIZE_DOUBLE_SIZE, _unit1.isLiving()));
		refreshUnit1();
		final AbstractUnitList enemyList = p_unit.getPlayer().getEnemySet().getUnitAt(p_coord);
		_unit1Lbl.setText("#" + _unit1.getId());
		if (enemyList.size() == 1)
		{
			_unitPickerBtn.setEnabled(false);
			_unit2 = enemyList.get(0);
			_attackBtn.setText(getNbAttackString(_unit1));
			_attackBtn.setEnabled(true);
			refreshUnit2();
		} else
		{
			_attackBtn.setText("N/A");
			_attackBtn.setEnabled(false);
			_unit2 = null;
			refreshUnit2();
		}
	}

	private void initComponents()
	{
		setLayout(new MigLayout("", "[202.00,center]", "[][][grow]"));

		JLabel lblAttackYourEnemy = new JLabel("Attack your enemy");

		JPanel mainPanel = new JPanel(new MigLayout("", "[25.00][80.00][80.00]", "[grow][][][][]"));
		_unit1Lbl = new JLabel("# 2");
		_unit1Lbl.setIcon(new ImageIcon(AttackPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_40x40/unit/fighter_p1.gif")));

		_unit2Lbl = new JLabel("# 15");
		_unit2Lbl.setIcon(new ImageIcon(AttackPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_40x40/unit/fighter_p2.gif")));

		_unit1HitPoints = new JLabel("120");
		_unit1HitPoints.setBackground(Color.GREEN);
		_unit1HitPoints.setOpaque(true);
		_unit1HitPoints.setBorder(new LineBorder(Color.GRAY));
		_unit1HitPoints.setHorizontalTextPosition(SwingConstants.RIGHT);
		_unit1HitPoints.setHorizontalAlignment(SwingConstants.CENTER);
		_unit1HitPoints.setFont(new Font("Tahoma", Font.PLAIN, 8));

		_unit1MovePoints = new JLabel("10");
		_unit1MovePoints.setBackground(new Color(0, 255, 255));
		_unit1MovePoints.setOpaque(true);
		_unit1MovePoints.setBorder(new LineBorder(Color.GRAY));
		_unit1MovePoints.setHorizontalTextPosition(SwingConstants.RIGHT);
		_unit1MovePoints.setHorizontalAlignment(SwingConstants.CENTER);
		_unit1MovePoints.setFont(new Font("Tahoma", Font.PLAIN, 8));

		_unit1FuelPoints = new JLabel("10");
		_unit1FuelPoints.setBackground(new Color(147, 112, 219));
		_unit1FuelPoints.setOpaque(true);
		_unit1FuelPoints.setBorder(new LineBorder(Color.GRAY));
		_unit1FuelPoints.setHorizontalTextPosition(SwingConstants.RIGHT);
		_unit1FuelPoints.setHorizontalAlignment(SwingConstants.CENTER);
		_unit1FuelPoints.setFont(new Font("Tahoma", Font.PLAIN, 8));

		final JLabel moveLbl = new JLabel();
		moveLbl.setIcon(new ImageIcon(AttackPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/footprint.png")));
		moveLbl.setToolTipText(_bundle.getString("fs.attack.move_point"));
		final JLabel hpLbl = new JLabel();
		hpLbl.setIcon(new ImageIcon(AttackPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/shield.png")));
		hpLbl.setToolTipText(_bundle.getString("fs.attack.hit_point"));
		final JLabel fuelLbl = new JLabel();
		fuelLbl.setIcon(new ImageIcon(AttackPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/fuel.png")));
		fuelLbl.setToolTipText(_bundle.getString("fs.attack.fuel_point"));

		JLabel unit2HitPointsLbl = new JLabel("?");
		unit2HitPointsLbl.setOpaque(true);
		unit2HitPointsLbl.setBackground(Color.WHITE);
		unit2HitPointsLbl.setBorder(new LineBorder(Color.GRAY));
		unit2HitPointsLbl.setHorizontalAlignment(SwingConstants.CENTER);
		unit2HitPointsLbl.setHorizontalTextPosition(SwingConstants.CENTER);
		unit2HitPointsLbl.setFont(new Font("Tahoma", Font.PLAIN, 8));

		JLabel unit2MovePointsLbl = new JLabel("?");
		unit2MovePointsLbl.setOpaque(true);
		unit2MovePointsLbl.setBackground(Color.WHITE);
		unit2MovePointsLbl.setBorder(new LineBorder(Color.GRAY));
		unit2MovePointsLbl.setHorizontalAlignment(SwingConstants.CENTER);
		unit2MovePointsLbl.setHorizontalTextPosition(SwingConstants.CENTER);
		unit2MovePointsLbl.setFont(new Font("Tahoma", Font.PLAIN, 8));

		JLabel unit2FuelPointsLbl = new JLabel("?");
		unit2FuelPointsLbl.setOpaque(true);
		unit2FuelPointsLbl.setBackground(Color.WHITE);
		unit2FuelPointsLbl.setBorder(new LineBorder(Color.GRAY));
		unit2FuelPointsLbl.setHorizontalAlignment(SwingConstants.CENTER);
		unit2FuelPointsLbl.setHorizontalTextPosition(SwingConstants.CENTER);
		unit2FuelPointsLbl.setFont(new Font("Tahoma", Font.PLAIN, 8));

		_unit1AttackLbl = new JLabel();
		_unit1AttackLbl.setIcon(new ImageIcon(AttackPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_attack_on.png")));

		_unit2DefenceLbl = new JLabel();
		_unit2DefenceLbl.setIcon(new ImageIcon(AttackPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_attack_on.png")));

		mainPanel.add(hpLbl, "cell 0 1");

		mainPanel.add(moveLbl, "cell 0 2,alignx center");
		mainPanel.add(_unit1Lbl, "cell 1 0,alignx center");
		mainPanel.add(_unit1HitPoints, "cell 1 1,growx");
		mainPanel.add(_unit1MovePoints, "cell 1 2,growx");

		mainPanel.add(fuelLbl, "cell 0 3");
		mainPanel.add(_unit1FuelPoints, "cell 1 3,growx");
		mainPanel.add(_unit1AttackLbl, "cell 1 4,alignx center");
		mainPanel.add(_unit2Lbl, "cell 2 0,alignx center");
		mainPanel.add(unit2HitPointsLbl, "cell 2 1,growx");
		mainPanel.add(unit2MovePointsLbl, "cell 2 2,growx");
		mainPanel.add(unit2FuelPointsLbl, "cell 2 3,growx");
		mainPanel.add(_unit2DefenceLbl, "cell 2 4,alignx center");
		JPanel buttonPanel = new JPanel();
		add(lblAttackYourEnemy, "cell 0 0,alignx left");
		add(mainPanel, "cell 0 1");
		add(buttonPanel, "cell 0 2,alignx right,growy");

		_unitPickerBtn = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				// TODO BZ Ajuster la coloration des caractéristiques de
				// l'unité
				final AbstractUnitList enemyList = _unit1.getPlayer().getEnemySet()
						.getUnitAt(_coord);
				final UnitPickerDlg piker = new UnitPickerDlg(_frame, enemyList, _bundle.getString("fs.unit_picker.select_enemy"));
				piker.pack();
				piker.setVisible(true);
				final AbstractUnit selectedEnemy = piker.getSelectedUnit();

				if (selectedEnemy != null)
				{
					_unit1AttackLbl.setText("");
					_unit2DefenceLbl.setText("");
					_unit2 = selectedEnemy;
					refreshUnit2();
					if (_unit1.getNbAttack() > 0 && _unit1.getMove() > 0)
					{
						_attackBtn.setText(getNbAttackString(_unit1));
						_attackBtn.setEnabled(true);
					}
					
					if (enemyList.size() == 1)
					{
						_unitPickerBtn.setEnabled(false);
					}
				}
			}
		});
		_unitPickerBtn.setMargin(new Insets(3, 10, 4, 10));
		_unitPickerBtn.setIcon(new ImageIcon(AttackPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/target.png")));
		_unitPickerBtn.setToolTipText(_bundle.getString("fs.attack.select_enemy"));
		buttonPanel.add(_unitPickerBtn);

		_attackBtn = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				attack();
			}
		});
		_attackBtn.setFont(new Font("Tahoma", Font.PLAIN, 10));
		_attackBtn.setMargin(new Insets(7, 10, 7, 10));
		_attackBtn.setIcon(new ImageIcon(AttackPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_on.png")));
		_attackBtn.setToolTipText(_bundle.getString("fs.attack.attack_btn"));
		buttonPanel.add(_attackBtn);

		final JButton btnClose = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				close();
			}
		});
		btnClose.setText(_bundle.getString("fs.common.close_btn"));
		btnClose.setMargin(new Insets(5, 14, 6, 14));
		buttonPanel.add(btnClose);
	}

	private void close()
	{
		Container currentContainer = this.getParent();
		while (!(currentContainer instanceof FSDialog))
		{
			currentContainer = currentContainer.getParent();
		}

		currentContainer.setVisible(false);
	}

	private void attack()
	{
		final FirstStrikeGame game = _frame.getGame();
		final SingleAttackResult result = game.performAttackUnitAt(_unit1, _unit2);

		_unit1AttackLbl.setText("" + result.getAttack());
		_unit2DefenceLbl.setText("" + result.getDefense());

		final CombatResultCst combatResult = result.getCombatResult();
		if (combatResult.equals(CombatResultCst.NO_WIN))
		{
			_attackBtn.setText(getNbAttackString(_unit1));
			if (_unit1.getNbAttack() == 0 || _unit1.getMove() == 0)
			{
				_attackBtn.setEnabled(false);
				_unitPickerBtn.setEnabled(false);
			}
		} else if (combatResult.equals(CombatResultCst.ATT_AND_DEF_LOOSE))
		{
			JOptionPane.showMessageDialog(	_dialog,
											_bundle.getString("fs.attack.all_unit_destroyed"),
											_bundle.getString("fs.attack.attack_report"),
											JOptionPane.WARNING_MESSAGE);

			_unit1Lbl.setIcon(_factory.getUnitIconByType(	_unit1.getUnitType(),
															_unit1.getPlayer().getPlayerId(),
															IconFactory.SIZE_DOUBLE_SIZE, _unit1.isLiving()));
			_unit2Lbl.setIcon(_factory.getUnitIconByType(	_unit2.getUnitType(),
															_unit2.getPlayer().getPlayerId(),
															IconFactory.SIZE_DOUBLE_SIZE, _unit2.isLiving()));
			_attackBtn.setText("N/A");
			_attackBtn.setEnabled(false);
			_unitPickerBtn.setEnabled(false);
		} else if (combatResult.equals(CombatResultCst.ATTACKER_WIN))
		{
			_unit2Lbl.setIcon(_factory.getUnitIconByType(	_unit2.getUnitType(),
															_unit2.getPlayer().getPlayerId(),
															IconFactory.SIZE_DOUBLE_SIZE, _unit2.isLiving()));
//			_unit2Lbl.setText("#" + _unit2.getId().getId());
			_attackBtn.setText(getNbAttackString(_unit1));
			_attackBtn.setEnabled(false);
		} else if (combatResult.equals(CombatResultCst.DEFENSER_WIN))
		{
			final String message = _bundle.getString("fs.attack.player_unit_destroyed");
			JOptionPane.showMessageDialog(_dialog, message, "Attack Report", JOptionPane.INFORMATION_MESSAGE);
			displayDestroyedUnit(_unit1Lbl);
			_unit1Lbl.setIcon(_factory.getUnitIconByType(	_unit1.getUnitType(),
															_unit1.getPlayer().getPlayerId(),
															IconFactory.SIZE_DOUBLE_SIZE, _unit1.isLiving()));
			_unit2Lbl.paint(_unit2Lbl.getGraphics());
			_attackBtn.setEnabled(false);
			_attackBtn.setText("N/A");
			_unitPickerBtn.setEnabled(false);
		} else
		{
			throw new InvalidResultException();
		}

		refreshUnit1();
	}

	private void displayDestroyedUnit(final JLabel p_label)
	{
		final String text = p_label.getText();
		p_label.setText("");
		p_label.setOpaque(true);
		p_label.setBackground(Color.red);
		p_label.setText(text); // Pour forcer la maj graphique
	}

	private void refreshUnit1()
	{
		if (_unit1.getHitPoints() == _unit1.getHitPointsCapacity())
		{
			_unit1HitPoints.setText("" + _unit1.getHitPoints());
		} else
		{
			_unit1HitPoints.setText("" + _unit1.getHitPoints() + "/" + _unit1.getHitPointsCapacity());
		}

		if (_unit1.getMove() == _unit1.getMovementCapacity())
		{
			_unit1MovePoints.setText("" + _unit1.getMove());
		} else
		{
			_unit1MovePoints.setText("" + _unit1.getMove() + "/" + _unit1.getMovementCapacity());
		}

		if (_unit1.getFuelCapacity() > 0)
		{
			if (_unit1.getFuel() == _unit1.getFuelCapacity())
			{
				_unit1FuelPoints.setText("" + _unit1.getFuel());
			} else
			{
				_unit1FuelPoints.setText("" + _unit1.getFuel() + "/" + _unit1.getFuelCapacity());
			}
		} else
		{
			_unit1FuelPoints.setText("N/A");
		}

	}

	private String getNbAttackString(final AbstractUnit p_unit)
	{
		final int move = p_unit.getMove();
		final int nbAttack = Math.min(move, p_unit.getNbAttack());
		return "" + nbAttack + "/" + p_unit.getNbAttackCapacity();
	}

	private void refreshUnit2()
	{
		if (_unit2 == null)
		{
			_unit2Lbl.setIcon(null);
			_unit2Lbl.setText("");
			_unit2Lbl.setOpaque(false);
			_unit2Lbl.setBackground(Color.gray);
		} else
		{
			_unit2Lbl.setIcon(_factory.getUnitIconByType(	_unit2.getUnitType(),
															_unit2.getPlayer().getPlayerId(),
															IconFactory.SIZE_DOUBLE_SIZE, _unit2.isLiving()));
			_unit2Lbl.setText("#" + _unit2.getId());
			_unit2Lbl.setOpaque(!_unit2.isLiving());
		}
	}
}
