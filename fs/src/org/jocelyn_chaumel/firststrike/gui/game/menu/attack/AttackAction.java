package org.jocelyn_chaumel.firststrike.gui.game.menu.attack;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;

// TODO CZ V�rifier si cette classe peut �tre supprim�e?
public final class AttackAction extends AbstractAction
{
	private final AbstractUnit _enemy;
	private final FSFrame _frame;

	public AttackAction(final FSFrame p_frame, final AbstractUnit p_enemy)
	{
		_frame = p_frame;
		_enemy = p_enemy;
	}

	@Override
	public void actionPerformed(final ActionEvent p_actionEvent)
	{
		_frame.attackEnemy(_enemy.getPosition());
	}

}
