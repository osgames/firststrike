package org.jocelyn_chaumel.firststrike.gui.game.menu.rename;

import java.util.ResourceBundle;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;

public class RenameUnitDlg extends FSDialog
{
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private final AbstractUnit _unit;
	private final RenameUnitPanel _panel;
	private boolean _status;

	public RenameUnitDlg(final FSFrame p_frame, final AbstractUnit p_unit)
	{
		super(p_frame, bundle.getString("fs.rename_unit.title"), true);
		_unit = p_unit;
		_panel = new RenameUnitPanel(p_unit, this);
		getContentPane().add(_panel);
	}

	@Override
	public void setVisible(final boolean p_isVisible)
	{

		super.setVisible(p_isVisible);
		if (!p_isVisible)
		{
			_status = _panel.getStatus();
			if (_panel.getStatus())
			{
				final String newName = _panel.getNewName();
				_unit.setUnitName(newName);

			}
		}
	}

	public final boolean getStatus()
	{
		return _status;
	}
}
