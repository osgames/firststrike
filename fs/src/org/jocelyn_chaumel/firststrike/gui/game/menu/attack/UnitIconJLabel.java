package org.jocelyn_chaumel.firststrike.gui.game.menu.attack;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;

public class UnitIconJLabel extends JLabel
{
	private final IconFactory _iconFactory = IconFactory.getInstance();
	private AbstractUnit _unit;

	public UnitIconJLabel(final AbstractUnit p_unit)
	{
		super();
		_unit = p_unit;
	}

	public void setUnit(final AbstractUnit p_unit)
	{
		_unit = p_unit;
	}

	@Override
	public void paint(final Graphics p_graphs)
	{
		super.paint(p_graphs);
		final UnitTypeCst unitType = _unit.getUnitType();

		ImageIcon icon = _iconFactory.getUnitIconByType(unitType,
														_unit.getPlayer().getPlayerId(),
														IconFactory.SIZE_DOUBLE_SIZE, _unit.isLiving());
		Image image = icon.getImage();
		p_graphs.drawImage(image, 0, 0, null, null);

		if (!_unit.isLiving())
		{
			ImageIcon explosion = _iconFactory.getCommunIcon(IconFactory.COMMON_EXPLOSION);
			Image explosionImage = explosion.getImage();
			p_graphs.drawImage(explosionImage, 10, 0, null, null);
		}
	}
}
