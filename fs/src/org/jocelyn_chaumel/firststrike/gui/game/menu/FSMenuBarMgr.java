package org.jocelyn_chaumel.firststrike.gui.game.menu;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.help.HelpMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.gui.commun.MemoryMgr;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSFrameEvent;
import org.jocelyn_chaumel.firststrike.gui.game.event.IFSFrameEventListener;
import org.jocelyn_chaumel.firststrike.gui.game.menu.about.FSAboutDialog;

public class FSMenuBarMgr implements IFSFrameEventListener
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(FSMenuBarMgr.class.getName());
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");
	private final FSFrame _frame;

	private final JMenuBar _menuBar = new JMenuBar();

	private final JMenu _menuFile = new JMenu(_bundle.getString("fs.menu.file"));

	private final JMenuItem _itemNew;

	private final JMenuItem _itemOpen;

	private final JMenuItem _itemClose;

	private final JMenuItem _itemSave;

	private final JMenuItem _itemSaveAs;

	private JMenuItem _itemGameList[] = null;

	private final JMenuItem _itemExit;

	private final JMenu _menuGame = new JMenu(_bundle.getString("fs.menu.game"));

	private final JMenuItem _itemOldScenarioMsg = new JMenuItem();

	private final JMenu _menuAbout = new JMenu(_bundle.getString("fs.menu.help"));

	private final JMenuItem _itemHelp;

	private final JMenuItem _itemAbout = new JMenuItem();

	private final ApplicationConfigurationMgr _appConf;

	public FSMenuBarMgr(final FSFrame p_frame) throws IOException
	{
		_frame = p_frame;
		_appConf = ApplicationConfigurationMgr.getInstance();
		_frame.addActionListener(this);
		_frame.setJMenuBar(_menuBar);

		// FILE MENU
		// =========

		// New Game
		_itemNew = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent anActionEvent)
			{
				_frame.newGame();
			}

		});
		_itemNew.setText(_bundle.getString("fs.item_menu.new_game"));
		_menuFile.add(_itemNew);

		// Open game
		_itemOpen = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent anActionEvent)
			{
				_frame.openGame(null);
			}
		});
		_itemOpen.setText(_bundle.getString("fs.item_menu.open_game"));
		_menuFile.add(_itemOpen);

		// Close game
		_itemClose = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent anActionEvent)
			{
				_frame.closeGame(true);
			}

		});
		_itemClose.setText(_bundle.getString("fs.item_menu.close_game"));
		_menuFile.add(_itemClose);

		// Separator
		_menuFile.addSeparator();

		// Save game
		_itemSave = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent anActionEvent)
			{
				_frame.saveGame();
			}

		});
		_itemSave.setText(_bundle.getString("fs.item_menu.save_game"));
		_menuFile.add(_itemSave);

		// Save game as
		_itemSaveAs = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent anActionEvent)
			{
				_frame.saveAsGame();
			}

		});
		_itemSaveAs.setText(_bundle.getString("fs.item_menu.save_game_as"));
		_menuFile.add(_itemSaveAs);

		// Separator
		_menuFile.addSeparator();

		// auto Open game 1
		_itemGameList = new JMenuItem[ApplicationConfigurationMgr.NB_LAST_GAME_DIR];
		_itemGameList[0] = new JMenuItem(new OpenRecentGameMenuItem(_frame, this));
		_itemGameList[0].setText("");
		_menuFile.add(_itemGameList[0]);

		// auto Open game 2
		_itemGameList[1] = new JMenuItem(new OpenRecentGameMenuItem(_frame, this));

		_itemGameList[1].setText("");
		_menuFile.add(_itemGameList[1]);

		// auto Open game 3
		_itemGameList[2] = new JMenuItem(new OpenRecentGameMenuItem(_frame, this));
		_itemGameList[2].setText("");
		_menuFile.add(_itemGameList[2]);

		// auto Open game 4
		_itemGameList[3] = new JMenuItem(new OpenRecentGameMenuItem(_frame, this));
		_itemGameList[3].setText("");
		_menuFile.add(_itemGameList[3]);
		updateLastGameList();

		// Separator
		_menuFile.addSeparator();

		// Exit First Strike
		_itemExit = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent p_actionEvent)
			{
				_frame.exitFirstStrike();
			}

		});
		_itemExit.setText(_bundle.getString("fs.item_menu.exit"));
		_menuFile.add(_itemExit);

		_menuBar.add(_menuFile);
		// GAME
		// ==================

		_itemOldScenarioMsg.setAction(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_arg0)
			{
				_frame.displayOldInstantMessage();
			}
		});
		_itemOldScenarioMsg.setText(_bundle.getString("fs.item_menu.old_instant_message"));
		_menuGame.add(_itemOldScenarioMsg);

		_menuBar.add(_menuGame);

		// ? MENU
		// ==========

		// Help ...
		final HelpMgr helpMgr = HelpMgr.getOrCreateInstance();
		_itemHelp = new JMenuItem(new AbstractAction()
		{
				
				@Override
				public void actionPerformed(ActionEvent p_arg0)
				{
					helpMgr.displayHelpOnDemand(p_frame, HelpMgr.HELP_TOPIC_ARRAY);
				}
			});
		_itemHelp.setText(_bundle.getString("fs.item_menu.help"));
		_menuAbout.add(_itemHelp);

		// About ...
		_itemAbout.setAction(new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent anActionEvent)
			{
				FSAboutDialog aboutDlg = null;
				try
				{
					aboutDlg = new FSAboutDialog(_frame);
				} catch (IOException ex)
				{
					return;
				}
				MemoryMgr.log(_logger, "MEMORY STATUS - Displaying About Dlg:");
				aboutDlg.pack();
				aboutDlg.setVisible(true);
				aboutDlg.dispose();
				MemoryMgr.log(_logger, "MEMORY STATUS - About Dlg displayed:");
			}

		});
		_itemAbout.setText(_bundle.getString("fs.item_menu.about"));
		_menuAbout.add(_itemAbout);

		_menuBar.add(_menuAbout);
		closeGame();

	}

	public final JMenuBar getMenuBar()
	{
		return _menuBar;
	}

	private final void loadGame()
	{
		loadGameFileMenu();
		_menuGame.setEnabled(true);
	}

	private final void closeGame()
	{
		closeGameFileMenu();
		_menuGame.setEnabled(false);
	}

	public final void saveAs()
	{
		updateLastGameList();
	}

	private void closeGameFileMenu()
	{
		_itemClose.setEnabled(false);

		_itemSave.setEnabled(false);
		_itemSaveAs.setEnabled(false);
	}

	/**
	 * Update the list of last used games.
	 */
	public void updateLastGameList()
	{
		final ArrayList lastGameList = _appConf.getLastGameList();
		int i = 0;
		final int size = lastGameList.size();
		for (; i < size; i++)
		{
			_itemGameList[i].setVisible(true);
			_itemGameList[i].setText((String) lastGameList.get(i));
		}

		for (; i < ApplicationConfigurationMgr.NB_LAST_GAME_DIR; i++)
		{
			_itemGameList[i].setVisible(false);
		}
	}

	private void loadGameFileMenu()
	{
		_itemClose.setEnabled(true);

		_itemSave.setEnabled(true);
		_itemSaveAs.setEnabled(true);

		updateLastGameList();
	}

	@Override
	public void closeGameEvent(FSFrameEvent p_event)
	{
		closeGame();
	}

	@Override
	public void loadGameEvent(FSFrameEvent p_event)
	{
		loadGame();
	}

	@Override
	public void conquerCityEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostCityEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostUnitEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void updateCurrentCellEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void updateCurrentUnitEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void unitCreatedEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void unitMovedEvent(FSFrameEvent p_event)
	{
		// NOP
	}
}
