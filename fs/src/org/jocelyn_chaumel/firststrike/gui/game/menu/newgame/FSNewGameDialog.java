package org.jocelyn_chaumel.firststrike.gui.game.menu.newgame;

import java.awt.HeadlessException;
import java.util.ResourceBundle;

import javax.swing.JFrame;

import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FSNewGameDialog extends FSDialog
{
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private final FSNewGamePnl _newGamePnl;

	public FSNewGameDialog(final JFrame p_fsFrame, final FSMap p_map) throws HeadlessException
	{
		super(p_fsFrame, bundle.getString("fs.new_game.title"), true);
		_newGamePnl = new FSNewGamePnl(this, p_map);

		getContentPane().add(_newGamePnl);
	}

	public final PlayerInfoList getPlayerInfoList()
	{
		Assert.check(getWizardStatus() == FSDialog.WIZARD_STATUS_NEXT, "Invalid status");
		return _newGamePnl.getInfoPlayerList();
	}

}
