package org.jocelyn_chaumel.firststrike.gui.game.menu.city;

import java.util.ResourceBundle;

import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;

public class CityDialog extends FSDialog
{
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private CityPanelPnl _cityPanel;
	private final City _city;
	private boolean _updatedPanel = false;

	public CityDialog(final FSFrame p_frame, final City p_city)
	{
		super(p_frame, bundle.getString("fs.city.title"), true);
		_cityPanel = new CityPanelPnl(this, p_city);
		_city = p_city;

		this.getContentPane().add(_cityPanel);
	}

	@Override
	public void setVisible(final boolean p_isVisible)
	{
		if (p_isVisible)
		{
			// NOP
		} else
		{
			final boolean status = _cityPanel.getStatus();
			if (status)
			{
				final UnitTypeCst oldUnitTypeCst = _city.getProductionType();
				final UnitTypeCst newUnitTypeCst = _cityPanel.getUnitType();
				if (!newUnitTypeCst.equals(oldUnitTypeCst) && !UnitTypeCst.NULL_TYPE.equals(newUnitTypeCst))
				{
					_city.setProductionType(newUnitTypeCst);
					_updatedPanel = true;
				}

				final boolean oldRecursiveBuildOption = _city.isContinueProduction();
				final boolean newRecursiveBuildOption = _cityPanel.getRecurciveBuildOption();
				if (oldRecursiveBuildOption != newRecursiveBuildOption)
				{
					_city.setContinueProduction(newRecursiveBuildOption);
					_updatedPanel = true;
				}
			}
		}
		super.setVisible(p_isVisible);
	}

	public final boolean isUpdateNeeded()
	{
		return _updatedPanel;
	}
}
