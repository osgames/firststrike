/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * FSNewGamePnl.java
 *
 * Created on 21 déc. 2008, 16:53:30
 */

package org.jocelyn_chaumel.firststrike.gui.game.menu.newgame;

import java.util.Collections;
import java.util.Iterator;
import java.util.ResourceBundle;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.firststrike.core.player.cst.PlayerTypeCst;
import org.jocelyn_chaumel.firststrike.data_type.PlayerInfo;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * 
 * @author Client
 */
public class FSNewGamePnl extends javax.swing.JPanel
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(FSNewGamePnl.class.getName());
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private final FSDialog _parent;
	private FSMap _map = null;
	private CoordList _cityCoordList = null;
	private String _easy = bundle.getString("fs.new_game.easy_level");
	private String _normal = bundle.getString("fs.new_game.normal_level");
	private String _hard = bundle.getString("fs.new_game.hard_level");

	/** Creates new form FSNewGamePnl */
	public FSNewGamePnl()
	{
		initComponents();

		_parent = null;
	}

	public FSNewGamePnl(final FSDialog p_parent, final FSMap p_map)
	{
		_parent = p_parent;
		_map = p_map;
		initComponents();
		loadMapAndInitList();

		_okBtn.setEnabled(true);
		_playerLevelCbx.setEnabled(true);
		_playerLocationCbx.setEnabled(false);
		_playerLocationCbx.setRenderer(new CoordListCellRenderer());
		_playerRandomLocRadio.setEnabled(true);
		_playerSpecifiedLocRadio.setEnabled(true);
		DefaultComboBoxModel model = (DefaultComboBoxModel) _playerLevelCbx.getModel();
		model.removeAllElements();
		model.addElement(_easy);
		model.addElement(_normal);
		model.addElement(_hard);
		_playerLevelCbx.setSelectedIndex(1);

		_computerLevelCbx.setEnabled(true);
		_computerLocationCbx.setEnabled(false);
		_computerLocationCbx.setRenderer(new CoordListCellRenderer());
		_computerRandomLocRadio.setEnabled(true);
		_computerSpecifiedLocRadio.setEnabled(true);
		model = (DefaultComboBoxModel) _computerLevelCbx.getModel();
		model.removeAllElements();
		model.addElement(_easy);
		model.addElement(_normal);
		model.addElement(_hard);
		_computerLevelCbx.setSelectedIndex(1);
	}

	public final PlayerInfoList getInfoPlayerList()
	{
		Assert.check(	_parent.getWizardStatus() == FSDialog.WIZARD_STATUS_NEXT,
						"Invalid new game.  The map can't be retrieved");

		Coord playerLocation = null;
		if (_playerSpecifiedLocRadio.isSelected())
		{
			playerLocation = (Coord) _playerLocationCbx.getSelectedItem();
		} else
		{
			playerLocation = (Coord) FSRandom.getInstance().getRandomObjectFromTheList(_cityCoordList);
			_cityCoordList.remove(playerLocation);
		}

		Coord computerLocation = null;
		if (_computerSpecifiedLocRadio.isSelected())
		{
			computerLocation = (Coord) _computerLocationCbx.getSelectedItem();
		} else
		{
			computerLocation = (Coord) FSRandom.getInstance().getRandomObjectFromTheList(_cityCoordList);
		}

		final PlayerInfoList playerInfoList = new PlayerInfoList();
		String levelStr = (String) _playerLevelCbx.getSelectedItem();
		PlayerLevelCst level = getLevelFromLabel(levelStr);
		PlayerInfo playerInfo = new PlayerInfo(PlayerTypeCst.HUMAN, playerLocation, level);
		playerInfoList.add(playerInfo);

		levelStr = (String) _computerLevelCbx.getSelectedItem();
		level = getLevelFromLabel(levelStr);
		playerInfo = new PlayerInfo(PlayerTypeCst.COMPUTER, computerLocation, level);
		playerInfoList.add(playerInfo);

		return playerInfoList;
	}

	private PlayerLevelCst getLevelFromLabel(final String p_levelStr)
	{
		if (p_levelStr.equals(_easy))
		{
			return PlayerLevelCst.EASY_LEVEL;
		} else if (p_levelStr.equals(_normal))
		{
			return PlayerLevelCst.NORMAL_LEVEL;
		} else if (p_levelStr.equals(_hard))
		{
			return PlayerLevelCst.HARD_LEVEL;
		}

		throw new FSFatalException("Unsupported level " + p_levelStr);
	}

	private boolean loadMapAndInitList()
	{

		_cityCoordList = new CoordList(_map.getCityList().getCoordSet());
		Collections.sort(_cityCoordList);
		DefaultComboBoxModel playerModel = (DefaultComboBoxModel) _playerLocationCbx.getModel();
		if (playerModel != null)
		{
			playerModel.removeAllElements();
		} else
		{
			playerModel = new DefaultComboBoxModel();
		}

		fillModel(playerModel);
		_playerRandomLocRadio.setSelected(true);
		_playerLocationCbx.setEnabled(false);
		_playerLocationCbx.setSelectedIndex(-1);
		_playerLevelCbx.setSelectedIndex(1);
		_playerLevelCbx.setEnabled(true);
		_playerRandomLocRadio.setEnabled(true);
		_playerSpecifiedLocRadio.setEnabled(true);

		DefaultComboBoxModel computerModel = (DefaultComboBoxModel) _computerLocationCbx.getModel();
		if (computerModel != null)
		{
			computerModel.removeAllElements();
		} else
		{
			computerModel = new DefaultComboBoxModel();
		}
		fillModel(computerModel);

		_computerRandomLocRadio.setSelected(true);
		_computerLocationCbx.setEnabled(false);
		_computerLocationCbx.setSelectedIndex(-1);
		_computerLevelCbx.setSelectedIndex(1);
		_computerLevelCbx.setEnabled(true);
		_computerRandomLocRadio.setEnabled(true);
		_computerSpecifiedLocRadio.setEnabled(true);

		_okBtn.setEnabled(true);

		return true;
	}

	private final void fillModel(final DefaultComboBoxModel p_model)
	{
		final Iterator iter = _cityCoordList.iterator();
		Coord currentCoord;
		while (iter.hasNext())
		{
			currentCoord = (Coord) iter.next();
			p_model.addElement(currentCoord);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents()
	{

		_playerCbxGroup = new javax.swing.ButtonGroup();
		_computerCbxGroup = new javax.swing.ButtonGroup();
		_playerLevelCbx = new javax.swing.JComboBox();
		jSeparator1 = new javax.swing.JSeparator();
		javax.swing.JLabel _playerLevelLbl = new javax.swing.JLabel();
		javax.swing.JLabel playerDefinitionLbl = new javax.swing.JLabel();
		javax.swing.JLabel computerDefinitionLbl = new javax.swing.JLabel();
		javax.swing.JLabel playerStartLocationLbl = new javax.swing.JLabel();
		_playerRandomLocRadio = new javax.swing.JRadioButton();
		_playerSpecifiedLocRadio = new javax.swing.JRadioButton();
		_playerLocationCbx = new javax.swing.JComboBox();
		_computerLevelLbl = new javax.swing.JLabel();
		_computerLevelCbx = new javax.swing.JComboBox();
		javax.swing.JLabel computerStartLocationLbl = new javax.swing.JLabel();
		_computerRandomLocRadio = new javax.swing.JRadioButton();
		_computerSpecifiedLocRadio = new javax.swing.JRadioButton();
		_computerLocationCbx = new javax.swing.JComboBox();
		_cancelBtn = new javax.swing.JButton();
		_okBtn = new javax.swing.JButton();
		jSeparator3 = new javax.swing.JSeparator();
		_previousBtn = new javax.swing.JButton();

		_playerLevelCbx.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Easy", "Normal", "Hard" }));
		_playerLevelCbx.setSelectedItem("Normal");

		java.util.ResourceBundle bundle = java.util.ResourceBundle
				.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle"); // NOI18N
		_playerLevelLbl.setText(bundle.getString("fs.new_game.level")); // NOI18N

		playerDefinitionLbl.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
		playerDefinitionLbl.setText(bundle.getString("fs.new_game.player_definition")); // NOI18N

		computerDefinitionLbl.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
		computerDefinitionLbl.setText(bundle.getString("fs.new_game.computer_definition")); // NOI18N

		playerStartLocationLbl.setText(bundle.getString("fs.new_game.location")); // NOI18N

		_playerCbxGroup.add(_playerRandomLocRadio);
		_playerRandomLocRadio.setSelected(true);
		_playerRandomLocRadio.setText(bundle.getString("fs.new_game.random_location")); // NOI18N
		_playerRandomLocRadio.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				playerRandomLocRadioActionPerformed(evt);
			}
		});

		_playerCbxGroup.add(_playerSpecifiedLocRadio);
		_playerSpecifiedLocRadio.setText(bundle.getString("fs.new_game.specific_location")); // NOI18N
		_playerSpecifiedLocRadio.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				playerSpecifiedLocRadioActionPerformed(evt);
			}
		});

		_playerLocationCbx.setEnabled(false);

		_computerLevelLbl.setText(bundle.getString("fs.new_game.level")); // NOI18N

		_computerLevelCbx.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Easy", "Normal", "Hard" }));
		_computerLevelCbx.setSelectedItem("Normal");

		computerStartLocationLbl.setText(bundle.getString("fs.new_game.location")); // NOI18N

		_computerCbxGroup.add(_computerRandomLocRadio);
		_computerRandomLocRadio.setSelected(true);
		_computerRandomLocRadio.setText(bundle.getString("fs.new_game.random_location")); // NOI18N
		_computerRandomLocRadio.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				computerRandomLocRadioActionPerformed(evt);
			}
		});

		_computerCbxGroup.add(_computerSpecifiedLocRadio);
		_computerSpecifiedLocRadio.setText(bundle.getString("fs.new_game.specific_location")); // NOI18N
		_computerSpecifiedLocRadio.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				computerSpecifiedLocRadioActionPerformed(evt);
			}
		});

		_computerLocationCbx.setEnabled(false);

		_cancelBtn.setText(bundle.getString("fs.common.cancel_btn")); // NOI18N
		_cancelBtn.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				cancelBtnActionPerformed(evt);
			}
		});

		_okBtn.setText(bundle.getString("fs.common.ok_btn")); // NOI18N
		_okBtn.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				okBtnActionPerformed(evt);
			}
		});

		_previousBtn.setText("Previous");
		_previousBtn.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				previousBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup()
				.addContainerGap()
				.addGroup(layout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
						.addGroup(	javax.swing.GroupLayout.Alignment.TRAILING,
									layout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE)
											.addComponent(_cancelBtn)
											.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(_previousBtn)
											.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(_okBtn))
						.addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.TRAILING)
						.addGroup(layout
								.createSequentialGroup()
								.addGroup(layout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(playerDefinitionLbl)
										.addComponent(computerDefinitionLbl)
										.addGroup(layout
												.createSequentialGroup()
												.addComponent(_playerLevelLbl)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(	_playerLevelCbx,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																102,
																javax.swing.GroupLayout.PREFERRED_SIZE)
												.addGap(18, 18, 18)
												.addComponent(playerStartLocationLbl)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(_playerRandomLocRadio)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addGroup(layout
														.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(	_playerLocationCbx,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		100,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(_playerSpecifiedLocRadio))))
								.addGap(0, 16, Short.MAX_VALUE))
						.addGroup(	javax.swing.GroupLayout.Alignment.TRAILING,
									layout.createSequentialGroup()
											.addComponent(_computerLevelLbl)
											.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(	_computerLevelCbx,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															102,
															javax.swing.GroupLayout.PREFERRED_SIZE)
											.addGap(18, 18, 18)
											.addComponent(computerStartLocationLbl)
											.addPreferredGap(	javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
											.addComponent(_computerRandomLocRadio)
											.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addGroup(layout
													.createParallelGroup(	javax.swing.GroupLayout.Alignment.LEADING,
																			false)
													.addComponent(	_computerLocationCbx,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	100,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
													.addGroup(layout
															.createSequentialGroup()
															.addComponent(	_computerSpecifiedLocRadio,
																			javax.swing.GroupLayout.DEFAULT_SIZE,
																			97,
																			Short.MAX_VALUE).addGap(19, 19, 19)))))
				.addContainerGap()));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(	javax.swing.GroupLayout.Alignment.TRAILING,
							layout.createSequentialGroup()
									.addContainerGap()
									.addComponent(playerDefinitionLbl)
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(layout
											.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
											.addGroup(layout
													.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(_playerLevelLbl)
													.addComponent(	_playerLevelCbx,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
													.addComponent(playerStartLocationLbl))
											.addGroup(layout
													.createSequentialGroup()
													.addGroup(layout
															.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(_playerRandomLocRadio)
															.addComponent(_playerSpecifiedLocRadio))
													.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
													.addComponent(	_playerLocationCbx,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)))
									.addGap(18, 18, 18)
									.addComponent(	jSeparator1,
													javax.swing.GroupLayout.PREFERRED_SIZE,
													10,
													javax.swing.GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addComponent(computerDefinitionLbl)
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(layout
											.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
											.addComponent(	_computerLevelCbx,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE)
											.addComponent(_computerRandomLocRadio)
											.addComponent(_computerSpecifiedLocRadio).addComponent(_computerLevelLbl)
											.addComponent(computerStartLocationLbl))
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(	_computerLocationCbx,
													javax.swing.GroupLayout.PREFERRED_SIZE,
													javax.swing.GroupLayout.DEFAULT_SIZE,
													javax.swing.GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addComponent(	jSeparator3,
													javax.swing.GroupLayout.PREFERRED_SIZE,
													10,
													javax.swing.GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
											.addComponent(_cancelBtn).addComponent(_okBtn).addComponent(_previousBtn))
									.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
	}// </editor-fold>//GEN-END:initComponents

	private void previousBtnActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_previousBtnActionPerformed
		_parent.setWizardStatus(FSDialog.WIZARD_STATUS_PREVIOUS);
		_parent.setVisible(false);
	}// GEN-LAST:event_previousBtnActionPerformed

	private void playerSpecifiedLocRadioActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_playerSpecifiedLocRadioActionPerformed
		_playerLocationCbx.setEnabled(true);
		_playerLocationCbx.setSelectedIndex(0);
	}// GEN-LAST:event_playerSpecifiedLocRadioActionPerformed

	private void playerRandomLocRadioActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_playerRandomLocRadioActionPerformed
		_playerLocationCbx.setEnabled(false);
		_playerLocationCbx.setSelectedIndex(-1);
	}// GEN-LAST:event_playerRandomLocRadioActionPerformed

	private void computerSpecifiedLocRadioActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_computerSpecifiedLocRadioActionPerformed
		_computerLocationCbx.setEnabled(true);
		_computerLocationCbx.setSelectedIndex(0);
	}// GEN-LAST:event_computerSpecifiedLocRadioActionPerformed

	private void computerRandomLocRadioActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_computerRandomLocRadioActionPerformed
		_computerLocationCbx.setEnabled(false);
		_computerLocationCbx.setSelectedIndex(-1);
	}// GEN-LAST:event_computerRandomLocRadioActionPerformed

	private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_cancelBtnActionPerformed
		_parent.setWizardStatus(FSDialog.WIZARD_STATUS_CANCEL);
		_parent.setVisible(false);
	}// GEN-LAST:event_cancelBtnActionPerformed

	private void okBtnActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_okBtnActionPerformed
		if (_cityCoordList == null)
		{
			final String message = "No map have been selected.\n\nA map must be specified to be able to start a new game";
			JOptionPane.showMessageDialog(	this,
											message,
											bundle.getString("fs.error.invalid_command_title"),
											JOptionPane.WARNING_MESSAGE);
			return;
		}
		final Coord playerLocation;
		if (_playerSpecifiedLocRadio.isSelected())
		{
			if (_playerLocationCbx.getSelectedIndex() == -1)
			{
				final String message = "No player start location has been selected.\n\nAs the radio button 'specified location' has been selected, a start location must by selected in the available list.";
				JOptionPane.showMessageDialog(	this,
												message,
												bundle.getString("fs.error.invalid_command_title"),
												JOptionPane.WARNING_MESSAGE);
				return;
			}
			playerLocation = (Coord) _playerLocationCbx.getSelectedItem();
		} else
		{
			playerLocation = null;
		}

		if (playerLocation == null)
		{
			_parent.setWizardStatus(FSDialog.WIZARD_STATUS_NEXT);
			_parent.setVisible(false);
			return;
		}

		final Coord computerLocation;
		if (_computerSpecifiedLocRadio.isSelected())
		{
			if (_computerLocationCbx.getSelectedIndex() == -1)
			{
				final String message = "No computer start location has been selected.\n\nAs the radio button 'specified location' has been selected, a start location must by selected in the available list.";
				JOptionPane.showMessageDialog(	this,
												message,
												bundle.getString("fs.error.invalid_command_title"),
												JOptionPane.WARNING_MESSAGE);
				return;
			}
			computerLocation = (Coord) _computerLocationCbx.getSelectedItem();
		} else
		{
			computerLocation = null;
		}

		if (!playerLocation.equals(computerLocation))
		{
			_parent.setWizardStatus(FSDialog.WIZARD_STATUS_NEXT);
			_parent.setVisible(false);
			return;
		}
		final String message = bundle.getString("fs.new_game.error.same_start_location");
		JOptionPane.showMessageDialog(	this,
										message,
										bundle.getString("fs.error.invalid_command_title"),
										JOptionPane.WARNING_MESSAGE);
	}// GEN-LAST:event_okBtnActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton _cancelBtn;
	private javax.swing.ButtonGroup _computerCbxGroup;
	private javax.swing.JComboBox _computerLevelCbx;
	private javax.swing.JLabel _computerLevelLbl;
	private javax.swing.JComboBox _computerLocationCbx;
	private javax.swing.JRadioButton _computerRandomLocRadio;
	private javax.swing.JRadioButton _computerSpecifiedLocRadio;
	private javax.swing.JButton _okBtn;
	private javax.swing.ButtonGroup _playerCbxGroup;
	private javax.swing.JComboBox _playerLevelCbx;
	private javax.swing.JComboBox _playerLocationCbx;
	private javax.swing.JRadioButton _playerRandomLocRadio;
	private javax.swing.JRadioButton _playerSpecifiedLocRadio;
	private javax.swing.JButton _previousBtn;
	private javax.swing.JSeparator jSeparator1;
	private javax.swing.JSeparator jSeparator3;
	// End of variables declaration//GEN-END:variables

}
