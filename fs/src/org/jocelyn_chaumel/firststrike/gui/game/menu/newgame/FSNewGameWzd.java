package org.jocelyn_chaumel.firststrike.gui.game.menu.newgame;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.FirstStrikeMgr;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.scenario.MapScenario;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FSNewGameWzd {
	private final JFrame _parent;
	private FSMap _map = null;
	private String _selectedMapName = null;
	private boolean _wizardFinished = false;
	private FirstStrikeGame _fsGame = null;
	private final String _defaultGameName;

	private static final int CHOOSE_MAP = 1;
	private static final int CHOOSE_MAP_OR_SCENARIO = 2;
	private static final int CHOOSE_SCENARIO = 3;
	private static final int CREATE_NEW_GAME = 100;
	private static final int CANCEL_NEW_GAME = -1;
	private static final int WIZARD_FINISHED = Integer.MAX_VALUE;

	private static final FSLogger _logger = FSLoggerManager.getLogger(FSNewGameWzd.class);
	private static final ResourceBundle _bundle = java.util.ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	public FSNewGameWzd(final JFrame p_parent, final String p_defaultGameName) {
		_parent = p_parent;
		_defaultGameName = p_defaultGameName;
	}

	public boolean launch() throws IOException {
		int wizardStep = CHOOSE_MAP;
		final ApplicationConfigurationMgr appConf = ApplicationConfigurationMgr.getInstance();
		final File mapSourceDir = appConf.getMapDir();
		File mapDir = null;
		File mapFile = null;
		int result;
		ArrayList<MapScenario> scenarioList = null;
		while (wizardStep >= CHOOSE_MAP && wizardStep != Integer.MAX_VALUE) {
			// Choose a map
			if (wizardStep == CHOOSE_MAP) {
				final FSChooseMapDialog chooseMapDlg = new FSChooseMapDialog(_parent, FSMapMgr.retrieveMapNameList());
				chooseMapDlg.pack();
				chooseMapDlg.setVisible(true);
				_selectedMapName = chooseMapDlg.getSelectedMapname();
				if (_selectedMapName == null) {
					wizardStep = CANCEL_NEW_GAME;
				} else {
					wizardStep = CHOOSE_MAP_OR_SCENARIO;
				}
				chooseMapDlg.dispose();
			}

			// If scenario exists, map or scenario?
			if (wizardStep == CHOOSE_MAP_OR_SCENARIO) {
				mapDir = new File(mapSourceDir, _selectedMapName);
				mapFile = new File(mapDir, _selectedMapName + FSMapMgr.XML_MAP_FILE_EXTENTION);
				try {
					scenarioList = FSMapMgr.retrieveScenarioList(mapDir);
				} catch (Exception ex) {
					_logger.gameError("Invalid Scenario", ex);
					JOptionPane.showMessageDialog(_parent,
							"Invalid scenario.  Error detected during scenario loading step.", "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				}
				if (scenarioList.size() == 0) {
					wizardStep = CREATE_NEW_GAME;
				} else {
					result = JOptionPane.showConfirmDialog(_parent, _bundle.getString("fs.frame.contains_scenario"),
							_bundle.getString("fs.frame.contains_scenario.title"), JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE);
					if (result == JOptionPane.CANCEL_OPTION) {
						wizardStep = CANCEL_NEW_GAME;
					} else if (result == JOptionPane.YES_OPTION) {
						wizardStep = CHOOSE_SCENARIO;
					} else {
						wizardStep = CREATE_NEW_GAME;
					}
				}
			}

			// Choose a scenario
			if (wizardStep == CHOOSE_SCENARIO) {
				try {
					_map = FSMapMgr.loadMap(mapFile, false);
				} catch (Exception ex) {
					_logger.gameError("Invalid Map", ex);
					JOptionPane.showMessageDialog(_parent, "Invalid Map.  Error detected during scenario loading step.",
							"Error", JOptionPane.ERROR_MESSAGE);
				}
				wizardStep = createNewGameFromScenario(scenarioList);
			}

			// Create a new game
			if (wizardStep == CREATE_NEW_GAME) {
				try {
					_map = FSMapMgr.loadMap(mapFile, true);
				} catch (Exception ex) {
					_logger.gameError("Invalid Map", ex);
					JOptionPane.showMessageDialog(_parent, "Invalid Map.  Error detected during scenario loading step.",
							"Error", JOptionPane.ERROR_MESSAGE);
				}
				wizardStep = createNewGame();
			}

		}
		_wizardFinished = wizardStep == WIZARD_FINISHED;
		return _wizardFinished;
	}

	private final int createNewGameFromScenario(final ArrayList<MapScenario> p_scenarioList) {
		final FSChooseScenarioDialog chooseScenarioDlg = new FSChooseScenarioDialog(_parent, p_scenarioList);
		try {
			chooseScenarioDlg.pack();
			chooseScenarioDlg.setVisible(true);
			final short result = chooseScenarioDlg.getWizardStatus();
			if (result == FSDialog.WIZARD_STATUS_CANCEL) {
				return CANCEL_NEW_GAME;

			} else if (result == FSDialog.WIZARD_STATUS_PREVIOUS) {
				return CHOOSE_MAP;
			}

			final MapScenario scenario = chooseScenarioDlg.getSelectedScenario();
			_fsGame = FirstStrikeMgr.createFirstStrikeGame(scenario, _map);
		} catch (Exception ex) {
			_logger.gameError("Unexpected error occurs", ex);
			JOptionPane.showMessageDialog(_parent,
					"An error occurs during the scenario building.  Unable to play this scenario.\n Error message: "
							+ ex.getMessage(),
					"Error", JOptionPane.ERROR_MESSAGE);
			return CHOOSE_MAP;
		} finally {
			chooseScenarioDlg.dispose();
		}
		return WIZARD_FINISHED;
	}

	private final int createNewGame() throws IOException {
		final FSNewGameDialog newGameDialog = new FSNewGameDialog(_parent, _map);
		newGameDialog.pack();
		newGameDialog.setVisible(true);
		short result = newGameDialog.getWizardStatus();
		if (result == FSDialog.WIZARD_STATUS_CANCEL) {
			newGameDialog.dispose();
			return CANCEL_NEW_GAME;
		} else if (result == FSDialog.WIZARD_STATUS_PREVIOUS) {
			newGameDialog.dispose();
			return CHOOSE_MAP;
		}

		final PlayerInfoList playerInfoList = newGameDialog.getPlayerInfoList();
		newGameDialog.dispose();

		_fsGame = FirstStrikeMgr.createFirstStrikeGame(_defaultGameName, playerInfoList, _map);

		return WIZARD_FINISHED;

	}

	public FirstStrikeGame getGame() {
		Assert.check(_wizardFinished, "invalid wizard status");
		return _fsGame;
	}

}
