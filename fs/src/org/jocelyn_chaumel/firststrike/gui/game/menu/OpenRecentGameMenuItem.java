package org.jocelyn_chaumel.firststrike.gui.game.menu;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class OpenRecentGameMenuItem extends AbstractAction
{
	private final FSFrame _frame;
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");
	private final static FSLogger _logger = FSLoggerManager.getLogger(FSFrame.class.getName());
	private final FSMenuBarMgr _menuBar;

	public OpenRecentGameMenuItem(final FSFrame p_frame, final FSMenuBarMgr p_menuBar)
	{

		_frame = p_frame;
		_menuBar = p_menuBar;
	}

	@Override
	public void actionPerformed(final ActionEvent p_actionEvent)
	{
		final JMenuItem menuItem = (JMenuItem) p_actionEvent.getSource();
		Assert.precondition(menuItem.isVisible(), "A Menu Item activated must be visible");
		final String gameName = menuItem.getText();
		Assert.precondition(gameName.length() > 0, "MenuItem's Text attribut is to short");
		final File gameFile = FileMgr.getFileFromRelativPath(gameName);
		if (gameFile != null && !gameFile.isFile())
		{
			JOptionPane.showInternalMessageDialog(	_frame.getContentPane(),
													bundle.getString("fs.frame.error.file_not_found_error"),
													bundle.getString("fs.error.title"),
													JOptionPane.OK_OPTION);
			ApplicationConfigurationMgr _appConf = ApplicationConfigurationMgr.getInstance();
			_appConf.removeGameFromLastGameList(gameFile);
			if (_appConf.isModified())
			{
				try
				{
					_appConf.save();
				} catch (IOException ex)
				{
					_logger.systemError("Unable to save Application Configuration", ex);
				}
				_menuBar.updateLastGameList();
			}
			return;
		}

		_frame.openGame(gameFile);
	}

}
