package org.jocelyn_chaumel.firststrike.gui.game.menu.newgame;

import java.awt.HeadlessException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JFrame;

import org.jocelyn_chaumel.firststrike.core.map.scenario.MapScenario;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;

public class FSChooseScenarioDialog extends FSDialog
{
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private final FSChooseScenarioPnl _chooseScenarioPnl;

	public FSChooseScenarioDialog(final JFrame p_fsFrame, final ArrayList<MapScenario> p_scenarioList) throws HeadlessException
	{
		super(p_fsFrame, bundle.getString("fs.new_game.title"), true);
		_chooseScenarioPnl = new FSChooseScenarioPnl(this, p_scenarioList);

		getContentPane().add(_chooseScenarioPnl);
	}

	public final MapScenario getSelectedScenario()
	{
		return _chooseScenarioPnl.getSelectedScenario();
	}

}
