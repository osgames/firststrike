package org.jocelyn_chaumel.firststrike.gui.game.menu.city;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import net.miginfocom.swing.MigLayout;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.help.HelpMgr;
import org.jocelyn_chaumel.firststrike.core.unit.UnitCombatInfo;
import org.jocelyn_chaumel.firststrike.core.unit.UnitInfo;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;

public class CityPanelPnl extends JPanel
{
	private JLabel _tankDescLbl;
	private JLabel _artilleryDescLbl;
	private JLabel _fighterDescLbl;
	private JLabel _transportDescLbl;
	private JLabel _carrierDescLbl;
	private JLabel _submarinDescLbl;
	private JLabel _battleshipDescLbl;
	private JLabel _destroyerDescLbl;
	private boolean _status = false;
	private JToggleButton _tankToggle;
	private JToggleButton _artilleryToggle;
	private JToggleButton _fighterToggle;
	private JToggleButton _transportToggle;
	private JToggleButton _destroyerToggle;
	private JToggleButton _battleshipToggle;
	private JToggleButton _submarinToggle;
	private JToggleButton _carrierToggle;
	private final JLabel _slidePictureLbl = new JLabel();
	private JToggleButton _recurciveBuildToggle;
	private City _city;
	private CityDialog _cityDialog;
	private final UnitTypeCst _originalProductionType;
	private final int _originalProductionTime;
	private UnitTypeCst _currentProductionType;
	private JButton _okBtn;
	private JButton _cancelBtn;
	private final ResourceBundle _RS_BUNDLE = ResourceBundle
			.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle");

	public CityPanelPnl(final CityDialog p_dialog, final City p_city)
	{
		_cityDialog = p_dialog;
		_city = p_city;
		_currentProductionType = _originalProductionType = _city.getProductionType();

		final Area area = p_city.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
		JPanel cityDescPnl = initCityDescPanel(p_city.getCell().getDefaultCellName(), p_city.getPosition()
				.toStringLight(), area.getInternalAreaName());
		JPanel unitPictPnl = initUnitPictPanel(calculateUnitPict(_currentProductionType));
		JPanel unitSelectorPnl = initUnitSelectorPanel();
		JPanel bottomBarPnl = initBottomBarPnl();
		initComponents(cityDescPnl, unitPictPnl, unitSelectorPnl, bottomBarPnl);

		if (_city.isUnitUnderConstruction())
		{
			_originalProductionTime = _city.getProductionTime();
		} else
		{
			_originalProductionTime = -1;
		}
		_tankDescLbl.setText(updateDescription(UnitCombatInfo.TANK_COMBAT_INFO, UnitInfo.TANK_INFO));
		_fighterDescLbl.setText(updateDescription(UnitCombatInfo.FIGHTER_COMBAT_INFO, UnitInfo.FIGHTER_INFO));
		_transportDescLbl
				.setText(updateDescription(UnitCombatInfo.TRANSPORT_SHIP_COMBAT_INFO, UnitInfo.TRANSPORT_INFO));
		_destroyerDescLbl.setText(updateDescription(UnitCombatInfo.DESTROYER_COMBAT_INFO, UnitInfo.DESTROYER_INFO));
		_battleshipDescLbl.setText(updateDescription(UnitCombatInfo.BATTLESHIP_COMBAT_INFO, UnitInfo.BATTLESHIP_INFO));
		_artilleryDescLbl.setText(updateDescription(UnitCombatInfo.ARTILLERY_COMBAT_INFO, UnitInfo.ARTILLERY_INFO));
		_submarinToggle.setEnabled(false);
		_carrierToggle.setEnabled(false);

		_recurciveBuildToggle.setSelected(_city.isContinueProduction());

		if (!_city.isPortCity())
		{
			_transportToggle.setEnabled(false);
			_destroyerToggle.setEnabled(false);
			_battleshipToggle.setEnabled(false);
		} else
		{
			_transportToggle.setEnabled(true);
			_destroyerToggle.setEnabled(true);
			_battleshipToggle.setEnabled(true);
		}

		_tankToggle.addActionListener(new ActionListener()
		{
			@Override
			public final void actionPerformed(final ActionEvent e)
			{
				tankToggled();
			}
		});
		_fighterToggle.addActionListener(new ActionListener()
		{
			@Override
			public final void actionPerformed(final ActionEvent e)
			{
				fighterToggled();
			}
		});
		_transportToggle.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(final ActionEvent e)
			{
				transportToggled();
			}
		});
		_artilleryToggle.addActionListener(new ActionListener()
		{
			@Override
			public final void actionPerformed(final ActionEvent e)
			{
				artilleryToggled();
			}
		});
		_destroyerToggle.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(final ActionEvent e)
			{
				destroyerToggled();
			}
		});
		_battleshipToggle.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(final ActionEvent e)
			{
				battleshipToggled();
			}
		});

		_okBtn.setAction(new AbstractAction(_RS_BUNDLE.getString("fs.common.ok_btn"))
		{
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_status = true;
				_cityDialog.setVisible(false);
			}
		});

		_cancelBtn.setAction(new AbstractAction(_RS_BUNDLE.getString("fs.common.cancel_btn"))
		{
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_cityDialog.setVisible(false);
			}
		});

		if (!UnitTypeCst.NULL_TYPE.equals(_originalProductionType))
		{
			selectUnit(_originalProductionType);
		}

	}

	/**
	 * Create the panel.
	 */
	public CityPanelPnl()
	{
		_cityDialog = null;
		_city = null;
		_originalProductionType = UnitTypeCst.NULL_TYPE;
		_originalProductionTime = 0;

		JPanel cityDescPnl = initCityDescPanel("cityName", "pos", "Quebec");
		JPanel unitPictPnl = initUnitPictPanel(calculateUnitPict(UnitTypeCst.NULL_TYPE));
		JPanel unitSelectorPnl = initUnitSelectorPanel();
		JPanel bottomBarPnl = initBottomBarPnl();

		initComponents(cityDescPnl, unitPictPnl, unitSelectorPnl, bottomBarPnl);
		selectUnit(UnitTypeCst.NULL_TYPE);
	}

	private ImageIcon calculateUnitPict(final UnitTypeCst p_unitTypeCst)
	{
		if (UnitTypeCst.TANK.equals(p_unitTypeCst))
		{
			return new ImageIcon(CityPanelPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/city_slide_tank.PNG"));
		} else if (UnitTypeCst.ARTILLERY.equals(p_unitTypeCst))
		{
			return new ImageIcon(CityPanelPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/city_slide_artillery.PNG"));
		} else if (UnitTypeCst.FIGHTER.equals(p_unitTypeCst))
		{
			return new ImageIcon(CityPanelPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/city_slide_fighter.PNG"));
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_unitTypeCst))
		{
			return new ImageIcon(CityPanelPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/city_slide_transport.PNG"));
		} else if (UnitTypeCst.DESTROYER.equals(p_unitTypeCst))
		{
			return new ImageIcon(CityPanelPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/city_slide_destroyer.PNG"));
		} else if (UnitTypeCst.BATTLESHIP.equals(p_unitTypeCst))
		{
			return new ImageIcon(CityPanelPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/city_slide_battleship.PNG"));
		} else if (UnitTypeCst.SUBMARIN.equals(p_unitTypeCst))
		{
			return new ImageIcon(CityPanelPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/city_slide_submarin.PNG"));
		} else if (UnitTypeCst.CARRIER.equals(p_unitTypeCst))
		{
			return new ImageIcon(CityPanelPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/city_slide_carrier.PNG"));
		}

		return new ImageIcon(CityPanelPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/city_slide_default.gif"));
	}

	private void initComponents(final JPanel p_cityDescPnl,
								final JPanel p_unitPictPnl,
								final JPanel p_unitSelectorPnl,
								final JPanel p_bottomBarPnl)
	{
		setLayout(new MigLayout("", "[881.00px]", "[74px][248px][]"));
		add(p_cityDescPnl, "flowx,cell 0 0,alignx center,growy");

		JPanel unitPanel = new JPanel();
		unitPanel.setLayout(new MigLayout("", "[204px][658.00px]", "[379px]"));
		unitPanel.add(p_unitPictPnl, "cell 0 0,alignx left,aligny top");
		unitPanel.add(p_unitSelectorPnl, "cell 1 0,alignx left,aligny center");

		add(unitPanel, "cell 0 1,grow");
		add(p_bottomBarPnl, "cell 0 2,grow");
	}

	private JPanel initUnitPictPanel(final ImageIcon p_sildeImage)
	{
		JPanel slidePictubrePnl = new JPanel();
		slidePictubrePnl.setBackground(Color.WHITE);
		slidePictubrePnl.setBorder(BorderFactory.createEtchedBorder());
		GridBagLayout gbl_slidePictubrePnl = new GridBagLayout();
		gbl_slidePictubrePnl.columnWidths = new int[] { 200, 0 };
		gbl_slidePictubrePnl.rowHeights = new int[] { 375, 0 };
		gbl_slidePictubrePnl.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_slidePictubrePnl.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		slidePictubrePnl.setLayout(gbl_slidePictubrePnl);
		_slidePictureLbl.setIcon(p_sildeImage);
		GridBagConstraints gbc_slidePictureLbl = new GridBagConstraints();
		gbc_slidePictureLbl.gridx = 0;
		gbc_slidePictureLbl.gridy = 0;
		slidePictubrePnl.add(_slidePictureLbl, gbc_slidePictureLbl);

		return slidePictubrePnl;
	}

	private JPanel initCityDescPanel(final String p_cityName, final String p_cityPosition, final String p_areaName)
	{
		final JPanel cityDescPanel = new JPanel();
		cityDescPanel.setLayout(new MigLayout("", "[center][]", "[60.00px:n,center]"));
		JLabel lblCityPicture = new JLabel("");
		cityDescPanel.add(lblCityPicture, "cell 0 0");
		lblCityPicture.setIcon(new ImageIcon(CityPanelPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_40x40/land/city_i1.gif")));

		JPanel panel = new JPanel();
		cityDescPanel.add(panel, "cell 1 0,grow");
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 118, 176, 0 };
		gbl_panel.rowHeights = new int[] { 20, 20, 20, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel cityNameLbl = new JLabel(ResourceBundle
				.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle").getString("fs.city.city_name")); //$NON-NLS-1$ //$NON-NLS-2$
		GridBagConstraints gbc__cityNameLbl = new GridBagConstraints();
		gbc__cityNameLbl.anchor = GridBagConstraints.EAST;
		gbc__cityNameLbl.fill = GridBagConstraints.VERTICAL;
		gbc__cityNameLbl.insets = new Insets(0, 0, 5, 5);
		gbc__cityNameLbl.gridx = 0;
		gbc__cityNameLbl.gridy = 0;
		panel.add(cityNameLbl, gbc__cityNameLbl);

		JLabel cityName = new JLabel(p_cityName);
		cityName.setForeground(Color.BLUE);
		GridBagConstraints gbc__cityName = new GridBagConstraints();
		gbc__cityName.fill = GridBagConstraints.BOTH;
		gbc__cityName.insets = new Insets(0, 0, 5, 0);
		gbc__cityName.gridx = 1;
		gbc__cityName.gridy = 0;
		panel.add(cityName, gbc__cityName);

		JLabel areaNameLbl = new JLabel(ResourceBundle
				.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle").getString("fs.city.area_name")); //$NON-NLS-1$ //$NON-NLS-2$
		GridBagConstraints gbc__areaNameLbl = new GridBagConstraints();
		gbc__areaNameLbl.anchor = GridBagConstraints.EAST;
		gbc__areaNameLbl.fill = GridBagConstraints.VERTICAL;
		gbc__areaNameLbl.insets = new Insets(0, 0, 5, 5);
		gbc__areaNameLbl.gridx = 0;
		gbc__areaNameLbl.gridy = 1;
		panel.add(areaNameLbl, gbc__areaNameLbl);

		JLabel areaName = new JLabel(p_areaName);
		areaName.setForeground(Color.BLUE);
		GridBagConstraints gbc__areaName = new GridBagConstraints();
		gbc__areaName.anchor = GridBagConstraints.WEST;
		gbc__areaName.insets = new Insets(0, 0, 5, 0);
		gbc__areaName.gridx = 1;
		gbc__areaName.gridy = 1;
		panel.add(areaName, gbc__areaName);

		JLabel cityPositionLbl = new JLabel(ResourceBundle
				.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle").getString("fs.city.city_coord")); //$NON-NLS-1$ //$NON-NLS-2$
		GridBagConstraints gbc__cityPositionLbl = new GridBagConstraints();
		gbc__cityPositionLbl.anchor = GridBagConstraints.EAST;
		gbc__cityPositionLbl.fill = GridBagConstraints.VERTICAL;
		gbc__cityPositionLbl.insets = new Insets(0, 0, 0, 5);
		gbc__cityPositionLbl.gridx = 0;
		gbc__cityPositionLbl.gridy = 2;
		panel.add(cityPositionLbl, gbc__cityPositionLbl);

		JLabel _cityPosition = new JLabel(p_cityPosition);
		_cityPosition.setForeground(Color.BLUE);
		GridBagConstraints gbc__cityPosition = new GridBagConstraints();
		gbc__cityPosition.fill = GridBagConstraints.BOTH;
		gbc__cityPosition.gridx = 1;
		gbc__cityPosition.gridy = 2;
		panel.add(_cityPosition, gbc__cityPosition);

		return cityDescPanel;
	}

	private JPanel initUnitSelectorPanel()
	{
		final JPanel unitBodyPanel = new JPanel();
		unitBodyPanel.setLayout(new MigLayout("", "[129.00][525.00,leading]", "[54.00][]"));

		JPanel unitInfoArrayHeaderPnl = new JPanel();
		unitInfoArrayHeaderPnl.setLayout(new MigLayout("", "[]", "[][]"));

		JLabel unitInfoArrayHeader = new JLabel("<html><table style=\"display:table;margin:0px; border-color:black;border-spacing:border-collapse:collapse\"><tr>\r\n<td style=\"border:1px; border-bottom-style: solid;border-right-style: solid\"></td>\r\n<td colspan=\"2\" height=\"20\" width=\"75\" align=\"center\" bgcolor=\"#FDFDFD\"\r\nstyle=\"border:1px; border-bottom-style: dotted; border-right-style: solid; border-top-style: solid;\">Defence</td>\r\n<td colspan=\"2\" width=\"75\" align=\"center\"  bgcolor=\"#FDFDFD\"\r\nstyle=\"border:1px; border-bottom-style: dotted; border-right-style: solid; border-top-style: solid;\">Attack</td>\r\n<td  colspan=\"2\" width=\"75\" align=\"center\"  bgcolor=\"#FDFDFD\"\r\nstyle=\"border:1px; border-bottom-style: dotted; border-right-style: solid; border-top-style: solid;\">Bombardment</td>\r\n<td  colspan=\"2\" style=\"border:1px; border-bottom-style: solid;\"></td>\r\n</tr><tr><td width=\"75\" align=\"center\" bgcolor=\"#FDFDFD\"\r\nstyle=\"border:1px; border-bottom-style: solid; border-right-style: solid;border-left-style: solid\">Time</td>\r\n<td align=\"center\" width=\"25\"\r\nstyle=\"border:1px; border-bottom-style: solid; border-right-style: dotted\">Nb</td>\r\n<td width=\"75\" align=\"center\"\r\nstyle=\"border:1px; border-bottom-style: solid; border-right-style: solid\">Min / Max</td>\r\n<td width=\"25\" align=\"center\"\r\nstyle=\"border:1px; border-bottom-style: solid; border-right-style: dotted\">Nb</td>\r\n<td width=\"75\" align=\"center\"\r\nstyle=\"border:1px; border-bottom-style: solid; border-right-style: solid\">Min / Max</td>\r\n<td width=\"25\" align=\"center\"\r\nstyle=\"border:1px; border-bottom-style: solid; border-right-style: solid\">Nb</td>\r\n<td width=\"75\" align=\"center\"\r\nstyle=\"border:1px; border-bottom-style: solid; border-right-style: solid\">Min / Max</td><td width=\"75\" align=\"center\" bgcolor=\"#FDFDFD\" style=\"border:1px; border-bottom-style: solid; border-right-style: solid\">Hit Points</td><td width=\"75\" align=\"center\" bgcolor=\"#FDFDFD\" style=\"border:1px; border-bottom-style: solid; border-right-style: solid\">Move / fuel</td></tr></table></html>");
		unitInfoArrayHeaderPnl.add(unitInfoArrayHeader, "cell 0 0");
		unitBodyPanel.add(unitInfoArrayHeaderPnl, "cell 1 0,alignx center,aligny bottom");

		JPanel panel = new JPanel();
		unitBodyPanel.add(panel, "cell 0 1,growx,aligny top");

		_tankToggle = new JToggleButton("");
		_tankToggle.setIcon(new ImageIcon(CityPanelPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/tank_p1.png")));

		_fighterToggle = new JToggleButton("");
		_fighterToggle.setIcon(new ImageIcon(CityPanelPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/fighter_p1.png")));

		_transportToggle = new JToggleButton("");
		_transportToggle.setIcon(new ImageIcon(CityPanelPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/transport_p1.png")));

		_artilleryToggle = new JToggleButton("");
		_artilleryToggle.setIcon(new ImageIcon(CityPanelPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/artillery_p1.png")));

		_destroyerToggle = new JToggleButton("");
		_destroyerToggle.setIcon(new ImageIcon(CityPanelPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/destroyer_p1.png")));

		_battleshipToggle = new JToggleButton("");
		_battleshipToggle.setIcon(new ImageIcon(CityPanelPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/battleship_p1.png")));

		_submarinToggle = new JToggleButton("");
		_submarinToggle.setIcon(new ImageIcon(CityPanelPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/submarin_p1.png")));

		_carrierToggle = new JToggleButton("");
		_carrierToggle.setIcon(new ImageIcon(CityPanelPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/carrier_p1.png")));
		panel.setLayout(new MigLayout("", "[115.00px]", "[30px][30px][30px][30px][30px][30px][30px][30px]"));
		panel.add(_tankToggle, "cell 0 0, grow");
		panel.add(_artilleryToggle, "cell 0 1, grow");
		panel.add(_fighterToggle, "cell 0 2, grow");
		panel.add(_transportToggle, "cell 0 3, grow");
		panel.add(_destroyerToggle, "cell 0 4, grow");
		panel.add(_battleshipToggle, "cell 0 5, grow");
		panel.add(_submarinToggle, "cell 0 6, grow");
		panel.add(_carrierToggle, "cell 0 7, grow");

		ButtonGroup btnGroup = new ButtonGroup();
		btnGroup.add(_tankToggle);
		btnGroup.add(_artilleryToggle);
		btnGroup.add(_fighterToggle);
		btnGroup.add(_transportToggle);
		btnGroup.add(_destroyerToggle);
		btnGroup.add(_battleshipToggle);
		btnGroup.add(_submarinToggle);
		btnGroup.add(_carrierToggle);

		JPanel panel_2 = new JPanel();
		unitBodyPanel.add(panel_2, "cell 1 1,alignx center,aligny top");
		panel_2.setLayout(new MigLayout("", "[525px]", "[30px][30px][30px][30px][30px][30px][30px][30px]"));
		_carrierDescLbl = new JLabel("<html><head><style>\r\ntable, { display:table; border-collapse: collapse; border-spacing:0px; margin:0px}\r\ntd {height:20px;text-align:center; border:1px; border-bottom-style: solid; border-top-style: solid}\r\n</style></head><body>\r\n<table><tr ><td width=\"75\" style=\" border-right-style: solid; border-left-style: solid\">11 / 12</td><td width=\"25\" style=\"border-right-style: dotted\">01</td><td  width=\"75\" style=\"border-right-style: solid\">21 / 22</td><td width=\"25\" style=\"border-right-style: dotted\">02</td><td width=\"75\" style=\"border-right-style: solid\">31 / 32</td><td width=\"25\" style=\"border-right-style: dotted\">03</td><td width=\"75\" style=\"border-right-style: solid\"> 41 / 42</td><td width=\"75\" style=\"border-right-style: solid\">04</td><td width=\"75\" style=\"border-right-style: solid\">05</td> </tr></table>\r\n</body>\r\n</html>");

		_tankDescLbl = new JLabel("<html><head><style>\r\ntable, { display:table; border-collapse: collapse; border-spacing:0px; margin:0px}\r\ntd {height:20px;text-align:center; border:1px; border-bottom-style: solid; border-top-style: solid}\r\n</style></head><body>\r\n<table><tr ><td width=\"75\" style=\" border-right-style: solid; border-left-style: solid\">11 / 12</td><td width=\"25\" style=\"border-right-style: dotted\">01</td><td  width=\"75\" style=\"border-right-style: solid\">21 / 22</td><td width=\"25\" style=\"border-right-style: dotted\">02</td><td width=\"75\" style=\"border-right-style: solid\">31 / 32</td><td width=\"25\" style=\"border-right-style: dotted\">03</td><td width=\"75\" style=\"border-right-style: solid\"> 41 / 42</td><td width=\"75\" style=\"border-right-style: solid\">04</td><td width=\"75\" style=\"border-right-style: solid\">05</td> </tr></table>\r\n</body>\r\n</html>");

		panel_2.add(_tankDescLbl, "cell 0 0,alignx left,aligny top");

		_artilleryDescLbl = new JLabel("<html><head><style>\r\ntable, { display:table; border-collapse: collapse; border-spacing:0px; margin:0px}\r\ntd {height:20px;text-align:center; border:1px; border-bottom-style: solid; border-top-style: solid}\r\n</style></head><body>\r\n<table><tr ><td width=\"75\" style=\" border-right-style: solid; border-left-style: solid\">11 / 12</td><td width=\"25\" style=\"border-right-style: dotted\">01</td><td  width=\"75\" style=\"border-right-style: solid\">21 / 22</td><td width=\"25\" style=\"border-right-style: dotted\">02</td><td width=\"75\" style=\"border-right-style: solid\">31 / 32</td><td width=\"25\" style=\"border-right-style: dotted\">03</td><td width=\"75\" style=\"border-right-style: solid\"> 41 / 42</td><td width=\"75\" style=\"border-right-style: solid\">04</td><td width=\"75\" style=\"border-right-style: solid\">05</td> </tr></table>\r\n</body>\r\n</html>");
		panel_2.add(_artilleryDescLbl, "cell 0 1,alignx left,aligny top");

		_fighterDescLbl = new JLabel("<html><head><style>\r\ntable, { display:table; border-collapse: collapse; border-spacing:0px; margin:0px}\r\ntd {height:20px;text-align:center; border:1px; border-bottom-style: solid; border-top-style: solid}\r\n</style></head><body>\r\n<table><tr ><td width=\"75\" style=\" border-right-style: solid; border-left-style: solid\">11 / 12</td><td width=\"25\" style=\"border-right-style: dotted\">01</td><td  width=\"75\" style=\"border-right-style: solid\">21 / 22</td><td width=\"25\" style=\"border-right-style: dotted\">02</td><td width=\"75\" style=\"border-right-style: solid\">31 / 32</td><td width=\"25\" style=\"border-right-style: dotted\">03</td><td width=\"75\" style=\"border-right-style: solid\"> 41 / 42</td><td width=\"75\" style=\"border-right-style: solid\">04</td><td width=\"75\" style=\"border-right-style: solid\">05</td> </tr></table>\r\n</body>\r\n</html>");
		panel_2.add(_fighterDescLbl, "cell 0 2,alignx left,aligny top");

		_transportDescLbl = new JLabel("<html><head><style>\r\ntable, { display:table; border-collapse: collapse; border-spacing:0px; margin:0px}\r\ntd {height:20px;text-align:center; border:1px; border-bottom-style: solid; border-top-style: solid}\r\n</style></head><body>\r\n<table><tr ><td width=\"75\" style=\" border-right-style: solid; border-left-style: solid\">11 / 12</td><td width=\"25\" style=\"border-right-style: dotted\">01</td><td  width=\"75\" style=\"border-right-style: solid\">21 / 22</td><td width=\"25\" style=\"border-right-style: dotted\">02</td><td width=\"75\" style=\"border-right-style: solid\">31 / 32</td><td width=\"25\" style=\"border-right-style: dotted\">03</td><td width=\"75\" style=\"border-right-style: solid\"> 41 / 42</td><td width=\"75\" style=\"border-right-style: solid\">04</td><td width=\"75\" style=\"border-right-style: solid\">05</td> </tr></table>\r\n</body>\r\n</html>");
		panel_2.add(_transportDescLbl, "cell 0 3,alignx left,aligny top");

		_destroyerDescLbl = new JLabel("<html><head><style>\r\ntable, { display:table; border-collapse: collapse; border-spacing:0px; margin:0px}\r\ntd {height:20px;text-align:center; border:1px; border-bottom-style: solid; border-top-style: solid}\r\n</style></head><body>\r\n<table><tr ><td width=\"75\" style=\" border-right-style: solid; border-left-style: solid\">11 / 12</td><td width=\"25\" style=\"border-right-style: dotted\">01</td><td  width=\"75\" style=\"border-right-style: solid\">21 / 22</td><td width=\"25\" style=\"border-right-style: dotted\">02</td><td width=\"75\" style=\"border-right-style: solid\">31 / 32</td><td width=\"25\" style=\"border-right-style: dotted\">03</td><td width=\"75\" style=\"border-right-style: solid\"> 41 / 42</td><td width=\"75\" style=\"border-right-style: solid\">04</td><td width=\"75\" style=\"border-right-style: solid\">05</td> </tr></table>\r\n</body>\r\n</html>");
		panel_2.add(_destroyerDescLbl, "cell 0 4,alignx left,aligny top");

		_battleshipDescLbl = new JLabel("<html><head><style>\r\ntable, { display:table; border-collapse: collapse; border-spacing:0px; margin:0px}\r\ntd {height:20px;text-align:center; border:1px; border-bottom-style: solid; border-top-style: solid}\r\n</style></head><body>\r\n<table><tr ><td width=\"75\" style=\" border-right-style: solid; border-left-style: solid\">11 / 12</td><td width=\"25\" style=\"border-right-style: dotted\">01</td><td  width=\"75\" style=\"border-right-style: solid\">21 / 22</td><td width=\"25\" style=\"border-right-style: dotted\">02</td><td width=\"75\" style=\"border-right-style: solid\">31 / 32</td><td width=\"25\" style=\"border-right-style: dotted\">03</td><td width=\"75\" style=\"border-right-style: solid\"> 41 / 42</td><td width=\"75\" style=\"border-right-style: solid\">04</td><td width=\"75\" style=\"border-right-style: solid\">05</td> </tr></table>\r\n</body>\r\n</html>");
		panel_2.add(_battleshipDescLbl, "cell 0 5,alignx left,aligny top");

		_submarinDescLbl = new JLabel("<html><head><style>\r\ntable, { display:table; border-collapse: collapse; border-spacing:0px; margin:0px}\r\ntd {height:20px;text-align:center; border:1px; border-bottom-style: solid; border-top-style: solid}\r\n</style></head><body>\r\n<table><tr ><td width=\"75\" style=\" border-right-style: solid; border-left-style: solid\">11 / 12</td><td width=\"25\" style=\"border-right-style: dotted\">01</td><td  width=\"75\" style=\"border-right-style: solid\">21 / 22</td><td width=\"25\" style=\"border-right-style: dotted\">02</td><td width=\"75\" style=\"border-right-style: solid\">31 / 32</td><td width=\"25\" style=\"border-right-style: dotted\">03</td><td width=\"75\" style=\"border-right-style: solid\"> 41 / 42</td><td width=\"75\" style=\"border-right-style: solid\">04</td><td width=\"75\" style=\"border-right-style: solid\">05</td> </tr></table>\r\n</body>\r\n</html>");
		panel_2.add(_submarinDescLbl, "cell 0 6,alignx left,aligny top");
		panel_2.add(_carrierDescLbl, "cell 0 7,alignx left,aligny top");

		return unitBodyPanel;
	}

	private JPanel initBottomBarPnl()
	{
		JPanel bottomBarPnl = new JPanel();
		bottomBarPnl.setLayout(new MigLayout("", "[72.00][323.00][grow][][68.00]", "[]"));

		_recurciveBuildToggle = new JToggleButton("");
		_recurciveBuildToggle.setIcon(new ImageIcon(CityPanelPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/recursive.png")));
		bottomBarPnl.add(_recurciveBuildToggle, "cell 0 0,growx");

		JLabel lblAlwaysBuildThis = new JLabel(ResourceBundle
				.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle").getString("fs.city.repeat_build")); //$NON-NLS-1$ //$NON-NLS-2$
		bottomBarPnl.add(lblAlwaysBuildThis, "cell 1 0");

		_okBtn = new JButton(ResourceBundle
				.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle").getString("fs.common.ok_btn")); //$NON-NLS-1$ //$NON-NLS-2$
		bottomBarPnl.add(_okBtn, "cell 3 0");

		_cancelBtn = new JButton(ResourceBundle
				.getBundle("org.jocelyn_chaumel.firststrike.resources.i18n.fs_bundle").getString("fs.common.cancel_btn")); //$NON-NLS-1$ //$NON-NLS-2$
		bottomBarPnl.add(_cancelBtn, "cell 4 0");

		return bottomBarPnl;
	}

	public final boolean getRecurciveBuildOption()
	{
		return _recurciveBuildToggle.isSelected();
	}

	public final boolean getStatus()
	{
		return _status;
	}

	public final UnitTypeCst getUnitType()
	{
		return _currentProductionType;
	}

	private void tankToggled()
	{
		HelpMgr.getInstance().displayHelpIfRequired(_cityDialog, new String[] { HelpMgr.HELP_TANK_CHAPTER });

		unselectUnit(_currentProductionType);
		selectUnit(UnitTypeCst.TANK);
	}

	private void fighterToggled()
	{
		HelpMgr.getInstance().displayHelpIfRequired(_cityDialog, new String[] { HelpMgr.HELP_FIGHTER_CHAPTER });

		unselectUnit(_currentProductionType);
		selectUnit(UnitTypeCst.FIGHTER);
	}

	private void transportToggled()
	{
		HelpMgr.getInstance().displayHelpIfRequired(_cityDialog, new String[] { HelpMgr.HELP_TRANSPORT_SHIP_CHAPTER });

		unselectUnit(_currentProductionType);
		selectUnit(UnitTypeCst.TRANSPORT_SHIP);
	}

	private void destroyerToggled()
	{
		HelpMgr.getInstance().displayHelpIfRequired(_cityDialog, new String[] { HelpMgr.HELP_DESTROYER_CHAPTER });

		unselectUnit(_currentProductionType);
		selectUnit(UnitTypeCst.DESTROYER);
	}

	private void battleshipToggled()
	{
		HelpMgr.getInstance().displayHelpIfRequired(_cityDialog, new String[] { HelpMgr.HELP_BATTLESHIP_CHAPTER });

		unselectUnit(_currentProductionType);
		selectUnit(UnitTypeCst.BATTLESHIP);
	}

	private void artilleryToggled()
	{
		HelpMgr.getInstance().displayHelpIfRequired(_cityDialog, new String[] { HelpMgr.HELP_ARTILLERY_CHAPTER });

		unselectUnit(_currentProductionType);
		selectUnit(UnitTypeCst.ARTILLERY);
	}

	private void selectUnit(final UnitTypeCst p_newUnitType)
	{
		_currentProductionType = p_newUnitType;
		_slidePictureLbl.setIcon(calculateUnitPict(p_newUnitType));
		final String prodTime = City.getProductionTimeByType(p_newUnitType) + "";

		if (UnitTypeCst.TANK.equals(p_newUnitType))
		{
			String description = _tankDescLbl.getText();
			_tankDescLbl.setText(description.replaceFirst("display", "background-color:#A9E2F3; display"));
			if (p_newUnitType == _originalProductionType)
			{
				_tankToggle.setText(" " + _originalProductionTime + "/" + prodTime);
			} else
			{
				_tankToggle.setText(" " + prodTime + "/" + prodTime);
			}
		} else if (UnitTypeCst.FIGHTER.equals(p_newUnitType))
		{
			String description = _fighterDescLbl.getText();
			description = description.replaceAll("display", "background-color:#A9E2F3; display");
			_fighterDescLbl.setText(description);
			if (p_newUnitType == _originalProductionType)
			{
				_fighterToggle.setText(" " + _originalProductionTime + "/" + prodTime);
			} else
			{
				_fighterToggle.setText(" " + prodTime + "/" + prodTime);
			}
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_newUnitType))
		{
			String description = _transportDescLbl.getText();
			_transportDescLbl.setText(description.replaceFirst("display", "background-color:#A9E2F3; display"));
			if (p_newUnitType == _originalProductionType)
			{
				_transportToggle.setText(" " + _originalProductionTime + "/" + prodTime);
			} else
			{
				_transportToggle.setText(" " + prodTime + "/" + prodTime);
			}
		} else if (UnitTypeCst.DESTROYER.equals(p_newUnitType))
		{
			_destroyerDescLbl.setText(_destroyerDescLbl.getText().replaceFirst(	"display",
																				"background-color:#A9E2F3; display"));
			if (p_newUnitType == _originalProductionType)
			{
				_destroyerToggle.setText(" " + _originalProductionTime + "/" + prodTime);
			} else
			{
				_destroyerToggle.setText(" " + prodTime + "/" + prodTime);
			}
		} else if (UnitTypeCst.BATTLESHIP.equals(p_newUnitType))
		{
			_battleshipDescLbl.setText(_battleshipDescLbl.getText()
					.replaceFirst("display", "background-color:#A9E2F3; display"));
			if (p_newUnitType == _originalProductionType)
			{
				_battleshipToggle.setText(" " + _originalProductionTime + "/" + prodTime);
			} else
			{
				_battleshipToggle.setText(" " + prodTime + "/" + prodTime);
			}
		} else if (UnitTypeCst.ARTILLERY.equals(p_newUnitType))
		{
			_artilleryDescLbl.setText(_artilleryDescLbl.getText().replaceFirst(	"display",
																				"background-color:#A9E2F3; display"));
			if (p_newUnitType == _originalProductionType)
			{
				_artilleryToggle.setText(" " + _originalProductionTime + "/" + prodTime);
			} else
			{
				_artilleryToggle.setText(" " + prodTime + "/" + prodTime);
			}
		}

	}

	private void unselectUnit(final UnitTypeCst p_unitType)
	{
		final JLabel unitLbl;
		final JToggleButton unitToggle;
		if (UnitTypeCst.TANK.equals(p_unitType))
		{
			unitLbl = _tankDescLbl;
			unitToggle = _tankToggle;

		} else if (UnitTypeCst.FIGHTER.equals(p_unitType))
		{
			unitLbl = _fighterDescLbl;
			unitToggle = _fighterToggle;
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(p_unitType))
		{
			unitLbl = _transportDescLbl;
			unitToggle = _transportToggle;
		} else if (UnitTypeCst.DESTROYER.equals(p_unitType))
		{
			unitLbl = _destroyerDescLbl;
			unitToggle = _destroyerToggle;
		} else if (UnitTypeCst.BATTLESHIP.equals(p_unitType))
		{
			unitLbl = _battleshipDescLbl;
			unitToggle = _battleshipToggle;
		} else if (UnitTypeCst.ARTILLERY.equals(p_unitType))
		{
			unitLbl = _artilleryDescLbl;
			unitToggle = _artilleryToggle;
		} else if (UnitTypeCst.SUBMARIN.equals(p_unitType))
		{
			unitLbl = _submarinDescLbl;
			unitToggle = _submarinToggle;
		} else if (UnitTypeCst.CARRIER.equals(p_unitType))
		{
			unitLbl = _carrierDescLbl;
			unitToggle = _carrierToggle;
		} else
		{
			return;
		}
		// Remove the color
		final String desc = unitLbl.getText().replaceAll("background-color:#A9E2F3;", "");
		unitLbl.setText(desc);

		// Remove Time indicator on Toggle button
		unitToggle.setText("");

	}

	private String updateDescription(final UnitCombatInfo p_combatInfo, final UnitInfo p_info)
	{
		String description = "<html><head><style>table, { display:table; border-collapse: collapse; border-spacing:0px; margin:0px}\r\ntd {height:20px;text-align:center; border:1px; border-bottom-style: solid; border-top-style: solid}\r\n</style></head><body>\r\n<table><tr ><td width=\"75\" style=\" border-right-style: solid; border-left-style: solid\">11 / 12</td><td width=\"25\" style=\"border-right-style: dotted\">01</td><td  width=\"75\" style=\"border-right-style: solid\">21 / 22</td><td width=\"25\" style=\"border-right-style: dotted\">02</td><td width=\"75\" style=\"border-right-style: solid\">31 / 32</td><td width=\"25\" style=\"border-right-style: dotted\">03</td><td width=\"75\" style=\"border-right-style: solid\"> 41 / 42</td><td width=\"75\" style=\"border-right-style: solid\">04</td><td width=\"75\" style=\"border-right-style: solid\">05</td> </tr></table>\r\n</body>\r\n</html>";
		final String defence = p_combatInfo.getMinDefense() + "/" + p_combatInfo.getMaxDefense();
		final String attack = p_combatInfo.getMinAttack() + "/" + p_combatInfo.getMaxAttack();
		final String bombardment = p_combatInfo.getMinBombardmentAttack() + "/"
				+ p_combatInfo.getMaxBombardmentAttack();
		final String moveAndFuel;
		if (p_info.getMaxFuelCapacity() > 0)
		{
			moveAndFuel = p_info.getMaxMovementCapacity() + "/" + p_info.getMaxFuelCapacity();
		} else
		{
			moveAndFuel = p_info.getMaxMovementCapacity() + "";
		}
		description = description.replaceAll("11 / 12", City.getProductionTimeByType(p_info.getUnitType()) + ""); // Time
		description = description.replaceAll("01", p_combatInfo.getNbDefenseCapacity() + "");
		description = description.replaceAll("21 / 22", defence);
		description = description.replaceAll("02", p_combatInfo.getNbAttackCapacity() + "");
		description = description.replaceAll("31 / 32", attack);
		description = description.replaceAll("03", p_combatInfo.getNbBombardmentCapacity() + "");
		description = description.replaceAll("41 / 42", bombardment);
		description = description.replaceAll("04", p_combatInfo.getHitPointsCapacity() + ""); // Hit
																								// points
		description = description.replaceAll("05", moveAndFuel); // Move and
																	// Fuel

		return description;
	}
}
