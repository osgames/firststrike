package org.jocelyn_chaumel.firststrike.gui.game.menu.newgame;

import java.awt.HeadlessException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JFrame;

import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;

public class FSChooseMapDialog extends FSDialog
{
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private final FSChooseMapPnl _chooseMapPnl;

	public FSChooseMapDialog(final JFrame p_fsFrame, final ArrayList<String> p_mapnameList) throws HeadlessException
	{
		super(p_fsFrame, bundle.getString("fs.new_game.title"), true);
		_chooseMapPnl = new FSChooseMapPnl(this, p_mapnameList);

		getContentPane().add(_chooseMapPnl);
	}

	public String getSelectedMapname()
	{
		return _chooseMapPnl.getSelectedMapname();
	}

}
