package org.jocelyn_chaumel.firststrike.gui.game.menu.attack;

import java.util.ResourceBundle;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class AttackDlg extends FSDialog
{
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");
	
	private final AttackPnl _attackPnl;

	public AttackDlg(final FSFrame p_frame, final AbstractUnit p_unit, final Coord p_target)
	{
		super(p_frame, _bundle.getString("fs.attack.title"), true);
		_attackPnl = new AttackPnl(p_frame, this, p_unit, p_target);
		getContentPane().add(_attackPnl);
	}
}
