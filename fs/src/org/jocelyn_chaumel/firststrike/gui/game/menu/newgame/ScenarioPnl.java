/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jocelyn_chaumel.firststrike.gui.game.menu.newgame;

import javax.swing.JRadioButton;

/**
 * 
 * @author jchaumel
 */
public class ScenarioPnl extends javax.swing.JPanel
{

	/**
	 * Creates new form ScenarioPnl
	 */
	public ScenarioPnl(final String p_idx, final String p_scenarioName, final String p_description, final int p_nbRow)
	{
		initComponents();
		_scenarioNameRadio.setText(p_scenarioName);
		_scenarioNameRadio.setActionCommand(p_idx);
		_descArea.setText(p_description);
		_descArea.setRows(p_nbRow);

	}

	public JRadioButton getRadioButton()
	{
		return _scenarioNameRadio;
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents()
	{

		_scenarioNameRadio = new javax.swing.JRadioButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		_descArea = new javax.swing.JTextArea();

		_scenarioNameRadio.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
		_scenarioNameRadio.setText("jRadioButton1");

		_descArea.setEditable(false);
		_descArea.setColumns(20);
		_descArea.setLineWrap(true);
		_descArea.setRows(5);
		_descArea.setWrapStyleWord(true);
		jScrollPane1.setViewportView(_descArea);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(	javax.swing.GroupLayout.Alignment.TRAILING,
							layout.createSequentialGroup()
									.addGroup(layout
											.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
											.addGroup(	javax.swing.GroupLayout.Alignment.LEADING,
														layout.createSequentialGroup()
																.addContainerGap()
																.addComponent(	_scenarioNameRadio,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE))
											.addGroup(	javax.swing.GroupLayout.Alignment.LEADING,
														layout.createSequentialGroup()
																.addGap(27, 27, 27)
																.addComponent(	jScrollPane1,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				363,
																				Short.MAX_VALUE))).addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup()
				.addContainerGap()
				.addComponent(_scenarioNameRadio)
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
				.addComponent(	jScrollPane1,
								javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE)
				.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
	}// </editor-fold>//GEN-END:initComponents
		// Variables declaration - do not modify//GEN-BEGIN:variables

	private javax.swing.JTextArea _descArea;
	private javax.swing.JRadioButton _scenarioNameRadio;
	private javax.swing.JScrollPane jScrollPane1;
	// End of variables declaration//GEN-END:variables
}
