package org.jocelyn_chaumel.firststrike.gui.game.menu.about;

import java.io.IOException;
import java.util.ResourceBundle;

import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;

public class FSAboutDialog extends FSDialog
{
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");
	private final FSAboutPanel _aboutPnl;

	public FSAboutDialog(final FSFrame p_frame) throws IOException
	{
		super(p_frame, bundle.getString("fs.about.title"), true);
		_aboutPnl = new FSAboutPanel(this);

		getContentPane().add(_aboutPnl);
	}

}
