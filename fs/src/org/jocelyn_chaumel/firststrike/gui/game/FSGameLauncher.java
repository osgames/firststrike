/*
 * Created on Jan 3, 2006
 */
package org.jocelyn_chaumel.firststrike.gui.game;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLevel;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.InvalidMapException;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.tools.Dir;
import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;
import org.jocelyn_chaumel.tools.zip.ZipMgr;

/**
 * @author Jocelyn Chaumel
 */
public final class FSGameLauncher
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(FSGameLauncher.class.getName());
	private static ApplicationConfigurationMgr _confMgr = null;

	// TODO BZ Evolution : Ajouter un niveau de difficult� pour les sc�narii
	// TODO BZ Evolution : Ajouter les points de ralliement

	public static void main(final String[] p_argArray)
	{
		try
		{
			// Locale.setDefault(Locale.FRENCH);
			Locale.setDefault(Locale.ENGLISH);
			if (p_argArray != null && p_argArray.length >= 1)
			{
				File localAppDataDir = new File(p_argArray[0]);
				if (!localAppDataDir.isDirectory())
				{
					System.out.println("The local Application Data Folder is not a valid folder: '" + p_argArray[0]
							+ "'.");
				}
				_confMgr = ApplicationConfigurationMgr.getCreateInstance(localAppDataDir);
			} else
			{
				_confMgr = ApplicationConfigurationMgr.getCreateInstance(FileMgr.getCurrentDir());
			}
		} catch (IOException ex)
		{
			System.out.println("Unable to load FirstStrike.Properties File");
			ex.printStackTrace();
			System.exit(1);
		}
		if (_confMgr.isFSLogsActivation())
		{
			try
			{
				final File logDir = _confMgr.getFSLogsDir();
				FSLoggerManager.setSystemFileLogger(logDir);
			} catch (IOException ex)
			{
				System.out.println("Unable to set log file");
				ex.printStackTrace();
				System.exit(1);
			}
		}

		if (_confMgr.isDebugMode())
		{
			FSLoggerManager.setConsoleLogger(FSLevel.ALL);
		} else
		{
			FSLoggerManager.setConsoleLogger(FSLevel.ALL);
		}

		_logger.system("First Strike is launched");

		_logger.systemAction("First Strike version :" + _confMgr.getFSVersion());
		try
		{
			extractGameFromZipFiles();
			createAndShowLocalGUI();
		} catch (Exception e)
		{
			_logger.systemError("Error:", e);
		}
	}

	private static final void extractGameFromZipFiles()
	{
		final File mapSourceDirFile = _confMgr.getMapDir();
		_logger.systemAction("Extraction of the map zip files from the folder: " + mapSourceDirFile.getAbsolutePath());
		final Dir mapSourceDir = new Dir(mapSourceDirFile);
		final File[] zipFileArray = mapSourceDir.getFileEndingWith(".zip");
		File mapDir;
		String currentZipfilename;
		for (int i = 0; i < zipFileArray.length; i++)
		{
			currentZipfilename = FileMgr.getFileNameWithoutExtention(zipFileArray[i]);
			mapDir = new File(mapSourceDirFile, currentZipfilename);
			if (mapDir.isDirectory())
			{
				_logger.systemSubAction(currentZipfilename + " already unzipped");
				continue;
			}

			try
			{
				ZipMgr.extractZip(zipFileArray[i], mapDir, false);
				_logger.systemSubAction(currentZipfilename + " unzipped successfully");
			} catch (IOException ex)
			{
				_logger.systemWarning(currentZipfilename + " unable to unzip this zipped map.");
			}
		}
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 * 
	 * @throws IOException
	 * 
	 * @throws IOException
	 * @throws SecurityException
	 * @throws IOException
	 * @throws InvalidMapException
	 * @throws InvalidConstantValueException
	 * @throws FileNotFoundException
	 */
	private static void createAndShowLocalGUI() throws IOException
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				IconFactory componentFactory = null;
				try
				{
					componentFactory = IconFactory.createInstance();
				} catch (Exception ex)
				{
					_logger.systemError(ex.getMessage(), ex);
					System.exit(-1);
				}

				JFrame.setDefaultLookAndFeelDecorated(true);
				FSFrame frame = null;
				try
				{
					frame = new FSFrame();
				} catch (IOException ex)
				{
					ex.printStackTrace();
				}
				ImageIcon fsIcon = componentFactory.getCommunIcon(IconFactory.COMMUN_FS_ICON);
				frame.setIconImage(fsIcon.getImage());

				// Set the location and the size of the FS Window
				Dimension screenMaxSize = Toolkit.getDefaultToolkit().getScreenSize();
				Dimension screenSize = _confMgr.getScreenDimension();
				Point screenLocation = null;
				int width, height, x, y;

				if (screenMaxSize.equals(screenSize))
				{
					screenSize = null;
				} else
				{
					screenLocation = _confMgr.getScreenLocation();
				}

				if (screenSize == null)
				{
					width = (int) (screenMaxSize.getWidth() * 0.75);
					height = (int) (screenMaxSize.getHeight() * 0.75);
					screenSize = new Dimension(width, height);

					x = (int) (screenSize.getWidth() * 0.125);
					y = (int) (screenSize.getHeight() * 0.125);
					screenLocation = new Point(x, y);
				}
				System.out.println("Thread name: " + Thread.currentThread().getName());
				// Display the window.
				frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				frame.setSize(screenSize);
				frame.setLocation(screenLocation);
				frame.setVisible(true);
			}
		});
	}
}