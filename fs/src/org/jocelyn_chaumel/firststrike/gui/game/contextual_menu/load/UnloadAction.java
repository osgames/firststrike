package org.jocelyn_chaumel.firststrike.gui.game.contextual_menu.load;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractGroundLoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.gui.game.board.BattleBoard;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class UnloadAction extends AbstractAction
{
	private final BattleBoard _battleBoard;
	private final AbstractGroundLoadableUnit _loadableUnit;
	private final Coord _targetLocation;

	public UnloadAction(final BattleBoard p_battleBoard, final AbstractGroundLoadableUnit p_tank, final Coord p_targetLocation)
	{
		_battleBoard = p_battleBoard;
		_loadableUnit = p_tank;
		_targetLocation = p_targetLocation;
	}

	@Override
	public void actionPerformed(final ActionEvent p_actionEvent)
	{
		_battleBoard.unloadTank(_targetLocation, _loadableUnit);
	}

}
