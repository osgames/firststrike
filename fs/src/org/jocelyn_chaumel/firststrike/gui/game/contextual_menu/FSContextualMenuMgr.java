package org.jocelyn_chaumel.firststrike.gui.game.contextual_menu;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractGroundLoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.ILoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.board.BattleBoard;
import org.jocelyn_chaumel.firststrike.gui.game.contextual_menu.load.LoadAction;
import org.jocelyn_chaumel.firststrike.gui.game.contextual_menu.load.UnloadAction;
import org.jocelyn_chaumel.firststrike.gui.game.menu.attack.AttackAction;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class FSContextualMenuMgr extends JPopupMenu
{
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private final BattleBoard _frame;
	private final JMenuItem _moveItem;
	private final JMenuItem _tracePathItem;
	private final JMenuItem _tracePathAndMoveItem;
	private final JMenuItem _tracePathAndMoveAllItem;
	private final JMenuItem _removePathItem;
	private final JMenuItem _openCityItem;
	private final JMenuItem _bombardmentItem;
	private final JMenu _attackMenu;
	private final JMenu _loadUnitItem;
	private final JMenu _unloadUnitItem;
	private Coord _menuCoord;
	private final IconFactory _iconFactory;

	public FSContextualMenuMgr(final BattleBoard p_frame)
	{

		_frame = p_frame;
		_iconFactory = IconFactory.getInstance();

		_tracePathAndMoveItem = new JMenuItem(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_actionEvent)
			{
				_frame.tracePathAndGo(_menuCoord);

			}
		});
		_tracePathAndMoveItem.setText(bundle.getString("fs.contextual_menu.trace_and_move"));
		add(_tracePathAndMoveItem);

		_tracePathAndMoveAllItem = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent p_actionEvent)
			{
				_frame.tracePathAndGoAll(_menuCoord);
			}

		});
		_tracePathAndMoveAllItem.setText(bundle.getString("fs.contextual_menu.trace_and_move_all_unit"));
		add(_tracePathAndMoveAllItem);

		_removePathItem = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent p_actionEvent)
			{
				_frame.removePath();
			}

		});
		_removePathItem.setText(bundle.getString("fs.contextual_menu.remove_path"));
		add(_removePathItem);

		_tracePathItem = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent p_actionEvent)
			{
				_frame.tracePath(_menuCoord);
			}
		});
		_tracePathItem.setText(bundle.getString("fs.contextual_menu.trace"));
		add(_tracePathItem);

		_moveItem = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_frame.moveUnit(null);
			}
		});
		_moveItem.setText(bundle.getString("fs.contextual_menu.move"));
		add(_moveItem);

		_loadUnitItem = new JMenu(bundle.getString("fs.contextual_menu.load_unit"));
		add(_loadUnitItem);

		_unloadUnitItem = new JMenu(bundle.getString("fs.contextual_menu.unload_unit"));
		add(_unloadUnitItem);

		_attackMenu = new JMenu(bundle.getString("fs.contextual_menu.attack"));
		add(_attackMenu);

		_bombardmentItem = new JMenuItem(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_frame.bombPosition(_menuCoord);

			}
		});
		_bombardmentItem.setText(bundle.getString("fs.contextual_menu.bombardment"));
		add(_bombardmentItem);

		_openCityItem = new JMenuItem(new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent p_actionEvent)
			{
				FSFrame.getFSFrameContainer(_frame).openCity(_menuCoord);

			}
		});
		_openCityItem.setText(bundle.getString("fs.contextual_menu.open_city"));
		add(_openCityItem);
	}

	@Override
	public void show(Component p_invoker, int p_x, int p_y)
	{
		final JTable board = (JTable) p_invoker;
		final Point point = new Point(p_x, p_y);
		final int row = board.rowAtPoint(point);
		final int col = board.columnAtPoint(point);
		_menuCoord = new Coord(col, row);
		if (adjustItemSelection())
		{
			super.show(p_invoker, p_x, p_y);
		}
	}

	private boolean adjustItemSelection()
	{
		final FirstStrikeGame game = _frame.getCurrentGame();
		final HumanPlayer player = (HumanPlayer) game.getCurrentPlayer();
		final AbstractUnitList enemySet = player.getEnemySet();
		if (enemySet.hasUnitAt(_menuCoord))
		{
			return false;
		}
		if (player.getShadowSet().contains(_menuCoord) && !game.getOptionMgr().isDebutMode())
		{
			return false;
		}
		boolean hasAction = false;

		final AbstractUnit currentUnit = _frame.getCurrentUnit();
		if (currentUnit == null)
		{
			return false;
		}
		final AbstractPlayer unitPlayer = currentUnit.getPlayer();
		if (unitPlayer != player)
		{
			return false;
		}
		if (!(currentUnit instanceof Tank))
		{
			final CityList enemyCitySet = game.getMapMgr().getCityList().getEnemyCity(player);
			if (enemyCitySet.hasCityAtPosition(_menuCoord))
			{
				return false;
			}
		}

		// Trace Path & Move | Trace Path | Move
		hasAction = true;
		final Coord unitPosition = currentUnit.getPosition();
		boolean isSameCell = unitPosition.equals(_menuCoord);
		if (!isSameCell)
		{
			_moveItem.setEnabled(false);

			final UnitTypeCst unitType = currentUnit.getUnitType();
			final boolean isValidDest;
			boolean isValidForBombarment = false;
			if (unitType.equals(UnitTypeCst.TANK))
			{
				isValidDest = game.isValidDestinationForGroundUnit(unitPosition, _menuCoord);
			} else if (unitType.equals(UnitTypeCst.FIGHTER))
			{
				isValidDest = game.isValidDestinationForAirUnit(_menuCoord);
			} else if (unitType.equals(UnitTypeCst.DESTROYER) || unitType.equals(UnitTypeCst.TRANSPORT_SHIP)
					|| unitType.equals(UnitTypeCst.BATTLESHIP))
			{
				isValidDest = game.isValidDestinationForSeaUnit(unitPosition, _menuCoord);
			} else
			{
				throw new FSFatalException("Unsupported unit type :" + unitType);
			}
			if (isValidDest)
			{
				_tracePathItem.setEnabled(true);
				_tracePathAndMoveItem.setEnabled(true);
				final boolean status = player.getUnitList().getUnitAt(unitPosition).getUnitByType(unitType).size() > 1;

				_tracePathAndMoveAllItem.setEnabled(status);
			} else
			{
				_tracePathItem.setEnabled(false);
				_tracePathAndMoveItem.setEnabled(false);
				_tracePathAndMoveAllItem.setEnabled(false);
			}

			_bombardmentItem.setEnabled(isValidForBombarment);

		} else if (currentUnit.hasPath())
		{
			_tracePathItem.setEnabled(false);
			_tracePathAndMoveItem.setEnabled(false);
			_tracePathAndMoveAllItem.setEnabled(false);
			_moveItem.setEnabled(true);
		} else
		{
			_tracePathItem.setEnabled(false);
			_tracePathAndMoveItem.setEnabled(false);
			_tracePathAndMoveAllItem.setEnabled(false);

			_moveItem.setEnabled(false);
		}

		// Attack
		adjustAttackMenu();

		// Bomb
		adjustBombMenu();

		// Load | Unload
		adjustLoadUnloadMenu();

		// Open City
		final Cell cell = (Cell) _frame.getValueAt(_menuCoord.getY(), _menuCoord.getX());
		if (cell.isCity() && cell.getCity().getPlayer() instanceof HumanPlayer)
		{
			_openCityItem.setEnabled(true);
			hasAction = true;
		} else
		{
			_openCityItem.setEnabled(false);
		}

		if (hasAction)
		{
			return true;
		} else
		{
			return false;
		}
	}

	/**
	 * Set the sub-menu attack used by all attacking units (Tank, Fighter &
	 * Destroyer).
	 * 
	 * @return Indicate if almost 1 item is visible.
	 */
	private void adjustAttackMenu()
	{
		final AbstractUnit currentUnit = _frame.getCurrentUnit();
		if (currentUnit == null)
		{
			_attackMenu.setVisible(false);
			return;
		}
		Assert.check(currentUnit.getPlayer() instanceof HumanPlayer, "Invalid unit owner");

		if (currentUnit instanceof TransportShip)
		{
			_attackMenu.setVisible(false);
			return;
		}

		_attackMenu.setVisible(true);
		if (currentUnit.getMove() == 0)
		{
			_attackMenu.setEnabled(false);
			return;
		}

		final HumanPlayer player = (HumanPlayer) currentUnit.getPlayer();
		final AbstractUnitList globalEnemyList = player.getEnemySet();
		if (globalEnemyList.size() == 0)
		{
			_attackMenu.setEnabled(false);
			return;
		}
		final CoordSet coordSet = CoordSet
				.createInitiazedCoordSet(currentUnit.getPosition(), 1, Integer.MAX_VALUE, Integer.MAX_VALUE);

		final AbstractUnitList enemyList = globalEnemyList.getUnitByCoordList(coordSet);
		if (enemyList.size() == 0)
		{
			_attackMenu.setEnabled(false);
			return;
		}

		_attackMenu.removeAll();
		_attackMenu.setVisible(true);
		final Coord unitCoord = currentUnit.getPosition();
		AbstractUnit currentEnemy = null;
		ImageIcon orientationIcon = null;
		ImageIcon unitIcon = null;
		JMenuItem currentItem = null;
		int enemyOrientation = -1;
		final Iterator enemyIter = enemyList.iterator();
		while (enemyIter.hasNext())
		{
			currentEnemy = (AbstractUnit) enemyIter.next();
			enemyOrientation = unitCoord.getOrientation(currentEnemy.getPosition());
			orientationIcon = _iconFactory.getOrientionIcon(enemyOrientation);
			unitIcon = _iconFactory.getUnitIconByType(	currentEnemy.getUnitType(),
														currentEnemy.getPlayer().getPlayerId(),
														IconFactory.SIZE_SIMPLE_SIZE, currentEnemy.isLiving());
			AttackAction action = new AttackAction(FSFrame.getFSFrameContainer(_frame), currentEnemy);
			currentItem = new UnitSubMenuItem(action, orientationIcon, unitIcon,
					currentEnemy.getBestUnitName() + " ...");
			_attackMenu.add(currentItem);
		}
		_attackMenu.setEnabled(true);

		return;
	}

	private void adjustBombMenu()
	{
		_bombardmentItem.setEnabled(false);
		if (!UnitTypeCst.BATTLESHIP.equals(_frame.getCurrentUnit().getUnitType()))
		{
			_bombardmentItem.setVisible(false);
		} else
		{
			_bombardmentItem.setVisible(true);
			_bombardmentItem.setEnabled(false);

			final Battleship battleship = (Battleship) _frame.getCurrentUnit();
			final Coord unitPosition = battleship.getPosition();
			if (!_menuCoord.equals(unitPosition))
			{

				if (unitPosition.calculateIntDistance(_menuCoord) <= battleship.getBombardmentRange()
						&& battleship.getMove() == battleship.getMovementCapacity())
				{
					_bombardmentItem.setEnabled(true);
				}
			}
		}
	}

	/**
	 * Set the sub-menu load / unload used by the transport ships.
	 * 
	 * @return Indicate if almost 1 item is visible.
	 */
	private void adjustLoadUnloadMenu()
	{
		final AbstractUnit currentUnit = _frame.getCurrentUnit();
		if (!(currentUnit instanceof TransportShip))
		{
			_loadUnitItem.setVisible(false);
			_unloadUnitItem.setVisible(false);
			return;
		}
		_loadUnitItem.setVisible(true);
		_unloadUnitItem.setVisible(true);
		final TransportShip transport = (TransportShip) currentUnit;

		// Load Menu
		if (transport.getFreeSpace() == 0)
		{
			_loadUnitItem.setEnabled(false);
		} else
		{
			adjustLoadMenu(transport);
		}

		// Unload Menu
		JMenu unitMenu;
		_unloadUnitItem.setEnabled(false);
		_unloadUnitItem.removeAll();
		if (transport.getFreeSpace() < transport.getSpaceCapacity())
		{
			for (ILoadableUnit unit : transport.getContainedUnitList())
			{
				if (((AbstractGroundLoadableUnit) unit).getMove() == ((AbstractGroundLoadableUnit) unit)
						.getMovementCapacity())
				{
					unitMenu = null;
					unitMenu = adjustUnloadMenu(transport, (AbstractUnit) unit);
					if (unitMenu != null)
					{
						_unloadUnitItem.setEnabled(true);
						_unloadUnitItem.add(unitMenu);
					}
				}
			}
		}
	}

	private JMenu adjustUnloadMenu(final TransportShip p_transport, final AbstractUnit p_containedUnit)
	{
		FSMap map = p_transport.getPlayer().getMapMgr().getMap();
		int yBase = p_transport.getPosition().getY();
		int xBase = p_transport.getPosition().getX();

		String hp = (int) (p_containedUnit.getHitPointsCapacity() / (float) p_containedUnit.getHitPoints() * 100f)
				+ "%";
		final JMenu unitMenu = new JMenu("id:" + p_containedUnit.getUnitId() + "  hp:" + hp);

		unitMenu.setIcon(_iconFactory.getUnitIconByType(p_containedUnit.getUnitType(),
														p_containedUnit.getPlayer().getPlayerId(),
														IconFactory.SIZE_SIMPLE_SIZE, p_containedUnit.isLiving()));

		int i = 1;
		Cell cell;
		Coord coord;
		boolean unloadPosible = false;
		for (int y = -1; y <= 1; y++)
		{
			for (int x = -1; x <= 1; x++, i++)
			{
				coord = new Coord(xBase + x, yBase + y);
				if (!map.isBoundedCoord(coord))
				{
					continue;
				}

				cell = map.getCellAt(coord);
				if (cell.isGroundCell())
				{
					unloadPosible = true;
					JMenuItem direction = new JMenuItem(new UnloadAction(_frame, (AbstractGroundLoadableUnit) p_containedUnit, coord));
					direction.setIcon(_iconFactory.getOrientionIcon(i));
					unitMenu.add(direction);
				}
			}
		}

		if (unloadPosible)
		{
			return unitMenu;
		}
		return null;
	}

	private void adjustLoadMenu(final TransportShip p_transport)
	{
		final HumanPlayer player = (HumanPlayer) _frame.getCurrentGame().getCurrentPlayer();
		final AbstractUnitList unitReadyToLoadList = player.getUnitList().getUnitInRange(p_transport.getPosition(), 1)
				.getLoadableUnit(p_transport);
		_loadUnitItem.setEnabled(false);
		if (unitReadyToLoadList.isEmpty())
		{
			return;
		}

		_loadUnitItem.removeAll();

		String hp;
		ImageIcon orientationIcon = null;
		ImageIcon unitIcon = null;
		JMenuItem currentItem = null;
		int orientation = -1;
		for (AbstractUnit currentUnit : unitReadyToLoadList)
		{
			_loadUnitItem.setEnabled(true);
			orientation = p_transport.getPosition().getOrientation(currentUnit.getPosition());
			orientationIcon = _iconFactory.getOrientionIcon(orientation);
			unitIcon = _iconFactory.getUnitIconByType(	currentUnit.getUnitType(),
														currentUnit.getPlayer().getPlayerId(),
														IconFactory.SIZE_SIMPLE_SIZE, currentUnit.isLiving());
			LoadAction action = new LoadAction(_frame, currentUnit, p_transport);
			hp = (int) (currentUnit.getHitPointsCapacity() / (float) currentUnit.getHitPoints() * 100f) + "%";
			currentItem = new UnitSubMenuItem(action, orientationIcon, unitIcon,
					"id:" + currentUnit.getBestUnitName() + "  hp:" + hp);
			_loadUnitItem.add(currentItem);
		}

	}
}
