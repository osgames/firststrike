package org.jocelyn_chaumel.firststrike.gui.game.contextual_menu.load;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.ILoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.gui.game.board.BattleBoard;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class LoadAction extends AbstractAction
{
	private final BattleBoard _battleBoard;
	private final AbstractUnit _tank;
	private final TransportShip _transport;

	public LoadAction(final BattleBoard p_battleBoard, final AbstractUnit p_tank, final TransportShip p_transport)
	{
		Assert.precondition(p_tank.getUnitType() == UnitTypeCst.ARTILLERY || p_tank.getUnitType() == UnitTypeCst.TANK, "Invalid unit type");
		_battleBoard = p_battleBoard;
		_transport = p_transport;
		_tank = p_tank;
	}

	@Override
	public void actionPerformed(final ActionEvent p_actionEvent)
	{
		_battleBoard.loadTank(_transport, (ILoadableUnit) _tank);
	}

}
