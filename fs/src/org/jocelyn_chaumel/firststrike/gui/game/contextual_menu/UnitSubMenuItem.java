package org.jocelyn_chaumel.firststrike.gui.game.contextual_menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;

public class UnitSubMenuItem extends JMenuItem
{
	private final Image _unit;
	private final String _text;

	public UnitSubMenuItem(	final AbstractAction p_action,
							final ImageIcon p_orientation,
							final ImageIcon p_unitIcon,
							final String p_text)
	{
		super(p_action);
		setIcon(p_orientation);
		_unit = p_unitIcon.getImage();
		_text = p_text;
		setText("        " + _text);
	}

	@Override
	public void paint(final Graphics p_graphics)
	{
		super.paint(p_graphics);
		Color bgc = this.getBackground();

		p_graphics.setColor(bgc);
		p_graphics.drawImage(_unit, 23, 4, null, null);

	}

}
