package org.jocelyn_chaumel.firststrike.gui.game;

import java.util.Comparator;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.ILoadableUnit;

public class ILoadableUnitListComparator implements Comparator<ILoadableUnit>
{

	@Override
	public int compare(final ILoadableUnit p_unit1, final ILoadableUnit p_unit2)
	{
		int unit1 = 0;
		if (p_unit1 != null)
		{
			unit1 = (int) ((((AbstractUnit)p_unit1).getMove() / (float) ((AbstractUnit)p_unit1).getMovementCapacity()) * 100);
		}

		int unit2 = 0;
		if (p_unit2 != null)
		{
			unit2 = (int) ((((AbstractUnit)p_unit2).getMove() / (float) ((AbstractUnit)p_unit2).getMovementCapacity()) * 100);
		}

		if (unit1 != unit2)
		{
			return unit2 - unit1;
		}

		unit1 = ((AbstractUnit)p_unit1).getUnitType().getValue();
		unit2 = ((AbstractUnit)p_unit2).getUnitType().getValue();

		return unit2 - unit1;
	}

}
