package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.MatteBorder;

import net.miginfocom.swing.MigLayout;

import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;

public class CurrentCellPnl extends JPanel
{
	private static final ApplicationConfigurationMgr _conf = ApplicationConfigurationMgr.getInstance();

	private JLabel _cityNameValue;
	private JLabel _areaNameValue;
	private JLabel _airportIcon;
	private JLabel _bombardmentIcon;
	private JLabel _seaportIcon;
	private JLabel _factoryTankIcon;
	private JLabel _factoryAirIcon;
	private JLabel _factorySeaIcon;
	private JLabel _positionValue;
	private JLabel _unitTypeUnderConstIcon;
	private JLabel _cellIcon;
	private final static IconFactory _iconFactory = IconFactory.getInstance();

	private FSFrame _frame = null;
	private JButton _cityConfigurationBtn;
	private JLabel _factoryIcon;
	private JLabel _cityNameLbl;
	private JToggleButton _recurciveBuildBtn;
	private City _currentCity;

	/**
	 * Create the panel.
	 */
	public CurrentCellPnl()
	{
		createComponent();
	}

	public CurrentCellPnl(final FSFrame p_frame)
	{
		_frame = p_frame;
		createComponent();
	}

	public void preparePanel(final Cell p_cell)
	{

		final CellTypeCst cellType = p_cell.getCellType();
		_positionValue.setText(p_cell.getCellCoord().toStringLight());
		_areaNameValue.setText(p_cell.getAreaList().getPrincipalArea().getInternalAreaName());
		_cellIcon
				.setIcon(_iconFactory.getSimpleCellIcon(cellType, IconFactory.NO_FOG, IconFactory.SIZE_DOUBLE_SIZE, 1));
		
		HumanPlayer human = (HumanPlayer) _frame.getGame().getCurrentPlayer();
		if (human.isBombardmentPlanned(p_cell.getCellCoord()))
		{
			_bombardmentIcon.setIcon(new ImageIcon(
					CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_on.png")));
		} else
		{
			_bombardmentIcon.setIcon(new ImageIcon(
					CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_off.png")));
		}

		if (!CellTypeCst.CITY.equals(cellType))
		{
			_cityNameLbl.setVisible(false);
			_cityNameValue.setVisible(false);
			_cityConfigurationBtn.setEnabled(false);
			_factoryIcon.setVisible(false);
			_unitTypeUnderConstIcon.setVisible(false);
			_seaportIcon.setVisible(false);
			_airportIcon.setVisible(false);
			_factoryAirIcon.setVisible(false);
			_factoryTankIcon.setVisible(false);
			_factorySeaIcon.setVisible(false);
			_recurciveBuildBtn.setEnabled(false);
			_recurciveBuildBtn.setSelected(false);
			_currentCity = null;

		} else
		{
			_cityNameValue.setVisible(true);
			_cityNameValue.setText(p_cell.getCellName());
			_airportIcon.setVisible(true);
			_airportIcon.setIcon(new ImageIcon(
					CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/airport_on.png")));
			_cityNameLbl.setVisible(true);
			final City city = p_cell.getCity();
			_seaportIcon.setVisible(true);
			if (city.isPortCity())
			{
				_seaportIcon.setIcon(new ImageIcon(
						CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/seaport_on.png")));
				_factorySeaIcon.setVisible(true);
				_factorySeaIcon.setIcon(new ImageIcon(CurrentCellPnl.class
						.getResource("/org/jocelyn_chaumel/firststrike/resources/factory_sea_on.png")));
			} else
			{
				_seaportIcon.setIcon(new ImageIcon(CurrentCellPnl.class
						.getResource("/org/jocelyn_chaumel/firststrike/resources/seaport_off.png")));
				_factorySeaIcon.setVisible(false);
			}
			if (city.getPlayer() != _frame.getGame().getCurrentPlayer() && !_conf.isDebugMode())
			{
				_currentCity = null;
				_recurciveBuildBtn.setEnabled(false);

				_factoryAirIcon.setVisible(false);
				_factoryTankIcon.setVisible(false);
				_factorySeaIcon.setVisible(false);
				_factoryIcon.setVisible(false);
				_cityConfigurationBtn.setEnabled(false);
				_unitTypeUnderConstIcon.setVisible(false);
			} else
			{
				_factoryAirIcon.setVisible(true);
				_factoryAirIcon.setIcon(new ImageIcon(CurrentCellPnl.class
						.getResource("/org/jocelyn_chaumel/firststrike/resources/factory_air_on.png")));
				_factoryTankIcon.setVisible(true);
				_factoryTankIcon.setIcon(new ImageIcon(CurrentCellPnl.class
						.getResource("/org/jocelyn_chaumel/firststrike/resources/factory_tank_on.png")));

				_cityConfigurationBtn.setEnabled(true);
				_currentCity = city;
				_recurciveBuildBtn.setEnabled(true);
				_recurciveBuildBtn.setSelected(city.isContinueProduction());
				_unitTypeUnderConstIcon.setVisible(false);
				_factoryIcon.setVisible(true);
				if (city.isUnitUnderConstruction())
				{
					_factoryIcon.setIcon(new ImageIcon(CurrentCellPnl.class
							.getResource("/org/jocelyn_chaumel/firststrike/resources/factory_on.png")));
					_unitTypeUnderConstIcon.setVisible(true);
					final UnitTypeCst unitType = city.getProductionType();
					_unitTypeUnderConstIcon.setIcon(_iconFactory
							.getUnitIconByType(unitType, city.getPlayer().getPlayerId(), IconFactory.SIZE_SIMPLE_SIZE, true));
					_unitTypeUnderConstIcon
							.setText(city.getProductionTime() + "/" + City.getProductionTimeByType(unitType));
					;

				} else
				{
					_factoryIcon.setIcon(new ImageIcon(CurrentCellPnl.class
							.getResource("/org/jocelyn_chaumel/firststrike/resources/factory_off.png")));
					_unitTypeUnderConstIcon.setVisible(false);
					_unitTypeUnderConstIcon.setVisible(false);
				}
				// _airportIcon.setIcon();
			}
		}
	}

	private void createComponent()
	{
		setMinimumSize(new Dimension(5, 5));
		setLayout(new MigLayout("", "[300.00]", "[76.00]0[20.00]"));

		JPanel currentCellDetailsPnl = new JPanel();
		add(currentCellDetailsPnl, "cell 0 0,grow");
		currentCellDetailsPnl.setLayout(new MigLayout("", "0[]0[grow]0[]0", "0[68.00]0"));

		JPanel col1CityPnl = new JPanel();

		_cellIcon = new JLabel("");
		_cellIcon.setIconTextGap(0);
		col1CityPnl.add(_cellIcon, "alignx center");
		_cellIcon.setIcon(new ImageIcon(CurrentCellPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_40x40/land/city_i1.gif")));
		currentCellDetailsPnl.add(col1CityPnl, "cell 0 0,alignx center,aligny center");
		col1CityPnl.setLayout(new MigLayout("", "0[left]0", "0[20][40px]0"));

		JPanel positionPnl = new JPanel();
		FlowLayout fl_positionPnl = (FlowLayout) positionPnl.getLayout();
		fl_positionPnl.setVgap(0);
		fl_positionPnl.setHgap(1);
		col1CityPnl.add(positionPnl, "cell 0 1,alignx left,aligny top");

		JLabel positionIcon = new JLabel("");
		positionPnl.add(positionIcon);
		positionIcon.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/position.png")));

		_positionValue = new JLabel("10, 10");
		_positionValue.setFont(new Font("Tahoma", Font.PLAIN, 9));
		positionPnl.add(_positionValue);

		JPanel col2CityDetailsPnl = new JPanel();
		currentCellDetailsPnl.add(col2CityDetailsPnl, "cell 1 0,growx,aligny center");
		col2CityDetailsPnl.setLayout(new MigLayout("", "[right][grow]", "[][][]"));

		JLabel areaNameLbl = new JLabel();
		areaNameLbl.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/area.png")));
		col2CityDetailsPnl.add(areaNameLbl, "cell 0 0,alignx right,aligny baseline");

		_areaNameValue = new JLabel("Area name");
		_areaNameValue.setOpaque(true);
		_areaNameValue.setBackground(new Color(255, 255, 255));
		_areaNameValue.setSize(new Dimension(200, 0));
		_areaNameValue.setBorder(new MatteBorder(1, 1, 2, 1, (Color) Color.GRAY));
		_areaNameValue.setForeground(Color.BLUE);
		col2CityDetailsPnl.add(_areaNameValue, "cell 1 0,growx");

		_cityNameLbl = new JLabel();
		_cityNameLbl.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/city.png")));
		col2CityDetailsPnl.add(_cityNameLbl, "cell 0 1");

		_cityNameValue = new JLabel("Montr\u00E9al");
		_cityNameValue.setBackground(new Color(255, 255, 255));
		_cityNameValue.setOpaque(true);
		_cityNameValue.setBorder(new MatteBorder(1, 1, 2, 1, (Color) Color.GRAY));
		_cityNameValue.setForeground(Color.BLUE);
		col2CityDetailsPnl.add(_cityNameValue, "cell 1 1,growx");

		_factoryIcon = new JLabel("");
		_factoryIcon.setAlignmentX(Component.CENTER_ALIGNMENT);
		col2CityDetailsPnl.add(_factoryIcon, "cell 0 2,alignx center");
		_factoryIcon.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/factory_on.png")));

		_unitTypeUnderConstIcon = new JLabel("10/10");
		_unitTypeUnderConstIcon.setOpaque(true);
		_unitTypeUnderConstIcon.setBackground(new Color(255, 255, 255));
		_unitTypeUnderConstIcon.setBorder(new MatteBorder(1, 1, 2, 1, (Color) Color.GRAY));
		_unitTypeUnderConstIcon.setForeground(Color.BLUE);
		_unitTypeUnderConstIcon.setAlignmentX(Component.CENTER_ALIGNMENT);
		_unitTypeUnderConstIcon.setIcon(new ImageIcon(CurrentCellPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/fighter_p1.png")));
		col2CityDetailsPnl.add(_unitTypeUnderConstIcon, "cell 1 2,growx");

		JPanel col3CityOptionsPnl = new JPanel();
		currentCellDetailsPnl.add(col3CityOptionsPnl, "cell 2 0,grow");
		col3CityOptionsPnl.setLayout(new MigLayout("", "[][]", "[grow,center]0[grow,center]0[grow,center]"));

		_factoryTankIcon = new JLabel("");
		_factoryTankIcon.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/factory_tank_off.png")));
		col3CityOptionsPnl.add(_factoryTankIcon, "cell 0 0");

		_bombardmentIcon = new JLabel("");
		_bombardmentIcon.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_off.png")));
		col3CityOptionsPnl.add(_bombardmentIcon, "cell 1 0");

		_factoryAirIcon = new JLabel("");
		_factoryAirIcon.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/factory_air_off.png")));
		col3CityOptionsPnl.add(_factoryAirIcon, "cell 0 1");

		_airportIcon = new JLabel("");
		_airportIcon.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/airport_off.png")));
		col3CityOptionsPnl.add(_airportIcon, "cell 1 1");

		_factorySeaIcon = new JLabel("");
		_factorySeaIcon.setHorizontalAlignment(SwingConstants.CENTER);
		_factorySeaIcon.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/factory_sea_off.png")));
		col3CityOptionsPnl.add(_factorySeaIcon, "cell 0 2");

		_seaportIcon = new JLabel("");
		_seaportIcon.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/seaport_off.png")));
		col3CityOptionsPnl.add(_seaportIcon, "cell 1 2,aligny baseline");

		JPanel CurrentCellActionListPnl = new JPanel();
		CurrentCellActionListPnl.setAlignmentY(0.2f);
		CurrentCellActionListPnl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		add(CurrentCellActionListPnl, "cell 0 1,growx,aligny top");
		CurrentCellActionListPnl.setLayout(new MigLayout("", "[][grow]", "0[]0")); //$NON-NLS-2$

		JLabel actionListIcon = new JLabel("");
		CurrentCellActionListPnl.add(actionListIcon, "cell 0 0");
		actionListIcon.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/list.png")));

		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setVgap(1);
		flowLayout.setHgap(1);
		CurrentCellActionListPnl.add(panel, "cell 1 0,grow");

		_cityConfigurationBtn = new JButton("");
		_cityConfigurationBtn.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/configuration.png")));
		_cityConfigurationBtn.setMargin(new Insets(1, 1, 1, 1));
		_cityConfigurationBtn.addActionListener(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_frame.openCity(_frame.getCurrentCell().getCellCoord());
			}
		});
		panel.add(_cityConfigurationBtn);

		_recurciveBuildBtn = new JToggleButton("");
		_recurciveBuildBtn.setMargin(new Insets(1, 1, 1, 1));
		_recurciveBuildBtn.setAction(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_currentCity.setContinueProduction(!_currentCity.isContinueProduction());
			}
		});
		_recurciveBuildBtn.setIcon(new ImageIcon(
				CurrentCellPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/recursive.png")));
		panel.add(_recurciveBuildBtn);
	}
}
