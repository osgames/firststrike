package org.jocelyn_chaumel.firststrike.gui.game.toolbar.horizontal;

import java.util.ResourceBundle;

import javax.swing.ImageIcon;

import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;

public class ShadowCardPanel extends CellCardPanel
{
	public final static String SHADOW_CARD_IDENTIFIER = "shadow card";
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	public ShadowCardPanel()
	{
		super();
		_areaName.setVisible(false);
		_OpenCityBtn.setVisible(false);
		_cellCoordonates.setVisible(false);
		ImageIcon icon = _iconFactory.getSimpleCellIcon(CellTypeCst.SHADOW,
														IconFactory.FOG,
														IconFactory.SIZE_DOUBLE_SIZE,
														1);
		_cellIcon.setIcon(icon);
		_cellType.setText(_bundle.getString("fs.common.cell_type." + CellTypeCst.SHADOW.getLabel()));
		_cityName.setVisible(false);
		_cityProductionTypeIcon.setVisible(false);
		_lblAreaName.setVisible(false);
		_lblCellCoordonates.setVisible(false);
		_lblCityName.setVisible(false);
		_lblProductionType.setVisible(false);
		_recursiveIcon.setIcon(null);
		_productionTime.setVisible(false);

	}
}
