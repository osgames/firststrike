package org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.city;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaList;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.VerticalToolBar;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.Id;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class AreaCityJList extends JList
{
	private final DefaultListModel _model = new DefaultListModel();
	private HumanPlayer _currentPlayer;
	private FSMap _currentMap;
	private final HashMap<Id, Boolean> _deployedAreaItemList = new HashMap<Id, Boolean>();
	private VerticalToolBar _verticalToolBar = null;

	public AreaCityJList()
	{
		super();
		setModel(_model);
		setCellRenderer(new AreaCityJListRenderer());
		addMouseListener(new AreaCityJListMouseListener());
	}

	public final void setVerticalToolBar(final VerticalToolBar p_verticalToolBar)
	{
		_verticalToolBar = p_verticalToolBar;
	}

	public final void setGame(final AbstractPlayer p_player, final FSMap p_map)
	{
		Assert.precondition(_model.isEmpty(), "Model is not empty");

		_currentPlayer = (HumanPlayer) p_player;
		_currentMap = p_map;

		refreshAllCityOfList();
	}

	public void refreshAllCityOfList()
	{
		_model.clear();
		final AreaList areaList = _currentMap.getAreaSet().getAllGroundAreaSetWithCity();
		areaList.sortListByName();
		final Iterator<Area> areaIter = areaList.iterator();
		AreaListItem currentGroup = null;
		CityList currentCityList = null;
		City currentCity = null;
		Iterator<City> cityIter = null;
		Area currentArea = null;
		Id currentAreaId = null;
		while (areaIter.hasNext())
		{
			currentArea = areaIter.next();
			currentAreaId = currentArea.getId();
			currentCityList = currentArea.getCityList().getCityByPlayer(_currentPlayer);
			if (currentCityList.size() == 0)
			{
				_deployedAreaItemList.remove(currentAreaId);
			} else
			{
				if (!_deployedAreaItemList.containsKey(currentAreaId))
				{
					_deployedAreaItemList.put(currentAreaId, new Boolean(true));
				}
				
				currentGroup = new AreaListItem(currentArea, _currentPlayer);
				_model.addElement(currentGroup);

				currentCityList.sortByName();
				cityIter = currentCityList.iterator();
				while (cityIter.hasNext())
				{
					currentCity = cityIter.next();
					_model.addElement(currentCity);
				}
			}
		}
	}

	public void addCity(final City p_city)
	{
		final Id areaId = p_city.getAreaId();
		// Si l'entr�e existe d�j�, ceci permet de garantir qu'elle est d�ploy�e
		_deployedAreaItemList.put(areaId, new Boolean(true));
		refreshAllCityOfList();
		final int indexOfNewCity = _model.indexOf(p_city);
		setSelectedIndex(indexOfNewCity);
	}

	public void lostCity(final City p_city)
	{
		_model.removeElement(p_city);
		// refreshAllCityOfList();
	}

	public void closeGame()
	{
		_model.clear();
	}

	/**
	 * Renderer for the AreaCityJList.
	 * 
	 * @author Jocelyn Chaumel
	 */
	private class AreaCityJListRenderer implements ListCellRenderer
	{

		private final JLabel _label = new JLabel();
		private final CityItemPnl _cityItem = new CityItemPnl();

		@Override
		public Component getListCellRendererComponent(	JList p_list,
														Object p_value,
														int p_index,
														boolean p_isSelected,
														boolean p_cellHasFocus)
		{
			if (p_value instanceof AreaListItem)
			{
				final AreaListItem group = (AreaListItem) p_value;
				_label.setText(group.getArea().getInternalAreaName());
				_label.setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
				_label.setBackground(Color.LIGHT_GRAY);
				return _label;

			} else
			{
				final City city = (City) p_value;
				if (city.getPlayer() instanceof HumanPlayer)
				{
					_cityItem.updatePanel(city, p_isSelected);
					return _cityItem;
				} else
				{
					_label.setText("To Be Removed");
					return _label;
				}
			}
		}

	}

	private class AreaCityJListMouseListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent p_event)
		{
			final JList myList = (JList) p_event.getSource();

			final Object value = myList.getSelectedValue();
			if (value == null)
			{
				return;
			}
			final int index = myList.getSelectedIndex();
			final DefaultListModel model = (DefaultListModel) myList.getModel();
			if (value instanceof AreaListItem)
			{
				final AreaListItem group = (AreaListItem) value;
				final Id areaId = group.getArea().getId();
				final Boolean isDeployed = _deployedAreaItemList.get(areaId);
				if (isDeployed != null && isDeployed.booleanValue())
				{
					int size = group.getSize();
					while (size > 0)
					{
						model.remove(index + size);
						size--;
					}
					_deployedAreaItemList.put(areaId, new Boolean(false));
				} else
				{
					int size = group.getSize();
					while (size > 0)
					{
						size--;
						model.add(index + 1, group.getCityList().get(size));
					}
					_deployedAreaItemList.put(areaId, new Boolean(true));
				}
			} else
			{
				final Coord coord = ((City) value).getCell().getCellCoord();
				_verticalToolBar.setCurrentCell(coord, false);
				if (p_event.getClickCount() >= 2)
				{
					_verticalToolBar.openCity(coord);
				}
			}
		}

		@Override
		public void mouseEntered(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mouseExited(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mousePressed(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mouseReleased(MouseEvent p_e)
		{
			// NOP
		}

	}

}
