package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractGroundLoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.IBombardment;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.menu.rename.RenameUnitDlg;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class CurrentUnitPnl extends JPanel
{
	private final IconFactory _iconFactory = IconFactory.getInstance();
	private JLabel _idValueLbl;
	private JLabel _unitNameValueLbl;
	private JLabel _fuelValueLbl;
	private JLabel _moveValueLbl;
	private JLabel _hpValueLbl;
	private JLabel _nbAttackValueLbl;
	private JLabel _nbBombardmentValueLbl;
	private JLabel _bombardmentDmgValueLbl;
	private JLabel _attackDmgValueLbl;
	private JLabel _nbDefenceValueLbl;
	private JLabel _defenceDmgValueLbl;
	private JButton _moveBtn;
	private JButton _bombBtn;
	private JButton _unitConfigurationBtn;
	private JButton _attackBtn;
	private JButton _loadTank;

	private FSFrame _frame = null;
	private JLabel _unitTypeIcon;
	private JLabel _defenceDmgIcon;
	private JLabel _defenceIcon;
	private JLabel _bombardmebtIcon;
	private JLabel _attackIcon;
	private JLabel _attackDmgIcon;
	private JLabel _bombardmentDmgIcon;

	/**
	 * Create the panel.
	 */
	public CurrentUnitPnl()
	{
		createComponents();
	}

	public CurrentUnitPnl(final FSFrame p_frame)
	{
		_frame = p_frame;
		createComponents();
	}

	private void createComponents()
	{
		setLayout(new MigLayout("", "[295.00,grow]", "5[][][]"));

		JPanel currentUnitPnl = new JPanel();
		add(currentUnitPnl, "cell 0 0,grow");
		currentUnitPnl.setLayout(new MigLayout("", "[][][41.00][grow]", "0[]0"));

		_unitTypeIcon = new JLabel("");
		_unitTypeIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_40x40/unit/fighter_p1.gif")));
		currentUnitPnl.add(_unitTypeIcon, "cell 0 0");

		JLabel idLbl = new JLabel("#");
		currentUnitPnl.add(idLbl, "cell 1 0");

		_idValueLbl = new JLabel("362");
		_idValueLbl.setForeground(Color.BLUE);
		currentUnitPnl.add(_idValueLbl, "cell 2 0");

		_unitNameValueLbl = new JLabel("Destructor!");
		_unitNameValueLbl.setForeground(Color.BLUE);
		currentUnitPnl.add(_unitNameValueLbl, "cell 3 0");

		JPanel currentUnitDetailsPnl = new JPanel();
		add(currentUnitDetailsPnl, "cell 0 1,grow");
		currentUnitDetailsPnl.setLayout(new MigLayout("", "[106.00][grow]", "0[grow]0"));

		JPanel currentUnitDetailsCol1Pnl = new JPanel();
		currentUnitDetailsCol1Pnl.setBorder(null);
		currentUnitDetailsPnl.add(currentUnitDetailsCol1Pnl, "cell 0 0,grow");
		currentUnitDetailsCol1Pnl.setLayout(new MigLayout("", "0[][]0", "[][grow][]"));

		JLabel hpIcon = new JLabel("");
		hpIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/hit_points.png")));
		currentUnitDetailsCol1Pnl.add(hpIcon, "cell 0 0");

		_hpValueLbl = new JLabel("1000/1000");
		_hpValueLbl.setForeground(Color.BLUE);
		currentUnitDetailsCol1Pnl.add(_hpValueLbl, "cell 1 0");

		JLabel moveIcon = new JLabel("");
		moveIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/footprint.png")));
		currentUnitDetailsCol1Pnl.add(moveIcon, "cell 0 1");

		_moveValueLbl = new JLabel("10/10");
		_moveValueLbl.setForeground(Color.BLUE);
		currentUnitDetailsCol1Pnl.add(_moveValueLbl, "cell 1 1");

		JLabel fuelIcon = new JLabel("");
		fuelIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/fuel.png")));
		currentUnitDetailsCol1Pnl.add(fuelIcon, "cell 0 2");

		_fuelValueLbl = new JLabel("20/20");
		_fuelValueLbl.setForeground(Color.BLUE);
		currentUnitDetailsCol1Pnl.add(_fuelValueLbl, "cell 1 2");

		JPanel currentUnitDetailsCol2Pnl = new JPanel();
		currentUnitDetailsPnl.add(currentUnitDetailsCol2Pnl, "cell 1 0,grow");
		currentUnitDetailsCol2Pnl.setLayout(new MigLayout("", "0[grow]0", "[grow][][grow]"));

		JPanel attackDetailsPnl = new JPanel();
		attackDetailsPnl.setBorder(new LineBorder(Color.GRAY, 1, true));
		currentUnitDetailsCol2Pnl.add(attackDetailsPnl, "cell 0 0,grow");
		attackDetailsPnl.setLayout(new MigLayout("", "[20][][][][]", "0[]0"));

		_attackIcon = new JLabel("");
		_attackIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_on.png")));
		attackDetailsPnl.add(_attackIcon, "cell 0 0,aligny baseline");

		JLabel label_1 = new JLabel("#");
		attackDetailsPnl.add(label_1, "cell 1 0");

		_nbAttackValueLbl = new JLabel("3/3");
		_nbAttackValueLbl.setForeground(Color.BLUE);
		attackDetailsPnl.add(_nbAttackValueLbl, "cell 2 0");

		_attackDmgIcon = new JLabel("");
		_attackDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_attack_on.png")));
		attackDetailsPnl.add(_attackDmgIcon, "cell 3 0");

		_attackDmgValueLbl = new JLabel("100/200");
		_attackDmgValueLbl.setForeground(Color.BLUE);
		attackDetailsPnl.add(_attackDmgValueLbl, "cell 4 0");

		JPanel bombardmentDetailsPnl = new JPanel();
		bombardmentDetailsPnl.setBorder(new LineBorder(Color.GRAY, 1, true));
		currentUnitDetailsCol2Pnl.add(bombardmentDetailsPnl, "cell 0 1,grow");
		bombardmentDetailsPnl.setLayout(new MigLayout("", "[20][][][][]", "0[]0"));

		_bombardmebtIcon = new JLabel("");
		_bombardmebtIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_on.png")));
		bombardmentDetailsPnl.add(_bombardmebtIcon, "cell 0 0");

		JLabel label_2 = new JLabel("#");
		bombardmentDetailsPnl.add(label_2, "cell 1 0");

		_nbBombardmentValueLbl = new JLabel("2/2");
		_nbBombardmentValueLbl.setForeground(Color.BLUE);
		bombardmentDetailsPnl.add(_nbBombardmentValueLbl, "cell 2 0");

		_bombardmentDmgIcon = new JLabel("");
		_bombardmentDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_attack_on.png")));
		bombardmentDetailsPnl.add(_bombardmentDmgIcon, "cell 3 0");

		_bombardmentDmgValueLbl = new JLabel("100/200");
		_bombardmentDmgValueLbl.setForeground(Color.BLUE);
		bombardmentDetailsPnl.add(_bombardmentDmgValueLbl, "cell 4 0");

		JPanel defenceDetailsPnl = new JPanel();
		defenceDetailsPnl.setBorder(new LineBorder(new Color(128, 128, 128), 1, true));
		currentUnitDetailsCol2Pnl.add(defenceDetailsPnl, "cell 0 2,grow");
		defenceDetailsPnl.setLayout(new MigLayout("", "[20, CENTER][][][][]", "0[]0"));

		_defenceIcon = new JLabel("");
		_defenceIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_on.png")));
		defenceDetailsPnl.add(_defenceIcon, "cell 0 0");

		JLabel label_3 = new JLabel("#");
		defenceDetailsPnl.add(label_3, "cell 1 0");

		_nbDefenceValueLbl = new JLabel("3/3");
		_nbDefenceValueLbl.setForeground(Color.BLUE);
		defenceDetailsPnl.add(_nbDefenceValueLbl, "cell 2 0");

		_defenceDmgIcon = new JLabel("");
		_defenceDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_attack_on.png")));
		defenceDetailsPnl.add(_defenceDmgIcon, "cell 3 0");

		_defenceDmgValueLbl = new JLabel("100/200");
		_defenceDmgValueLbl.setForeground(Color.BLUE);
		defenceDetailsPnl.add(_defenceDmgValueLbl, "cell 4 0");

		JPanel currentUnitActionList = new JPanel();
		currentUnitActionList.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		add(currentUnitActionList, "cell 0 2,grow");
		currentUnitActionList.setLayout(new MigLayout("", "[][grow]", "0[]0"));

		JLabel label_4 = new JLabel("");
		label_4.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/list.png")));
		currentUnitActionList.add(label_4, "cell 0 0");

		JPanel panel_2 = new JPanel();
		currentUnitActionList.add(panel_2, "cell 1 0,grow");
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 1));

		_moveBtn = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				Assert.precondition(_frame.getCurrentUnit().getMove() > 0, "No move point");
				_frame.getBattleBoard().activateMoveUnitAction(_frame.getCurrentUnit());
			}
		});

		_moveBtn.setMargin(new Insets(1, 1, 1, 1));
		_moveBtn.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/move.png")));
		panel_2.add(_moveBtn);

		_bombBtn = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_frame.getBattleBoard().activateBombAction(_frame.getCurrentUnit());
			}
		});
		_bombBtn.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_on.png")));
		_bombBtn.setMargin(new Insets(6, 1, 6, 1));
		panel_2.add(_bombBtn);

		_loadTank = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_frame.getBattleBoard().activateLoadTankAction((AbstractGroundLoadableUnit) _frame.getCurrentUnit());
			}
		});
		_loadTank.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/tank_load.png")));
		_loadTank.setMargin(new Insets(1, 1, 1, 1));

		panel_2.add(_loadTank);

		_attackBtn = new JButton(new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_frame.getBattleBoard().activateAttackAction(_frame.getCurrentUnit());

			}
		});
		_attackBtn.setMargin(new Insets(6, 1, 5, 1));
		_attackBtn.setIcon(new ImageIcon(CurrentUnitPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_on.png")));
		panel_2.add(_attackBtn);

		_unitConfigurationBtn = new JButton("");
		_unitConfigurationBtn.setAction(new AbstractAction()
		{
			
			@Override
			public void actionPerformed(ActionEvent p_arg0)
			{
				final RenameUnitDlg renameDlg = new RenameUnitDlg(_frame, _frame.getCurrentUnit());
				renameDlg.pack();
				renameDlg.setVisible(true);
				renameDlg.dispose();
			}
		});
		_unitConfigurationBtn.setIcon(new ImageIcon(CurrentUnitPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/configuration.png")));
		_unitConfigurationBtn.setMargin(new Insets(1, 1, 1, 1));
		panel_2.add(_unitConfigurationBtn);
	}

	public final void preparePanel(final AbstractUnit p_currentUnit)
	{
		final UnitTypeCst unitType = p_currentUnit.getUnitType();
		_unitTypeIcon.setIcon(_iconFactory.getUnitIconByType(	unitType,
																p_currentUnit.getPlayer().getPlayerId(),
																IconFactory.SIZE_DOUBLE_SIZE, 
																true));
		_idValueLbl.setText(p_currentUnit.getId().toString());
		_unitNameValueLbl.setText(p_currentUnit.getUnitName());
		_hpValueLbl.setText(p_currentUnit.getHitPoints() + "/" + p_currentUnit.getHitPointsCapacity());
		_moveValueLbl.setText(p_currentUnit.getMove() + "/" + p_currentUnit.getMovementCapacity());
		if (UnitTypeCst.TANK.equals(unitType))
		{
			_fuelValueLbl.setText("- / -");
			_attackIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_on.png")));
			_attackDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_attack_on.png")));
			_nbAttackValueLbl.setText(p_currentUnit.getNbAttack() + "/" + p_currentUnit.getNbAttackCapacity());
			_attackDmgValueLbl.setText(p_currentUnit.getMinAttack() + " - " + p_currentUnit.getMaxAttack());
			_bombardmebtIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_off.png")));
			_bombardmentDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_attack_off.png")));
			_nbBombardmentValueLbl.setText("- / -");
			_bombardmentDmgValueLbl.setText(" - ");
			_defenceIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_on.png")));
			_defenceDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
			        .getResource("/org/jocelyn_chaumel/firststrike/resources/defence_attack_on.png")));
			_nbDefenceValueLbl.setText(p_currentUnit.getNbDefense() + "/" + p_currentUnit.getNbDefenseCapacity());
			_defenceDmgValueLbl.setText(p_currentUnit.getMinDefense() + " - " + p_currentUnit.getMaxDefense());
		} else if (UnitTypeCst.FIGHTER.equals(unitType))
		{
			_fuelValueLbl.setText(p_currentUnit.getFuel() + "/" + p_currentUnit.getFuelCapacity());
			_attackIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_on.png")));
			_attackDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_on.png")));
			_attackDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_attack_on.png")));
			_nbAttackValueLbl.setText(p_currentUnit.getNbAttack() + "/" + p_currentUnit.getNbAttackCapacity());
			_attackDmgValueLbl.setText(p_currentUnit.getMinAttack() + " - " + p_currentUnit.getMaxAttack());
			_bombardmebtIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_off.png")));
			_bombardmentDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_attack_off.png")));
			_nbBombardmentValueLbl.setText("- / -");
			_bombardmentDmgValueLbl.setText(" - ");
			_defenceIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_on.png")));
			_defenceDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_attack_on.png")));
			_nbDefenceValueLbl.setText(p_currentUnit.getNbDefense() + "/" + p_currentUnit.getNbDefenseCapacity());
			_defenceDmgValueLbl.setText(p_currentUnit.getMinDefense() + " - " + p_currentUnit.getMaxDefense());
		} else if (UnitTypeCst.ARTILLERY.equals(unitType))
		{
			final Artillery artillery = (Artillery) p_currentUnit;
			_fuelValueLbl.setText("- / -");
			_attackIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_off.png")));
			_attackDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_attack_off.png")));
			_nbAttackValueLbl.setText("- / -");
			_attackDmgValueLbl.setText(" - ");
			_bombardmebtIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_on.png")));
			_bombardmentDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_attack_on.png")));
			_nbBombardmentValueLbl.setText(artillery.getNbBombardment() + "/" + artillery.getNbBombardmentCapacity());
			_bombardmentDmgValueLbl.setText(artillery.getMinBombardmentAttack() + "-"
					+ artillery.getMaxBombardmentAttack());
			_defenceIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_off.png")));
			_defenceDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
       				.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_attack_on.png")));
			_nbDefenceValueLbl.setText("- / -");
			_defenceDmgValueLbl.setText(" - ");
		} else if (UnitTypeCst.TRANSPORT_SHIP.equals(unitType))
		{
			_fuelValueLbl.setText("- / -");
			_attackIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_off.png")));
			_attackDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_attack_off.png")));
			_nbAttackValueLbl.setText("- / -");
			_attackDmgValueLbl.setText(" - ");
			_bombardmebtIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_off.png")));
			_bombardmentDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_attack_off.png")));
			_nbBombardmentValueLbl.setText("- / -");
			_bombardmentDmgValueLbl.setText(" - ");
			_defenceIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_off.png")));
			_defenceDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
      				.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_attack_on.png")));
			_nbDefenceValueLbl.setText("- / -");
			_defenceDmgValueLbl.setText(" - ");
			
		} else if (UnitTypeCst.DESTROYER.equals(unitType))
		{
			_fuelValueLbl.setText("- / -");
			_attackIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_on.png")));
			_attackDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_attack_on.png")));
			_nbAttackValueLbl.setText(p_currentUnit.getNbAttack() + "/" + p_currentUnit.getNbAttackCapacity());
			_attackDmgValueLbl.setText(p_currentUnit.getMinAttack() + " - " + p_currentUnit.getMaxAttack());
			_bombardmebtIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_off.png")));
			_bombardmentDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_attack_off.png")));
			_nbBombardmentValueLbl.setText("- / -");
			_bombardmentDmgValueLbl.setText(" - ");
			_defenceIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_on.png")));
			_defenceDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_attack_on.png")));
			_nbDefenceValueLbl.setText(p_currentUnit.getNbDefense() + "/" + p_currentUnit.getNbDefenseCapacity());
			_defenceDmgValueLbl.setText(p_currentUnit.getMinDefense() + " - " + p_currentUnit.getMaxDefense());
		} else if (UnitTypeCst.BATTLESHIP.equals(unitType))
		{
			final Battleship battleship = (Battleship) p_currentUnit;
			_fuelValueLbl.setText("- / -");
			_attackIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_on.png")));
			_attackDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/attack_attack_on.png")));
			_nbAttackValueLbl.setText(p_currentUnit.getNbAttack() + "/" + p_currentUnit.getNbAttackCapacity());
			_attackDmgValueLbl.setText(p_currentUnit.getMinAttack() + " - " + p_currentUnit.getMaxAttack());
			_bombardmebtIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_on.png")));
			_bombardmentDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/bomb_attack_on.png")));
			_nbBombardmentValueLbl.setText(battleship.getNbBombardment() + "/" + battleship.getNbBombardmentCapacity());
			_bombardmentDmgValueLbl.setText(battleship.getMinBombardmentAttack() + "-"
					+ battleship.getMaxBombardmentAttack());
			_defenceIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_on.png")));
			_defenceDmgIcon.setIcon(new ImageIcon(CurrentUnitPnl.class
					.getResource("/org/jocelyn_chaumel/firststrike/resources/defence_attack_on.png")));
			_nbDefenceValueLbl.setText(p_currentUnit.getNbDefense() + "/" + p_currentUnit.getNbDefenseCapacity());
			_defenceDmgValueLbl.setText(p_currentUnit.getMinDefense() + " - " + p_currentUnit.getMaxDefense());
		}

		_moveBtn.setEnabled(p_currentUnit.getMove() > 0);
		_attackBtn.setEnabled(p_currentUnit.getNbAttack() > 0 && p_currentUnit.getMove() > 0);
		_loadTank.setEnabled((p_currentUnit instanceof Tank || p_currentUnit instanceof Artillery) && p_currentUnit.getMove() == p_currentUnit.getMovementCapacity());
		if (UnitTypeCst.BATTLESHIP.equals(unitType) || UnitTypeCst.ARTILLERY.equals(unitType))
		{
			_bombBtn.setEnabled(((IBombardment) p_currentUnit).getNbBombardment() > 0 && p_currentUnit.getMove() > 0);
		} else
		{
			_bombBtn.setEnabled(false);
		}
	}

}
