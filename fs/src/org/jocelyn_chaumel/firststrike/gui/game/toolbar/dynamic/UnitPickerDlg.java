package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;

public class UnitPickerDlg extends FSDialog
{
	private final UnitPickerPnl _unitPickerPnl;

	public UnitPickerDlg(final FSFrame p_frame, final AbstractUnitList p_unitList, final String p_question)
	{
		super(p_frame, "", true);
		_unitPickerPnl = new UnitPickerPnl(p_unitList, p_question);
		getContentPane().add(_unitPickerPnl);
	}

	public final AbstractUnit getSelectedUnit()
	{
		return _unitPickerPnl.getSelectedUnit();
	}
}
