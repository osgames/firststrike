package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collections;
import java.util.ResourceBundle;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.UnitListComparator;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSFrameEvent;
import org.jocelyn_chaumel.firststrike.gui.game.event.IFSFrameEventListener;
import org.jocelyn_chaumel.tools.data_type.Coord;

import net.miginfocom.swing.MigLayout;

public class CurrentUnitListPnl extends JPanel implements MouseListener, IFSFrameEventListener
{
	private final FSFrame _frame;
	private final DefaultListModel<AbstractUnit> _listModel;
	private final UnitListComparator _unitComparator = new UnitListComparator();
	private FirstStrikeGame _currentGame = null;
	private HumanPlayer _humanPlayer = null;
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	/** Creates new form UnitListContext */
	public CurrentUnitListPnl()
	{
		_listModel = new DefaultListModel<AbstractUnit>();
		initComponents();
		_frame = null;
	}

	public CurrentUnitListPnl(final FSFrame p_frame)
	{
		initComponents();
		_frame = p_frame;
		_listModel = new DefaultListModel<AbstractUnit>();
		_unitList.setCellRenderer(new UnitItemListRenderer());
		_unitList.setModel(_listModel);
		_unitList.addMouseListener(this);
		setEnabled(false);
	}

	public void preparePanel()
	{
		_listModel.clear();
		if (_frame.isGameInProgesss())
		{
			final Coord currentCoord = _frame.getCurrentCell().getCellCoord();
			final FirstStrikeGame game = _frame.getGame();
			final AbstractUnitList unitList = game.getCurrentPlayer().getUnitIndex().getUnitListAt(currentCoord);
			if (unitList == null || unitList.size() == 0)
			{
				_listModel.clear();
				return;
			}
			final AbstractUnit currentUnit = _frame.getCurrentUnit();
			Collections.sort(unitList, _unitComparator);
			for (AbstractUnit unit : unitList)
			{
				_listModel.addElement(unit);
			}

			_unitList.setSelectedValue(currentUnit, true);
		}
	}

	@Override
	public void mouseClicked(MouseEvent p_e)
	{
		if (p_e.getButton() != MouseEvent.BUTTON1)
		{
			return;
		}

		final JList source = (JList) p_e.getSource();
		AbstractUnit unit = (AbstractUnit) source.getSelectedValue();
		if (unit != null)
		{
			_frame.setCurrentUnit(this, unit);
		}

		if (p_e.getClickCount() == 2)
		{
			if (unit.isTurnCompleted())
			{
				unit.resetTurnCompletedFlag();
			} else
			{
				unit.markAsTurnCompleted();
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mouseExited(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mousePressed(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mouseReleased(MouseEvent p_e)
	{
		// NOP
	}

	private void initComponents()
	{
		jScrollPane1 = new JScrollPane();
		_unitList = new JList<AbstractUnit>();
		jLabel1 = new JLabel();
		jLabel1.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		jLabel1.setText(_bundle.getString("fs.unit_list_card.unit_list")); //$NON-NLS-1$

		setLayout(new MigLayout("", "[280px]", "[14px][96px]"));
		_unitList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jScrollPane1.setViewportView(_unitList);
		add(jScrollPane1, "cell 0 1,grow");
		add(jLabel1, "cell 0 0,alignx left,aligny top");
	}

	// Variables declaration - do not modify
	private JLabel jLabel1;
	private JList<AbstractUnit> _unitList;
	private JScrollPane jScrollPane1;

	// End of variables declaration

	@Override
	public void loadGameEvent(final FSFrameEvent p_event)
	{
		setEnabled(true);
		_currentGame = p_event.getGame();
		_humanPlayer = (HumanPlayer) _currentGame.getPlayerMgr().getCurrentPlayer();
	}

	@Override
	public void closeGameEvent(final FSFrameEvent p_event)
	{
		_currentGame = null;
		_humanPlayer = null;
		_listModel.clear();
		setEnabled(false);
	}

	@Override
	public void conquerCityEvent(final FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostCityEvent(final FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostUnitEvent(final FSFrameEvent p_event)
	{
		final AbstractUnit unit = p_event.getCurrentUnit();
		final int size = _listModel.getSize();
		AbstractUnit currentUnit = null;
		for (int i = 0; i < size; i++)
		{
			currentUnit = _listModel.get(i);
			if (currentUnit.getUnitId().equals(unit.getUnitId()))
			{
				_listModel.remove(i);
				break;
			}
		}
	}

	@Override
	public void updateCurrentCellEvent(final FSFrameEvent p_event)
	{
		// TODO BZ Comment code on 4-Nov-2018
		// refreshUnitList(p_event);
	}

	@Override
	public void updateCurrentUnitEvent(final FSFrameEvent p_event)
	{
		// TODO BZ Comment code on 4-Nov-2018
		// refreshUnitList(p_event);
	}

	/**
	// TODO BZ Comment code on 4-Nov-2018
	private final void refreshUnitList(final FSFrameEvent p_event)
	{
		_listModel.clear();
		final Coord position = p_event.getCurrentCell().getCellCoord();
		final AbstractUnitList completeUnitList = _humanPlayer.getUnitList();
		AbstractUnitList unitList = completeUnitList.getUnitAt(position);
		if (unitList.isEmpty())
		{
			AbstractUnitList enemySet = _humanPlayer.getEnemySet();
			unitList = enemySet.getUnitAt(position);
		}

		if (unitList.isEmpty())
		{
			return;
		}

		Collections.sort(unitList, _unitComparator);
		for (AbstractUnit currentUnit : unitList)
		{
			_listModel.addElement(currentUnit);
		}
		_unitList.setSelectedValue(p_event.getCurrentUnit(), true);
	}
	
	*/

	@Override
	public void unitCreatedEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void unitMovedEvent(FSFrameEvent p_event)
	{
		// NOP
	}

}
