package org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.event;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashSet;
import java.util.ResourceBundle;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.EventLog;
import org.jocelyn_chaumel.firststrike.core.player.EventLogList;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.VerticalToolBar;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class EventLogMessageJList extends JList
{
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	private final static IconFactory _iconFactory = IconFactory.getInstance();
	private final DefaultListModel<EventTypeItem> _model = new DefaultListModel<EventTypeItem>();
	private HumanPlayer _currentPlayer;
	private FSMap _currentMap;
	private final HashSet<Short> _eventLogItemList = new HashSet<Short>();
	private VerticalToolBar _verticalToolBar = null;

	public EventLogMessageJList()
	{
		super();
		setModel(_model);
		setCellRenderer(new EventJListRenderer());
		addMouseListener(new EventJListMouseListener());
	}

	public final void setVerticalToolBar(final VerticalToolBar p_verticalToolBar)
	{
		_verticalToolBar = p_verticalToolBar;
	}

	public final void setGame(final AbstractPlayer p_player, final FSMap p_map)
	{
		Assert.precondition(_model.isEmpty(), "Model is not empty");

		_currentPlayer = (HumanPlayer) p_player;
		_currentMap = p_map;

		refreshItemList();
	}

	public void refreshItemList()
	{
		_model.clear();
		final EventLogList citylostList = _currentPlayer.getEventLog()
				.getEventLogListByType(EventLog.EL_TYPE_CITY_LOST);
		if (citylostList.isEmpty())
		{
			_eventLogItemList.remove(EventTypeItem.CITY_LOST);
		} else
		{
			_model.addElement(new EventTypeItem(EventTypeItem.CITY_LOST));
			_eventLogItemList.add(EventTypeItem.CITY_LOST);

			for (EventLog event : citylostList)
			{
				_model.addElement(new EventTypeItem(event));
			}
		}

		final EventLogList unitLostList = _currentPlayer.getEventLog()
				.getEventLogListByType(EventLog.EL_TYPE_UNIT_LOST);
		if (unitLostList.isEmpty())
		{
			_eventLogItemList.remove(EventTypeItem.UNIT_LOST);

		} else
		{
			_model.addElement(new EventTypeItem(EventTypeItem.UNIT_LOST));
			_eventLogItemList.add(EventTypeItem.UNIT_LOST);

			for (EventLog event : unitLostList)
			{
				_model.addElement(new EventTypeItem(event));
			}
		}

		final EventLogList bombardmentList = _currentPlayer.getEventLog()
				.getEventLogListByType(EventLog.EL_TYPE_ENEMY_BOMBARDMENT);
		if (bombardmentList.isEmpty())
		{
			_eventLogItemList.remove(EventTypeItem.BOMBARDMENT);

		} else
		{
			_model.addElement(new EventTypeItem(EventTypeItem.BOMBARDMENT));
			_eventLogItemList.add(EventTypeItem.BOMBARDMENT);

			for (EventLog event : bombardmentList)
			{
				_model.addElement(new EventTypeItem(event));
			}
		}

		final EventLogList unitCreatedList = _currentPlayer.getEventLog()
				.getEventLogListByType(EventLog.EL_TYPE_UNIT_CREATION);
		if (unitCreatedList.isEmpty())
		{
			_eventLogItemList.remove(EventTypeItem.UNIT_CREATED);

		} else
		{
			_model.addElement(new EventTypeItem(EventTypeItem.UNIT_CREATED));
			_eventLogItemList.add(EventTypeItem.UNIT_CREATED);

			for (EventLog event : unitCreatedList)
			{
				_model.addElement(new EventTypeItem(event));
			}
		}
	}

	public void closeGame()
	{
		_model.clear();
	}

	/**
	 * Renderer for the AreaCityJList.
	 * 
	 * @author Jocelyn Chaumel
	 */
	private class EventJListRenderer implements ListCellRenderer
	{

		private final JLabel _label = new JLabel();
		private final EventUnitItemPnl _unitPnl = new EventUnitItemPnl();
		private final EventCityItemPnl _cityPnl = new EventCityItemPnl();
		private final EventBombardmentItemPnl _bombPnl = new EventBombardmentItemPnl();

		@Override
		public Component getListCellRendererComponent(	JList p_list,
														Object p_value,
														int p_index,
														boolean p_isSelected,
														boolean p_cellHasFocus)
		{
			EventTypeItem item = (EventTypeItem) p_value;

			if (item.getType() != 0)
			{

				_label.setText(_bundle.getString("fs.vertical_bar.event_type." + item.getType()));
				_label.setBorder(javax.swing.BorderFactory.createRaisedBevelBorder());
				_label.setBackground(Color.LIGHT_GRAY);

				return _label;
			} else if (item.getEvent().getMsgType() == EventLog.EL_TYPE_UNIT_LOST
					|| item.getEvent().getMsgType() == EventLog.EL_TYPE_UNIT_CREATION)
			{
				if (p_isSelected)
				{
					_unitPnl.setOpaque(true);
					_unitPnl.setBackground(Color.LIGHT_GRAY);
				} else
				{
					_unitPnl.setOpaque(false);
					_unitPnl.setBackground(Color.WHITE);
				}
				_unitPnl.updatePanel(item.getEvent());

				return _unitPnl;
			} else if (item.getEvent().getMsgType() == EventLog.EL_TYPE_CITY_LOST)
			{
				if (p_isSelected)
				{
					_cityPnl.setOpaque(true);
					_cityPnl.setBackground(Color.LIGHT_GRAY);
				} else
				{
					_cityPnl.setOpaque(false);
					_cityPnl.setBackground(Color.WHITE);
				}
				_cityPnl.updatePanel(item.getEvent());

				return _cityPnl;
			} else if (item.getEvent().getMsgType() == EventLog.EL_TYPE_ENEMY_BOMBARDMENT)
			{
				if (p_isSelected)
				{
					_bombPnl.setOpaque(true);
					_bombPnl.setBackground(Color.LIGHT_GRAY);
				} else
				{
					_bombPnl.setOpaque(false);
					_bombPnl.setBackground(Color.WHITE);
				}

				_bombPnl.updatePanel(item.getEvent());

				return _bombPnl;
			} else
			{
				throw new FSFatalException("Unsupported EventLog Type : " + item.getEvent().getMsgType());
			}
		}
	}

	private class EventJListMouseListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent p_event)
		{
			final JList myList = (JList) p_event.getSource();

			final Object value = myList.getSelectedValue();
			if (value == null)
			{
				return;
			}
			final int index = myList.getSelectedIndex();
			final DefaultListModel model = (DefaultListModel) myList.getModel();
			if (!(value instanceof EventTypeItem))
			{
				throw new FSFatalException("Unsupported Data Type : " + value.getClass().getName());
			}
			EventTypeItem eventType = (EventTypeItem) value;
			EventTypeItem subEvent;

			if (eventType.getType() != 0)
			{

				if (_eventLogItemList.contains(eventType.getType()))
				{
					do
					{
						subEvent = (EventTypeItem) model.get(index + 1);
						if (subEvent.getType() != 0)
						{
							break;
						}
						model.remove(index + 1);
					} while (index + 1 < model.size());
					_eventLogItemList.remove(eventType.getType());
				} else
				{
					final EventLogList eventList;
					switch (eventType.getType())
					{
					case EventTypeItem.UNIT_LOST:

						eventList = _currentPlayer.getEventLog().getEventLogListByType(EventLog.EL_TYPE_UNIT_LOST);
						break;
					case EventTypeItem.BOMBARDMENT:
						eventList = _currentPlayer.getEventLog().getEventLogListByType(EventLog.EL_TYPE_ENEMY_BOMBARDMENT);
						break;
					case EventTypeItem.CITY_LOST:
						eventList = _currentPlayer.getEventLog().getEventLogListByType(EventLog.EL_TYPE_CITY_LOST);
						break;
					case EventTypeItem.UNIT_CREATED:
						eventList = _currentPlayer.getEventLog().getEventLogListByType(EventLog.EL_TYPE_UNIT_CREATION);
						break;
					default:
						throw new FSFatalException("Unsupported EventType: " + eventType.getType());
					}

					int count = 0;
					for (EventLog event : eventList)
					{
						count++;
						_model.add(index + count, new EventTypeItem(event));
					}
					_eventLogItemList.add(eventType.getType());
				}

			} else
			{
				final EventLog event = eventType.getEvent();
				final Coord coord = event.getCoord();
				_verticalToolBar.setCurrentCell(coord, true);
			}
		}

		@Override
		public void mouseEntered(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mouseExited(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mousePressed(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mouseReleased(MouseEvent p_e)
		{
			// NOP
		}

	}

}
