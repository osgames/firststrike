package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.gui.game.menu.attack.EnemyUnitItemPnl;
import org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.unit.UnitItemPnl;

public class UnitItemListRenderer implements ListCellRenderer
{
	private final UnitItemPnl _panel = new UnitItemPnl();
	private final EnemyUnitItemPnl _enemyPanel = new EnemyUnitItemPnl();
	private final JLabel _emptyLabel = new JLabel();

	public UnitItemListRenderer()
	{
		_emptyLabel.setBackground(Color.WHITE);
	}

	@Override
	public Component getListCellRendererComponent(	JList p_list,
													Object p_value,
													int p_index,
													boolean p_isSelected,
													boolean p_cellHasFocus)
	{
		if (p_value == null)
		{
			return _emptyLabel;
		} else if (p_value instanceof AbstractUnit)
		{
			final AbstractUnit unit = (AbstractUnit) p_value;
			final AbstractPlayer player = unit.getPlayer();
			if (player instanceof HumanPlayer)
			{
				if (player.isLiving())
				{
					_panel.refreshPanel(unit, p_isSelected);
				}
				return _panel;
			} else
			{
				_enemyPanel.refreshPanel(unit, p_isSelected);
				return _enemyPanel;
			}

		} else
		{
			throw new FSFatalException("Unsupported JList item detected " + p_value.getClass().getName());
		}
	}
}
