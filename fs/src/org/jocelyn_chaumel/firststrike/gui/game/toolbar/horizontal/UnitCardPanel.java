/*
 * UnitCardPanel.java
 *
 * Created on 9 janvier 2009, 13:31
 */

package org.jocelyn_chaumel.firststrike.gui.game.toolbar.horizontal;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.ILoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.IUnitContainer;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.menu.rename.RenameUnitDlg;
import org.jocelyn_chaumel.tools.StringManipulatorHelper;

/**
 * This Panel describes an unit in the First Strike's context bar.
 * 
 * @author Joss
 */
public class UnitCardPanel extends javax.swing.JPanel
{

	private final static IconFactory _iconFactory = IconFactory.getInstance();

	private AbstractUnit _unit;
	private final FSFrame _frame;
	private final DefaultListModel<ILoadableUnit> _listModel;

	/**
	 * Creates new form UnitCardPanel
	 */
	public UnitCardPanel()
	{
		initComponents();
		_unit = null;
		_frame = null;
		_listModel = null;
	}

	/**
	 * Build the panel taking into account the current player and the unit's
	 * player.
	 * 
	 * @param p_unit Unit to describe
	 * @param p_player Current player.
	 */
	public UnitCardPanel(final AbstractUnit p_unit, final FSFrame p_frame)
	{
		initComponents();
		_frame = p_frame;
		_listModel = new DefaultListModel<ILoadableUnit>();
		_containedUnitListLst.setModel(_listModel);
		_containedUnitListLst.setCellRenderer(new UnitJListRenderer());

		updateCardProperties(p_unit);
	}

	public final void updateUnitName(final String p_unitName)
	{
		_unitName.setText(p_unitName);
	}

	public void updateCardProperties(final AbstractUnit p_unit)
	{

		_unit = p_unit;
		if (p_unit instanceof TransportShip)
		{
		} else
		{

		}

		final AbstractPlayer unitPlayer = p_unit.getPlayer();
		final ImageIcon unitIcon = _iconFactory.getUnitIconByType(	p_unit.getUnitType(),
																	unitPlayer.getPlayerId(),
																	IconFactory.SIZE_DOUBLE_SIZE, p_unit.isLiving());
		_unitIconLbl.setIcon(unitIcon);
		_unitName.setText(p_unit.getBestUnitName());

		if (unitPlayer instanceof HumanPlayer || ApplicationConfigurationMgr.getInstance().isDebugMode())
		{
			buildFriendlyUnitCardPanel(p_unit);
		} else
		{
			buildEnemyUnitCardPanel(p_unit);
		}
	}

	private void buildEnemyUnitCardPanel(final AbstractUnit p_unit)
	{
		_movePointsLbl.setVisible(false);
		_movePoints.setVisible(false);

		_hitPointsLbl.setVisible(false);
		_hitPoints.setVisible(false);

		_nbrAttackLbl.setVisible(false);
		_nbrAttack.setVisible(false);

		_fuelLbl.setVisible(false);
		_fuel.setVisible(false);

		_capacityLbl.setVisible(false);
		_capacity.setVisible(false);

		_containedUnitListLst.setVisible(false);
		_unitListScrollPane.setVisible(false);

	}

	private void buildFriendlyUnitCardPanel(final AbstractUnit p_unit)
	{
		// Hit points
		final String hitPointsText = StringManipulatorHelper.formatProperty(p_unit.getHitPoints(),
																			p_unit.getHitPointsCapacity());
		_hitPoints.setText(hitPointsText);
		_hitPoints.setVisible(true);
		_hitPointsLbl.setVisible(true);

		// Move points
		final String moveText = StringManipulatorHelper.formatProperty(p_unit.getMove(), p_unit.getMovementCapacity());
		_movePoints.setText(moveText);
		_movePoints.setVisible(true);
		_movePointsLbl.setVisible(true);

		// NB Attack
		final String nbAttackText = StringManipulatorHelper.formatProperty(	p_unit.getNbAttack(),
																			p_unit.getNbAttackCapacity());
		_nbrAttack.setText(nbAttackText);
		_nbrAttack.setVisible(true);
		_nbrAttackLbl.setVisible(true);

		if (p_unit instanceof Fighter)
		{
			final String fuelText = StringManipulatorHelper.formatProperty(p_unit.getFuel(), p_unit.getFuelCapacity());
			_fuel.setText(fuelText);
			_fuelLbl.setVisible(true);
			_fuel.setVisible(true);
		} else
		{
			_fuelLbl.setVisible(false);
			_fuel.setVisible(false);
		}

		if (p_unit instanceof TransportShip)
		{
			final String capacityText = StringManipulatorHelper.formatProperty(	((IUnitContainer)p_unit).getFreeSpace(),
																				p_unit.getSpaceCapacity());
			_capacity.setText(capacityText);
			refreshContainedUnitList((TransportShip) p_unit);
			_capacityLbl.setVisible(true);
			_capacity.setVisible(true);
			_containedUnitListLst.setVisible(true);
			_unitListScrollPane.setVisible(true);

		} else
		{
			_capacityLbl.setVisible(false);
			_capacity.setVisible(false);
			_containedUnitListLst.setVisible(false);

			_unitListScrollPane.setVisible(false);
		}
	}

	public final void refreshContainedUnitList(final TransportShip p_transport)
	{
		_listModel.clear();
		final ArrayList<ILoadableUnit> unitList = p_transport.getContainedUnitList();

		final Iterator<ILoadableUnit> unitIter = unitList.iterator();
		while (unitIter.hasNext())
		{
			_listModel.addElement(unitIter.next());
		}

	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents()
	{

		_unitIconLbl = new javax.swing.JLabel();
		_unitNameLbl = new javax.swing.JLabel();
		_renameUnitBtn = new javax.swing.JButton();
		_unitName = new javax.swing.JLabel();
		_hitPointsLbl = new javax.swing.JLabel();
		_hitPoints = new javax.swing.JLabel();
		_movePointsLbl = new javax.swing.JLabel();
		_movePoints = new javax.swing.JLabel();
		_nbrAttackLbl = new javax.swing.JLabel();
		_nbrAttack = new javax.swing.JLabel();
		_fuelLbl = new javax.swing.JLabel();
		_fuel = new javax.swing.JLabel();
		_unitListScrollPane = new javax.swing.JScrollPane();
		_containedUnitListLst = new javax.swing.JList<ILoadableUnit>();
		_capacityLbl = new javax.swing.JLabel();
		_capacity = new javax.swing.JLabel();

		setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
		setPreferredSize(new java.awt.Dimension(303, 185));

		_unitIconLbl.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/org/jocelyn_chaumel/firststrike/resources/size_40x40/unit/tank_p1.gif"))); // NOI18N

		java.util.ResourceBundle bundle = java.util.ResourceBundle
				.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle"); // NOI18N
		_unitNameLbl.setText(bundle.getString("fs.unit_card.unit_name")); // NOI18N

		_renameUnitBtn.setText("...");
		_renameUnitBtn.setFocusPainted(false);
		_renameUnitBtn.setMargin(new java.awt.Insets(1, 2, 1, 2));
		_renameUnitBtn.setPreferredSize(new java.awt.Dimension(15, 15));
		_renameUnitBtn.addActionListener(new java.awt.event.ActionListener()
		{
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				renameUnitBtnActionPerformed(evt);
			}
		});

		_unitName.setForeground(new java.awt.Color(0, 0, 204));
		_unitName.setText("No name");

		_hitPointsLbl.setText(bundle.getString("fs.unit_card.hit_point")); // NOI18N

		_hitPoints.setForeground(new java.awt.Color(0, 0, 204));
		_hitPoints.setText("50 / 50");

		_movePointsLbl.setText(bundle.getString("fs.unit_card.move_point")); // NOI18N

		_movePoints.setForeground(new java.awt.Color(0, 0, 204));
		_movePoints.setText("10 / 10");

		_nbrAttackLbl.setText(bundle.getString("fs.unit_card.nbr_attack")); // NOI18N

		_nbrAttack.setForeground(new java.awt.Color(0, 0, 204));
		_nbrAttack.setText("2 / 2");

		_fuelLbl.setText(bundle.getString("fs.unit_card.fuel")); // NOI18N

		_fuel.setForeground(new java.awt.Color(0, 0, 204));
		_fuel.setText("20 / 20");
		_fuel.setMaximumSize(new java.awt.Dimension(25, 14));
		_fuel.setMinimumSize(new java.awt.Dimension(25, 14));
		_fuel.setPreferredSize(new java.awt.Dimension(25, 14));

		_unitListScrollPane.setViewportView(_containedUnitListLst);

		_capacityLbl.setText(bundle.getString("fs.unit_card.capacity")); // NOI18N

		_capacity.setForeground(new java.awt.Color(0, 0, 204));
		_capacity.setText("2 / 2");

		org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(layout
				.createSequentialGroup()
				.add(layout
						.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
						.add(layout
								.createSequentialGroup()
								.addContainerGap()
								.add(_unitIconLbl)
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
								.add(layout
										.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
										.add(layout
												.createSequentialGroup()
												.add(_nbrAttackLbl)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(	_nbrAttack,
														org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
														97,
														Short.MAX_VALUE))
										.add(layout
												.createSequentialGroup()
												.add(_movePointsLbl)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(	_movePoints,
														org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
														42,
														org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(_fuelLbl)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(	_fuel,
														org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
														41,
														org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
										.add(layout
												.createSequentialGroup()
												.add(_hitPointsLbl)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(	_hitPoints,
														org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
														135,
														Short.MAX_VALUE))
										.add(layout
												.createSequentialGroup()
												.add(_unitNameLbl)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(	_renameUnitBtn,
														org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
														org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
														org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(	_unitName,
														org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
														115,
														Short.MAX_VALUE))).add(40, 40, 40))
						.add(layout
								.createSequentialGroup()
								.add(56, 56, 56)
								.add(layout
										.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
										.add(	org.jdesktop.layout.GroupLayout.TRAILING,
												_unitListScrollPane,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												233,
												Short.MAX_VALUE)
										.add(layout
												.createSequentialGroup()
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(_capacityLbl)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(	_capacity,
														org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
														178,
														Short.MAX_VALUE))))).addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(layout
				.createSequentialGroup()
				.addContainerGap()
				.add(layout
						.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
						.add(layout
								.createSequentialGroup()
								.add(layout
										.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
										.add(_unitNameLbl)
										.add(	_renameUnitBtn,
												org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(_unitName))
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
								.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
										.add(_hitPointsLbl).add(_hitPoints))
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
								.add(layout
										.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
										.add(_movePointsLbl)
										.add(_movePoints)
										.add(_fuelLbl)
										.add(	_fuel,
												org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
								.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
										.add(_nbrAttackLbl).add(_nbrAttack))).add(_unitIconLbl))
				.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
				.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(_capacity)
						.add(_capacityLbl))
				.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
				.add(	_unitListScrollPane,
						org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
						52,
						org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addContainerGap(17, Short.MAX_VALUE)));
	}// </editor-fold>//GEN-END:initComponents

	private void renameUnitBtnActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_renameUnitBtnActionPerformed
		final RenameUnitDlg dialog = new RenameUnitDlg(_frame, _unit);
		dialog.pack();
		dialog.setVisible(true);
		_frame.getUnitListContext().refreshCurrentUnit();
		if (dialog.getStatus())
		{
			_unitName.setText(_unit.getBestUnitName());
		}
		dialog.dispose();
	}// GEN-LAST:event_renameUnitBtnActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JLabel _capacity;
	private javax.swing.JLabel _capacityLbl;
	private javax.swing.JList<ILoadableUnit> _containedUnitListLst;
	private javax.swing.JLabel _fuel;
	private javax.swing.JLabel _fuelLbl;
	private javax.swing.JLabel _hitPoints;
	private javax.swing.JLabel _hitPointsLbl;
	private javax.swing.JLabel _movePoints;
	private javax.swing.JLabel _movePointsLbl;
	private javax.swing.JLabel _nbrAttack;
	private javax.swing.JLabel _nbrAttackLbl;
	private javax.swing.JButton _renameUnitBtn;
	private javax.swing.JLabel _unitIconLbl;
	private javax.swing.JScrollPane _unitListScrollPane;
	private javax.swing.JLabel _unitName;
	private javax.swing.JLabel _unitNameLbl;
	// End of variables declaration//GEN-END:variables

}
