package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;

import net.miginfocom.swing.MigLayout;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.gui.commun.FSDialog;

public class UnitPickerPnl extends JPanel
{
	private JList<AbstractUnit> _unitList;
	private JLabel _msgLbl;
	private JButton _okBtn;
	private JButton _cancelBtn;
	private final DefaultListModel<AbstractUnit> _listModel;
	private boolean _isOk = false;
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");
	
	public UnitPickerPnl()
	{
		_listModel = new DefaultListModel<AbstractUnit>();
		buildComponent("Joss");
	}
	
	/**
	 * Create the panel.
	 */
	public UnitPickerPnl(final AbstractUnitList p_unitList, final String p_message)
	{
		buildComponent(p_message);
		_listModel = new DefaultListModel<AbstractUnit>();
		_unitList.setCellRenderer(new UnitItemListRenderer());
		_unitList.setModel(_listModel);
		for (AbstractUnit unit : p_unitList)
		{
			_listModel.addElement(unit);
		}
	}

	private final void buildComponent(final String p_message)
	{
		setLayout(new MigLayout("", "[grow]", "[grow][grow][34.00]"));
		
		_msgLbl = new JLabel();
		_msgLbl.setText(p_message);
		add(_msgLbl, "cell 0 0");
		
		JPanel panel_1 = new JPanel();
		add(panel_1, "cell 0 1,grow");
		panel_1.setLayout(new MigLayout("", "[][][grow]", "[grow]"));
		
		_unitList = new JList<AbstractUnit>();
		_unitList.setBorder(new BevelBorder(BevelBorder.LOWERED));
		_unitList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		panel_1.add(_unitList, "cell 1 0,grow");
		_unitList.addMouseListener(new MouseListener()
		{
			
			@Override
			public void mouseReleased(MouseEvent p_e)
			{
				_okBtn.setEnabled(!_unitList.isSelectionEmpty());
			}
			
			@Override
			public void mousePressed(MouseEvent p_e)
			{
			}
			
			@Override
			public void mouseExited(MouseEvent p_e)
			{
			}
			
			@Override
			public void mouseEntered(MouseEvent p_e)
			{
			}
			
			@Override
			public void mouseClicked(MouseEvent p_e)
			{
				_okBtn.setEnabled(!_unitList.isSelectionEmpty());
			}
		});
		
		JPanel panel = new JPanel();
		add(panel, "cell 0 2,grow");
		panel.setLayout(new MigLayout("", "[grow][][]", "[20]"));
		
		_okBtn = new JButton("New button");
		panel.add(_okBtn, "cell 1 0");
		_okBtn.setAction(new AbstractAction()
		{
			
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				Component unitPickerDlg = (Component)p_e.getSource(); 
				while (!(unitPickerDlg instanceof FSDialog)) 
				{
					unitPickerDlg = unitPickerDlg.getParent();
				} 
				_isOk = true;
				
				((FSDialog) unitPickerDlg).setVisible(false);
			}
		});
		_okBtn.setText(bundle.getString("fs.common.ok_btn"));
		_okBtn.setEnabled(false);
		
		_cancelBtn = new JButton();
		panel.add(_cancelBtn, "cell 2 0");
		_cancelBtn.setAction(new AbstractAction()
		{
			
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				_isOk = false;
				Component component = (Component)p_e.getSource(); 
				while (!(component instanceof FSDialog)) 
				{
					component = component.getParent();
				} 
				
				((FSDialog) component).setVisible(false);
			}
		});
		_cancelBtn.setText(bundle.getString("fs.common.cancel_btn"));
	}
	
	public final AbstractUnit getSelectedUnit()
	{
		if (!_isOk)
		{
			return null;
		} else {
			return _unitList.getSelectedValue();
		}
	}
}
