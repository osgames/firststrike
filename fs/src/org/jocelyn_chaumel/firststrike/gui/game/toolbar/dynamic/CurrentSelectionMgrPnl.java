package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import net.miginfocom.swing.MigLayout;

import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSFrameEvent;
import org.jocelyn_chaumel.firststrike.gui.game.event.IFSFrameEventListener;

public class CurrentSelectionMgrPnl extends JPanel implements IFSFrameEventListener
{
	private CurrentCellCardPnlMgr _currentCellCardPnlMgr;
	private CurrentUnitListCardPnlMgr _currentUnitListPnlMgr;
	private CurrentUnitCardPnlMgr _currentUnitCardPnlMgr;
	private CurrentContainedUnitListCardPnlMgr _currentContainedUnitListPnlMgr;
	private FSFrame _frame = null;

	public CurrentSelectionMgrPnl()
	{
		createComponent();
	}

	public CurrentSelectionMgrPnl(final FSFrame p_frame)
	{
		if (p_frame != null)
		{
			_frame = p_frame;
		}
		createComponent();
	}

	private void createComponent()
	{
		setLayout(new MigLayout("", "[300.00]", "0[]0[]0[]0[]0[grow]0"));

		_currentCellCardPnlMgr = new CurrentCellCardPnlMgr(_frame);
		_currentCellCardPnlMgr.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(_currentCellCardPnlMgr, "cell 0 0,grow");

		_currentUnitListPnlMgr = new CurrentUnitListCardPnlMgr(_frame);
		_currentUnitListPnlMgr.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(_currentUnitListPnlMgr, "cell 0 1,grow");

		_currentUnitCardPnlMgr = new CurrentUnitCardPnlMgr(_frame);
		_currentUnitCardPnlMgr.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(_currentUnitCardPnlMgr, "cell 0 2,grow");

		_currentContainedUnitListPnlMgr = new CurrentContainedUnitListCardPnlMgr(_frame);
		add(_currentContainedUnitListPnlMgr, "cell 0 3,grow");
	}

	@Override
	public void loadGameEvent(FSFrameEvent p_event)
	{
		// _currentCellCardPnlMgr.showCard();
	}
	
	public final void setMouseEnabled(final boolean p_enabled)
	{
		_currentCellCardPnlMgr.setMouseEnabled(p_enabled);
		_currentUnitListPnlMgr.setMouseEnabled(p_enabled);
		_currentUnitCardPnlMgr.setMouseEnabled(p_enabled);
		_currentContainedUnitListPnlMgr.setMouseEnabled(p_enabled);
	}


	@Override
	public void updateCurrentCellEvent(FSFrameEvent p_event)
	{
		_currentCellCardPnlMgr.showCard();
		_currentUnitListPnlMgr.showCard();
		_currentUnitCardPnlMgr.showCard();
		_currentContainedUnitListPnlMgr.showCard();
	}

	@Override
	public void conquerCityEvent(FSFrameEvent p_event)
	{
	}

	@Override
	public void lostCityEvent(FSFrameEvent p_event)
	{
	}

	@Override
	public void lostUnitEvent(FSFrameEvent p_event)
	{
		if (_frame.getCurrentCell().getCellCoord().equals(p_event.getCurrentUnit().getPosition()))
		{
			_currentUnitListPnlMgr.showCard();
			_currentUnitCardPnlMgr.showCard();
			_currentContainedUnitListPnlMgr.showCard();
		}
	}

	@Override
	public void unitCreatedEvent(FSFrameEvent p_event)
	{
	}

	@Override
	public void updateCurrentUnitEvent(FSFrameEvent p_event)
	{
		_currentUnitListPnlMgr.showCard();
		_currentUnitCardPnlMgr.showCard();
		_currentContainedUnitListPnlMgr.showCard();
	}

	@Override
	public void unitMovedEvent(FSFrameEvent p_event)
	{
		_currentUnitListPnlMgr.showCard();
		_currentUnitCardPnlMgr.showCard();
		_currentContainedUnitListPnlMgr.showCard();
	}

	@Override
	public void closeGameEvent(FSFrameEvent p_event)
	{
		_currentCellCardPnlMgr.showCard();
		_currentUnitListPnlMgr.showCard();
		_currentUnitCardPnlMgr.showCard();
		_currentContainedUnitListPnlMgr.showCard();
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event dispatch thread.
	 */
	private static void createAndShowGUI()
	{
		// Create and set up the window.
		JFrame frame = new JFrame("CardLayoutDemo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		frame.getContentPane().add(new CurrentSelectionMgrPnl());

		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] p_params)
	{
		try
		{
			IconFactory.createInstance();
		} catch (IOException ex)
		{
			ex.printStackTrace();
		}
		// Schedule a job for the event dispatch thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				createAndShowGUI();
			}
		});
	}
}
