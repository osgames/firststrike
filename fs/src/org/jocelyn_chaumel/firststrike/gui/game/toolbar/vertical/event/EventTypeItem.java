package org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.event;

import org.jocelyn_chaumel.firststrike.core.player.EventLog;

public class EventTypeItem
{
	
	public final static short CITY_LOST = 1;
	public final static short UNIT_LOST = 2;
	public final static short UNIT_CREATED = 3;
	public final static short BOMBARDMENT = 4;
	
	private final short _type;
	private final EventLog _item;
	
	public EventTypeItem(final short p_type)
	{
		_type = p_type;
		_item = null;
	}
	public EventTypeItem(final EventLog p_event)
	{
		_type = 0;
		_item = p_event;
	}
	
	public final EventLog getEvent()
	{
		return _item;
	}
	
	public final short getType()
	{
		return _type;
	}
}
