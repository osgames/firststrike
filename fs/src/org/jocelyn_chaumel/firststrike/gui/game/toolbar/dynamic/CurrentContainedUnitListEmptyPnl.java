package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import net.miginfocom.swing.MigLayout;

public class CurrentContainedUnitListEmptyPnl extends JPanel
{

	/**
	 * Create the panel.
	 */
	public CurrentContainedUnitListEmptyPnl()
	{
		setOpaque(false);
		setLayout(new MigLayout("", "0[300]0", "0[]0"));

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		lblNewLabel.setIcon(new ImageIcon(CurrentContainedUnitListEmptyPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/current_contained_unit_blank.png")));
		add(lblNewLabel, "cell 0 0");

	}

}
