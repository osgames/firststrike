package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import java.awt.CardLayout;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;

public class CurrentContainedUnitListCardPnlMgr extends JPanel
{
	private static final String CARD_1 = "CARD_1";
	private static final String CARD_2 = "CARD_2";
	private static final String EMPTY_CARD = "CARD_E";

	private CurrentContainedUnitListPnl[] _cardList = null;
	private String[] _cardNameList = new String[] { CARD_1, CARD_2 };
	private final CurrentContainedUnitListEmptyPnl _emptyCard = new CurrentContainedUnitListEmptyPnl();
	private short _currentCard = 0;
	private FSFrame _frame = null;
	private boolean _enabled = true;

	public CurrentContainedUnitListCardPnlMgr()
	{
		super(new CardLayout());
		createComponent();
	}

	public CurrentContainedUnitListCardPnlMgr(final FSFrame p_frame)
	{
		super(new CardLayout());
		_frame = p_frame;
		createComponent();
	}

	private void createComponent()
	{
		if (_frame == null)
		{
			_cardList = new CurrentContainedUnitListPnl[] { new CurrentContainedUnitListPnl(),
					new CurrentContainedUnitListPnl() };
		} else
		{
			_cardList = new CurrentContainedUnitListPnl[] { new CurrentContainedUnitListPnl(_frame),
					new CurrentContainedUnitListPnl(_frame) };
		}

		add(_cardList[0], _cardNameList[0]); // CARD_1
		add(_cardList[1], _cardNameList[1]); // CARD_2
		add(_emptyCard, EMPTY_CARD);
		showCard();
	}
	
	public final void setMouseEnabled(final boolean p_enabled)
	{
		_enabled = p_enabled;
	}

	public void showCard()
	{
		final CardLayout cardLayout = (CardLayout) getLayout();
		if (_frame == null || !_frame.isGameInProgesss() || _frame.getCurrentUnit() == null
				|| !(_frame.getCurrentUnit() instanceof TransportShip) || !_enabled)
		{
			cardLayout.show(this, EMPTY_CARD);
		} else
		{
			_cardList[_currentCard].preparePanel(_frame.getCurrentUnit());
			cardLayout.show(this, _cardNameList[_currentCard]);
			_currentCard = (short) ((_currentCard + 1) % 2);
		}
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event dispatch thread.
	 */
	private static void createAndShowGUI()
	{
		// Create and set up the window.
		JFrame frame = new JFrame("CardLayoutDemo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		frame.getContentPane().add(new CurrentContainedUnitListCardPnlMgr());

		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] p_params)
	{
		try
		{
			IconFactory.createInstance();
		} catch (IOException ex)
		{
			ex.printStackTrace();
		}
		// Schedule a job for the event dispatch thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				createAndShowGUI();
			}
		});

	}
}
