package org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.unit;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collections;

import javax.swing.DefaultListModel;
import javax.swing.JList;

import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitTypeComparator;
import org.jocelyn_chaumel.firststrike.gui.game.toolbar.horizontal.UnitJListRenderer;
import org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.VerticalToolBar;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class UnitJList extends JList
{
	private final static AbstractUnitTypeComparator _unitTypeComparator = new AbstractUnitTypeComparator();

	private final DefaultListModel _model = new DefaultListModel();
	private HumanPlayer _currentPlayer = null;
	private VerticalToolBar _verticalToolBar = null;

	public UnitJList()
	{
		super();
		setModel(_model);
		setCellRenderer(new UnitJListRenderer());
		addMouseListener(new UnitJListMouseListener());
	}

	public final void setVerticalToolBar(final VerticalToolBar p_verticalToolBar)
	{
		_verticalToolBar = p_verticalToolBar;
	}

	public final void setGame(final HumanPlayer p_player)
	{
		_currentPlayer = p_player;

		refreshAllUnitOfList();
	}

	public final void refreshUnitOfList(final AbstractUnit p_unit)
	{
		Assert.check(((DefaultListModel<AbstractUnit>)this.getModel()).contains(p_unit), "This unit doesn't exist in the List.  List is no more up to date.");
		this.setSelectedValue(p_unit, true);
	}

	public final void refreshAllUnitOfList()
	{
		final AbstractUnitList unitList = _currentPlayer.getUnitList();
		Collections.sort(unitList, _unitTypeComparator);
		_model.clear();

		for (AbstractUnit currentUnit : unitList)
		{
			_model.addElement(currentUnit);
		}
	}

	public void closeGame()
	{
		_model.clear();
	}

	private class UnitJListMouseListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent p_event)
		{
			final JList myList = (JList) p_event.getSource();

			final Object value = myList.getSelectedValue();
			if (value == null)
			{
				return;
			}

			if (p_event.getButton() != MouseEvent.BUTTON1)
			{
				return;
			}

			if (!(value instanceof AbstractUnit))
			{
				throw new FSFatalException("Unsupported JList item detected " + value.getClass().getName());
			}

			final AbstractUnit unit = (AbstractUnit) value;
			_verticalToolBar.setCurrentUnit(unit);

			if (p_event.getClickCount() == 2)
			{
				if (unit.isTurnCompleted())
				{
					unit.resetTurnCompletedFlag();
				} else
				{
					unit.markAsTurnCompleted();

				}
			}
		}

		@Override
		public void mouseEntered(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mouseExited(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mousePressed(MouseEvent p_e)
		{
			// NOP
		}

		@Override
		public void mouseReleased(MouseEvent p_e)
		{
			// NOP
		}

	}

}
