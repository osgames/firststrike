package org.jocelyn_chaumel.firststrike.gui.game.toolbar.horizontal;

import java.awt.CardLayout;

import javax.swing.JPanel;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSFrameEvent;
import org.jocelyn_chaumel.firststrike.gui.game.event.IFSFrameEventListener;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * 
 * @author Joss
 */
public class CellCardMgr extends JPanel implements IFSFrameEventListener
{

	// private final static FSLogger _logger =
	// FSLoggerManager.getLogger(CellCardMgr.class.getName());

	private final CardLayout _cardLayout = new CardLayout();
	private final FSFrame _frame;
	private boolean _refreshPossible = false;
	private Cell _currentCell = null;
	private CellCardPanel _firstCard = null;
	private CellCardPanel _secondCard = null;
	private boolean _cellCardCreated = false;
	private boolean _isFirstCard = true;

	/** Creates a new instance of ContextCardPanel */
	public CellCardMgr(final FSFrame p_frame)
	{
		super.setLayout(_cardLayout);
		_frame = p_frame;

		final GenericCellCardPanel genericCard = new GenericCellCardPanel();
		// _cardCacheMap.put(GenericCellCardPanel.CARD_NAME, genericCard);
		add(GenericCellCardPanel.GENERIC_CARD_IDENTIFIER, genericCard);

		final ShadowCardPanel shadowCard = new ShadowCardPanel();
		add(ShadowCardPanel.SHADOW_CARD_IDENTIFIER, shadowCard);

		_cardLayout.show(this, CellCardPanel.FIRST_CARD);
		_currentCell = null;

		p_frame.addActionListener(this);
	}

	private void setCurrentCellInternal(final Cell p_cell)
	{
		if (!_cellCardCreated)
		{

			_firstCard = new CellCardPanel(_frame, p_cell);
			add(CellCardPanel.FIRST_CARD, _firstCard);

			_secondCard = new CellCardPanel(_frame, p_cell);
			add(CellCardPanel.SECOND_CARD, _secondCard);
			_cellCardCreated = true;
		}

		final HumanPlayer human = (HumanPlayer) _frame.getGame().getCurrentPlayer();
		if (!ApplicationConfigurationMgr.getInstance().isDebugMode())
		{
			final CoordSet shadowSet = human.getShadowSet();
			final Coord coord = p_cell.getCellCoord();
			if (shadowSet.contains(coord))
			{
				_cardLayout.show(this, ShadowCardPanel.SHADOW_CARD_IDENTIFIER);
				_refreshPossible = false;
				_currentCell = null;
				return;
			}
		}
		_currentCell = p_cell;
		_refreshPossible = true;

		final CellCardPanel card;
		final String cardName;
		if (_isFirstCard)
		{
			card = _firstCard;
			cardName = CellCardPanel.FIRST_CARD;
		} else
		{
			card = _secondCard;
			cardName = CellCardPanel.SECOND_CARD;
		}

		card.refreshCard(_currentCell);

		_cardLayout.show(this, cardName);
	}

	public void showGenericCard()
	{
		_cardLayout.show(this, GenericCellCardPanel.GENERIC_CARD_IDENTIFIER);
		_refreshPossible = false;
	}

	public final void refreshCurrentCard(final HumanPlayer p_player)
	{
		Assert.preconditionNotNull(p_player, "Human Player");
		if (!_refreshPossible)
		{
			return;
		}
		final CellCardPanel card;
		final String cardName;
		if (_isFirstCard)
		{
			card = _firstCard;
			cardName = CellCardPanel.FIRST_CARD;
		} else
		{
			card = _secondCard;
			cardName = CellCardPanel.SECOND_CARD;
		}

		card.refreshCard(_currentCell);

		_cardLayout.show(this, cardName);
	}

	@Override
	public void closeGameEvent(final FSFrameEvent p_event)
	{
		showGenericCard();
	}

	@Override
	public void conquerCityEvent(final FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostCityEvent(final FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostUnitEvent(final FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void loadGameEvent(final FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void updateCurrentCellEvent(final FSFrameEvent p_event)
	{
		Cell currentCell = (Cell) p_event.getCurrentCell();
		setCurrentCellInternal(currentCell);
	}

	@Override
	public void unitCreatedEvent(final FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void updateCurrentUnitEvent(final FSFrameEvent p_event)
	{
		final Cell newCell = p_event.getCurrentCell();
		if (newCell != _currentCell)
		{
			setCurrentCellInternal(newCell);
		}
	}

	@Override
	public void unitMovedEvent(FSFrameEvent p_event)
	{
		// NOP
	}
}
