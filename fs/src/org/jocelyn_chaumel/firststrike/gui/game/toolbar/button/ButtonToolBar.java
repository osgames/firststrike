package org.jocelyn_chaumel.firststrike.gui.game.toolbar.button;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JToolBar;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSFrameEvent;
import org.jocelyn_chaumel.firststrike.gui.game.event.IFSFrameEventListener;
import org.jocelyn_chaumel.tools.debugging.Assert;

public class ButtonToolBar extends JToolBar implements IFSFrameEventListener
{
	private final static ResourceBundle bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");
	private final FSFrame _frame;
	private final JButton _nextTurnBtn;
	private final JButton _autoMoveSlowerBtn;
	private final JButton _autoMovePauseBtn;
	private final JButton _autoMoveFasterBtn;
	private final IconFactory _iconFactory = IconFactory.getInstance();
	private final ApplicationConfigurationMgr _config = ApplicationConfigurationMgr.getInstance();
	private final static FSLogger _logger = FSLoggerManager.getLogger(ButtonToolBar.class.getName());

	public ButtonToolBar(final FSFrame p_frame)
	{
		_frame = p_frame;
		_frame.addActionListener(this);

		_nextTurnBtn = add(new FSAbstractAction(_frame)
		{
			/**
			 * Invoked when an action occurs.
			 */
			@Override
			public void actionPerformed(ActionEvent p_actionEvent)
			{
				_frame.nextTurn();
				_frame.getBattleBoard().grabFocus();
			}

		});
		_nextTurnBtn.setIcon(_iconFactory.getCommunIcon(IconFactory.COMMON_NEXT_TURN));
		_nextTurnBtn.setToolTipText(bundle.getString("fs.tool_bar.finish_turn"));

		_autoMoveSlowerBtn = add(new FSAbstractAction(_frame)
		{
			/**
			 * Invoked when an action occurs.
			 */
			@Override
			public void actionPerformed(ActionEvent p_actionEvent)
			{
				final int sleep = _config.getAutoMoveSpeed() + 100;
				if (sleep > 4000)
				{
					return;
				}
				try
				{
					_config.setAutoMoveSpeed(sleep);
				} catch (IOException ex)
				{
					ex.printStackTrace();
				}
			}

		});
		_autoMoveSlowerBtn.setIcon(_iconFactory.getCommunIcon(IconFactory.COMMON_AUTO_MOVE_SLOWER_ON));
		_autoMoveSlowerBtn.setToolTipText(bundle.getString("fs.tool_bar.auto_move_slower"));

		_autoMovePauseBtn = add(new FSAbstractAction(_frame)
		{
			/**
			 * Invoked when an action occurs.
			 */
			@Override
			public void actionPerformed(ActionEvent p_actionEvent)
			{
				try
				{

					if (_config.isAutoMoveActivated())
					{
						_logger.playerAction("Auto move deactivated.");
						Assert.check(!_config.toggleAutoMoveActivation(), "Invalid State");
						_autoMovePauseBtn.setIcon(_iconFactory.getCommunIcon(IconFactory.COMMON_AUTO_MOVE_OFF));

					} else
					{
						_logger.playerAction("Auto move activated.");
						Assert.check(_config.toggleAutoMoveActivation(), "Invalid State");
						_autoMovePauseBtn.setIcon(_iconFactory.getCommunIcon(IconFactory.COMMON_AUTO_MOVE_ON));
					}
					
					_frame.setFocusOnBoard();
					
				} catch (IOException ex)
				{
					ex.printStackTrace();
				}
			}

		});

		if (_config.isAutoMoveActivated())
		{
			_autoMovePauseBtn.setIcon(_iconFactory.getCommunIcon(IconFactory.COMMON_AUTO_MOVE_ON));
		} else
		{
			_autoMovePauseBtn.setIcon(_iconFactory.getCommunIcon(IconFactory.COMMON_AUTO_MOVE_OFF));
		}
		_autoMovePauseBtn.setToolTipText(bundle.getString("fs.tool_bar.auto_move"));

		_autoMoveFasterBtn = add(new FSAbstractAction(_frame)
		{
			/**
			 * Invoked when an action occurs.
			 */
			@Override
			public void actionPerformed(ActionEvent p_actionEvent)
			{
				final int sleep = _config.getAutoMoveSpeed() - 100;
				try
				{
					if (sleep < 100)
					{
						_config.setAutoMoveSpeed(100);
					} else if (sleep == 100)
					{
						return;
					} else
					{
						_config.setAutoMoveSpeed(sleep);
					}

				} catch (IOException ex)
				{
					ex.printStackTrace();
				}
			}

		});
		_autoMoveFasterBtn.setIcon(_iconFactory.getCommunIcon(IconFactory.COMMON_AUTO_MOVE_FASTER_ON));
		_autoMoveFasterBtn.setToolTipText(bundle.getString("fs.tool_bar.auto_move_faster"));

		closeGame();
	}

	public final void setMouseEnabled(final boolean p_enabled)
	{
		_nextTurnBtn.setEnabled(p_enabled);
	}

	public final void startAutoMove()
	{
		Assert.check(_config.isAutoMoveActivated(), "Invalid State");
		_autoMovePauseBtn.setIcon(_iconFactory.getCommunIcon(IconFactory.COMMON_AUTO_MOVE_ON));
	}

	private final void loadGame()
	{
		_nextTurnBtn.setEnabled(true);
		_autoMoveSlowerBtn.setEnabled(true);
		_autoMovePauseBtn.setEnabled(true);
		_autoMoveFasterBtn.setEnabled(true);
	}

	private final void closeGame()
	{
		_nextTurnBtn.setEnabled(false);
		_autoMoveSlowerBtn.setEnabled(false);
		_autoMovePauseBtn.setEnabled(false);
		_autoMoveFasterBtn.setEnabled(false);
	}

	abstract class FSAbstractAction extends AbstractAction
	{
		protected final FSFrame _mainFrame;

		FSAbstractAction(final FSFrame p_mainFrame)
		{
			_mainFrame = p_mainFrame;
		}
	}

	@Override
	public void closeGameEvent(final FSFrameEvent p_event)
	{
		closeGame();
	}

	@Override
	public void loadGameEvent(FSFrameEvent p_event)
	{
		loadGame();
	}

	@Override
	public void conquerCityEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostCityEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostUnitEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void updateCurrentCellEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void updateCurrentUnitEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void unitCreatedEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void unitMovedEvent(FSFrameEvent p_event)
	{
		// NOP
	}
}
