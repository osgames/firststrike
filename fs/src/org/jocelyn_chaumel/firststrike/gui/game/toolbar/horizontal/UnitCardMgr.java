package org.jocelyn_chaumel.firststrike.gui.game.toolbar.horizontal;

import java.awt.CardLayout;

import javax.swing.JPanel;

import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.gui.commun.MemoryMgr;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSFrameEvent;
import org.jocelyn_chaumel.firststrike.gui.game.event.IFSFrameEventListener;

public class UnitCardMgr extends JPanel implements IFSFrameEventListener
{
	private final CardLayout _cardLayout = new CardLayout();
	private UnitCardPanel _firstCard = null;
	private UnitCardPanel _secondCard = null;
	private boolean _isFirstCard = true;
	private final FSFrame _frame;

	public UnitCardMgr(final FSFrame p_frame)
	{
		super.setLayout(_cardLayout);
		_frame = p_frame;

		final GenericUnitCardPanel genericUnitCard = new GenericUnitCardPanel();
		add(GenericUnitCardPanel.CARD_NAME, genericUnitCard);
	}

	private void setCurrentUnitInternal(final AbstractUnit p_unit)
	{

		if (_isFirstCard)
		{
			// currentCard = _secondCard;
			if (_secondCard == null)
			{
				_secondCard = new UnitCardPanel(p_unit, _frame);
				add("2", _secondCard);
			}
			_secondCard.updateCardProperties(p_unit);
			_cardLayout.show(this, "2");
		} else
		{
			if (_firstCard == null)
			{
				_firstCard = new UnitCardPanel(p_unit, _frame);
				add("1", _firstCard);
			}
			_firstCard.updateCardProperties(p_unit);
			_cardLayout.show(this, "1");

		}
		_isFirstCard = !_isFirstCard;
		MemoryMgr.log(null, "MEMORY STATUS - show unit card:");
	}

	public final void showGenericCard()
	{
		_cardLayout.show(this, GenericUnitCardPanel.CARD_NAME);

	}

	public final void refreshCurrentUnit()
	{
		final AbstractUnit unit = _frame.getCurrentUnit();
		if (unit == null)
		{
			showGenericCard();
		} else if (_isFirstCard)
		{
			_firstCard.updateCardProperties(unit);
			_cardLayout.show(this, "1");
		} else
		{
			_secondCard.updateCardProperties(unit);
			_cardLayout.show(this, "2");
		}

	}

	@Override
	public void closeGameEvent(final FSFrameEvent p_event)
	{
		showGenericCard();
	}

	@Override
	public void conquerCityEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostCityEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostUnitEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void loadGameEvent(FSFrameEvent p_event)
	{
		showGenericCard();
	}

	@Override
	public void updateCurrentCellEvent(FSFrameEvent p_event)
	{
		final AbstractUnit currentUnit = p_event.getCurrentUnit();
		if (currentUnit == null)
		{
			showGenericCard();
		} else
		{
			setCurrentUnitInternal(currentUnit);
		}
	}

	@Override
	public void updateCurrentUnitEvent(final FSFrameEvent p_event)
	{
		final AbstractUnit currentUnit = p_event.getCurrentUnit();
		setCurrentUnitInternal(currentUnit);
	}

	@Override
	public void unitCreatedEvent(final FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void unitMovedEvent(FSFrameEvent p_event)
	{
		// NOP
	}
}
