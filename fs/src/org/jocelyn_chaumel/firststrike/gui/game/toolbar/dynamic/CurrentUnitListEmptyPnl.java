package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import net.miginfocom.swing.MigLayout;

public class CurrentUnitListEmptyPnl extends JPanel
{
	private JLabel _iconLbl;

	public CurrentUnitListEmptyPnl()
	{
		setLayout(new MigLayout("", "0[300.00]0", "0[]0"));

		_iconLbl = new JLabel("");
		_iconLbl.setBackground(Color.white);
		_iconLbl.setHorizontalAlignment(SwingConstants.CENTER);
		_iconLbl.setIcon(new ImageIcon(CurrentCellEmptyPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/unit_list.png")));
		add(_iconLbl, "cell 0 0");

		setOpaque(true);
		setBackground(Color.white);
	}

}
