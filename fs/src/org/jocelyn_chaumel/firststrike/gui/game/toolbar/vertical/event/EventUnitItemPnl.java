/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CityItemPnl.java
 *
 * Created on 21 juil. 2010, 23:02:17
 */

package org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.event;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import net.miginfocom.swing.MigLayout;

import org.jocelyn_chaumel.firststrike.core.player.EventLog;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;

/**
 * 
 * @author Client
 */
public class EventUnitItemPnl extends javax.swing.JPanel
{
	private final static IconFactory _iconFacoty = IconFactory.getInstance();
	private JLabel _unitTypeIcon;
	private JLabel _unitIdLbl;
	private JLabel _unitCoordLbl;

	/** Creates new form CityItemPnl */
	public EventUnitItemPnl()
	{
		setLayout(new MigLayout("", "0[][20][][180,grow]0", "0[20]0"));
		
		_unitTypeIcon = new JLabel("");
		_unitTypeIcon.setIcon(new ImageIcon(EventUnitItemPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/tank_p1.png")));
		add(_unitTypeIcon, "cell 0 0");
		
		_unitIdLbl = new JLabel("(1)");
		add(_unitIdLbl, "cell 1 0,alignx center");
		
		JLabel targetIcon = new JLabel("");
		targetIcon.setIcon(new ImageIcon(EventUnitItemPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/target.png")));
		add(targetIcon, "cell 2 0");
		
		_unitCoordLbl = new JLabel("1,1");
		add(_unitCoordLbl, "cell 3 0");
	}
	
	public final void updatePanel(final EventLog p_eventLog)
	{
		final ImageIcon unitIcon = _iconFacoty.getUnitIconByType(	p_eventLog.getUnitType(),
		                                                         	p_eventLog.getPlayerId(),
		                                                         	IconFactory.SIZE_SIMPLE_SIZE,
		                                                         	true);
		_unitTypeIcon.setIcon(unitIcon);
		_unitCoordLbl.setText(p_eventLog.getCoord().toStringLight());
		_unitIdLbl.setText(p_eventLog.getMessage());
	}

}
