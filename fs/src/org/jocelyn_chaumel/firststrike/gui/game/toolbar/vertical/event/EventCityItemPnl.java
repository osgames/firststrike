/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CityItemPnl.java
 *
 * Created on 21 juil. 2010, 23:02:17
 */

package org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.event;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import net.miginfocom.swing.MigLayout;

import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.player.EventLog;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;

/**
 * 
 * @author Client
 */
public class EventCityItemPnl extends javax.swing.JPanel
{
	private EventLog _message;
	private JLabel _cityIcon;

	private final static IconFactory _iconFacoty = IconFactory.getInstance();
	private JLabel _cityName;
	private JLabel _cityCoordLbl;

	/** Creates new form CityItemPnl */
	public EventCityItemPnl()
	{
		setLayout(new MigLayout("", "0[][20][][180,grow]0", "0[20]0"));
		
		_cityIcon = new JLabel("");
		_cityIcon.setIcon(new ImageIcon(EventUnitItemPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/tank_p1.png")));
		add(_cityIcon, "cell 0 0");
		
		_cityName = new JLabel("(1)");
		add(_cityName, "cell 1 0,alignx center");
		
		JLabel targetIcon = new JLabel("");
		targetIcon.setIcon(new ImageIcon(EventUnitItemPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/target.png")));
		add(targetIcon, "cell 2 0");
		
		_cityCoordLbl = new JLabel("1,1");
		add(_cityCoordLbl, "cell 3 0");
	}
	
	public final void updatePanel(final EventLog p_event)
	{
		
		_cityIcon.setIcon(_iconFacoty.getSimpleCellIcon(CellTypeCst.CITY, IconFactory.NO_FOG, IconFactory.SIZE_SIMPLE_SIZE, p_event.getCellIdx()));
		_cityCoordLbl.setText(p_event.getCoord().toStringLight());
		_cityName.setText(p_event.getMessage());
	}

}
