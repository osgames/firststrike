/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CityItemPnl.java
 *
 * Created on 21 juil. 2010, 23:02:17
 */

package org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.event;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import net.miginfocom.swing.MigLayout;

import org.jocelyn_chaumel.firststrike.core.player.EventLog;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;

/**
 * 
 * @author Client
 */
public class EventBombardmentItemPnl extends javax.swing.JPanel
{
	private final static IconFactory _iconFacoty = IconFactory.getInstance();
	private JLabel _cellTypeIcon;
	private JLabel _bombCoordLbl;

	/** Creates new form CityItemPnl */
	public EventBombardmentItemPnl()
	{
		setLayout(new MigLayout("", "[][20,grow,left]", "[20]"));
		
		_cellTypeIcon = new JLabel("");
		_cellTypeIcon.setIcon(new ImageIcon(EventUnitItemPnl.class.getResource("/org/jocelyn_chaumel/firststrike/resources/size_20x20/unit/tank_p1.png")));
		add(_cellTypeIcon, "cell 0 0");
		
		_bombCoordLbl = new JLabel("(1)");
		add(_bombCoordLbl, "cell 1 0");
	}
	
	public final void updatePanel(final EventLog p_eventLog)
	{
		final ImageIcon unitIcon = _iconFacoty.getSimpleCellIcon(	p_eventLog.getCellType(), IconFactory.NO_FOG, IconFactory.SIZE_SIMPLE_SIZE, p_eventLog.getCellIdx());
		_cellTypeIcon.setIcon(unitIcon);
		_bombCoordLbl.setText(p_eventLog.getCoord().toStringLight());
	}

}
