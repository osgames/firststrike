package org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical.city;

import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;

public class AreaListItem
{
	private final Area _area;
	private final HumanPlayer _player;
	private ArrayList<City> _cityList;

	public AreaListItem(final Area p_area, final HumanPlayer p_player)
	{
		_area = p_area;
		_player = p_player;
		final CityList cityList = _area.getCityList().getCityByPlayer(_player);
		cityList.sortByName();
		_cityList = new ArrayList<City>(cityList);
	}

	public final Area getArea()
	{
		return _area;
	}

	public final ArrayList<City> getCityList()
	{
		return _cityList;
	}

	public final int getSize()
	{
		return _cityList.size();
	}
}
