package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import net.miginfocom.swing.MigLayout;

public class CurrentCellEmptyPnl extends JPanel
{
	private JLabel _iconLbl;

	public CurrentCellEmptyPnl()
	{
		setBackground(Color.WHITE);
		setOpaque(true);
		setLayout(new MigLayout("", "0[300.00]0", "0[]0"));

		_iconLbl = new JLabel("");
		_iconLbl.setBackground(Color.WHITE);
		_iconLbl.setHorizontalAlignment(SwingConstants.CENTER);
		_iconLbl.setIcon(new ImageIcon(CurrentCellEmptyPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/current_cell_blank.PNG")));
		add(_iconLbl, "cell 0 0,alignx center,aligny center");
	}

}
