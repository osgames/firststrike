/*
 * CellCardPanel.java Created on 9 janvier 2009, 11:38
 */

package org.jocelyn_chaumel.firststrike.gui.game.toolbar.horizontal;

import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.gui.commun.icon_mngt.IconFactory;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;

/**
 * @author Joss
 */
public class CellCardPanel extends JPanel
{

	protected static final IconFactory _iconFactory = IconFactory.getInstance();
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	// private final String _cardIdentifier;
	private final FSFrame _frame;

	public final static String FIRST_CARD = "First Card";
	public final static String SECOND_CARD = "Second Card";

	/**
	 * Creates new form CellCardPanel
	 */
	public CellCardPanel()
	{
		initComponents();
		// _cardIdentifier = null;
		_frame = null;
	}

	public CellCardPanel(final FSFrame p_frame, final Cell p_cell2)
	{
		initComponents();
		_frame = p_frame;

		// _cardIdentifier = p_cardName;
		refreshCard(p_cell2);
	}

	public void refreshCard(final Cell p_cell)
	{
		final CellTypeCst cellType = p_cell.getCellType();
		final Area currentArea = p_cell.getAreaList().getPrincipalArea();
		final String areaName = currentArea.getInternalAreaName();
		_cellCoordonates.setText(p_cell.getCellCoord().toStringLight());
		_cellType.setText(_bundle.getString("fs.common.cell_type." + cellType.getLabel()));
		_areaName.setText(areaName);

		final ImageIcon cellIcon = _iconFactory.getSimpleCellIcon(	cellType,
																	IconFactory.NO_FOG,
																	IconFactory.SIZE_DOUBLE_SIZE,
																	p_cell.getIndex());
		_cellIcon.setIcon(cellIcon);

		if (!cellType.equals(CellTypeCst.CITY))
		{
			_lblCityName.setVisible(false);
			_cityName.setVisible(false);
			_lblProductionType.setVisible(false);
			_cityProductionTypeIcon.setVisible(false);
			_OpenCityBtn.setVisible(false);
			_productionTime.setVisible(false);
		} else
		{
			_cityName.setText(p_cell.getDefaultCellName());
			_lblCityName.setVisible(true);
			_cityName.setVisible(true);
			_productionTime.setVisible(true);
		}

		if (!cellType.equals(CellTypeCst.CITY))
		{
			_recursiveIcon.setIcon(null);
			return;
		}

		final City city = p_cell.getCity();
		final AbstractPlayer player = city.getPlayer();
		if (player == null
				|| (player instanceof ComputerPlayer && !ApplicationConfigurationMgr.getInstance().isDebugMode()))
		{
			displayCityProduction(false, null);
			_recursiveIcon.setVisible(false);
			_OpenCityBtn.setVisible(false);
		} else if (player instanceof ComputerPlayer)
		{
			displayCityProduction(true, city);
			_recursiveIcon.setVisible(false);
			_OpenCityBtn.setVisible(false);
		} else if (player instanceof HumanPlayer)
		{
			displayCityProduction(true, city);
			_recursiveIcon.setVisible(true);
			if (city.isContinueProduction())
			{
				_recursiveIcon.setIcon(_iconFactory.getCommunIcon(IconFactory.COMMUN_RECURSIVE_ICON));
			} else
			{
				_recursiveIcon.setIcon(_iconFactory.getCommunIcon(IconFactory.COMMON_INVISIBLE_SPACE));
			}
			_OpenCityBtn.setVisible(true);
			_OpenCityBtn.setAction(new AbstractAction()
			{
				@Override
				public void actionPerformed(ActionEvent p_e)
				{
					_frame.openCity(p_cell.getCellCoord());
				}
			});
			_OpenCityBtn.setText("...");
		}
	}

	private void displayCityProduction(final boolean p_isVisible, final City p_city)
	{
		if (!p_isVisible)
		{
			_lblProductionType.setVisible(false);
			_cityProductionTypeIcon.setVisible(false);
			_productionTime.setVisible(false);

		} else
		{
			_lblProductionType.setVisible(true);
			_cityProductionTypeIcon.setVisible(true);
			_productionTime.setVisible(true);
			final UnitTypeCst productionType = p_city.getProductionType();
			if (UnitTypeCst.NULL_TYPE.equals(productionType))
			{
				_cityProductionTypeIcon.setIcon(null);
				_productionTime.setText("(N/A)");
			} else
			{
				final ImageIcon unitIcon = _iconFactory.getUnitIconByType(productionType, p_city.getPlayer()
						.getPlayerId(), IconFactory.SIZE_SIMPLE_SIZE, true);
				_cityProductionTypeIcon.setIcon(unitIcon);
				_cityProductionTypeIcon.setVisible(true);
				_productionTime.setText("(" + p_city.getProductionTime() + ")");
			}

		}
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents()
	{

		_cellIcon = new javax.swing.JLabel();
		_lblCellCoordonates = new javax.swing.JLabel();
		_areaName = new javax.swing.JLabel();
		_cellCoordonates = new javax.swing.JLabel();
		_lblCellType = new javax.swing.JLabel();
		_cellType = new javax.swing.JLabel();
		_lblProductionType = new javax.swing.JLabel();
		_lblAreaName = new javax.swing.JLabel();
		jSeparator1 = new javax.swing.JSeparator();
		_lblCityName = new javax.swing.JLabel();
		_cityName = new javax.swing.JLabel();
		_cityProductionTypeIcon = new javax.swing.JLabel();
		_OpenCityBtn = new javax.swing.JButton();
		_recursiveIcon = new javax.swing.JLabel();
		_productionTime = new javax.swing.JLabel();

		setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));

		java.util.ResourceBundle bundle = java.util.ResourceBundle
				.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle"); // NOI18N
		_lblCellCoordonates.setText(bundle.getString("fs.cell_card.coordonates")); // NOI18N

		_areaName.setForeground(new java.awt.Color(0, 0, 204));
		_areaName.setText("Amérique");

		_cellCoordonates.setForeground(new java.awt.Color(0, 0, 204));
		_cellCoordonates.setText("10, 4");

		_lblCellType.setText(bundle.getString("fs.cell_card.cell_type")); // NOI18N

		_cellType.setForeground(new java.awt.Color(0, 0, 204));
		_cellType.setText("City");

		_lblProductionType.setText(bundle.getString("fs.cell_card.production")); // NOI18N

		_lblAreaName.setText(bundle.getString("fs.cell_card.area_name")); // NOI18N

		_lblCityName.setText(bundle.getString("fs.cell_card.city_name")); // NOI18N

		_cityName.setForeground(new java.awt.Color(0, 0, 204));
		_cityName.setText("Montréal");

		_cityProductionTypeIcon.setForeground(new java.awt.Color(0, 0, 204));
		_cityProductionTypeIcon.setToolTipText("");

		_OpenCityBtn.setText("...");
		_OpenCityBtn.setFocusPainted(false);
		_OpenCityBtn.setMargin(new java.awt.Insets(1, 2, 1, 2));
		_OpenCityBtn.setPreferredSize(new java.awt.Dimension(15, 15));

		_recursiveIcon.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/org/jocelyn_chaumel/firststrike/resources/recursive.png"))); // NOI18N

		_productionTime.setText("1");

		org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(layout
				.createSequentialGroup()
				.addContainerGap()
				.add(layout
						.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
						.add(layout
								.createSequentialGroup()
								.add(_cellIcon)
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
								.add(layout
										.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
										.add(layout
												.createSequentialGroup()
												.add(_lblCellType)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(	_cellType,
														org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
														130,
														Short.MAX_VALUE))
										.add(layout
												.createSequentialGroup()
												.add(_lblAreaName)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(	_areaName,
														org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
														120,
														Short.MAX_VALUE))
										.add(layout
												.createSequentialGroup()
												.add(_lblCellCoordonates)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(	_cellCoordonates,
														org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
														110,
														Short.MAX_VALUE))))
						.add(layout.createSequentialGroup().add(_lblProductionType)
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(_cityProductionTypeIcon)
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(_recursiveIcon)
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(_productionTime))
						.add(layout
								.createSequentialGroup()
								.add(_lblCityName)
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
								.add(	_OpenCityBtn,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
								.add(_cityName, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE))
						.add(	jSeparator1,
								org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
								191,
								org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(layout
				.createSequentialGroup()
				.addContainerGap()
				.add(layout
						.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
						.add(_cellIcon)
						.add(layout
								.createSequentialGroup()
								.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
										.add(_lblCellCoordonates).add(_cellCoordonates))
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
								.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
										.add(_lblCellType).add(_cellType))
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
								.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
										.add(_lblAreaName).add(_areaName))))
				.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
				.add(	jSeparator1,
						org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
						10,
						org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
				.add(layout
						.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
						.add(_lblCityName)
						.add(	_OpenCityBtn,
								org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(_cityName))
				.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
				.add(layout
						.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
						.add(	_lblProductionType,
								org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
								28,
								org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
						.add(layout
								.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
								.add(_recursiveIcon)
								.add(	_cityProductionTypeIcon,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
										20,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(_productionTime)))
				.addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
	}// </editor-fold>//GEN-END:initComponents

	// Variables declaration - do not modify//GEN-BEGIN:variables
	protected javax.swing.JButton _OpenCityBtn;
	protected javax.swing.JLabel _areaName;
	protected javax.swing.JLabel _cellCoordonates;
	protected javax.swing.JLabel _cellIcon;
	protected javax.swing.JLabel _cellType;
	protected javax.swing.JLabel _cityName;
	protected javax.swing.JLabel _cityProductionTypeIcon;
	protected javax.swing.JLabel _lblAreaName;
	protected javax.swing.JLabel _lblCellCoordonates;
	protected javax.swing.JLabel _lblCellType;
	protected javax.swing.JLabel _lblCityName;
	protected javax.swing.JLabel _lblProductionType;
	protected javax.swing.JLabel _productionTime;
	protected javax.swing.JLabel _recursiveIcon;
	protected javax.swing.JSeparator jSeparator1;
	// End of variables declaration//GEN-END:variables

}
