package org.jocelyn_chaumel.firststrike.gui.game.toolbar.vertical;

import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSFrameEvent;
import org.jocelyn_chaumel.firststrike.gui.game.event.IFSFrameEventListener;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class VerticalToolBar extends JToolBar implements IFSFrameEventListener
{
	private final VerticalPnl _verticalPnl = new VerticalPnl(this);
	private final FSFrame _frame;
	private boolean _isSelectedTabUpdatable = true;

	public VerticalToolBar(final FSFrame p_frame)
	{
		super("Vertical Context Bar", SwingConstants.NORTH);
		add(_verticalPnl);
		_frame = p_frame;
		_frame.addActionListener(this);
	}

	@Override
	public void closeGameEvent(final FSFrameEvent p_event)
	{
		_verticalPnl.removeCurrentGame();
	}

	@Override
	public void loadGameEvent(final FSFrameEvent p_event)
	{
		_verticalPnl.setCurrentGame(((FSFrame) p_event.getSource()).getGame());
	}
	
	public void setMouseEnabled(final boolean p_enabled)
	{
		_verticalPnl.setMouseEnabled(p_enabled);
	}

	@Override
	public void updateCurrentCellEvent(final FSFrameEvent p_event)
	{
		final Cell currentCell = p_event.getCurrentCell();
		if (!_isSelectedTabUpdatable && currentCell.isCity())
		{
			final City currentCity = currentCell.getCity();
			if (currentCity.getPlayer() instanceof HumanPlayer)
			{
				_verticalPnl.selectCity(currentCity);
				_isSelectedTabUpdatable = true;
			}
		}

		final AbstractUnit currentUnit = p_event.getCurrentUnit();
		if (currentUnit != null && currentUnit.getPlayer() instanceof HumanPlayer)
		{
			_verticalPnl.selectUnit(currentUnit, !_isSelectedTabUpdatable);
		}
		
		_isSelectedTabUpdatable = false;
	}

	@Override
	public void conquerCityEvent(final FSFrameEvent p_event)
	{
		_verticalPnl.conquerCity((City) p_event.getOtherData());
	}

	@Override
	public void lostCityEvent(final FSFrameEvent p_event)
	{
		_verticalPnl.lostCity(p_event.getCurrentCell().getCity());
	}

	public final void setCurrentCell(final Coord p_cellCoord, final boolean p_isSelectedTabUpdatable)
	{
		_isSelectedTabUpdatable = p_isSelectedTabUpdatable;
		_frame.setCurrentCell(this, p_cellCoord);
		_frame.setCenterOfBattleBoard(p_cellCoord);
	}

	public final void setCurrentUnit(final AbstractUnit p_unit)
	{
		_frame.setCurrentUnit(this, p_unit);
		_frame.setCenterOfBattleBoard(p_unit.getPosition());
	}

	public final void openCity(final Coord p_cellCoord)
	{
		_frame.openCity(p_cellCoord);
	}

	@Override
	public void lostUnitEvent(final FSFrameEvent p_event)
	{
		_verticalPnl.lostUnit((AbstractUnit) p_event.getOtherData());
	}

	@Override
	public void unitCreatedEvent(final FSFrameEvent p_event)
	{
		_verticalPnl.unitCreated();
	}

	@Override
	public void unitMovedEvent(final FSFrameEvent p_event)
	{
		final Object otherData = p_event.getOtherData();
		if (otherData instanceof AbstractUnit)
		{
			_verticalPnl.updateUnit((AbstractUnit) otherData);
		}
	}

	@Override
	public void updateCurrentUnitEvent(final FSFrameEvent p_event)
	{
		_verticalPnl.selectUnit(p_event.getCurrentUnit(), true);
	}
	
	public void refreshAllList()
	{
		_verticalPnl.refreshAllList();
		if (!_frame.getGame().getCurrentPlayer().getEventLog().isEmpty())
		{
			_verticalPnl.selectEventTab();
		}
	}
}
