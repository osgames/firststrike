package org.jocelyn_chaumel.firststrike.gui.game.toolbar.horizontal;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collections;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.UnitListComparator;
import org.jocelyn_chaumel.firststrike.gui.game.event.FSFrameEvent;
import org.jocelyn_chaumel.firststrike.gui.game.event.IFSFrameEventListener;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class UnitListMgr extends JPanel implements MouseListener, IFSFrameEventListener
{
	private final FSFrame _frame;
	private final DefaultListModel _listModel;
	private final UnitListComparator _unitComparator = new UnitListComparator();
	private FirstStrikeGame _currentGame = null;
	private HumanPlayer _humanPlayer = null;

	/** Creates new form UnitListContext */
	public UnitListMgr()
	{
		_listModel = new DefaultListModel();
		initComponents();
		_frame = null;
	}

	public UnitListMgr(final FSFrame p_frame)
	{
		_listModel = new DefaultListModel();
		initComponents();
		_frame = p_frame;
		_unitList.addMouseListener(this);
		setEnabled(false);
	}

	/**
	 * Refresh the item in the unit list & all properties displayed in the unit
	 * card panel.
	 */
	public void refreshCurrentUnit()
	{
		int idx = _unitList.getSelectedIndex();
		final AbstractUnit unit = (AbstractUnit) _unitList.getSelectedValue();
		DefaultListModel model = (DefaultListModel) _unitList.getModel();
		model.remove(idx);
		model.add(idx, unit);
		_unitList.setSelectedIndex(idx);
	}

	@Override
	public void mouseClicked(MouseEvent p_e)
	{
		final JList source = (JList) p_e.getSource();
		AbstractUnit unit = (AbstractUnit) source.getSelectedValue();
		if (unit != null)
		{
			_frame.setCurrentUnit(this, unit);
		}
	}

	@Override
	public void mouseEntered(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mouseExited(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mousePressed(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mouseReleased(MouseEvent p_e)
	{
		// NOP
	}

	private void initComponents()
	{
		jScrollPane1 = new javax.swing.JScrollPane();
		_unitList = new javax.swing.JList();
		_unitList.setCellRenderer(new UnitJListRenderer());
		jLabel1 = new javax.swing.JLabel();

		setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
		_unitList.setModel(_listModel);
		_unitList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jScrollPane1.setViewportView(_unitList);

		java.util.ResourceBundle bundle = java.util.ResourceBundle
				.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle"); // NOI18N
		jLabel1.setText(bundle.getString("fs.unit_list_card.unit_list"));

		org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(layout
				.createSequentialGroup()
				.addContainerGap()
				.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
						.add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
						.add(jLabel1)).addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(layout
				.createSequentialGroup().addContainerGap().add(jLabel1)
				.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
				.add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
				.addContainerGap()));
	}

	// Variables declaration - do not modify
	private javax.swing.JLabel jLabel1;
	private javax.swing.JList _unitList;
	private javax.swing.JScrollPane jScrollPane1;

	// End of variables declaration

	@Override
	public void loadGameEvent(final FSFrameEvent p_event)
	{
		setEnabled(true);
		_currentGame = p_event.getGame();
		_humanPlayer = (HumanPlayer) _currentGame.getPlayerMgr().getCurrentPlayer();
	}

	@Override
	public void closeGameEvent(final FSFrameEvent p_event)
	{
		_currentGame = null;
		_humanPlayer = null;
		_listModel.clear();
		setEnabled(false);
	}

	@Override
	public void conquerCityEvent(final FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostCityEvent(final FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void lostUnitEvent(final FSFrameEvent p_event)
	{
		final AbstractUnit unit = p_event.getCurrentUnit();
		final int size = _listModel.getSize();
		AbstractUnit currentUnit = null;
		for (int i = 0; i < size; i++)
		{
			currentUnit = (AbstractUnit) _listModel.get(i);
			if (currentUnit.getUnitId().equals(unit.getUnitId()))
			{
				_listModel.remove(i);
				break;
			}
		}
	}

	@Override
	public void updateCurrentCellEvent(final FSFrameEvent p_event)
	{
		refreshUnitList(p_event);
	}

	@Override
	public void updateCurrentUnitEvent(final FSFrameEvent p_event)
	{
		refreshUnitList(p_event);
	}

	private final void refreshUnitList(final FSFrameEvent p_event)
	{
		_listModel.clear();
		final Coord position = p_event.getCurrentCell().getCellCoord();
		final AbstractUnitList completeUnitList = _humanPlayer.getUnitList();
		AbstractUnitList unitList = completeUnitList.getUnitAt(position);
		if (unitList.isEmpty())
		{
			AbstractUnitList enemySet = _humanPlayer.getEnemySet();
			unitList = enemySet.getUnitAt(position);
		}

		if (unitList.isEmpty())
		{
			return;
		}

		Collections.sort(unitList, _unitComparator);
		for (AbstractUnit currentUnit : unitList)
		{
			_listModel.addElement(currentUnit);
		}
		_unitList.setSelectedValue(p_event.getCurrentUnit(), true);
	}

	@Override
	public void unitCreatedEvent(FSFrameEvent p_event)
	{
		// NOP
	}

	@Override
	public void unitMovedEvent(FSFrameEvent p_event)
	{
		// NOP
	}

}
