package org.jocelyn_chaumel.firststrike.gui.game.toolbar.dynamic;

import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EtchedBorder;

import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnit;
import org.jocelyn_chaumel.firststrike.core.unit.ILoadableUnit;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.firststrike.gui.game.FSFrame;
import org.jocelyn_chaumel.firststrike.gui.game.ILoadableUnitListComparator;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

import net.miginfocom.swing.MigLayout;

public class CurrentContainedUnitListPnl extends JPanel implements MouseListener
{
	private final FSFrame _frame;
	private final DefaultListModel<AbstractUnit> _listModel;
	private final ILoadableUnitListComparator _iLoadableUnitComparator = new ILoadableUnitListComparator();
	private final static ResourceBundle _bundle = ResourceBundle
			.getBundle("org/jocelyn_chaumel/firststrike/resources/i18n/fs_bundle");

	/** Creates new form UnitListContext */
	public CurrentContainedUnitListPnl()
	{
		_listModel = new DefaultListModel<AbstractUnit>();
		initComponents();
		_frame = null;
	}

	public CurrentContainedUnitListPnl(final FSFrame p_frame)
	{
		_listModel = new DefaultListModel<AbstractUnit>();
		initComponents();
		_unitListJList.setCellRenderer(new UnitItemListRenderer());
		_unitListJList.setModel(_listModel);
		_frame = p_frame;
		_unitListJList.addMouseListener(this);
		setEnabled(false);
	}

	@Override
	public void mouseClicked(MouseEvent p_e)
	{
		final JList source = (JList) p_e.getSource();
		AbstractUnit unit = (AbstractUnit) source.getSelectedValue();
		if (unit == null)
		{
			_unloadTankBtn.setEnabled(false);
			return;
		}
		
		if (getAvailableCoordForUnload(unit).size() > 0)
		{
			_unloadTankBtn.setEnabled(true);
		}
	}
	
	private CoordSet getAvailableCoordForUnload(final AbstractUnit p_tank)
	{
		final CoordSet coordSet = new CoordSet();
		final FSMapMgr mapMgr = p_tank.getPlayer().getMapMgr();
		final FSMap map = mapMgr.getMap();
		final CoordSet potentialUnloadCoordSet = CoordSet.createInitiazedCoordSet(p_tank.getPosition(), 1, map.getWidth(), map.getHeight());
		for (Coord currentCoord : potentialUnloadCoordSet)
		{
			if (mapMgr.isAccessibleCell(p_tank.getMove(), currentCoord, p_tank.getMoveCost()))
			{
				coordSet.add(currentCoord);
			}
		}
		
		return coordSet;
	}

	@Override
	public void mouseEntered(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mouseExited(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mousePressed(MouseEvent p_e)
	{
		// NOP
	}

	@Override
	public void mouseReleased(MouseEvent p_e)
	{
		// NOP
	}

	private void initComponents()
	{
		currentContainedUnitListPnl = new JScrollPane();
		_unitListJList = new JList<AbstractUnit>();
		jLabel1 = new JLabel(_bundle.getString("fs.contained_unit_list_card.unit_list"));

		setBorder(BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.LOWERED));
		setLayout(new MigLayout("", "[426px]", "[14px][84.00px][]"));
		_unitListJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		currentContainedUnitListPnl.setViewportView(_unitListJList);
		add(currentContainedUnitListPnl, "cell 0 1,grow");
		add(jLabel1, "cell 0 0,alignx left,aligny top");

		JPanel currentContainedUnitActionListPnl = new JPanel();
		currentContainedUnitActionListPnl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		add(currentContainedUnitActionListPnl, "cell 0 2,grow");
		currentContainedUnitActionListPnl
				.setLayout(new MigLayout("", "[][grow]", "0[]0"));

		JLabel actionListIcon = new JLabel("");
		actionListIcon.setIcon(new ImageIcon(CurrentContainedUnitListPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/list.png")));
		currentContainedUnitActionListPnl.add(actionListIcon, "cell 0 0");

		actionListPnl = new JPanel();
		FlowLayout flowLayout = (FlowLayout) actionListPnl.getLayout();
		flowLayout.setVgap(1);
		currentContainedUnitActionListPnl.add(actionListPnl, "cell 1 0,grow");

		_unloadTankBtn = new JButton("");
		_unloadTankBtn.setAction(new AbstractAction()
		{
			
			@Override
			public void actionPerformed(ActionEvent p_e)
			{
				final AbstractUnit unit = (AbstractUnit) _unitListJList.getSelectedValue();
				final CoordSet unloadCoordSet = getAvailableCoordForUnload(unit);
				_frame.getBattleBoard().activateUnloadUnitAction((ILoadableUnit) unit, unloadCoordSet);
			}
		});
		_unloadTankBtn.setMargin(new Insets(1, 1, 1, 1));
		_unloadTankBtn.setIcon(new ImageIcon(CurrentContainedUnitListPnl.class
				.getResource("/org/jocelyn_chaumel/firststrike/resources/tank_unload.png")));
		actionListPnl.add(_unloadTankBtn);
	}

	// Variables declaration - do not modify
	private javax.swing.JLabel jLabel1;
	private javax.swing.JList<AbstractUnit> _unitListJList;
	private javax.swing.JScrollPane currentContainedUnitListPnl;
	private JPanel actionListPnl;
	private JButton _unloadTankBtn;

	// End of variables declaration

	public final void preparePanel(final AbstractUnit p_currentUnit)
	{
		_listModel.clear();
		_unloadTankBtn.setEnabled(false);
		
		if (!UnitTypeCst.TRANSPORT_SHIP.equals(p_currentUnit.getUnitType()))
		{
			return;
		}

		final TransportShip transport = (TransportShip) p_currentUnit;
		final ArrayList<ILoadableUnit> unitList = transport.getContainedUnitList();
		if (unitList.isEmpty())
		{
			return;
		}

		Collections.sort(unitList, _iLoadableUnitComparator);
		for (ILoadableUnit currentUnit : unitList)
		{
			_listModel.addElement((AbstractUnit)currentUnit);
		}
		
		_unitListJList.setSelectedIndex(-1);
	}
}
