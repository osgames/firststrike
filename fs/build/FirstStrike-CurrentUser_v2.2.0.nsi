!include LogicLib.nsh

; FirstStrike.nsi
;
; This script is based on example1.nsi, but it remember the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install FirstStrike.nsi into a directory that the user selects,

;--------------------------------

; The name of the installer
Name "FirstStrike - For current user"

; The file to write
OutFile "FirstStrike-CurrentUser.exe"

; The default installation directory
InstallDir $LOCALAPPDATA\FirstStrike

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKCU "Software\FirstStrike" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

Page components "java_test" "" ""
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

Function java_test
# Check for C:
  IfFileExists "C:\Program Files (x86)\Java\jre8\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  C:\Program Files (x86)\Java\jre8\bin\java.exe"
    return
  
  IfFileExists "C:\Program Files (x86)\Java\jre7\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  C:\Program Files (x86)\Java\jre7\bin\java.exe"
    return
  
  IfFileExists "C:\Program Files (x86)\Java\jre6\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  C:\Program Files (x86)\Java\jre6\bin\java.exe"
    return
  
  IfFileExists "C:\Program Files\Java\jre8\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  C:\Program Files\Java\jre8\bin\java.exe"
    return
  
  IfFileExists "C:\Program Files\Java\jre7\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  C:\Program Files\Java\jre7\bin\java.exe"
    return
  
  IfFileExists "C:\Program Files\Java\jre6\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  C:\Program Files\Java\jre6\bin\java.exe"
    return

# Check for D:
  IfFileExists "D:\Program Files (x86)\Java\jre8\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  D:\Program Files (x86)\Java\jre8\bin\java.exe"
    return
  
  IfFileExists "D:\Program Files (x86)\Java\jre7\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  D:\Program Files (x86)\Java\jre7\bin\java.exe"
    return
  
  IfFileExists "D:\Program Files (x86)\Java\jre6\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  D:\Program Files (x86)\Java\jre6\bin\java.exe"
    return
  
  IfFileExists "D:\Program Files\Java\jre8\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  D:\Program Files\Java\jre8\bin\java.exe"
    return
  
  IfFileExists "D:\Program Files\Java\jre7\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  D:\Program Files\Java\jre7\bin\java.exe"
    return
  
  IfFileExists "D:\Program Files\Java\jre6\bin\java.exe" 0 +3
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in the following folder:$\n  D:\Program Files\Java\jre6\bin\java.exe"
    return
  
  
# Check for JAVA_HOME
  ReadEnvStr $3 JAVA_HOME
  IfErrors display_message
  
  IfFileExists "$3\bin\java.exe" 0 display_message
    MessageBox MB_ICONINFORMATION|MB_OK "Java Runtime Environment (java.exe) detected in using the JAVA_HOME system's variable:$\n   $3\bin\java.exe"
    return
  
display_message:
  MessageBox MB_ICONEXCLAMATION|MB_OK "No Java Runtime Environment (java.exe) has been found in the following folders :$\n \
   C:\Program Files (x86)\Java\jre6\bin\$\n \
   C:\Program Files (x86)\Java\jre7\bin\$\n \
   C:\Program Files (x86)\Java\jre8\bin\$\n \
   C:\Program Files\Java\jre6\bin\$\n \
   C:\Program Files\Java\jre7\bin\$\n \
   C:\Program Files\Java\jre8\bin\$\n \
   D:\...$\n \
  $\nOnce First Strike will be installed, could you download (http://www.java.com/) and install Java or update the Java installation folder manually in the following file please :$\n \
   $INSTDIR\set_java_location_manually.bat"
  
FunctionEnd

;--------------------------------

; The stuff to install
Section "FirstStrike (required)"

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  File "readme.txt"
  File "lisez-moi.txt"
  File "fs.bat"
  File "fs.ico"
  File "java_test.bat"
  File "fs_mapbuilder.bat"
  File "fs_mapbuilder.ico"
  File "set_java_location_manually.bat"

  SetOutPath $INSTDIR\lib
  File "lib\fs.jar"
  File "lib\infra.jar"
  File "lib\jdom.jar"
  File "lib\log4j-1.2.12.jar"
  File "lib\swing-layout-1.0.3.jar"

  SetOutPath $INSTDIR\help
  File "help\help_fr.pdf"
  File "help\help.pdf"

  SetOutPath $LOCALAPPDATA\FirstStrike\maps
  File "maps\map 20x20 - tutorial.zip"
  File "maps\map 40x40.zip"
  File "maps\map 50x50.zip"
  
  CreateDirectory $LOCALAPPDATA\FirstStrike\games
  CreateDirectory $LOCALAPPDATA\FirstStrike\logs
  
  ; Write the installation path into the registry
  WriteRegStr HKCU SOFTWARE\FirstStrike "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\FirstStrike" "DisplayName" "FirstStrike"
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\FirstStrike" "UninstallString" "$INSTDIR\uninstall_FirstStrike.exe"
  WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\FirstStrike" "NoModify" 1
  WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\FirstStrike" "NoRepair" 1
  WriteUninstaller "uninstall_FirstStrike.exe"
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  SetOutPath $INSTDIR
  CreateDirectory "$SMPROGRAMS\FirstStrike"
  CreateShortCut "$SMPROGRAMS\FirstStrike\Uninstall FirstStrike.lnk" "$INSTDIR\uninstall_FirstStrike.exe"
  CreateShortCut "$SMPROGRAMS\FirstStrike\FirstStrike.lnk" "$INSTDIR\fs.bat" "" "$INSTDIR\fs.ico" 
  CreateShortCut "$SMPROGRAMS\FirstStrike\FirstStrike - Map Builder.lnk" "$INSTDIR\fs_mapbuilder.bat" "" "$INSTDIR\fs_mapbuilder.ico"
  
SectionEnd

Section "Desktop Shortcut"
  SetOutPath $INSTDIR
  CreateShortCut "$DESKTOP\FirstStrike.lnk" "$INSTDIR\fs.bat" "" "$INSTDIR\fs.ico"
  CreateShortCut "$DESKTOP\FirstStrike - Map Builder.lnk" "$INSTDIR\fs_mapbuilder.bat" "" "$INSTDIR\fs_mapbuilder.ico"
SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\FirstStrike"
  DeleteRegKey HKCU SOFTWARE\FirstStrike

  ; Remove shortcuts, if any
  RMDir /r "$SMPROGRAMS\FirstStrike"
  RMDir /r "$LOCALAPPDATA\FirstStrike"
  Delete "$DESKTOP\FirstStrike.lnk"
  Delete "$DESKTOP\FirstStrike - Map Builder.lnk"

  ; Remove directories used
  RMDir /r "$INSTDIR"

SectionEnd
