package org.jocelyn_chaumel.firststrike;
/*
 * Created on Nov 15, 2004
 */


import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

/**
 * @author Jocelyn Chaumel
 */
public class FSMapTest extends MapTestCase
{
	public FSMapTest(final String aName)
	{
		super(aName);
	}

	public final void test_isSameArea_nullParam1()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			map.isSameArea(null, new Coord(0, 0), AreaTypeCst.SEA_AREA);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_isSameArea_nullParam2()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			map.isSameArea(new Coord(0, 0), null, AreaTypeCst.SEA_AREA);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_isSameArea_invalidCoord1()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			map.isSameArea(new Coord(-1, 0), new Coord(0, 0), AreaTypeCst.SEA_AREA);
			fail("Invalid coord has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_isSameArea_invalidCoord2()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			map.isSameArea(new Coord(0, 0), new Coord(0, -1), AreaTypeCst.SEA_AREA);
			fail("Invalid coord has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_isSameArea_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			assertTrue("Invalid result.", map.isSameArea(new Coord(0, 0), new Coord(0, 1), AreaTypeCst.SEA_AREA));
			assertTrue("Invalid result.", map.isSameArea(new Coord(0, 0), new Coord(1, 1), AreaTypeCst.SEA_AREA));
			assertTrue("Invalid result.", !map.isSameArea(new Coord(0, 0), new Coord(1, 1), AreaTypeCst.GROUND_AREA));
			assertTrue("Invalid result.", map.isSameArea(new Coord(2, 2), new Coord(1, 1), AreaTypeCst.GROUND_AREA));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetCellAt_nullParam()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			map.getCellAt(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetCellAt_invalidX1()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			map.getCellAt(-1, 0);
			fail("Invalid X has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetCellAt_invalidX2()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			try
			{
				map.getCellAt(STANDARD_MAP_1[0].length, 0);
				fail("Invalid X has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetCellAt_invalidY1()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			try
			{
				map.getCellAt(0, -1);
				fail("Invalid Y has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetCellAt_invalidY2()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			try
			{
				map.getCellAt(0, STANDARD_MAP_1.length);
				fail("Invalid Y has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetCellAt_Coord_invalidX1()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			try
			{
				map.getCellAt(new Coord(-1, 0));
				fail("Invalid X has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetCellAt_Coord_invalidX2()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			try
			{
				map.getCellAt(new Coord(STANDARD_MAP_1[0].length, 0));
				fail("Invalid X has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetCellAt_Coord_invalidY1()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			map.getCellAt(new Coord(0, -1));
			fail("Invalid Y has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetCellAt_Coord_invalidY2()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			try
			{
				map.getCellAt(new Coord(0, STANDARD_MAP_1.length));
				fail("Invalid Y has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getWatherCoordNearToGroundCoord_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final CoordSet coordSet = map.getWatherCoordNearToGroundCoord(new Coord(1, 1));
			assertEquals(5, coordSet.size());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getWatherCoordNearToGroundCoord_nullParam()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			try
			{
				map.getWatherCoordNearToGroundCoord(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getWatherCoordNearToGroundCoord_invalidSeaCoord()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_7);
			try
			{
				map.getWatherCoordNearToGroundCoord(new Coord(0, 0));
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getCellCoordNearToWather_all()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			CoordSet coordSet = map.getCellCoordNearToWather();
			assertEquals(13, coordSet.size());

			map = buildFSMap(STANDARD_MAP_2);
			coordSet = map.getCellCoordNearToWather();
			assertEquals(12 * 4, coordSet.size());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getWatherAndBeachCellCoord_all()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			CoordSet coordSet = map.getWatherAndBeachCellCoord();
			assertEquals(32, coordSet.size());

			map = buildFSMap(STANDARD_MAP_2);
			coordSet = map.getWatherAndBeachCellCoord();
			assertEquals(128, coordSet.size());

			map = buildFSMap(STANDARD_MAP_3);
			coordSet = map.getWatherAndBeachCellCoord();
			assertEquals(0, coordSet.size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}