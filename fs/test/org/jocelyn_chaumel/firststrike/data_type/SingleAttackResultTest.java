/*
 * Created on Apr 23, 2005
 */
package org.jocelyn_chaumel.firststrike.data_type;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public class SingleAttackResultTest extends MapTestCase
{

	/**
	 * @param aName
	 */
	public SingleAttackResultTest(final String aName)
	{
		super(aName);
	}

	public final void test_Cntr_4params_nullParam_1()
	{
		final Tank tank2 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
		try
		{
			new SingleAttackResult(null, 100, 100, tank2);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_4params_nullParam_2()
	{
		final Tank tank1 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
		try
		{
			new SingleAttackResult(tank1, 100, 100, null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_4params_invalid_Attack()
	{
		final Tank tank1 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
		final Tank tank2 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
		try
		{
			new SingleAttackResult(tank1, -100, 60, tank2);
			fail("Invalid Attack has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_4params_invalid_Defense()
	{
		final Tank tank1 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
		final Tank tank2 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
		try
		{
			new SingleAttackResult(tank1, 100, -60, tank2);
			fail("Invalid Defense has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_3params_nullParam()
	{
		try
		{
			new SingleAttackResult(null, 100, 100);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_3params_invalid_Attack()
	{
		final Tank tank1 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
		try
		{
			new SingleAttackResult(tank1, -100, 60);
			fail("Invalid Attack has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_3params_invalid_Defense()
	{
		final Tank tank1 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
		try
		{
			new SingleAttackResult(tank1, 100, -60);
			fail("Invalid Defense has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_3paramsBis_invalid_Attack()
	{
		final Tank tank2 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
		try
		{
			new SingleAttackResult(-100, 60, tank2);
			fail("Invalid Attack has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_3paramsBis_invalid_Defense()
	{
		final Tank tank2 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
		try
		{
			new SingleAttackResult(100, -60, tank2);
			fail("Invalid Defense has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_3paramsBis_nullParam()
	{
		try
		{
			new SingleAttackResult(100, 100, null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_2params_invalid_Attack()
	{
		try
		{
			new SingleAttackResult(-100, 60);
			fail("Invalid Attack has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_2params_invalid_Defense()
	{
		try
		{
			new SingleAttackResult(100, -60);
			fail("Invalid Defense has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_valid()
	{
		try
		{
			final Tank tank1 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			final Tank tank2 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			new SingleAttackResult(100, 60);
			new SingleAttackResult(tank1, 100, 60);
			new SingleAttackResult(100, 60, tank2);
			new SingleAttackResult(tank1, 100, 60, tank2);
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
