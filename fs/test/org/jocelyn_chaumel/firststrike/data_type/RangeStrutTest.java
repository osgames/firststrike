/*
 * Created on Apr 23, 2005
 */
package org.jocelyn_chaumel.firststrike.data_type;

import org.jocelyn_chaumel.firststrike.FSTestCase;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.data_type.RangeStrut;

/**
 * @author Jocelyn Chaumel
 */
public class RangeStrutTest extends FSTestCase
{

	/**
	 * @param aName
	 */
	public RangeStrutTest(final String aName)
	{
		super(aName);
	}

	public final void test_Cntr_invalidRange()
	{
		try
		{
			new RangeStrut(new Coord(1, 1), -1, 2, 2);
			fail("Invalid range has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_invalidWidht()
	{
		try
		{
			new RangeStrut(new Coord(0, 1), 2, 0, 2);
			fail("Invalid widht has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_invalidHeight()
	{
		try
		{
			new RangeStrut(new Coord(1, 0), 2, 2, 0);
			fail("Invalid height has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_invalidXPosition()
	{
		try
		{
			new RangeStrut(new Coord(2, 1), 2, 2, 2);
			fail("Invalid x position has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_invalidYPosition()
	{
		try
		{
			new RangeStrut(new Coord(1, 2), 2, 2, 2);
			fail("Invalid y position has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_valid()
	{
		try
		{
			RangeStrut rs = new RangeStrut(new Coord(1, 1), 1, 2, 2);
			assertEquals(0, rs._minX);
			assertEquals(0, rs._minY);
			assertEquals(1, rs._maxX);
			assertEquals(1, rs._maxY);

			rs = new RangeStrut(new Coord(0, 0), 1, 2, 2);
			assertEquals(0, rs._minX);
			assertEquals(0, rs._minY);
			assertEquals(1, rs._maxX);
			assertEquals(1, rs._maxY);

			// Coin haut gauche
			rs = new RangeStrut(new Coord(0, 0), 2, 4, 4);
			assertEquals(0, rs._minX);
			assertEquals(0, rs._minY);
			assertEquals(2, rs._maxX);
			assertEquals(2, rs._maxY);

			// Coin haut droit
			rs = new RangeStrut(new Coord(3, 0), 2, 4, 4);
			assertEquals(1, rs._minX);
			assertEquals(0, rs._minY);
			assertEquals(3, rs._maxX);
			assertEquals(2, rs._maxY);

			// Coin bas gauche
			rs = new RangeStrut(new Coord(0, 3), 2, 4, 4);
			assertEquals(0, rs._minX);
			assertEquals(1, rs._minY);
			assertEquals(2, rs._maxX);
			assertEquals(3, rs._maxY);

			// Coin bas droit
			rs = new RangeStrut(new Coord(3, 3), 2, 4, 4);
			assertEquals(1, rs._minX);
			assertEquals(1, rs._minY);
			assertEquals(3, rs._maxX);
			assertEquals(3, rs._maxY);

			// milieu
			rs = new RangeStrut(new Coord(3, 3), 2, 8, 8);
			assertEquals(1, rs._minX);
			assertEquals(1, rs._minY);
			assertEquals(5, rs._maxX);
			assertEquals(5, rs._maxY);

			// Range == 0
			rs = new RangeStrut(new Coord(3, 3), 0, 8, 8);
			assertEquals(3, rs._minX);
			assertEquals(3, rs._minY);
			assertEquals(3, rs._maxX);
			assertEquals(3, rs._maxY);

			final CoordSet coordSet = new CoordSet();
			coordSet.add(new Coord(3, 3));
			rs = new RangeStrut(coordSet, 0, 8, 8);
			assertEquals(3, rs._minX);
			assertEquals(3, rs._minY);
			assertEquals(3, rs._maxX);
			assertEquals(3, rs._maxY);

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_Cntr_coordSet_all()
	{
		try
		{
			final CoordSet coordSet = new CoordSet();

			try
			{
				new RangeStrut((CoordSet) null, 1, 1, 1);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new RangeStrut(coordSet, 1, 4, 4);
				fail("Empty coord set has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			coordSet.add(new Coord(0, 0));
			try
			{
				new RangeStrut(coordSet, -1, 1, 1);
				fail("Invalid range has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new RangeStrut(coordSet, 1, -1, 1);
				fail("Invalid width has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new RangeStrut(coordSet, 1, 1, -1);
				fail("Invalid height has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			RangeStrut rs = new RangeStrut(coordSet, 0, 8, 8);
			assertEquals(0, rs._minX);
			assertEquals(0, rs._minY);
			assertEquals(0, rs._maxX);
			assertEquals(0, rs._maxY);

			rs = new RangeStrut(coordSet, 1, 8, 8);
			assertEquals(0, rs._minX);
			assertEquals(0, rs._minY);
			assertEquals(1, rs._maxX);
			assertEquals(1, rs._maxY);

			coordSet.clear();
			coordSet.add(new Coord(2, 2));
			coordSet.add(new Coord(2, 3));
			rs = new RangeStrut(coordSet, 1, 8, 8);
			assertEquals(1, rs._minX);
			assertEquals(1, rs._minY);
			assertEquals(3, rs._maxX);
			assertEquals(4, rs._maxY);

			coordSet.add(new Coord(3, 2));
			rs = new RangeStrut(coordSet, 1, 8, 8);
			assertEquals(1, rs._minX);
			assertEquals(1, rs._minY);
			assertEquals(4, rs._maxX);
			assertEquals(4, rs._maxY);

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
