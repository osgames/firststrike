/*
 * Created on Nov 27, 2004
 */
package org.jocelyn_chaumel.firststrike;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLevel;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.scenario.MapScenario;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.firststrike.core.player.cst.PlayerTypeCst;
import org.jocelyn_chaumel.firststrike.data_type.PlayerInfo;
import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.Assert;

/**
 * @author Jocelyn Chaumel
 */
public class FirstStrikeMgrTest extends MapTestCase
{
	public FirstStrikeMgrTest(final String aName)
	{
		super(aName);
		FSLoggerManager.setConsoleLogger(FSLevel.ALL);
	}

	public final void test_createFSGame_noHuman()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final PlayerInfo playerInfo1 = new PlayerInfo(	PlayerTypeCst.COMPUTER,
															new Coord(1, 1),
															PlayerLevelCst.NORMAL_LEVEL);
			final PlayerInfo playerInfo2 = new PlayerInfo(	PlayerTypeCst.COMPUTER,
															new Coord(4, 4),
															PlayerLevelCst.NORMAL_LEVEL);
			PlayerInfoList playerInfoList = new PlayerInfoList();
			playerInfoList.add(playerInfo1);
			playerInfoList.add(playerInfo2);
			try
			{
				FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
				fail("Invalid operation has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_loadFSGame_invalidRootDirectory()
	{
		final File fsGame = new File("c:\\temp\\gameSaved.fs");
		try
		{
			try
			{
				FirstStrikeMgr.loadFirstStrikeGame(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				FirstStrikeMgr.loadFirstStrikeGame(fsGame);
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_saveFSGame_nullParam_1()
	{
		final File dir = new File("c:\\temp");
		final File fsGame = new File(dir, "\\fsGame.fs");

		try
		{
			FileMgr.forceCreateDirIfNotPresent(dir);
			try
			{
				FirstStrikeMgr.saveFirstStrikeGame(null, fsGame);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			try
			{
				FileMgr.forceDeleteIfPresent(fsGame);
			} catch (IOException ex)
			{
				fail(ex);
			}

		}
	}

	public final void test_saveFSGame_nullParam_2()
	{
		final File dir = new File("c:\\temp");
		final File fsGame = new File(dir, "\\fsGame.fs");

		try
		{
			FileMgr.forceCreateDirIfNotPresent(dir);
			final FSMap map = buildFSMap(STANDARD_MAP_1);

			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			try
			{
				FirstStrikeMgr.saveFirstStrikeGame(game, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			try
			{
				FileMgr.forceDeleteIfPresent(fsGame);
			} catch (IOException ex)
			{
				fail(ex);
			}
		}
	}

	public final void test_createFSGame_fromScenario_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final File mapDir = new File(ApplicationConfigurationMgr.getInstance().getMapDir(), "map 6x6 - Test");
			final ArrayList<MapScenario> mapScenarioList = FSMapMgr.retrieveScenarioList(mapDir);

			final MapScenario scenario = mapScenarioList.get(0);

			try
			{
				FirstStrikeMgr.createFirstStrikeGame((MapScenario) null, map);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				FirstStrikeMgr.createFirstStrikeGame(scenario, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			assertEquals(3, scenario.getPlayerList().get(0).getMessageList().size());
			final FirstStrikeGame fsGame = FirstStrikeMgr.createFirstStrikeGame(scenario, map);
			fsGame.startGame();
			assertEquals(1, fsGame.getCurrentPlayer().getUnitList().size());
			assertEquals(1, fsGame.getCurrentPlayer().getUnitList().getNbUnitAtPosition(new Coord(1, 1)));
			fsGame.performNextTurn();
			Assert.precondition(PlayerTypeCst.HUMAN == fsGame.getCurrentPlayer().getPlayerType(), "Unexpected result");
			assertEquals(2, fsGame.getCurrentPlayer().getUnitList().size());
			assertEquals(1, fsGame.getCurrentPlayer().getUnitList().getNbUnitAtPosition(new Coord(1, 1)));
			assertEquals(1, fsGame.getCurrentPlayer().getUnitList().getNbUnitAtPosition(new Coord(0, 0)));
			ComputerPlayer computer = (ComputerPlayer) fsGame.getPlayerMgr().getPlayerList().get(1);
			assertEquals(1, computer.getUnitList().size());
			assertEquals(new Coord(4, 4), computer.getUnitList().get(0).getPosition());
			fsGame.performNextTurn();
			assertEquals(2, computer.getUnitList().size());
			assertEquals(new Coord(4, 1), computer.getUnitList().get(0).getPosition());
			assertEquals(new Coord(4, 4), computer.getUnitList().get(1).getPosition());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_createFSGame_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			try
			{
				FirstStrikeMgr.createFirstStrikeGame("game name", (PlayerInfoList) null, map);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				FirstStrikeMgr.createFirstStrikeGame("game name", buildPlayerInfoList(TWO_PLAYERS), null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				FirstStrikeMgr.createFirstStrikeGame("game name", new PlayerInfoList(), map);
				fail("Empty player info list has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();
			final AbstractPlayer currentPlayer = game.getCurrentPlayer();
			assertTrue(currentPlayer instanceof HumanPlayer);
			assertEquals(16, ((HumanPlayer) currentPlayer).getNoFogSet().size());
			assertEquals(1, currentPlayer.getCitySet().size());
			assertEquals(0, currentPlayer.getUnitList().size());

			playerInfoList.remove(0);
			try
			{
				FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
				fail("Too small player info list has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}