/*
 * Created on Dec 18, 2004
 */
package org.jocelyn_chaumel.firststrike.core.area;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

/**
 * @author Jocelyn Chaumel
 */
public class AreaTest extends MapTestCase
{
	public AreaTest(final String aName)
	{
		super(aName);
	}

	public void test_Cntr_nullParam_1()
	{
		try
		{
			new Area(null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_ExtractEdgeCoordSet()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final Area area = map.getAreaAt(new Coord(2, 2), AreaTypeCst.GROUND_AREA);
			
			CoordSet coordSet = area.getCellSet().getCoordSet();
			assertEquals(17, coordSet.size());
			assertTrue(coordSet.contains(new Coord(1, 1)));
			assertTrue(coordSet.contains(new Coord(2, 1)));
			assertTrue(coordSet.contains(new Coord(3, 1)));
			assertTrue(coordSet.contains(new Coord(4, 1)));
			assertTrue(coordSet.contains(new Coord(1, 1)));
			assertTrue(coordSet.contains(new Coord(2, 1)));
			assertTrue(coordSet.contains(new Coord(3, 1)));
			assertTrue(coordSet.contains(new Coord(4, 1)));
			assertTrue(coordSet.contains(new Coord(1, 2)));
			assertTrue(coordSet.contains(new Coord(2, 2)));
			assertTrue(coordSet.contains(new Coord(3, 2)));
			assertTrue(coordSet.contains(new Coord(4, 2)));
			assertTrue(coordSet.contains(new Coord(1, 3)));
			assertTrue(coordSet.contains(new Coord(2, 3)));
			assertTrue(coordSet.contains(new Coord(3, 3)));
			assertTrue(coordSet.contains(new Coord(4, 3)));
			assertTrue(coordSet.contains(new Coord(1, 4)));
			assertTrue(coordSet.contains(new Coord(2, 4)));
			assertTrue(coordSet.contains(new Coord(3, 4)));
			assertTrue(coordSet.contains(new Coord(4, 4)));
			assertTrue(coordSet.contains(new Coord(0, 5)));
			
			coordSet = area.getExtractEdgeCoordSet();
			assertEquals(19, coordSet.size());
			assertTrue(coordSet.contains(new Coord(0, 0)));
			assertTrue(coordSet.contains(new Coord(1, 0)));
			assertTrue(coordSet.contains(new Coord(2, 0)));
			assertTrue(coordSet.contains(new Coord(3, 0)));
			assertTrue(coordSet.contains(new Coord(4, 0)));
			assertTrue(coordSet.contains(new Coord(5, 0)));
			assertTrue(coordSet.contains(new Coord(0, 1)));
			assertTrue(coordSet.contains(new Coord(5, 1)));
			assertTrue(coordSet.contains(new Coord(0, 2)));
			assertTrue(coordSet.contains(new Coord(5, 2)));
			assertTrue(coordSet.contains(new Coord(0, 3)));
			assertTrue(coordSet.contains(new Coord(5, 3)));
			assertTrue(coordSet.contains(new Coord(0, 4)));
			assertTrue(coordSet.contains(new Coord(5, 4)));
			assertTrue(coordSet.contains(new Coord(5, 1)));
			assertTrue(coordSet.contains(new Coord(5, 2)));
			assertTrue(coordSet.contains(new Coord(5, 3)));
			assertTrue(coordSet.contains(new Coord(5, 4)));
			assertTrue(coordSet.contains(new Coord(5, 5)));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
	
}