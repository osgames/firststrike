package org.jocelyn_chaumel.firststrike.core.area;

import org.jocelyn_chaumel.firststrike.MapTestCase;

public class AreaStatusComparatorTest extends MapTestCase
{

	public AreaStatusComparatorTest(final String p_name)
	{
		super(p_name);
	}

	public void test_convertToLevel_valid()
	{
		try
		{
			assertEquals(1, AreaStatusComparator.convertToLevel(AreaStatusCst.NO_CITY_AREA));
			assertEquals(1, AreaStatusComparator.convertToLevel(AreaStatusCst.TO_CONQUER_AREA));
			assertEquals(2, AreaStatusComparator.convertToLevel(AreaStatusCst.UNDER_CONTROL_AREA));
			assertEquals(4, AreaStatusComparator.convertToLevel(AreaStatusCst.OWNED_AREA));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_convertToLevel_nullParam()
	{
		try
		{
			AreaStatusComparator.convertToLevel(null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
