package org.jocelyn_chaumel.firststrike.core.area;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class AreaSetTest extends MapTestCase
{

	public AreaSetTest(final String p_name)
	{
		super(p_name);
	}

	public void test_getAreaByType_all()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			AreaList areaSet = map.getAreaSet();
			try
			{
				areaSet.getFirstAreaByType(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			assertTrue(map.getAreaSet().getFirstAreaByType(AreaTypeCst.GROUND_AREA).isGroundArea());
			assertFalse(map.getAreaSet().getFirstAreaByType(AreaTypeCst.SEA_AREA).isGroundArea());
			assertEquals(	map.getAreaAt(new Coord(2, 2), AreaTypeCst.GROUND_AREA),
							map.getAreaSet().getFirstAreaByType(AreaTypeCst.GROUND_AREA));

			map = buildFSMap(STANDARD_MAP_4);
			areaSet = map.getAreaSet();
			assertTrue(map.getAreaSet().getFirstAreaByType(AreaTypeCst.GROUND_AREA).isGroundArea());
			assertFalse(map.getAreaSet().getFirstAreaByType(AreaTypeCst.SEA_AREA).isGroundArea());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getAllGroundArea_all()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_4);
			AreaList areaSet = (AreaList) map.getAreaSet().clone();

			assertEquals(6, areaSet.size());
			assertEquals(5, areaSet.getAllGroundAreaSet().size());
			assertTrue(areaSet.getAllGroundAreaSet().contains(map.getAreaAt(new Coord(1, 1), AreaTypeCst.GROUND_AREA)));
			assertTrue(areaSet.getAllGroundAreaSet().contains(map.getAreaAt(new Coord(7, 1), AreaTypeCst.GROUND_AREA)));
			assertTrue(areaSet.getAllGroundAreaSet().contains(map.getAreaAt(new Coord(1, 7), AreaTypeCst.GROUND_AREA)));
			assertTrue(areaSet.getAllGroundAreaSet().contains(map.getAreaAt(new Coord(7, 7), AreaTypeCst.GROUND_AREA)));
			assertTrue(areaSet.getAllGroundAreaSet().contains(map.getAreaAt(new Coord(13, 1), AreaTypeCst.GROUND_AREA)));

			areaSet.remove(map.getAreaAt(new Coord(13, 1), AreaTypeCst.GROUND_AREA));
			assertEquals(5, areaSet.size());
			assertEquals(4, areaSet.getAllGroundAreaSet().size());
			assertTrue(areaSet.getAllGroundAreaSet().contains(map.getAreaAt(new Coord(1, 1), AreaTypeCst.GROUND_AREA)));
			assertTrue(areaSet.getAllGroundAreaSet().contains(map.getAreaAt(new Coord(7, 1), AreaTypeCst.GROUND_AREA)));
			assertTrue(areaSet.getAllGroundAreaSet().contains(map.getAreaAt(new Coord(1, 7), AreaTypeCst.GROUND_AREA)));
			assertTrue(areaSet.getAllGroundAreaSet().contains(map.getAreaAt(new Coord(7, 7), AreaTypeCst.GROUND_AREA)));

			areaSet.clear();
			assertEquals(0, areaSet.size());
			assertEquals(0, areaSet.getAllGroundAreaSet().size());

			areaSet.add(map.getAreaAt(new Coord(0, 0), AreaTypeCst.SEA_AREA));
			assertEquals(1, areaSet.size());
			assertEquals(0, areaSet.getAllGroundAreaSet().size());
			
			assertEquals(5, map.getAreaSet().getAllGroundAreaSetWithCity().size());
			

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
