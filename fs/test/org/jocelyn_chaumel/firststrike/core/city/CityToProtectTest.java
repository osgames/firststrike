/*
 * Created on Aug 19, 2005
 */
package org.jocelyn_chaumel.firststrike.core.city;

import java.util.ArrayList;
import java.util.Collections;

import org.jocelyn_chaumel.firststrike.FSTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class CityToProtectTest extends FSTestCase
{

	/**
	 * @param aName
	 */
	public CityToProtectTest(final String aName)
	{
		super(aName);
	}

	public final void test_compareTo_nullParam()
	{
		try
		{
			new CityToProtect(null, 1, 1);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_compareTo_invalidNbUnit()
	{
		try
		{
			final City city = new City(new CityId(1));
			new CityToProtect(city, -1, 0);
			fail("Invalid nb unit has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_compareTo_invalidDistance()
	{
		try
		{
			final City city = new City(new CityId(1));
			new CityToProtect(city, 0, -1);
			fail("Invalid distance has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_compareTo_valid()
	{
		try
		{
			final City city = new City(new CityId(1));
			final CityToProtect cityToProtect1 = new CityToProtect(city, 0, 0);
			final CityToProtect cityToProtect2 = new CityToProtect(city, 1, 0);
			final CityToProtect cityToProtect3 = new CityToProtect(city, 1, 1);
			final CityToProtect cityToProtect4 = new CityToProtect(city, 2, 0);

			final ArrayList<CityToProtect> list = new ArrayList<CityToProtect>();
			list.add(cityToProtect4);
			list.add(cityToProtect2);
			list.add(cityToProtect1);
			list.add(cityToProtect3);

			Collections.sort(list, Collections.reverseOrder());

			assertEquals(cityToProtect1, list.get(0));
			assertEquals(cityToProtect2, list.get(1));
			assertEquals(cityToProtect3, list.get(2));
			assertEquals(cityToProtect4, list.get(3));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
