/*
 * Created on Jun 24, 2005
 */
package org.jocelyn_chaumel.firststrike.core.city;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.FirstStrikeMgr;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public class CitySetTest extends MapTestCase
{

	/**
	 * Constructor for CityListTest.
	 * 
	 * @param name
	 */
	public CitySetTest(final String name)
	{
		super(name);
	}

	public final void test_hasCityAtPosition_nullParam()
	{
		try
		{
			CityList cityList = new CityList();
			cityList.hasCityAtPosition(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_hasCityAtPosition_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final CityList cityList = map.getCityList();
			assertEquals(true, cityList.hasCityAtPosition(new Coord(1, 1)));
			assertEquals(false, cityList.hasCityAtPosition(new Coord(1, 2)));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getGroupedCityList_nullParam()
	{
		try
		{
			CityList list = new CityList();
			list.getGroupedCityList(null, 2);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getGroupedCityList_invalidRange()
	{
		try
		{
			CityList list = new CityList();
			list.getGroupedCityList(new Coord(1, 1), 0);
			fail("Invalid range has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getGroupedCityList_emptyList()
	{
		try
		{
			CityList list = new CityList();
			list.getGroupedCityList(new Coord(1, 1), 1);
			fail("Invalid operation has been accepted.");
		} catch (IllegalStateException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// Une seule ville
	public final void test_getGroupedCityList_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final CityList cityList = map.getCityList();

			assertEquals(1, cityList.getGroupedCityList(new Coord(0, 0), 1).size());
			assertEquals(9, cityList.getGroupedCityList(new Coord(1, 4), 3).size());
			assertEquals(2, cityList.getGroupedCityList(new Coord(2, 2), 2).size());
			assertEquals(1, cityList.getGroupedCityList(new Coord(0, 1), 1).size());
			assertEquals(1, cityList.getGroupedCityList(new Coord(5, 5), 1).size());
			assertEquals(9, cityList.getGroupedCityList(new Coord(20, 20), 30).size());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getCityAtPosition_nullParam()
	{
		try
		{
			CityList cityList = new CityList();
			cityList.getCityAtPosition(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getCityAtPosition_invalidPosition()
	{
		try
		{
			CityList cityList = new CityList();
			cityList.getCityAtPosition(new Coord(1, 1));
			fail("Invalid position has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getCityWithoutProduction_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final CityList cityList = map.getCityList();
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);

			assertEquals(2, cityList.getCityWithoutProduction().size());

			cityList.getCityAtPosition(new Coord(1, 1)).setProductionType(UnitTypeCst.TANK);
			assertEquals(1, cityList.getCityWithoutProduction().size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getCoordSet_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final CityList cityList = map.getCityList();

			assertEquals(9, cityList.getCoordSet().size());
			assertTrue(cityList.hasCityAtPosition(new Coord(1, 1)));
			assertTrue(cityList.hasCityAtPosition(new Coord(4, 4)));

			assertTrue(cityList.hasCityAtPosition(new Coord(1, 7)));
			assertTrue(cityList.hasCityAtPosition(new Coord(4, 10)));

			assertTrue(cityList.hasCityAtPosition(new Coord(7, 1)));
			assertTrue(cityList.hasCityAtPosition(new Coord(10, 4)));

			assertTrue(cityList.hasCityAtPosition(new Coord(7, 7)));
			assertTrue(cityList.hasCityAtPosition(new Coord(10, 10)));

			assertTrue(cityList.hasCityAtPosition(new Coord(13, 1)));

			final CityList cityListCloned = (CityList) cityList.clone();
			assertEquals(9, cityListCloned.getCoordSet().size());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getCityByPlayer_nullParam()
	{
		try
		{
			final CityList cityList = new CityList();
			try
			{
				cityList.getCityByPlayer(null);
				fail("Null param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getCityByPlayer_and_enemyCity_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final CityList cityList = map.getCityList();
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			HumanPlayer human = (HumanPlayer) game.getPlayerMgr().getPlayerList().get(0);
			ComputerPlayer computer = (ComputerPlayer) game.getPlayerMgr().getPlayerList().get(1);

			assertEquals(2, cityList.getCityWithoutProduction().size());
			assertEquals(7, cityList.getCityWithoutOwner().size());

			assertEquals(1, cityList.getCityByPlayer(human).size());
			assertEquals(1, cityList.getCityByPlayer(human).getCityWithoutProduction().size());
			assertTrue(cityList.getCityByPlayer(human).hasCityAtPosition(new Coord(1, 1)));
			assertEquals(1, cityList.getEnemyCity(human).size());
			assertTrue(cityList.getEnemyCity(human).hasCityAtPosition(new Coord(4, 4)));
			assertEquals(8, cityList.getCityToConquer(human).size());

			assertEquals(1, cityList.getCityByPlayer(computer).size());
			assertTrue(cityList.getCityByPlayer(computer).hasCityAtPosition(new Coord(4, 4)));
			assertEquals(1, cityList.getEnemyCity(computer).size());
			assertTrue(cityList.getEnemyCity(computer).hasCityAtPosition(new Coord(1, 1)));
			assertEquals(8, cityList.getCityToConquer(computer).size());

			cityList.getCityAtPosition(new Coord(7, 7)).setPlayer(human);
			assertEquals(6, cityList.getCityWithoutOwner().size());

			assertEquals(2, cityList.getCityByPlayer(human).size());
			assertEquals(2, cityList.getCityByPlayer(human).getCityWithoutProduction().size());
			assertTrue(cityList.getCityByPlayer(human).hasCityAtPosition(new Coord(1, 1)));
			assertTrue(cityList.getCityByPlayer(human).hasCityAtPosition(new Coord(7, 7)));
			assertEquals(1, cityList.getEnemyCity(human).size());
			assertTrue(cityList.getEnemyCity(human).hasCityAtPosition(new Coord(4, 4)));
			assertEquals(7, cityList.getCityToConquer(human).size());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getShortestDistanceBetweenCoord_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final CityList cityList = map.getCityList();

			CityDistanceFactor distanceFactor = cityList.getShortestDistanceBetweenCoord(new Coord(1, 1));
			assertEquals(0d, distanceFactor.getDistance(), 0.00001d);
			assertEquals(new Coord(1, 1), distanceFactor.getCityRef().getPosition());

			distanceFactor = cityList.getShortestDistanceBetweenCoord(new Coord(0, 0));
			assertEquals(1.5d, distanceFactor.getDistance(), 0.001d);
			assertEquals(new Coord(1, 1), distanceFactor.getCityRef().getPosition());

			distanceFactor = cityList.getShortestDistanceBetweenCoord(new Coord(2, 2));
			assertEquals(1.5d, distanceFactor.getDistance(), 0.00001d);
			assertEquals(new Coord(1, 1), distanceFactor.getCityRef().getPosition());

			distanceFactor = cityList.getShortestDistanceBetweenCoord(new Coord(5, 5));
			assertEquals(1.5d, distanceFactor.getDistance(), 0.00001d);
			assertEquals(new Coord(4, 4), distanceFactor.getCityRef().getPosition());

			distanceFactor = cityList.getShortestDistanceBetweenCoord(new Coord(5, 2));
			assertEquals(2.33d, distanceFactor.getDistance(), 0.01d);
			assertEquals(new Coord(4, 4), distanceFactor.getCityRef().getPosition());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getShortestDistanceBetweenCoord_nullParam()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final CityList cityList = map.getCityList();
			try
			{
				cityList.getShortestDistanceBetweenCoord(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getShortestDistanceBetweenCoord_emptySet()
	{
		try
		{
			final CityList citySet = new CityList();
			try
			{
				citySet.getShortestDistanceBetweenCoord(new Coord(1, 1));
				fail("Illegal operation has been executed");
			} catch (IllegalStateException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getPortList_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final CityList cityList = map.getCityList();

			assertEquals(9, cityList.size());
			assertEquals(9, cityList.getPortList().size());
			assertTrue(cityList.hasCityAtPosition(new Coord(1, 1)));
			assertTrue(cityList.hasCityAtPosition(new Coord(4, 4)));

			assertTrue(cityList.hasCityAtPosition(new Coord(1, 7)));
			assertTrue(cityList.hasCityAtPosition(new Coord(4, 10)));

			assertTrue(cityList.hasCityAtPosition(new Coord(7, 1)));
			assertTrue(cityList.hasCityAtPosition(new Coord(10, 4)));

			assertTrue(cityList.hasCityAtPosition(new Coord(7, 7)));
			assertTrue(cityList.hasCityAtPosition(new Coord(10, 10)));

			assertTrue(cityList.hasCityAtPosition(new Coord(13, 1)));

			final FSMap map2 = buildFSMap(STANDARD_MAP_6);
			final CityList cityList2 = map2.getCityList();

			assertEquals(12, cityList2.size());
			assertEquals(1, cityList2.getPortList().size());
			assertTrue(cityList2.hasCityAtPosition(new Coord(1, 1)));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getNearestCityFrom_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final CityList cityList = map.getCityList();
			assertEquals(cityList.getCityAtPosition(new Coord(1, 1)), cityList.getDirectNearestCityFrom(new Coord(1, 1)));
			assertEquals(cityList.getCityAtPosition(new Coord(1, 1)), cityList.getDirectNearestCityFrom(new Coord(2, 2)));
			assertEquals(cityList.getCityAtPosition(new Coord(4, 4)), cityList.getDirectNearestCityFrom(new Coord(3, 3)));

			final CityList secondCityList = new CityList();
			secondCityList.add(cityList.getCityAtPosition(new Coord(1, 1)));
			secondCityList.add(cityList.getCityAtPosition(new Coord(7, 1)));
			assertEquals(	cityList.getCityAtPosition(new Coord(1, 1)),
							secondCityList.getDirectNearestCityFrom(new Coord(3, 5)));
			assertEquals(	cityList.getCityAtPosition(new Coord(7, 1)),
							secondCityList.getDirectNearestCityFrom(new Coord(5, 5)));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}