/*
 * CityTest.java JUnit based test
 * 
 * Created on 11 juin 2003, 00:08
 */

package org.jocelyn_chaumel.firststrike.core.city;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.FirstStrikeMgr;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * 
 * @author Jocelyn Chaumel
 */
public class CityTest extends MapTestCase
{
	public CityTest(final String aName)
	{
		super(aName);
	}

	public void test_Cntr_nullParam_1()
	{
		try
		{
			new City(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_SetPlayer_all()
	{
		try
		{
			FSMap map = buildFSMap(STANDARD_MAP_1);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			City city = map.getCityList().getCityAtPosition(new Coord(4, 4));
			try
			{
				city.setPlayer(null);
				fail("Invalid player id has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			city.setPlayer(game.getCurrentPlayer());
			assertEquals(game.getCurrentPlayer(), city.getPlayer());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_SetProductionType_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_5);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "0", "0" }, { "COMPUTER", "4", "3" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2), new FSOptionMgr());

			final City city = mapMgr.getCityList().getCityAtPosition(new Coord(0, 0));
			try
			{
				city.setProductionType(UnitTypeCst.TRANSPORT_SHIP);
				fail("Invalid operation has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			city.setProductionType(UnitTypeCst.TANK);
			assertEquals(City.TANK_TIME_PRODUCTION, city.getProductionTime());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getUnitProductionType_noOwner()
	{
		try
		{
			final FSMap map = super.buildFSMap(STANDARD_MAP_4);
			City city = map.getCityList().getCityAtPosition(new Coord(1, 1));
			city.getProductionType();
			fail("Invalid operation has been executed");
		} catch (IllegalStateException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_get_and_decProductionTime_all()
	{
		try
		{
			final FSMap map = super.buildFSMap(STANDARD_MAP_4);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);

			City city = map.getCityList().getCityAtPosition(new Coord(1, 1));
			City notOwnedcity = map.getCityList().getCityAtPosition(new Coord(7, 1));

			try
			{
				notOwnedcity.setProductionType(UnitTypeCst.DESTROYER);
				fail("Production type was not specified");
			} catch (IllegalStateException ex)
			{ // NOP
			}

			try
			{
				notOwnedcity.decProductionTime();
				fail("Owner & production type hasn't been specified");
			} catch (IllegalStateException ex)
			{ // NOP
			}

			try
			{
				city.getProductionTime();
				fail("Production type was not specified");
			} catch (IllegalStateException ex)
			{ // NOP
			}

			city.setProductionType(UnitTypeCst.FIGHTER);
			assertEquals(UnitTypeCst.FIGHTER, city.getProductionType());
			assertEquals(City.FIGHTER_TIME_PRODUCTION, city.getProductionTime());
			city.decProductionTime();
			assertEquals(City.FIGHTER_TIME_PRODUCTION - 1, city.getProductionTime());
			city.decProductionTime();
			assertEquals(City.FIGHTER_TIME_PRODUCTION - 2, city.getProductionTime());
			city.decProductionTime();
			assertEquals(City.FIGHTER_TIME_PRODUCTION - 3, city.getProductionTime());
			city.decProductionTime();
			assertEquals(City.FIGHTER_TIME_PRODUCTION - 4, city.getProductionTime());
			city.decProductionTime();

			try
			{
				city.decProductionTime();
				fail("Invalid operation has been accepted");
			} catch (IllegalStateException ex)
			{ // NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}