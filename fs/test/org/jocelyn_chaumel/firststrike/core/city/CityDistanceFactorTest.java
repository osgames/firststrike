/*
 * Created on Jul 14, 2005
 */
package org.jocelyn_chaumel.firststrike.core.city;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public class CityDistanceFactorTest extends MapTestCase
{

	/**
	 * @param aName
	 */
	public CityDistanceFactorTest(final String aName)
	{
		super(aName);
	}

	public final void test_Cntr_nullParam_1()
	{
		try
		{
			new CityDistanceFactor(null, new Coord(1, 1));
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_nullParam_2()
	{
		try
		{
			City city = new City(new CityId(1));
			new CityDistanceFactor(city, null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_compareTo_valid()
	{
		try
		{
			FSMap map = super.buildFSMap(STANDARD_MAP_1);
			Coord position = new Coord(0, 0);
			City city = map.getCellAt(1, 1).getCity();
			CityDistanceFactor factor = new CityDistanceFactor(city, position);

			assertEquals(1, factor.compareTo(null));

			assertEquals(0, factor.compareTo(factor));

			City far_city = map.getCellAt(4, 4).getCity();
			assertEquals(1, factor.compareTo(new CityDistanceFactor(far_city, position)));

			assertEquals(0, factor.compareTo(new CityDistanceFactor(city, position)));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}
