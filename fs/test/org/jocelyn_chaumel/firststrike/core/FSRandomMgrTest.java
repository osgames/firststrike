package org.jocelyn_chaumel.firststrike.core;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.tools.data_type.FloatList;

public class FSRandomMgrTest extends MapTestCase
{

	public FSRandomMgrTest(final String aName)
	{
		super(aName);
	}

	public final void test_cntr_valid()
	{
		try
		{
			final FSRandomMgr mgr = new FSRandomMgr(2);
			assertEquals(2, mgr.getRandomFloatListList().size());
			assertEquals(10, ((FloatList) mgr.getRandomFloatListList().get(0)).size());
			assertEquals(10, ((FloatList) mgr.getRandomFloatListList().get(1)).size());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_finishTurn_valid()
	{
		try
		{
			final FSRandomMgr mgr = new FSRandomMgr(2);

			assertEquals(2, mgr.getRandomFloatListList().size());
			assertEquals(10, ((FloatList) mgr.getRandomFloatListList().get(0)).size());
			assertEquals(10, ((FloatList) mgr.getRandomFloatListList().get(1)).size());

			final FSRandom random = FSRandom.getInstance();

			int i = 6 + 1;
			while (--i != 0)
			{
				random.getRandomFloat();
			}
			assertEquals(4, ((FloatList) mgr.getRandomFloatListList().get(0)).size());

			mgr.finishTurn();
			assertEquals(2, mgr.getRandomFloatListList().size());
			assertEquals(20, ((FloatList) mgr.getRandomFloatListList().get(0)).size());
			assertEquals(20, ((FloatList) mgr.getRandomFloatListList().get(1)).size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
