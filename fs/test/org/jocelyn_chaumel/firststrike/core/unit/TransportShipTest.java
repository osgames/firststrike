package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.params.IllegalParameterException;

public class TransportShipTest extends MapTestCase
{

	public TransportShipTest(final String p_name)
	{
		super(p_name);
	}

	public final void test_choiseEnemyTargetFromList_valid()
	{
		try
		{
			final AbstractUnitList enemyList = new AbstractUnitList();
			final TransportShip transport = new TransportShip(new Coord(1, 1), COMPUTER_PLAYER_1);

			// ======================================
			final AbstractUnit enemyTank1 = new Tank(new Coord(2, 2), COMPUTER_PLAYER_2);
			enemyList.add(enemyTank1);

			try
			{
				assertEquals(enemyTank1, transport.chooseBestEnemyTargetFromList(enemyList));
				fail("Unsupported operation has been executed");
			} catch (UnsupportedOperationException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_addContainedUnit_valid()
	{
		try
		{
			final TransportShip transport_1 = new TransportShip(new Coord(1, 1), COMPUTER_PLAYER_1);
			final TransportShip transport_2 = new TransportShip(new Coord(1, 1), COMPUTER_PLAYER_1);

			try
			{
				transport_1.loadUnit(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			final Tank invalidTank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_2);
			try
			{
				transport_1.loadUnit(invalidTank);
				fail("Invalid tank has been loaded");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			final Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			assertEquals(false, transport_1.isContainingUnit());
			transport_1.loadUnit(tank);
			assertEquals(1, transport_1.getContainedUnitList().size());

			try
			{
				transport_2.loadUnit(invalidTank);
				fail("Tank already contained tank by another transport has been loaded");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				transport_1.loadUnit(invalidTank);
				fail("A tank has been loaded twice");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_unloadTank_nullParam()
	{
		try
		{
			final TransportShip transport = new TransportShip(new Coord(1, 1), COMPUTER_PLAYER_1);
			try
			{
				transport.unloadUnit(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getAvailableSpace_valid_and_noMoreSpace()
	{
		try
		{
			final TransportShip transport = new TransportShip(new Coord(1, 1), COMPUTER_PLAYER_1);
			final Tank tank1 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			final Tank tank2 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			final Tank tank3 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);

			final int maxCapacity = transport.getSpaceCapacity();
			assertEquals(maxCapacity, transport.getFreeSpace());
			transport.loadUnit(tank1);
			assertEquals(maxCapacity - tank1.getSpaceCost(), transport.getFreeSpace());
			transport.loadUnit(tank2);
			assertEquals(maxCapacity - (tank1.getSpaceCost() + tank2.getSpaceCost()), transport.getFreeSpace());
			transport.loadUnit(tank3);
			assertEquals(	maxCapacity - (tank1.getSpaceCost() + tank2.getSpaceCost() + tank3.getSpaceCost()),
							transport.getFreeSpace());

			final Tank tank4 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			try
			{
				transport.loadUnit(tank4);
				fail("Invalid operation has been executed");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_restoreMove_valid()
	{
		try
		{
			final TransportShip transport = new TransportShip(new Coord(1, 1), COMPUTER_PLAYER_1);
			final Destroyer destroyer = new Destroyer(new Coord(1, 1), COMPUTER_PLAYER_1);
			final Tank tank1 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			transport.loadUnit(tank1);

			destroyer.setMove(0);
			tank1.setMove(0);
			tank1.setHitPoints(20);

			transport.restoreMove();
			assertEquals(tank1.getMovementCapacity(), tank1.getMove());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
