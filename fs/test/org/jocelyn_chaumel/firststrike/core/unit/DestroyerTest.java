package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class DestroyerTest extends MapTestCase
{

	public DestroyerTest(final String p_name)
	{
		super(p_name);
	}

	public final void test_choiseEnemyTargetFromList_valid()
	{
		try
		{
			final AbstractUnitList enemyList = new AbstractUnitList();
			final Destroyer destroyer = new Destroyer(new Coord(1, 1), COMPUTER_PLAYER_1);

			final AbstractUnit enemyTank1 = new Tank(new Coord(2, 2), COMPUTER_PLAYER_2);
			enemyList.add(enemyTank1);
			assertEquals(enemyTank1, destroyer.chooseBestEnemyTargetFromList(enemyList));

			final AbstractUnit enemyDestoyer = new Destroyer(new Coord(2, 2), COMPUTER_PLAYER_2);
			enemyList.add(enemyDestoyer);
			assertEquals(enemyTank1, destroyer.chooseBestEnemyTargetFromList(enemyList));

			final AbstractUnit enemyFighter = new Fighter(new Coord(2, 2), COMPUTER_PLAYER_2);
			enemyList.add(enemyFighter);
			assertEquals(enemyFighter, destroyer.chooseBestEnemyTargetFromList(enemyList));

			final AbstractUnit enemyTansport1 = new TransportShip(new Coord(2, 2), COMPUTER_PLAYER_2);
			enemyList.add(enemyTansport1);
			assertEquals(enemyFighter, destroyer.chooseBestEnemyTargetFromList(enemyList));

			enemyDestoyer.setHitPoints(30);
			assertEquals(enemyDestoyer, destroyer.chooseBestEnemyTargetFromList(enemyList));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}
