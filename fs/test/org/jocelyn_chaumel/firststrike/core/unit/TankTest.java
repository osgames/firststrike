/*
 * Created on Dec 28, 2004
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithoutCombatDirective;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;

/**
 * @author Jocelyn Chaumel
 */
public class TankTest extends MapTestCase
{
	/**
	 * @param aName
	 */
	public TankTest(final String aName)
	{
		super(aName);
	}

	public final void test_Cntr_nullParam_1()
	{
		try
		{
			new Tank(null, COMPUTER_PLAYER_1);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_nullParam_2()
	{
		try
		{
			new Tank(new Coord(1, 1), null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_SetPath_nullParam()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);

			tank.setPath(null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_SetPath_invalidPath()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);

			CoordList coordList = new CoordList();
			coordList.add(new Coord(3, 3));
			Path path = new Path(coordList);

			try
			{
				tank.setPath(path);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetPath_noPathSetted()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);

			tank.getPath();
			fail("Invalid operation has been executed");
		} catch (IllegalStateException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Detect_nullParam()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);

			tank.detect(null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Detect_incoherentPlayerId()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			try
			{
				tank.detect(new Tank(new Coord(1, 1), COMPUTER_PLAYER_1));
				fail("Incoherent operation has been executed");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Detect_enemyTooFar()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			Tank enemy = new Tank(new Coord(3, 3), COMPUTER_PLAYER_2);

			assertFalse(tank.detect(enemy));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Detect_Valid()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);

			Tank enemy = new Tank(new Coord(2, 1), COMPUTER_PLAYER_2);
			assertEquals(true, tank.detect(enemy));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Move_emptyPath()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			try
			{
				tank.move();

				fail("Null param has been accepted");
			} catch (IllegalStateException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Move_valid_Not_Move_Pts()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			CoordList list = new CoordList();
			list.add(new Coord(2, 1));
			list.add(new Coord(3, 1));
			list.add(new Coord(4, 1));
			tank.setPath(new Path(list));

			tank.setMove(66);
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, tank.move());
			assertEquals(new Coord(2, 1), tank.getPosition());
			assertEquals(true, tank.hasPath());

			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, tank.move());
			assertEquals(new Coord(3, 1), tank.getPosition());

			assertEquals(0, tank.getMove());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, tank.move());
			assertEquals(new Coord(3, 1), tank.getPosition());
			assertEquals(0, tank.getMove());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_decreaseNbAttack_noAttack()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);

			tank.decreaseNbAttack();
			tank.decreaseNbAttack();
			try
			{
				tank.decreaseNbAttack();
				fail("Null param has been accepted");
			} catch (IllegalStateException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_decreaseNbAttack_valid()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);

			tank.decreaseNbAttack();
			assertEquals(1, tank.getNbAttack());
			assertEquals(75, tank.getMove());
			tank.decreaseNbAttack();
			assertEquals(0, tank.getNbAttack());
			assertEquals(0, tank.getMove());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_decreaseNbAttack_noMove()
	{
		try
		{
			final Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			final CoordList coordList = new CoordList();
			coordList.add(new Coord(2, 2));
			coordList.add(new Coord(1, 1));
			coordList.add(new Coord(2, 2));
			tank.setPath(new Path(coordList));

			tank.move();
			tank.move();
			tank.decreaseNbAttack();
			try
			{
				tank.decreaseNbAttack();
				fail("Null param has been accepted");
			} catch (IllegalStateException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_chooseBestEnemyTargetFromList_valid()
	{
		try
		{
			final AbstractUnitList enemyList = new AbstractUnitList();
			final Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			final AbstractUnit enemyTank1 = new Tank(new Coord(2, 2), COMPUTER_PLAYER_2);
			enemyList.add(enemyTank1);
			assertEquals(enemyTank1, tank.chooseBestEnemyTargetFromList(enemyList));

			AbstractUnit enemyFigther1 = new Fighter(new Coord(2, 2), COMPUTER_PLAYER_2);
			enemyList.add(enemyFigther1);
			assertEquals(enemyFigther1, tank.chooseBestEnemyTargetFromList(enemyList));

			enemyList.clear();
			AbstractUnit enemyFigther2 = new Fighter(new Coord(4, 1), COMPUTER_PLAYER_2);
			enemyList.add(enemyTank1);
			enemyList.add(enemyFigther2);

			assertEquals(enemyTank1, tank.chooseBestEnemyTargetFromList(enemyList));

			AbstractUnit destroyer1 = new Destroyer(new Coord(2, 2), COMPUTER_PLAYER_2);
			enemyList.add(destroyer1);
			assertEquals(enemyTank1, tank.chooseBestEnemyTargetFromList(enemyList));

			AbstractUnit destroyer2 = new Destroyer(new Coord(2, 2), COMPUTER_PLAYER_2);
			enemyList.add(destroyer2);
			destroyer2.setHitPoints(60);
			assertEquals(destroyer2, tank.chooseBestEnemyTargetFromList(enemyList));

			try
			{
				tank.chooseBestEnemyTargetFromList(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			} catch (Exception ex)
			{
				fail(ex);
			}

			try
			{
				tank.chooseBestEnemyTargetFromList(new AbstractUnitList());
				fail("Empty unit list has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			} catch (Exception ex)
			{
				fail(ex);
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}


	public final void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Tank computerTank1 = new Tank(new Coord(10, 10), computer);
			computer.addUnit(computerTank1, true);

			computerTank1.addDirective(new FindPathAndMoveWithoutCombatDirective(	computerTank1,
																					new Coord(8, 10),
																					true,
																					false));
			computerTank1.addDirective(new FindPathAndMoveWithoutCombatDirective(	computerTank1,
																					new Coord(8, 8),
																					true,
																					false));
			assertEquals(2, computerTank1.getDirectiveList().size());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, computerTank1.playDirectives());
			assertEquals(new Coord(8, 9), computerTank1.getPosition());
			assertEquals(1, computerTank1.getDirectiveList().size());

			computerTank1.restoreMove();
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, computerTank1.playDirectives());
			assertEquals(new Coord(8, 8), computerTank1.getPosition());
			assertEquals(67, computerTank1.getMove());
			assertEquals(0, computerTank1.getDirectiveList().size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}