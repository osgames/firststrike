package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.FSTestCase;

public class MoveStatusCstTest extends FSTestCase
{

	public MoveStatusCstTest(final String aName)
	{
		super(aName);
	}

	public final void test_isAlive_valid()
	{
		try
		{
			assertTrue(MoveStatusCst.isAlive(MoveStatusCst.ENEMY_DETECTED));
			assertTrue(MoveStatusCst.isAlive(MoveStatusCst.MOVE_SUCCESFULL));
			assertTrue(MoveStatusCst.isAlive(MoveStatusCst.NOT_ENOUGH_MOVE_PTS));
			assertFalse(MoveStatusCst.isAlive(MoveStatusCst.NOT_ENOUGH_FUEL_PTS));
			assertFalse(MoveStatusCst.isAlive(MoveStatusCst.UNIT_DEFEATED));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_isAlive_nullParam()
	{
		try
		{
			assertTrue(MoveStatusCst.isAlive(null));
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
