/*
 * Created on Dec 28, 2004
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSFatalException;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithoutCombatDirective;
import org.jocelyn_chaumel.firststrike.core.directive.tank.TankAutoDirective;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;

/**
 * @author Jocelyn Chaumel
 */
public class RadarStationTest extends MapTestCase
{
	/**
	 * @param aName
	 */
	public RadarStationTest(final String aName)
	{
		super(aName);
	}

	public final void test_Cntr_nullParam_1()
	{
		try
		{
			
			new RadarStation(null, COMPUTER_PLAYER_1);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Cntr_nullParam_2()
	{
		try
		{
			new RadarStation(new Coord(1, 1), null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_invalidMethod() {
		try 
		{
			final RadarStation radar = new RadarStation(new Coord(1, 1), COMPUTER_PLAYER_1);
			try 
			{

				radar.setPath(new Path(new Coord(1,1)));
				fail("Invalid method performed");
			} catch (FSFatalException ex) 
			{
				// NOP
			}
			
			
			final ComputerTank tank = new ComputerTank(new Coord(1,1), COMPUTER_PLAYER_1);
			try 
			{
				radar.addDirective(new TankAutoDirective(tank));
				fail("Invalid method performed");
			} catch (FSFatalException ex) 
			{
				// NOP
			}

		} catch (Exception ex) {
			fail(ex);
		}
	}

	public final void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray, PlayerLevelCst.EASY_LEVEL);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);

			final Tank humanTank = new Tank(new Coord(1,1), human);
			human.addUnit(humanTank, false);
			
			final Destroyer humanDest = new Destroyer(new Coord(0,0), human);
			human.addUnit(humanDest, false);
			
			final RadarStation computerRS = new RadarStation(new Coord(10, 10), computer);
			computer.addUnit(computerRS, true);
			
			computer.startTurn(1);

			
			assertEquals(1, computer.getEnemySet().size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}