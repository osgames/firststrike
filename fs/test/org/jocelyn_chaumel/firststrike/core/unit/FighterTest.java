/*
 * Created on Mar 7, 2005
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.FirstStrikeMgr;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;

/**
 * @author Jocelyn Chaumel
 */
public class FighterTest extends MapTestCase
{

	public FighterTest(final String aName)
	{
		super(aName);
	}

	public final void test_Move_NotEnoughFuel_validAndInvalid()
	{
		try
		{
			Fighter fighter = new Fighter(new Coord(1, 1), COMPUTER_PLAYER_1);
			final CoordList list = new CoordList();
			for (int i = 0; i < 20; i++)
			{
				list.add(new Coord(2, 2));
				list.add(new Coord(1, 1));
			}
			fighter.setPath(new Path(list));

			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, fighter.move());
			assertEquals(new Coord(2, 2), fighter.getPosition());
			assertEquals(190, fighter.getFuel());
			assertEquals(90, fighter.getMove());

			fighter.move();
			fighter.move();
			assertEquals(170, fighter.getFuel());
			assertEquals(70, fighter.getMove());

			fighter.move();
			fighter.move();
			assertEquals(150, fighter.getFuel());
			assertEquals(50, fighter.getMove());

			fighter.move();
			fighter.move();
			assertEquals(130, fighter.getFuel());
			assertEquals(30, fighter.getMove());

			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, fighter.move());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, fighter.move());
			assertEquals(110, fighter.getFuel());
			assertEquals(10, fighter.getMove());

			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, fighter.move());
			assertEquals(100, fighter.getFuel());
			assertEquals(0, fighter.getMove());

			fighter.restoreMove();
			assertEquals(100, fighter.getMove());

			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, fighter.move());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, fighter.move());
			assertEquals(80, fighter.getFuel());
			assertEquals(80, fighter.getMove());

			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, fighter.move());
			fighter.move();
			fighter.move();
			fighter.move();
			fighter.move();
			fighter.move();
			fighter.restoreMove();
			assertEquals(100, fighter.getMove());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, fighter.move());
			assertEquals(90, fighter.getMove());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, fighter.move());
			assertEquals(MoveStatusCst.NOT_ENOUGH_FUEL_PTS, fighter.move());

			assertEquals(0, fighter.getFuel());
			assertEquals(80, fighter.getMove());
			fighter.setHitPoints(-1);
			try
			{
				fighter.move();
				fail("Invalid Operation has been executed");
			} catch (IllegalStateException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			// COMPUTER_PLAYER_1.getControlledUnitList().clear();
		}
	}

	public final void test_addUnit_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();

			final HumanPlayer human = (HumanPlayer) game.getPlayerMgr().getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) game.getPlayerMgr().getPlayerList().get(1);

			computer.addUnit(new Fighter(new Coord(5, 0), computer), true);
			human.addUnit(new Fighter(new Coord(5, 1), human), true);

			try
			{
				human.addUnit(new Fighter(new Coord(5, 0), human), true);
				fail("Invalid unit position has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_choiseEnemyTargetFromList()
	{
		try
		{
			final Fighter figther = new Fighter(new Coord(0, 0), COMPUTER_PLAYER_1);

			AbstractUnitList enemyList = new AbstractUnitList();
			final Tank enemyTank1 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_2);

			enemyList.add(enemyTank1);
			assertEquals(enemyTank1, figther.chooseBestEnemyTargetFromList(enemyList));

			final Destroyer enemyDestroyer1 = new Destroyer(new Coord(2, 0), COMPUTER_PLAYER_2);
			enemyList.add(enemyDestroyer1);
			assertEquals(enemyTank1, figther.chooseBestEnemyTargetFromList(enemyList));

			enemyDestroyer1.setHitPoints(20);
			assertEquals(enemyDestroyer1, figther.chooseBestEnemyTargetFromList(enemyList));

			enemyDestroyer1.setHitPoints(enemyDestroyer1.getHitPointsCapacity());
			final Fighter enemyFighter1 = new Fighter(new Coord(1, 0), COMPUTER_PLAYER_2);
			enemyList.add(enemyFighter1);
			assertEquals(enemyFighter1, figther.chooseBestEnemyTargetFromList(enemyList));

			final TransportShip enemyTransport1 = new TransportShip(new Coord(1, 0), COMPUTER_PLAYER_2);
			final TransportShip enemyTransport2 = new TransportShip(new Coord(1, 0), COMPUTER_PLAYER_2);
			enemyTransport2.loadUnit(new Tank(enemyTransport2.getPosition(), COMPUTER_PLAYER_2));
			enemyTransport2.loadUnit(new Tank(enemyTransport2.getPosition(), COMPUTER_PLAYER_2));

			enemyList.clear();
			enemyList.add(enemyTransport1);
			enemyList.add(enemyTransport2);
			assertEquals(enemyTransport2, figther.chooseBestEnemyTargetFromList(enemyList));

			try
			{
				figther.chooseBestEnemyTargetFromList(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				figther.chooseBestEnemyTargetFromList(new AbstractUnitList());
				fail("Empty list has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}
