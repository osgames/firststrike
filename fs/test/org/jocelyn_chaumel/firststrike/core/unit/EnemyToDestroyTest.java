/*
 * Created on Jul 14, 2005
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import java.util.ArrayList;
import java.util.Collections;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public class EnemyToDestroyTest extends MapTestCase
{

	/**
	 * @param aName
	 */
	public EnemyToDestroyTest(final String aName)
	{
		super(aName);
	}

	public final void test_cntr_nullParam()
	{
		try
		{
			new EnemyToDestroy(null, 1);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_cntr_invalidPriority()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			try
			{
				new EnemyToDestroy(tank, -1);
				fail("Invaid destruction priority has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_setDestructionPriority_invalidPriority()
	{
		try
		{
			Tank tank = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			EnemyToDestroy enemy = new EnemyToDestroy(tank, 1);
			try
			{
				enemy.setDestructionPriority(-1);
				fail("Invalid destruction priority has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_setDestructionPriority_valid()
	{
		try
		{
			Tank tank1 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			Tank tank2 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_1);
			EnemyToDestroy enemy1 = new EnemyToDestroy(tank1, 1);
			EnemyToDestroy enemy2 = new EnemyToDestroy(tank2, 2);
			ArrayList<EnemyToDestroy> list = new ArrayList<EnemyToDestroy>();
			list.add(enemy1);
			list.add(enemy2);

			assertEquals(enemy1, list.get(0));
			assertEquals(enemy2, list.get(1));
			Collections.sort(list);
			assertEquals(enemy2, list.get(0));
			assertEquals(enemy1, list.get(1));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
