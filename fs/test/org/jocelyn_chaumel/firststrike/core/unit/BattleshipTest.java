package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class BattleshipTest extends MapTestCase
{

	public BattleshipTest(final String p_aName)
	{
		super(p_aName);
	}

	public final void test_chooseTargetCellForBombardment()
	{
		try
		{
			// See BattleshipBombardmentDirectiveTest
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_chooseEnemyTargetFromList()
	{
		try
		{
			final Battleship battleship = new Battleship(new Coord(0, 0), COMPUTER_PLAYER_1);

			AbstractUnitList enemyList = new AbstractUnitList();
			final Tank enemyTank1 = new Tank(new Coord(1, 1), COMPUTER_PLAYER_2);

			enemyList.add(enemyTank1);
			assertEquals(enemyTank1, battleship.chooseBestEnemyTargetFromList(enemyList));

			final Destroyer enemyDestroyer1 = new Destroyer(new Coord(2, 0), COMPUTER_PLAYER_2);
			enemyList.add(enemyDestroyer1);
			assertEquals(enemyTank1, battleship.chooseBestEnemyTargetFromList(enemyList));

			enemyDestroyer1.setHitPoints(20);
			assertEquals(enemyDestroyer1, battleship.chooseBestEnemyTargetFromList(enemyList));

			enemyDestroyer1.setHitPoints(enemyDestroyer1.getHitPointsCapacity());
			final Fighter enemyFighter1 = new Fighter(new Coord(1, 0), COMPUTER_PLAYER_2);
			enemyList.add(enemyFighter1);
			assertEquals(enemyFighter1, battleship.chooseBestEnemyTargetFromList(enemyList));

			final TransportShip enemyTransport1 = new TransportShip(new Coord(1, 0), COMPUTER_PLAYER_2);
			final TransportShip enemyTransport2 = new TransportShip(new Coord(1, 0), COMPUTER_PLAYER_2);
			enemyTransport2.loadUnit(new Tank(enemyTransport2.getPosition(), COMPUTER_PLAYER_2));
			enemyTransport2.loadUnit(new Tank(enemyTransport2.getPosition(), COMPUTER_PLAYER_2));

			enemyList.clear();
			enemyList.add(enemyTransport1);
			enemyList.add(enemyTransport2);
			assertEquals(enemyTransport2, battleship.chooseBestEnemyTargetFromList(enemyList));

			try
			{
				battleship.chooseBestEnemyTargetFromList(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				battleship.chooseBestEnemyTargetFromList(new AbstractUnitList());
				fail("Empty list has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}
