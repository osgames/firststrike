/*
 * Created on Aug 7, 2005
 */
package org.jocelyn_chaumel.firststrike.core.unit;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.FirstStrikeMgr;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public class AbstractUnitListTest extends MapTestCase
{
	/**
	 * @param aName
	 */
	public AbstractUnitListTest(final String aName)
	{
		super(aName);
	}

	public final void test_getNbUnitAtPosition_nullParam()
	{
		try
		{
			new AbstractUnitList().getNbUnitAtPosition(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getNbUnitAtPosition_valid()
	{
		try
		{
			final Fighter fighter_1 = new Fighter(new Coord(0, 0), COMPUTER_PLAYER_1);
			final Fighter fighter_2 = new Fighter(new Coord(0, 0), COMPUTER_PLAYER_1);
			final Fighter fighter_3 = new Fighter(new Coord(1, 1), COMPUTER_PLAYER_1);
			final AbstractUnitList list = new AbstractUnitList();
			assertEquals(0, list.getNbUnitAtPosition(new Coord(0, 0)));

			list.add(fighter_1);
			assertEquals(1, list.getNbUnitAtPosition(new Coord(0, 0)));
			assertEquals(0, list.getNbUnitAtPosition(new Coord(1, 1)));

			list.add(fighter_2);
			assertEquals(2, list.getNbUnitAtPosition(new Coord(0, 0)));
			assertEquals(0, list.getNbUnitAtPosition(new Coord(1, 1)));

			list.add(fighter_3);
			assertEquals(2, list.getNbUnitAtPosition(new Coord(0, 0)));
			assertEquals(1, list.getNbUnitAtPosition(new Coord(1, 1)));
			assertEquals(0, list.getNbUnitAtPosition(new Coord(2, 2)));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getCoordList_valid()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();
			final Fighter fighter_1 = new Fighter(new Coord(0, 0), COMPUTER_PLAYER_1);
			final Fighter fighter_2 = new Fighter(new Coord(0, 1), COMPUTER_PLAYER_1);
			final Fighter fighter_3 = new Fighter(new Coord(0, 1), COMPUTER_PLAYER_1);

			assertEquals(0, list.getCoordList().size());

			list.add(fighter_1);
			assertEquals(1, list.getCoordList().size());

			list.add(fighter_2);
			assertEquals(2, list.getCoordList().size());
			assertEquals(new Coord(0, 0), list.getCoordList().get(0));
			assertEquals(new Coord(0, 1), list.getCoordList().get(1));

			list.add(fighter_3);
			assertEquals(3, list.getCoordList().size());
			assertEquals(new Coord(0, 0), list.getCoordList().get(0));
			assertEquals(new Coord(0, 1), list.getCoordList().get(1));
			assertEquals(new Coord(0, 1), list.getCoordList().get(2));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getCoordSet_valid()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();
			final Fighter fighter_1 = new Fighter(new Coord(0, 0), COMPUTER_PLAYER_1);
			final Fighter fighter_2 = new Fighter(new Coord(0, 1), COMPUTER_PLAYER_1);
			final Fighter fighter_3 = new Fighter(new Coord(0, 1), COMPUTER_PLAYER_1);

			assertEquals(0, list.getCoordSet().size());

			list.add(fighter_1);
			assertEquals(1, list.getCoordSet().size());

			list.add(fighter_2);
			assertEquals(2, list.getCoordSet().size());
			assertEquals(true, list.getCoordSet().contains(new Coord(0, 0)));
			assertEquals(true, list.getCoordSet().contains(new Coord(0, 1)));
			assertEquals(false, list.getCoordSet().contains(new Coord(0, 2)));

			list.add(fighter_3);
			assertEquals(2, list.getCoordSet().size());
			assertEquals(true, list.getCoordSet().contains(new Coord(0, 0)));
			assertEquals(true, list.getCoordSet().contains(new Coord(0, 1)));
			assertEquals(false, list.getCoordSet().contains(new Coord(0, 2)));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getUnitByType_valid()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();
			final Fighter fighter_1 = new Fighter(new Coord(0, 0), COMPUTER_PLAYER_1);
			final Fighter fighter_2 = new Fighter(new Coord(0, 1), COMPUTER_PLAYER_1);
			final Tank tank_3 = new Tank(new Coord(0, 1), COMPUTER_PLAYER_1);
			final TransportShip transport_4 = new TransportShip(new Coord(0, 1), COMPUTER_PLAYER_1);

			assertEquals(0, list.getUnitByType(UnitTypeCst.TANK).size());

			list.add(fighter_1);
			assertEquals(0, list.getUnitByType(UnitTypeCst.DESTROYER).size());
			assertEquals(0, list.getUnitByType(UnitTypeCst.TANK).size());
			assertEquals(0, list.getUnitByType(UnitTypeCst.TRANSPORT_SHIP).size());
			assertEquals(1, list.getUnitByType(UnitTypeCst.FIGHTER).size());

			list.add(fighter_2);
			assertEquals(0, list.getUnitByType(UnitTypeCst.DESTROYER).size());
			assertEquals(0, list.getUnitByType(UnitTypeCst.TANK).size());
			assertEquals(0, list.getUnitByType(UnitTypeCst.TRANSPORT_SHIP).size());
			assertEquals(2, list.getUnitByType(UnitTypeCst.FIGHTER).size());

			list.add(tank_3);
			assertEquals(0, list.getUnitByType(UnitTypeCst.DESTROYER).size());
			assertEquals(1, list.getUnitByType(UnitTypeCst.TANK).size());
			assertEquals(0, list.getUnitByType(UnitTypeCst.TRANSPORT_SHIP).size());
			assertEquals(2, list.getUnitByType(UnitTypeCst.FIGHTER).size());

			list.add(transport_4);
			assertEquals(0, list.getUnitByType(UnitTypeCst.DESTROYER).size());
			assertEquals(1, list.getUnitByType(UnitTypeCst.TANK).size());
			assertEquals(1, list.getUnitByType(UnitTypeCst.TRANSPORT_SHIP).size());
			assertEquals(2, list.getUnitByType(UnitTypeCst.FIGHTER).size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getUnitByPlayer_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			final PlayerMgr playerMgr = game.getPlayerMgr();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Fighter fighter_1 = new Fighter(new Coord(0, 0), human);
			final Fighter fighter_2 = new Fighter(new Coord(0, 1), human);
			final Tank enemyTank = new Tank(new Coord(2, 1), computer);

			final AbstractUnitList list = new AbstractUnitList();
			assertEquals(0, list.getUnitByPlayer(computer).size());
			assertEquals(0, list.getUnitByPlayer(human).size());

			list.add(fighter_1);
			assertEquals(0, list.getUnitByPlayer(computer).size());
			assertEquals(1, list.getUnitByPlayer(human).size());

			list.add(enemyTank);
			assertEquals(1, list.getUnitByPlayer(computer).size());
			assertEquals(enemyTank, list.getUnitByPlayer(computer).get(0));
			assertEquals(1, list.getUnitByPlayer(human).size());
			assertEquals(fighter_1, list.getUnitByPlayer(human).get(0));

			list.add(fighter_2);
			assertEquals(1, list.getUnitByPlayer(computer).size());
			assertEquals(enemyTank, list.getUnitByPlayer(computer).get(0));
			assertEquals(2, list.getUnitByPlayer(human).size());
			assertTrue(list.getUnitByPlayer(human).contains(fighter_1));
			assertTrue(list.getUnitByPlayer(human).contains(fighter_2));

			try
			{
				list.getUnitByPlayer(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getUnitByType_nullParam()
	{
		try
		{
			new AbstractUnitList().getUnitByType(null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getUnitByType_invalidParam()
	{
		try
		{
			new AbstractUnitList().getUnitByType(UnitTypeCst.NULL_TYPE);
			fail("Invalid param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getTankProtectingArea_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();
			final PlayerMgr playerMgr = game.getPlayerMgr();
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final AbstractUnitList unitList = new AbstractUnitList();
			final Area groundArea = map.getAreaAt(new Coord(2, 2), AreaTypeCst.GROUND_AREA);
			final Area seaArea = map.getAreaAt(new Coord(0, 0), AreaTypeCst.SEA_AREA);

			final ComputerTank tank1 = new ComputerTank(new Coord(3, 3), computer);
			computer.addUnit(tank1, true);
			unitList.add(tank1);
			assertEquals(1, unitList.getTankProtectingArea(groundArea).size());
			assertEquals(0, unitList.getTankProtectingArea(seaArea).size());

			final ComputerTank tank2 = new ComputerTank(new Coord(3, 3), computer);
			computer.addUnit(tank2, true);
			unitList.add(tank2);
			tank2.setProtectedCity(groundArea.getCityList().getCityAtPosition(new Coord(4, 4)));
			assertEquals(1, unitList.getTankProtectingArea(groundArea).size());
			assertTrue(unitList.getTankProtectingArea(groundArea).contains(tank1));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getNearestUnit_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();
			final PlayerMgr playerMgr = game.getPlayerMgr();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final AbstractUnitList unitList = new AbstractUnitList();

			final ComputerTank tank = new ComputerTank(new Coord(4,4), computer);
			final Tank humanTank1 = new Tank(new Coord(7,4), human);
			
			computer.addUnit(tank, true);
			human.addUnit(humanTank1, true);
			
			unitList.add(humanTank1);
			assertNull(unitList.getNearestUnit(tank, game.getMapMgr()));

			final Tank humanTank2 = new Tank(new Coord(1,1), human);
			unitList.add(humanTank2);
			assertEquals(humanTank2, unitList.getNearestUnit(tank, game.getMapMgr()));
			
			final Tank humanTank3 = new Tank(new Coord(2,2), human);
			unitList.add(humanTank3);
			assertEquals(humanTank3, unitList.getNearestUnit(tank, game.getMapMgr()));
			
			final Fighter humanFighter1 = new Fighter(new Coord(5,4), human);
			unitList.add(humanFighter1);
			assertEquals(humanFighter1, unitList.getNearestUnit(tank, game.getMapMgr()));
			
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getTotalLoadCost_valid()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			final Tank tank_1 = new Tank(new Coord(0, 1), COMPUTER_PLAYER_1);
			final Tank tank_2 = new Tank(new Coord(0, 1), COMPUTER_PLAYER_1);
			final Tank tank_3 = new Tank(new Coord(0, 1), COMPUTER_PLAYER_1);

			assertEquals(0, list.getTotalSpaceCost());

			list.add(tank_1);
			assertEquals(tank_1.getSpaceCost(), list.getTotalSpaceCost());

			list.add(tank_2);
			assertEquals(tank_1.getSpaceCost() + tank_2.getSpaceCost(), list.getTotalSpaceCost());

			list.add(tank_3);
			assertEquals(	tank_1.getSpaceCost() + tank_2.getSpaceCost() + tank_3.getSpaceCost(),
							list.getTotalSpaceCost());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getTotalLoadCost_invalidList()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			final Tank tank_1 = new Tank(new Coord(0, 1), COMPUTER_PLAYER_1);
			final Destroyer destroyer_2 = new Destroyer(new Coord(0, 1), COMPUTER_PLAYER_1);

			assertEquals(0, list.getTotalSpaceCost());

			list.add(tank_1);
			list.add(destroyer_2);
			try
			{
				list.getTotalSpaceCost();
				fail("Invalid operation has been executed");
			} catch (IllegalStateException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getUnitAt_valid()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			final Tank tank_1 = new Tank(new Coord(0, 1), COMPUTER_PLAYER_1);
			final Tank tank_2 = new Tank(new Coord(0, 1), COMPUTER_PLAYER_1);
			final Tank tank_3 = new Tank(new Coord(0, 2), COMPUTER_PLAYER_1);

			assertEquals(0, list.getUnitAt(new Coord(1, 1)).size());

			list.add(tank_1);
			assertEquals(1, list.getUnitAt(new Coord(0, 1)).size());

			list.add(tank_2);
			assertEquals(2, list.getUnitAt(new Coord(0, 1)).size());

			list.add(tank_3);
			assertEquals(2, list.getUnitAt(new Coord(0, 1)).size());
			assertEquals(1, list.getUnitAt(new Coord(0, 2)).size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getUnitAt_nullParam()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			try
			{
				list.getUnitAt(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getFirstUnitAt_valid()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			final Tank tank_1 = new Tank(new Coord(0, 1), COMPUTER_PLAYER_1);
			final Tank tank_2 = new Tank(new Coord(0, 1), COMPUTER_PLAYER_1);
			final Tank tank_3 = new Tank(new Coord(0, 2), COMPUTER_PLAYER_1);

			assertEquals(null, list.getFirstUnitAt(new Coord(1, 1)));

			list.add(tank_1);
			assertEquals(tank_1, list.getFirstUnitAt(new Coord(0, 1)));
			assertEquals(null, list.getFirstUnitAt(new Coord(0, 2)));

			list.add(tank_2);
			assertEquals(tank_1, list.getFirstUnitAt(new Coord(0, 1)));
			assertEquals(null, list.getFirstUnitAt(new Coord(0, 2)));

			list.add(tank_3);
			assertEquals(tank_1, list.getFirstUnitAt(new Coord(0, 1)));
			assertEquals(tank_3, list.getFirstUnitAt(new Coord(0, 2)));
			assertEquals(null, list.getFirstUnitAt(new Coord(0, 3)));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getFirstUnitAt_nullParam()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			try
			{
				list.getFirstUnitAt(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_contains_Ground_air_sea_Unit_valid()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			assertFalse(list.containsGroundUnit());
			assertFalse(list.containsAirUnit());
			assertFalse(list.containsSeaUnit());

			final Coord position = new Coord(1, 1);
			final Fighter fighter = new Fighter(position, COMPUTER_PLAYER_1);
			final Tank tank = new Tank(position, COMPUTER_PLAYER_1);
			final TransportShip transport = new TransportShip(position, COMPUTER_PLAYER_1);

			list.add(fighter);
			assertFalse(list.containsGroundUnit());
			assertTrue(list.containsAirUnit());
			assertFalse(list.containsSeaUnit());

			list.clear();
			list.add(tank);
			assertTrue(list.containsGroundUnit());
			assertFalse(list.containsAirUnit());
			assertFalse(list.containsSeaUnit());

			list.clear();
			list.add(transport);
			assertFalse(list.containsGroundUnit());
			assertFalse(list.containsAirUnit());
			assertTrue(list.containsSeaUnit());

			list.clear();
			list.add(transport);
			list.add(tank);
			list.add(fighter);
			assertTrue(list.containsGroundUnit());
			assertTrue(list.containsAirUnit());
			assertTrue(list.containsSeaUnit());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_hasEnemyInRange_coord_valid()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			assertFalse(list.hasUnitInRange(new Coord(1, 1), 1));

			final Fighter fighter = new Fighter(new Coord(1, 1), COMPUTER_PLAYER_1);
			final Tank tank = new Tank(new Coord(2, 2), COMPUTER_PLAYER_1);

			list.add(fighter);
			assertFalse(list.hasUnitInRange(new Coord(0, 0), 0));
			assertTrue(list.hasUnitInRange(new Coord(1, 1), 0));
			assertTrue(list.hasUnitInRange(new Coord(0, 0), 1));
			assertTrue(list.hasUnitInRange(new Coord(1, 1), 1));

			list.clear();
			list.add(fighter);
			list.add(tank);
			assertFalse(list.hasUnitInRange(new Coord(0, 0), 0));
			assertTrue(list.hasUnitInRange(new Coord(1, 1), 0));
			assertTrue(list.hasUnitInRange(new Coord(0, 0), 1));
			assertTrue(list.hasUnitInRange(new Coord(1, 1), 1));
			assertTrue(list.hasUnitInRange(new Coord(3, 3), 2));
			assertTrue(list.hasUnitInRange(new Coord(4, 4), 2));
			assertFalse(list.hasUnitInRange(new Coord(5, 5), 2));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_hasUnitInRange_nullParam()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			try
			{
				list.hasUnitInRange(null, 1);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_hasUnitInRange_invalidRange()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			try
			{
				list.hasUnitInRange(new Coord(1, 1), -1);
				fail("Invalid range has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getEnemyMinimumRange_valid()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			final Fighter fighter = new Fighter(new Coord(1, 1), COMPUTER_PLAYER_1);
			final Tank tank = new Tank(new Coord(1, 5), COMPUTER_PLAYER_1);

			assertEquals(Integer.MAX_VALUE, list.getMinimumRange(new Coord(1, 1), 0));
			assertEquals(Integer.MAX_VALUE, list.getMinimumRange(new Coord(1, 1), 1));
			list.add(fighter);
			assertEquals(0, list.getMinimumRange(new Coord(1, 1), 0));
			assertEquals(1, list.getMinimumRange(new Coord(1, 1), 1));
			assertEquals(2, list.getMinimumRange(new Coord(1, 1), 2));
			assertEquals(1, list.getMinimumRange(new Coord(1, 2), 1));

			list.add(tank);
			assertEquals(1, list.getMinimumRange(new Coord(1, 6), 0));
			assertEquals(2, list.getMinimumRange(new Coord(1, 6), 2));
			assertEquals(10, list.getMinimumRange(new Coord(1, 6), 10));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getMinimumRange_nullParam()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			try
			{
				list.getMinimumRange(null, 1);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getMinimumRange_InvalidParam()
	{
		try
		{
			final AbstractUnitList list = new AbstractUnitList();

			try
			{
				list.getMinimumRange(new Coord(1, 1), -1);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getNonMovingFighter_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			final AbstractUnitList list = new AbstractUnitList();
			final HumanPlayer human = (HumanPlayer) game.getPlayerMgr().getPlayerList().get(0);

			final Fighter fighter_1 = new Fighter(new Coord(0, 1), human);
			final Fighter fighter_2 = new Fighter(new Coord(0, 1), human);
			final Fighter fighter_3 = new Fighter(new Coord(0, 2), human);
			final Fighter fighter_4 = new Fighter(new Coord(0, 2), human);
			final Fighter fighter_5 = new Fighter(new Coord(0, 0), human);

			assertEquals(null, list.getFirstUnitAt(new Coord(1, 1)));

			assertEquals(0, list.getNonMovingFighter().size());
			list.add(fighter_1);
			assertEquals(1, list.getNonMovingFighter().size());

			fighter_1.setPath(new Path(new Coord(1, 1)));
			fighter_1.move();
			assertEquals(0, list.getNonMovingFighter().size());

			list.add(fighter_2);
			assertEquals(1, list.getNonMovingFighter().size());

			list.add(fighter_3);
			list.add(fighter_4);
			list.add(fighter_5);
			fighter_5.setFuel(0);
			fighter_5.setMove(fighter_5.getMovementCapacity() - 1);
			assertEquals(4, list.getNonMovingFighter().size());
			assertTrue(list.getNonMovingFighter().contains(fighter_2));
			assertTrue(list.getNonMovingFighter().contains(fighter_3));
			assertTrue(list.getNonMovingFighter().contains(fighter_4));
			assertTrue(list.getNonMovingFighter().contains(fighter_5));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_retrieveAccessibleUnitBySea_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			final AbstractUnitList list = new AbstractUnitList();
			final HumanPlayer human = (HumanPlayer) game.getPlayerMgr().getPlayerList().get(0);

			final Fighter fighter_1 = new Fighter(new Coord(0, 0), human);
			final Fighter fighter_2 = new Fighter(new Coord(1, 1), human);
			final Fighter fighter_3 = new Fighter(new Coord(2, 2), human);
			final Fighter fighter_4 = new Fighter(new Coord(3, 2), human);

			try
			{
				list.retrieveAccessibleUnitBySea(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			AbstractUnitList currentList = list.retrieveAccessibleUnitBySea(map);
			assertTrue(currentList.isEmpty());

			list.add(fighter_1);
			currentList = list.retrieveAccessibleUnitBySea(map);
			assertEquals(1, currentList.size());
			assertTrue(currentList.contains(fighter_1));

			list.add(fighter_2);
			currentList = list.retrieveAccessibleUnitBySea(map);
			assertEquals(2, currentList.size());
			assertTrue(currentList.contains(fighter_1));
			assertTrue(currentList.contains(fighter_2));

			list.add(fighter_3);
			currentList = list.retrieveAccessibleUnitBySea(map);
			assertEquals(2, currentList.size());
			assertTrue(currentList.contains(fighter_1));
			assertTrue(currentList.contains(fighter_2));

			list.add(fighter_4);
			currentList = list.retrieveAccessibleUnitBySea(map);
			assertEquals(2, currentList.size());
			assertTrue(currentList.contains(fighter_1));
			assertTrue(currentList.contains(fighter_2));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getNonFlyingUnits_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			final PlayerMgr playerMgr = game.getPlayerMgr();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			assertEquals(0, human.getUnitList().size());

			final Tank cityTank = new Tank(new Coord(1, 1), human);
			human.addUnit(cityTank, false);
			assertEquals(1, human.getUnitList().size());
			assertEquals(1, human.getUnitList().getNonFlyingUnitList(human.getCitySet()).size());

			final Tank groundTank = new Tank(new Coord(2, 1), human);
			human.addUnit(groundTank, false);
			assertEquals(2, human.getUnitList().size());
			assertEquals(2, human.getUnitList().getNonFlyingUnitList(human.getCitySet()).size());

			final Fighter flyingFighter1 = new Fighter(new Coord(2, 1), human);
			human.addUnit(flyingFighter1, false);
			assertEquals(3, human.getUnitList().size());
			assertEquals(2, human.getUnitList().getNonFlyingUnitList(human.getCitySet()).size());

			final Fighter flyingFighter2 = new Fighter(new Coord(1, 1), human);
			human.addUnit(flyingFighter2, false);
			flyingFighter2.setPath(new Path(new Coord(2, 1)));
			assertEquals(4, human.getUnitList().size());
			assertEquals(2, human.getUnitList().getNonFlyingUnitList(human.getCitySet()).size());

			final Fighter nonFlyingFighter = new Fighter(new Coord(1, 1), human);
			human.addUnit(nonFlyingFighter, false);
			assertEquals(5, human.getUnitList().size());
			assertEquals(3, human.getUnitList().getNonFlyingUnitList(human.getCitySet()).size());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}
