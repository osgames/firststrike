package org.jocelyn_chaumel.firststrike.core.directive.fighter;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class FighterAutoDirectiveTest extends MapTestCase
{

	public FighterAutoDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Fighter computerFighter1 = new Fighter(new Coord(7, 7), computer);
			computer.addUnit(computerFighter1, true);
			try
			{
				new FighterAutoDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Return to city
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new FighterAutoDirective(computerFighter1).play());
			assertEquals(new Coord(10, 10), computerFighter1.getPosition());

			// Kamikaze computer unit killed (during return to city)
			final Fighter humanFighter1 = new Fighter(new Coord(8, 8), human);
			human.addUnit(humanFighter1, true);

			final Fighter computerFighter2 = new Fighter(new Coord(6, 6), computer);
			computerFighter2.setHitPoints(1);
			computer.addUnit(computerFighter2, true);
			while (computerFighter2.getFuel() > 30)
			{
				computerFighter2.decreaseFuel();
			}
			computer.startTurn(1);

			FSRandom.getInstance(0.1f);
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(MoveStatusCst.UNIT_DEFEATED, new FighterAutoDirective(computerFighter2).play());
			assertFalse(computerFighter2.isLiving());
			assertTrue(humanFighter1.isLiving());

			// Kamikaze human unit killed
			final Fighter computerFighter3 = new Fighter(new Coord(6, 6), computer);
			computer.addUnit(computerFighter3, true);
			while (computerFighter3.getFuel() > 30)
			{
				computerFighter3.decreaseFuel();
			}
			FSRandom.getInstance(0.5f);
			assertEquals(1, computer.getEnemySet().size());

			assertEquals(1, computer.getEnemySet().size());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new FighterAutoDirective(computerFighter3).play());
			assertFalse(humanFighter1.isLiving());
			assertEquals(new Coord(7, 7), computerFighter3.getPosition());
			assertEquals(0, computer.getEnemySet().size());

			// Patrol
			FSRandom.getInstance(0.01f);
			final Fighter computerFighter4 = new Fighter(new Coord(10, 10), computer);
			computer.addUnit(computerFighter4, true);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new FighterAutoDirective(computerFighter4).play());
			assertEquals(new Coord(4, 4), computerFighter4.getPosition());
			assertEquals(0, computerFighter4.getMove());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
