package org.jocelyn_chaumel.firststrike.core.directive.fighter;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

public class FighterAttackDirectiveTest extends MapTestCase
{

	public FighterAttackDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Fighter computerFighter1 = new Fighter(new Coord(9, 9), computer);
			final CityList cityList = computer.getCitySet();
			final CoordSet coordSet = cityList.getCoordSet().getExtendedCoordSet(8);
			computer.addUnit(computerFighter1, true);
			try
			{
				new FighterAttackDirective(null, cityList, coordSet);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}
			try
			{
				new FighterAttackDirective(computerFighter1, null, coordSet);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new FighterAttackDirective(computerFighter1, cityList, null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// No ennemy and return to city
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new FighterAttackDirective(computerFighter1, cityList).play());
			assertEquals(new Coord(10, 10), computerFighter1.getPosition());
			assertEquals(computerFighter1.getFuelCapacity(), computerFighter1.getFuel());

			// No move point
			final Fighter computerFighter2 = new Fighter(new Coord(9, 9), computer);
			computer.addUnit(computerFighter2, true);
			computerFighter2.setMove(0);
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new FighterAttackDirective(computerFighter2, cityList).play());
			assertEquals(new Coord(9, 9), computerFighter2.getPosition());

			// Attack Enemy & return to city
			final Fighter humanFighter1 = new Fighter(new Coord(7, 7), human);
			humanFighter1.setHitPoints(1);
			human.addUnit(humanFighter1, true);
			computer.startTurn(1);
			assertEquals(1, computer.getEnemySet().size());

			FSRandom.getInstance(0.5f);
			computerFighter2.restoreMove();
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new FighterAttackDirective(computerFighter2, cityList).play());
			assertEquals(new Coord(10, 10), computerFighter2.getPosition());
			assertEquals(0, computer.getEnemySet().size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}
