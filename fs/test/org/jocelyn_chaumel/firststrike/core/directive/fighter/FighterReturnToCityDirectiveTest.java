package org.jocelyn_chaumel.firststrike.core.directive.fighter;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class FighterReturnToCityDirectiveTest extends MapTestCase
{

	public FighterReturnToCityDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "4", "4" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			try
			{
				new FighterReturnToCityDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Already into a city
			final Fighter computerFighter1 = new Fighter(new Coord(4, 4), computer);
			computer.addUnit(computerFighter1, true);
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new FighterReturnToCityDirective(computerFighter1).play());
			assertEquals(new Coord(4, 4), computerFighter1.getPosition());
			assertEquals(computerFighter1.getFuelCapacity(), computerFighter1.getFuel());

			// No move point
			final Fighter computerFighter2 = new Fighter(new Coord(2, 2), computer);
			computer.addUnit(computerFighter2, true);
			computerFighter2.setMove(0);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new FighterReturnToCityDirective(computerFighter2).play());
			assertEquals(new Coord(2, 2), computerFighter2.getPosition());

			// Not Enough move point (no move)
			computerFighter2.setMove(MoveCostFactory.AIR_MOVE_COST.getMinCost());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new FighterReturnToCityDirective(computerFighter2).play());
			assertEquals(new Coord(3, 3), computerFighter2.getPosition());

			// Move Successfully (end of city protection cause enough tank to do
			// it)
			computerFighter2.setMove(MoveCostFactory.AIR_MOVE_COST.getMinCost());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new FighterReturnToCityDirective(computerFighter2).play());
			assertEquals(new Coord(4, 4), computerFighter2.getPosition());

			// Not Enough Fuel (play Kamikaze)
			final Fighter humanFighter1 = new Fighter(new Coord(1, 1), human);
			humanFighter1.setHitPoints(1);
			human.addUnit(humanFighter1, true);

			final Fighter computerFighter3 = new Fighter(new Coord(1, 2), computer);
			computer.addUnit(computerFighter3, true);
			computerFighter3.setMove(MoveCostFactory.AIR_MOVE_COST.getMinCost() * 2);
			while (computerFighter3.getFuel() != 20)
			{
				computerFighter3.decreaseFuel();
			}
			computer.startTurn(1);
			FSRandom.getInstance(0.5f);
			assertEquals(1, computer.getEnemySet().size());

			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new FighterReturnToCityDirective(computerFighter3).play());
			assertEquals(new Coord(1, 2), computerFighter3.getPosition());
			assertEquals(0, computer.getEnemySet().size());

			final Fighter computerFighter4 = new Fighter(new Coord(0, 4), computer);
			computer.addUnit(computerFighter4, true);
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new FighterReturnToCityDirective(computerFighter4).play());
			assertEquals(new Coord(4, 4), computerFighter4.getPosition());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
