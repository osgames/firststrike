package org.jocelyn_chaumel.firststrike.core.directive.fighter;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class FighterKamikazeDirectiveTest extends MapTestCase
{

	public FighterKamikazeDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Fighter computerFighter1 = new Fighter(new Coord(11, 11), computer);
			computer.addUnit(computerFighter1, true);
			try
			{
				new FighterKamikazeDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// No ennemy then do nothing
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new FighterKamikazeDirective(computerFighter1).play());
			assertEquals(new Coord(11, 11), computerFighter1.getPosition());
			assertEquals(computerFighter1.getFuelCapacity(), computerFighter1.getFuel());

			// Attack Enemy & return to city
			FSRandom.getInstance(0.5f);
			final Fighter humanFighter1 = new Fighter(new Coord(9, 9), human);
			humanFighter1.setHitPoints(1);
			human.addUnit(humanFighter1, true);
			computer.startTurn(1);
			assertEquals(1, computer.getEnemySet().size());

			final Fighter computerFighter2 = new Fighter(new Coord(5, 5), computer);
			computer.addUnit(computerFighter2, true);

			while (2 < computerFighter2.getFuel())
			{
				computerFighter2.decreaseFuel();
			}
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new FighterKamikazeDirective(computerFighter2).play());
			assertEquals(new Coord(5, 5), computerFighter2.getPosition());

			assertEquals(1, computer.getEnemySet().size());

			computerFighter2.restoreFuel();
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new FighterKamikazeDirective(computerFighter2).play());
			assertEquals(new Coord(8, 8), computerFighter2.getPosition());
			assertEquals(0, computer.getEnemySet().size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}
