package org.jocelyn_chaumel.firststrike.core.directive.fighter;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class FighterPatrolDirectiveTest extends MapTestCase
{

	public FighterPatrolDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final CityList cityList = computer.getCitySet();

			final Fighter computerFighter1 = new Fighter(new Coord(11, 11), computer);
			computer.addUnit(computerFighter1, true);
			try
			{
				new FighterPatrolDirective(null, cityList);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}
			try
			{
				new FighterPatrolDirective(computerFighter1, null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}
			try
			{
				new FighterPatrolDirective(computerFighter1, new CityList());
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Return to city
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new FighterPatrolDirective(computerFighter1, cityList).play());
			assertEquals(new Coord(10, 10), computerFighter1.getPosition());

			// Patrol without enemy
			FSRandom.getInstance(0.5f);
			computerFighter1.restoreMove();
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new FighterPatrolDirective(computerFighter1, cityList).play());
			assertEquals(new Coord(10, 10), computerFighter1.getPosition());
			assertEquals(80, computerFighter1.getMove());

			// Patrol with an enemy
			FSRandom.getInstance(0.1f);
			final Fighter humanFighter1 = new Fighter(new Coord(6, 5), human);
			humanFighter1.setHitPoints(1);
			human.addUnit(humanFighter1, true);
			computer.startTurn(1);
			assertEquals(0, computer.getEnemySet().size());

			computerFighter1.restoreMove();
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new FighterPatrolDirective(computerFighter1, cityList).play());
			assertEquals(new Coord(10, 10), computerFighter1.getPosition());

			assertEquals(0, computer.getEnemySet().size());
			assertFalse(humanFighter1.isLiving());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
