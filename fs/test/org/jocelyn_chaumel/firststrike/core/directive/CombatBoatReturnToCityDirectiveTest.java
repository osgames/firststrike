package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class CombatBoatReturnToCityDirectiveTest extends MapTestCase
{

	public CombatBoatReturnToCityDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			assertFalse(ApplicationConfigurationMgr.getInstance().isDebugMode());

			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);

			final Destroyer computerDestroyer1 = new Destroyer(new Coord(11, 5), computer);
			computer.addUnit(computerDestroyer1, true);

			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new CombatBoatReturnToCityDirective(computerDestroyer1).play());
			assertEquals(new Coord(11, 9), computerDestroyer1.getPosition());

			computerDestroyer1.restoreMove();
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new CombatBoatReturnToCityDirective(computerDestroyer1).play());
			assertEquals(new Coord(10, 10), computerDestroyer1.getPosition());

			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new CombatBoatReturnToCityDirective(computerDestroyer1).play());
			assertEquals(new Coord(10, 10), computerDestroyer1.getPosition());

			computerDestroyer1.setPath(new Path(new Coord(11, 10)));
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new MoveDirective(computerDestroyer1).play());

			// DestroyerKamikaze (no more city with a port is available
			final Tank humanTank1 = new Tank(new Coord(9, 9), human);
			human.addUnit(humanTank1, true);
			humanTank1.setPath(new Path(new Coord[] { new Coord(10, 10), new Coord(10, 9) }));
			assertEquals(MoveStatusCst.ENEMY_DETECTED, new MoveDirective(humanTank1).play());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new FindPathAndMoveWithoutCombatDirective(	humanTank1,
																									new Coord(10, 9),
																									false,
																									false).play());

			computer.startTurn(1);
			assertEquals(1, computer.getEnemySet().size());
			FSRandom.getInstance(0.8f);
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new CombatBoatReturnToCityDirective(computerDestroyer1).play());
			assertEquals(new Coord(8, 11), computerDestroyer1.getPosition());
			assertEquals(0, computer.getEnemySet().size());

		} catch (Exception ex)
		{
			fail(ex);
			System.err.println("JOSS -------------------------------------------------");
		}
	}
}
