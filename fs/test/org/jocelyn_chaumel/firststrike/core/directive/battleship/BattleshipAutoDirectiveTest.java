package org.jocelyn_chaumel.firststrike.core.directive.battleship;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class BattleshipAutoDirectiveTest extends MapTestCase
{

	public BattleshipAutoDirectiveTest(final String p_name)
	{
		super(p_name);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);

			final Battleship computerBattleship1 = new Battleship(new Coord(11, 8), computer);
			computer.addUnit(computerBattleship1, true);

			try
			{
				new BattleshipAutoDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			// Return to City
			computerBattleship1.setHitPoints(50);

			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new BattleshipAutoDirective(computerBattleship1).play());
			assertEquals(new Coord(10, 10), computerBattleship1.getPosition());

			// Attack
			computerBattleship1.restoreMove();
			final Fighter humanFighter1 = new Fighter(new Coord(9, 10), human);
			human.addUnit(humanFighter1, true);
			humanFighter1.setHitPoints(10);

			assertEquals(0, computer.getEnemySet().size());
			computer.startTurn(1);
			assertEquals(1, computer.getEnemySet().size());

			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new BattleshipAutoDirective(computerBattleship1).play());
			assertEquals(0, computer.getEnemySet().size());

			// Bombardment
			final Tank humanTank1 = new Tank(new Coord(8, 8), human);
			human.addUnit(humanTank1, true);
			humanTank1.setHitPoints(10);

			assertEquals(0, computer.getEnemySet().size());
			computer.startTurn(1);
			assertEquals(1, computer.getEnemySet().size());
			computer.refreshBombardmentCoord();
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new BattleshipAutoDirective(computerBattleship1).play());
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(0, computerBattleship1.getNbBombardment());
			assertEquals(true, computer.isBombardmentPlanned(humanTank1.getPosition()));
			human.killUnit(humanTank1);

			// Patrol
			assertEquals(0, computer.getEnemySet().size());
			computerBattleship1.restoreUnit();
			computer.refreshBombardmentCoord();
			final Coord startCoord = computerBattleship1.getPosition();
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new BattleshipAutoDirective(computerBattleship1).play());
			assertFalse(startCoord.equals(computerBattleship1.getPosition()));
			assertEquals(0, computerBattleship1.getMove());
			assertEquals(computerBattleship1.getNbBombardmentCapacity(), computerBattleship1.getNbBombardment());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
