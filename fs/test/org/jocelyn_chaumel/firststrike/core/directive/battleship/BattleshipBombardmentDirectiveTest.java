package org.jocelyn_chaumel.firststrike.core.directive.battleship;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class BattleshipBombardmentDirectiveTest extends MapTestCase
{

	public BattleshipBombardmentDirectiveTest(final String p_name)
	{
		super(p_name);
	}

	public void test_BattleshipBombardmentDirective_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);

			final Battleship computerBattleship1 = new Battleship(new Coord(2, 0), computer);
			computer.addUnit(computerBattleship1, true);

			// Null parameter tests
			try
			{
				new BattleshipBombardmentDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			// No enemy - empty list
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new BattleshipBombardmentDirective(computerBattleship1).play());

			// No enemy - list with invalid target
			assertEquals(0, computer.getEnemySet().size());
			final Fighter humanFighter1 = new Fighter(new Coord(8, 8), human);
			human.addUnit(humanFighter1, false);

			computer.startTurn(0);
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new BattleshipBombardmentDirective(computerBattleship1).play());
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(1, computerBattleship1.getNbBombardment());
			assertEquals(computerBattleship1.getMovementCapacity(), computerBattleship1.getMove());

			// With one Enemy
			FSRandom.getInstance(0.1f);
			final Tank humanTank1 = new Tank(new Coord(8, 8), human);
			human.addUnit(humanTank1, false);

			computer.startTurn(0);
			assertEquals(2, computer.getEnemySet().size());
			computer.refreshBombardmentCoord();
			assertEquals(false, computer.isBombardmentPlanned(humanTank1.getPosition()));
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new BattleshipBombardmentDirective(computerBattleship1).play());
			assertEquals(2, computer.getEnemySet().size());
			assertEquals(0, computerBattleship1.getNbBombardment());
			assertEquals(0, computerBattleship1.getMove());
			assertEquals(true, computer.isBombardmentPlanned(humanTank1.getPosition()));
			assertEquals(humanTank1.getHitPointsCapacity(), humanTank1.getHitPoints());
			computer.startTurn(1);
			assertTrue(humanTank1.isLiving());
			assertEquals(false, computer.isBombardmentPlanned(humanTank1.getPosition()));
			assertTrue(humanTank1.getHitPointsCapacity() > humanTank1.getHitPoints());

			// With 2 enemies but Tank still is better

			final TransportShip humanTransport1 = new TransportShip(new Coord(11, 9), human);
			human.addUnit(humanTransport1, false);
			computer.startTurn(2);

			assertEquals(3, computer.getEnemySet().size());
			assertEquals(1, computerBattleship1.getNbBombardment());
			humanTank1.setMove(10);
			computer.refreshBombardmentCoord();
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new BattleshipBombardmentDirective(computerBattleship1).play());
			assertEquals(3, computer.getEnemySet().size());
			assertEquals(0, computerBattleship1.getNbBombardment());
			assertEquals(0, computerBattleship1.getMove());
			assertEquals(false, computer.isBombardmentPlanned(humanTank1.getPosition()));
			assertEquals(true, computer.isBombardmentPlanned(humanTransport1.getPosition()));
			computer.startTurn(2);

			// With 2 units on the same coord
			final Tank humanTank2 = new Tank(new Coord(8, 9), human);
			human.addUnit(humanTank2, false);

			final Tank humanTank3 = new Tank(new Coord(8, 9), human);
			human.addUnit(humanTank3, false);
			computer.startTurn(3);
			computer.refreshBombardmentCoord();

			assertEquals(5, computer.getEnemySet().size());
			assertEquals(1, computerBattleship1.getNbBombardment());
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new BattleshipBombardmentDirective(computerBattleship1).play());
			assertEquals(5, computer.getEnemySet().size());
			assertEquals(0, computerBattleship1.getNbBombardment());
			assertEquals(0, computerBattleship1.getMove());
			assertEquals(true, computer.isBombardmentPlanned(humanTank2.getPosition()));

			// With 4 enemies but loaded transportship is better
			final TransportShip humanTransport2 = new TransportShip(new Coord(11, 10), human);
			human.addUnit(humanTransport2, false);

			final Tank transportTank1 = new Tank(humanTransport2.getPosition(), human);
			final Tank transportTank2 = new Tank(humanTransport2.getPosition(), human);
			final Tank transportTank3 = new Tank(humanTransport2.getPosition(), human);
			transportTank1.loadInContainer(humanTransport2);
			transportTank2.loadInContainer(humanTransport2);
			transportTank3.loadInContainer(humanTransport2);

			computer.startTurn(3);
			computer.refreshBombardmentCoord();
			assertEquals(6, computer.getEnemySet().size());
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new BattleshipBombardmentDirective(computerBattleship1).play());
			assertEquals(6, computer.getEnemySet().size());
			assertEquals(0, computerBattleship1.getNbBombardment());
			assertEquals(0, computerBattleship1.getMove());
			assertEquals(true, computer.isBombardmentPlanned(humanTransport2.getPosition()));
			computer.startTurn(4);
			assertTrue(humanTransport2.getHitPointsCapacity() > humanTransport2.getHitPoints());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
