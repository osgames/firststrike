package org.jocelyn_chaumel.firststrike.core.directive.artillery;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.RadarStation;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class ArtilleryDefendBombardmentDirectiveTest extends MapTestCase
{

	public ArtilleryDefendBombardmentDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2),
					new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Artillery computerArtillery = new Artillery(new Coord(9, 9), computer);
			computer.addUnit(computerArtillery, true);

			try
			{
				new ArtilleryDefendBombardmentDirective(null, new Coord(1, 1));
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new ArtilleryDefendBombardmentDirective(computerArtillery, null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Return to city
			computerArtillery.setHitPoints(computerArtillery.getHitPointsCapacity() / 3);
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new ArtilleryDefendBombardmentDirective(computerArtillery, new Coord(9, 9)).play());
			assertEquals(new Coord(10, 10), computerArtillery.getPosition());

			// Go back to Defending position
			computerArtillery.restoreUnit();
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new ArtilleryDefendBombardmentDirective(computerArtillery, new Coord(9, 9)).play());
			assertEquals(50, computerArtillery.getMove());
			assertEquals(new Coord(9, 9), computerArtillery.getPosition());

			// Nothing to do
			computerArtillery.restoreUnit();
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new ArtilleryDefendBombardmentDirective(computerArtillery, new Coord(9, 9)).play());
			assertEquals(computerArtillery.getMovementCapacity(), computerArtillery.getMove());
			assertEquals(new Coord(9, 9), computerArtillery.getPosition());

			// Enemy to bombard
			final Tank humanTank1 = new Tank(new Coord(1, 2), human);
			human.addUnit(humanTank1, true);
			computerArtillery.addDirective(new ArtilleryDefendBombardmentDirective(computerArtillery, new Coord(9, 9)));
			final RadarStation rs = new RadarStation(new Coord(9, 9), computer);
			computer.addUnit(rs, false);
			computer.startTurn(2);
			assertEquals(1, computer.getEnemySet().size());

			FSRandom.getInstance(0.5f);
			assertEquals(false, computer.isBombardmentPlanned(new Coord(1, 2)));
			computer.play();
			assertEquals(0, computerArtillery.getMove());
			assertEquals(true, computer.isBombardmentPlanned(new Coord(1, 2)));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
