package org.jocelyn_chaumel.firststrike.core.directive.artillery;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

public class ArtilleryAutoDirectiveTest extends MapTestCase
{

	public ArtilleryAutoDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Artillery computerArtillery = new Artillery(new Coord(9, 9), computer);
			computer.addUnit(computerArtillery, true);

			try
			{
				new ArtilleryAutoDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}
			map.getCityList().getCityAtPosition(new Coord(1,1)).setProductionType(UnitTypeCst.TANK);

			// Return to city
			computerArtillery.setHitPoints(computerArtillery.getHitPointsCapacity() / 3);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new ArtilleryAutoDirective(computerArtillery).play());
			assertEquals(new Coord(10, 10), computerArtillery.getPosition());
			
			//Bombardment - nothing to do
			
			computerArtillery.restoreUnit();
			computer.refreshBombardmentCoord();
			assertEquals (false, computer.isBombardmentPlanned(new Coord (1,1)));
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new ArtilleryAutoDirective(computerArtillery).play());
			assertEquals(0, computerArtillery.getMove());
			assertEquals (true, computer.isBombardmentPlanned(new Coord (1,1)));

			
			//Bombardment - with enemy without path
			computerArtillery.restoreUnit();
			
			final Tank humanTank1 = new Tank(new Coord(1,2), human);
			human.addUnit(humanTank1, true);
			computer.startTurn(2);
			final Fighter computerFighter2 = new Fighter(new Coord(3,3), computer);
			computer.addUnit(computerFighter2, true);
			computerFighter2.setPath(mapMgr.getPath(new Coord(2,2), new CoordSet(), computerFighter2, false));
			computerFighter2.move();
			assertEquals(new Coord(2,2), computerFighter2.getPosition());
			assertEquals(1, computer.getEnemySet().size());

			FSRandom.getInstance(0.5f);
			assertEquals(false, computer.isBombardmentPlanned(new Coord(4,3)));
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new ArtilleryAutoDirective(computerArtillery).play());
			assertEquals(0, computerArtillery.getMove());

			
			//Bombardment - with enemy with path
			computerArtillery.restoreUnit();
			
			final Tank humanTank = new Tank(new Coord(1,1), human);
			human.addUnit(humanTank, true);
			computer.startTurn(2);
			humanTank.setPath(mapMgr.getPath(new Coord(4,4), new CoordSet(), humanTank, false));
			humanTank.getPath().calculateTurnCost(map, humanTank.getMoveCost(), humanTank.getMove(), humanTank.getMovementCapacity());
			final Fighter computerFighter = new Fighter(new Coord(3,3), computer);
			computer.addUnit(computerFighter, true);
			computerFighter.setPath(mapMgr.getPath(new Coord(2,2), new CoordSet(), computerFighter, false));
			computerFighter.move();
			assertEquals(new Coord(2,2), computerFighter.getPosition());
			assertEquals(2, computer.getEnemySet().size());

			assertEquals(false, computer.isBombardmentPlanned(new Coord(4,4)));
			computer.refreshBombardmentCoord();
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new ArtilleryAutoDirective(computerArtillery).play());
			assertEquals(0, computerArtillery.getMove());
			assertEquals(true, computer.isBombardmentPlanned(new Coord(4,4)));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
