package org.jocelyn_chaumel.firststrike.core.directive.destroyer;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class DestroyerProtectTransportDirectiveTest extends MapTestCase
{

	public DestroyerProtectTransportDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);

			final Destroyer computerDestroyer1 = new Destroyer(new Coord(11, 8), computer);
			computer.addUnit(computerDestroyer1, true);

			try
			{
				new DestroyerProtectTransportDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new DestroyerProtectTransportDirective(computerDestroyer1).play();
				fail();
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			// Return to City
			final TransportShip computerTranport1 = new TransportShip(new Coord(0, 11), computer);
			computer.addUnit(computerTranport1, true);
			computerDestroyer1.setProtectedTransport(computerTranport1);
			computerDestroyer1.setHitPoints(50);

			assertTrue(computerDestroyer1.isProtectingTransport());
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new DestroyerProtectTransportDirective(computerDestroyer1).play());
			assertEquals(new Coord(10, 10), computerDestroyer1.getPosition());
			assertFalse(computerDestroyer1.isProtectingTransport());

			// Patrol around Transport Ship without path
			FSRandom.getInstance(0.01f);
			computerDestroyer1.restoreHitPoints();
			computerDestroyer1.restoreMove();
			computerDestroyer1.setProtectedTransport(computerTranport1);
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new DestroyerProtectTransportDirective(computerDestroyer1).play());
			assertEquals(new Coord(6, 10), computerDestroyer1.getPosition());
			assertEquals(new Coord(0, 6), computerDestroyer1.getPath().getDestination());
			assertTrue(computerDestroyer1.isProtectingTransport());

			// Destroy an enemy and start patrol.
			computerDestroyer1.removeProtectedTransport();
			final Destroyer computerDestroyer2 = new Destroyer(new Coord(10, 10), computer);
			computer.addUnit(computerDestroyer2, true);
			final Fighter humanFighter1 = new Fighter(new Coord(11, 11), human);
			human.addUnit(humanFighter1, true);
			humanFighter1.setHitPoints(1);
			computer.startTurn(1);
			computerDestroyer2.setProtectedTransport(computerTranport1);

			assertTrue(computerDestroyer2.isProtectingTransport());
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new DestroyerProtectTransportDirective(computerDestroyer2).play());
			assertEquals(0, computer.getEnemySet().size());
			assertEquals(new Coord(7, 11), computerDestroyer2.getPosition());
			assertEquals(new Coord(0, 6), computerDestroyer2.getPath().getDestination());
			assertTrue(computerDestroyer2.isProtectingTransport());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
