package org.jocelyn_chaumel.firststrike.core.directive.destroyer;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

public class DestroyerAutoDirectiveTest extends MapTestCase
{

	public DestroyerAutoDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);

			final Destroyer computerDestroyer1 = new Destroyer(new Coord(11, 8), computer);
			computer.addUnit(computerDestroyer1, true);

			try
			{
				new DestroyerAutoDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			// Return to City
			final TransportShip computerTransport1 = new TransportShip(new Coord(11, 9), computer);
			computer.addUnit(computerTransport1, true);
			computerDestroyer1.setProtectedTransport(computerTransport1);
			computerDestroyer1.setHitPoints(50);

			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new DestroyerAutoDirective(computerDestroyer1).play());
			assertEquals(new Coord(10, 10), computerDestroyer1.getPosition());

			// Patrol around Transport Ship without path
			FSRandom.getInstance(0.5f);
			computerDestroyer1.restoreHitPoints();
			computerDestroyer1.restoreMove();
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new DestroyerAutoDirective(computerDestroyer1).play());
			assertEquals(new Coord(6, 10), computerDestroyer1.getPosition());
			assertEquals(new Coord(6, 7), computerDestroyer1.getPath().getDestination());

			computerDestroyer1.restoreMove();
			final Path path = mapMgr.getPathMgr().getAreaPath(	computerTransport1.getPosition(),
																new Coord(6, 6),
																computerTransport1.getMoveCost(),
																computerTransport1.getMove(),
																computerTransport1.getMovementCapacity(),
																new CoordSet(),
																false);
			computerTransport1.setPath(path);

			// Patrol around Transport with path
			FSRandom.getInstance(0.25f);
			computerDestroyer1.restoreMove();
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new DestroyerProtectTransportDirective(computerDestroyer1).play());
			assertEquals(new Coord(7, 6), computerDestroyer1.getPosition());
			assertEquals(new Coord(11, 4), computerDestroyer1.getPath().getDestination());

			// Patrol but enemy detected and destroyed
			computerDestroyer1.restoreMove();
			final Fighter humanFighter1 = new Fighter(new Coord(9, 5), human);
			human.addUnit(humanFighter1, true);
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new DestroyerProtectTransportDirective(computerDestroyer1).play());
			assertEquals(new Coord(10, 4), computerDestroyer1.getPosition());
			assertFalse(humanFighter1.isLiving());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
