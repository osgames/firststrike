package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TankProtectPositionDirectiveTest extends MapTestCase
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TankProtectPositionDirectiveTest.class);

	public TankProtectPositionDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public final void test_play_all()
	{
		try
		{

			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "7", "7" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final ComputerTank computerTank1 = new ComputerTank(new Coord(7, 7), computer);

			try
			{
				new TankProtectPositionDirective(null, new Coord(1, 1));
				fail("Null param accepted");
			} catch (IllegalArgumentException ex)
			{
				// nop
			}
			try
			{
				new TankProtectPositionDirective(computerTank1, null);
				fail("Null param accepted");
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			computer.addUnit(computerTank1, true);

			_logger.system("DEPLACEMENT A FAIRE POUR PROTEGER LA ZONE");
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TankProtectPositionDirective(computerTank1, new Coord(10, 9)).play());
			assertEquals(new Coord(10, 9), computerTank1.getPosition());
			computerTank1.restoreMove();

			_logger.system("DEJA SUR ZONE MAIS RIEN A FAIRE");
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TankProtectPositionDirective(computerTank1, new Coord(10, 9)).play());
			assertEquals(new Coord(10, 9), computerTank1.getPosition());
			assertEquals(computerTank1.getMovementCapacity(), computerTank1.getMove());

			_logger.system("ENEMI VISIBLE MAIS TROP LOIN");
			final Tank humanTank1 = new Tank(new Coord(7, 5), human);
			human.addUnit(humanTank1, true);
			computer.startTurn(1);
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TankProtectPositionDirective(computerTank1, new Coord(10, 9)).play());
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(new Coord(10, 9), computerTank1.getPosition());
			assertEquals(computerTank1.getMovementCapacity(), computerTank1.getMove());

			_logger.system("ENEMI VISIBLE ET A PORTEE");
			final Tank humanTank2 = new Tank(new Coord(8, 9), human);
			human.addUnit(humanTank2, true);
			humanTank2.setHitPoints(1);
			FSRandom.getInstance(0.5f);
			computer.startTurn(1);
			assertEquals(2, computer.getEnemySet().size());
			assertEquals(new Coord(10, 9), computerTank1.getPosition());
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TankProtectPositionDirective(computerTank1, new Coord(10, 9)).play());
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(new Coord(10, 9), computerTank1.getPosition());
			assertEquals(0, computerTank1.getMove());
			assertEquals(1, computerTank1.getNbAttack());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
