package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Artillery;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.RadarStation;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TankProtectArtilleryDirectiveTest extends MapTestCase
{

	public TankProtectArtilleryDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "4" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2),
					new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final RadarStation radar = new RadarStation(new Coord(7, 4), computer);
			computer.addUnit(radar, true);

			final Artillery artillery = new Artillery(new Coord(7, 3), computer);
			computer.addUnit(artillery, true);

			final ComputerTank computerTank1 = new ComputerTank(new Coord(10, 4), computer);
			computer.addUnit(computerTank1, true);
			computer.startTurn(1);

			assertEquals(0, computer.getEnemySet().size());

			try
			{
				new TankProtectArtilleryDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Not Enough Move point
			computerTank1.setMove(0);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankProtectArtilleryDirective(computerTank1).play());
			assertEquals(new Coord(10, 4), computerTank1.getPosition());

			// Get closer to Artillery position
			computerTank1.setMove(MoveCostFactory.GROUND_MOVE_COST.getCost(CellTypeCst.GRASSLAND)*2);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankProtectArtilleryDirective(computerTank1).play());
			assertEquals(new Coord(8, 3), computerTank1.getPosition());
			assertEquals(0, computerTank1.getMove());
			assertEquals(2, computerTank1.getNbAttack());

			// No enemy, no need to get closer
			computerTank1.restoreMove();
			computerTank1.restoreNbAttack();
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankProtectArtilleryDirective(computerTank1).play());
			assertEquals(new Coord(8, 3), computerTank1.getPosition());
			assertEquals(0, computer.getEnemySet().size());
			assertEquals(computerTank1.getMovementCapacity(), computerTank1.getMove());
			assertEquals(2, computerTank1.getNbAttack());

			// Attack a Fighter over the sea with distance = 1
			Fighter humanFighter = new Fighter(new Coord(6, 3), human);
			human.addUnit(humanFighter, true);
			humanFighter.setHitPoints(1);

			computer.startTurn(2);
			FSRandom.getInstance(0.1f);
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankProtectArtilleryDirective(computerTank1).play());
			assertEquals(new Coord(8, 3), computerTank1.getPosition());
			assertEquals(0, computer.getEnemySet().size());
			assertFalse(computerTank1.isDamaged());
			assertTrue(computerTank1.getHitPoints() < computerTank1.getHitPointsCapacity());
			assertEquals(1, computerTank1.getNbAttack());

			// Attack a Fighter over the sea, but reachable, with distance = 2
			// and get back to initial position
			Fighter humanFighter2 = new Fighter(new Coord(6, 1), human);
			human.addUnit(humanFighter2, true);
			humanFighter2.setHitPoints(1);

			computer.startTurn(3);
			FSRandom.getInstance(0.1f);
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankProtectArtilleryDirective(computerTank1).play());
			assertEquals(new Coord(8, 3), computerTank1.getPosition());
			assertEquals(0, computer.getEnemySet().size());
			assertFalse(computerTank1.isDamaged());
			assertTrue(computerTank1.getHitPoints() < computerTank1.getHitPointsCapacity());
			assertEquals(1, computerTank1.getNbAttack());
			assertEquals(0, computerTank1.getMove());

			// Tank Damaged

			computerTank1.setHitPoints(1);
			computerTank1.restoreMove();
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankProtectArtilleryDirective(computerTank1).play());
			assertEquals(new Coord(10, 4), computerTank1.getPosition());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
