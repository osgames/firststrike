package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TankWaitingForLoadingDirectiveTest extends MapTestCase
{

	public TankWaitingForLoadingDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "7", "7" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Fighter humanFighter1 = new Fighter(new Coord(6, 7), human);
			humanFighter1.setHitPoints(1);
			human.addUnit(humanFighter1, true);

			final ComputerTank computerTank1 = new ComputerTank(new Coord(10, 7), computer);
			computer.addUnit(computerTank1, true);
			computer.startTurn(1);
			assertEquals(1, computer.getEnemySet().size());

			final ComputerTank computerTank2 = new ComputerTank(new Coord(9, 9), computer);
			computer.addUnit(computerTank2, true);
			assertEquals(1, computer.getEnemySet().size());

			try
			{
				new TankWaitingForLoadingDirective(computerTank1).play();
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			mapMgr.getCityList().getCityAtPosition(new Coord(10, 10)).setPlayer(computer);

			try
			{
				new TankWaitingForLoadingDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			computer.startTurn(1);
			// Get closer to the loading position
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankWaitingForLoadingDirective(computerTank1).play());
			assertEquals(new Coord(7, 7), computerTank1.getPosition());
			assertEquals(1, computer.getEnemySet().size());

			// Get closer and track ennemies
			FSRandom.getInstance(0.5f);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankWaitingForLoadingDirective(computerTank2).play());
			assertEquals(new Coord(7, 7), computerTank2.getPosition());
			assertEquals(0, computer.getEnemySet().size());

			// Nothing to do...
			computerTank1.setMove(3);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankWaitingForLoadingDirective(computerTank1).play());
			assertEquals(new Coord(7, 7), computerTank1.getPosition());
			assertEquals(0, computer.getEnemySet().size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
