package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TankProtectAreaDirectiveTest extends MapTestCase {

	public TankProtectAreaDirectiveTest(final String p_aName) {
		super(p_aName);
	}

	public void test_play_protectArea() {
		try {
			assertFalse(ApplicationConfigurationMgr.getInstance().isDebugMode());

			final FSMap map = buildFSMap(STANDARD_MAP_5);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final String[][] playersArray = { { "HUMAN", "3", "0" }, { "COMPUTER", "0", "0" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2),
					new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final ComputerTank computerTank = new ComputerTank(new Coord(1, 1), computer);
			computer.addUnit(computerTank, true);
			computerTank.addDirective(new TankAutoDirective(computerTank));

			computer.startTurn(1);
			// Assign all city to Computer
			for (City city : mapMgr.getCityList()) {
				if (city.getPlayer() != computer) {
					computer.addCity(city, false);
				}
			}

			// No enemy
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankProtectAreaDirective(computerTank).play());
			assertEquals(computerTank.getMovementCapacity(), computerTank.getMove());

			// Move and attack 1 enemy
			final Tank enemyTank1 = new Tank(new Coord(4, 4), human);
			human.addUnit(enemyTank1, true);

			computer.startTurn(2);
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankProtectAreaDirective(computerTank).play());
			assertEquals(new Coord(3, 3), computerTank.getPosition());
			assertEquals(0, computerTank.getMove());

			// Too much enemy
			final Tank enemyTank2 = new Tank(new Coord(4, 4), human);
			human.addUnit(enemyTank2, true);
			final Tank enemyTank3 = new Tank(new Coord(4, 4), human);
			human.addUnit(enemyTank3, true);

			computer.startTurn(3);
			enemyTank1.setHitPoints(1);
			enemyTank2.setHitPoints(1);
			enemyTank3.setHitPoints(1);
			assertEquals(3, computer.getEnemySet().size());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankProtectAreaDirective(computerTank).play());
			assertEquals(new Coord(3, 3), computerTank.getPosition());
			assertEquals(1, computer.getEnemySet().size());

			// Kill last and wait
			computer.startTurn(4);
			assertEquals(1, computer.getEnemySet().size());
			FSRandom.getInstance(0.01f);
			computerTank.setHitPoints(computerTank.getHitPointsCapacity());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankProtectAreaDirective(computerTank).play());
			assertEquals(new Coord(3, 3), computerTank.getPosition());
			assertEquals(0, computer.getEnemySet().size());

			// Go back to city (reparing required
			computer.startTurn(5);
			ComputerTank computerTank2 = new ComputerTank(new Coord(4, 0), computer);
			computer.addUnit(computerTank2, true);
			computerTank2.setHitPoints(5);
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankProtectAreaDirective(computerTank2).play());
			assertEquals(new Coord(4, 3), computerTank2.getPosition());

		} catch (Exception ex) {
			fail(ex);
		}
	}

	public void test_play_protectArea2() {
		try {
			assertFalse(ApplicationConfigurationMgr.getInstance().isDebugMode());

			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "4", "4" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2),
					new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Fighter humanFighter = new Fighter(new Coord(5, 3), human);
			human.addUnit(humanFighter, true);

			final ComputerTank computerTank = new ComputerTank(new Coord(2, 2), computer);
			computerTank.addDirective(new TankAutoDirective(computerTank));
			computer.addUnit(computerTank, true);

			computer.startTurn(1);
			// Assign all city to Computer
			assertEquals(1, computer.getCitySet().size());
			assertEquals(1, computer.getEnemySet().size());

			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, computerTank.playDirectives());
			assertEquals(2, computer.getCitySet().size());
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(new Coord(3,3), computerTank.getPosition());
			
			humanFighter.setHitPoints(1);
			FSRandom.getInstance(0.01f);

			computerTank.restoreUnit();
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankProtectAreaDirective(computerTank).play());
			assertEquals(computerTank.getMovementCapacity()-(25 + 25), computerTank.getMove());
			assertEquals(computerTank.getNbAttackCapacity()-1, computerTank.getNbAttack());
			assertEquals(0, computer.getEnemySet().size());
			
		} catch (Exception ex) {
			fail(ex);
		}
	}
}
