package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TankAttackCityDirectiveTest extends MapTestCase
{

	public TankAttackCityDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "4", "4" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final ComputerTank computerTank = new ComputerTank(new Coord(4, 4), computer);
			computer.addUnit(computerTank, true);

			computer.startTurn(1);
			computerTank.setMove(MoveCostFactory.GROUND_MOVE_COST.getCost(CellTypeCst.GRASSLAND));
			try
			{
				new TankAttackCityDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Attack City but a move is required (not enough move point to reach the city
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankAttackCityDirective(computerTank).play());
			assertEquals(new Coord (3,3), computerTank.getPosition());
			

			// Reach the city, attack the enemy in the city and get the city
			Tank humanTank = new Tank(new Coord(1,1), human);
			human.addUnit(humanTank, true);
			computer.startTurn(2);
			humanTank.setHitPoints(1);
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankAttackCityDirective(computerTank).play());
			

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
