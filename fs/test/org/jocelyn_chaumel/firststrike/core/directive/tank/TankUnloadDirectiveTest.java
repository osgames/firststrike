package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.directive.FindPathAndMoveWithoutCombatDirective;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TankUnloadDirectiveTest extends MapTestCase
{

	public TankUnloadDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "7", "7" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			mapMgr.getCityList().getCityAtPosition(new Coord(10, 10)).setPlayer(computer);

			final Tank computerTank1 = new Tank(new Coord(7, 7), computer);
			computer.addUnit(computerTank1, true);

			final Tank computerTank2 = new Tank(new Coord(7, 7), computer);
			computer.addUnit(computerTank2, true);

			final Tank computerTank3 = new Tank(new Coord(7, 7), computer);
			computer.addUnit(computerTank3, true);

			final TransportShip computerTransport = new TransportShip(new Coord(6, 6), computer);
			computer.addUnit(computerTransport, true);

			final Tank humanTransport = new Tank(new Coord(4, 2), human);
			human.addUnit(humanTransport, true);

			try
			{
				new TankUnloadDirective(null, new Coord(4, 3));
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new TankUnloadDirective(computerTank1, null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new TankUnloadDirective(computerTank1, new Coord(0, 0));
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new TankUnloadDirective(computerTank1, new Coord(2, 2)).play();
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankLoadDirective(computerTank1, computerTransport).play());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankLoadDirective(computerTank2, computerTransport).play());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankLoadDirective(computerTank3, computerTransport).play());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new FindPathAndMoveWithoutCombatDirective(	computerTransport,
																									new Coord(5, 3),
																									false,
																									false).play());

			// No move point
			assertEquals(1, computer.getUnitList().size());
			assertEquals(0, computer.getUnitIndex().getNonNullUnitListAt(new Coord(7, 7)).size());
			assertEquals(0, computerTransport.getFreeSpace());
			assertEquals(new Coord(5, 3), computerTransport.getPosition());
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TankUnloadDirective(computerTank1, new Coord(4, 3)).play());
			assertEquals(0, computer.getUnitIndex().getNonNullUnitListAt(new Coord(4, 3)).size());
			assertEquals(1, computer.getUnitList().size());
			assertEquals(0, computerTransport.getFreeSpace());

			// Basic unloading
			computerTransport.restoreUnit();
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankUnloadDirective(computerTank2, new Coord(4, 3)).play());
			assertEquals(1, computer.getUnitIndex().getNonNullUnitListAt(new Coord(4, 3)).size());
			assertEquals(2, computer.getUnitList().size());
			assertEquals(1, computerTransport.getFreeSpace());

			// Unable to unload because the enemy is not destroyed
			FSRandom.getInstance(0.01f);
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TankUnloadDirective(computerTank3, new Coord(4, 2)).play());
			assertEquals(1, computer.getUnitIndex().getNonNullUnitListAt(new Coord(4, 3)).size());
			assertEquals(2, computer.getUnitList().size());
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(1, computerTransport.getFreeSpace());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
