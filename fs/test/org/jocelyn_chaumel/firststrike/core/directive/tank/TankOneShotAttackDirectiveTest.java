package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TankOneShotAttackDirectiveTest extends MapTestCase
{
	private final static FSLogger _logger = FSLoggerManager.getLogger(TankProtectPositionDirectiveTest.class);

	public TankOneShotAttackDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public final void test_play_all()
	{
		try
		{

			final FSMap map = buildFSMap(STANDARD_MAP_5);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final String[][] playersArray = { { "HUMAN", "0", "0" }, { "COMPUTER", "4", "3" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final ComputerTank computerTank1 = new ComputerTank(new Coord(4, 3), computer);

			try
			{
				new TankOneShotAttackDirective(null);
				fail("Null param accepted");
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			computer.addUnit(computerTank1, true);


			_logger.system("AUCUN ENNEMI A PORTEE");
			assertEquals(	MoveStatusCst.MOVE_SUCCESFULL,
							new TankOneShotAttackDirective(computerTank1).play());
			assertEquals(computerTank1.getMovementCapacity(), computerTank1.getMove());
			
			_logger.system("ENEMI VISIBLE MAIS TROP LOIN");
			final Tank humanTank1 = new Tank(new Coord(6, 5), human);
			human.addUnit(humanTank1, true);
			computer.startTurn(1);
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(	MoveStatusCst.MOVE_SUCCESFULL,
							new TankOneShotAttackDirective(computerTank1).play());
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(new Coord(4, 3), computerTank1.getPosition());
			assertEquals(computerTank1.getMovementCapacity(), computerTank1.getMove());

			_logger.system("ENEMI VISIBLE ET A PORTEE MAIS SANS ASSEZ DE MOVE");
			computerTank1.setMove(computerTank1.getMovementCapacity() - 1);
			final Tank humanTank2 = new Tank(new Coord(4, 5), human);
			human.addUnit(humanTank2, true);
			humanTank2.setHitPoints(1);
			FSRandom.getInstance(0.5f);
			computer.startTurn(1);

			computerTank1.setMove(66);
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TankOneShotAttackDirective(computerTank1).play());
			assertEquals(66, computerTank1.getMove());
			computerTank1.restoreMove();

			
			_logger.system("ENEMI VISIBLE ET A PORTEE");
			assertEquals(2, computer.getEnemySet().size());
			assertEquals(	MoveStatusCst.MOVE_SUCCESFULL,
							new TankOneShotAttackDirective(computerTank1).play());
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(new Coord(4, 3), computerTank1.getPosition());
			assertEquals(0, computerTank1.getMove());
			assertEquals(1, computerTank1.getNbAttack());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
