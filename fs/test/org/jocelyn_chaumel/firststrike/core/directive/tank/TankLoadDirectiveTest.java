package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TankLoadDirectiveTest extends MapTestCase
{

	public TankLoadDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "7", "7" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			mapMgr.getCityList().getCityAtPosition(new Coord(10, 10)).setPlayer(computer);

			final Tank computerTank1 = new Tank(new Coord(7, 7), computer);
			computer.addUnit(computerTank1, true);

			final Tank computerTank2 = new Tank(new Coord(7, 7), computer);
			computer.addUnit(computerTank2, true);

			final Tank computerTank3 = new Tank(new Coord(7, 7), computer);
			computer.addUnit(computerTank3, true);

			final Tank computerTank4 = new Tank(new Coord(7, 7), computer);
			computer.addUnit(computerTank4, true);

			final TransportShip computerTransport1 = new TransportShip(new Coord(6, 6), computer);
			computer.addUnit(computerTransport1, true);

			final TransportShip computerTransport2 = new TransportShip(new Coord(6, 0), computer);
			computer.addUnit(computerTransport2, true);

			final TransportShip humanTransport = new TransportShip(new Coord(0, 0), human);
			human.addUnit(humanTransport, true);

			try
			{
				new TankLoadDirective(null, computerTransport1);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new TankLoadDirective(computerTank1, null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new TankLoadDirective(computerTank1, humanTransport);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Get closer to the loading position
			assertEquals(6, computer.getUnitList().size());
			assertEquals(4, computer.getUnitIndex().getNonNullUnitListAt(new Coord(7, 7)).size());
			assertEquals(3, computerTransport1.getFreeSpace());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankLoadDirective(computerTank1, computerTransport1).play());
			assertEquals(5, computer.getUnitList().size());
			assertEquals(3, computer.getUnitIndex().getNonNullUnitListAt(new Coord(7, 7)).size());
			assertEquals(2, computerTransport1.getFreeSpace());

			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankLoadDirective(computerTank2, computerTransport1).play());
			assertEquals(4, computer.getUnitList().size());
			assertEquals(2, computer.getUnitIndex().getNonNullUnitListAt(new Coord(7, 7)).size());
			assertEquals(1, computerTransport1.getFreeSpace());

			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankLoadDirective(computerTank3, computerTransport1).play());
			assertEquals(3, computer.getUnitList().size());
			assertEquals(1, computer.getUnitIndex().getNonNullUnitListAt(new Coord(7, 7)).size());
			assertEquals(0, computerTransport1.getFreeSpace());

			try
			{
				new TankLoadDirective(computerTank4, computerTransport1).play();
				fail();
			} catch (IllegalStateException ex)
			{
				// nop
			}

			try
			{
				new TankLoadDirective(computerTank4, computerTransport2).play();
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
