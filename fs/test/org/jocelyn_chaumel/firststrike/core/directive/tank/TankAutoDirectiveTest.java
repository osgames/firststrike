package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TankAutoDirectiveTest extends MapTestCase
{

	public TankAutoDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final ComputerTank computerLoadedTank = new ComputerTank(new Coord(10, 10), computer);
			computer.addUnit(computerLoadedTank, true);

			final TransportShip computerTransport = new TransportShip(new Coord(10, 10), computer);
			computer.addUnit(computerTransport, true);
			computer.startTurn(1);
			computer.addCity(mapMgr.getCityList().getCityAtPosition(new Coord(7,7)), false);

			try
			{
				new TankAutoDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new TankAutoDirective(computerLoadedTank, null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			computer.loadUnit(computerLoadedTank, computerTransport);
			// Loaded Tank
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankAutoDirective(computerLoadedTank).play());

			final ComputerTank computerTank2 = new ComputerTank(new Coord(10, 10), computer);
			City computerCity = computer.getCitySet().getCityAtPosition(new Coord(10,10));
			computerTank2.setProtectedCity(computerCity);
			computer.addUnit(computerTank2, true);

			// City Protection
			map.getCityList().getCityAtPosition(new Coord(7, 7)).setPlayer(human);
			computer.removeCity(mapMgr.getCityList().getCityAtPosition(new Coord(7,7)));
			computer.startTurn(2);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankAutoDirective(computerTank2).play());
			assertEquals(new Coord(10, 10), computerTank2.getPosition());
			assertTrue(computerTank2.isProtectingCity());

			// Conquer the nearest city and finish to secure the area
			final ComputerTank computerTank3 = new ComputerTank(new Coord(10, 10), computer);
			computer.addUnit(computerTank3, true);
			computer.startTurn(3);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankAutoDirective(computerTank3).play());
			assertEquals(new Coord(7, 7), computerTank3.getPosition());
			assertFalse(computerTank3.isProtectingCity());

			final ComputerTank computerTank4 = new ComputerTank(new Coord(8, 8), computer);
			computer.addUnit(computerTank4, true);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankAutoDirective(computerTank4).play());
			assertEquals(new Coord(7, 7), computerTank4.getPosition());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
