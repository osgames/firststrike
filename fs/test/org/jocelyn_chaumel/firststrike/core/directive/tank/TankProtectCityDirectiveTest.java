package org.jocelyn_chaumel.firststrike.core.directive.tank;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TankProtectCityDirectiveTest extends MapTestCase
{

	public TankProtectCityDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "4", "4" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Fighter humanFighter1 = new Fighter(new Coord(3, 3), human);
			humanFighter1.setHitPoints(1);
			human.addUnit(humanFighter1, true);

			final Fighter humanFighter2 = new Fighter(new Coord(2, 2), human);
			humanFighter2.setHitPoints(1);
			human.addUnit(humanFighter2, true);

			final ComputerTank computerTank1 = new ComputerTank(new Coord(4, 4), computer);
			computer.addUnit(computerTank1, true);
			computer.startTurn(1);
			assertEquals(2, computer.getEnemySet().size());

			try
			{
				new TankProtectCityDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new TankProtectCityDirective(computerTank1);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			computerTank1.setProtectedCity(computer.getCitySet().get(0));

			// Not Enough Move point
			computerTank1.setMove(0);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankProtectCityDirective(computerTank1).play());
			assertEquals(2, computer.getEnemySet().size());
			assertEquals(new Coord(4, 4), computerTank1.getPosition());

			// Closest Enemy defeated
			FSRandom.getInstance(0.5f);
			computerTank1.restoreMove();
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankProtectCityDirective(computerTank1).play());
			assertEquals(new Coord(4, 4), computerTank1.getPosition());
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(75, computerTank1.getMove());
			assertEquals(1, computerTank1.getNbAttack());

			// Enemy defeated (One shot attack)
			computerTank1.restoreMove();
			computerTank1.restoreNbAttack();
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TankProtectCityDirective(computerTank1).play());
			assertEquals(new Coord(4, 4), computerTank1.getPosition());
			assertEquals(0, computer.getEnemySet().size());
			assertEquals(0, computerTank1.getMove());

			// Move Successfully (end of city protection cause enough tank to do
			// it)
			final ComputerTank computerTank2 = new ComputerTank(new Coord(4, 4), computer);
			computer.addUnit(computerTank2, true);
			computerTank2.setProtectedCity(computer.getCitySet().get(0));
			assertTrue(computerTank2.isProtectingCity());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TankProtectCityDirective(computerTank2).play());
			assertFalse(computerTank2.isProtectingCity());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
