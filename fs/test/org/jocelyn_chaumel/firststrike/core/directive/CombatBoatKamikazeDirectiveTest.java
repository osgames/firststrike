package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class CombatBoatKamikazeDirectiveTest extends MapTestCase
{

	public CombatBoatKamikazeDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);

			final Destroyer computerDestroyer1 = new Destroyer(new Coord(11, 5), computer);
			computer.addUnit(computerDestroyer1, true);

			try
			{
				new CombatBoatKamikazeDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			// No Enemy, then switch to Patrol Directive
			FSRandom.getInstance(0.5f);
			assertTrue(computer.getEnemySet().isEmpty());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new CombatBoatKamikazeDirective(computerDestroyer1).play());
			assertEquals(0, computerDestroyer1.getMove());
			assertEquals(new Coord(7, 6), computerDestroyer1.getPosition());
			assertEquals(new Coord(0, 6), computerDestroyer1.getPath().getDestination());

			// Enemy Detected
			final Fighter humanFighter1 = new Fighter(new Coord(3, 6), human);
			human.addUnit(humanFighter1, true);
			computerDestroyer1.restoreMove();
			assertTrue(computer.getEnemySet().isEmpty());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new CombatBoatKamikazeDirective(computerDestroyer1).play());
			assertEquals(new Coord(4, 6), computerDestroyer1.getPosition());
			assertFalse(humanFighter1.isLiving());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
