package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.RadarStation;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.UnitIndex;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class HuntEnemyDirectiveTest extends MapTestCase
{

	public HuntEnemyDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final UnitIndex unitIndex = human.getUnitIndex();
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Fighter computerFighter1 = new Fighter(new Coord(10, 10), computer);
			computer.addUnit(computerFighter1, true);
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(10, 10)).size());
			assertEquals(1, computer.getUnitList().size());

			final Fighter humanFighter1 = new Fighter(new Coord(1, 1), human);
			human.addUnit(humanFighter1, true);
			Destroyer humanDestroyer1 = new Destroyer(new Coord(0, 0), human);
			human.addUnit(humanDestroyer1, true);
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(1, 1)).size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(0, 0)).size());
			assertEquals(2, human.getUnitList().size());

			final Fighter computerFighterBidon = new Fighter(new Coord(4, 4), computer);
			computer.addUnit(computerFighterBidon, true);
			assertEquals(1, computer.getEnemySet().size());

			try
			{
				new HuntEnemyDirective(null, computerFighter1, true);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new HuntEnemyDirective(humanFighter1, null, true);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Not Enough Move point
			computerFighter1.setMove(MoveCostFactory.AIR_MOVE_COST.getMinCost() * 2);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new HuntEnemyDirective(	computerFighter1,
																					humanFighter1,
																					true).play());
			assertEquals(new Coord(8, 8), computerFighter1.getPosition());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(10, 10)).size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(8, 8)).size());

			// Enemy Detected
			FSRandom.getInstance(0.5f);
			computerFighter1.restoreMove();
			humanFighter1.setHitPoints(2);
			assertTrue(humanFighter1.isLiving());
			assertEquals(	MoveStatusCst.ENEMY_DETECTED,
							new HuntEnemyDirective(computerFighter1, humanFighter1, true).play());
			assertEquals(new Coord(3, 3), computerFighter1.getPosition());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(8, 8)).size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(3, 3)).size());

			// Move Successfully
			assertEquals(	MoveStatusCst.MOVE_SUCCESFULL,
							new HuntEnemyDirective(computerFighter1, humanFighter1, true).play());
			assertEquals(new Coord(2, 2), computerFighter1.getPosition());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(1, 1)).size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(2, 2)).size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(0, 0)).size());
			assertEquals(1, human.getUnitList().size());
			assertFalse(humanFighter1.isLiving());
			assertTrue(computerFighter1.isLiving());

			// Unit defeated
			computerFighter1.setHitPoints(1);
			computerFighter1.restoreMove();
			assertEquals(2, computer.getUnitList().size());
			assertEquals(	MoveStatusCst.UNIT_DEFEATED,
							new HuntEnemyDirective(computerFighter1, humanDestroyer1, true).play());
			assertFalse(computerFighter1.isLiving());
			assertTrue(humanDestroyer1.isLiving());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(2, 2)).size());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(1, 1)).size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(0, 0)).size());
			assertEquals(1, computer.getUnitList().size());
			
			// Get Path with Combat cause enemy not reachable
			

		} catch (Exception ex)
		{
			fail(ex);
		}
	}


	public void test_play_getPathWithCombat()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final RadarStation radar = new RadarStation(new Coord(10,10), computer);
			computer.addUnit(radar, false);

			final Fighter target = new Fighter(new Coord(7, 10), human);
			human.addUnit(target, false);

			final Tank humanTank1 = new Tank(new Coord(7, 9), human);
			human.addUnit(humanTank1, false);
			final Tank humanTank2 = new Tank(new Coord(8, 9), human);
			human.addUnit(humanTank2, false);
			final Tank humanTank3 = new Tank(new Coord(8, 10), human);
			human.addUnit(humanTank3, false);
			
			computer.startTurn(1);
			assertEquals(4, computer.getEnemySet().size());

			// Get Path with Combat cause enemy not reachable
			final ComputerTank computerTank1 = new ComputerTank(new Coord(10, 10), computer);
			computer.addUnit(computerTank1, true);
			computerTank1.setMove(MoveCostFactory.GROUND_MOVE_COST.getCost(CellTypeCst.GRASSLAND));
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new HuntEnemyDirective(computerTank1, target, true).play());
			assertEquals(new Coord(9,10), computerTank1.getPosition());
			
			final Fighter computerFighter1 = new Fighter(new Coord(10, 10), computer);
			computer.addUnit(computerFighter1, true);
			
			// Get Path without Combat
			computerFighter1.setMove(MoveCostFactory.AIR_MOVE_COST.getMinCost() * 2);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new HuntEnemyDirective(computerFighter1, target, true).play());
			assertEquals(new Coord(8,11), computerFighter1.getPosition());
			

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
