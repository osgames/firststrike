package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.UnitIndex;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class MoveDirectiveTest extends MapTestCase
{

	public MoveDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final UnitIndex unitIndex = human.getUnitIndex();
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Fighter computerFighter1 = new Fighter(new Coord(10, 10), computer);
			computer.addUnit(computerFighter1, true);
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(10, 10)).size());
			assertEquals(1, computer.getUnitList().size());

			final Fighter humanFighter1 = new Fighter(new Coord(1, 1), human);
			human.addUnit(humanFighter1, true);
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(1, 1)).size());
			assertEquals(1, human.getUnitList().size());

			final Fighter humanFighter2 = new Fighter(new Coord(10, 1), human);
			human.addUnit(humanFighter2, true);
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(10, 1)).size());
			assertEquals(2, human.getUnitList().size());

			try
			{
				new MoveDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new MoveDirective(computerFighter1).play();
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Not Enough Move point
			computerFighter1.setMove(10);
			computerFighter1.setPath(new Path(new Coord[] { new Coord(9, 9), new Coord(8, 8) }));
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new MoveDirective(computerFighter1).play());
			assertEquals(new Coord(9, 9), computerFighter1.getPosition());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(10, 10)).size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(9, 9)).size());

			// Move Successfully
			computerFighter1.restoreMove();
			assertEquals(0, computer.getEnemySet().size());
			assertTrue(humanFighter1.isLiving());
			computerFighter1.setPath(new Path(new Coord[] { new Coord(8, 8), new Coord(7, 7), new Coord(6, 6) }));
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new MoveDirective(computerFighter1).play());
			assertEquals(new Coord(6, 6), computerFighter1.getPosition());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(9, 9)).size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(6, 6)).size());
			assertEquals(0, computer.getEnemySet().size());

			// Unit detected even if the move is finished
			final Fighter computerFighter2 = new Fighter(new Coord(10, 6), computer);
			computer.addUnit(computerFighter2, true);
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(10, 6)).size());
			assertEquals(2, computer.getUnitList().size());
			assertEquals(0, computer.getEnemySet().size());

			computerFighter2.setPath(new Path(new Coord[] { new Coord(10, 5), new Coord(10, 4) }));
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new MoveDirective(computerFighter2).play());
			assertEquals(new Coord(10, 4), computerFighter2.getPosition());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(10, 4)).size());
			assertEquals(1, computer.getEnemySet().size());

			// Unit detected
			final Fighter computerFighter3 = new Fighter(new Coord(5, 10), computer);
			computer.addUnit(computerFighter3, true);

			final Fighter humanFighter3 = new Fighter(new Coord(1, 10), human);
			human.addUnit(humanFighter3, true);

			assertEquals(3, human.getUnitList().size());
			assertEquals(3, computer.getUnitList().size());
			assertEquals(1, computer.getEnemySet().size());
			computerFighter3.setPath(new Path(new Coord[] { new Coord(4, 10), new Coord(3, 10) }));
			assertEquals(MoveStatusCst.ENEMY_DETECTED, new MoveDirective(computerFighter3).play());
			assertEquals(2, computer.getEnemySet().size());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(5, 10)).size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(4, 10)).size());
			assertEquals(new Coord(4, 10), computerFighter3.getPosition());

			human.killUnit(humanFighter1);
			human.killUnit(humanFighter2);
			human.killUnit(humanFighter3);
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
