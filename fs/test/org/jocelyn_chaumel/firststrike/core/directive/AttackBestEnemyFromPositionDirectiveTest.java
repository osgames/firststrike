package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.UnitIndex;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class AttackBestEnemyFromPositionDirectiveTest extends MapTestCase
{

	public AttackBestEnemyFromPositionDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "4", "4" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final UnitIndex unitIndex = human.getUnitIndex();
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			// Human's unit(s)
			final Fighter humanFighter1 = new Fighter(new Coord(1, 1), human);
			human.addUnit(humanFighter1, true);

			final Destroyer humanDestroyer1 = new Destroyer(new Coord(1, 0), human);
			human.addUnit(humanDestroyer1, true);
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(1, 0)).size());
			assertEquals(2, human.getUnitList().size());

			// Computer's unit(s)
			final Fighter computerFighter1 = new Fighter(new Coord(5, 5), computer);
			computer.addUnit(computerFighter1, true);
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(5, 5)).size());
			assertEquals(1, computer.getUnitList().size());

			final Fighter computerFighter2 = new Fighter(new Coord(4, 4), computer);
			computer.addUnit(computerFighter2, true);
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(4, 4)).size());
			assertEquals(2, computer.getUnitList().size());

			// Null param tests
			try
			{
				new AttackBestEnemyFromPositionDirective(null, new Coord(4, 4), 4);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new AttackBestEnemyFromPositionDirective(humanFighter1, null, 4);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Not Enough Move point test
			computerFighter1.setMove(computerFighter1.getMoveCost().getMinCost());
			assertEquals(1, computer.getEnemySet().size());
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new AttackBestEnemyFromPositionDirective(computerFighter1, new Coord(4, 4), 4).play());
			assertEquals(new Coord(4, 4), computerFighter1.getPosition());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(5, 5)).size());
			assertEquals(2, unitIndex.getNonNullUnitListAt(new Coord(4, 4)).size());
			assertEquals(1, computer.getEnemySet().size());

			// Enemy Detected destroyed
			FSRandom.getInstance(0.5f);
			computerFighter1.restoreMove();
			humanFighter1.setHitPoints(2);
			assertEquals(2, human.getUnitList().size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(1, 1)).size());
			assertTrue(humanFighter1.isLiving());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new AttackBestEnemyFromPositionDirective(	computerFighter1,
																									new Coord(4, 4),
																									3).play());
			assertEquals(new Coord(2, 2), computerFighter1.getPosition());
			assertFalse(humanFighter1.isLiving());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(1, 1)).size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(2, 2)).size());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(4, 4)).size());
			assertEquals(1, human.getUnitList().size());

			// Unit defeated
			FSRandom.getInstance(0.8f);
			assertEquals(	MoveStatusCst.UNIT_DEFEATED,
							new HuntEnemyDirective(computerFighter1, humanDestroyer1, true).play());
			assertFalse(computerFighter1.isLiving());
			assertTrue(humanDestroyer1.isLiving());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(1, 1)).size());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(2, 2)).size());
			assertEquals(1, human.getUnitList().size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
