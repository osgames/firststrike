/*
 * Created on Jun 30, 2005
 */
package org.jocelyn_chaumel.firststrike.core.directive.helper;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.FirstStrikeMgr;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

/**
 * @author Jocelyn Chaumel
 */
public final class TargetFinderHelperTest extends MapTestCase
{

	private final static FSLogger _logger = FSLoggerManager.getLogger(TargetFinderHelperTest.class.getName());

	/**
	 * Contructeur paramétrisé.
	 * 
	 * @param aName
	 */
	public TargetFinderHelperTest(final String aName)
	{
		super(aName);
	}

	public final void test_findBestTargetInList_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);

			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();
			final PlayerMgr playerMgr = game.getPlayerMgr();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Tank enemyTank = new Tank(new Coord(2, 1), computer);
			final Tank tank = new Tank(new Coord(1, 1), human);

			final AbstractUnitList enemyList = new AbstractUnitList();
			enemyList.add(enemyTank);
			try
			{
				TargetFinderHelper.findBestTargetInList(null, new Coord(1, 1), enemyList);
				fail("Null param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				TargetFinderHelper.findBestTargetInList(tank, null, enemyList);
				fail("Null param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				TargetFinderHelper.findBestTargetInList(tank, new Coord(1, 1), null);
				fail("Null param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			enemyList.clear();
			try
			{
				TargetFinderHelper.findBestTargetInList(tank, new Coord(1, 1), enemyList);
				fail("Empty enemy list has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			_logger.system("TANK - ONE ENEMY");
			final Tank enemyTank1 = new Tank(new Coord(3, 3), computer);
			enemyList.add(enemyTank1);
			assertEquals(enemyTank1, TargetFinderHelper.findBestTargetInList(tank, tank.getPosition(), enemyList));
			
			_logger.system("TANK - TWO ENEMIES");
			final Fighter enemyFighter1 = new Fighter(new Coord(5, 5), computer);
			enemyList.add(enemyFighter1);
			assertEquals(enemyTank1, TargetFinderHelper.findBestTargetInList(tank, tank.getPosition(), enemyList));

			_logger.system("FIGTHER - TWO ENEMIES");
			final Fighter fighter = new Fighter(new Coord(4, 4), human);
			fighter.setMove(2);
			assertEquals(enemyFighter1, TargetFinderHelper.findBestTargetInList(fighter, fighter.getPosition(), enemyList));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_findBestTargetInRange()
	{
		try
		{
			ApplicationConfigurationMgr.getInstance().setDebugMode(false);
			final FSMap map = buildFSMap(STANDARD_MAP_1);

			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();
			final PlayerMgr playerMgr = game.getPlayerMgr();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			human.setCityDetectionRange(5);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Tank computerTank1 = new Tank(new Coord(3, 2), computer);
			computer.addUnit(computerTank1, true);

			final Fighter computerFighter = new Fighter(new Coord(5, 5), computer);
			computer.addUnit(computerFighter, true);

			assertEquals(2, computer.getUnitList().size());
			assertEquals(0, computer.getEnemySet().size());

			human.startTurn(1);
			final Tank humanTank = new Tank(new Coord(1, 1), human);
			human.addUnit(humanTank, true);

			assertEquals(1, human.getUnitList().size());
			assertEquals(2, human.getEnemySet().size());

			try
			{
				TargetFinderHelper.findBestTargetInRange(null, humanTank.getPosition(), 1);
				fail("null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				TargetFinderHelper.findBestTargetInRange(humanTank, null, 1);
				fail("null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				TargetFinderHelper.findBestTargetInRange(humanTank, humanTank.getPosition(), -1);
				fail("Invalid range has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			_logger.system("NO ENEMY BECAUSE TOO SMALL RANGE");
			assertNull(TargetFinderHelper.findBestTargetInRange(humanTank, humanTank.getPosition(), 1));

			// ===================================================
			_logger.system("ONLY ONE ENEMY");
			assertEquals(1, human.getEnemySet().getCoordSet().getCoordInRange(humanTank.getPosition(), 2).size());
			assertEquals(computerTank1, TargetFinderHelper.findBestTargetInRange(humanTank, humanTank.getPosition(), 2));

			// ===================================================
			_logger.system("TWO ENEMIES");
			assertEquals(2, human.getEnemySet().size());
			assertEquals(2, human.getEnemySet().getCoordSet().getCoordInRange(humanTank.getPosition(), 4).size());
			assertEquals(computerTank1, TargetFinderHelper.findBestTargetInRange(humanTank, humanTank.getPosition(), 4));
			
			// ===================================================
			computerTank1.setHitPoints(-1);
			computer.killUnit(computerTank1);
			human.killEnemyUnit(computerTank1);

			Fighter fighter = new Fighter(new Coord(3, 3), human);
			human.addUnit(fighter, true);
			fighter.setMove(2);

			assertEquals(2, human.getUnitList().size());
			assertEquals(1, human.getEnemySet().size());
			assertEquals(1, human.getEnemySet().getCoordSet().getCoordInRange(fighter.getPosition(), 3).size());
			assertEquals(computerFighter, TargetFinderHelper.findBestTargetInRange(fighter, fighter.getPosition(), 3));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getBestUnloadingCoord_all()
	{
		try
		{
			_logger.system("test_getBestUnloadingCoord_all");

			final FSMap map = buildFSMap(STANDARD_MAP_4);

			final PlayerInfoList playerInfoList = buildPlayerInfoList(new String[][] { { "HUMAN", "4", "4" },
					{ "COMPUTER", "1", "1" } });
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();

			final PlayerMgr playerMgr = game.getPlayerMgr();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			human.addUnit(new Destroyer(new Coord(6, 1), human), true);
			human.addUnit(new Destroyer(new Coord(6, 0), human), true);
			human.addUnit(new Destroyer(new Coord(7, 0), human), true);

			final TransportShip transport1 = new TransportShip(new Coord(0, 0), computer);
			computer.addUnit(transport1, true);
			final TransportShip transport2 = new TransportShip(new Coord(5, 7), computer);
			computer.addUnit(transport2, true);
			computer.addUnit(new Destroyer(new Coord(5, 1), computer), true);

			final Area destArea = map.getAreaAt(new Coord(7, 1), AreaTypeCst.GROUND_AREA);

			try
			{
				TargetFinderHelper.getBestUnloadingCoord(null, destArea);
				fail("null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				TargetFinderHelper.getBestUnloadingCoord(transport1, null);
				fail("null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			Coord destCoord = TargetFinderHelper.getBestUnloadingCoord(transport1, destArea);
			assertEquals(new Coord(6, 2), destCoord);

			destCoord = TargetFinderHelper.getBestUnloadingCoord(transport2, destArea);
			assertEquals(new Coord(6, 5), destCoord);
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getRandomMoveCoord_all()
	{
		try
		{
			final CoordSet expectedCoordSet = CoordSet.createInitiazedCoordSet(new Coord(1, 1), 1, 3, 3);
			expectedCoordSet.remove(new Coord(1, 1));
			Coord currentCoord = null;
			assertEquals(8, expectedCoordSet.size());
			for (int i = 0; i < 100; i++)
			{
				currentCoord = TargetFinderHelper.getRandomMoveCoord(new Coord(1, 1), 1, 2, 2);
				assertTrue("This coord is invalid :" + currentCoord, expectedCoordSet.contains(currentCoord));
			}

			expectedCoordSet.clear();
			expectedCoordSet.add(new Coord(1, 0));
			expectedCoordSet.add(new Coord(0, 1));
			expectedCoordSet.add(new Coord(1, 1));
			for (int i = 0; i < 100; i++)
			{
				currentCoord = TargetFinderHelper.getRandomMoveCoord(new Coord(0, 0), 1, 2, 2);
				assertTrue("This coord is invalid :" + currentCoord, expectedCoordSet.contains(currentCoord));
			}

			expectedCoordSet.clear();
			expectedCoordSet.add(new Coord(2, 1));
			expectedCoordSet.add(new Coord(1, 0));
			expectedCoordSet.add(new Coord(1, 1));
			for (int i = 0; i < 100; i++)
			{
				currentCoord = TargetFinderHelper.getRandomMoveCoord(new Coord(2, 0), 1, 2, 2);
				assertTrue("This coord is invalid :" + currentCoord, expectedCoordSet.contains(currentCoord));
			}

			expectedCoordSet.clear();
			expectedCoordSet.add(new Coord(2, 1));
			expectedCoordSet.add(new Coord(1, 2));
			expectedCoordSet.add(new Coord(1, 1));
			for (int i = 0; i < 100; i++)
			{
				currentCoord = TargetFinderHelper.getRandomMoveCoord(new Coord(2, 2), 1, 2, 2);
				assertTrue("This coord is invalid :" + currentCoord, expectedCoordSet.contains(currentCoord));
			}

			expectedCoordSet.clear();
			expectedCoordSet.add(new Coord(0, 1));
			expectedCoordSet.add(new Coord(1, 2));
			expectedCoordSet.add(new Coord(1, 1));
			for (int i = 0; i < 100; i++)
			{
				currentCoord = TargetFinderHelper.getRandomMoveCoord(new Coord(0, 2), 1, 2, 2);
				assertTrue("This coord is invalid :" + currentCoord, expectedCoordSet.contains(currentCoord));
			}

			try
			{
				TargetFinderHelper.getRandomMoveCoord(null, 1, 2, 2);
				fail("Null param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				TargetFinderHelper.getRandomMoveCoord(new Coord(1, 1), 0, 2, 2);
				fail("Invalid range param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				TargetFinderHelper.getRandomMoveCoord(new Coord(1, 1), 1, 1, 2);
				fail("Invalid xMax param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				TargetFinderHelper.getRandomMoveCoord(new Coord(1, 1), 1, 2, 1);
				fail("Invalid yMax param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getRandomPatrolCoord_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);

			final PlayerInfoList playerInfoList = buildPlayerInfoList(new String[][] { { "HUMAN", "13", "1" },
					{ "COMPUTER", "4", "4" } });
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			final PlayerMgr playerMgr = game.getPlayerMgr();
			final AbstractPlayerList playerSet = playerMgr.getPlayerList();
			final ComputerPlayer computer = (ComputerPlayer) playerSet.get(1);

			final Coord start = new Coord(4, 4);
			final Fighter fighter1 = new Fighter(start, computer);
			final CoordSet coorSetToExclude = new CoordSet();
			final CoordSet expectedCoord = new CoordSet();
			expectedCoord.add(new Coord(0, 0));
			expectedCoord.add(new Coord(1, 0));
			expectedCoord.add(new Coord(2, 0));
			expectedCoord.add(new Coord(3, 0));
			expectedCoord.add(new Coord(4, 0));
			expectedCoord.add(new Coord(5, 0));
			expectedCoord.add(new Coord(6, 0));
			expectedCoord.add(new Coord(7, 0));
			expectedCoord.add(new Coord(8, 0));
			expectedCoord.add(new Coord(9, 0));
			expectedCoord.add(new Coord(10, 0));
			expectedCoord.add(new Coord(11, 0));
			expectedCoord.add(new Coord(12, 0));
			expectedCoord.add(new Coord(12, 1));
			expectedCoord.add(new Coord(12, 2));
			expectedCoord.add(new Coord(12, 3));
			expectedCoord.add(new Coord(12, 4));
			expectedCoord.add(new Coord(12, 5));
			expectedCoord.add(new Coord(12, 6));
			expectedCoord.add(new Coord(12, 7));
			expectedCoord.add(new Coord(12, 8));
			expectedCoord.add(new Coord(12, 9));
			expectedCoord.add(new Coord(12, 10));
			expectedCoord.add(new Coord(12, 11));
			expectedCoord.add(new Coord(11, 11));
			expectedCoord.add(new Coord(10, 11));
			expectedCoord.add(new Coord(9, 11));
			expectedCoord.add(new Coord(8, 11));
			expectedCoord.add(new Coord(7, 11));
			expectedCoord.add(new Coord(6, 11));
			expectedCoord.add(new Coord(5, 11));
			expectedCoord.add(new Coord(4, 11));
			expectedCoord.add(new Coord(3, 11));
			expectedCoord.add(new Coord(2, 11));
			expectedCoord.add(new Coord(1, 11));
			expectedCoord.add(new Coord(0, 11));
			expectedCoord.add(new Coord(0, 10));
			expectedCoord.add(new Coord(0, 9));
			expectedCoord.add(new Coord(0, 8));
			expectedCoord.add(new Coord(0, 7));
			expectedCoord.add(new Coord(0, 6));
			expectedCoord.add(new Coord(0, 5));
			expectedCoord.add(new Coord(0, 4));
			expectedCoord.add(new Coord(0, 3));
			expectedCoord.add(new Coord(0, 2));
			expectedCoord.add(new Coord(0, 1));
			expectedCoord.add(new Coord(0, 0));

			Coord currentCoord = null;
			for (int i = 0; i < 1000; i++)
			{
				currentCoord = TargetFinderHelper.getRandomPatrolCoord(fighter1, coorSetToExclude);
				assertTrue("This coord is invalid :" + currentCoord, expectedCoord.contains(currentCoord));
			}

			try
			{
				TargetFinderHelper.getRandomPatrolCoord(null, coorSetToExclude);
				fail("Null param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				TargetFinderHelper.getRandomPatrolCoord(fighter1, null);
				fail("Null param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}