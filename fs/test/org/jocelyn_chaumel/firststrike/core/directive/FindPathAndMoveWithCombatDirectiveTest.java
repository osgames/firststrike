package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.UnitIndex;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class FindPathAndMoveWithCombatDirectiveTest extends MapTestCase
{

	public FindPathAndMoveWithCombatDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final UnitIndex unitIndex = human.getUnitIndex();
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Fighter computerFighter1 = new Fighter(new Coord(10, 10), computer);
			computer.addUnit(computerFighter1, true);
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(10, 10)).size());
			assertEquals(1, computer.getUnitList().size());

			final Fighter humanFighter1 = new Fighter(new Coord(5, 5), human);
			humanFighter1.setHitPoints(1);
			human.addUnit(humanFighter1, true);
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(5, 5)).size());
			assertEquals(1, human.getUnitList().size());

			try
			{
				new FindPathAndMoveWithCombatDirective(null, new Coord(1, 1), true);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new FindPathAndMoveWithCombatDirective(computerFighter1, null, true);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Not Enough Move point
			computerFighter1.setMove(0);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new FindPathAndMoveWithCombatDirective(	computerFighter1,
																									new Coord(1, 1),
																									true).play());
			assertEquals(new Coord(10, 10), computerFighter1.getPosition());

			// Enemy defeated
			FSRandom.getInstance(0.5f);
			computerFighter1.restoreMove();
			assertEquals(0, computer.getEnemySet().size());
			assertTrue(humanFighter1.isLiving());

			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new FindPathAndMoveWithCombatDirective(	computerFighter1,
																								new Coord(1, 1),
																								true).play());
			assertEquals(new Coord(2, 2), computerFighter1.getPosition());
			assertEquals(1, unitIndex.getNonNullUnitListAt(new Coord(2, 2)).size());
			assertEquals(0, computer.getEnemySet().size());
			assertEquals(0, unitIndex.getNonNullUnitListAt(new Coord(5, 5)).size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
