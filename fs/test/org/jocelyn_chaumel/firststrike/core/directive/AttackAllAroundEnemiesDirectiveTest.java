package org.jocelyn_chaumel.firststrike.core.directive;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.FirstStrikeMgr;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class AttackAllAroundEnemiesDirectiveTest extends MapTestCase
{

	public AttackAllAroundEnemiesDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public final void test_combatAllEnemies_tank_valid_AttackerWin_1()
	{
		try
		{

			final FSMap map = buildFSMap(STANDARD_MAP_1);

			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();

			final PlayerMgr playerMgr = game.getPlayerMgr();
			final AbstractPlayerList playerList = playerMgr.getPlayerList();

			final ComputerPlayer computer = (ComputerPlayer) playerList.get(1);
			final HumanPlayer human = (HumanPlayer) playerList.get(0);
			assertEquals(0, computer.getUnitList().size());

			try
			{
				new AttackAllAroundEnemiesDirective(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			// No Enemy
			final ComputerTank computerTank1 = new ComputerTank(new Coord(2, 2), computer);
			computer.addUnit(computerTank1, true);
			assertEquals(0, computer.getEnemySet().size());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new AttackAllAroundEnemiesDirective(computerTank1).play());

			// No Move point
			final Fighter humanFighter1 = new Fighter(new Coord(1, 1), human);
			humanFighter1.setHitPoints(1);
			human.addUnit(humanFighter1, true);

			computer.startTurn(1);
			while (computerTank1.getNbAttack() > 0)
			{
				computerTank1.decreaseNbAttack();
			}

			assertEquals(1, computer.getEnemySet().size());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new AttackAllAroundEnemiesDirective(computerTank1).play());

			// With 2 enemies
			FSRandom.getInstance(0.01f);
			final Fighter humanFighter2 = new Fighter(new Coord(1, 1), human);
			humanFighter2.setHitPoints(1);
			human.addUnit(humanFighter2, true);
			computer.startTurn(2);
			assertEquals(2, computer.getEnemySet().size());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new AttackAllAroundEnemiesDirective(computerTank1).play());
			assertEquals(0, computer.getEnemySet().size());

			// Unit defeated
			final ComputerTank computerTank2 = new ComputerTank(new Coord(2, 2), computer);
			computer.addUnit(computerTank2, true);
			computerTank2.setHitPoints(1);

			final Fighter humanFighter3 = new Fighter(new Coord(1, 1), human);
			human.addUnit(humanFighter3, true);
			computer.startTurn(3);

			assertEquals(1, computer.getEnemySet().size());
			assertEquals(MoveStatusCst.UNIT_DEFEATED, new AttackAllAroundEnemiesDirective(computerTank2).play());
			assertEquals(1, computer.getEnemySet().size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}
