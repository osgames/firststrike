package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.directive.MoveDirective;
import org.jocelyn_chaumel.firststrike.core.directive.tank.TankAutoDirective;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TransportShipAutoDirectiveTest extends MapTestCase
{

	public TransportShipAutoDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			assertFalse(ApplicationConfigurationMgr.getInstance().isDebugMode());
			
			FSRandom.getInstance(0.99f);
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);

			final ComputerTransportShip computerTransport1 = new ComputerTransportShip(new Coord(7, 6), computer);
			final Area areaHome = map.getAreaAt(new Coord(8, 8), AreaTypeCst.GROUND_AREA);
			computer.addUnit(computerTransport1, true);
			
			try
			{
				new TransportShipAutoDirective(null, areaHome);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}
			
			try
			{
				new TransportShipAutoDirective(computerTransport1, null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Adding 3 tanks at 7,7
			final ComputerTank computerTank1 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank1, true);
			computerTank1.addDirective(new TankAutoDirective(computerTank1));

			final ComputerTank computerTank2 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank2, true);
			computerTank2.addDirective(new TankAutoDirective(computerTank2));

			final ComputerTank computerTank3 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank3, true);
			computerTank3.addDirective(new TankAutoDirective(computerTank3));
			computerTransport1.setAssignedArea(areaHome);

			computer.addCity(mapMgr.getCityList().getCityAtPosition(new Coord(7,7)), false);
			computer.startTurn(1);

			// Area fully under control
			assertEquals(4, computer.getUnitList().size());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TransportShipAutoDirective(computerTransport1, areaHome).play());
			assertEquals(1, computer.getUnitList().size());
			assertEquals(0, computerTransport1.getFreeSpace());
//			assertEquals(new Coord(8, 5), computerTransport1.getPosition());

			// Area no more under control (go back required)
			final Tank humanTank = new Tank(new Coord(9, 9), human);
			human.addUnit(humanTank, true);
			humanTank.setHitPoints(1);
			humanTank.setPath(new Path(new Coord(10, 10)));
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new MoveDirective(humanTank).play());
			
			computer.startTurn(1);
			assertEquals(1, computer.getEnemySet().size());
			computerTank1.setMove(0);
			assertEquals(2, human.getCitySet().size());
			assertEquals(1, computer.getCitySet().size());

			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TransportShipAutoDirective(computerTransport1, areaHome).play());
//			assertEquals(new Coord(7, 6), computerTransport1.getPosition());
			assertEquals(3, computer.getUnitList().size());
			assertEquals(1, computer.getEnemySet().size());

			computer.startTurn(1);
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TransportShipAutoDirective(computerTransport1, areaHome).play());
			assertEquals(4, computer.getUnitList().size());
			assertEquals(1, computer.getEnemySet().size());
			computerTank2.playDirectives();
			computerTank3.playDirectives();

			computer.startTurn(1);
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TransportShipAutoDirective(computerTransport1, areaHome).play());
			computerTank1.playDirectives();
			computerTank2.playDirectives();
			computerTank3.playDirectives();
			assertEquals(4, computer.getUnitList().size());
			assertEquals(computerTransport1.getSpaceCapacity(), computerTransport1.getFreeSpace());
			assertEquals(new Coord(7, 7), computerTransport1.getPosition());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}
