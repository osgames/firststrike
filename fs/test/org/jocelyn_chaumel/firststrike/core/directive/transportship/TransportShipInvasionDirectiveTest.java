package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.directive.tank.TankAutoDirective;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TransportShipInvasionDirectiveTest extends MapTestCase
{

	public TransportShipInvasionDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			FSRandom.getInstance(0.5f);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			try
			{
				new TransportShipInvasionDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			final ComputerTransportShip computerTransport1 = new ComputerTransportShip(new Coord(6, 5), computer);
			computer.addUnit(computerTransport1, true);
			final Area humanArea = map.getAreaAt(new Coord(3, 3), AreaTypeCst.GROUND_AREA);
			computerTransport1.setAssignedArea(humanArea);
			computer.startTurn(1);
			try
			{
				new TransportShipInvasionDirective(computerTransport1).play();
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Switch of protected area
			final ComputerTank computerTank1 = new ComputerTank(new Coord(8, 8), computer);
			computer.addUnit(computerTank1, true);
			computerTank1.addDirective(new TankAutoDirective(computerTank1));
			computer.startTurn(1);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, computerTank1.playDirectives());
			assertEquals(new Coord(7, 7), computerTank1.getPosition());

			final Area computerArea = map.getAreaAt(new Coord(8, 8), AreaTypeCst.GROUND_AREA);
			computerTransport1.setAssignedArea(computerArea);
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TransportShipInvasionDirective(computerTransport1).play());
			assertEquals(new Coord(7, 7), computerTransport1.getPosition());

			// Load and move to target area - unloading coord=(7,7)
			computerTank1.restoreUnit();
			final ComputerTank computerTank2 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank2, true);
			computerTank2.addDirective(new TankAutoDirective(computerTank2));

			final ComputerTank computerTank3 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank3, true);
			computerTank3.addDirective(new TankAutoDirective(computerTank3));

			assertEquals(4, computer.getUnitList().size());
			assertEquals(4, computer.getUnitIndex().getUnitListAt(new Coord(7, 7)).size());
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TransportShipInvasionDirective(computerTransport1).play());
			assertEquals(3, computerTransport1.getContainedUnitList().size());
			assertEquals(1, computer.getUnitList().size());
			assertNotSame(new Coord(7,7), computerTransport1.getPosition());
			assertEquals(0, computerTransport1.getFreeSpace());

			// Move and unload tank
			computerTransport1.restoreMove();
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TransportShipInvasionDirective(computerTransport1).play());
			assertEquals(0, computerTransport1.getContainedUnitList().size());
			assertEquals(computerTransport1.getSpaceCapacity(), computerTransport1.getFreeSpace());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
