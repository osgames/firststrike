package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.directive.MoveDirective;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TransportShipUnloadDirectiveTest extends MapTestCase
{

	public TransportShipUnloadDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();

			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);

			final ComputerTransportShip computerTransport1 = new ComputerTransportShip(new Coord(7, 6), computer);
			computer.addUnit(computerTransport1, true);
			computerTransport1.setAssignedArea(map.getAreaAt(new Coord(8, 8), AreaTypeCst.GROUND_AREA));

			final ComputerTank computerTank1 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank1, true);

			final ComputerTank computerTank2 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank2, true);

			final ComputerTank computerTank3 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank3, true);

			computer.addCity(mapMgr.getCityList().getCityAtPosition(new Coord(7,7)), false);
			computer.startTurn(1);
			assertEquals(4, computer.getUnitList().size());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TransportShipLoadDirective(computerTransport1).play());
			assertEquals(1, computer.getUnitList().size());
			assertEquals(0, computerTransport1.getFreeSpace());
			try
			{
				new TransportShipUnloadDirective(null, new Coord(7, 4));
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				new TransportShipUnloadDirective(computerTransport1, null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// Move the transport ship to the unload position
			computerTransport1.setPath(new Path(new Coord(7, 5)));
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new MoveDirective(computerTransport1).play());
			assertEquals(1, computer.getUnitList().size());
			assertEquals(new Coord(7,5), computerTransport1.getPosition());

			// Unload impossible : 1 Enemy detected on unloading position
			final Tank humanTank = new Tank(new Coord(7, 4), human);
			human.addUnit(humanTank, true);
			computer.startTurn(1);
			assertEquals(1, computer.getEnemySet().size());

			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TransportShipUnloadDirective(	computerTransport1,
																								new Coord(7, 4)).play());
			assertEquals(new Coord(7, 5), computerTransport1.getPosition());
			assertEquals(1, computer.getUnitList().size());
			assertEquals(1, computer.getEnemySet().size());

			computer.startTurn(1);
			computerTank1.setMove(2);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TransportShipUnloadDirective(	computerTransport1,
																							new Coord(8, 4)).play());
			assertEquals(3, computer.getUnitList().size());

			
			computer.startTurn(1);
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TransportShipUnloadDirective(	computerTransport1,
																							new Coord(8, 4)).play());
			assertEquals(4, computer.getUnitList().size());
			assertEquals(computerTransport1.getSpaceCapacity(), computerTransport1.getFreeSpace());
			assertEquals(1, computer.getEnemySet().size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
