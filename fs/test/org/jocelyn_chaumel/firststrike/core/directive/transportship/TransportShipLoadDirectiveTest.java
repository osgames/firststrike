package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TransportShipLoadDirectiveTest extends MapTestCase
{

	public TransportShipLoadDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final ComputerTransportShip computerTransport1 = new ComputerTransportShip(new Coord(7, 7), computer);
			computer.addUnit(computerTransport1, true);
			try
			{
				new TransportShipLoadDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}
			computer.addCity(mapMgr.getCityList().getCityAtPosition(new Coord(7,7)), false);
			computer.startTurn(1);

			// No tank available
			computerTransport1.setAssignedArea(map.getAreaAt(new Coord(8, 8), AreaTypeCst.GROUND_AREA));
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TransportShipLoadDirective(computerTransport1).play());

			// Only one tank available
			final ComputerTank computerTank1 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank1, true);
			assertEquals(2, computer.getUnitList().size());
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, new TransportShipLoadDirective(computerTransport1).play());
			assertEquals(computerTransport1.getSpaceCapacity(), computerTransport1.getFreeSpace());
			assertEquals(2, computer.getUnitList().size());

			// Only one tank available
			final ComputerTank computerTank2 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank2, true);

			final ComputerTank computerTank3 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank3, true);
			


			assertEquals(4, computer.getUnitList().size());
			assertEquals(4, computer.getUnitIndex().getUnitListAt(new Coord(7, 7)).size());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TransportShipLoadDirective(computerTransport1).play());
			assertEquals(1, computer.getUnitList().size());
			assertEquals(1, computer.getUnitIndex().getUnitListAt(new Coord(7, 7)).size());
			assertEquals(0, computerTransport1.getFreeSpace());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}
