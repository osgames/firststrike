package org.jocelyn_chaumel.firststrike.core.directive.transportship;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class TransportShipMoveAndLoadDirectiveTest extends MapTestCase
{

	public TransportShipMoveAndLoadDirectiveTest(final String p_aName)
	{
		super(p_aName);
	}

	public void test_play()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			FSRandom.getInstance(0.01f);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final ComputerTransportShip computerTransport1 = new ComputerTransportShip(new Coord(6, 5), computer);
			computer.addUnit(computerTransport1, true);
			computer.startTurn(1);
			try
			{
				new TransportShipMoveAndLoadDirective(null);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			// No tank available
			computerTransport1.setAssignedArea(map.getAreaAt(new Coord(8, 8), AreaTypeCst.GROUND_AREA));
			assertEquals(	MoveStatusCst.NOT_ENOUGH_MOVE_PTS,
							new TransportShipMoveAndLoadDirective(computerTransport1).play());
			assertEquals(new Coord(7, 7), computerTransport1.getPosition());

			final ComputerTank computerTank1 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank1, true);

			final ComputerTank computerTank2 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank2, true);

			final ComputerTank computerTank3 = new ComputerTank(new Coord(7, 7), computer);
			computer.addUnit(computerTank3, true);

			assertEquals(4, computer.getUnitList().size());
			assertEquals(4, computer.getUnitIndex().getUnitListAt(new Coord(7, 7)).size());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, new TransportShipLoadDirective(computerTransport1).play());
			assertEquals(1, computer.getUnitList().size());
			assertEquals(1, computer.getUnitIndex().getUnitListAt(new Coord(7, 7)).size());
			assertEquals(0, computerTransport1.getFreeSpace());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
