/*
 * Created on Aug 27, 2005
 */
package org.jocelyn_chaumel.firststrike.core.configuration;

import java.io.File;
import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.FSTestCase;
import org.jocelyn_chaumel.tools.FileMgr;

/**
 * @author Jocelyn Chaumel
 */
public class ConfigurationMgrTest extends FSTestCase
{

	public ConfigurationMgrTest(final String aName)
	{
		super(aName);
	}

	public final void test_updateLastGameList_valid()
	{
		try
		{
			final ApplicationConfigurationMgr cfgMgr = ApplicationConfigurationMgr.getCreateInstance(FileMgr
					.getCurrentDir());

			cfgMgr.setLastGameList(new ArrayList<String>());
			assertEquals(0, cfgMgr.getLastGameList().size());

			final File game1 = new File("c:\\temps\\toto.fsg");
			cfgMgr.updateLastGameList(game1);
			assertEquals(1, cfgMgr.getLastGameList().size());
			assertEquals("c:\\temps\\toto.fsg", (String) cfgMgr.getLastGameList().get(0));

			final File game2 = new File("c:\\temps\\tata.fsg");
			cfgMgr.updateLastGameList(game2);
			assertEquals(2, cfgMgr.getLastGameList().size());
			assertEquals("c:\\temps\\tata.fsg", (String) cfgMgr.getLastGameList().get(0));
			assertEquals("c:\\temps\\toto.fsg", (String) cfgMgr.getLastGameList().get(1));

			final File game3 = new File("c:\\temps\\tutu.fsg");
			cfgMgr.updateLastGameList(game3);
			assertEquals(3, cfgMgr.getLastGameList().size());
			assertEquals("c:\\temps\\tutu.fsg", (String) cfgMgr.getLastGameList().get(0));
			assertEquals("c:\\temps\\tata.fsg", (String) cfgMgr.getLastGameList().get(1));
			assertEquals("c:\\temps\\toto.fsg", (String) cfgMgr.getLastGameList().get(2));

			cfgMgr.updateLastGameList(game2);
			assertEquals(3, cfgMgr.getLastGameList().size());
			assertEquals("c:\\temps\\tata.fsg", (String) cfgMgr.getLastGameList().get(0));
			assertEquals("c:\\temps\\tutu.fsg", (String) cfgMgr.getLastGameList().get(1));
			assertEquals("c:\\temps\\toto.fsg", (String) cfgMgr.getLastGameList().get(2));

			final File game4 = new File("c:\\temps\\titi.fsg");
			cfgMgr.updateLastGameList(game4);
			assertEquals(4, cfgMgr.getLastGameList().size());
			assertEquals("c:\\temps\\titi.fsg", (String) cfgMgr.getLastGameList().get(0));
			assertEquals("c:\\temps\\tata.fsg", (String) cfgMgr.getLastGameList().get(1));
			assertEquals("c:\\temps\\tutu.fsg", (String) cfgMgr.getLastGameList().get(2));
			assertEquals("c:\\temps\\toto.fsg", (String) cfgMgr.getLastGameList().get(3));

			final File game5 = new File("c:\\temps\\tete.fsg");
			cfgMgr.updateLastGameList(game5);
			assertEquals(4, cfgMgr.getLastGameList().size());
			assertEquals("c:\\temps\\tete.fsg", (String) cfgMgr.getLastGameList().get(0));
			assertEquals("c:\\temps\\titi.fsg", (String) cfgMgr.getLastGameList().get(1));
			assertEquals("c:\\temps\\tata.fsg", (String) cfgMgr.getLastGameList().get(2));
			assertEquals("c:\\temps\\tutu.fsg", (String) cfgMgr.getLastGameList().get(3));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_updateLastGameList_nullParam()
	{
		try
		{
			final ApplicationConfigurationMgr cfgMgr = ApplicationConfigurationMgr.getInstance();

			try
			{
				cfgMgr.updateLastGameList(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_removeGameDirFromLastGameList_valid()
	{
		try
		{
			final ApplicationConfigurationMgr cfgMgr = ApplicationConfigurationMgr.getCreateInstance(FileMgr
					.getCurrentDir());
			cfgMgr.setLastGameList(new ArrayList<String>());
			cfgMgr.updateLastGameList(new File("dir1.fsg"));
			cfgMgr.updateLastGameList(new File("dir2.fsg"));
			cfgMgr.updateLastGameList(new File("dir3.fsg"));
			assertEquals(3, cfgMgr.getLastGameList().size());
			assertEquals(FileMgr.getCurrentDir().getAbsolutePath() + "\\dir3.fsg", cfgMgr.getLastGameList().get(0));
			assertEquals(FileMgr.getCurrentDir().getAbsolutePath() + "\\dir2.fsg", cfgMgr.getLastGameList().get(1));
			assertEquals(FileMgr.getCurrentDir().getAbsolutePath() + "\\dir1.fsg", cfgMgr.getLastGameList().get(2));

			cfgMgr.removeGameFromLastGameList(new File("dir2.fsg"));
			assertEquals(2, cfgMgr.getLastGameList().size());
			assertEquals(FileMgr.getCurrentDir().getAbsolutePath() + "\\dir3.fsg", cfgMgr.getLastGameList().get(0));
			assertEquals(FileMgr.getCurrentDir().getAbsolutePath() + "\\dir1.fsg", cfgMgr.getLastGameList().get(1));

			cfgMgr.removeGameFromLastGameList(new File("dir3.fsg"));
			assertEquals(1, cfgMgr.getLastGameList().size());
			assertEquals(FileMgr.getCurrentDir().getAbsolutePath() + "\\dir1.fsg", cfgMgr.getLastGameList().get(0));

			cfgMgr.removeGameFromLastGameList(new File("dir1.fsg"));
			assertEquals(0, cfgMgr.getLastGameList().size());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_removeGameDirFromLastGameList_nullParam()
	{
		try
		{
			final ApplicationConfigurationMgr cfgMgr = ApplicationConfigurationMgr.getInstance();
			cfgMgr.setLastGameList(new ArrayList<String>());
			try
			{
				cfgMgr.removeGameFromLastGameList(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
