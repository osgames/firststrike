/*
 * Created on Aug 27, 2005
 */
package org.jocelyn_chaumel.firststrike.core.configuration;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class ApplicationConfigurationTest extends JCTestCase
{
	private File _oldCurrentFile = null;

	public ApplicationConfigurationTest(final String aName)
	{
		super(aName);
		saveOldCurrentDir();
	}

	private void saveOldCurrentDir()
	{
		_oldCurrentFile = FileMgr.getCurrentDir();
	}

	private void restoreOldCurrentDir()
	{
		FileMgr.setCurrentDir(_oldCurrentFile);
	}

	public final void test_set_and_getMapDirectory_all()
	{
		final File currentDir = new File("c:\\temp");
		final File mapDir1 = new File("c:\\cartes");
		final File mapDir2 = new File(currentDir, "cartes");
		try
		{
			FileMgr.setCurrentDir(currentDir);
			final URL url = ClassLoader.getSystemResource(ApplicationConfiguration.APPLICATION_PROPERTIES_RESOURCE);
			final ApplicationConfiguration appCfg = new ApplicationConfiguration(url);
			FileMgr.forceCreateDirIfNotPresent(mapDir1);
			FileMgr.forceCreateDirIfNotPresent(mapDir2);

			appCfg.setMapDir(mapDir1);
			assertEquals(mapDir1, appCfg.getMapDir());

			appCfg.setMapDir(mapDir2);
			assertEquals(mapDir2, appCfg.getMapDir());

			try
			{
				appCfg.setMapDir(null);
				fail("Null Param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				appCfg.setMapDir(new File("c:\\bibi"));
				fail("Invalid directory has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			restoreOldCurrentDir();
			try
			{
				FileMgr.forceDeleteCascadeIfPresent(mapDir1);
				FileMgr.forceDeleteCascadeIfPresent(mapDir2);
			} catch (IOException ex)
			{
				fail(ex);
			}
		}
	}

	public final void test_get_and_setGameDir_all()
	{
		final File currentDir = new File("c:\\temp");
		final File mapDir1 = new File("c:\\games");
		final File mapDir2 = new File(currentDir, "games");
		try
		{
			currentDir.mkdirs();
			FileMgr.setCurrentDir(currentDir);
			final URL url = ClassLoader.getSystemResource(ApplicationConfiguration.APPLICATION_PROPERTIES_RESOURCE);
			final ApplicationConfiguration appCfg = new ApplicationConfiguration(url);
			FileMgr.forceCreateDirIfNotPresent(mapDir1);
			FileMgr.forceCreateDirIfNotPresent(mapDir2);

			appCfg.setGameDir(mapDir1);
			assertEquals(mapDir1, appCfg.getGameDir());

			appCfg.setGameDir(mapDir2);
			assertEquals(mapDir2, appCfg.getGameDir());

			try
			{
				appCfg.setGameDir(null);
				fail("Null Param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				appCfg.setGameDir(new File("c:\\bibi"));
				fail("Invalid directory has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			restoreOldCurrentDir();
			try
			{
				FileMgr.forceDeleteCascadeIfPresent(mapDir1);
				FileMgr.forceDeleteCascadeIfPresent(mapDir2);
			} catch (IOException ex)
			{
				fail(ex);
			}
		}
	}

	public final void test_save_and_saveAs_valid()
	{
		final File currentDir = new File("c:\\temp");
		final File appProperties = new File(currentDir, ApplicationConfiguration.APPLICATION_PROPERTIES_FILENAME);
		FileMgr.setCurrentDir(currentDir);
		try
		{
			URL url = ClassLoader.getSystemResource(ApplicationConfiguration.APPLICATION_PROPERTIES_RESOURCE);
			ApplicationConfiguration conf = new ApplicationConfiguration(url);

			try
			{
				conf.save();
				fail("Conf loaded from resource included in a jar file.  It is impossible to save the file");
			} catch (UnsupportedOperationException ex)
			{
				// NOP
			}
			assertFalse(appProperties.isFile());
			assertTrue(conf.isSaveDisabled());
			assertFalse(conf.isModified());
			assertTrue(conf.isSaveDisabled());
			String oldVersion = conf.getFSVersion();

			conf.saveAs(appProperties);
			assertFalse(conf.isSaveDisabled());
			assertFalse(conf.isModified());
			assertTrue(appProperties.isFile());
			String newVersion = conf.getFSVersion();
			assertEquals(oldVersion, newVersion);

			try
			{
				conf.saveAs(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				conf.saveAs(new File("c:\\temp"));
				fail("Null file has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (IOException ex)
		{
			fail(ex);
		} finally
		{
			restoreOldCurrentDir();
			try
			{
				FileMgr.forceDeleteIfPresent(appProperties);
			} catch (IOException ex)
			{
				fail(ex);
			}
		}
	}

	public final void test_set_and_getLast_valid()
	{
		final File currentDir = new File("c:\\Temp");
		ArrayList<String> listeGame = new ArrayList<String>();
		FileMgr.setCurrentDir(currentDir);
		try
		{
			final URL url = ClassLoader.getSystemResource(ApplicationConfiguration.APPLICATION_PROPERTIES_RESOURCE);
			final ApplicationConfiguration appCfg = new ApplicationConfiguration(url);
			listeGame.add("c:\\temp\\rep1.fsg");
			listeGame.add("rep2.fsg");
			listeGame.add("rep3.fsg");

			appCfg.setLastGameList(listeGame);
			listeGame = appCfg.getLastGameList();
			assertEquals("c:\\temp\\rep1.fsg", listeGame.get(0));
			assertEquals("rep2.fsg", listeGame.get(1));
			assertEquals("rep3.fsg", listeGame.get(2));

			appCfg.setLastGameList(new ArrayList<String>());
			assertEquals(0, appCfg.getLastGameList().size());

			try
			{
				appCfg.setLastGameList(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			restoreOldCurrentDir();
		}
	}
}
