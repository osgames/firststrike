package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.FSTestCase;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;

public class MoveCostFactoryTest extends FSTestCase
{

	public MoveCostFactoryTest(final String aName)
	{
		super(aName);
	}

	public void test_getMoveCostInstance_nullParam()
	{
		try
		{
			MoveCostFactory.getMoveCostInstance(null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}

	public void test_getMoveCostInstance_valid()
	{
		assertTrue(MoveCostFactory.getMoveCostInstance(UnitTypeCst.FIGHTER) instanceof AirPlaneMoveCost);
		assertTrue(MoveCostFactory.getMoveCostInstance(UnitTypeCst.TANK) instanceof GroundMoveCost);
	}

	public void test_getMoveCostInstance_invalid()
	{
		try
		{
			MoveCostFactory.getMoveCostInstance(UnitTypeCst.NULL_TYPE);
			fail("Invalid unit type has been accepted");

		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}
}
