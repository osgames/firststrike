/*
 * FSMapCellsTest.java JUnit based test Created on 28 f�vrier 2003, 18:31
 */

package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

/**
 * @author Jocelyn Chaumel
 */
public class FSMapCellsTest extends MapTestCase
{
	public FSMapCellsTest(final String aName)
	{
		super(aName);
	}

	/***********************************************************************************************
	 * D�but des tests
	 */

	// ----------------------------------------
	public final void test_CntrFSMapCells_nullParam_1()
	{
		try
		{
			new FSMapCells(null, null, false);
			fail("Invalid map width has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_GetCellAt_InvalidX_lower()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.getCellAt(-1, 1);
				fail("Invalid X param has been accecpted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_GetCellAt_InvalidX_upper()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.getCellAt(STANDARD_MAP_1[0].length, 1);
				fail("Invalid X param has been accecpted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_GetCellAt_InvalidY_lower()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.getCellAt(1, -1);
				fail("Invalid Y param has been accecpted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_GetCellAt_InvalidY_upper()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.getCellAt(1, STANDARD_MAP_1.length);
				fail("Invalid Y param has been accecpted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_GetCellAt_valid()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			Cell cell = mapCells.getCellAt(0, 0);
			assertEquals("Different cell type.", cell.getCellType(), CellTypeCst.SEA);
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_areaMapName_valid()
	{
		try
		{
			Cell[][] cellTypeArray = buildCellArray(STANDARD_MAP_1);
			final AreaNameMap areaNameMap = new AreaNameMap();
			FSMapCells mapCells = null;
			Cell cell = null;

			//areaNameMap.put(new Coord(0, 0), "Invalid coord");
			mapCells = new FSMapCells(cellTypeArray, areaNameMap, true);
			cell = mapCells.getCellAt(0, 0);
			assertEquals(1, cell.getAreaList().size());
			assertFalse(cell.getAreaList().get(0).hasAreaName());
			cell = mapCells.getCellAt(3, 3);
			assertFalse(cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA).hasAreaName());

			cellTypeArray = buildCellArray(STANDARD_MAP_1);
			areaNameMap.put(new Coord(2, 1), "Valid coord");
			mapCells = new FSMapCells(cellTypeArray, areaNameMap, true);
			cell = mapCells.getCellAt(0, 0);
			
			assertEquals(1, cell.getAreaList().size());
			assertFalse(cell.getAreaList().get(0).hasAreaName());
			cell = mapCells.getCellAt(3, 3);
			assertTrue(cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA).hasAreaName());
			assertEquals("Valid coord", cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA).getInternalAreaName());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_GetCellAt_NullCoord()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.getCellAt(null);
				fail("Null param has been accecpted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_NullParam1()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.isSameArea(null, new Coord(1, 1), AreaTypeCst.GROUND_AREA);
				fail("Null param has been accecpted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_NullParam2()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.isSameArea(new Coord(1, 1), null, AreaTypeCst.GROUND_AREA);
				fail("Null param has been accecpted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_NullParam3()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.isSameArea(new Coord(1, 1), new Coord(2, 1), null);
				fail("Null param has been accecpted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_InvalidFirstX_1()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.isSameArea(new Coord(-1, 1), new Coord(1, 1), AreaTypeCst.GROUND_AREA);
				fail("Invalid coord has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_InvalidFirstX_2()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.isSameArea(new Coord(100, 1), new Coord(1, 1), AreaTypeCst.GROUND_AREA);
				fail("Invalid coord has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_InvalidSecondX_1()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.isSameArea(new Coord(1, 1), new Coord(-1, 1), AreaTypeCst.GROUND_AREA);
				fail("Invalid coord has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_InvalidSecondX_2()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.isSameArea(new Coord(1, 1), new Coord(100, 1), AreaTypeCst.GROUND_AREA);
				fail("Invalid coord has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_InvalidFirstY_1()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.isSameArea(new Coord(1, -1), new Coord(1, 1), AreaTypeCst.GROUND_AREA);
				fail("Invalid coord has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_InvalidFirstY_2()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.isSameArea(new Coord(1, 100), new Coord(1, 1), AreaTypeCst.GROUND_AREA);
				fail("Invalid coord has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_InvalidSecondY_1()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.isSameArea(new Coord(1, 1), new Coord(1, -1), AreaTypeCst.GROUND_AREA);
				fail("Invalid coord has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_InvalidSecondY_2()
	{
		try
		{
			final FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			try
			{
				mapCells.isSameArea(new Coord(1, 1), new Coord(1, 100), AreaTypeCst.GROUND_AREA);
				fail("Invalid coord has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_IsSameCountry_valid1()
	{
		try
		{
			FSMapCells map = buildFSMap(STANDARD_MAP_1).getFSMapCells();

			assertTrue(map.isSameArea(new Coord(1, 1), new Coord(1, 1), AreaTypeCst.GROUND_AREA));
			assertTrue(map.isSameArea(new Coord(1, 1), new Coord(1, 2), AreaTypeCst.GROUND_AREA));
			assertTrue(map.isSameArea(new Coord(1, 1), new Coord(2, 1), AreaTypeCst.GROUND_AREA));
			assertTrue(map.isSameArea(new Coord(1, 1), new Coord(2, 2), AreaTypeCst.GROUND_AREA));
			assertTrue(map.isSameArea(new Coord(1, 1), new Coord(3, 1), AreaTypeCst.GROUND_AREA));
			assertTrue(map.isSameArea(new Coord(1, 1), new Coord(3, 2), AreaTypeCst.GROUND_AREA));
			assertTrue(map.isSameArea(new Coord(1, 1), new Coord(4, 4), AreaTypeCst.SEA_AREA));
			assertFalse(map.isSameArea(new Coord(1, 1), new Coord(4, 3), AreaTypeCst.SEA_AREA));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getWatherCellNearToGroundCell_valid()
	{
		try
		{
			FSMapCells mapCells = buildFSMap(STANDARD_MAP_1).getFSMapCells();
			CoordSet coordSet = mapCells.getWatherCellNearToGroundCell(new Coord(1, 1));
			assertEquals(5, coordSet.size());
			assertTrue(coordSet.contains(new Coord(0, 2)));
			assertTrue(coordSet.contains(new Coord(0, 1)));
			assertTrue(coordSet.contains(new Coord(0, 0)));
			assertTrue(coordSet.contains(new Coord(1, 0)));
			assertTrue(coordSet.contains(new Coord(2, 0)));

			mapCells = buildFSMap(STANDARD_MAP_7).getFSMapCells();
			coordSet = mapCells.getWatherCellNearToGroundCell(new Coord(2, 3));
			assertEquals(3, coordSet.size());
			assertTrue(coordSet.contains(new Coord(1, 2)));
			assertTrue(coordSet.contains(new Coord(2, 2)));
			assertTrue(coordSet.contains(new Coord(3, 2)));

			coordSet = mapCells.getWatherCellNearToGroundCell(new Coord(0, 5));
			assertEquals(2, coordSet.size());
			assertTrue(coordSet.contains(new Coord(0, 4)));
			assertTrue(coordSet.contains(new Coord(1, 5)));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}