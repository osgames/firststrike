package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class CellSetTest extends MapTestCase
{

	public CellSetTest(final String p_name)
	{
		super(p_name);
	}

	public void test_get_allMethods_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final CellSet cellSet = new CellSet();

			assertEquals(0, cellSet.getGroundCellNearToSea().size());
			assertEquals(0, cellSet.getCoordSet().size());
			assertNull(cellSet.getCellByCoord(new Coord(1, 1)));
			cellSet.add(map.getCellAt(0, 1));
			cellSet.add(map.getCellAt(1, 2));
			cellSet.add(map.getCellAt(1, 3));
			cellSet.add(map.getCellAt(2, 2));

			assertNull(cellSet.getCellByCoord(new Coord(1, 1)));
			assertEquals(map.getCellAt(0, 1), cellSet.getCellByCoord(new Coord(0, 1)));
			assertEquals(2, cellSet.getGroundCellNearToSea().size());
			assertTrue(cellSet.getGroundCellNearToSea().contains(map.getCellAt(1, 2)));
			assertTrue(cellSet.getGroundCellNearToSea().contains(map.getCellAt(1, 3)));

			assertTrue(cellSet.getCoordSet().contains(new Coord(0, 1)));
			assertTrue(cellSet.getCoordSet().contains(new Coord(1, 2)));
			assertTrue(cellSet.getCoordSet().contains(new Coord(1, 3)));
			assertTrue(cellSet.getCoordSet().contains(new Coord(2, 2)));
			try
			{
				cellSet.getCellByCoord(null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
