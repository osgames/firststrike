/*
 * FSMapMgrTest.java JUnit based test
 * 
 * Created on 6 juin 2003, 14:05
 */

package org.jocelyn_chaumel.firststrike.core.map;

import java.io.File;
import java.util.ArrayList;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.map.path.PathMgr;
import org.jocelyn_chaumel.firststrike.core.map.scenario.MapScenario;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;

/**
 * 
 * @author Jocelyn Chaumel
 */
public class FSMapMgrTest extends MapTestCase
{
	public FSMapMgrTest(final String aName)
	{
		super(aName);
	}

	public final void test_Cntr_nullParam()
	{
		try
		{
			new FSMapMgr(null);
			fail("Null ref has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_LoadMap_nullParam()
	{
		try
		{
			FSMapMgr.loadMap(null, true);
			fail("Null ref has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_LoadMap_invalidFile()
	{
		try
		{
			FSMapMgr.loadMap(new File("c:\\temp\\joss.fsm"), true);
			fail("Invalid file has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_SaveMap_nullParam_1()
	{
		try
		{
			FSMapMgr.saveMap(null, new File("c:\\temp\\joss.fsm"));
			fail("Null ref has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_SaveMap_nullParam_2()
	{
		try
		{
			FSMapMgr.saveMap(buildFSMap(super.STANDARD_MAP_1), null);
			fail("Null ref has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_SaveMap_fileAlreadyExists() throws InvalidMapException, InvalidConstantValueException
	{
		FSMap map = buildFSMap(super.STANDARD_MAP_1);
		File directory = new File("c:\\dirToDelete");
		try
		{
			assertTrue(directory.mkdir());
			FSMapMgr.saveMap(map, directory);

			FSMapMgr.saveMap(map, directory);
			fail("Invalid file has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			try
			{
				FileMgr.forceDeleteCascade(directory);
			} catch (Exception ex)
			{
				// NOP
			}
		}
	}

	public final void test_SaveAndLoadMapWithoutName_valid() throws InvalidConstantValueException, InvalidMapException
	{
		File directory = new File("c:\\dirToDelete");
		try
		{
			FSMap map = buildFSMap(super.STANDARD_MAP_1);
			File mapFile = new File(directory, map.getName());

			assertTrue(directory.mkdir());
			FSMapMgr.saveMap(map, mapFile);

			FSMap loadedMap = FSMapMgr.loadMap(mapFile, true);

			assertEquals(loadedMap.getName(), "map test");
			assertEquals(loadedMap.getDescription(), "no description");
			final int maxY = loadedMap.getHeight();
			final int maxX = loadedMap.getWidth();
			assertEquals(maxY, 6);
			assertEquals(maxX, 6);
			FSMapCells mapCells = loadedMap.getFSMapCells();
			CellTypeCst currentCellType = null;
			Cell currentCell = null;
			for (int y = 0; y < maxY; y++)
			{
				for (int x = 0; x < maxX; x++)
				{
					currentCellType = CellTypeCst.getInstante(super.STANDARD_MAP_1[y][x]);
					currentCell = mapCells.getCellAt(x, y);
					assertEquals(currentCell.getCellType(), currentCellType);
				}
			}
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			try
			{
				FileMgr.forceDeleteCascade(directory);
			} catch (Exception ex)
			{
				// NOP
			}
		}
	}

	public final void test_SaveAndLoadMap_valid() throws InvalidConstantValueException, InvalidMapException
	{
		FSMap map = buildFSMap(super.STANDARD_MAP_1);
		File directory = new File("c:\\dirToDelete");
		String filename = "mapToDelete.FSMAP";
		File mapFile = new File(directory, filename);
		try
		{
			assertTrue(directory.mkdir());
			FSMapMgr.saveMap(map, mapFile);
			FSMap loadedMap = FSMapMgr.loadMap(mapFile, true);

			assertEquals("mapToDelete", loadedMap.getName());
			assertEquals("no description", loadedMap.getDescription());
			final int maxY = loadedMap.getHeight();
			final int maxX = loadedMap.getWidth();
			assertEquals(maxY, 6);
			assertEquals(maxX, 6);
			FSMapCells mapCells = loadedMap.getFSMapCells();
			CellTypeCst currentCellType = null;
			Cell currentCell = null;
			for (int y = 0; y < maxY; y++)
			{
				for (int x = 0; x < maxX; x++)
				{
					currentCellType = CellTypeCst.getInstante(super.STANDARD_MAP_1[y][x]);
					currentCell = mapCells.getCellAt(x, y);
					assertEquals(currentCell.getCellType(), currentCellType);
				}
			}
		} catch (Exception ex)
		{
			fail(ex);
		} finally
		{
			try
			{
				FileMgr.forceDeleteCascade(directory);
			} catch (Exception ex)
			{
				// NOP
			}
		}
	}

	public final void test_Create_nullParam_1()
	{
		try
		{
			FSMapMgr.create(null, buildCellArray(MAP_1), new AreaNameMap(), true);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Create_nullParam_2()
	{
		try
		{
			FSMapInfo mapInfo = new FSMapInfo("map name", null, "map description", FSMapMgr.VERSION, 20, 20);

			FSMapMgr.create(mapInfo, null, new AreaNameMap(), true);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Create_nullParam_3()
	{
		try
		{
			FSMapInfo mapInfo = new FSMapInfo("map name", null, "map description", FSMapMgr.VERSION, 20, 20);

			Cell[][] cellArray = buildCellArray(MAP_1);
			cellArray[1][1] = null;
			FSMapMgr.create(mapInfo, cellArray, new AreaNameMap(), true);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Create_valid()
	{
		try
		{
			FSMapInfo mapInfo = new FSMapInfo("map name", null, "map description", FSMapMgr.VERSION, 20, 20);

			Cell[][] cellArray = buildCellArray(MAP_1);
			assertNotNull(FSMapMgr.create(mapInfo, cellArray, new AreaNameMap(), true));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getPathDistance_nullParam_1()
	{
		try
		{
			final FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			try
			{
				mapMgr.getPathDistance(null, MoveCostFactory.GROUND_MOVE_COST);
				fail("Null param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getPathDistance_nullParam_2()
	{
		try
		{
			final FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			final CoordList coordList = new CoordList();
			coordList.add(new Coord(1, 1));
			final Path path = new Path(coordList);
			try
			{
				mapMgr.getPathDistance(path, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getPathDistance_invalidPath()
	{
		try
		{
			FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			CoordList coordList = new CoordList();
			coordList.add(new Coord(1, 1));
			Path path = new Path(coordList);
			path.getNextStep();
			try
			{
				mapMgr.getPathDistance(path, MoveCostFactory.GROUND_MOVE_COST);
				fail("Empty path has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getPathDistance_valid()
	{
		try
		{
			final FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			final CoordList coordList = new CoordList();
			final Coord start = new Coord(1, 1);
			coordList.add(start);
			Path path = new Path(coordList);
			assertEquals(25, mapMgr.getPathDistance(path, MoveCostFactory.GROUND_MOVE_COST));

			coordList.add(new Coord(2, 2));
			path = new Path(coordList);
			assertEquals(58, mapMgr.getPathDistance(path, MoveCostFactory.GROUND_MOVE_COST));

			final TransportShip transport = new TransportShip(start, COMPUTER_PLAYER_1);

			path = mapMgr.getPath(new Coord(4, 4), new CoordSet(), transport, false);
			assertEquals(231, mapMgr.getPathDistance(path, transport.getMoveCost()));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getAreaIdAt_nullParam_1()
	{
		try
		{
			FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			mapMgr.getAreaAt(null, AreaTypeCst.GROUND_AREA);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getAreaIdAt_nullParam_2()
	{
		try
		{
			FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			mapMgr.getAreaAt(new Coord(1, 1), null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getAreaIdAt_invalidCoord_1()
	{
		try
		{
			FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			mapMgr.getAreaAt(new Coord(10, 1), AreaTypeCst.GROUND_AREA);
			fail("Invalid coord has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getAreaIdAt_invalidCoord_2()
	{
		try
		{
			FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			mapMgr.getAreaAt(new Coord(1, 10), AreaTypeCst.GROUND_AREA);
			fail("Invalid coord has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getAreaIdAt_invalidCoord_3()
	{
		try
		{
			FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			mapMgr.getAreaAt(new Coord(-1, 1), AreaTypeCst.GROUND_AREA);
			fail("Invalid coord has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getAreaIdAt_invalidCoord_4()
	{
		try
		{
			FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			mapMgr.getAreaAt(new Coord(1, -1), AreaTypeCst.GROUND_AREA);
			fail("Invalid coord has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_isAccessibleCoord_valid()
	{
		try
		{
			final FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			final Coord start = new Coord(1, 1);
			final TransportShip transport = new TransportShip(start, COMPUTER_PLAYER_1);
			final CoordSet coordSet = new CoordSet();
			assertTrue(mapMgr.isAccessibleCoord(transport, new Coord(4, 4), coordSet, false));
			assertFalse(mapMgr.isAccessibleCoord(transport, new Coord(3, 3), coordSet, false));
			assertTrue(mapMgr.isAccessibleCoord(transport, new Coord(3, 3), coordSet, true));

			coordSet.add(new Coord(5, 3));
			assertTrue(mapMgr.isAccessibleCoord(transport, new Coord(4, 4), coordSet, false));

			coordSet.add(new Coord(3, 5));
			assertFalse(mapMgr.isAccessibleCoord(transport, new Coord(4, 4), coordSet, false));

			final Fighter fighter = new Fighter(start, COMPUTER_PLAYER_1);
			final Tank tank = new Tank(start, COMPUTER_PLAYER_1);
			assertTrue(mapMgr.isAccessibleCoord(tank, new Coord(4, 4), coordSet, false));
			assertFalse(mapMgr.isAccessibleCoord(tank, new Coord(5, 5), coordSet, false));
			assertTrue(mapMgr.isAccessibleCoord(fighter, new Coord(4, 4), coordSet, false));
			assertTrue(mapMgr.isAccessibleCoord(fighter, new Coord(5, 5), coordSet, false));

			coordSet.add(new Coord(3, 3));
			coordSet.add(new Coord(4, 3));
			coordSet.add(new Coord(3, 4));
			assertFalse(mapMgr.isAccessibleCoord(tank, new Coord(4, 4), coordSet, false));
			assertFalse(mapMgr.isAccessibleCoord(fighter, new Coord(4, 4), coordSet, false));

			try
			{
				assertTrue(mapMgr.isAccessibleCoord(null, new Coord(4, 4), coordSet, false));
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				assertTrue(mapMgr.isAccessibleCoord(transport, null, coordSet, false));
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				assertTrue(mapMgr.isAccessibleCoord(transport, new Coord(4, 4), null, false));
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				assertTrue(mapMgr.isAccessibleCoord(transport, start, new CoordSet(), false));
				fail("Invalid destination param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getPath_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final PathMgr pathMgr = mapMgr.getPathMgr();
			final Coord start = new Coord(1, 1);
			final Fighter fighter = new Fighter(start, COMPUTER_PLAYER_1);
			final TransportShip transport = new TransportShip(start, COMPUTER_PLAYER_1);
			final CoordSet coordSet = new CoordSet();
			Path path = null;
			Path expectedPath = null;

			path = mapMgr.getPath(new Coord(4, 4), coordSet, transport, false);
			assertEquals(new Coord(2, 0), path.getNextStep());
			assertEquals(new Coord(3, 0), path.getNextStep());
			assertEquals(new Coord(4, 0), path.getNextStep());
			assertEquals(new Coord(5, 1), path.getNextStep());
			assertEquals(new Coord(5, 2), path.getNextStep());
			assertEquals(new Coord(5, 3), path.getNextStep());
			assertEquals(new Coord(4, 4), path.getNextStep());

			path = mapMgr.getPath(new Coord(4, 4), coordSet, fighter, false);
			assertEquals(new Coord(2, 2), path.getNextStep());
			assertEquals(new Coord(3, 3), path.getNextStep());
			assertEquals(new Coord(4, 4), path.getNextStep());

			coordSet.add(new Coord(5, 3));
			path = mapMgr.getPath(new Coord(4, 4), coordSet, transport, false);
			assertEquals(new Coord(0, 2), path.getNextStep());
			assertEquals(new Coord(0, 3), path.getNextStep());
			assertEquals(new Coord(0, 4), path.getNextStep());
			assertEquals(new Coord(1, 5), path.getNextStep());
			assertEquals(new Coord(2, 5), path.getNextStep());
			assertEquals(new Coord(3, 5), path.getNextStep());
			assertEquals(new Coord(4, 4), path.getNextStep());

			path = mapMgr.getPath(new Coord(4, 4), coordSet, fighter, false);
			assertEquals(new Coord(2, 2), path.getNextStep());
			assertEquals(new Coord(3, 3), path.getNextStep());
			assertEquals(new Coord(4, 4), path.getNextStep());

			final Tank tank = new Tank(start, COMPUTER_PLAYER_1);
			path = mapMgr.getPath(new Coord(4, 4), coordSet, tank, false);
			assertEquals(new Coord(2, 2), path.getNextStep());
			assertEquals(new Coord(3, 3), path.getNextStep());
			assertEquals(new Coord(4, 4), path.getNextStep());

			path = mapMgr.getPath(new Coord(4, 4), coordSet, tank, true);

			assertEquals(new Coord(3, 3), path.getDestination());
			assertEquals(new Coord(2, 2), path.getNextStep());
			assertEquals(new Coord(3, 3), path.getNextStep());

			coordSet.add(new Coord(3, 3));
			path = mapMgr.getPath(new Coord(4, 4), coordSet, tank, false);
			expectedPath = pathMgr.getAreaPath(	start,
												new Coord(4, 4),
												tank.getMoveCost(),
												tank.getMove(),
												tank.getMovementCapacity(),
												coordSet,
												false);
			assertEquals(expectedPath, path);

			path = mapMgr.getPath(new Coord(4, 4), coordSet, fighter, false);
			expectedPath = pathMgr.getAirPath(start, new Coord(4, 4), 10, 10, COMPUTER_PLAYER_1, coordSet, false);
			assertEquals(expectedPath, path);

			coordSet.add(new Coord(4, 3));
			coordSet.add(new Coord(3, 4));
			path = mapMgr.getPath(new Coord(4, 4), coordSet, fighter, false);
			expectedPath = pathMgr.getAirPath(start, new Coord(4, 4), 10, 10, COMPUTER_PLAYER_1, coordSet, false);
			assertEquals(expectedPath, path);

			try
			{
				mapMgr.getPath(null, coordSet, fighter, true);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mapMgr.getPath(new Coord(5, 5), null, fighter, true);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mapMgr.getPath(new Coord(5, 5), coordSet, null, true);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mapMgr.getPath(fighter.getPosition(), coordSet, fighter, true);
				fail("Invalid destination coord param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			final Coord destination = new Coord(5, 5);
			coordSet.add(destination);
			try
			{
				mapMgr.getPath(destination, coordSet, fighter, false);
				fail("Invalid coordset param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_isAccessibleCell_2_params_all()
	{
		try
		{
			final FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_1));
			final Coord start = new Coord(1, 1);
			final Fighter fighter = new Fighter(start, COMPUTER_PLAYER_1);
			final TransportShip transport = new TransportShip(start, COMPUTER_PLAYER_1);
			final Tank tank = new Tank(start, COMPUTER_PLAYER_1);

			assertTrue(mapMgr.isAccessibleCell(new Coord(0, 0), fighter.getMoveCost()));
			assertTrue(mapMgr.isAccessibleCell(new Coord(0, 0), transport.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(new Coord(0, 0), tank.getMoveCost()));

			assertTrue(mapMgr.isAccessibleCell(new Coord(1, 1), fighter.getMoveCost()));
			assertTrue(mapMgr.isAccessibleCell(new Coord(1, 1), transport.getMoveCost()));
			assertTrue(mapMgr.isAccessibleCell(new Coord(1, 1), tank.getMoveCost()));

			assertTrue(mapMgr.isAccessibleCell(new Coord(2, 2), fighter.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(new Coord(2, 2), transport.getMoveCost()));
			assertTrue(mapMgr.isAccessibleCell(new Coord(2, 2), tank.getMoveCost()));

			try
			{
				assertTrue(mapMgr.isAccessibleCell(null, fighter.getMoveCost()));
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				assertTrue(mapMgr.isAccessibleCell(new Coord(2, 2), null));
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_isAccessibleCell_3_params_all()
	{
		try
		{
			final FSMapMgr mapMgr = new FSMapMgr(buildFSMap(STANDARD_MAP_6));
			final Coord start = new Coord(1, 1);
			final Fighter fighter = new Fighter(start, COMPUTER_PLAYER_1);
			final TransportShip transport = new TransportShip(start, COMPUTER_PLAYER_1);
			final Tank tank = new Tank(start, COMPUTER_PLAYER_1);

			assertTrue(mapMgr.isAccessibleCell(fighter.getMovementCapacity(), new Coord(0, 0), fighter.getMoveCost()));
			assertTrue(mapMgr.isAccessibleCell(transport.getMovementCapacity(), new Coord(0, 0), transport.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(0, new Coord(0, 0), fighter.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(0, new Coord(0, 0), transport.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(tank.getMovementCapacity(), new Coord(0, 0), tank.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(0, new Coord(0, 0), tank.getMoveCost()));

			assertTrue(mapMgr.isAccessibleCell(fighter.getMovementCapacity(), new Coord(1, 1), fighter.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(0, new Coord(1, 1), fighter.getMoveCost()));
			assertTrue(mapMgr.isAccessibleCell(transport.getMovementCapacity(), new Coord(1, 1), transport.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(0, new Coord(1, 1), transport.getMoveCost()));
			assertTrue(mapMgr.isAccessibleCell(tank.getMovementCapacity(), new Coord(1, 1), tank.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(0, new Coord(1, 1), tank.getMoveCost()));

			assertTrue(mapMgr.isAccessibleCell(fighter.getMovementCapacity(), new Coord(2, 2), fighter.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(0, new Coord(2, 2), fighter.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(transport.getMovementCapacity(), new Coord(2, 2), transport.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(0, new Coord(2, 2), transport.getMoveCost()));
			assertTrue(mapMgr.isAccessibleCell(tank.getMovementCapacity(), new Coord(2, 2), tank.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(0, new Coord(2, 2), tank.getMoveCost()));

			assertTrue(mapMgr.isAccessibleCell(tank.getMovementCapacity(), new Coord(0, 1), tank.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(10, new Coord(0, 1), tank.getMoveCost()));
			assertFalse(mapMgr.isAccessibleCell(0, new Coord(0, 1), tank.getMoveCost()));

			try
			{
				assertTrue(mapMgr.isAccessibleCell(-1, new Coord(1, 1), fighter.getMoveCost()));
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				assertTrue(mapMgr.isAccessibleCell(1, null, fighter.getMoveCost()));
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				assertTrue(mapMgr.isAccessibleCell(1, new Coord(2, 2), null));
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_extractMaps_all()
	{
		try
		{
			FSMapMgr.extractMaps();
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_retrieveScenarioList_all()
	{
		try
		{
			final File mapDir = new File(ApplicationConfigurationMgr.getCreateInstance(FileMgr.getCurrentDir())
					.getMapDir(), "map 6x6 - Test");
			final ArrayList<MapScenario> mapScenarioList = FSMapMgr.retrieveScenarioList(mapDir);
			assertEquals(2, mapScenarioList.size());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}