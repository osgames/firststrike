/*
 * CountryMgrTest.java JUnit based test Created on 10 mars 2003, 19:03
 */

package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.map.cst.ValidationResult;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

/**
 * @author Jocelyn Chaumel
 */
public class AreaBuilderTest extends MapTestCase
{

	public AreaBuilderTest(final String aName)
	{
		super(aName);
	}

	private boolean isEquals(final int[][] p_expectedArray, final Cell[][] p_cellsArray, final AreaTypeCst p_areaType)
	{

		if (p_expectedArray == null && p_cellsArray == null)
		{
			return true;
		}

		if (p_expectedArray == null || p_cellsArray == null)
		{
			return false;
		}

		if (p_expectedArray.length != p_cellsArray.length)
		{
			return false;
		}

		Area currentArea = null;
		int currentId;
		for (int y = 0; y < p_expectedArray.length; y++)
		{
			if (p_expectedArray[y].length != p_cellsArray[y].length)
			{
				return false;
			}
			for (int x = 0; x < p_expectedArray[y].length; x++)
			{
				currentArea = p_cellsArray[y][x].getAreaList().getFirstAreaByType(p_areaType);
				if (currentArea == null)
				{
					currentId = 0;
				} else
				{
					currentId = p_cellsArray[y][x].getAreaList().getFirstAreaByType(p_areaType).getId().getId();
				}

				if (currentId != p_expectedArray[y][x])
				{
					throw new IllegalStateException("Invalid (" + x + "," + y + ") Expected:" + p_expectedArray[y][x]
							+ " Result:" + currentId);
				}
			}
		}

		return true;
	}

	/***********************************************************************************************
	 * D�but des tests
	 */

	// ----------------------------------------
	public final void testCntr_NullParam()
	{
		try
		{
			new AreaBuilder(null, true);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getCellsDefinition_noCity()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(AREA_BUILD_MAP_2[0]), true);
			try
			{
				areaBuilder.getCellsDefinition();
				fail("Invalid param has been accepted");
			} catch (InvalidMapException ex)
			{
				assertEquals(ValidationResult.NOT_ENOUGH_CITY, ex.getErrorDetail());
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getCellsDefinition_oneCity()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(MAP_UNIQUE_AREA_INVALID_3), true);
			try
			{
				areaBuilder.getCellsDefinition();
				fail("Invalid param has been accepted");
			} catch (InvalidMapException ex)
			{
				assertEquals(ValidationResult.NOT_ENOUGH_CITY, ex.getErrorDetail());
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getCellsDefinition_Valid_map3()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(AREA_BUILD_MAP_3[0]), true);
			final Cell[][] cells = areaBuilder.getCellsDefinition();
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_3[1], cells, AreaTypeCst.GROUND_AREA));
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_3[2], cells, AreaTypeCst.SEA_AREA));

			final CoordSet cityCoordSet = areaBuilder.getCityList().getCoordSet();
			assertEquals(8, cityCoordSet.size());
			assertTrue(cityCoordSet.contains(new Coord(0, 1)));
			assertTrue(cityCoordSet.contains(new Coord(1, 1)));
			assertTrue(cityCoordSet.contains(new Coord(2, 1)));
			assertTrue(cityCoordSet.contains(new Coord(3, 1)));

			assertTrue(cells[1][0].isNearToWather());
			assertTrue(cells[1][1].isNearToWather());
			assertTrue(cells[1][2].isNearToWather());
			assertTrue(cells[1][3].isNearToWather());

			assertTrue(cityCoordSet.contains(new Coord(0, 2)));
			assertTrue(cityCoordSet.contains(new Coord(1, 2)));
			assertTrue(cityCoordSet.contains(new Coord(2, 2)));
			assertTrue(cityCoordSet.contains(new Coord(3, 2)));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getCellsDefinition_Valid_map4()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(AREA_BUILD_MAP_4[0]), true);
			final Cell[][] cells = areaBuilder.getCellsDefinition();
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_4[1], cells, AreaTypeCst.GROUND_AREA));
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_4[2], cells, AreaTypeCst.SEA_AREA));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getCellsDefinition_Valid_map5()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(AREA_BUILD_MAP_5[0]), true);
			final Cell[][] cells = areaBuilder.getCellsDefinition();
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_5[1], cells, AreaTypeCst.GROUND_AREA));
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_5[2], cells, AreaTypeCst.SEA_AREA));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getCellsDefinition_Valid_map6()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(AREA_BUILD_MAP_6[0]), true);
			final Cell[][] cells = areaBuilder.getCellsDefinition();
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_6[1], cells, AreaTypeCst.GROUND_AREA));
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_6[2], cells, AreaTypeCst.SEA_AREA));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getCellsDefinition_Valid_map7()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(AREA_BUILD_MAP_7[0]), true);
			final Cell[][] cells = areaBuilder.getCellsDefinition();
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_7[1], cells, AreaTypeCst.GROUND_AREA));
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_7[2], cells, AreaTypeCst.SEA_AREA));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getCellsDefinition_Valid_map8()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(AREA_BUILD_MAP_8[0]), true);
			final Cell[][] cells = areaBuilder.getCellsDefinition();
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_8[1], cells, AreaTypeCst.GROUND_AREA));
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_8[2], cells, AreaTypeCst.SEA_AREA));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getCellsDefinition_Valid_map9()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(AREA_BUILD_MAP_9[0]), true);
			final Cell[][] cells = areaBuilder.getCellsDefinition();
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_9[1], cells, AreaTypeCst.GROUND_AREA));
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_9[2], cells, AreaTypeCst.SEA_AREA));
			assertEquals(3, areaBuilder.getCityList().size());
			assertEquals(3, areaBuilder.getAreaList().size());

			City city = areaBuilder.getCityList().getCityAtPosition(new Coord(1, 1));
			assertTrue(areaBuilder.getAreaList().contains(city.getGroundArea()));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getCellsDefinition_Valid_map10()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(AREA_BUILD_MAP_10[0]), true);
			final Cell[][] cells = areaBuilder.getCellsDefinition();
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_10[1], cells, AreaTypeCst.GROUND_AREA));
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_10[2], cells, AreaTypeCst.SEA_AREA));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getCellsDefinition_Valid_map11()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(AREA_BUILD_MAP_11[0]), true);
			final Cell[][] cells = areaBuilder.getCellsDefinition();
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_11[1], cells, AreaTypeCst.GROUND_AREA));
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_11[2], cells, AreaTypeCst.SEA_AREA));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// ----------------------------------------
	public final void test_getCellsDefinition_Valid_map12()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(AREA_BUILD_MAP_12[0]), true);
			final Cell[][] cells = areaBuilder.getCellsDefinition();
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_12[1], cells, AreaTypeCst.GROUND_AREA));
			assertTrue(	"At least one country ID in the map cells is invalid.",
						isEquals(AREA_BUILD_MAP_12[2], cells, AreaTypeCst.SEA_AREA));

			final CoordSet coordSet = areaBuilder.getCityList().getCoordSet();
			assertEquals(4, coordSet.size());
			assertTrue(coordSet.contains(new Coord(0, 0)));
			assertTrue(coordSet.contains(new Coord(0, 5)));
			assertTrue(coordSet.contains(new Coord(3, 5)));
			assertTrue(coordSet.contains(new Coord(4, 5)));

			// 1th Area
			assertTrue(cells[0][0].isNearToWather());
			assertTrue(cells[0][1].isNearToWather());
			assertTrue(cells[1][0].isNearToWather());

			// 2th Area
			assertTrue(cells[3][0].isNearToWather());
			assertTrue(cells[3][1].isNearToWather());
			assertTrue(cells[4][1].isNearToWather());
			assertTrue(cells[4][0].isNearToWather());
			assertTrue(cells[5][0].isNearToWather());

			// 3th Area
			assertTrue(cells[0][3].isNearToWather());
			assertTrue(cells[2][3].isNearToWather());
			assertTrue(cells[3][3].isNearToWather());
			assertTrue(cells[4][3].isNearToWather());
			assertTrue(cells[5][3].isNearToWather());
			assertTrue(cells[0][4].isNearToWather());
			assertTrue(cells[1][4].isNearToWather());
			assertTrue(cells[2][4].isNearToWather());
			assertFalse(cells[4][4].isNearToWather());
			assertFalse(cells[5][4].isNearToWather());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_validateCityAccessToWather_invalid_2()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(MAP_UNIQUE_AREA_INVALID_2), true);
			try
			{
				areaBuilder.getCellsDefinition();
				fail("Invalid operation has been executed");
			} catch (InvalidMapException ex)
			{
				assertEquals(ValidationResult.DIFFERENT_WATHER_CONTACT, ex.getErrorDetail());
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_validateCityAccessToWather_invalid_4()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(MAP_MULTI_AREA_INVALID_1), true);
			try
			{
				areaBuilder.getCellsDefinition();
				fail("Invalid operation has been executed");
			} catch (InvalidMapException ex)
			{
				assertEquals(ValidationResult.DIFFERENT_WATHER_CONTACT, ex.getErrorDetail());
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_validateAreaAccessToWather_invalid_4()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(MAP_MULTI_AREA_INVALID_2), true);
			try
			{
				areaBuilder.getCellsDefinition();
				fail("Invalid operation has been executed");
			} catch (InvalidMapException ex)
			{
				assertEquals(ValidationResult.AREA_WITHOUT_CONTACT, ex.getErrorDetail());
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getCellsDefinition_valid_2()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(MAP_UNIQUE_AREA_WITH_PORT), true);
			areaBuilder.getCellsDefinition();

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getCoordCellByArea_valid_map1()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(MAP_1), true);
			final Cell[][] cellArray = areaBuilder.getCellsDefinition();

			final Cell cell = cellArray[1][1];
			Area area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
			CoordSet coordSet = area.getCellSet().getCoordSet();
			assertEquals(5, coordSet.size());
			assertTrue(coordSet.contains(new Coord(1, 1)));
			assertTrue(coordSet.contains(new Coord(2, 1)));
			assertTrue(coordSet.contains(new Coord(3, 1)));
			assertTrue(coordSet.contains(new Coord(4, 1)));
			assertTrue(coordSet.contains(new Coord(5, 1)));

			area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.SEA_AREA);
			coordSet = area.getCellSet().getCoordSet();
			assertEquals(35, coordSet.size());
			assertTrue(coordSet.contains(new Coord(0, 0)));
			assertTrue(coordSet.contains(new Coord(1, 0)));
			assertTrue(coordSet.contains(new Coord(2, 0)));
			assertTrue(coordSet.contains(new Coord(3, 0)));
			assertTrue(coordSet.contains(new Coord(4, 0)));
			assertTrue(coordSet.contains(new Coord(5, 0)));
			assertTrue(coordSet.contains(new Coord(6, 0)));

			assertTrue(coordSet.contains(new Coord(0, 1)));
			assertTrue(coordSet.contains(new Coord(1, 1)));
			assertTrue(coordSet.contains(new Coord(2, 1)));
			assertTrue(coordSet.contains(new Coord(3, 1)));
			assertTrue(coordSet.contains(new Coord(4, 1)));
			assertTrue(coordSet.contains(new Coord(5, 1)));
			assertTrue(coordSet.contains(new Coord(6, 1)));

			assertTrue(coordSet.contains(new Coord(0, 2)));
			assertTrue(coordSet.contains(new Coord(1, 2)));
			assertTrue(coordSet.contains(new Coord(2, 2)));
			assertTrue(coordSet.contains(new Coord(3, 2)));
			assertTrue(coordSet.contains(new Coord(4, 2)));
			assertTrue(coordSet.contains(new Coord(5, 2)));
			assertTrue(coordSet.contains(new Coord(6, 2)));

			assertTrue(coordSet.contains(new Coord(0, 3)));
			assertTrue(coordSet.contains(new Coord(1, 3)));
			assertTrue(coordSet.contains(new Coord(2, 3)));
			assertTrue(coordSet.contains(new Coord(3, 3)));
			assertTrue(coordSet.contains(new Coord(4, 3)));
			assertTrue(coordSet.contains(new Coord(5, 3)));
			assertTrue(coordSet.contains(new Coord(6, 3)));

			assertTrue(coordSet.contains(new Coord(0, 4)));
			assertTrue(coordSet.contains(new Coord(1, 4)));
			assertTrue(coordSet.contains(new Coord(2, 4)));
			assertTrue(coordSet.contains(new Coord(3, 4)));
			assertTrue(coordSet.contains(new Coord(4, 4)));
			assertTrue(coordSet.contains(new Coord(5, 4)));
			assertTrue(coordSet.contains(new Coord(6, 4)));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getCoordCellByArea_valid_map2()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(MAP_2), true);
			final Cell[][] cellArray = areaBuilder.getCellsDefinition();

			final Cell cell = cellArray[1][1];
			Area area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
			CoordSet coordSet = area.getCellSet().getCoordSet();
			assertEquals(5, coordSet.size());
			assertTrue(coordSet.contains(new Coord(1, 1)));
			assertTrue(coordSet.contains(new Coord(2, 1)));
			assertTrue(coordSet.contains(new Coord(3, 1)));
			assertTrue(coordSet.contains(new Coord(4, 1)));
			assertTrue(coordSet.contains(new Coord(5, 1)));

			area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.SEA_AREA);
			coordSet = area.getCellSet().getCoordSet();
			assertEquals(32, coordSet.size());
			assertTrue(coordSet.contains(new Coord(0, 0)));
			assertTrue(coordSet.contains(new Coord(1, 0)));
			assertTrue(coordSet.contains(new Coord(2, 0)));
			assertTrue(coordSet.contains(new Coord(3, 0)));
			assertTrue(coordSet.contains(new Coord(4, 0)));
			assertTrue(coordSet.contains(new Coord(5, 0)));
			assertTrue(coordSet.contains(new Coord(6, 0)));

			assertTrue(coordSet.contains(new Coord(0, 1)));
			assertTrue(coordSet.contains(new Coord(1, 1)));
			assertTrue(coordSet.contains(new Coord(5, 1)));
			assertTrue(coordSet.contains(new Coord(6, 1)));

			assertTrue(coordSet.contains(new Coord(0, 2)));
			assertTrue(coordSet.contains(new Coord(1, 2)));
			assertTrue(coordSet.contains(new Coord(2, 2)));
			assertTrue(coordSet.contains(new Coord(3, 2)));
			assertTrue(coordSet.contains(new Coord(4, 2)));
			assertTrue(coordSet.contains(new Coord(5, 2)));
			assertTrue(coordSet.contains(new Coord(6, 2)));

			assertTrue(coordSet.contains(new Coord(0, 3)));
			assertTrue(coordSet.contains(new Coord(1, 3)));
			assertTrue(coordSet.contains(new Coord(2, 3)));
			assertTrue(coordSet.contains(new Coord(3, 3)));
			assertTrue(coordSet.contains(new Coord(4, 3)));
			assertTrue(coordSet.contains(new Coord(5, 3)));
			assertTrue(coordSet.contains(new Coord(6, 3)));

			assertTrue(coordSet.contains(new Coord(0, 4)));
			assertTrue(coordSet.contains(new Coord(1, 4)));
			assertTrue(coordSet.contains(new Coord(2, 4)));
			assertTrue(coordSet.contains(new Coord(3, 4)));
			assertTrue(coordSet.contains(new Coord(4, 4)));
			assertTrue(coordSet.contains(new Coord(5, 4)));
			assertTrue(coordSet.contains(new Coord(6, 4)));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getCoordCellByArea_valid_map3()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(MAP_3), true);
			final Cell[][] cellArray = areaBuilder.getCellsDefinition();

			final Cell cell = cellArray[1][1];
			Area area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
			CoordSet coordSet = area.getCellSet().getCoordSet();
			assertEquals(5, coordSet.size());
			assertTrue(coordSet.contains(new Coord(1, 1)));
			assertTrue(coordSet.contains(new Coord(2, 2)));
			assertTrue(coordSet.contains(new Coord(3, 1)));
			assertTrue(coordSet.contains(new Coord(4, 0)));
			assertTrue(coordSet.contains(new Coord(5, 1)));

			area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.SEA_AREA);
			coordSet = area.getCellSet().getCoordSet();
			assertEquals(34, coordSet.size());
			assertTrue(coordSet.contains(new Coord(0, 0)));
			assertTrue(coordSet.contains(new Coord(1, 0)));
			assertTrue(coordSet.contains(new Coord(2, 0)));
			assertTrue(coordSet.contains(new Coord(3, 0)));
			assertTrue(coordSet.contains(new Coord(5, 0)));
			assertTrue(coordSet.contains(new Coord(6, 0)));

			assertTrue(coordSet.contains(new Coord(0, 1)));
			assertTrue(coordSet.contains(new Coord(1, 1)));
			assertTrue(coordSet.contains(new Coord(2, 1)));
			assertTrue(coordSet.contains(new Coord(3, 1)));
			assertTrue(coordSet.contains(new Coord(4, 1)));
			assertTrue(coordSet.contains(new Coord(5, 1)));
			assertTrue(coordSet.contains(new Coord(6, 1)));

			assertTrue(coordSet.contains(new Coord(0, 2)));
			assertTrue(coordSet.contains(new Coord(1, 2)));
			assertTrue(coordSet.contains(new Coord(2, 2)));
			assertTrue(coordSet.contains(new Coord(3, 2)));
			assertTrue(coordSet.contains(new Coord(4, 2)));
			assertTrue(coordSet.contains(new Coord(5, 2)));
			assertTrue(coordSet.contains(new Coord(6, 2)));

			assertTrue(coordSet.contains(new Coord(0, 3)));
			assertTrue(coordSet.contains(new Coord(1, 3)));
			assertTrue(coordSet.contains(new Coord(2, 3)));
			assertTrue(coordSet.contains(new Coord(3, 3)));
			assertTrue(coordSet.contains(new Coord(4, 3)));
			assertTrue(coordSet.contains(new Coord(5, 3)));
			assertTrue(coordSet.contains(new Coord(6, 3)));

			assertTrue(coordSet.contains(new Coord(0, 4)));
			assertTrue(coordSet.contains(new Coord(1, 4)));
			assertTrue(coordSet.contains(new Coord(2, 4)));
			assertTrue(coordSet.contains(new Coord(3, 4)));
			assertTrue(coordSet.contains(new Coord(4, 4)));
			assertTrue(coordSet.contains(new Coord(5, 4)));
			assertTrue(coordSet.contains(new Coord(6, 4)));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getCoordCellByArea_valid_map7()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(MAP_7), true);
			final Cell[][] cellArray = areaBuilder.getCellsDefinition();

			final Cell cell = cellArray[0][0];
			Area area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
			CoordSet coordSet = area.getCellSet().getCoordSet();

			assertEquals(24, coordSet.size());
			assertTrue(coordSet.contains(new Coord(0, 0)));
			assertTrue(coordSet.contains(new Coord(2, 0)));
			assertTrue(coordSet.contains(new Coord(3, 0)));
			assertTrue(coordSet.contains(new Coord(4, 0)));
			assertTrue(coordSet.contains(new Coord(0, 1)));
			assertTrue(coordSet.contains(new Coord(4, 1)));

			assertTrue(coordSet.contains(new Coord(0, 2)));
			assertTrue(coordSet.contains(new Coord(1, 2)));
			assertTrue(coordSet.contains(new Coord(2, 2)));
			assertTrue(coordSet.contains(new Coord(3, 2)));
			assertTrue(coordSet.contains(new Coord(4, 2)));

			assertTrue(coordSet.contains(new Coord(0, 3)));
			assertTrue(coordSet.contains(new Coord(1, 3)));
			assertTrue(coordSet.contains(new Coord(2, 3)));
			assertTrue(coordSet.contains(new Coord(3, 3)));
			assertTrue(coordSet.contains(new Coord(4, 3)));

			assertTrue(coordSet.contains(new Coord(0, 4)));
			assertTrue(coordSet.contains(new Coord(1, 4)));
			assertTrue(coordSet.contains(new Coord(2, 4)));

			area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.SEA_AREA);
			coordSet = area.getCellSet().getCoordSet();
			assertEquals(6, coordSet.size());
			assertTrue(coordSet.contains(new Coord(0, 0)));
			assertTrue(coordSet.contains(new Coord(1, 0)));
			assertTrue(coordSet.contains(new Coord(2, 0)));

			assertTrue(coordSet.contains(new Coord(1, 1)));
			assertTrue(coordSet.contains(new Coord(2, 1)));
			assertTrue(coordSet.contains(new Coord(3, 1)));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getCoordCellByArea_valid_map11()
	{
		try
		{
			final AreaBuilder areaBuilder = new AreaBuilder(buildCellArray(MAP_11), true);
			final Cell[][] cellArray = areaBuilder.getCellsDefinition();

			Cell cell = cellArray[1][1];
			Area area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
			CoordSet coordSet = area.getCellSet().getCoordSet();

			assertEquals(4, coordSet.size());
			assertTrue(coordSet.contains(new Coord(0, 0)));
			assertTrue(coordSet.contains(new Coord(1, 0)));
			assertTrue(coordSet.contains(new Coord(0, 1)));
			assertTrue(coordSet.contains(new Coord(1, 1)));

			coordSet = area.getGroundCellNearToSea();
			assertEquals(3, coordSet.size());
			assertTrue(coordSet.contains(new Coord(1, 0)));
			assertTrue(coordSet.contains(new Coord(0, 1)));
			assertTrue(coordSet.contains(new Coord(1, 1)));

			// 2th Ground Area
			cell = cellArray[1][3];
			area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
			coordSet = area.getCellSet().getCoordSet();
			assertEquals(1, coordSet.size());
			assertTrue(coordSet.contains(new Coord(3, 1)));

			coordSet = area.getGroundCellNearToSea();
			assertEquals(1, coordSet.size());
			assertTrue(coordSet.contains(new Coord(3, 1)));

			// 3th Ground Area
			cell = cellArray[0][5];
			area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
			coordSet = area.getCellSet().getCoordSet();
			assertEquals(2, coordSet.size());
			assertTrue(coordSet.contains(new Coord(5, 1)));
			assertTrue(coordSet.contains(new Coord(5, 0)));

			coordSet = area.getGroundCellNearToSea();
			assertEquals(2, coordSet.size());
			assertTrue(coordSet.contains(new Coord(5, 1)));
			assertTrue(coordSet.contains(new Coord(5, 0)));

			// 4th Ground Area
			cell = cellArray[3][1];
			area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
			coordSet = area.getCellSet().getCoordSet();
			assertEquals(5, coordSet.size());
			assertTrue(coordSet.contains(new Coord(1, 3)));
			assertTrue(coordSet.contains(new Coord(0, 4)));
			assertTrue(coordSet.contains(new Coord(1, 4)));
			assertTrue(coordSet.contains(new Coord(0, 5)));
			assertTrue(coordSet.contains(new Coord(1, 5)));

			coordSet = area.getGroundCellNearToSea();
			assertEquals(3, coordSet.size());
			assertTrue(coordSet.contains(new Coord(1, 3)));
			assertTrue(coordSet.contains(new Coord(1, 4)));
			assertTrue(coordSet.contains(new Coord(1, 5)));

			// 5th Ground Area
			cell = cellArray[3][3];
			area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
			coordSet = area.getCellSet().getCoordSet();
			assertEquals(2, coordSet.size());
			assertTrue(coordSet.contains(new Coord(3, 3)));
			assertTrue(coordSet.contains(new Coord(3, 4)));

			coordSet = area.getGroundCellNearToSea();
			assertEquals(2, coordSet.size());
			assertTrue(coordSet.contains(new Coord(3, 3)));
			assertTrue(coordSet.contains(new Coord(3, 4)));

			// 6th Ground Area
			cell = cellArray[5][5];
			area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.GROUND_AREA);
			coordSet = area.getCellSet().getCoordSet();
			assertEquals(1, coordSet.size());
			assertTrue(coordSet.contains(new Coord(5, 5)));

			coordSet = area.getGroundCellNearToSea();
			assertEquals(1, coordSet.size());
			assertTrue(coordSet.contains(new Coord(5, 5)));

			// Mountain Area
			cell = cellArray[3][0];
			area = cell.getAreaList().getFirstAreaByType(AreaTypeCst.MOUNTAIN_AREA);
			coordSet = area.getCellSet().getCoordSet();
			assertEquals(1, coordSet.size());
			assertTrue(coordSet.contains(new Coord(0, 3)));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}