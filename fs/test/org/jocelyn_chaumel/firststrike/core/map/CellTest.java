/*
 * CellTest.java JUnit based test Created on 2 mars 2003, 18:16
 */

package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.FSTestCase;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public class CellTest extends FSTestCase
{
	public CellTest(final String aName)
	{
		super(aName);
	}

	/***********************************************************************************************
	 * D�but des tests
	 */

	public void testIsCity_all()
	{
		try
		{
			Cell cell = new Cell(CellTypeCst.CITY, 1, new Coord(1, 1), null, false);
			assertTrue(cell.isCity());

			cell = new Cell(CellTypeCst.GRASSLAND, 1, new Coord(1, 1), "name", false);
			assertFalse(cell.isCity());

			try
			{
				new Cell((CellTypeCst) null, 1, new Coord(1, 1), "name", false);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new Cell(CellTypeCst.MOUNTAIN, 1, null, "name", false);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new Cell(CellTypeCst.MOUNTAIN, 0, new Coord(1, 1), "name", false);
				fail("Invalid Index param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}