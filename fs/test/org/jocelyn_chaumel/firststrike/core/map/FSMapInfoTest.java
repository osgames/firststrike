/*
 * FSMapInfoTest.java JUnit based test
 * 
 * Created on 23 f�vrier 2003, 11:00
 */

package org.jocelyn_chaumel.firststrike.core.map;

import org.jocelyn_chaumel.firststrike.FSTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class FSMapInfoTest extends FSTestCase
{
	public FSMapInfoTest(final String aName)
	{
		super(aName);
	}

	public void testCntrFSMapInfo_all()
	{
		try
		{
			new FSMapInfo("carte", null, null, FSMapMgr.VERSION, 5, 5);
			new FSMapInfo("carte", null, null, "2.1", 5, 5);

			try
			{
				new FSMapInfo(null, null, "description de la carte", FSMapMgr.VERSION, 20, 20);
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			} catch (Exception ex)
			{
				fail(ex);
			}

			try
			{
				new FSMapInfo("carte", null, "description de la carte", FSMapMgr.VERSION, FSMapMgr.MAP_MIN_XY - 1, 20);
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new FSMapInfo("carte", null, "description de la carte", FSMapMgr.VERSION, 20, FSMapMgr.MAP_MIN_XY - 1);
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new FSMapInfo("carte", null, "description de la carte", null, 20, 20);
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new FSMapInfo("carte", null, "description de la carte", "1.0", 20, 20);
				fail("Invalid param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}