/*
 * PathFinderTest.java JUnit based test Created on 15 mai 2003, 12:07
 */

package org.jocelyn_chaumel.firststrike.core.map.path;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

/**
 * @author Jocelyn Chaumel
 */
public class AreaPathFinderTest extends MapTestCase
{

	private int[][] MAP_9 = { { 01, 00, 01, 02, 03 }, { 02, 00, 00, 00, 02 }, { 00, 04, 04, 04, 02 },
			{ 00, 04, 04, 04, 03 }, { 02, 02, 04, 03, 02 }, { 02, 05, 01, 05, 07 } };

	public AreaPathFinderTest(final String aName)
	{
		super(aName);
	}

	public void test_findAreaPathIncludingDestination_nullParam_all()
	{
		try
		{
			final FSMap fsMap = buildFSMap(MAP_1);
			AreaPathFinder pathFinder = new AreaPathFinder(fsMap);
			try
			{
				pathFinder.findAreaPathIncludingDestination(null,
															new Coord(5, 1),
															MoveCostFactory.GROUND_MOVE_COST,
															new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(1, 1),
															null,
															MoveCostFactory.GROUND_MOVE_COST,
															new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(1, 1), new Coord(5, 1), null, new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(1, 1),
															new Coord(5, 1),
															MoveCostFactory.GROUND_MOVE_COST,
															null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(0, 0),
															new Coord(4, 1),
															MoveCostFactory.GROUND_MOVE_COST,
															new CoordSet());
				fail("Invalid start coord param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(4, 1),
															new Coord(0, 0),
															MoveCostFactory.GROUND_MOVE_COST,
															new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(0, 0),
															new Coord(3, 5),
															MoveCostFactory.GROUND_MOVE_COST,
															new CoordSet());
				fail("Invalid destination area param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(0, 0),
															new Coord(3, 1),
															MoveCostFactory.GROUND_MOVE_COST,
															new CoordSet());
				fail("Invalid destination area param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_valid_1()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_1);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			Path path = pathFinder.findAreaPathIncludingDestination(new Coord(1, 1),
																	new Coord(5, 1),
																	MoveCostFactory.GROUND_MOVE_COST,
																	new CoordSet());
			CoordList list = new CoordList();
			list.add(new Coord(2, 1));
			list.add(new Coord(3, 1));
			list.add(new Coord(4, 1));
			list.add(new Coord(5, 1));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));

			path = pathFinder.findAreaPathIncludingDestination(	new Coord(5, 1),
																new Coord(1, 1),
																MoveCostFactory.GROUND_MOVE_COST,
																new CoordSet());
			list = new CoordList();
			list.add(new Coord(4, 1));
			list.add(new Coord(3, 1));
			list.add(new Coord(2, 1));
			list.add(new Coord(1, 1));
			resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));

			// La destination n'est pas occup�e par un ennemi et la distance du
			// chemin = 1.
			final CoordSet excludedCoordSet = new CoordSet();
			path = pathFinder.findAreaPathIncludingDestination(	new Coord(1, 1),
																new Coord(2, 1),
																MoveCostFactory.GROUND_MOVE_COST,
																excludedCoordSet);

			assertEquals(new Coord(2, 1), path.getDestination());
			assertEquals(new Coord(2, 1), path.getNextStep());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_valid_2()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_2);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			Path path = pathFinder.findAreaPathIncludingDestination(new Coord(1, 1),
																	new Coord(5, 1),
																	MoveCostFactory.GROUND_MOVE_COST,
																	new CoordSet());
			CoordList list = new CoordList();
			list.add(new Coord(2, 1));
			list.add(new Coord(3, 1));
			list.add(new Coord(4, 1));
			list.add(new Coord(5, 1));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));

			path = pathFinder.findAreaPathIncludingDestination(	new Coord(5, 1),
																new Coord(1, 1),
																MoveCostFactory.GROUND_MOVE_COST,
																new CoordSet());
			list = new CoordList();
			list.add(new Coord(4, 1));
			list.add(new Coord(3, 1));
			list.add(new Coord(2, 1));
			list.add(new Coord(1, 1));
			resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));

			path = pathFinder.findAreaPathIncludingDestination(	new Coord(1, 1),
																new Coord(5, 1),
																MoveCostFactory.BATTLESHIP_MOVE_COST,
																new CoordSet());
			list = new CoordList();
			list.add(new Coord(2, 0));
			list.add(new Coord(3, 0));
			list.add(new Coord(4, 0));
			list.add(new Coord(5, 1));
			resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestinationfindAreaPathIncludingDestination_valid_3()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_3);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			final Path path = pathFinder.findAreaPathIncludingDestination(	new Coord(1, 1),
																			new Coord(5, 1),
																			MoveCostFactory.GROUND_MOVE_COST,
																			new CoordSet());
			final CoordList list = new CoordList();
			list.add(new Coord(2, 2));
			list.add(new Coord(3, 1));
			list.add(new Coord(4, 0));
			list.add(new Coord(5, 1));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_valid_4()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_4);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			Path path = pathFinder.findAreaPathIncludingDestination(new Coord(0, 0),
																	new Coord(6, 0),
																	MoveCostFactory.GROUND_MOVE_COST,
																	new CoordSet());
			CoordList list = new CoordList();
			list.add(new Coord(1, 0));
			list.add(new Coord(2, 0));
			list.add(new Coord(3, 0));
			list.add(new Coord(4, 0));
			list.add(new Coord(5, 0));
			list.add(new Coord(6, 0));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));

			path = pathFinder.findAreaPathIncludingDestination(	new Coord(0, 2),
																new Coord(6, 2),
																MoveCostFactory.GROUND_MOVE_COST,
																new CoordSet());
			list = new CoordList();
			list.add(new Coord(1, 2));
			list.add(new Coord(2, 2));
			list.add(new Coord(3, 2));
			list.add(new Coord(4, 2));
			list.add(new Coord(5, 2));
			list.add(new Coord(6, 2));
			resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_valid_5()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_5);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			final Path path = pathFinder.findAreaPathIncludingDestination(	new Coord(1, 1),
																			new Coord(1, 4),
																			MoveCostFactory.GROUND_MOVE_COST,
																			new CoordSet());
			CoordList list = new CoordList();
			list.add(new Coord(1, 2));
			list.add(new Coord(1, 3));
			list.add(new Coord(1, 4));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_valid_6()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_6);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			final Path path = pathFinder.findAreaPathIncludingDestination(	new Coord(0, 0),
																			new Coord(0, 5),
																			MoveCostFactory.GROUND_MOVE_COST,
																			new CoordSet());
			CoordList list = new CoordList();
			list.add(new Coord(0, 1));
			list.add(new Coord(0, 2));
			list.add(new Coord(0, 3));
			list.add(new Coord(0, 4));
			list.add(new Coord(0, 5));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_valid_7()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_7);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			final Path path = pathFinder.findAreaPathIncludingDestination(	new Coord(0, 0),
																			new Coord(2, 0),
																			MoveCostFactory.GROUND_MOVE_COST,
																			new CoordSet());
			CoordList list = new CoordList();
			list.add(new Coord(0, 1));
			list.add(new Coord(1, 2));
			list.add(new Coord(2, 3));
			list.add(new Coord(3, 2));
			list.add(new Coord(4, 1));
			list.add(new Coord(3, 0));
			list.add(new Coord(2, 0));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_valid_8()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_8);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			final Path path = pathFinder.findAreaPathIncludingDestination(	new Coord(0, 0),
																			new Coord(0, 6),
																			MoveCostFactory.GROUND_MOVE_COST,
																			new CoordSet());
			CoordList list = new CoordList();
			list.add(new Coord(1, 0));
			list.add(new Coord(2, 0));
			list.add(new Coord(3, 0));
			list.add(new Coord(4, 0));
			list.add(new Coord(5, 1));
			list.add(new Coord(5, 2));
			list.add(new Coord(5, 3));
			list.add(new Coord(5, 4));
			list.add(new Coord(5, 5));
			list.add(new Coord(4, 6));
			list.add(new Coord(3, 6));
			list.add(new Coord(2, 5));
			list.add(new Coord(1, 4));
			list.add(new Coord(0, 5));
			list.add(new Coord(0, 6));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// private int[][] MAP_9 = {
	// 01, 00, 01, 02, 03
	// 02, 00, 00, 00, 02
	// 00, 04, 04, 04, 02
	// 00, 04, 04, 04, 02
	// 02, 02, 04, 02, 02
	// 02, 00, 01, 00, 00
	// };
	public void test_findAreaPathIncludingDestination_valid_9()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_9);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			final Path path = pathFinder.findAreaPathIncludingDestination(	new Coord(0, 0),
																			new Coord(2, 0),
																			MoveCostFactory.GROUND_MOVE_COST,
																			new CoordSet());
			final CoordList list = new CoordList();
			list.add(new Coord(0, 1));
			list.add(new Coord(1, 2));
			list.add(new Coord(2, 3));
			list.add(new Coord(3, 2));
			list.add(new Coord(4, 1));
			list.add(new Coord(3, 0));
			list.add(new Coord(2, 0));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_valid_10()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_10);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);

			final CoordSet excludedCoordSet = new CoordSet();
			final Path path = pathFinder.findAreaPathIncludingDestination(	new Coord(0, 0),
																			new Coord(4, 1),
																			MoveCostFactory.GROUND_MOVE_COST,
																			excludedCoordSet);

			assertEquals(new Coord(4, 1), path.getDestination());
			assertEquals(new Coord(1, 1), path.getNextStep());
			assertEquals(new Coord(2, 1), path.getNextStep());
			assertEquals(new Coord(3, 1), path.getNextStep());
			assertEquals(new Coord(4, 1), path.getNextStep());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_valid_11()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_11);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);

			final CoordSet excludedCoordSet = new CoordSet();
			final Path path = pathFinder.findAreaPathIncludingDestination(	new Coord(0, 0),
																			new Coord(1, 1),
																			MoveCostFactory.GROUND_MOVE_COST,
																			excludedCoordSet);

			assertEquals(new Coord(1, 1), path.getDestination());
			assertEquals(new Coord(1, 1), path.getNextStep());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_pathNotFound_all()
	{
		try
		{
			FSMap map = buildFSMap(MAP_1);
			AreaPathFinder pathFinder = new AreaPathFinder(map);

			final CoordSet excludedCoordSet = new CoordSet();
			excludedCoordSet.add(new Coord(2, 1));

			// En d�but de parcours, un ennemi barre le chemin
			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(1, 1),
															new Coord(4, 1),
															MoveCostFactory.GROUND_MOVE_COST,
															excludedCoordSet);
				fail("A valid path has been returned");
			} catch (PathNotFoundException ex)
			{
				// NOP
			}

			// La destination est occup�e par un ennemi et la distance du chemin
			// = 1.
			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(1, 1),
															new Coord(2, 1),
															MoveCostFactory.GROUND_MOVE_COST,
															excludedCoordSet);
				fail("A valid path has been returned");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			excludedCoordSet.clear();
			excludedCoordSet.add(new Coord(4, 1));
			// La destination est occup�e par un ennemi.
			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(1, 1),
															new Coord(4, 1),
															MoveCostFactory.GROUND_MOVE_COST,
															excludedCoordSet);
				fail("A valid path has been returned");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			excludedCoordSet.clear();
			excludedCoordSet.add(new Coord(3, 1));
			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(1, 1),
															new Coord(4, 1),
															MoveCostFactory.GROUND_MOVE_COST,
															excludedCoordSet);
				fail("A valid path has been returned");
			} catch (PathNotFoundException ex)
			{
				// NOP
			}

			map = buildFSMap(MAP_3);
			pathFinder = new AreaPathFinder(map);

			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(1, 1),
															new Coord(5, 1),
															MoveCostFactory.GROUND_MOVE_COST,
															excludedCoordSet);
				fail("A valid path has been returned");
			} catch (PathNotFoundException ex)
			{
				// NOP
			}

			map = buildFSMap(MAP_7);
			pathFinder = new AreaPathFinder(map);

			excludedCoordSet.clear();
			excludedCoordSet.add(new Coord(0, 3));
			excludedCoordSet.add(new Coord(1, 3));
			excludedCoordSet.add(new Coord(2, 3));
			excludedCoordSet.add(new Coord(3, 3));
			excludedCoordSet.add(new Coord(4, 3));
			// Plusieurs ennemis bloquent tous les parcours possibles.
			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(0, 0),
															new Coord(2, 5),
															MoveCostFactory.GROUND_MOVE_COST,
															excludedCoordSet);
				fail("A valid path has been returned");
			} catch (PathNotFoundException ex)
			{
				// NOP
			}

			excludedCoordSet.clear();
			excludedCoordSet.add(new Coord(0, 2));
			excludedCoordSet.add(new Coord(0, 3));
			excludedCoordSet.add(new Coord(1, 3));
			excludedCoordSet.add(new Coord(2, 3));
			excludedCoordSet.add(new Coord(3, 3));
			excludedCoordSet.add(new Coord(3, 4));
			excludedCoordSet.add(new Coord(4, 4));
			// Plusieurs ennemis bloquent tous les parcours possibles.
			try
			{
				pathFinder.findAreaPathIncludingDestination(new Coord(0, 0),
															new Coord(2, 5),
															MoveCostFactory.GROUND_MOVE_COST,
															excludedCoordSet);
				fail("A valid path has been returned");
			} catch (PathNotFoundException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_withEnemy_valid_1()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_3);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			final CoordSet excludedCoordSet = new CoordSet();
			excludedCoordSet.add(new Coord(0, 0));
			excludedCoordSet.add(new Coord(1, 0));
			excludedCoordSet.add(new Coord(2, 0));
			excludedCoordSet.add(new Coord(3, 0));
			excludedCoordSet.add(new Coord(5, 0));
			excludedCoordSet.add(new Coord(6, 0));
			excludedCoordSet.add(new Coord(0, 1));
			excludedCoordSet.add(new Coord(2, 1));
			excludedCoordSet.add(new Coord(4, 1));
			excludedCoordSet.add(new Coord(6, 1));
			excludedCoordSet.add(new Coord(0, 2));
			excludedCoordSet.add(new Coord(1, 2));
			excludedCoordSet.add(new Coord(3, 2));
			excludedCoordSet.add(new Coord(4, 2));
			excludedCoordSet.add(new Coord(5, 2));
			excludedCoordSet.add(new Coord(6, 2));
			Path path = pathFinder.findAreaPathIncludingDestination(new Coord(1, 1),
																	new Coord(5, 1),
																	MoveCostFactory.GROUND_MOVE_COST,
																	excludedCoordSet);

			final CoordList list = new CoordList();
			list.add(new Coord(2, 2));
			list.add(new Coord(3, 1));
			list.add(new Coord(4, 0));
			list.add(new Coord(5, 1));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_withEnemy_valid_2()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_7);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			final CoordSet excludedCoordSet = new CoordSet();
			excludedCoordSet.add(new Coord(0, 2));
			excludedCoordSet.add(new Coord(0, 3));
			excludedCoordSet.add(new Coord(2, 2));
			Path path = pathFinder.findAreaPathIncludingDestination(new Coord(0, 0),
																	new Coord(2, 0),
																	MoveCostFactory.GROUND_MOVE_COST,
																	excludedCoordSet);
			final CoordList list = new CoordList();
			list.add(new Coord(0, 1));
			list.add(new Coord(1, 2));
			list.add(new Coord(2, 3));
			list.add(new Coord(3, 2));
			list.add(new Coord(4, 1));
			list.add(new Coord(3, 0));
			list.add(new Coord(2, 0));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	// private int[][] MAP_7 = {
	// 01 , 00 , 01 , 02 , 03
	// 02 , 00 , 00, 00 , 02
	// 02e, 04 , 04 , 04e, 02
	// 02e, 04e, 04e, 04 , 03
	// 02 , 02 , 04 , 03 , 02
	// 02 , 00 , 01 , 00 , 00
	// };

	public void test_findAreaPathIncludingDestination_withEnemy_valid_3()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_7);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			final CoordSet excludedCoordSet = new CoordSet();
			excludedCoordSet.add(new Coord(0, 2));
			excludedCoordSet.add(new Coord(0, 3));
			excludedCoordSet.add(new Coord(1, 3));
			excludedCoordSet.add(new Coord(2, 3));
			excludedCoordSet.add(new Coord(3, 2));
			final Path path = pathFinder.findAreaPathIncludingDestination(	new Coord(0, 0),
																			new Coord(2, 0),
																			MoveCostFactory.GROUND_MOVE_COST,
																			excludedCoordSet);
			final CoordList list = new CoordList();
			list.add(new Coord(0, 1));
			list.add(new Coord(1, 2));
			list.add(new Coord(2, 2));
			list.add(new Coord(3, 3));
			list.add(new Coord(4, 2));
			list.add(new Coord(4, 1));
			list.add(new Coord(3, 0));
			list.add(new Coord(2, 0));
			Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_findAreaPathIncludingDestination_withEnemy_valid_4()
	{
		try
		{
			final FSMap map = buildFSMap(MAP_7);
			final AreaPathFinder pathFinder = new AreaPathFinder(map);
			final CoordSet excludedCoordSet = new CoordSet();
			excludedCoordSet.add(new Coord(0, 2));
			excludedCoordSet.add(new Coord(2, 2));
			excludedCoordSet.add(new Coord(2, 3));
			final Path path = pathFinder.findAreaPathIncludingDestination(	new Coord(0, 0),
																			new Coord(2, 0),
																			MoveCostFactory.GROUND_MOVE_COST,
																			excludedCoordSet);
			final CoordList list = new CoordList();
			list.add(new Coord(0, 1));
			list.add(new Coord(1, 2));
			list.add(new Coord(0, 3));
			list.add(new Coord(1, 4));
			list.add(new Coord(2, 5));
			list.add(new Coord(3, 4));
			list.add(new Coord(4, 3));
			list.add(new Coord(4, 2));
			list.add(new Coord(4, 1));
			list.add(new Coord(3, 0));
			list.add(new Coord(2, 0));
			final Path resultedPath = new Path(list);

			assertTrue("These path aren't identical.", path.equals(resultedPath));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}