/*
 * PathTest.java JUnit based test
 * 
 * Created on 2 mai 2003, 00:01
 */

package org.jocelyn_chaumel.firststrike.core.map.path;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

/**
 * 
 * @author Jocelyn Chaumel
 */
public class PathTest extends MapTestCase
{
	public PathTest(final String aName)
	{
		super(aName);
	}

	public void test_cntrCoordList_nullParam()
	{
		try
		{
			new Path((CoordList) null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void test_cntrCoordList_emptyList()
	{
		try
		{
			new Path(new CoordList());
			fail("Empty list has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void test_cntrCoordList_Valid()
	{
		try
		{
			final CoordList list = new CoordList();
			list.add(new Coord());
			new Path(list);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public final void test_GetNext_AndIsEmpty_valid()
	{
		try
		{
			CoordList list = new CoordList();
			list.add(new Coord(1, 1));
			list.add(new Coord(2, 2));
			Path path = new Path(list);
			assertTrue(new Coord(1, 1).equals(path.getNextStep()));
			assertEquals(false, path.isEmpty());
			assertTrue(new Coord(2, 2).equals(path.getNextStep()));
			assertEquals(true, path.isEmpty());
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public final void test_GetNext_endOfPathReached()
	{
		try
		{
			CoordList list = new CoordList();
			list.add(new Coord(1, 1));
			list.add(new Coord(2, 2));
			Path path = new Path(list);
			assertTrue(new Coord(1, 1).equals(path.getNextStep()));
			assertTrue(new Coord(2, 2).equals(path.getNextStep()));
			path.getNextStep();
			fail("getNext() has been accepted with an empty list.");
		} catch (IllegalStateException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public final void test_HasNext_valid()
	{
		try
		{
			CoordList list = new CoordList();
			list.add(new Coord(1, 1));
			Path path = new Path(list);
			assertTrue(path.hasNextStep());
			path.getNextStep();
			assertFalse(path.hasNextStep());
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public final void test_Equals_NullParam()
	{
		try
		{
			CoordList list = new CoordList();
			list.add(new Coord(1, 1));
			list.add(new Coord(2, 2));

			Path path = new Path(list);

			path.equals(null);
			fail("Null has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Equals_NotEqual()
	{
		try
		{
			final CoordList l1 = new CoordList();
			final CoordList l2 = new CoordList();
			final Coord c1 = new Coord(1, 1);
			final Coord c2 = new Coord(1, 2);
			final Coord c3 = new Coord(1, 3);
			l1.add(c1);
			l2.add(c1);
			l1.add(c2);
			l2.add(c2);

			l1.add(c3);

			Path p1 = new Path(l1);
			Path p2 = new Path(l2);

			assertTrue("These path are not equals.", !p1.equals(p2));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Equals_egal_valid()
	{
		try
		{
			CoordList l1 = new CoordList();
			CoordList l2 = new CoordList();
			Coord c = new Coord(1, 1);
			l1.add(c);
			l2.add(c);
			c = new Coord(2, 2);
			l1.add(c);
			l2.add(c);

			Path p1 = new Path(l1);
			Path p2 = new Path(l2);

			assertTrue("These path are equals.", p1.equals(p2));
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetDestination_invalid()
	{
		try
		{
			CoordList list = new CoordList();
			list.add(new Coord(1, 1));
			Path path = new Path(list);
			path.getNextStep();
			path.getDestination();
			fail("A invalid operation has been succesfully executed");

		} catch (IllegalStateException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_GetDestination_valid()
	{
		try
		{
			CoordList list = new CoordList();
			list.add(new Coord(1, 1));
			list.add(new Coord(1, 2));
			Path path = new Path(list);
			assertEquals(new Coord(1, 2), path.getDestination());

			list = new CoordList();
			list.add(new Coord(1, 1));
			list.add(new Coord(1, 2));
			list.add(new Coord(1, 3));
			path = new Path(list);
			assertEquals(new Coord(1, 3), path.getDestination());
			path.getNextStep();
			assertEquals(new Coord(1, 3), path.getDestination());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_FallsOnEnemyPosition_NullParam()
	{
		try
		{
			CoordList list = new CoordList();
			list.add(new Coord(1, 1));
			list.add(new Coord(2, 2));

			Path path = new Path(list);

			path.fallsOnEnemyPosition(null);
			fail("Null has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_FallsOnEnemyPosition_valid()
	{
		try
		{
			CoordList list = new CoordList();
			list.add(new Coord(1, 1));
			list.add(new Coord(2, 2));

			Path path = new Path(list);

			final CoordSet enemyCoordSet = new CoordSet();
			assertEquals(false, path.fallsOnEnemyPosition(enemyCoordSet));

			enemyCoordSet.add(new Coord(2, 1));
			assertEquals(false, path.fallsOnEnemyPosition(enemyCoordSet));

			enemyCoordSet.add(new Coord(1, 2));
			assertEquals(false, path.fallsOnEnemyPosition(enemyCoordSet));

			enemyCoordSet.add(new Coord(2, 2));
			assertEquals(true, path.fallsOnEnemyPosition(enemyCoordSet));

			enemyCoordSet.clear();
			enemyCoordSet.add(new Coord(1, 1));
			assertEquals(true, path.fallsOnEnemyPosition(enemyCoordSet));

			path.getNextStep();
			path.getNextStep();
			assertEquals(false, path.fallsOnEnemyPosition(enemyCoordSet));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_Clear_valid()
	{
		try
		{
			CoordList list = new CoordList();
			list.add(new Coord(1, 1));
			list.add(new Coord(2, 2));

			Path path = new Path(list);

			assertEquals(true, path.hasNextStep());
			path.clear();
			assertEquals(false, path.hasNextStep());
			path.clear();
			assertEquals(false, path.hasNextStep());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_removeLastStep_noLastStepToRemove()
	{
		try
		{
			final CoordList list = new CoordList();
			list.add(new Coord(1, 1));
			Path path = new Path(list);
			path.getNextStep();
			try
			{
				path.removeLastStep();
				fail("No last step to remove");
			} catch (IllegalStateException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public final void test_getCoordList_emptyPath()
	{
		try
		{
			final CoordList coordList = new CoordList();
			coordList.add(new Coord(1, 1));
			Path path = new Path(coordList);
			path.getNextStep();
			path.getCoordList();
			fail("Invalid operation has been accepted.");
		} catch (IllegalStateException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_clone_valid()
	{
		final Coord c1 = new Coord(1, 1);
		final Coord c2 = new Coord(1, 2);
		CoordList list = new CoordList();
		list.add(c1);
		list.add(c2);

		Path p1 = new Path(list);
		Path p2 = (Path) p1.clone();

		assertEquals(p1.getNextStep(), c1);
		assertEquals(p2.getNextStep(), c1);
		assertEquals(p1.getNextStep(), c2);
		assertEquals(p2.getNextStep(), c2);
	}

	public final void test_reversePath_valid()
	{
		final Coord c1 = new Coord(1, 1);
		final Coord c2 = new Coord(1, 2);
		final Coord c3 = new Coord(1, 3);
		CoordList list = new CoordList();
		list.add(c2);
		list.add(c3);

		Path p1 = new Path(list);
		Path p2 = (Path) p1.clone();
		p1.reversePath(c1);

		assertEquals(p1.getNextStep(), c2);
		assertEquals(p1.getNextStep(), c1);
		assertEquals(p2.getNextStep(), c2);
		assertEquals(p2.getNextStep(), c3);
	}

	public final void test_validateFuelAlimentation_all()
	{
		try
		{
			CoordList coordList = new CoordList();
			int i = 1;
			while (i <= 5)
			{
				coordList.add(new Coord(i++, 0));
			}

			Path path = new Path(coordList);

			// When the destination is "on the way"
			assertTrue(path.validateFuelAlimentation(10, 10, new CoordSet()));
			assertTrue(path.validateFuelAlimentation(5, 5, new CoordSet()));
			assertFalse(path.validateFuelAlimentation(4, 5, new CoordSet()));
			assertFalse(path.validateFuelAlimentation(1, 7, new CoordSet(new Coord(1, 0))));
			assertTrue(path.validateFuelAlimentation(1, 10, new CoordSet(new Coord(1, 0))));

			// When the destination is not "on the way"
			coordList = new CoordList();
			i = 11;
			while (i <= 15)
			{
				coordList.add(new Coord(i++, 10));
			}

			path = new Path(coordList);

			assertFalse(path.validateFuelAlimentation(1, 7, new CoordSet(new Coord(1, 0))));
			assertFalse(path.validateFuelAlimentation(1, 9, new CoordSet(new Coord(1, 0))));
			assertFalse(path.validateFuelAlimentation(1, 9, new CoordSet(new Coord(1, 1))));
			assertTrue(path.validateFuelAlimentation(10, 10, new CoordSet(new Coord(20, 10))));
			assertTrue(path.validateFuelAlimentation(10, 10, new CoordSet(new Coord(20, 5))));
			assertFalse(path.validateFuelAlimentation(10, 10, new CoordSet(new Coord(20, 4))));

			try
			{
				path.validateFuelAlimentation(0, 10, new CoordSet());
				fail("Invalid fuel has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				path.validateFuelAlimentation(10, 1, new CoordSet());
				fail("Invalid fuel capacity has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				path.validateFuelAlimentation(10, 10, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getTurnCost_all()
	{
		try
		{
			CoordList coordList = new CoordList();
			int i = 1;
			while (i <= 4)
			{
				coordList.add(new Coord(i++, 1));
			}

			Path path = new Path(coordList);
			FSMap map1 = super.buildFSMap(STANDARD_MAP_4);

			assertEquals(	1,
							path.calculateTurnCost(	map1,
												MoveCostFactory.GROUND_MOVE_COST,
												MoveCostFactory.GROUND_MOVE_COST.getMaxCost(),
												MoveCostFactory.GROUND_MOVE_COST.getMaxCost()));
			assertEquals(0, path.getTurnCostForCoord(new Coord(1, 1)));
			assertEquals(0, path.getTurnCostForCoord(new Coord(2, 1)));
			assertEquals(1, path.getTurnCostForCoord(new Coord(3, 1)));
			assertEquals(1, path.getTurnCostForCoord(new Coord(4, 1)));

			assertEquals(2, path.calculateTurnCost(	map1,
													MoveCostFactory.GROUND_MOVE_COST,
													MoveCostFactory.GROUND_MOVE_COST.getMaxCost() / 3,
													MoveCostFactory.GROUND_MOVE_COST.getMaxCost()));
			assertEquals(0, path.getTurnCostForCoord(new Coord(1, 1)));
			assertEquals(1, path.getTurnCostForCoord(new Coord(2, 1)));
			assertEquals(1, path.getTurnCostForCoord(new Coord(3, 1)));

			assertEquals(2, path.calculateTurnCost(	map1,
													MoveCostFactory.GROUND_MOVE_COST,
													0,
													MoveCostFactory.GROUND_MOVE_COST.getMaxCost()));
			assertEquals(1, path.getTurnCostForCoord(new Coord(1, 1)));
			assertEquals(1, path.getTurnCostForCoord(new Coord(2, 1)));
			assertEquals(2, path.getTurnCostForCoord(new Coord(3, 1)));
			assertEquals(2, path.getTurnCostForCoord(new Coord(4, 1)));

			coordList.clear();
			i = 0;
			while (i < 7)
			{
				coordList.add(new Coord(i++, 0));
			}

			path = new Path(coordList);
			FSMap map2 = super.buildFSMap(STANDARD_MAP_5);
			path.calculateTurnCost(	map2,
									MoveCostFactory.GROUND_MOVE_COST,
									MoveCostFactory.GROUND_MOVE_COST.getMaxCost(),
									MoveCostFactory.GROUND_MOVE_COST.getMaxCost());
			assertEquals(0, path.getTurnCostForCoord(new Coord(0, 0)));
			assertEquals(0, path.getTurnCostForCoord(new Coord(1, 0)));
			assertEquals(1, path.getTurnCostForCoord(new Coord(2, 0)));
			assertEquals(1, path.getTurnCostForCoord(new Coord(3, 0)));
			assertEquals(2, path.getTurnCostForCoord(new Coord(4, 0)));
			assertEquals(3, path.getTurnCostForCoord(new Coord(5, 0)));
			assertEquals(4, path.getTurnCostForCoord(new Coord(6, 0)));

			path.calculateTurnCost(	map2,
									MoveCostFactory.GROUND_MOVE_COST,
									MoveCostFactory.GROUND_MOVE_COST.getMaxCost() / 3,
									MoveCostFactory.GROUND_MOVE_COST.getMaxCost());
			assertEquals(0, path.getTurnCostForCoord(new Coord(0, 0)));
			assertEquals(1, path.getTurnCostForCoord(new Coord(1, 0)));
			assertEquals(1, path.getTurnCostForCoord(new Coord(2, 0)));
			assertEquals(2, path.getTurnCostForCoord(new Coord(3, 0)));
			assertEquals(2, path.getTurnCostForCoord(new Coord(4, 0)));
			assertEquals(3, path.getTurnCostForCoord(new Coord(5, 0)));
			assertEquals(4, path.getTurnCostForCoord(new Coord(6, 0)));

			path.getNextStep();
			try
			{
				path.getTurnCostForCoord(new Coord(0, 0));
				fail("Invalid coord has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			assertEquals(1, path.getTurnCostForCoord(new Coord(1, 0)));
			assertEquals(1, path.getTurnCostForCoord(new Coord(2, 0)));
			assertEquals(2, path.getTurnCostForCoord(new Coord(3, 0)));
			assertEquals(2, path.getTurnCostForCoord(new Coord(4, 0)));
			assertEquals(3, path.getTurnCostForCoord(new Coord(5, 0)));
			assertEquals(4, path.getTurnCostForCoord(new Coord(6, 0)));

			assertEquals(new Coord(1, 0), path.getNextStepWithoutConsume());
			try
			{
				path.getTurnCostForCoord(new Coord(0, 0));
				fail("Invalid coord has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			assertEquals(1, path.getTurnCostForCoord(new Coord(1, 0)));
			assertEquals(1, path.getTurnCostForCoord(new Coord(2, 0)));
			assertEquals(2, path.getTurnCostForCoord(new Coord(3, 0)));
			assertEquals(2, path.getTurnCostForCoord(new Coord(4, 0)));
			assertEquals(3, path.getTurnCostForCoord(new Coord(5, 0)));
			assertEquals(4, path.getTurnCostForCoord(new Coord(6, 0)));

			try
			{
				path.getTurnCostForCoord(null);
				fail("Null coord has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				path.calculateTurnCost(	null,
										MoveCostFactory.GROUND_MOVE_COST,
										MoveCostFactory.GROUND_MOVE_COST.getMaxCost(),
										MoveCostFactory.GROUND_MOVE_COST.getMaxCost());
				fail("Null coord has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				path.calculateTurnCost(	map1,
										null,
										MoveCostFactory.GROUND_MOVE_COST.getMaxCost(),
										MoveCostFactory.GROUND_MOVE_COST.getMaxCost());
				fail("Null coord has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				path.calculateTurnCost(	map1,
										MoveCostFactory.GROUND_MOVE_COST,
										MoveCostFactory.GROUND_MOVE_COST.getMaxCost() + 1,
										MoveCostFactory.GROUND_MOVE_COST.getMaxCost());
				fail("Null coord has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				path.calculateTurnCost(	map1,
										MoveCostFactory.GROUND_MOVE_COST,
										-1,
										MoveCostFactory.GROUND_MOVE_COST.getMaxCost());
				fail("Null coord has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				path.calculateTurnCost(	map1,
										MoveCostFactory.GROUND_MOVE_COST,
										MoveCostFactory.GROUND_MOVE_COST.getMaxCost(),
										0);
				fail("Null coord has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			path.removeLastStep();
			try
			{
				path.getTurnCostForCoord(new Coord(1, 0));
				fail("Invalid operation has been executed");
			} catch (IllegalStateException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
	

	public final void test_getTotalDistance_all()
	{
		try
		{
			CoordList coordList = new CoordList();

			FSMap map = super.buildFSMap(STANDARD_MAP_4);

			coordList.add(new Coord(1,1)); // City - 25
			
			Path path = new Path(coordList);
			path.calculateTurnCost(map, MoveCostFactory.GROUND_MOVE_COST, 100, 100);
			assertEquals(0, path.getTurnCostForCoord(new Coord(1,1)));
			assertEquals(25, path.getTotalDistance());

			coordList.add(new Coord(2,1)); // Grass - 33;
			path = new Path(coordList);
			path.calculateTurnCost(map, MoveCostFactory.GROUND_MOVE_COST, 100, 100);
			assertEquals(25 + 33, path.getTotalDistance());
			assertEquals(0, path.getTurnCostForCoord(new Coord(2,1)));
			
			coordList.add(new Coord(3,1)); // Grass - 33;
			path = new Path(coordList);
			path.calculateTurnCost(map, MoveCostFactory.GROUND_MOVE_COST, 100, 100);
			assertEquals(25 + 33 + 33, path.getTotalDistance());
			assertEquals(0, path.getTurnCostForCoord(new Coord(3,1)));
			
			coordList.add(new Coord(4,1)); // Grass - 33;
			path = new Path(coordList);
			path.calculateTurnCost(map, MoveCostFactory.GROUND_MOVE_COST, 100, 100);
			assertEquals(100 + 33, path.getTotalDistance());
			assertEquals(1, path.getTurnCostForCoord(new Coord(4,1)));
			

		} catch (Exception ex)
		{
			fail(ex);
		}
	}


	public void test_hasSecondStepInSameTurn_all()
	{
		try
		{
			CoordList coordList = new CoordList();
			int i = 2;
			while (i <= 4)
			{
				coordList.add(new Coord(i++, 1));
			}
			coordList.add(new Coord(4, 2));

			Path path = new Path(coordList);
			FSMap map1 = super.buildFSMap(STANDARD_MAP_4);
			path.calculateTurnCost(map1, MoveCostFactory.GROUND_MOVE_COST, 2, 3);
			assertEquals(new Coord(2, 1), path.getEndTurnPosition());

			assertEquals(true, path.hasNextStep());
			assertEquals(new Coord(2, 1), path.getNextStepWithoutConsume());
			assertEquals(1, path.getTurnCostForCoord(path.getNextStepWithoutConsume()));
			assertEquals(false, path.hasSecondStepInSameTurn());
			assertEquals(new Coord(2, 1), path.getEndTurnPosition());
			assertEquals(new Coord(2, 1), path.getNextStep());

			assertEquals(true, path.hasNextStep());
			assertEquals(new Coord(3, 1), path.getNextStepWithoutConsume());
			assertEquals(2, path.getTurnCostForCoord(path.getNextStepWithoutConsume()));
			assertEquals(false, path.hasSecondStepInSameTurn());
			assertEquals(new Coord(3, 1), path.getEndTurnPosition());
			assertEquals(new Coord(3, 1), path.getNextStep());

			assertEquals(true, path.hasNextStep());
			assertEquals(new Coord(4, 1), path.getNextStepWithoutConsume());
			assertEquals(3, path.getTurnCostForCoord(path.getNextStepWithoutConsume()));
			assertEquals(false, path.hasSecondStepInSameTurn());
			assertEquals(new Coord(4, 1), path.getEndTurnPosition());
			assertEquals(new Coord(4, 1), path.getNextStep());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}