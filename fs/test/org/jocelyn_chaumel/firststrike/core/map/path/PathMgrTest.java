/*
 * Created on Nov 21, 2004
 */
package org.jocelyn_chaumel.firststrike.core.map.path;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

/**
 * @author Jocelyn Chaumel
 */
public class PathMgrTest extends MapTestCase
{
	public PathMgrTest(final String aName)
	{
		super(aName);
	}

	public void test_cntr_nullParam()
	{
		try
		{
			new PathMgr(null);
			fail("Null Ref has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getAreaPath_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final PathMgr mgr = new PathMgr(map);

			// PATH IN THE AREA
			// ================

			// Ground Move Cost
			// ----------------
			Path path = mgr.getAreaPath(new Coord(1, 1),
										new Coord(4, 4),
										MoveCostFactory.GROUND_MOVE_COST,
										3,
										3,
										new CoordSet(),
										false);
			assertNotNull(path);
			assertEquals(new Coord(4, 4), path.getDestination());

			path = mgr.getAreaPath(	new Coord(1, 1),
									new Coord(4, 4),
									MoveCostFactory.GROUND_MOVE_COST,
									3,
									3,
									new CoordSet(),
									true);
			assertNotNull(path);
			assertEquals(new Coord(3, 3), path.getDestination());

			path = mgr.getAreaPath(	new Coord(1, 1),
									new Coord(4, 4),
									MoveCostFactory.GROUND_MOVE_COST,
									3,
									3,
									new CoordSet(new Coord(3, 3)),
									false);
			assertNotNull(path);
			assertEquals(new Coord(4, 4), path.getDestination());

			// Sea move cost
			// -------------
			path = mgr.getAreaPath(	new Coord(0, 0),
									new Coord(4, 4),
									MoveCostFactory.DESTROYER_MOVE_COST,
									10,
									10,
									new CoordSet(),
									false);
			assertNotNull(path);
			assertEquals(new Coord(4, 4), path.getDestination());

			path = mgr.getAreaPath(	new Coord(1, 1),
									new Coord(4, 4),
									MoveCostFactory.DESTROYER_MOVE_COST,
									10,
									10,
									new CoordSet(),
									false);
			assertNotNull(path);
			assertEquals(new Coord(4, 4), path.getDestination());

			path = mgr.getAreaPath(	new Coord(1, 1),
									new Coord(0, 1),
									MoveCostFactory.DESTROYER_MOVE_COST,
									10,
									10,
									new CoordSet(),
									false);
			assertNotNull(path);
			assertEquals(new Coord(0, 1), path.getDestination());

			path = mgr.getAreaPath(	new Coord(0, 0),
									new Coord(0, 3),
									MoveCostFactory.DESTROYER_MOVE_COST,
									10,
									10,
									new CoordSet(),
									true);
			assertNotNull(path);
			assertEquals(new Coord(0, 2), path.getDestination());

			// PATH OVER THE START AREA
			// ========================

			// Ground Move Cost
			// ----------------
			path = mgr.getAreaPath(	new Coord(1, 1),
									new Coord(5, 5),
									MoveCostFactory.GROUND_MOVE_COST,
									3,
									3,
									new CoordSet(),
									true);
			assertNotNull(path);
			assertEquals(new Coord(4, 4), path.getDestination());

			// Sea move cost
			// -------------
			path = mgr.getAreaPath(	new Coord(0, 0),
									new Coord(3, 1),
									MoveCostFactory.DESTROYER_MOVE_COST,
									10,
									10,
									new CoordSet(),
									true);
			assertNotNull(path);
			assertEquals(new Coord(2, 0), path.getDestination());

			path = mgr.getAreaPath(	new Coord(1, 1),
									new Coord(3, 3),
									MoveCostFactory.DESTROYER_MOVE_COST,
									10,
									10,
									new CoordSet(),
									true);
			assertNotNull(path);
			assertEquals(new Coord(4, 4), path.getDestination());

			try
			{
				mgr.getAreaPath(null, new Coord(3, 3), MoveCostFactory.DESTROYER_MOVE_COST, 10, 10, new CoordSet(), true);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPath(new Coord(1, 1), null, MoveCostFactory.DESTROYER_MOVE_COST, 10, 10, new CoordSet(), true);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPath(new Coord(1, 1), new Coord(3, 3), null, 10, 10, new CoordSet(), true);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPath(new Coord(1, 1),
								new Coord(3, 3),
								MoveCostFactory.DESTROYER_MOVE_COST,
								-1,
								10,
								new CoordSet(),
								true);
				fail("Invalid value for param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPath(new Coord(1, 1),
								new Coord(3, 3),
								MoveCostFactory.DESTROYER_MOVE_COST,
								10,
								2,
								new CoordSet(),
								true);
				fail("Invalid value for param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPath(new Coord(1, 1),
								new Coord(3, 3),
								MoveCostFactory.DESTROYER_MOVE_COST,
								0,
								0,
								new CoordSet(),
								true);
				fail("Invalid value for param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPath(new Coord(1, 1), new Coord(3, 3), MoveCostFactory.DESTROYER_MOVE_COST, 1, 10, null, true);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPath(new Coord(1, 1),
								new Coord(3, 3),
								MoveCostFactory.AIR_MOVE_COST,
								10,
								10,
								new CoordSet(),
								true);
				fail("Invalid move cost param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPath(new Coord(3, 3),
								new Coord(3, 3),
								MoveCostFactory.DESTROYER_MOVE_COST,
								3,
								3,
								new CoordSet(),
								true);
				fail("Invalid start & destination coord param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getAreaPathIncludingDestination_all()
	{
		try
		{
			final FSMap map = buildFSMap(super.STANDARD_MAP_1);
			final PathMgr mgr = new PathMgr(map);
			final Coord start = new Coord(1, 1);
			Path path = mgr.getAreaPathIncludingDestination(start,
															new Coord(3, 2),
															MoveCostFactory.GROUND_MOVE_COST,
															new CoordSet());
			assertNotNull(path);
			path = mgr.getAreaPathIncludingDestination(	start,
														new Coord(3, 2),
														MoveCostFactory.GROUND_MOVE_COST,
														new CoordSet());
			assertNotNull(path);
			path = mgr.getAreaPathIncludingDestination(	start,
														new Coord(3, 2),
														MoveCostFactory.GROUND_MOVE_COST,
														new CoordSet());
			assertNotNull(path);
			path = mgr.getAreaPathIncludingDestination(	start,
														new Coord(3, 3),
														MoveCostFactory.GROUND_MOVE_COST,
														new CoordSet(new Coord(2, 2)));
			assertNotNull(path);

			path = mgr.getAreaPathIncludingDestination(	start,
														new Coord(4, 4),
														MoveCostFactory.GROUND_MOVE_COST,
														new CoordSet());
			assertEquals(new Coord(2, 2), path.getCoordList().get(0));
			assertEquals(new Coord(3, 3), path.getCoordList().get(1));
			assertEquals(new Coord(4, 4), path.getCoordList().get(2));

			path = mgr.getAreaPathIncludingDestination(	start,
														new Coord(4, 4),
														MoveCostFactory.DESTROYER_MOVE_COST,
														new CoordSet(new Coord(2, 2)));
			assertNotNull(path);
			assertEquals(new Coord(2, 0), path.getCoordList().get(0));
			assertEquals(new Coord(3, 0), path.getCoordList().get(1));
			assertEquals(new Coord(4, 0), path.getCoordList().get(2));
			assertEquals(new Coord(5, 1), path.getCoordList().get(3));
			assertEquals(new Coord(5, 2), path.getCoordList().get(4));
			assertEquals(new Coord(5, 3), path.getCoordList().get(5));
			assertEquals(new Coord(4, 4), path.getCoordList().get(6));

			try
			{
				mgr.getAreaPathIncludingDestination(null,
													new Coord(2, 2),
													MoveCostFactory.GROUND_MOVE_COST,
													new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPathIncludingDestination(new Coord(1, 1),
													null,
													MoveCostFactory.GROUND_MOVE_COST,
													new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPathIncludingDestination(new Coord(1, 1), new Coord(2, 2), null, new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPathIncludingDestination(new Coord(1, 1),
													new Coord(2, 2),
													MoveCostFactory.GROUND_MOVE_COST,
													null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAreaPathIncludingDestination(new Coord(1, 1),
													new Coord(1, 1),
													MoveCostFactory.GROUND_MOVE_COST,
													new CoordSet());
				fail("Invalid start & destination params has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			final Coord destination = new Coord(4, 4);
			try
			{
				mgr.getAreaPathIncludingDestination(new Coord(1, 1),
													destination,
													MoveCostFactory.GROUND_MOVE_COST,
													new CoordSet(destination));
				fail("Invalid destination has been accepted");
			} catch (PathNotFoundException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_isAccessibleAreaCoord_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final PathMgr mgr = new PathMgr(map);

			assertTrue(mgr.isAccessibleAreaCoord(	new Coord(1, 1),
													new Coord(1, 2),
													MoveCostFactory.GROUND_MOVE_COST,
													new CoordSet(),
													false));

			assertTrue(mgr.isAccessibleAreaCoord(	new Coord(1, 1),
													new Coord(4, 4),
													MoveCostFactory.GROUND_MOVE_COST,
													new CoordSet(),
													false));
			assertTrue(mgr.isAccessibleAreaCoord(	new Coord(1, 1),
													new Coord(4, 4),
													MoveCostFactory.GROUND_MOVE_COST,
													new CoordSet(),
													true));
			assertTrue(mgr.isAccessibleAreaCoord(	new Coord(1, 0),
													new Coord(4, 4),
													MoveCostFactory.DESTROYER_MOVE_COST,
													new CoordSet(),
													false));

			assertFalse(mgr.isAccessibleAreaCoord(	new Coord(1, 0),
													new Coord(3, 3),
													MoveCostFactory.DESTROYER_MOVE_COST,
													new CoordSet(),
													false));
			assertTrue(mgr.isAccessibleAreaCoord(	new Coord(1, 0),
													new Coord(3, 3),
													MoveCostFactory.DESTROYER_MOVE_COST,
													new CoordSet(),
													true));

			try
			{
				mgr.isAccessibleAreaCoord(	null,
											new Coord(1, 2),
											MoveCostFactory.GROUND_MOVE_COST,
											new CoordSet(),
											false);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				mgr.isAccessibleAreaCoord(	new Coord(1, 1),
											null,
											MoveCostFactory.GROUND_MOVE_COST,
											new CoordSet(),
											false);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.isAccessibleAreaCoord(new Coord(1, 1), new Coord(1, 2), null, new CoordSet(), false);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.isAccessibleAreaCoord(	new Coord(1, 1),
											new Coord(1, 2),
											MoveCostFactory.GROUND_MOVE_COST,
											null,
											false);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.isAccessibleAreaCoord(	new Coord(1, 1),
											new Coord(1, 1),
											MoveCostFactory.GROUND_MOVE_COST,
											new CoordSet(),
											false);
				fail("Invalid start & destination coord param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getAirPathIncludingDestinationCoord_all()
	{
		try
		{
			FSMap map = buildFSMap(super.STANDARD_MAP_1);
			PathMgr mgr = new PathMgr(map);

			Path path = mgr.getAirPathIncludingDestinationCoord(new Coord(1, 1),
																new Coord(1, 2),
																10,
																10,
																COMPUTER_PLAYER_1,
																new CoordSet());
			assertNotNull(path);

			try
			{
				mgr.getAirPathIncludingDestinationCoord(null,
														new Coord(1, 2),
														10,
														10,
														COMPUTER_PLAYER_1,
														new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				mgr.getAirPathIncludingDestinationCoord(new Coord(1, 1),
														null,
														10,
														10,
														COMPUTER_PLAYER_1,
														new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				mgr.getAirPathIncludingDestinationCoord(new Coord(1, 1), new Coord(1, 2), 10, 10, null, new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				mgr.getAirPathIncludingDestinationCoord(new Coord(1, 1),
														new Coord(1, 2),
														10,
														10,
														COMPUTER_PLAYER_1,
														null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.getAirPathIncludingDestinationCoord(new Coord(1, 1),
														new Coord(1, 1),
														10,
														10,
														COMPUTER_PLAYER_1,
														new CoordSet());
				fail("Same Start and Destination coord has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			final Coord destination = new Coord(2, 2);
			try
			{
				mgr.getAirPathIncludingDestinationCoord(new Coord(1, 1),
														destination,
														10,
														10,
														COMPUTER_PLAYER_1,
														new CoordSet(destination));
				fail("Same Start and Destination coord has been accepted");
			} catch (PathNotFoundException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_isAccessibleAirCoord_all()
	{
		try
		{
			FSMap map = buildFSMap(super.STANDARD_MAP_1);
			PathMgr mgr = new PathMgr(map);
			assertTrue(mgr.isAccessibleAirCoord(new Coord(1, 1),
												new Coord(1, 2),
												10,
												10,
												COMPUTER_PLAYER_1,
												new CoordSet(),
												false));

			final CoordSet excludedCoordSet = new CoordSet();
			excludedCoordSet.add(new Coord(1, 0));
			excludedCoordSet.add(new Coord(0, 1));
			excludedCoordSet.add(new Coord(1, 1));
			assertFalse(mgr.isAccessibleAirCoord(	new Coord(0, 0),
													new Coord(1, 2),
													10,
													10,
													COMPUTER_PLAYER_1,
													excludedCoordSet,
													false));
			try
			{
				mgr.isAccessibleAirCoord(null, new Coord(1, 1), 10, 10, COMPUTER_PLAYER_1, new CoordSet(), false);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				mgr.isAccessibleAirCoord(new Coord(1, 1), null, 10, 10, COMPUTER_PLAYER_1, new CoordSet(), false);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				mgr.isAccessibleAirCoord(new Coord(1, 1), new Coord(1, 2), 10, 10, null, new CoordSet(), false);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			try
			{
				mgr.isAccessibleAirCoord(new Coord(1, 1), new Coord(1, 2), 10, 10, COMPUTER_PLAYER_1, null, false);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.isAccessibleAirCoord(	new Coord(1, 1),
											new Coord(1, 1),
											10,
											10,
											COMPUTER_PLAYER_1,
											new CoordSet(),
											false);
				fail("Same Start and Destination coord has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_isValidPath_all()
	{
		try
		{
			final FSMap map = buildFSMap(super.STANDARD_MAP_1);
			final PathMgr mgr = new PathMgr(map);

			final CoordList coordList = new CoordList();
			coordList.add(new Coord(1, 1));
			coordList.add(new Coord(1, 2));
			coordList.add(new Coord(2, 2));
			coordList.add(new Coord(3, 3));
			coordList.add(new Coord(4, 3));
			Path path = new Path(coordList);

			try
			{
				mgr.isValidPath(null, path, MoveCostFactory.GROUND_MOVE_COST);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.isValidPath(new Coord(2, 1), null, MoveCostFactory.GROUND_MOVE_COST);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				mgr.isValidPath(new Coord(2, 1), path, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			assertTrue(mgr.isValidPath(new Coord(2, 1), path, MoveCostFactory.GROUND_MOVE_COST));
			assertTrue(mgr.isValidPath(new Coord(0, 0), path, MoveCostFactory.AIR_MOVE_COST));
			assertFalse(mgr.isValidPath(new Coord(3, 3), path, MoveCostFactory.AIR_MOVE_COST));
			assertFalse(mgr.isValidPath(new Coord(0, 0), path, MoveCostFactory.DESTROYER_MOVE_COST));

			coordList.clear();
			coordList.add(new Coord(4, 4));
			assertTrue(mgr.isValidPath(new Coord(5, 5), path, MoveCostFactory.DESTROYER_MOVE_COST));

			coordList.add(new Coord(5, 2));
			assertFalse(mgr.isValidPath(new Coord(5, 5), path, MoveCostFactory.DESTROYER_MOVE_COST));
		} catch (Exception ex)
		{
			fail(ex);
		}

	}
}