/*
 * Created on May 5, 2005
 */
package org.jocelyn_chaumel.firststrike.core.map.path;

import java.io.File;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.FirstStrikeMgr;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

/**
 * @author Jocelyn Chaumel
 */
public class AirPathFinderTest extends MapTestCase
{

	/**
	 * @param aName
	 */
	public AirPathFinderTest(final String aName)
	{
		super(aName);
	}

	public final void test_getLongPath_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);

			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			final FSMapMgr mapMgr = game.getMapMgr();
			final PlayerMgr playerMgr = game.getPlayerMgr();
			final AbstractPlayerList playerSet = playerMgr.getPlayerList();
			final HumanPlayer human = (HumanPlayer) playerSet.get(0);
			// final ComputerPlayer computer = (ComputerPlayer)
			// playerSet.get(1);

			map.getCellAt(new Coord(1, 7)).getCity().setPlayer(human);

			CoordSet cityCoordSet = mapMgr.getCityList().getCityByPlayer(human).getCoordSet();

			final AirPathFinder apf = new AirPathFinder(map.getWidth(), map.getHeight(), mapMgr.getCityList());
			final CoordSet coordToExclude = new CoordSet(new Coord(4, 4));
			Coord start = new Coord(1, 1);
			Coord dest = new Coord(2, 9);

			// Starting location is a city
			// Destination location isn't a city
			Path result = apf.getLongPath(start, dest, 6, 7, cityCoordSet, coordToExclude);
			assertEquals(new Coord(1, 2), result.getNextStepWithoutConsume());
			assertTrue(result.getCoordList().contains(new Coord(1, 7)));
			assertEquals(new Coord(2, 9), result.getDestination());

			// Starting location isn't a city
			// Destination location is a city
			start = new Coord(0, 0);
			dest = new Coord(1, 7);
			result = apf.getLongPath(start, dest, 6, 7, cityCoordSet, coordToExclude);
			assertTrue(result.getCoordList().contains(new Coord(1, 1)));
			assertEquals(new Coord(1, 7), result.getDestination());

			// Starting location isn't a city
			// Destination location is a city
			start = new Coord(0, 3);
			dest = new Coord(1, 7);
			result = apf.getLongPath(start, dest, 3, 7, cityCoordSet, coordToExclude);
			assertTrue(result.getCoordList().contains(new Coord(1, 1)));
			assertEquals(new Coord(1, 7), result.getDestination());

			map.getCellAt(new Coord(7, 7)).getCity().setPlayer(human);
			map.getCellAt(new Coord(13, 1)).getCity().setPlayer(human);
			cityCoordSet = mapMgr.getCityList().getCityByPlayer(human).getCoordSet();

			// Starting location is a city
			// Destination location is a city
			start = new Coord(1, 1);
			dest = new Coord(13, 1);
			result = apf.getLongPath(start, dest, 6, 7, cityCoordSet, coordToExclude);
			assertFalse(result.getCoordList().contains(new Coord(1, 7)));
			assertTrue(result.getCoordList().contains(new Coord(7, 7)));
			assertTrue(result.getCoordList().contains(new Coord(13, 1)));
			assertEquals(new Coord(13, 1), result.getDestination());

			coordToExclude.add(new Coord(4, 3));
			coordToExclude.add(new Coord(4, 5));
			start = new Coord(3, 3);
			dest = new Coord(15, 3);
			result = apf.getLongPath(start, dest, 2, 7, cityCoordSet, coordToExclude);
			assertTrue(result.getCoordList().contains(new Coord(1, 7)));
			assertTrue(result.getCoordList().contains(new Coord(7, 7)));
			assertTrue(result.getCoordList().contains(new Coord(13, 1)));
			assertEquals(new Coord(15, 3), result.getDestination());

			try
			{
				apf.getLongPath(null, dest, 10, 10, cityCoordSet, new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.getLongPath(start, null, 10, 10, cityCoordSet, new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.getLongPath(start, dest, 0, 10, cityCoordSet, new CoordSet());
				fail("Invalid fuel param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.getLongPath(start, dest, 10, 1, cityCoordSet, new CoordSet());
				fail("Invalid fuel capacity param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.getLongPath(start, dest, 10, 10, null, new CoordSet());
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.getLongPath(start, dest, 10, 10, cityCoordSet, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.getLongPath(start, dest, 10, 10, new CoordSet(), new CoordSet());
				fail("Empty city list param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_findPath_all()
	{
		try
		{

			final FSMap map = buildFSMap(STANDARD_MAP_4);

			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();
			final FSMapMgr mapMgr = game.getMapMgr();

			final AirPathFinder apf = new AirPathFinder(map.getWidth(), map.getHeight(), mapMgr.getCityList());
			final CoordSet coordToExclude = new CoordSet();
			final CoordSet coordCitySet = new CoordSet();

			final Coord start = new Coord(1, 1);
			final Coord dest = new Coord(6, 1);

			// Direct Path
			Path resultedPath = apf.findPath(start, dest, 2, 3, coordCitySet, coordToExclude, false);
			Path expectedPath = apf.getDirectPath(start, dest);
			assertFalse(resultedPath.validateFuelAlimentation(2, 3, coordCitySet));
			assertEquals(expectedPath, resultedPath);

			// Indirect Path
			coordToExclude.add(new Coord(3, 1));
			resultedPath = apf.findPath(start, dest, 2, 3, coordCitySet, coordToExclude, false);
			expectedPath = apf.getIndirectPath(start, dest, coordToExclude);
			assertFalse(resultedPath.validateFuelAlimentation(2, 3, coordCitySet));
			assertEquals(expectedPath, resultedPath);

			// Need a longPath but no city ready to restore
			resultedPath = apf.findPath(start, dest, 2, 3, coordCitySet, coordToExclude, true);
			expectedPath = apf.getIndirectPath(start, dest, coordToExclude);
			assertFalse(resultedPath.validateFuelAlimentation(2, 3, coordCitySet));
			assertEquals(expectedPath, resultedPath);

			// Long Path
			coordToExclude.clear();
			coordCitySet.add(new Coord(3, 2));
			resultedPath = apf.findPath(start, dest, 2, 6, coordCitySet, coordToExclude, true);
			expectedPath = apf.getLongPath(start, dest, 2, 6, coordCitySet, coordToExclude);
			assertTrue(resultedPath.validateFuelAlimentation(2, 6, coordCitySet));
			assertEquals(expectedPath, resultedPath);

			// Reproduction d'un bug
			File mapFile = new File(ApplicationConfigurationMgr.getInstance().getMapDir(), "map 40x40\\map 40x40.fsm");
			String[][] twoPlayers = { { "HUMAN", "13", "34" }, { "COMPUTER", "25", "37" } };
			PlayerInfoList playerInfoLsit = buildPlayerInfoList(twoPlayers);
			FSMap mapTemp = FSMapMgr.loadMap(mapFile, true);
			final FirstStrikeGame gameTemp = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoLsit, mapTemp);
			gameTemp.startGame();
			final FSMapMgr mapMgrTemp = gameTemp.getMapMgr();
			final ComputerPlayer computer = (ComputerPlayer) gameTemp.getPlayerMgr().getPlayerList().get(1);
			computer.addCity(mapTemp.getCityList().getCityAtPosition(new Coord(35, 26)), true);
			computer.addCity(mapTemp.getCityList().getCityAtPosition(new Coord(37, 21)), true);
			computer.addCity(mapTemp.getCityList().getCityAtPosition(new Coord(34, 15)), true);
			computer.addCity(mapTemp.getCityList().getCityAtPosition(new Coord(28, 13)), true);

			final AirPathFinder apfTemp = new AirPathFinder(mapTemp.getWidth(),
															mapTemp.getHeight(),
															mapMgrTemp.getCityList());
			final CoordSet coordCitySetTemp = computer.getCitySet().getCoordSet();

			try
			{
				Path path = apfTemp.findPath(	new Coord(28, 13),
												new Coord(18, 38),
												10,
												20,
												coordCitySetTemp,
												new CoordSet(),
												true);
				assertTrue(path.contains(new Coord(25, 37)));
			} catch (PathNotFoundException ex)
			{
				fail(ex);
			}

			try
			{
				apf.findPath(null, dest, 2, 4, coordCitySet, coordToExclude, true);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.findPath(start, null, 2, 4, coordCitySet, coordToExclude, true);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.findPath(start, dest, 0, 4, coordCitySet, coordToExclude, true);
				fail("Invalid fuel param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.findPath(start, dest, 2, 1, coordCitySet, coordToExclude, true);
				fail("Invalid fuel capacity param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.findPath(start, dest, 2, 4, null, coordToExclude, true);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.findPath(start, dest, 2, 4, coordCitySet, null, true);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.findPath(start, start, 2, 4, coordCitySet, coordToExclude, true);
				fail("Same start & dest coord param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getDirectPath_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);

			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			final FSMapMgr mapMgr = game.getMapMgr();

			final AirPathFinder apf = new AirPathFinder(map.getWidth(), map.getHeight(), mapMgr.getCityList());

			final Coord start = new Coord(1, 1);
			Coord dest = new Coord(2, 1);

			// Direct Path
			final CoordList expectedPath = new CoordList();
			expectedPath.add(dest);

			Path resultedPath = apf.getDirectPath(start, dest);
			assertEquals(expectedPath, resultedPath.getCoordList());

			dest = new Coord(3, 1);
			expectedPath.add(dest);
			resultedPath = apf.getDirectPath(start, dest);
			assertEquals(expectedPath, resultedPath.getCoordList());

			dest = new Coord(5, 5);
			expectedPath.clear();
			expectedPath.add(new Coord(2, 2));
			expectedPath.add(new Coord(3, 3));
			expectedPath.add(new Coord(4, 4));
			expectedPath.add(dest);
			resultedPath = apf.getDirectPath(start, dest);
			assertEquals(expectedPath, resultedPath.getCoordList());

			dest = new Coord(7, 3);
			expectedPath.clear();
			expectedPath.add(new Coord(2, 1));
			expectedPath.add(new Coord(3, 2));
			expectedPath.add(new Coord(4, 2));
			expectedPath.add(new Coord(5, 2));
			expectedPath.add(new Coord(6, 3));
			expectedPath.add(dest);
			resultedPath = apf.getDirectPath(start, dest);
			assertEquals(expectedPath, resultedPath.getCoordList());

			dest = new Coord(11, 3);
			expectedPath.clear();
			expectedPath.add(new Coord(2, 1));
			expectedPath.add(new Coord(3, 1));
			expectedPath.add(new Coord(4, 2));
			expectedPath.add(new Coord(5, 2));
			expectedPath.add(new Coord(6, 2));
			expectedPath.add(new Coord(7, 2));
			expectedPath.add(new Coord(8, 2));
			expectedPath.add(new Coord(9, 3));
			expectedPath.add(new Coord(10, 3));
			expectedPath.add(dest);
			resultedPath = apf.getDirectPath(start, dest);
			assertEquals(expectedPath, resultedPath.getCoordList());

			try
			{
				apf.getDirectPath(null, dest);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.getDirectPath(start, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.getDirectPath(start, start);
				fail("Same start & dest coord param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getIndirectPath_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);

			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			final FSMapMgr mapMgr = game.getMapMgr();

			final AirPathFinder apf = new AirPathFinder(map.getWidth(), map.getHeight(), mapMgr.getCityList());

			final Coord start = new Coord(1, 1);
			Coord dest = new Coord(2, 1);

			// Direct Path
			final CoordList expectedPath = new CoordList();
			final CoordSet excludedCoordSet = new CoordSet();
			expectedPath.add(dest);

			Path resultedPath = apf.getIndirectPath(start, dest, excludedCoordSet);
			assertEquals(expectedPath, resultedPath.getCoordList());

			dest = new Coord(3, 1);
			expectedPath.add(dest);
			resultedPath = apf.getIndirectPath(start, dest, excludedCoordSet);
			assertEquals(expectedPath, resultedPath.getCoordList());

			dest = new Coord(5, 5);
			expectedPath.clear();
			expectedPath.add(new Coord(2, 2));
			expectedPath.add(new Coord(3, 3));
			expectedPath.add(new Coord(4, 4));
			expectedPath.add(dest);
			resultedPath = apf.getIndirectPath(start, dest, excludedCoordSet);
			assertEquals(expectedPath, resultedPath.getCoordList());

			dest = new Coord(7, 3);
			expectedPath.clear();
			expectedPath.add(new Coord(2, 2));
			expectedPath.add(new Coord(3, 3));
			expectedPath.add(new Coord(4, 3));
			expectedPath.add(new Coord(5, 3));
			expectedPath.add(new Coord(6, 3));
			expectedPath.add(dest);
			resultedPath = apf.getIndirectPath(start, dest, excludedCoordSet);
			assertEquals(expectedPath, resultedPath.getCoordList());

			excludedCoordSet.add(new Coord(3, 3));

			expectedPath.clear();
			expectedPath.add(new Coord(2, 2));
			expectedPath.add(new Coord(3, 2));
			expectedPath.add(new Coord(4, 3));
			expectedPath.add(new Coord(5, 3));
			expectedPath.add(new Coord(6, 3));
			expectedPath.add(dest);
			resultedPath = apf.getIndirectPath(start, dest, excludedCoordSet);
			assertEquals(expectedPath, resultedPath.getCoordList());

			excludedCoordSet.add(new Coord(3, 2));

			expectedPath.clear();
			expectedPath.add(new Coord(2, 2));
			expectedPath.add(new Coord(3, 1));
			expectedPath.add(new Coord(4, 2));
			expectedPath.add(new Coord(5, 3));
			expectedPath.add(new Coord(6, 3));
			expectedPath.add(dest);
			resultedPath = apf.getIndirectPath(start, dest, excludedCoordSet);
			assertEquals(expectedPath, resultedPath.getCoordList());

			excludedCoordSet.clear();
			dest = new Coord(11, 3);
			expectedPath.clear();
			expectedPath.add(new Coord(2, 2));
			expectedPath.add(new Coord(3, 3));
			expectedPath.add(new Coord(4, 3));
			expectedPath.add(new Coord(5, 3));
			expectedPath.add(new Coord(6, 3));
			expectedPath.add(new Coord(7, 3));
			expectedPath.add(new Coord(8, 3));
			expectedPath.add(new Coord(9, 3));
			expectedPath.add(new Coord(10, 3));
			expectedPath.add(dest);
			resultedPath = apf.getIndirectPath(start, dest, excludedCoordSet);
			assertEquals(expectedPath, resultedPath.getCoordList());

			try
			{
				apf.getIndirectPath(null, dest, excludedCoordSet);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.getIndirectPath(start, null, excludedCoordSet);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.getIndirectPath(start, dest, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				apf.getIndirectPath(start, start, excludedCoordSet);
				fail("Same start & dest coord param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			excludedCoordSet.clear();
			excludedCoordSet.add(new Coord(1, 0));
			excludedCoordSet.add(new Coord(0, 1));
			excludedCoordSet.add(new Coord(1, 1));
			try
			{
				apf.getIndirectPath(new Coord(0, 0), new Coord(2, 2), excludedCoordSet);
				fail("Invalid path has been created");
			} catch (PathNotFoundException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
