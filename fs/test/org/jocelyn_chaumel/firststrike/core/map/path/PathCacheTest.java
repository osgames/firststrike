package org.jocelyn_chaumel.firststrike.core.map.path;

import org.jocelyn_chaumel.firststrike.FSTestCase;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordList;

public class PathCacheTest extends FSTestCase
{

	public PathCacheTest(final String aName)
	{
		super(aName);
	}

	public final void test_addPath_nullParam_1()
	{
		final PathCache cache = new PathCache();
		final Coord c1 = new Coord(1, 1);
		final Coord c2 = new Coord(2, 1);
		final CoordList list = new CoordList();
		list.add(c2);
		final Path path = new Path(list);
		try
		{
			cache.addPathInCache(null, c1, c2, path);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}

	public final void test_addPath_nullParam_2()
	{
		final PathCache cache = new PathCache();
		final Coord c2 = new Coord(2, 1);
		final CoordList list = new CoordList();
		list.add(c2);
		final Path path = new Path(list);
		try
		{
			cache.addPathInCache(MoveCostFactory.GROUND_MOVE_COST, null, c2, path);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}

	public final void test_addPath_nullParam_3()
	{
		final PathCache cache = new PathCache();
		final Coord c1 = new Coord(1, 1);
		final Coord c2 = new Coord(2, 1);
		final Path path = new Path(c2);
		try
		{
			cache.addPathInCache(MoveCostFactory.GROUND_MOVE_COST, c1, null, path);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}

	public final void test_addPath_nullParam_4()
	{
		final PathCache cache = new PathCache();
		final Coord c1 = new Coord(1, 1);
		final Coord c2 = new Coord(2, 1);
		try
		{
			cache.addPathInCache(MoveCostFactory.GROUND_MOVE_COST, c1, c2, null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}

	public final void test_addPath_sameCoord()
	{
		final PathCache cache = new PathCache();
		final Coord c2 = new Coord(2, 1);
		try
		{
			cache.addPathInCache(MoveCostFactory.GROUND_MOVE_COST, c2, c2, new Path(c2));
			fail("Invalid position coord param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}

	public final void test_addPath_invalidStartCoord()
	{
		final PathCache cache = new PathCache();
		final Coord c1 = new Coord(1, 1);
		final Coord c2 = new Coord(2, 1);
		try
		{
			cache.addPathInCache(MoveCostFactory.GROUND_MOVE_COST, c1, c2, new Path(c1));
			fail("Invalid start coord param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}

	public final void test_addPath_invalidDestCoord()
	{
		final PathCache cache = new PathCache();
		final Coord c1 = new Coord(1, 1);
		final Coord c2 = new Coord(2, 1);
		try
		{
			cache.addPathInCache(MoveCostFactory.GROUND_MOVE_COST, c2, c1, new Path(new Coord(3, 1)));
			fail("Invalid destination coord param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}

	public final void test_getPath_nullParam_1()
	{
		final PathCache cache = new PathCache();
		try
		{

			cache.getPathFromCache(null, new Coord(1, 1), new Coord(1, 2));

			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}

	public final void test_getPath_nullParam_2()
	{
		final PathCache cache = new PathCache();
		try
		{
			cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, null, new Coord(1, 2));

			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}

	public final void test_getPath_nullParam_3()
	{
		final PathCache cache = new PathCache();
		try
		{
			cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, new Coord(1, 2), null);

			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		}
	}

	public final void test_getPath_valid()
	{
		try
		{
			final PathCache cache = new PathCache();
			final Coord c1 = new Coord(1, 1);
			final Coord c2 = new Coord(2, 1);
			final Coord c3 = new Coord(1, 2);
			Path path = new Path(c2);

			assertTrue(null == cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, c1, c2));
			cache.addPathInCache(MoveCostFactory.GROUND_MOVE_COST, c1, c2, path);
			assertEquals(c2, cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, c1, c2).getDestination());
			assertEquals(c2, cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, c1, c2).getNextStep());
			assertEquals(c1, cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, c2, c1).getDestination());
			assertNull(cache.getPathFromCache(MoveCostFactory.AIR_MOVE_COST, c1, c2));
			assertNull(cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, c1, c3));
			path.getNextStep();
			assertEquals(c2, cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, c1, c2).getNextStep());

			final CoordList list2 = new CoordList();
			list2.add(c2);
			list2.add(c1);
			Path path2 = new Path(list2);
			cache.addPathInCache(MoveCostFactory.GROUND_MOVE_COST, c3, c1, path2);
			assertEquals(c2, cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, c1, c3).getNextStep());
			assertEquals(c3, cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, c1, c3).getDestination());
			assertEquals(c2, cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, c3, c1).getNextStep());
			assertEquals(c1, cache.getPathFromCache(MoveCostFactory.GROUND_MOVE_COST, c3, c1).getDestination());
		} catch (Exception ex)
		{
			fail(ex);
		}

	}
}
