package org.jocelyn_chaumel.firststrike.core.player;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.unit.Battleship;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.tools.data_type.Coord;

public class ComputerPlayerTest extends MapTestCase
{
	public static int unitId = 0;

	public ComputerPlayerTest(String p_aName)
	{
		super(p_aName);
	}

	public final void test_getTransportWithoutAnyProtection_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame(	"gameName",
																playerMgr,
																mapMgr,
																new FSRandomMgr(2),
																new FSOptionMgr());
			game.startGame();
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			final Destroyer computerDestroyer1 = new Destroyer(new Coord(11, 5), computer);
			computer.addUnit(computerDestroyer1, true);

			final TransportShip computerTransport1 = new TransportShip(new Coord(11, 11), computer);
			computer.addUnit(computerTransport1, true);

			assertEquals(1, computer.getTransportWithoutProtection().size());
			assertTrue(computer.getTransportWithoutProtection().contains(computerTransport1));

			computerDestroyer1.setProtectedTransport(computerTransport1);
			assertEquals(0, computer.getTransportWithoutProtection().size());
			assertFalse(computer.getTransportWithoutProtection().contains(computerTransport1));

			final TransportShip computerTransport2 = new TransportShip(new Coord(11, 11), computer);
			computer.addUnit(computerTransport2, true);

			assertEquals(1, computer.getTransportWithoutProtection().size());
			assertTrue(computer.getTransportWithoutProtection().contains(computerTransport2));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}
