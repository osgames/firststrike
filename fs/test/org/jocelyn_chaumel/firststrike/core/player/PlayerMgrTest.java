/*
 * Created on Dec 22, 2004
 */
package org.jocelyn_chaumel.firststrike.core.player;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.FirstStrikeMgr;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.scenario.MapScenario;
import org.jocelyn_chaumel.firststrike.core.player.cst.PlayerTypeCst;
import org.jocelyn_chaumel.firststrike.data_type.PlayerInfo;
import org.jocelyn_chaumel.tools.data_type.Coord;

/**
 * @author Jocelyn Chaumel
 */
public class PlayerMgrTest extends MapTestCase
{
	public PlayerMgrTest(final String aName)
	{
		super(aName);
	}

	public void test_cntr_nullParam()
	{
		try
		{
			try
			{
				new PlayerMgr(null);
				fail("Null param has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new PlayerMgr(new AbstractPlayerList());
				fail("Empty player list has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_createPlayers_nullParam_1()
	{
		try
		{
			PlayerMgr.createPlayers((PlayerInfoList) null, new FSMapMgr(buildFSMap(super.STANDARD_MAP_1)));
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}

		try
		{
			PlayerMgr.createPlayers((MapScenario) null, new FSMapMgr(buildFSMap(super.STANDARD_MAP_1)));
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_createPlayers_nullParam_2()
	{
		try
		{
			PlayerInfoList playerInfo = buildPlayerInfoList(TWO_PLAYERS);
			PlayerMgr.createPlayers(playerInfo, null);
			fail("Null param has been accepted.");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_createPlayers_arrayTooSmall()
	{
		try
		{
			final PlayerInfoList playerInfo = new PlayerInfoList();
			try
			{
				PlayerMgr.createPlayers(playerInfo, new FSMapMgr(buildFSMap(super.STANDARD_MAP_1)));
				fail("Invalid player info array has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_createPlayers_noHuman()
	{
		try
		{
			final PlayerInfo playerInfo1 = new PlayerInfo(	PlayerTypeCst.COMPUTER,
															new Coord(1, 1),
															PlayerLevelCst.NORMAL_LEVEL);
			final PlayerInfo playerInfo2 = new PlayerInfo(	PlayerTypeCst.COMPUTER,
															new Coord(4, 4),
															PlayerLevelCst.NORMAL_LEVEL);
			PlayerInfoList playerInfoList = new PlayerInfoList();
			playerInfoList.add(playerInfo1);
			playerInfoList.add(playerInfo2);
			try
			{
				PlayerMgr.createPlayers(playerInfoList, new FSMapMgr(buildFSMap(super.STANDARD_MAP_1)));
				fail("Invalid player info array has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_cntr_nullParam_arrayTooSmall()
	{
		try
		{
			final AbstractPlayerList playerListTooSmall = new AbstractPlayerList();
			try
			{
				new PlayerMgr(playerListTooSmall);
				fail("Invalid player List has been accepted.");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_nextPlayer_and_getTurnNumber_valid()
	{
		try
		{
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FSMap map = buildFSMap(super.STANDARD_MAP_1);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();
			final PlayerMgr playerMgr = game.getPlayerMgr();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);

			assertEquals(human, playerMgr.getCurrentPlayer());
			assertEquals(computer, playerMgr.nextPlayer());
			assertEquals(1, playerMgr.getTurnNumber());

			assertEquals(human, playerMgr.nextPlayer());
			assertEquals(2, playerMgr.getTurnNumber());
			assertEquals(computer, playerMgr.nextPlayer());
			assertEquals(2, playerMgr.getTurnNumber());

			assertEquals(human, playerMgr.nextPlayer());
			assertEquals(3, playerMgr.getTurnNumber());
			assertEquals(computer, playerMgr.nextPlayer());
			assertEquals(3, playerMgr.getTurnNumber());

			assertEquals(human, playerMgr.nextPlayer());
			assertEquals(4, playerMgr.getTurnNumber());

			map.getCityList().getCityAtPosition(new Coord(4, 4)).setPlayer(human);
			assertEquals(null, playerMgr.nextPlayer());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}