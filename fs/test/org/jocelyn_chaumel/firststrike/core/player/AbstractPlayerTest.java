/*
 * Created on Dec 8, 2004
 */
package org.jocelyn_chaumel.firststrike.core.player;

import org.jocelyn_chaumel.firststrike.FirstStrikeGame;
import org.jocelyn_chaumel.firststrike.FirstStrikeMgr;
import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.MoveCostFactory;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.UnitIndex;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

/**
 * @author Jocelyn Chaumel
 */
public class AbstractPlayerTest extends MapTestCase
{

	public AbstractPlayerTest(final String aName)
	{
		super(aName);
	}

	public final void testCntr_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			try
			{
				new ComputerPlayer(null, new Coord(1, 1), mapMgr, PlayerLevelCst.HARD_LEVEL, new UnitIndex(), null,
						null, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new ComputerPlayer(new PlayerId((short) 1), new Coord(1, 1), null, PlayerLevelCst.NORMAL_LEVEL,
						new UnitIndex(), null, null, null);
				fail("Null param has been accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			} catch (Exception ex)
			{
				fail(ex);
			}
			try
			{
				new ComputerPlayer(new PlayerId((short) 1), new Coord(1, 1), mapMgr, null, new UnitIndex(), null, null,
						null);
				fail("Null param has been accepted");

			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new HumanPlayer(new PlayerId((short) 1), new Coord(1, 1), null, PlayerLevelCst.EASY_LEVEL,
						new UnitIndex(), null, null, null, null);
				fail("Null param has been accepted");

			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new HumanPlayer(null, new Coord(1, 1), mapMgr, PlayerLevelCst.EASY_LEVEL, new UnitIndex(), null, null,
						null, null);
				fail("Null param has been accepted");

			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new HumanPlayer(new PlayerId((short) 1), new Coord(1, 1), mapMgr, null, new UnitIndex(), null, null,
						null, null);
				fail("Null param has been accepted");

			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new HumanPlayer(new PlayerId((short) 1), new Coord(1, 1), mapMgr, PlayerLevelCst.EASY_LEVEL, null, null,
						null, null, null);
				fail("Null param has been accepted");

			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			new HumanPlayer(new PlayerId((short) 1), new Coord(1, 1), mapMgr, PlayerLevelCst.EASY_LEVEL,
					new UnitIndex(), null, null, null, null);
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_isValidCoord_valid()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);

			final HumanPlayer human = (HumanPlayer) game.getPlayerMgr().getPlayerList().get(0);
			assertTrue(human.isValidCoord(new Coord(0, 0)));
			assertTrue(human.isValidCoord(new Coord(0, 1)));
			assertTrue(human.isValidCoord(new Coord(0, 2)));
			assertTrue(human.isValidCoord(new Coord(0, 3)));
			assertFalse(human.isValidCoord(new Coord(0, 4)));
			assertTrue(human.isValidCoord(new Coord(1, 0)));
			assertTrue(human.isValidCoord(new Coord(1, 1)));
			assertTrue(human.isValidCoord(new Coord(1, 2)));
			assertTrue(human.isValidCoord(new Coord(1, 3)));
			assertFalse(human.isValidCoord(new Coord(1, 4)));
			assertTrue(human.isValidCoord(new Coord(2, 0)));
			assertTrue(human.isValidCoord(new Coord(2, 1)));
			assertTrue(human.isValidCoord(new Coord(2, 2)));
			assertTrue(human.isValidCoord(new Coord(2, 3)));
			assertFalse(human.isValidCoord(new Coord(2, 4)));
			assertTrue(human.isValidCoord(new Coord(3, 0)));
			assertTrue(human.isValidCoord(new Coord(3, 1)));
			assertTrue(human.isValidCoord(new Coord(3, 2)));
			assertTrue(human.isValidCoord(new Coord(3, 3)));
			assertFalse(human.isValidCoord(new Coord(3, 4)));
			assertFalse(human.isValidCoord(new Coord(4, 0)));
			assertFalse(human.isValidCoord(new Coord(4, 1)));
			assertFalse(human.isValidCoord(new Coord(4, 2)));
			assertFalse(human.isValidCoord(new Coord(4, 3)));
			assertFalse(human.isValidCoord(new Coord(4, 4)));

			assertFalse(human.isValidCoord(new Coord(5, 0)));
			assertFalse(human.isValidCoord(new Coord(5, 1)));
			assertFalse(human.isValidCoord(new Coord(5, 2)));
			assertFalse(human.isValidCoord(new Coord(5, 3)));
			assertFalse(human.isValidCoord(new Coord(5, 4)));
			assertFalse(human.isValidCoord(new Coord(5, 5)));
			assertFalse(human.isValidCoord(new Coord(4, 5)));
			assertFalse(human.isValidCoord(new Coord(3, 5)));
			assertFalse(human.isValidCoord(new Coord(2, 5)));
			assertFalse(human.isValidCoord(new Coord(1, 5)));
			assertFalse(human.isValidCoord(new Coord(0, 5)));

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_moveUnit_see_FirstStrikeGameTest()
	{
		// For testing moveUnit method, go to FirstStrikeGameTest
	}

	public final void test_isLiving_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();

			final HumanPlayer human = (HumanPlayer) game.getPlayerMgr().getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) game.getPlayerMgr().getPlayerList().get(1);

			assertTrue(computer.isLiving());
			final Tank computerTank = new Tank(new Coord(2, 2), computer);
			computer.addUnit(computerTank, true);
			map.getCellAt(4, 4).getCity().setPlayer(human);
			assertTrue(computer.isLiving());
			computer.killUnit(computerTank);
			assertFalse(computer.isLiving());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_startTurn_all()
	{
		try
		{
			assertFalse(ApplicationConfigurationMgr.getInstance().isDebugMode());

			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();
			final FSMapMgr mapMgr = game.getMapMgr();

			final HumanPlayer human = (HumanPlayer) game.getPlayerMgr().getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) game.getPlayerMgr().getPlayerList().get(1);

			final Fighter computerFighter1 = new Fighter(new Coord(5, 0), computer);
			computer.addUnit(computerFighter1, true);

			final Fighter humanFighter1 = new Fighter(new Coord(1, 1), human);
			human.addUnit(humanFighter1, true);

			human.startTurn(1);
			assertEquals(0, human.getEnemySet().size());
			assertEquals(16, human.getNoFogSet().size());

			Path path = mapMgr.getPath(new Coord(4, 0), new CoordSet(), humanFighter1, false);
			humanFighter1.setPath(path);
			human.moveUnit(humanFighter1);
			assertEquals(humanFighter1.getMovementCapacity() - 10, humanFighter1.getMove());
			assertEquals(humanFighter1.getFuelCapacity() - 10, humanFighter1.getFuel());
			assertEquals(0, human.getEnemySet().size());
			human.moveUnit(humanFighter1);
			assertEquals(humanFighter1.getMovementCapacity() - 20, humanFighter1.getMove());
			assertEquals(humanFighter1.getFuelCapacity() - 20, humanFighter1.getFuel());
			assertEquals(new Coord(3, 0), humanFighter1.getPosition());
			human.moveUnit(humanFighter1);
			assertEquals(humanFighter1.getMovementCapacity() - 30, humanFighter1.getMove());
			assertEquals(humanFighter1.getFuelCapacity() - 30, humanFighter1.getFuel());
			assertEquals(new Coord(4, 0), humanFighter1.getPosition());
			assertEquals(1, human.getEnemySet().size());
			human.startTurn(1);
			assertEquals(25, human.getNoFogSet().size());
			assertEquals(humanFighter1.getMovementCapacity(), humanFighter1.getMove());
			assertEquals(humanFighter1.getFuelCapacity() - 30, humanFighter1.getFuel());
			assertEquals(new Coord(4, 0), humanFighter1.getPosition());
			assertEquals(1, human.getEnemySet().size());

			path = mapMgr.getPath(new Coord(1, 1), new CoordSet(), humanFighter1, false);
			humanFighter1.setPath(path);
			human.moveUnit(humanFighter1);
			human.moveUnit(humanFighter1);
			human.moveUnit(humanFighter1);
			assertEquals(humanFighter1.getMovementCapacity() - 30, humanFighter1.getMove());
			assertEquals(humanFighter1.getFuelCapacity(), humanFighter1.getFuel());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_finishTurn_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();

			final HumanPlayer human = (HumanPlayer) game.getPlayerMgr().getPlayerList().get(0);

			final Fighter humanFighter1 = new Fighter(new Coord(1, 2), human);
			human.addUnit(humanFighter1, true);
			final Fighter humanFighter2 = new Fighter(new Coord(1, 2), human);
			human.addUnit(humanFighter2, true);

			human.startTurn(1);
			assertEquals(2, human.getUnitList().size());
			while (humanFighter1.getFuel() > MoveCostFactory.AIR_MOVE_COST.getMinCost())
			{
				humanFighter1.decreaseFuel();
				humanFighter2.decreaseFuel();
			}
			assertEquals(MoveCostFactory.AIR_MOVE_COST.getMinCost(), humanFighter1.getFuel());
			humanFighter2.setMove(MoveCostFactory.AIR_MOVE_COST.getMinCost());
			assertEquals(2, human.getUnitList().size());
			AbstractUnitList killedFighterSet = human.finishTurn();
			assertEquals(1, human.getUnitList().size());
			assertEquals(1, killedFighterSet.size());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_killUnit_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_4);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();

			final HumanPlayer human = (HumanPlayer) game.getPlayerMgr().getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) game.getPlayerMgr().getPlayerList().get(1);

			assertEquals(0, human.getEnemySet().size());
			assertEquals(0, computer.getEnemySet().size());

			final Fighter humanFighter1 = new Fighter(new Coord(2, 2), human);
			human.addUnit(humanFighter1, true);
			final Fighter humanFighter2 = new Fighter(new Coord(2, 2), human);
			human.addUnit(humanFighter2, true);
			final Fighter computerFighter1 = new Fighter(new Coord(3, 3), computer);
			computer.addUnit(computerFighter1, true);
			final Fighter computerFighter2 = new Fighter(new Coord(3, 3), computer);
			computer.addUnit(computerFighter2, true);

			human.startTurn(1);
			assertEquals(2, human.getEnemySet().size());
			assertEquals(2, computer.getEnemySet().size());

			human.killUnit(humanFighter1);
			assertEquals(2, human.getEnemySet().size());
			assertEquals(1, computer.getEnemySet().size());
			assertTrue(computer.getEnemySet().contains(humanFighter2));

			try
			{
				human.killUnit(null);
				fail("Null param has been executed");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getUnitSet_all()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_1);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final FirstStrikeGame game = FirstStrikeMgr.createFirstStrikeGame("game name", playerInfoList, map);
			game.startGame();

			final HumanPlayer human = (HumanPlayer) game.getPlayerMgr().getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) game.getPlayerMgr().getPlayerList().get(1);

			assertEquals(0, human.getUnitList().size());
			assertEquals(0, computer.getUnitList().size());

			final Fighter humanFighter1 = new Fighter(new Coord(2, 2), human);
			human.addUnit(humanFighter1, true);
			assertEquals(1, human.getUnitList().size());
			assertEquals(0, computer.getUnitList().size());

			final Fighter humanFighter2 = new Fighter(new Coord(2, 2), human);
			human.addUnit(humanFighter2, true);
			assertEquals(2, human.getUnitList().size());
			assertEquals(0, computer.getUnitList().size());

			final Fighter computerFighter1 = new Fighter(new Coord(3, 3), computer);
			computer.addUnit(computerFighter1, true);
			assertEquals(2, human.getUnitList().size());
			assertTrue(human.getUnitList().contains(humanFighter1));
			assertTrue(human.getUnitList().contains(humanFighter2));

			assertEquals(1, computer.getUnitList().size());
			assertTrue(computer.getUnitList().contains(computerFighter1));

			final Fighter computerFighter2 = new Fighter(new Coord(3, 3), computer);
			computer.addUnit(computerFighter2, true);
			assertEquals(2, human.getUnitList().size());
			assertTrue(human.getUnitList().contains(humanFighter1));
			assertTrue(human.getUnitList().contains(humanFighter2));

			assertEquals(2, computer.getUnitList().size());
			assertTrue(computer.getUnitList().contains(computerFighter1));
			assertTrue(computer.getUnitList().contains(computerFighter2));

			final Destroyer computerDestroyer1 = new Destroyer(new Coord(5, 5), computer);
			computer.addUnit(computerDestroyer1, true);
			assertEquals(2, human.getUnitList().size());
			assertTrue(human.getUnitList().contains(humanFighter1));
			assertTrue(human.getUnitList().contains(humanFighter2));

			assertEquals(3, computer.getUnitList().size());
			assertTrue(computer.getUnitList().contains(computerFighter1));
			assertTrue(computer.getUnitList().contains(computerFighter2));
			assertTrue(computer.getUnitList().contains(computerDestroyer1));

			try
			{
				human.killUnit(null);
				fail("Null param has been executed");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

}