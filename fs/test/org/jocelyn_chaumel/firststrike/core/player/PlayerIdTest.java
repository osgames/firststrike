/*
 * Created on Dec 4, 2004
 */
package org.jocelyn_chaumel.firststrike.core.player;

import org.jocelyn_chaumel.firststrike.FSTestCase;

/**
 * @author Jocelyn Chaumel
 */
public class PlayerIdTest extends FSTestCase
{
	public PlayerIdTest(final String aName)
	{
		super(aName);
	}

	public final void testCntr_invalid_1()
	{
		try
		{

			new PlayerId((short) -1);
			fail("Invalid player id has been accepted");

		} catch (IllegalArgumentException ex)
		{
			// NOP

		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
