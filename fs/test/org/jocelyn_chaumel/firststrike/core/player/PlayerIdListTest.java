/*
 * Created on Oct 14, 2005
 */
package org.jocelyn_chaumel.firststrike.core.player;

import org.jocelyn_chaumel.firststrike.FSTestCase;
import org.jocelyn_chaumel.tools.exception.InvalidResultException;

/**
 * @author Jocelyn Chaumel
 */
public class PlayerIdListTest extends FSTestCase
{

	/**
	 * @param aName
	 */
	public PlayerIdListTest(final String aName)
	{
		super(aName);
	}

	public final void test_getPlayerId_invalidPlayerId_1()
	{
		try
		{
			final PlayerIdList playerIdList = new PlayerIdList();
			try
			{
				playerIdList.getPlayerId(1);
				fail("Invalid param has been accepted.");
			} catch (InvalidResultException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getPlayerId_invalidPlayer_2()
	{
		try
		{
			final PlayerIdList playerIdList = new PlayerIdList();
			playerIdList.add(new PlayerId(1));
			try
			{
				playerIdList.getPlayerId(2);
				fail("Invalid param has been accepted.");
			} catch (InvalidResultException ex)
			{
				// NOP
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public final void test_getPlayerId_valid()
	{
		try
		{
			final PlayerIdList playerIdList = new PlayerIdList();
			playerIdList.add(new PlayerId(1));
			playerIdList.add(new PlayerId(2));

			assertEquals(2, playerIdList.getPlayerId(2).getId());
			assertEquals(1, playerIdList.getPlayerId(1).getId());
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
