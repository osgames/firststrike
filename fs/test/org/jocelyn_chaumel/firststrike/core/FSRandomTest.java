/*
 * Created on May 1, 2005
 */
package org.jocelyn_chaumel.firststrike.core;

import org.jocelyn_chaumel.firststrike.MapTestCase;
import org.jocelyn_chaumel.tools.data_type.FloatList;

/**
 * @author Jocelyn Chaumel
 */
public class FSRandomTest extends MapTestCase
{

	public FSRandomTest(final String _string)
	{
		super(_string);
	}

	public void test_getInstance_nullParm_array()
	{
		try
		{
			FSRandom.getInstance((float[]) null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_getInstance_nullParm_Float()
	{
		try
		{
			FSRandom.getInstance((FloatList) null);
			fail("Null param has been accepted");
		} catch (IllegalArgumentException ex)
		{
			// NOP
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_floatList_valid()
	{
		try
		{
			final FloatList floatList = new FloatList();
			floatList.add(new Float(0.1));
			floatList.add(new Float(0.2));
			floatList.add(new Float(0.3));
			floatList.add(new Float(0.4));
			floatList.add(new Float(0.5));

			FSRandom rnd = FSRandom.getInstance(floatList);

			assertEquals(0.1f, rnd.getRandomFloat(), 0.0001);
			assertEquals(0.2f, rnd.getRandomFloat(), 0.0001);
			assertEquals(0.3f, rnd.getRandomFloat(), 0.0001);
			assertEquals(0.4f, rnd.getRandomFloat(), 0.0001);
			assertEquals(0.5f, rnd.getRandomFloat(), 0.0001);

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_floatConst_valid()
	{
		try
		{
			final FSRandom rnd = FSRandom.getInstance(0.55f);

			for (int i = 0; i < 1000; i++)
			{
				assertEquals(0.55f, rnd.getRandomFloat(), 0.0001);
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_float_valid()
	{
		try
		{
			final FSRandom rnd = FSRandom.getInstance();
			rnd.reset();

			for (int i = 0; i < 1000; i++)
			{
				assertTrue(rnd.getRandomFloat() < 1.00000001f);
			}
		} catch (Exception ex)
		{
			fail(ex);
		}
	}
}
