package org.jocelyn_chaumel.firststrike;
/*
 * MapTestCase.java JUnit based test
 * 
 * Created on 15 mai 2003, 17:49
 */



import org.jocelyn_chaumel.firststrike.core.city.CityList;
import org.jocelyn_chaumel.firststrike.core.logging.FSLevel;
import org.jocelyn_chaumel.firststrike.core.logging.FSLogger;
import org.jocelyn_chaumel.firststrike.core.logging.FSLoggerManager;
import org.jocelyn_chaumel.firststrike.core.map.Cell;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapCells;
import org.jocelyn_chaumel.firststrike.core.map.FSMapInfo;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.InvalidMapException;
import org.jocelyn_chaumel.firststrike.core.map.cst.CellTypeCst;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerId;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.firststrike.core.unit.UnitIndex;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.cst.InvalidConstantValueException;

/**
 * @author Jocelyn Chaumel
 */
public abstract class MapTestCase extends FSTestCase
{
	protected final static FSLogger _logger = FSLoggerManager.getLogger(MapTestCase.class.getName());

	protected final CityList _emptyCityList = new CityList();

	protected static final int[][][] AREA_BUILD_MAP_2 = { { { 00, 00, 00, 00 }, // 0
			{ 00, 00, 00, 00 }, // 1
			{ 00, 00, 00, 00 } // 2
			}, { { 1, 1, 1, 1 }, // 0
					{ 1, 1, 1, 1 }, // 1
					{ 1, 1, 1, 1 }, // 2
			} };

	protected static final int[][][] AREA_BUILD_MAP_3 = { { { 00, 00, 00, 00 }, // 0
			{ 01, 01, 01, 01 }, // 1
			{ 01, 01, 01, 01 } // 2
			}, { // Ground
			{ 0, 0, 0, 0 }, // 0
					{ 2, 2, 2, 2 }, // 1
					{ 2, 2, 2, 2 } // 2
			}, { // Sea
			{ 1, 1, 1, 1 }, // 0
					{ 1, 1, 1, 1 }, // 1
					{ 0, 0, 0, 0 } // 2
			} };

	protected static final int[][][] AREA_BUILD_MAP_4 = { { { 01, 00, 00, 00 }, // 0
			{ 01, 01, 00, 00 }, // 1
			{ 01, 01, 01, 01 } // 2
			}, { // Ground
			{ 2, 0, 0, 0 }, // 0
					{ 2, 2, 0, 0 }, // 1
					{ 2, 2, 2, 2 } // 2
			}, { // Sea
			{ 1, 1, 1, 1 }, // 0
					{ 1, 1, 1, 1 }, // 1
					{ 0, 1, 1, 1 } // 2
			} };

	protected static final int[][][] AREA_BUILD_MAP_5 = { { { 01, 00, 02, 03 }, // 0
			{ 00, 01, 00, 04 }, // 1
			{ 01, 00, 01, 00 } // 2
			}, { // Ground
			{ 2, 0, 2, 2 }, // 0
					{ 0, 2, 0, 2 }, // 1
					{ 2, 0, 2, 0 } // 2
			}, { // Sea
			{ 1, 1, 0, 0 }, // 0
					{ 1, 1, 1, 0 }, // 1
					{ 1, 1, 1, 1 } // 2
			} };

	protected static final int[][][] AREA_BUILD_MAP_6 = { { { 01, 01, 00, 00, 00, 01, 01 }, // 0
			{ 01, 00, 01, 00, 01, 00, 01 }, // 1
			{ 01, 00, 01, 00, 01, 00, 01 } }, { // Ground
			{ 2, 2, 0, 0, 0, 3, 3 }, // 0
					{ 2, 0, 2, 0, 3, 0, 3 }, // 1
					{ 2, 0, 2, 0, 3, 0, 3 } // 2
			}, { // Sea
			{ 1, 1, 1, 1, 1, 1, 1 }, // 0
					{ 1, 1, 1, 1, 1, 1, 1 }, // 1
					{ 1, 1, 1, 1, 1, 1, 1 } // 2
			} };

	protected static final int[][][] AREA_BUILD_MAP_7 = { { { 01, 01, 00, 00, 00, 01, 00 }, // 0
			{ 01, 00, 01, 00, 01, 00, 00 }, // 1
			{ 01, 00, 01, 00, 01, 00, 01 } // 2
			}, { // Ground
			{ 2, 2, 0, 0, 0, 3, 0 }, // 0
					{ 2, 0, 2, 0, 3, 0, 0 }, // 1
					{ 2, 0, 2, 0, 3, 0, 4 } // 2
			}, { // Sea
			{ 1, 1, 1, 1, 1, 1, 1 }, // 0
					{ 1, 1, 1, 1, 1, 1, 1 }, // 1
					{ 1, 1, 1, 1, 1, 1, 1 } // 2
			} };

	protected static final int[][][] AREA_BUILD_MAP_8 = { { { 01, 01, 02 }, // 0
			{ 01, 00, 01 }, // 1
			{ 01, 01, 01 } // 2
			}, { // Ground
			{ 2, 2, 2 }, // 0
					{ 2, 0, 2 }, // 1
					{ 2, 2, 2 } // 2
			}, { // Sea
			{ 1, 1, 0 }, // 0
					{ 1, 1, 1 }, // 1
					{ 1, 1, 1 } // 2
			} };

	protected static final int[][][] AREA_BUILD_MAP_9 = { { { 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 }, // 0
			{ 00, 01, 02, 02, 00, 00, 02, 01, 02, 00 }, // 1
			{ 00, 02, 02, 00, 00, 00, 02, 04, 02, 00 }, // 2
			{ 00, 02, 02, 02, 00, 00, 02, 01, 02, 00 }, // 3
			{ 00, 02, 02, 02, 00, 00, 02, 02, 02, 00 }, // 4
			{ 00, 00, 00, 02, 02, 00, 00, 02, 02, 00 } // 5
			}, { // Ground
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 0
					{ 0, 2, 2, 2, 0, 0, 3, 3, 3, 0 }, // 1
					{ 0, 2, 2, 0, 0, 0, 3, 3, 3, 0 }, // 2
					{ 0, 2, 2, 2, 0, 0, 3, 3, 3, 0 }, // 3
					{ 0, 2, 2, 2, 0, 0, 3, 3, 3, 0 }, // 4
					{ 0, 0, 0, 2, 2, 0, 0, 3, 3, 0 } // 5
			}, { // Sea
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // 0
					{ 1, 1, 0, 0, 1, 1, 0, 1, 0, 1 }, // 1
					{ 1, 0, 0, 1, 1, 1, 0, 0, 0, 1 }, // 2
					{ 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, // 3
					{ 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, // 4
					{ 1, 1, 1, 0, 0, 1, 1, 0, 0, 1 } // 5
			} };

	protected static final int[][][] AREA_BUILD_MAP_10 = { { { 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 }, // 0
			{ 00, 01, 02, 02, 00, 00, 02, 01, 02, 00 }, // 1
			{ 00, 02, 02, 00, 00, 00, 02, 04, 02, 00 }, // 2
			{ 00, 02, 02, 02, 00, 00, 02, 02, 02, 00 }, // 3
			{ 00, 00, 00, 00, 00, 00, 02, 02, 02, 00 }, // 4
			{ 00, 00, 00, 02, 02, 02, 02, 02, 02, 00 }, // 5
			{ 00, 01, 00, 02, 02, 00, 00, 02, 02, 00 }, // 6
			{ 00, 02, 00, 00, 02, 00, 00, 02, 02, 00 }, // 7
			{ 00, 03, 02, 00, 00, 00, 00, 02, 00, 00 }, // 8
			{ 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 } // 9
			}, { // Ground
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 0
					{ 0, 2, 2, 2, 0, 0, 3, 3, 3, 0 }, // 1
					{ 0, 2, 2, 0, 0, 0, 3, 3, 3, 0 }, // 2
					{ 0, 2, 2, 2, 0, 0, 3, 3, 3, 0 }, // 3
					{ 0, 0, 0, 0, 0, 0, 3, 3, 3, 0 }, // 4
					{ 0, 0, 0, 3, 3, 3, 3, 3, 3, 0 }, // 5
					{ 0, 4, 0, 3, 3, 0, 0, 3, 3, 0 }, // 6
					{ 0, 4, 0, 0, 3, 0, 0, 3, 3, 0 }, // 7
					{ 0, 4, 4, 0, 0, 0, 0, 3, 0, 0 }, // 8
					{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } // 9
			}, { // Sea
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // 0
					{ 1, 1, 0, 0, 1, 1, 0, 1, 0, 1 }, // 1
					{ 1, 0, 0, 1, 1, 1, 0, 0, 0, 1 }, // 2
					{ 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 }, // 3
					{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1 }, // 4
					{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 1 }, // 5
					{ 1, 1, 1, 0, 0, 1, 1, 0, 0, 1 }, // 6
					{ 1, 0, 1, 1, 0, 1, 1, 0, 0, 1 }, // 7
					{ 1, 0, 0, 1, 1, 1, 1, 0, 1, 1 }, // 8
					{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } // 9
			} };

	protected static final int[][][] AREA_BUILD_MAP_11 = {
			{ 
				{ 01, 00, 01, 02, 03 }, 
				{ 02, 00, 00, 00, 02 }, 
				{ 02, 04, 04, 04, 02 }, 
				{ 02, 04, 04, 04, 02 },
				{ 02, 02, 04, 02, 01 }, 
				{ 02, 07, 02, 07, 07 } 
			}, { // Ground
				{ 02, 00, 02, 02, 02 }, 
				{ 02, 00, 00, 00, 02 }, 
				{ 02, 02, 02, 02, 02 }, 
				{ 02, 02, 02, 02, 02 },
				{ 02, 02, 02, 02, 02 }, 
				{ 02, 00, 02, 00, 00 } 
			}, { // Sea
				{ 01, 01, 01, 00, 00 }, 
				{ 00, 01, 01, 01, 00 }, 
				{ 00, 00, 00, 00, 00 }, 
				{ 00, 00, 00, 00, 00 },
				{ 00, 00, 00, 00, 00 }, 
				{ 00, 00, 00, 00, 00 } 
			} };

	protected static final int[][][] AREA_BUILD_MAP_12 = {
			{ { 01, 03, 00, 02, 03 }, { 02, 00, 00, 00, 02 }, { 00, 00, 00, 04, 02 }, { 02, 04, 00, 04, 07 },
					{ 02, 02, 00, 02, 03 }, { 01, 00, 00, 01, 01 } }, { // Ground
			{ 02, 02, 00, 03, 03 }, { 02, 00, 00, 00, 03 }, { 00, 00, 00, 03, 03 }, { 04, 04, 00, 03, 00 },
					{ 04, 04, 00, 03, 03 }, { 04, 00, 00, 03, 03 } }, { // Sea
			{ 01, 00, 01, 00, 00 }, { 00, 01, 01, 01, 00 }, { 01, 01, 01, 00, 00 }, { 00, 00, 01, 00, 00 },
					{ 00, 00, 01, 00, 00 }, { 01, 01, 01, 01, 00 } } };

	// 1 island : 2 different sea cell
	protected static final int[][] MAP_UNIQUE_AREA_INVALID_2 = { { 02, 01, 02, 04, 00 }, { 03, 02, 03, 01, 00 },
			{ 03, 07, 01, 04, 00 }, { 03, 02, 03, 02, 00 }, { 03, 00, 02, 02, 00 } };

	// 1 island : No sea port
	protected static final int[][] MAP_UNIQUE_AREA_INVALID_3 = { { 07, 02, 02, 04, 00 }, { 03, 02, 03, 02, 00 },
			{ 03, 07, 01, 04, 00 }, { 03, 02, 03, 02, 00 }, { 03, 02, 02, 02, 00 } };

	// 2 islands : 2 different wather
	protected static final int[][] MAP_MULTI_AREA_INVALID_1 = { { 00, 01, 02, 00, 00 }, { 03, 02, 03, 00, 00 },
			{ 00, 00, 00, 00, 00 }, { 03, 02, 03, 02, 00 }, { 03, 01, 00, 00, 00 } };

	// 2 islands : No sea port
	protected static final int[][] MAP_MULTI_AREA_INVALID_2 = { 
		{ 02, 04, 02, 00, 00 }, 
		{ 01, 02, 03, 00, 00 },
		{ 00, 00, 00, 00, 00 }, 
		{ 00, 02, 03, 02, 03 }, 
		{ 04, 02, 02, 03, 01 } };

	protected static final int[][] MAP_UNIQUE_AREA_WITH_PORT = { { 00, 00, 00, 00, 00 }, { 00, 02, 03, 02, 00 },
			{ 00, 02, 01, 02, 00 }, { 00, 02, 03, 01, 00 }, { 00, 00, 00, 00, 00 } };

	public int[][] MAP_1 = { 
			{ 00, 00, 00, 00, 00, 00, 00 }, 
			{ 00, 01, 01, 01, 01, 01, 00 },
			{ 00, 00, 00, 00, 00, 00, 00 }, 
			{ 00, 00, 00, 00, 00, 00, 00 },
			{ 00, 00, 00, 00, 00, 00, 00 } 
			};

	public int[][] MAP_2 = { 
			{ 00, 00, 00, 00, 00, 00, 00 }, 
			{ 00, 01, 02, 03, 04, 01, 00 },
			{ 00, 00, 00, 00, 00, 00, 00 },
			{ 00, 00, 00, 00, 00, 00, 00 },
			{ 00, 00, 00, 00, 00, 00, 00 }
			};

	public int[][] MAP_3 = { 
			{ 00, 00, 00, 00, 02, 00, 00 }, 
			{ 00, 01, 00, 01, 00, 01, 00 },
			{ 00, 00, 01, 00, 00, 00, 00 },
			{ 00, 00, 00, 00, 00, 00, 00 },
			{ 00, 00, 00, 00, 00, 00, 00 }
			};

	public int[][] MAP_4 = { 
			{ 01, 02, 02, 03, 02, 02, 01 }, 
			{ 00, 00, 00, 00, 00, 00, 00 },
			{ 01, 03, 01, 02, 04, 02, 01 },
			{ 00, 00, 00, 00, 00, 00, 00 },
			{ 00, 00, 00, 00, 00, 00, 00 },
			{ 00, 00, 00, 00, 00, 00, 00 }
			};

	public int[][] MAP_5 = { 
			{ 00, 00, 00, 00, 00 }, 
			{ 00, 01, 00, 00, 00 }, 
			{ 00, 02, 00, 00, 00 }, 
			{ 00, 02, 00, 00, 00 }, 
			{ 00, 01, 00, 00, 00 },
			{ 00, 00, 00, 00, 00 } };

	public int[][] MAP_6 = { 
			{ 01, 00, 01, 00, 00 }, 
			{ 01, 00, 03, 00, 00 }, 
			{ 02, 00, 03, 00, 00 }, 
			{ 03, 00, 03, 00, 00}, 
			{ 04, 00, 03, 00, 00 },
			{ 01, 00, 01, 00, 00 } };

	public int[][] MAP_7 = { 
			{ 01, 00, 01, 02, 03 }, 
			{ 02, 00, 00, 00, 02 }, 
			{ 02, 04, 04, 04, 02 },
			{ 02, 04, 04, 04, 03 }, 
			{ 02, 02, 04, 03, 02 }, 
			{ 02, 05, 01, 06, 07 } };

	public int[][] MAP_8 = { 
			{ 01, 02, 02, 02, 02, 04 }, 
			{ 06, 07, 07, 07, 07, 02 }, 
			{ 06, 07, 07, 07, 07, 02 },
			{ 06, 06, 03, 04, 04, 02 }, 
			{ 06, 02, 04, 07, 07, 02 }, 
			{ 02, 05, 02, 07, 07, 02 },
			{ 01, 04, 02, 02, 02, 02 } };

	public int[][] MAP_10 = { 
			{ 01, 01, 01, 01, 01 }, 
			{ 01, 01, 01, 01, 01 }, 
			{ 01, 01, 01, 01, 01 },
			{ 01, 01, 01, 01, 01 }, 
			{ 01, 01, 01, 01, 01 }, 
			{ 01, 01, 01, 01, 01 } };

	protected int[][] MAP_11 = { 
			{ 02, 02, 00, 00, 00, 03 }, 
			{ 03, 01, 00, 01, 00, 01 }, 
			{ 00, 00, 00, 00, 00, 00 },
			{ 06, 03, 00, 01, 00, 00 }, 
			{ 01, 01, 00, 03, 00, 00 }, 
			{ 01, 03, 00, 00, 00, 01 } };

	// 1 �le
	protected int[][] STANDARD_MAP_1 = { 
			{ 00, 00, 00, 00, 00, 00 }, 
			{ 00, 01, 02, 02, 02, 00 },
			{ 00, 02, 02, 02, 02, 00 }, 
			{ 00, 02, 02, 02, 02, 00 }, 
			{ 00, 02, 02, 02, 01, 00 },
			{ 02, 00, 00, 00, 00, 00 }};

	// 4 �les
	protected int[][] STANDARD_MAP_2 = { 
			{ 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 },
			{ 00, 01, 02, 02, 02, 00, 00, 01, 02, 02, 02, 00 }, 
			{ 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02, 00 },
			{ 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02, 00 }, 
			{ 00, 02, 02, 02, 01, 00, 00, 02, 02, 02, 01, 00 },
			{ 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 }, 
			{ 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 },
			{ 00, 01, 02, 02, 02, 00, 00, 01, 02, 02, 02, 00 }, 
			{ 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02, 00 },
			{ 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02, 00 }, 
			{ 00, 02, 02, 02, 01, 00, 00, 02, 02, 02, 01, 00 },
			{ 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 }, };

	// Sans eau
	protected int[][] STANDARD_MAP_3 = { 
			{ 02, 02, 02, 02, 03, 03 }, 
			{ 02, 01, 02, 02, 02, 03 },
			{ 02, 02, 02, 02, 02, 03 }, 
			{ 02, 02, 02, 02, 02, 03 }, 
			{ 02, 02, 02, 02, 01, 03 },
			{ 03, 02, 03, 03, 03, 03 }, };

	// 5 �les
	protected static int[][] STANDARD_MAP_4 = { 
		    { 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 },
			{ 00, 01, 02, 02, 02, 00, 00, 01, 02, 02, 02, 00, 00, 01, 02, 02, 02 },
			{ 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02 },
			{ 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02 },
			{ 00, 02, 02, 02, 01, 00, 00, 02, 02, 02, 01, 00, 00, 02, 02, 02, 02 },
			{ 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 02, 02, 02, 02 },
			{ 00, 02, 02, 02, 02, 00, 00, 00, 00, 00, 00, 00, 00, 02, 02, 02, 02 },
			{ 00, 01, 02, 02, 02, 00, 00, 01, 02, 02, 02, 00, 00, 02, 02, 02, 02 },
			{ 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02 },
			{ 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02, 00, 00, 02, 02, 02, 02 },
			{ 00, 02, 02, 02, 01, 00, 00, 02, 02, 02, 01, 00, 00, 02, 02, 02, 02 },
			{ 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 02, 02, 02, 02 }};
	protected static FSMap REAL_MAP_4;
	static
	{
		try
		{
			REAL_MAP_4 = buildFSMap(STANDARD_MAP_4);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	protected int[][] STANDARD_MAP_5 = { 
			{ 01, 02, 02, 02, 02, 03, 03, 01, 02, 02, 01, 03, 03, 01, 02, 02, 01 },
			{ 03, 02, 02, 02, 02, 03, 03, 02, 02, 02, 02, 03, 03, 02, 02, 02, 01 },
			{ 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02 },
			{ 02, 02, 02, 02, 01, 03, 03, 02, 02, 02, 01, 03, 03, 02, 02, 02, 02 },
			{ 07, 03, 02, 02, 03, 03, 03, 03, 03, 03, 03, 01, 03, 02, 02, 02, 02 },
			{ 03, 02, 02, 02, 02, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 01, 02, 02, 02, 03, 03, 02, 02, 02, 02, 03, 03, 02, 02, 02, 02 },
			{ 03, 02, 02, 02, 02, 03, 03, 02, 02, 02, 02, 03, 03, 02, 02, 02, 02 },
			{ 03, 02, 02, 02, 02, 03, 03, 02, 02, 02, 02, 03, 03, 02, 02, 01, 02 },
			{ 03, 02, 02, 02, 01, 03, 03, 01, 02, 01, 02, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 }, };

	protected int[][] STANDARD_MAP_6 = { 
			{ 00, 02, 02, 02, 02, 03, 03, 01, 02, 02, 02, 03, 03, 01, 02, 02, 02 },
			{ 03, 01, 02, 02, 02, 03, 03, 02, 02, 02, 01, 03, 03, 02, 02, 02, 01 },
			{ 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02, 02 },
			{ 02, 02, 02, 02, 06, 03, 03, 02, 02, 02, 07, 03, 03, 02, 02, 02, 07 },
			{ 07, 03, 02, 02, 01, 03, 03, 03, 03, 03, 03, 02, 03, 02, 02, 02, 02 },
			{ 03, 02, 02, 02, 02, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 01, 02, 02, 02, 03, 03, 02, 02, 02, 02, 03, 03, 02, 02, 02, 02 },
			{ 03, 02, 02, 02, 02, 03, 03, 02, 02, 02, 02, 03, 03, 02, 02, 02, 02 },
			{ 03, 02, 02, 02, 02, 03, 03, 02, 02, 02, 02, 03, 03, 02, 02, 01, 02 },
			{ 03, 02, 02, 02, 01, 03, 03, 01, 02, 01, 02, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 02, 02, 02 },
			{ 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 02, 03, 01, 02, 02, 02 }, };

	protected int[][] STANDARD_MAP_7 = { 
			{ 00, 00, 00, 00, 00, 00 }, 
			{ 00, 00, 00, 00, 00, 00 },
			{ 00, 00, 00, 00, 00, 00 }, 
			{ 00, 01, 02, 02, 02, 00 }, 
			{ 00, 02, 02, 02, 01, 00 },
			{ 02, 00, 00, 00, 00, 00 }, };

	protected int[][] STANDARD_MAP_8 = { 
			{ 00, 00, 00, 00, 00, 00 }, 
			{ 00, 00, 00, 00, 03, 00 },
			{ 00, 00, 00, 00, 00, 00 }, 
			{ 00, 01, 02, 02, 02, 00 }, 
			{ 00, 02, 02, 02, 01, 00 },
			{ 02, 00, 00, 00, 00, 00 }, };

	public final static PlayerId PLAYER_1 = new PlayerId(1);
	public final static PlayerId PLAYER_2 = new PlayerId(2);
	public final static PlayerId PLAYER_3 = new PlayerId(3);
	public final static PlayerId PLAYER_4 = new PlayerId(4);
	public final static UnitIndex unitIndex = new UnitIndex();

	public final static ComputerPlayer COMPUTER_PLAYER_1 = new ComputerPlayer(	PLAYER_1,
																				new Coord(1, 1),
																				new FSMapMgr(REAL_MAP_4),
																				PlayerLevelCst.NORMAL_LEVEL, 
																				unitIndex,
																				null, null, null);
	
	
	public final static ComputerPlayer COMPUTER_PLAYER_2 = new ComputerPlayer(	PLAYER_2,
																				new Coord(4, 4),
																				new FSMapMgr(REAL_MAP_4),
																				PlayerLevelCst.NORMAL_LEVEL, 
																				unitIndex,
																				null, null, null);
	public final static ComputerPlayer COMPUTER_PLAYER_3 = new ComputerPlayer(	PLAYER_3,
																				new Coord(7, 7),
																				new FSMapMgr(REAL_MAP_4),
																				PlayerLevelCst.NORMAL_LEVEL,
																				unitIndex,
																				null, null, null);

	public final static PlayerId[] PLAYER_ARRAY = { PLAYER_1, PLAYER_2, PLAYER_3 // ,
																					// PLAYER_4
	};
	
	public MapTestCase(final String aName)
	{
		super(aName);
		FSLoggerManager.setConsoleLogger(FSLevel.ALL);
	}

	public static Cell[][] buildCellArray(final int[][] p_intArray) throws InvalidConstantValueException
	{
		Cell currentCell = null;
		final Cell[][] cellArray = new Cell[p_intArray.length][p_intArray[0].length];
		for (int y = 0; y < p_intArray.length; y++)
		{
			for (int x = 0; x < p_intArray[0].length; x++)
			{
				currentCell = new Cell(CellTypeCst.getInstante(p_intArray[y][x]), 1, new Coord(x, y), null, false);
				cellArray[y][x] = currentCell;
			}
		}
		return cellArray;
	}

	public static FSMapCells buildFSMapCells(final int[][] p_intArray) throws InvalidConstantValueException,
			InvalidMapException
	{
		final Cell[][] cellTypeArray = buildCellArray(p_intArray);
		FSMapCells mapCells = new FSMapCells(cellTypeArray, null, true);

		return mapCells;
	}

	public static FSMap buildFSMap(final int[][] p_cellTypeIntArray) throws InvalidConstantValueException,
			InvalidMapException
	{
		FSMapCells cells = buildFSMapCells(p_cellTypeIntArray);
		FSMapInfo info = new FSMapInfo("map test", null,"no description", FSMapMgr.VERSION, p_cellTypeIntArray[0].length, p_cellTypeIntArray.length);
		FSMap map = new FSMap(info, cells);
		return map;
	}
}