/*
 * Created on Dec 18, 2004
 */
package org.jocelyn_chaumel.firststrike;

import org.jocelyn_chaumel.firststrike.core.FSOptionMgr;
import org.jocelyn_chaumel.firststrike.core.FSRandom;
import org.jocelyn_chaumel.firststrike.core.FSRandomMgr;
import org.jocelyn_chaumel.firststrike.core.area.Area;
import org.jocelyn_chaumel.firststrike.core.area.AreaTypeCst;
import org.jocelyn_chaumel.firststrike.core.city.City;
import org.jocelyn_chaumel.firststrike.core.directive.destroyer.DestroyerAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.fighter.FighterAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.tank.TankAutoDirective;
import org.jocelyn_chaumel.firststrike.core.directive.transportship.TransportShipAutoDirective;
import org.jocelyn_chaumel.firststrike.core.map.FSMap;
import org.jocelyn_chaumel.firststrike.core.map.FSMapMgr;
import org.jocelyn_chaumel.firststrike.core.map.path.Path;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayer;
import org.jocelyn_chaumel.firststrike.core.player.AbstractPlayerList;
import org.jocelyn_chaumel.firststrike.core.player.ComputerPlayer;
import org.jocelyn_chaumel.firststrike.core.player.HumanPlayer;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerMgr;
import org.jocelyn_chaumel.firststrike.core.unit.AbstractUnitList;
import org.jocelyn_chaumel.firststrike.core.unit.CombatResultCst;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTank;
import org.jocelyn_chaumel.firststrike.core.unit.ComputerTransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.Destroyer;
import org.jocelyn_chaumel.firststrike.core.unit.Fighter;
import org.jocelyn_chaumel.firststrike.core.unit.MoveStatusCst;
import org.jocelyn_chaumel.firststrike.core.unit.Tank;
import org.jocelyn_chaumel.firststrike.core.unit.TransportShip;
import org.jocelyn_chaumel.firststrike.core.unit.cst.UnitTypeCst;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.data_type.CoordSet;

/**
 * @author Jocelyn Chaumel
 */
public class FirstStrikeGameTest extends MapTestCase
{

	public FirstStrikeGameTest(final String aName)
	{
		super(aName);
	}

	public void test_constructor_null_param()
	{
		_logger.system("test_cntr_nullParam");
		try
		{

			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			try
			{
				new FirstStrikeGame("gameName", null, mapMgr, new FSRandomMgr(2), new FSOptionMgr());
				fail("Null param has been accepted");

			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);
			try
			{
				new FirstStrikeGame(null, playerMgr, mapMgr, new FSRandomMgr(2), new FSOptionMgr());
				fail("Null param has been accepted");

			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new FirstStrikeGame("gameName", playerMgr, null, new FSRandomMgr(2), new FSOptionMgr());
				fail("Null param has been accepted");

			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new FirstStrikeGame("gameName", playerMgr, mapMgr, null, new FSOptionMgr());
				fail("Null param has been accepted");

			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2), null);
				fail("Null param has been accepted");

			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_human_win()
	{
		_logger.system("test_cntr_nullParam");
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2),
					new FSOptionMgr());
			game.startGame();
			HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final City city = human.getCitySet().getCityAtPosition(new Coord(1, 1));
			game.performSetCityProductionAt(city.getPosition(), UnitTypeCst.TANK);
			game.performNextTurn();
			game.performNextTurn();
			game.performNextTurn();
			game.performNextTurn();
			AbstractUnitList enemyList = human.getEnemySet();
			assertEquals("Enemy list size", 0, enemyList.size());

			Tank tank = (Tank) human.getUnitList().get(0);
			FSRandom.getInstance(0.99f);
			Destroyer destroyer = new Destroyer(new Coord(5, 5), human);
			human.addUnit(destroyer, true);
			enemyList = human.getEnemySet();
			assertEquals("Enemy list size", 1, enemyList.size());
			Tank enemyTank = (Tank) enemyList.get(0);
			game.performAttackUnitAt(destroyer, enemyTank);
			assertEquals("Enemy list size", 0, enemyList.size());

			tank.setPath(human.getMapMgr().getPath(new Coord(4, 4), new CoordSet(), tank, false));
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, game.performMoveUnitToNextCoord(tank));
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, game.performMoveUnitToNextCoord(tank));

			// Conquer the last enemy city
			assertFalse(game.isGameOver());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, game.performMoveUnitToNextCoord(tank));
			assertTrue(game.isGameOver());
			AbstractPlayer winner = game.getWinner();
			assertTrue(winner instanceof HumanPlayer);

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_computer_win()
	{
		_logger.system("test_cntr_nullParam");
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);
			final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2),
					new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final City city = human.getCitySet().getCityAtPosition(new Coord(1, 1));
			game.performSetCityProductionAt(city.getPosition(), UnitTypeCst.TANK);
			game.performNextTurn();

			FSRandom.getInstance(0.99f);
			ComputerTank tank = new ComputerTank(new Coord(2, 2), computer);
			tank.addDirective(new TankAutoDirective(tank));
			computer.addUnit(tank, true);

			// Conquer the last enemy city
			assertFalse(game.isGameOver());
			AbstractPlayer winner = game.performNextTurn();
			assertNull(winner);
			winner = game.getWinner();
			assertTrue(winner instanceof ComputerPlayer);

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_humanAction_performAttackUnitAt()
	{
		_logger.system("test_cntr_nullParam");
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2),
					new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final City city = human.getCitySet().getCityAtPosition(new Coord(1, 1));
			game.performSetCityProductionAt(city.getPosition(), UnitTypeCst.TANK);
			game.performNextTurn();

			final ComputerTank computerTank = new ComputerTank(new Coord(2, 2), computer);
			computer.addUnit(computerTank, true);

			final Tank tank1 = new Tank(new Coord(1, 1), human);
			final Tank tank2 = new Tank(new Coord(1, 1), human);
			human.addUnit(tank1, true);
			human.addUnit(tank2, true);

			assertEquals(2, human.getUnitIndex().getUnitListAt(new Coord(1, 1)).size());
			assertEquals(1, human.getEnemySet().size());

			try
			{
				game.performAttackUnitAt(null, computerTank);
				fail("Illegal state exception expected");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			try
			{
				game.performAttackUnitAt(tank1, null);
				fail("Illegal state exception expected");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			// Attack with no win
			FSRandom.getInstance(0.5f);
			assertEquals(CombatResultCst.NO_WIN, game.performAttackUnitAt(tank1, computerTank).getCombatResult());
			assertEquals(2, human.getUnitIndex().getUnitListAt(new Coord(1, 1)).size());
			assertEquals(1, human.getEnemySet().size());
			assertEquals(1, tank1.getNbAttack());
			assertEquals(75, tank1.getMove());

			// Attack and win
			FSRandom.getInstance(0.99f);
			assertEquals(CombatResultCst.ATTACKER_WIN, game.performAttackUnitAt(tank2, computerTank).getCombatResult());
			assertEquals(2, human.getUnitIndex().getUnitListAt(new Coord(1, 1)).size());
			assertEquals(0, human.getEnemySet().size());
			assertEquals(1, tank2.getNbAttack());
			assertEquals(75, tank2.getMove());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_humanAction_performLoadAndUnloadTank()
	{
		_logger.system("test_cntr_nullParam");
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2),
					new FSOptionMgr());
			game.startGame();
			HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final City city = human.getCitySet().getCityAtPosition(new Coord(1, 1));
			game.performSetCityProductionAt(city.getPosition(), UnitTypeCst.TANK);
			game.performNextTurn();

			final Tank tank1 = new Tank(new Coord(1, 1), human);
			final Tank tank2 = new Tank(new Coord(1, 1), human);
			final Tank tank3 = new Tank(new Coord(1, 1), human);
			final Tank tank4 = new Tank(new Coord(1, 1), human);
			final TransportShip transport = new TransportShip(new Coord(0, 0), human);
			human.addUnit(tank1, true);
			human.addUnit(tank2, true);
			human.addUnit(tank3, true);
			human.addUnit(tank4, true);
			human.addUnit(transport, true);

			assertEquals(5, human.getUnitList().size());
			assertEquals(4, human.getUnitIndex().getUnitListAt(new Coord(1, 1)).size());
			assertEquals(1, human.getUnitIndex().getUnitListAt(new Coord(0, 0)).size());

			// Load
			assertEquals(tank1.getMovementCapacity(), tank1.getMove());
			assertFalse(tank1.isLoaded());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, game.performLoadUnit(tank1, transport));
			assertEquals(0, tank1.getMove());
			assertEquals(4, human.getUnitList().size());
			assertEquals(3, human.getUnitIndex().getUnitListAt(new Coord(1, 1)).size());
			assertEquals(1, human.getUnitIndex().getUnitListAt(new Coord(0, 0)).size());
			assertEquals(1, transport.getContainedUnitList().size());
			assertTrue(tank1.isLoaded());

			assertFalse(tank2.isLoaded());
			assertEquals(tank2.getMovementCapacity(), tank2.getMove());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, game.performLoadUnit(tank2, transport));
			assertEquals(0, tank2.getMove());
			assertEquals(3, human.getUnitList().size());
			assertEquals(2, human.getUnitIndex().getUnitListAt(new Coord(1, 1)).size());
			assertEquals(1, human.getUnitIndex().getUnitListAt(new Coord(0, 0)).size());
			assertEquals(2, transport.getContainedUnitList().size());
			assertTrue(tank2.isLoaded());

			tank3.setMove(0);
			assertFalse(tank3.isLoaded());
			try
			{
				assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, game.performLoadUnit(tank3, transport));
				fail("Illegal input parameter accepted");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}
			assertEquals(3, human.getUnitList().size());
			assertFalse(tank3.isLoaded());

			assertEquals(1, transport.getFreeSpace());
			transport.setPath(new Path(new Coord(1, 1)));
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, game.performMoveUnitToNextCoord(transport));
			tank3.restoreUnit();
			assertEquals(new Coord(1, 1), transport.getPosition());
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, game.performLoadUnit(tank3, transport));
			assertEquals(2, human.getUnitList().size());
			assertEquals(2, human.getUnitIndex().getUnitListAt(new Coord(1, 1)).size());
			assertNull(human.getUnitIndex().getUnitListAt(new Coord(0, 0)));
			assertEquals(3, transport.getContainedUnitList().size());
			assertTrue(tank3.isLoaded());
			assertEquals(0, transport.getFreeSpace());

			try
			{
				game.performLoadUnit(tank4, transport);
				fail("Illegal state exception expected");
			} catch (IllegalArgumentException ex)
			{
				// NOP
			}

			// Unload
			tank1.restoreUnit();
			assertNull(human.getUnitIndex().getUnitListAt(new Coord(2, 2)));
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, game.performUnloadUnit(tank1, new Coord(2, 2)));
			assertFalse(tank1.isLoaded());
			assertEquals(2, transport.getContainedUnitList().size());
			assertEquals(3, human.getUnitList().size());
			assertEquals(new Coord(2, 2), tank1.getPosition());
			assertEquals(0, tank1.getMove());
			assertEquals(1, human.getUnitIndex().getUnitListAt(new Coord(2, 2)).size());

			tank2.restoreUnit();
			assertEquals(MoveStatusCst.MOVE_SUCCESFULL, game.performUnloadUnit(tank2, new Coord(2, 2)));
			assertFalse(tank2.isLoaded());
			assertEquals(1, transport.getContainedUnitList().size());
			assertEquals(4, human.getUnitList().size());
			assertEquals(new Coord(2, 2), tank2.getPosition());
			assertEquals(0, tank2.getMove());
			assertEquals(2, human.getUnitIndex().getUnitListAt(new Coord(2, 2)).size());

			tank3.setMove(2);
			assertEquals(MoveStatusCst.NOT_ENOUGH_MOVE_PTS, game.performUnloadUnit(tank3, new Coord(2, 2)));
			assertTrue(tank3.isLoaded());
			assertEquals(1, transport.getContainedUnitList().size());
			assertEquals(4, human.getUnitList().size());
			assertEquals(2, human.getUnitIndex().getUnitListAt(new Coord(2, 2)).size());

			FSRandom.getInstance(0.99f);

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_humanAction_performSetCityContinueProductionAt()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2),
					new FSOptionMgr());
			game.startGame();
			HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final City city = human.getCitySet().getCityAtPosition(new Coord(1, 1));
			game.performSetCityProductionAt(city.getPosition(), UnitTypeCst.TANK);
			game.performNextTurn();

			assertFalse(city.isContinueProduction());
			game.performSetCityContinueProductionAt(new Coord(1, 1), true);
			assertTrue(city.isContinueProduction());

			try
			{
				game.performSetCityContinueProductionAt(null, true);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				game.performSetCityContinueProductionAt(new Coord(4, 4), true);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				game.performSetCityContinueProductionAt(new Coord(2, 2), true);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_humanAction_performSetCityProductionAt()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2),
					new FSOptionMgr());
			game.startGame();
			HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final City city = human.getCitySet().getCityAtPosition(new Coord(1, 1));

			assertFalse(city.isUnitUnderConstruction());
			game.performSetCityProductionAt(city.getPosition(), UnitTypeCst.TANK);
			assertTrue(city.isUnitUnderConstruction());
			assertEquals(UnitTypeCst.TANK, city.getProductionType());

			try
			{
				game.performSetCityProductionAt(null, UnitTypeCst.TANK);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				game.performSetCityProductionAt(new Coord(4, 4), UnitTypeCst.TANK);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

			try
			{
				game.performSetCityProductionAt(new Coord(2, 2), UnitTypeCst.TANK);
				fail();
			} catch (IllegalArgumentException ex)
			{
				// nop
			}

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	public void test_computerAction_nextTurn()
	{
		try
		{
			final FSMap map = buildFSMap(STANDARD_MAP_2);
			final FSMapMgr mapMgr = new FSMapMgr(map);

			final String[][] playersArray = { { "HUMAN", "1", "1" }, { "COMPUTER", "10", "10" } };
			final PlayerInfoList playerInfoList = buildPlayerInfoList(playersArray);
			final AbstractPlayerList playerList = PlayerMgr.createPlayers(playerInfoList, mapMgr);
			final PlayerMgr playerMgr = new PlayerMgr(playerList);

			final FirstStrikeGame game = new FirstStrikeGame("gameName", playerMgr, mapMgr, new FSRandomMgr(2),
					new FSOptionMgr());
			game.startGame();
			final HumanPlayer human = (HumanPlayer) playerMgr.getPlayerList().get(0);
			final ComputerPlayer computer = (ComputerPlayer) playerMgr.getPlayerList().get(1);
			final City city = human.getCitySet().getCityAtPosition(new Coord(1, 1));
			game.performSetCityProductionAt(city.getPosition(), UnitTypeCst.TANK);
			game.performNextTurn();

			// Human's unit
			final Tank humanTank = new Tank(new Coord(8, 8), human);
			human.addUnit(humanTank, true);

			// Computer's unit
			final ComputerTransportShip computerTransport = new ComputerTransportShip(new Coord(0, 11), computer);
			final Area computerArea = map.getAreaAt(new Coord(8, 8), AreaTypeCst.GROUND_AREA);
			computerTransport.addDirective(new TransportShipAutoDirective(computerTransport, computerArea));
			computer.addUnit(computerTransport, true);

			final Fighter computerFighter = new Fighter(new Coord(10, 10), computer);
			computerFighter.addDirective(new FighterAutoDirective(computerFighter));
			computer.addUnit(computerFighter, true);

			final Destroyer computerDestroyer = new Destroyer(new Coord(10, 10), computer);
			computerDestroyer.addDirective(new DestroyerAutoDirective(computerDestroyer));
			computer.addUnit(computerDestroyer, true);

			final ComputerTank computerTank = new ComputerTank(new Coord(10, 10), computer);
			computerTank.addDirective(new TankAutoDirective(computerTank));
			computer.addUnit(computerTank, true);

			FSRandom.getInstance(0.5f);
			assertEquals(4, computer.getUnitList().size());
			assertEquals(1, computer.getEnemySet().size());
			game.performNextTurn();
			assertFalse(humanTank.isLiving());

		} catch (Exception ex)
		{
			fail(ex);
		}
	}

	/**
	 * public final void test_startANDfinishHumanPlayer_valid() {
	 * _logger.system("test_startANDfinishHumanPlayer_valid"); try { //
	 * Sauvegarde la carte source sur le disque final FSMap map =
	 * buildFSMap(STANDARD_MAP_1);
	 * 
	 * final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
	 * final FirstStrikeGame game =
	 * FirstStrikeMgr.createFirstStrikeGame("gameName",playerInfoList, map);
	 * final PlayerMgr playerMgr = game.getPlayerMgr(); HumanPlayer human =
	 * (HumanPlayer) playerMgr.getPlayerList().get(0);
	 * 
	 * // Turn 1 assertEquals(human, game.getCurrentPlayer()); final City city =
	 * human.getCitySet().getCityAtPosition(new Coord(1, 1));
	 * city.setProductionType(UnitTypeCst.TANK);
	 * assertEquals(City.TANK_TIME_PRODUCTION, city.getProductionTime());
	 * assertEquals(1, game.getTurnNumber());
	 * assertFalse(human.getShadowSet().contains(new Coord(0, 0)));
	 * assertTrue(human.getNoFogSet().contains(new Coord(0, 0)));
	 * 
	 * assertFalse(human.getShadowSet().contains(new Coord(4, 0)));
	 * assertTrue(human.getNoFogSet().contains(new Coord(4, 0)));
	 * 
	 * assertFalse(human.getShadowSet().contains(new Coord(4, 4)));
	 * assertTrue(human.getNoFogSet().contains(new Coord(4, 4)));
	 * 
	 * assertFalse(human.getShadowSet().contains(new Coord(0, 4)));
	 * assertTrue(human.getNoFogSet().contains(new Coord(0, 4)));
	 * 
	 * assertTrue(human.getShadowSet().contains(new Coord(5, 0)));
	 * assertTrue(human.getShadowSet().contains(new Coord(5, 5)));
	 * assertTrue(human.getShadowSet().contains(new Coord(0, 5)));
	 * 
	 * // Turn 2 human = (HumanPlayer) game.nextTurn(); assertEquals(2,
	 * game.getTurnNumber()); assertEquals(City.TANK_TIME_PRODUCTION - 1,
	 * city.getProductionTime()); city.decProductionTime();
	 * 
	 * assertFalse(human.getShadowSet().contains(new Coord(0, 0)));
	 * assertTrue(human.getNoFogSet().contains(new Coord(0, 0)));
	 * 
	 * assertFalse(human.getShadowSet().contains(new Coord(4, 0)));
	 * assertTrue(human.getNoFogSet().contains(new Coord(4, 0)));
	 * 
	 * assertFalse(human.getShadowSet().contains(new Coord(4, 4)));
	 * assertTrue(human.getNoFogSet().contains(new Coord(4, 4)));
	 * 
	 * assertFalse(human.getShadowSet().contains(new Coord(0, 4)));
	 * assertTrue(human.getNoFogSet().contains(new Coord(0, 4)));
	 * 
	 * assertTrue(human.getShadowSet().contains(new Coord(5, 0)));
	 * assertTrue(human.getShadowSet().contains(new Coord(0, 5)));
	 * assertTrue(human.getShadowSet().contains(new Coord(1, 5)));
	 * assertTrue(human.getShadowSet().contains(new Coord(2, 5)));
	 * assertTrue(human.getShadowSet().contains(new Coord(3, 5)));
	 * assertTrue(human.getShadowSet().contains(new Coord(4, 5)));
	 * assertTrue(human.getShadowSet().contains(new Coord(5, 5)));
	 * 
	 * // Turn 3 assertTrue(city.getProductionType().equals(UnitTypeCst.TANK));
	 * human = (HumanPlayer) game.nextTurn(); assertEquals(3,
	 * game.getTurnNumber());
	 * assertTrue(city.getProductionType().equals(UnitTypeCst.NULL_TYPE));
	 * assertEquals(1, human.getUnitList().size()); Tank tank = (Tank)
	 * human.getUnitList().get(0); tank.setPath(game.getPath(tank, new Coord(1,
	 * 4))); game.moveUnitToNextCoord(tank); assertEquals(new Coord(1, 2),
	 * tank.getPosition());
	 * 
	 * game.moveUnitToNextCoord(tank); assertEquals(new Coord(1, 3),
	 * tank.getPosition());
	 * 
	 * game.moveUnitToNextCoord(tank); assertEquals(new Coord(1, 4),
	 * tank.getPosition());
	 * 
	 * assertFalse(human.getShadowSet().contains(new Coord(0, 5)));
	 * assertFalse(human.getShadowSet().contains(new Coord(1, 5)));
	 * assertFalse(human.getShadowSet().contains(new Coord(2, 5)));
	 * assertTrue(human.getShadowSet().contains(new Coord(3, 5)));
	 * assertTrue(human.getShadowSet().contains(new Coord(4, 5)));
	 * assertTrue(human.getShadowSet().contains(new Coord(5, 5)));
	 * 
	 * } catch (Exception ex) { fail(ex); } }
	 * 
	 * public final void test_isValidDestinationForAirUnit_all() { try { final
	 * FSMap map = buildFSMap(STANDARD_MAP_4);
	 * 
	 * final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
	 * final FirstStrikeGame game =
	 * FirstStrikeMgr.createFirstStrikeGame("gameName",playerInfoList, map);
	 * 
	 * assertTrue(game.isValidDestinationForAirUnit(new Coord(3, 3)));
	 * assertFalse(game.isValidDestinationForAirUnit(new Coord(4, 4)));
	 * assertFalse(game.isValidDestinationForAirUnit(new Coord(5, 5)));
	 * assertTrue(game.isValidDestinationForAirUnit(new Coord(0, 0)));
	 * 
	 * try { game.isValidDestinationForAirUnit(null); fail("Null param has been
	 * accepted"); } catch (IllegalArgumentException ex) { // HOP }
	 * 
	 * } catch (Exception ex) { fail(ex); } }
	 * 
	 * public final void test_isValidDestinationForGroundUnit_all() { try {
	 * final FSMap map = buildFSMap(STANDARD_MAP_4);
	 * 
	 * final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
	 * final FirstStrikeGame game =
	 * FirstStrikeMgr.createFirstStrikeGame("gameName",playerInfoList, map);
	 * 
	 * assertTrue(game.isValidDestinationForGroundUnit(new Coord(1, 1), new
	 * Coord(3, 3))); assertTrue(game.isValidDestinationForGroundUnit(new
	 * Coord(1, 1),new Coord(4, 4)));
	 * assertFalse(game.isValidDestinationForGroundUnit(new Coord(1, 1),new
	 * Coord(5, 5))); assertFalse(game.isValidDestinationForGroundUnit(new
	 * Coord(1, 1),new Coord(0, 0)));
	 * 
	 * 
	 * try { game.isValidDestinationForGroundUnit(null, new Coord(1, 1));
	 * fail("Null param has been accepted"); } catch (IllegalArgumentException
	 * ex) { // HOP }
	 * 
	 * try { game.isValidDestinationForGroundUnit(new Coord(1, 1),null);
	 * fail("Null param has been accepted"); } catch (IllegalArgumentException
	 * ex) { // HOP }
	 * 
	 * } catch (Exception ex) { fail(ex); } }
	 * 
	 * public final void test_isValidDestinationForSeaUnit_all() { try { final
	 * FSMap map = buildFSMap(STANDARD_MAP_4);
	 * 
	 * final PlayerInfoList playerInfoList = buildPlayerInfoList(TWO_PLAYERS);
	 * final FirstStrikeGame game =
	 * FirstStrikeMgr.createFirstStrikeGame("gameName",playerInfoList, map);
	 * 
	 * assertFalse(game.isValidDestinationForSeaUnit(new Coord(1, 1), new
	 * Coord(3, 3))); assertFalse(game.isValidDestinationForSeaUnit(new Coord(1,
	 * 1),new Coord(4, 4))); assertFalse(game.isValidDestinationForSeaUnit(new
	 * Coord(1, 1),new Coord(5, 5)));
	 * assertTrue(game.isValidDestinationForSeaUnit(new Coord(1, 1),new Coord(0,
	 * 0))); assertTrue(game.isValidDestinationForSeaUnit(new Coord(0, 0), new
	 * Coord(1, 1)));
	 * 
	 * 
	 * try { game.isValidDestinationForSeaUnit(null, new Coord(1, 1));
	 * fail("Null param has been accepted"); } catch (IllegalArgumentException
	 * ex) { // HOP }
	 * 
	 * try { game.isValidDestinationForSeaUnit(new Coord(1, 1),null); fail("Null
	 * param has been accepted"); } catch (IllegalArgumentException ex) { // HOP
	 * }
	 * 
	 * } catch (Exception ex) { fail(ex); } }
	 **/
}