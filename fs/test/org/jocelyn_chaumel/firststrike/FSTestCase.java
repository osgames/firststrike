package org.jocelyn_chaumel.firststrike;
/*
 * FSTestCase.java JUnit based test
 * 
 * Created on 22 mai 2003, 22:16
 */



import org.jocelyn_chaumel.firststrike.core.configuration.ApplicationConfigurationMgr;
import org.jocelyn_chaumel.firststrike.core.player.PlayerInfoList;
import org.jocelyn_chaumel.firststrike.core.player.PlayerLevelCst;
import org.jocelyn_chaumel.firststrike.core.player.cst.PlayerTypeCst;
import org.jocelyn_chaumel.firststrike.data_type.PlayerInfo;
import org.jocelyn_chaumel.tools.FileMgr;
import org.jocelyn_chaumel.tools.data_type.Coord;
import org.jocelyn_chaumel.tools.debugging.JCTestCase;

/**
 * 
 * @author Jocelyn Chaumel
 */
public abstract class FSTestCase extends JCTestCase
{
	protected ApplicationConfigurationMgr _appConfMgr;

	public FSTestCase(final String aName)
	{
		super(aName);
		try
		{
			_appConfMgr = ApplicationConfigurationMgr.getCreateInstance(FileMgr.getCurrentDir());
		} catch (Exception ex)
		{
			ex.printStackTrace();
			throw new UnsupportedOperationException();
		}

	}

	public final static String[][] TWO_PLAYERS = { { "HUMAN", "1", "1" }, { "COMPUTER", "4", "4" } };

	public final static String[][] TWO_HUMAIN_PLAYERS = { { "HUMAN", "1", "1" }, { "HUMAN", "4", "4" } };

	public final static String[][] FOUR_PLAYERS = { { "HUMAN", "0", "0" }, { "COMPUTER", "10", "0" },
			{ "COMPUTER", "4", "9" }, { "COMPUTER", "15", "8" } };

	public final PlayerInfoList buildPlayerInfoList(final String[][] p_stringPlayerList)
	{
		return buildPlayerInfoList(p_stringPlayerList, PlayerLevelCst.NORMAL_LEVEL);
	}

	public final PlayerInfoList buildPlayerInfoList(final String[][] p_stringPlayerList, final PlayerLevelCst p_level)
	{
		PlayerInfoList playerInfoList = new PlayerInfoList();
		PlayerTypeCst currentOwnerType;
		Coord currentStartLocation;
		for (int i = 0; i < p_stringPlayerList.length; i++)
		{
			if (p_stringPlayerList[i][0].equals("COMPUTER"))
			{
				currentOwnerType = PlayerTypeCst.COMPUTER;
			} else
			{
				currentOwnerType = PlayerTypeCst.HUMAN;
			}
			currentStartLocation = new Coord(	Integer.parseInt(p_stringPlayerList[i][1]),
												Integer.parseInt(p_stringPlayerList[i][2]));
			playerInfoList.add(new PlayerInfo(currentOwnerType, currentStartLocation, p_level));
		}
		return playerInfoList;
	}
}