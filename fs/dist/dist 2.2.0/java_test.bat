@echo off
echo ENGLISH : This script allows to know if a Java installation is detected by FirstStrike
echo FRANCAIS : Ce script permet de savoir si une installation de l'outil Java est detectee par FirstStrike

set OLD_PATH=%PATH%
call set_java_location_manually.bat

if DEFINED JAVA_LOCATION set PATH=%JAVA_LOCATION%\bin;%PATH%
if DEFINED JAVA_HOME set PATH=%JAVA_HOME%\bin;%PATH%

set PATH=%PATH%;C:\Program Files (x86)\Java\jre8\bin;C:\Program Files (x86)\Java\jre7\bin;C:\Program Files (x86)\Java\jre6\bin;C:\Program Files\Java\jre8\bin;C:\Program Files\Java\jre7\bin;C:\Program Files\Java\jre6\bin;D:\Program Files (x86)\Java\jre8\bin;D:\Program Files (x86)\Java\jre7\bin;D:\Program Files (x86)\Java\jre6\bin;D:\Program Files\Java\jre8\bin;D:\Program Files\Java\jre7\bin;D:\Program Files\Java\jre6\bin;
java -version
echo Expected Return code / Code retour attendu : 0
echo Obtained Return code / Code retour obtenu : %errorlevel%
pause