@echo off
rem ENGLISH: 
rem Set the Java installation folder (for instance c:\java) to the JAVA_LOCATION variable below
rem Note : Do not include in the path the bin folder (for instance : c:\java\bin)


rem FRANCAIS:
rem Assigner le repertoire d'installation de Java (exemple c:\java) a la variable JAVA_LOCATION ci-dessous
rem Remarque : Ne pas inscrire le sous repertoire bin (par exemple comme ceci : c:\java\bin)


set JAVA_LOCATION=