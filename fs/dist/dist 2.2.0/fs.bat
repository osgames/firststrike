@echo off
set OLD_PATH=%PATH%
call set_java_location_manually.bat
if DEFINED JAVA_HOME set PATH=%JAVA_HOME%\bin;%PATH%
if DEFINED JAVA_LOCATION set PATH=%JAVA_LOCATION%\bin;%PATH%

set PATH=%PATH%;C:\Program Files (x86)\Java\jre8\bin;C:\Program Files (x86)\Java\jre7\bin;C:\Program Files (x86)\Java\jre6\bin;C:\Program Files\Java\jre8\bin;C:\Program Files\Java\jre7\bin;C:\Program Files\Java\jre6\bin;D:\Program Files (x86)\Java\jre8\bin;D:\Program Files (x86)\Java\jre7\bin;D:\Program Files (x86)\Java\jre6\bin;D:\Program Files\Java\jre8\bin;D:\Program Files\Java\jre7\bin;D:\Program Files\Java\jre6\bin;

start javaw -classpath lib/swing-layout-1.0.3.jar;lib/fs.jar;lib/infra.jar;lib/jdom.jar;lib/log4j-1.2.12.jar org.jocelyn_chaumel.firststrike.gui.game.FSGameLauncher %LOCALAPPDATA%\FirstStrike
set PATH=%OLD_PATH%
set OLD_PATH=

